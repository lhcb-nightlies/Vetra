// $Id: VeloPedCmSubtraction.h,v 1.1.1.1 2007-12-07 14:58:25 szumlat Exp $
#ifndef VELOPEDESTALNOISE_H 
#define VELOPEDESTALNOISE_H 1

// Include files from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// from Event model
#include "VeloEvent/EvtInfo.h"
#include "VeloEvent/VeloTELL1Data.h"

#include "VeloEvent/VeloProcessInfo.h"

#include "VetraKernel/IVeloBadChannels.h"
#include "VetraKernel/IVeloTell1List.h"

#include "Tell1Kernel/VeloTell1Core.h"

class DeVelo;
class DeVeloSensor;

//namespaces
using namespace LHCb;
using namespace VeloTELL1;

/** @class VeloPedCmSubtraction VeloPedCmSubtraction.h
 *  
 *
 *  @author Jianchun Wang
 *  @date   2006-12-16
 */
class VeloPedCmSubtraction : public GaudiTupleAlg {
public: 
  typedef std::vector< bool > bdataVec;
  typedef std::vector< bdataVec > bdataVecVec;
  typedef std::vector< bdataVecVec > bdataVecVecVec;

  typedef std::vector< unsigned int > idataVec;
  typedef std::vector< idataVec >     idataVecVec;
  typedef std::vector< idataVecVec >  idataVecVecVec;

  typedef std::vector< double >      ddataVec;
  typedef std::vector< ddataVec >    ddataVecVec;
  typedef std::vector< ddataVecVec > ddataVecVecVec;

  typedef std::list< double >        dlist;
  typedef std::vector< dlist* >      dlistVec;
  typedef std::vector< dlistVec >    dlistVecVec;
  typedef std::vector< dlistVecVec > dlistVecVecVec;


  /// Standard constructor
  VeloPedCmSubtraction( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloPedCmSubtraction( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override ;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization
  
  bool isBadChannel(int tell1, int link, int channel){return m_badChannelList[tell1][link*32+channel];}
protected:
  virtual StatusCode getTesData();
  virtual StatusCode calPedestalAndNoise();
  virtual StatusCode saveData();
  virtual StatusCode getCleanChannels();
  virtual StatusCode generateHistogram();
private:
  int m_evtNumber;
  const DeVelo* m_velo;
  VeloTELL1Datas* m_rawADCs;  // input ADC
  std::string m_rawADCsLoc;
  VeloTELL1Datas* m_subADCs;    // pedestal subtracted ADC
  std::string m_subADCsLoc;
  VeloTELL1Datas* m_cmsADCs;    // ped subtracted and CM suppressed ADC
  std::string m_cmsADCsLoc;
  VeloTELL1Datas* m_subPeds;    // pedstal
  std::string m_subPedsLoc;

  std::string m_badChannelsTool;
  std::string m_veloTell1ListTool;

  bool m_skipBadChannel;

  bdataVecVec m_badChannelList;
  bdataVecVec m_cleanChannel;

  ddataVecVecVec  m_adcValue;
  ddataVecVecVec  m_adcMean;
  ddataVecVecVec  m_pedestal;
  idataVecVecVec  m_numEventPed;
  ddataVecVecVec  m_pedSubSignal;
  ddataVecVecVec  m_cmSupSignal;

  ddataVecVec     m_cmNoise;
  idataVecVec     m_numChannelCms;

  ddataVecVec     m_chipPedestal;
  idataVecVec     m_numChannelChipPedestal;

  dlistVecVecVec  m_storedAdcLists;

  int m_numOfTell1s;
  sdataVec m_tell1List;
  sdataVec m_tell1Index;
  bdataVec m_tell1NotFound;

  int m_numInPedestalCalculation;
  int m_goodPedestalWindow;
  int m_cmGoodChannelWindow;
  int m_signalCutForCleanNoise;
  int m_largeChipSignalThr;
  int m_largeChanSignalThr;
  int m_numOfLargeChannelThr;
  
  bool m_generateHistogram;
  bool m_rejectLargeSignal;
  
  
};
#endif // VELOPEDESTALNOISE_H
