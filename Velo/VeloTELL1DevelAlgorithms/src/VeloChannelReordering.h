// $Id: VeloChannelReordering.h,v 1.1.1.1 2007-12-07 14:58:25 szumlat Exp $
#ifndef VELOPEDESTALNOISE_H 
#define VELOPEDESTALNOISE_H 1

// Include files from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// from Event model
#include "VeloEvent/EvtInfo.h"
#include "VeloEvent/VeloTELL1Data.h"

#include "VeloEvent/VeloProcessInfo.h"

#include "Tell1Kernel/VeloTell1Core.h"

class DeVelo;
class DeVeloSensor;

//namespaces
using namespace LHCb;
using namespace VeloTELL1;

/** @class VeloChannelReordering VeloChannelReordering.h
 *  
 *
 *  @author Jianchun Wang
 *  @date   2006-12-16
 */
class VeloChannelReordering : public GaudiTupleAlg {
public: 

  /// Standard constructor
  VeloChannelReordering( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloChannelReordering( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  const DeVelo* m_velo;
  std::string m_inputDataLoc;
  std::string m_outputDataLoc;
};
#endif // VELOPEDESTALNOISE_H
