// $Id: VeloClusterForming.h,v 1.1.1.1 2007-12-07 14:58:25 szumlat Exp $
#ifndef VELOCLUSTERFORMING_H 
#define VELOCLUSTERFORMING_H 1

#define MAXNUMOFCLUSTERS 1000

// Include files from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// from Event model
#include "VeloEvent/EvtInfo.h"
#include "VeloEvent/VeloTELL1Data.h"

#include "VeloEvent/VeloProcessInfo.h"

#include "VetraKernel/IVeloBadChannels.h"
#include "VetraKernel/IVeloTell1List.h"

#include "Tell1Kernel/VeloTell1Core.h"

#include "SiDAQ/SiRawBufferWord.h"


using namespace LHCb;
using namespace VeloTELL1;


class DeVelo;

/** @class VeloClusterForming VeloClusterForming.h
 *  
 *
 *  @author Jianchun Wang
 *  @date   2007-06-01
 */
class VeloClusterForming : public GaudiTupleAlg {
public: 
  typedef std::vector< bool > bdataVec;
  typedef std::vector< bdataVec > bdataVecVec;

  typedef std::vector< double >      ddataVec;
  typedef std::vector< ddataVec >    ddataVecVec;

  /// Standard constructor
  VeloClusterForming( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloClusterForming( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:
  virtual StatusCode getTesData();
  virtual StatusCode formCluster();
  virtual StatusCode generateHistogram();
  virtual StatusCode saveClusterBank();

  virtual StatusCode getDetectorConvert();
  virtual StatusCode printCluster();

  bool stripCrossBorder(int stripId, bool isRSensor, int offset);
  void dumpVeloClusters(const LHCb::VeloClusters* clusters) const;

private:
  const DeVelo* m_velo;
  std::string m_inputDataLoc;
  std::string m_veloClusterLoc;
  std::string m_badChannelsTool;
  std::string m_veloTell1ListTool;

  int m_signalSeedThreshold;
  int m_signalLowThreshold;
  int m_signalSumThreshold;
  int m_testSeedThreshold;
  int m_testLowThreshold;
  int m_testSumThreshold;
  std::vector<unsigned int> m_testSensors;

  bool m_skipBadChannel;
  bool m_generateHistogram;
  bool m_saveClusterBank;
  bool m_dumpVeloClusters;
  int m_printClusterLevel;

  int m_numOfTell1s;
  sdataVec m_tell1List;
  sdataVec m_tell1Index;
  bdataVecVec m_badStripList;
  ddataVecVec m_signalVsStrip;


  int m_totNumOfClusters;
  sdataVec m_numOfClusters;
  sdataVec m_tell1OfCluster;
  sdataVec m_numOfStrips;
  sdataVec m_firstStripId;
  ddataVec m_adcSum;
  ddataVec m_interStrip;

  std::vector<double> m_rOfStrip;  ///< Local R of strips.
  int    m_nbPhiInner;             ///< Number of strips in the internal Phi 
  double m_phiTiltInner; ///< phi tilt angle in inner sector of phi sensor
  double m_phiTiltOuter; ///< phi tilt angle in outer sector of phi sensor
};
#endif // VELOCLUSTERFORMING_H
