// $Id: VeloClusterForming.cpp,v 1.3 2010-03-22 15:02:40 erodrigu Exp $
// Include files

#include <stdio.h>
#include <math.h>

#include "Event/RawEvent.h"
#include "Event/RawBank.h"
#include "Event/VeloLiteCluster.h"
#include "Event/VeloCluster.h"

#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "GaudiKernel/SystemOfUnits.h"

// local
#include "VeloClusterForming.h"

using namespace std;

//-----------------------------------------------------------------------------
// Implementation file for class : VeloClusterForming
//
// 2007-06-01 : Jianchun Wang
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloClusterForming )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloClusterForming::VeloClusterForming( const std::string& name,
                        ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator )
  , m_inputDataLoc      ( VeloTELL1DataLocation::ReorderedADCs )
  , m_veloClusterLoc    ( VeloClusterLocation::Default )
  , m_badChannelsTool   ( "VeloBadChannels:PUBLIC" )
  , m_veloTell1ListTool ( "VeloTell1List:PUBLIC" )
{
  declareProperty( "InputDataLocation",   m_inputDataLoc);
  declareProperty( "VeloClusterLocation", m_veloClusterLoc);
  declareProperty( "BadChannelsTool" ,    m_badChannelsTool);
  declareProperty( "VeloTell1ListTool" ,  m_veloTell1ListTool);

  declareProperty( "SignalSeedThreshold", m_signalSeedThreshold = 10);
  declareProperty( "SignalLowThreshold",  m_signalLowThreshold  = 4);
  declareProperty( "SignalSumThreshold",  m_signalSumThreshold  = 20);
  declareProperty( "TestSeedThreshold",   m_testSeedThreshold = 8);
  declareProperty( "TestLowThreshold",    m_testLowThreshold  = 4);
  declareProperty( "TestSumThreshold",    m_testSumThreshold  = 15);
  
  declareProperty( "TestSensors",         m_testSensors) ;

  declareProperty( "SkipBadChannel",      m_skipBadChannel = true) ;
  declareProperty( "GenerateHistogram",   m_generateHistogram = true);
  declareProperty( "SaveClusterBank",     m_saveClusterBank = false);
  declareProperty( "DumpVeloClusters",    m_dumpVeloClusters = false);
  declareProperty( "PrintClusterLevel",   m_printClusterLevel = 0);
}
//=============================================================================
// Destructor
//=============================================================================
VeloClusterForming::~VeloClusterForming() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloClusterForming::initialize() {
  debug() << "==> Initialize" << endmsg;
  info() << "InputDataLocation   " << m_inputDataLoc        << endmsg;
  info() << "VeloClusterLocation " << m_veloClusterLoc      << endmsg;
  info() << "BadChannelsTool     " << m_badChannelsTool     << endmsg;
  info() << "VeloTell1ListTool   " << m_veloTell1ListTool   << endmsg;

  info() << "SignalSeedThreshold " << m_signalSeedThreshold << endmsg;
  info() << "SignalLowThreshold  " << m_signalLowThreshold  << endmsg;
  info() << "SignalSumThreshold  " << m_signalSumThreshold  << endmsg;

  if ( m_testSensors.size() > 0 ) {
    info() << "TestSeedThreshold " << m_testSeedThreshold << endmsg;
    info() << "TestLowThreshold  " << m_testLowThreshold  << endmsg;
    info() << "TestSumThreshold  " << m_testSumThreshold  << endmsg;
    for ( unsigned int i=0; i<m_testSensors.size(); ++i ) {
      info() << " Test sensor " << m_testSensors[i] << endmsg;
    }
  }

  info() << "SkipBadChannel    " << m_skipBadChannel    << endmsg;
  info() << "GenerateHistogram " << m_generateHistogram << endmsg;
  info() << "SaveClusterBank   " << m_saveClusterBank   << endmsg;
  info() << "DumpVeloClusters  " << m_dumpVeloClusters  << endmsg;
  info() << "PrintClusterLevel " << m_printClusterLevel << endmsg;


  setHistoTopDir("Vetra/");
  m_velo = getDet<DeVelo>(DeVeloLocation::Default);

  // Tell1 list
  const IVeloTell1List* m_veloTell1List = tool<IVeloTell1List> ( m_veloTell1ListTool , this ) ;
  m_tell1List     = m_veloTell1List->veloTell1List();
  m_tell1Index    = m_veloTell1List->veloTell1Index();
  m_numOfTell1s   = (int) m_tell1List.size();

  // Bad strip list
  m_badStripList = bdataVecVec(m_numOfTell1s, bdataVec(2048, false));

  if ( m_skipBadChannel ) {
    const IVeloBadChannels* m_badChannels   = tool<IVeloBadChannels> ( m_badChannelsTool , this ) ;
    const IVeloBadChannels::ChannelList& badChannels = m_badChannels->badTell1Strips();
    for ( int tell1=0; tell1<m_numOfTell1s; ++tell1 ) {
      int tell1Id = m_tell1List[tell1];
      for ( int channelId=0; channelId<2048; ++channelId ) {      
        if ( std::binary_search( badChannels.begin(), badChannels.end(), std::make_pair ( tell1Id, channelId ) ) ) {
          m_badStripList[tell1][channelId] = true;
        }
      }
    }
  }
  

  // Containers
  m_numOfClusters  = sdataVec(m_numOfTell1s, 0);
  m_tell1OfCluster = sdataVec(MAXNUMOFCLUSTERS, 0);
  m_numOfStrips    = sdataVec(MAXNUMOFCLUSTERS, 0);
  m_firstStripId   = sdataVec(MAXNUMOFCLUSTERS, 0);
  m_adcSum         = ddataVec(MAXNUMOFCLUSTERS, 0.0);
  m_interStrip     = ddataVec(MAXNUMOFCLUSTERS, 0.0);

  getDetectorConvert();
  
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloClusterForming::execute() {

  debug() << "==> Execute" << endmsg;

  // Find TES data
  StatusCode sc = getTesData();
  if (sc == StatusCode::FAILURE) {
    warning() << "Failed to extract ADC data from TES" << endmsg;
    return sc;
  }

  // form cluster
  sc = formCluster();
  if (sc == StatusCode::FAILURE) {
    warning() << "Fail to form cluster" << endmsg;
    return sc;
  }

  // Save cluster in TES
  if ( m_saveClusterBank ) { 
    sc = saveClusterBank();
    if (sc == StatusCode::FAILURE) {
      warning() << "Fail to save cluster bank" << endmsg;
      return sc;
    }
  }
  
  // Generate monitor histograms
  if ( m_generateHistogram ){
    sc = generateHistogram();
    if (sc == StatusCode::FAILURE) {
      warning() << "Fail to generate histograms" << endmsg;
      return sc;
    }
  }
  
  // Print out 
  if ( m_printClusterLevel > 0 ) printCluster();
  return StatusCode::SUCCESS;
}


//=============================================================================
//  Get TES stored ped/cm subtracted ADC Values
//=============================================================================
StatusCode VeloClusterForming::getTesData() {
  debug()<< " ==> getTesData() " << endmsg;

  if ( ! exist<VeloTELL1Datas>( m_inputDataLoc ) ){
    info()<< " ==> There is no input data at: "
           << m_inputDataLoc << endmsg;
    return ( StatusCode::FAILURE );
  }

  VeloTELL1Datas* m_stripADCs = get<VeloTELL1Datas>( m_inputDataLoc );

  // extract strip ADC values of all TELL1s
  int tell1;
  int link;
  int channel;
  VeloTELL1Datas::const_iterator sensIt;
  m_signalVsStrip = ddataVecVec(m_numOfTell1s, ddataVec(2048, 0));

  for (sensIt=m_stripADCs->begin(); sensIt!=m_stripADCs->end(); ++sensIt){
    VeloTELL1Data* adcData = (*sensIt);

    const int tell1Id = adcData->key();
    tell1 = m_tell1Index[tell1Id];
    if ( tell1 >= 0 ) {
      for (link=0; link<64; link++){
        VeloTELL1::ALinkPair iTPair=(*adcData)[link];
        VeloTELL1::scdatIt iT=iTPair.first;
        for (channel=0; channel<32; channel++) {
          int strip = link*32 + channel;
          m_signalVsStrip[tell1][strip] = *(iT+channel);
        }
      }
    }
  }

  return StatusCode::SUCCESS;
}


//=============================================================================
//  form cluster
//=============================================================================
StatusCode VeloClusterForming::formCluster()
{
  int tell1;
  int firstStrip;
  int lastStrip;
  int seedStrip;
  int strip;
  int numOfStrips;
  double adcSum;
  double interStrip;
  
  int seedThreshold;
  int lowThreshold;
  int sumThreshold;

  m_totNumOfClusters = 0;
  for (tell1 = 0; tell1 < m_numOfTell1s; tell1 ++) {
    m_numOfClusters[tell1] = 0;
  }

  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
    const DeVeloSensor* sensor = m_velo->sensorByTell1Id(m_tell1List[tell1]);
    
    if ( find(m_testSensors.begin(),m_testSensors.end(),sensor->sensorNumber()) == m_testSensors.end() ) {
      seedThreshold = m_signalSeedThreshold;
      lowThreshold  = m_signalLowThreshold;
      sumThreshold  = m_signalSumThreshold;
    } else {
      seedThreshold = m_testSeedThreshold;
      lowThreshold  = m_testLowThreshold;
      sumThreshold  = m_testSumThreshold;
    }
    
    seedStrip = 0;
    while ( seedStrip < 2048 ) {
      if ( m_signalVsStrip[tell1][seedStrip] < seedThreshold) {
        seedStrip ++;
      } else {
        lastStrip = seedStrip + 1;
        while ( ! stripCrossBorder(lastStrip, sensor->isR(), +1) &&
                m_signalVsStrip[tell1][lastStrip] >= seedThreshold ) lastStrip++;

        if ( stripCrossBorder(lastStrip, sensor->isR(), +1) || 
             m_signalVsStrip[tell1][lastStrip] < lowThreshold ) lastStrip --;

        firstStrip = seedStrip - 1;
        if ( stripCrossBorder(firstStrip, sensor->isR(), -1) || 
             m_signalVsStrip[tell1][firstStrip] < lowThreshold ) firstStrip ++;

        // Save cluster
        numOfStrips = lastStrip - firstStrip + 1;
        adcSum = 0.0;
        interStrip = 0.0;

        for ( strip = firstStrip; strip <=lastStrip; strip++ ) {
          adcSum     += m_signalVsStrip[tell1][strip];
          interStrip += m_signalVsStrip[tell1][strip] * (strip - firstStrip);
        }
        interStrip /= adcSum;
        
        bool goodStrips = true;
        for ( strip = firstStrip; strip <=lastStrip; strip++ ) {
          if ( m_badStripList[tell1][strip] ) goodStrips = false;
        }

        if ( adcSum >= sumThreshold && goodStrips ) {
          m_numOfClusters[tell1] ++;

          if ( m_totNumOfClusters < MAXNUMOFCLUSTERS ) {
            m_tell1OfCluster[m_totNumOfClusters] = m_tell1List[tell1];
            m_numOfStrips   [m_totNumOfClusters] = numOfStrips;
            m_firstStripId  [m_totNumOfClusters] = firstStrip;
            m_adcSum        [m_totNumOfClusters] = adcSum;
            m_interStrip    [m_totNumOfClusters] = interStrip;
          }
          m_totNumOfClusters ++;


          debug() << "Form::   SN=" << sensor->sensorNumber()
                  << "  N=" << numOfStrips
                  << "  FC=" << firstStrip
                  << "  ISP=" << interStrip
                  << "  ADCs:";
          for ( strip = firstStrip; strip <=lastStrip; strip++ ) {
            debug() << "  "
                    << m_signalVsStrip[tell1][strip] << "@"
                    << strip;
          }
          debug() << endmsg;

        }
        seedStrip = lastStrip + 1;
      }
      
    }
  }
  
  return StatusCode::SUCCESS;
}


//=============================================================================
//  Save cluster bank
//=============================================================================
StatusCode VeloClusterForming::saveClusterBank()
{
  int cluster;
  int i;
  
  LHCb::VeloClusters* clusters = new LHCb::VeloClusters();
  clusters->reserve(m_totNumOfClusters);

  for ( cluster = 0; cluster < m_totNumOfClusters; cluster++ ) {

    int tell1Id = m_tell1OfCluster[cluster];
    int tell1   = m_tell1Index[tell1Id];
    const DeVeloSensor* sensor = m_velo->sensorByTell1Id(tell1Id);
    if ( !sensor ){
      error() << " could not map tell1Id "
              << tell1Id
              << endmsg;
      return StatusCode::FAILURE;
    }
    
    double fracStrip = m_interStrip[cluster];
    unsigned int sensorNumber = (unsigned int) sensor->sensorNumber();
    unsigned int stripNumber  = (unsigned int) fracStrip + m_firstStripId[cluster];

    LHCb::VeloChannelID chan(sensorNumber,stripNumber);

    LHCb::VeloLiteCluster liteCluster(chan, fracStrip, (unsigned int) m_adcSum[cluster], true);

    LHCb::VeloCluster::ADCVector adcs;
    adcs.reserve(m_numOfStrips[cluster]);
    int firstStrip = m_firstStripId[cluster];
    for ( i = 0; i < m_numOfStrips[cluster]; i++ ) {
      adcs.push_back( std::pair<int,unsigned int>
                      (static_cast<int>(firstStrip+i),
                       static_cast<unsigned int> (m_signalVsStrip[tell1][firstStrip+i]) ));
    }

    clusters->insert(new LHCb::VeloCluster(liteCluster, adcs), chan);
  }

  put(clusters, m_veloClusterLoc);

  if (m_dumpVeloClusters) dumpVeloClusters(clusters);

  return StatusCode::SUCCESS;
}

//=============================================================================
//  get convertion parameters
//=============================================================================
StatusCode VeloClusterForming::getDetectorConvert()
{
  const DeVeloRType* rSens=(*(m_velo->rSensorsBegin()));
  const unsigned int numRStrips = rSens->stripsInZone(0);
  
  for ( unsigned int iSt = 0 ; numRStrips > iSt ; ++iSt ) {
    double rStrip = rSens->rOfStrip( iSt );
    m_rOfStrip.push_back( rStrip );
  }
  m_rOfStrip.push_back( rSens->rMax(0) );

  debug() << " numRstrips " << numRStrips << endmsg;
  debug() << " Rfirst " << m_rOfStrip[0] << endmsg;
  debug() << " Rlast " << m_rOfStrip[511] << endmsg;
  debug() << " Rextra " << m_rOfStrip[512] << endmsg;

  const DeVeloPhiType* phiSens=(*(m_velo->phiSensorsBegin()));
  int innerZone  = 0;
  int innerStrip = 0;
  m_nbPhiInner   = phiSens->stripsInZone(innerZone);
  LHCb::VeloChannelID innerChan(innerStrip);
  LHCb::VeloChannelID outerChan(m_nbPhiInner);
  m_phiTiltInner = phiSens->angleOfStrip(innerChan,0.);
  m_phiTiltOuter = phiSens->angleOfStrip(outerChan,0.);

  debug() << " nbPhiInner " <<  m_nbPhiInner << endmsg;
  debug() << " phiTiltInner " << m_phiTiltInner << endmsg;
  debug() << " phiTiltOuter " << m_phiTiltOuter << endmsg;

  return StatusCode::SUCCESS;
}


  
//=============================================================================
//  print out cluster
//=============================================================================
StatusCode VeloClusterForming::printCluster()
{
  int cluster;
  int i;
  
  for ( cluster = 0; cluster < m_totNumOfClusters; cluster++ ) {
    int tell1Id = m_tell1OfCluster[cluster];
    int tell1   = m_tell1Index[tell1Id];
    const DeVeloSensor* sensor = m_velo->sensorByTell1Id(tell1Id);
    if ( !sensor ){
      error() << " could not map tell1Id "
              << tell1Id
              << endmsg;
      return StatusCode::FAILURE;
    }
    
    unsigned int sensorNumber = (unsigned int) sensor->sensorNumber();

    double weightedStrip = m_interStrip[cluster]+ m_firstStripId[cluster];
    unsigned int stripNumber  = (unsigned int) weightedStrip;
    double fracStrip = weightedStrip - stripNumber;

    unsigned int zone;
    double z = sensor->z();
    double rp = 0;
    unsigned sensorType = 0;

    if ( sensor->isR() ) {
      const DeVeloRType* rSensor = m_velo->rSensor(sensorNumber);
      double r = rSensor->rOfStrip(stripNumber,fracStrip);
      zone=rSensor->zoneOfStrip(stripNumber);
      rp = r;
      
    } else {
      const DeVeloPhiType* pSensor = m_velo->phiSensor(sensorNumber);

      zone = pSensor->zoneOfStrip(stripNumber);
      double testRad1 = pSensor->rMin(zone);
      double testRad2 = pSensor->rMax(zone);
      double testRad  = (testRad1 + testRad2 ) / 2;
      // adapt to change in DeVelo
      //double testPhi = pSensor->phiOfStrip(stripNumber, fracStrip, testRad);
      //double phiCoor = pSensor->localPhiToGlobal(testPhi);
      double phiCoor=pSensor->idealPhi(stripNumber, fracStrip, testRad);
      rp = phiCoor;

      sensorType = 1; // 0 for R
      if ( sensor->isDownstream() ) sensorType += 1;
      if ( sensor->isRight() ) sensorType += 2;
      
//       cout << "  zone " << zone
//            << "  testRad1 " << testRad1
//            << "  testRad2 " << testRad2
//            << "  testPhi "  << testPhi
//            << "  phiCoor "  << phiCoor
//            << endl;
    }
    
    if ( m_printClusterLevel == 1 ) {
      info() << "Form:: "
             << "  SN="     << sensorNumber
             << "  TY="     << sensorType
             << "  ISR="    << sensor->isR()
             << "  N="      << m_numOfStrips[cluster]
             << "  FS="     << m_firstStripId[cluster]
             << "  WS="     << weightedStrip
             << "  SA="     << m_adcSum[cluster]
             << "  Z="      << z
             << "  RP="     << rp
             << "  ADCs:";    
      for ( i = 0; i < m_numOfStrips[cluster]; i++ ) {
        info() << "   " << m_signalVsStrip[tell1][m_firstStripId[cluster]+i];
      }
      info() << endmsg;
    }

    if ( m_printClusterLevel > 1 ) {
      info() << "SavedClusters:: "
             << "  " << sensorNumber
             << "  " << sensorType
             << "  " << sensor->isR()
             << "  " << m_numOfStrips[cluster]
             << "  " << m_firstStripId[cluster]
             << "  " << weightedStrip
             << "  " << m_adcSum[cluster]
             << "  " << rp;
      for ( i = 0; i < m_numOfStrips[cluster]; i++ ) {
        info() << "   " << m_signalVsStrip[tell1][m_firstStripId[cluster]+i];
      }
      info() << endmsg;
    }
  }
  
  return StatusCode::SUCCESS;
}

//=============================================================================
//  generate histograms
//=============================================================================
StatusCode VeloClusterForming::generateHistogram()
{
  
  char histoName[80];
  char tell1Tag[80];
  string sTell1Tag;
  int tell1;
  int tell1Id;
  int cluster;

  plot1D(m_totNumOfClusters, "TELL1_all/hTotNumOfClusters", "Total Number of Clusters", -0.5, 99.5, 100);
  int numOfSensors = 0;
  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
    if ( m_numOfClusters[tell1] > 0 ) numOfSensors++;
  }
  plot1D(numOfSensors, "TELL1_all/hNumOfSignalSensors", "Num of Sensors with Signal", -0.5, 14.5, 15);

  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
    sprintf(histoName, "TELL1_%d/hNumOfClusters", m_tell1List[tell1]);
    plot1D(m_numOfClusters[tell1], histoName, "Num of Clusters", -0.5, 39.5, 40);
  }

  int numSingleClusterSensor = 0;
  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
    if ( m_numOfClusters[tell1] == 1 ) numSingleClusterSensor ++;
  }
  
  for ( cluster = 0; cluster < m_totNumOfClusters; cluster++ ) {
    tell1Id = m_tell1OfCluster[cluster];
    sprintf(tell1Tag, "TELL1_%d/", tell1Id);
    string sTell1Tag = tell1Tag;

    plot1D(m_numOfStrips[cluster], sTell1Tag+"hNumOfStrips", "Num of Strips", -0.5, 9.5, 10);

    plot1D(m_adcSum[cluster], sTell1Tag+"hAdcSum", "ADC sum", 0.0, 150.0, 150);

    plot1D(m_firstStripId[cluster], sTell1Tag+"hFirstStrip", "First Strip of all cluter", -0.5, 2047.5, 128);

    if ( m_numOfStrips[cluster] > 1 ) {
      plot1D(m_firstStripId[cluster], sTell1Tag+"hFirstStrip2", "First Strip of Cluter with 2 or More Strips", -0.5, 2047.5, 128);
    }
    if ( m_numOfStrips[cluster] > 2 ) {
      plot1D(m_firstStripId[cluster], sTell1Tag+"hFirstStrip3", "First Strip of Cluter with 3 or More Strips", -0.5, 2047.5, 128);
    }

    bool goodPlane = false;
    for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
      if ( m_numOfClusters[tell1] == 1 && 
           m_tell1List[tell1] == tell1Id ) goodPlane = true;
    }
    if (  numSingleClusterSensor < 5 ) goodPlane = false;

    if ( goodPlane ) {
      plot1D(m_numOfStrips[cluster], sTell1Tag+"hNumOfStrips_s", "Num of Strips", -0.5, 9.5, 10);

      plot1D(m_adcSum[cluster], sTell1Tag+"hAdcSum_s", "ADC sum", 0.0, 150.0, 150);
      
      plot1D(m_firstStripId[cluster], sTell1Tag+"hFirstStrip_s", "First Strip of all cluter", -0.5, 2047.5, 128);
      
      if ( m_numOfStrips[cluster] > 1 ) {
        plot1D(m_firstStripId[cluster], sTell1Tag+"hFirstStrip2_s",
               "First Strip of Cluter with 2 or More Strips", -0.5, 2047.5, 128);
      }
      if ( m_numOfStrips[cluster] > 2 ) {
        plot1D(m_firstStripId[cluster], sTell1Tag+"hFirstStrip3_s",
               "First Strip of Cluter with 3 or More Strips", -0.5, 2047.5, 128);
      }
    }
    
  }
  

  return StatusCode::SUCCESS;
}


bool VeloClusterForming::stripCrossBorder(int stripId, bool isRSensor, int offset) {

  bool crossBoarder = false;
  if ( isRSensor ) {
    if ( offset == -1 ) {
      if ( stripId ==   -1 ) crossBoarder = true;
      if ( stripId ==  511 ) crossBoarder = true;
      if ( stripId == 1023 ) crossBoarder = true;
      if ( stripId == 1535 ) crossBoarder = true;
    } else {
      if ( stripId ==  512 ) crossBoarder = true;
      if ( stripId == 1024 ) crossBoarder = true;
      if ( stripId == 1536 ) crossBoarder = true;
      if ( stripId == 2048 ) crossBoarder = true;
    }
  } else {
    if ( offset == -1 ) {
      if ( stripId ==   -1 ) crossBoarder = true;
      if ( stripId ==  682 ) crossBoarder = true;
    } else {
      if ( stripId ==  683 ) crossBoarder = true;
      if ( stripId == 2048 ) crossBoarder = true;
    }
  }

  return crossBoarder;
}

void VeloClusterForming::dumpVeloClusters(const LHCb::VeloClusters* clusters) const
{
  //  std::vector<float> adcValues;
  
  for (LHCb::VeloClusters::const_iterator ci =  clusters->begin(); 
       ci != clusters->end(); 
       ++ci) {

    const LHCb::VeloCluster* clu = *ci;
    
    unsigned int sensorNumber = clu->channelID().sensor();
    unsigned int centroidStrip = clu->channelID().strip();
    double interStripPos = clu->interStripFraction();

    info() << "Save::  SN=" << sensorNumber
           << "  N="  << clu->size()
           << "  CS=" << centroidStrip
           << "  ISP=" << interStripPos
           << "  ADCs:";

    for (unsigned int adci=0; adci<clu->size(); ++adci) {
      info() << "  " << clu->adcValue(adci)
	     << "@" << clu->strip(adci);
    }
    info() << endmsg;
      
  }
  return;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloClusterForming::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

//=============================================================================
