// $Id: VeloPedNoiseMonitor.cpp,v 1.2 2010-03-22 15:07:00 erodrigu Exp $
// Include files

#include <stdio.h>
#include <math.h>

#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h"

// local
#include "VeloPedNoiseMonitor.h"

using namespace std;

//-----------------------------------------------------------------------------
// Implementation file for class : VeloPedNoiseMonitor
//
// 2006-12-16 : Jianchun Wang
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloPedNoiseMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloPedNoiseMonitor::VeloPedNoiseMonitor( const std::string& name,
                                                ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator )
    ,  m_rawADCsLoc ( LHCb::VeloTELL1DataLocation::ADCs )
    ,  m_subADCsLoc ( LHCb::VeloTELL1DataLocation::PedSubADCs )
    ,  m_cmsADCsLoc ( VeloTELL1DataLocation::CMSuppressedADCs )
    ,  m_subPedsLoc ( LHCb::VeloTELL1DataLocation::SubPeds )
    ,  m_badChannelsTool ( "VeloBadChannels:PUBLIC" )
    ,  m_veloTell1ListTool ( "VeloTell1List:PUBLIC" )
{
  declareProperty( "inputRawADCLocation",  m_rawADCsLoc);
  declareProperty( "inputPedsADCLocation", m_subADCsLoc);
  declareProperty( "inputCmsADCLocation",  m_cmsADCsLoc);
  declareProperty( "inputPedLocation",     m_subPedsLoc);
  
  declareProperty( "SkipBadChannel",       m_skipBadChannel = true) ;
  declareProperty( "BadChannelsTool" ,     m_badChannelsTool);
  declareProperty( "VeloTell1ListTool" ,   m_veloTell1ListTool);

  declareProperty( "SignalCutForCleanNoise",   m_signalCutForCleanNoise = 10);

}
//=============================================================================
// Destructor
//=============================================================================
VeloPedNoiseMonitor::~VeloPedNoiseMonitor() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloPedNoiseMonitor::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiTupleAlg

  debug()<< "==> Initialize" << endmsg;
  info() << "inputRawADCLocation  "  << m_rawADCsLoc << endmsg;
  info() << "inputPedsADCLocation  " << m_subADCsLoc << endmsg;
  info() << "inputCmsADCLocation  "  << m_cmsADCsLoc << endmsg;
  info() << "inputPedLocation   "    << m_subPedsLoc << endmsg;

  info() << "SignalCutForCleanNoise  " << m_signalCutForCleanNoise << endmsg;

  setHistoTopDir("Vetra/");
  m_velo = getDet<DeVelo>(DeVeloLocation::Default);

  // Tell1 list
  const IVeloTell1List* m_veloTell1List = tool<IVeloTell1List> ( m_veloTell1ListTool , this ) ;
  m_tell1List     = m_veloTell1List->veloTell1List();
  m_tell1Index    = m_veloTell1List->veloTell1Index();
  m_numOfTell1s   = (int) m_tell1List.size();

  // Bad strip list
  m_badChannelList = bdataVecVec(m_numOfTell1s, bdataVec(2048, false));

  const IVeloBadChannels* m_badChannels   = tool<IVeloBadChannels> ( m_badChannelsTool , this ) ;
  const IVeloBadChannels::ChannelList& badChannels = m_badChannels->badTell1Channels() ;
  for ( int tell1=0; tell1<m_numOfTell1s; ++tell1 ) {
    int tell1Id = m_tell1List[tell1];
    for ( int channelId=0; channelId<2048; ++channelId ) {      
      if ( std::binary_search( badChannels.begin() , badChannels.end() , std::make_pair ( tell1Id, channelId ) ) ) {
        m_badChannelList[tell1][channelId] = true;
      }
    }    
  }

  // Containers
  m_adcValue     = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));
  m_pedestal     = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));
  m_pedSubSignal = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));
  m_cmSupSignal  = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));

  return StatusCode::SUCCESS;
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloPedNoiseMonitor::execute() {

  debug() << "==> Execute" << endmsg;
  StatusCode sc;

  // Find the ADC data blocks
  sc = getTesData();
  if (sc == StatusCode::FAILURE) {
    warning() << "Failed to extract ADC data from the data" << endmsg;
    return sc;
  }

  // Find clean channels
  sc = getCleanChannels();
  if (sc == StatusCode::FAILURE) {
    warning() << "Fail to get clean channels" << endmsg;
    return sc;
  }

  sc = generateHistogram();
  if (sc == StatusCode::FAILURE) {
    warning() << "Fail to generate histograms" << endmsg;
    return sc;
  }

  return StatusCode::SUCCESS;
}


//=============================================================================
//  Get TES ADC pedestal and so on
//=============================================================================
StatusCode VeloPedNoiseMonitor::getTesData() {
  debug()<< "==> getTesData() " << endmsg;

  int tell1;
  int link;
  int channel;
  VeloTELL1Datas::const_iterator sensIt;

  m_rawADCsFound = false;  
  if ( exist<VeloTELL1Datas>( m_rawADCsLoc ) ){
    VeloTELL1Datas* m_rawADCs = get<VeloTELL1Datas>( m_rawADCsLoc );
    debug() << m_rawADCsLoc << " has " << m_rawADCs->size() << endmsg;
    for (sensIt=m_rawADCs->begin(); sensIt!=m_rawADCs->end(); ++sensIt){
      VeloTELL1Data* adcData = (*sensIt);
      const int tell1Id = adcData->key();
      tell1 = m_tell1Index[tell1Id];
      if ( tell1 >= 0 ) {
        for (link=0; link<64; link++){
        VeloTELL1::ALinkPair iTPair=(*adcData)[link];
        VeloTELL1::scdatIt iT=iTPair.first;
          for (channel=0; channel<32; channel++) {
            m_adcValue[tell1][link][channel] = *(iT+channel);
          }
        }
      }
    }
    m_rawADCsFound = true;
  }

  m_subADCsFound = false;  
  if ( exist<VeloTELL1Datas>( m_subADCsLoc ) ){
    VeloTELL1Datas* m_subADCs = get<VeloTELL1Datas>( m_subADCsLoc );
    debug() << m_subADCsLoc << " has " << m_subADCs->size() << endmsg;
    for (sensIt=m_subADCs->begin(); sensIt!=m_subADCs->end(); ++sensIt){
      VeloTELL1Data* adcData = (*sensIt);
      const int tell1Id = adcData->key();
      tell1 = m_tell1Index[tell1Id];
      if ( tell1 >= 0 ) {
        for (link=0; link<64; link++){
        VeloTELL1::ALinkPair iTPair=(*adcData)[link];
        VeloTELL1::scdatIt iT=iTPair.first;
          for (channel=0; channel<32; channel++) {
            m_pedSubSignal[tell1][link][channel] = *(iT+channel);
          }
        }
      }
    }
    m_subADCsFound = true;
  }

  m_cmsADCsFound = false;  
  if ( exist<VeloTELL1Datas>( m_cmsADCsLoc ) ){
    VeloTELL1Datas* m_cmsADCs = get<VeloTELL1Datas>( m_cmsADCsLoc );
    debug() << m_cmsADCsLoc << " has " << m_cmsADCs->size() << endmsg;
    for (sensIt=m_cmsADCs->begin(); sensIt!=m_cmsADCs->end(); ++sensIt){
      VeloTELL1Data* adcData = (*sensIt);
      const int tell1Id = adcData->key();
      tell1 = m_tell1Index[tell1Id];
      if ( tell1 >= 0 ) {
        for (link=0; link<64; link++){
        VeloTELL1::ALinkPair iTPair=(*adcData)[link];
        VeloTELL1::scdatIt iT=iTPair.first;
          for (channel=0; channel<32; channel++) {
            m_cmSupSignal[tell1][link][channel] = *(iT+channel);
          }
        }
      }
    }
    m_cmsADCsFound = true;
  }

  m_subPedsFound = false;  
  if ( exist<VeloTELL1Datas>( m_subPedsLoc ) ){
    VeloTELL1Datas* m_subPeds = get<VeloTELL1Datas>( m_subPedsLoc );
    debug() << m_subPedsLoc << " has " << m_subPeds->size() << endmsg;
    for (sensIt=m_subPeds->begin(); sensIt!=m_subPeds->end(); ++sensIt){
      VeloTELL1Data* adcData = (*sensIt);
      const int tell1Id = adcData->key();
      tell1 = m_tell1Index[tell1Id];
      if ( tell1 >= 0 ) {
        for (link=0; link<64; link++){
        VeloTELL1::ALinkPair iTPair=(*adcData)[link];
        VeloTELL1::scdatIt iT=iTPair.first;
          for (channel=0; channel<32; channel++) {
            m_pedestal[tell1][link][channel] = *(iT+channel);
          }
        }
      }
    }
    m_subPedsFound = true;
  }

  if ( ! (m_rawADCsFound || m_subADCsFound || m_cmsADCsFound || m_subPedsFound) ) {
    info() << " No bank found " << endmsg;
  }

  return StatusCode::SUCCESS;
}

StatusCode VeloPedNoiseMonitor::getCleanChannels()
{
  int tell1;
  int tell1Id;
  int strip;
  int link;
  int channel;
  int channelEid;
  int channelAll;

  m_cleanChannel = bdataVecVec (m_numOfTell1s, bdataVec(2048, true));

  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
    tell1Id = m_tell1List[tell1];
    const DeVeloSensor* sensor = m_velo->sensorByTell1Id(tell1Id);

    // remove channels on or near a cluster
    for ( channelAll=0; channelAll<2048; channelAll++ ) {
      strip = sensor->ChipChannelToStrip(channelAll);
      link = channelAll/32;
      channel = channelAll%32;

      bool busyChannel = false;
      if ( m_cmsADCsFound ) {
        busyChannel = (m_cmSupSignal[tell1][link][channel] > m_signalCutForCleanNoise);
      } else {
        busyChannel = (m_pedSubSignal[tell1][link][channel] > m_signalCutForCleanNoise);
      }
      if ( busyChannel ) {
        for ( int i=strip-1; i<strip+2; i++) {
          if ( i >=0 && i<2048 ) {
            channelEid = sensor->StripToChipChannel(i);
            m_cleanChannel[tell1][channelEid] = false;
          }
        }
      }
    }
  }
  
  return StatusCode::SUCCESS;
}


//=============================================================================
//  generate histograms
//=============================================================================
StatusCode VeloPedNoiseMonitor::generateHistogram()
{
  char tell1Tag[80];
  string sTell1Tag;
  int tell1;
  int link;
  int channel;
  int channelAll;
  
  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
    sprintf(tell1Tag, "TELL1_%d/", m_tell1List[tell1]);
    string sTell1Tag = tell1Tag;

    for (link=0; link<64; link++){
      for (channel=0; channel<32; channel++) {
        channelAll = link*32 + channel;

        // Raw ADC
        if ( m_rawADCsFound ) {
          plot2D(channelAll, m_adcValue[tell1][link][channel],
                 sTell1Tag+"hAdcValue", "Raw ADC Value",
                 -0.5, 2047.5, 450.5, 650.5, 2048, 200);
        }

        if ( m_rawADCsFound && m_cleanChannel[tell1][channelAll] ) {
          plot2D(channelAll, m_adcValue[tell1][link][channel],
                 sTell1Tag+"hsAdcValue", "Raw ADC Value",
                 -0.5, 2047.5, 450.5, 650.5, 2048, 200);
        }
        
        // Skip bad channels
        if ( m_skipBadChannel && m_badChannelList[tell1][channelAll] ) continue;

        if ( m_subPedsFound ) {
          plot2D(channelAll, m_pedestal[tell1][link][channel],
                 sTell1Tag+"hPedestal", "Pedestal",
                 -0.5, 2047.5, 489.5, 589.5, 2048, 100);
        }
        
        // Pedestal subtracted ADC
        if ( m_subADCsFound ) {
          plot2D(channelAll, m_pedSubSignal[tell1][link][channel],
                 sTell1Tag+"hPedSubSignal", "Pedestal subtracted ADC value",
                 -0.5, 2047.5, -40.0, 110.0, 2048, 150);
          
          plot1D(m_pedSubSignal[tell1][link][channel],
                 sTell1Tag+"hPedSubSignalDist", "Pedestal subtracted ADC value",
                 -40.0, 110.0, 150);
        }

        if ( m_subADCsFound && m_cleanChannel[tell1][channelAll] ) {
          plot2D(channelAll, m_pedSubSignal[tell1][link][channel],
                 sTell1Tag+"hsPedSubSignal", "Pedestal subtracted ADC value",
                 -0.5, 2047.5, -40.0, 110.0, 2048, 150);

          plot1D(m_pedSubSignal[tell1][link][channel],
                 sTell1Tag+"hsPedSubSignalDist", "Pedestal subtracted ADC value",
                 -40.0, 110.0, 150);
        }
        

        // Pedestal subtracted, CM suppressed ADC
        if ( m_cmsADCsFound ) {
          plot2D(channelAll, m_cmSupSignal[tell1][link][channel],
                 sTell1Tag+"hCmSupSignal", "Common mode noise suppressed ADC value",
                 -0.5, 2047.5, -40.0, 110.0, 2048, 150);

          plot1D(m_cmSupSignal[tell1][link][channel],
                 sTell1Tag+"hCmSupSignalDist", "Common mode noise suppressed ADC value",
                 -40.0, 110.0, 150);
        }
        
        if ( m_cmsADCsFound && m_cleanChannel[tell1][channelAll] ) {
          plot2D(channelAll, m_cmSupSignal[tell1][link][channel],
                 sTell1Tag+"hsCmSupSignal", "Common mode noise suppressed ADC value",
                 -0.5, 2047.5, -40.0, 110.0, 2048, 150);

          plot1D(m_cmSupSignal[tell1][link][channel],
                 sTell1Tag+"hsCmSupSignalDist", "Common mode noise suppressed ADC value",
                 -40.0, 110.0, 150);
        }
      }
    }
  }

  return StatusCode::SUCCESS;
}


//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloPedNoiseMonitor::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}

//=============================================================================
