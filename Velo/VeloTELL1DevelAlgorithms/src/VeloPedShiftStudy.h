// $Id: VeloPedShiftStudy.h,v 1.1.1.1 2007-12-07 14:58:25 szumlat Exp $
#ifndef VELOPEDSHIFTSTUDY_H 
#define VELOPEDSHIFTSTUDY_H 1

// Include files from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// from Event model
#include "VeloEvent/EvtInfo.h"
#include "VeloEvent/VeloTELL1Data.h"

#include "VeloEvent/VeloProcessInfo.h"

#include "VetraKernel/IVeloBadChannels.h"
#include "VetraKernel/IVeloTell1List.h"

class DeVelo;
class DeVeloSensor;

//namespaces
using namespace LHCb;
using namespace VeloTELL1;

/** @class VeloPedShiftStudy VeloPedShiftStudy.h
 *  
 *
 *  @author Jianchun Wang
 *  @date   2007-06-01
 */
class VeloPedShiftStudy : public GaudiTupleAlg {
public: 
  typedef std::vector< bool > bdataVec;
  typedef std::vector< bdataVec > bdataVecVec;
  typedef std::vector< double >      ddataVec;
  typedef std::vector< ddataVec >    ddataVecVec;
  typedef std::vector< ddataVecVec > ddataVecVecVec;
  
  /// Standard constructor
  VeloPedShiftStudy( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloPedShiftStudy( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:
  virtual StatusCode getTesData();
  virtual StatusCode pedestalShift();
  //  virtual StatusCode extractPedSignal();

private:
  int m_evtNumber;

  const DeVelo* m_velo;
  std::string m_rawADCsLoc;    // Raw ADC
  std::string m_subADCsLoc;    // pedestal subtracted ADC
  std::string m_cmsADCsLoc;    // ped subtracted and CM suppressed ADC
  std::string m_subPedsLoc;    // pedstal

  std::string m_badChannelsTool;
  std::string m_veloTell1ListTool;

  std::vector<int> m_eventList;

  bool m_skipBadChannel;
  bdataVecVec m_badChannelList;

  int m_numOfTell1s;
  sdataVec m_tell1List;
  sdataVec m_tell1Index;
  
  ddataVecVecVec m_adcValue;
  ddataVecVecVec m_pedestal;
  ddataVecVecVec m_pedSubSignal;
  ddataVecVecVec m_cmSupSignal;
  ddataVecVec m_pedShiftLink;
  ddataVecVec m_pedShiftChip;

};
#endif // VELOPEDSHIFTSTUDY_H
