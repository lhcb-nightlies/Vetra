// $Id: VeloPedCmSubtraction.cpp,v 1.2 2010-03-22 15:07:00 erodrigu Exp $
// Include files

#include <stdio.h>
#include <math.h>

#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h"

// local
#include "VeloPedCmSubtraction.h"

using namespace std;

//-----------------------------------------------------------------------------
// Implementation file for class : VeloPedCmSubtraction
//
// 2006-12-16 : Jianchun Wang
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloPedCmSubtraction )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloPedCmSubtraction::VeloPedCmSubtraction( const std::string& name,
                                                ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator )
    ,  m_evtNumber ( 0 )
    ,  m_rawADCs ( 0 )
    ,  m_rawADCsLoc ( VeloTELL1DataLocation::ADCs )
    ,  m_subADCs ( 0 )
    ,  m_subADCsLoc ( VeloTELL1DataLocation::PedSubADCs )
    ,  m_cmsADCs ( 0 )
    ,  m_cmsADCsLoc ( VeloTELL1DataLocation::CMSuppressedADCs )
    ,  m_subPeds ( 0 )
    ,  m_subPedsLoc ( VeloTELL1DataLocation::SubPeds )
    ,  m_badChannelsTool ( "VeloBadChannels:PUBLIC" )
    ,  m_veloTell1ListTool ( "VeloTell1List:PUBLIC" )
{
  declareProperty( "InputRawADCLocation",   m_rawADCsLoc);
  declareProperty( "OutputPedsADCLocation", m_subADCsLoc);
  declareProperty( "OutputPedLocation",     m_subPedsLoc);
  declareProperty( "OutputCmsADCLocation",  m_cmsADCsLoc);
  
  declareProperty( "NumInPedestalCalculation", m_numInPedestalCalculation = 100);
  declareProperty( "GoodPedestalWindow",       m_goodPedestalWindow  = 10);

  declareProperty( "RejectLargeSignal",        m_rejectLargeSignal = true);
  declareProperty( "LargeChipSignalThr",       m_largeChipSignalThr = 500);
  declareProperty( "LargeChanSignalThr",       m_largeChanSignalThr = 150);
  declareProperty( "NumOfLargeChannelThr",     m_numOfLargeChannelThr = 1);

  declareProperty( "GenerateHistogram",        m_generateHistogram = false);
  declareProperty( "SignalCutForCleanNoise",   m_signalCutForCleanNoise = 10);

  declareProperty( "SkipBadChannel",       m_skipBadChannel = true) ;
  declareProperty( "BadChannelsTool" ,     m_badChannelsTool);
  declareProperty( "VeloTell1ListTool" ,   m_veloTell1ListTool);

  declareProperty( "CMGoodChannelWindow",      m_cmGoodChannelWindow = 10);
  
}
//=============================================================================
// Destructor
//=============================================================================
VeloPedCmSubtraction::~VeloPedCmSubtraction() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloPedCmSubtraction::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiTupleAlg

  debug() << "==> Initialize" << endmsg;
  info() <<  "InputRawADCLocation   "  << m_rawADCsLoc << endmsg;
  info() <<  "OutputPedsADCLocation  " << m_subADCsLoc   << endmsg;
  info() <<  "OutputCmsADCLocation  "  << m_cmsADCsLoc   << endmsg;
  info() <<  "OutputPedLocation   "    << m_subPedsLoc   << endmsg;
  info() << "NumInPedestalCalculation  " << m_numInPedestalCalculation << endmsg;
  info() << "GoodPedestalWindow  "       << m_goodPedestalWindow << endmsg;
  info() << "RejectLargeSignal  "        << m_rejectLargeSignal << endmsg;
  if ( m_rejectLargeSignal ) {
    info() << "LargeChipSignalThr  "     << m_largeChipSignalThr << endmsg;
    info() << "LargeChanSignalThr  "     << m_largeChanSignalThr << endmsg;
    info() << "NumOfLargeChannelThr  "   << m_numOfLargeChannelThr << endmsg;
  }
  info() << "GenerateHistogram  "        << m_generateHistogram << endmsg;
  if ( m_generateHistogram ) {
    info() << "SignalCutForCleanNoise  "   << m_signalCutForCleanNoise << endmsg;
  }
  info() << "CMGoodChannelWindow  "      << m_cmGoodChannelWindow << endmsg;

  setHistoTopDir("Vetra/");
  m_velo = getDet<DeVelo>(DeVeloLocation::Default);

  // Tell1 list
  const IVeloTell1List* m_veloTell1List = tool<IVeloTell1List> ( m_veloTell1ListTool , this ) ;
  m_tell1List     = m_veloTell1List->veloTell1List();
  m_tell1Index    = m_veloTell1List->veloTell1Index();
  m_numOfTell1s   = (int) m_tell1List.size();

  // Bad strip list
  m_badChannelList = bdataVecVec(m_numOfTell1s, bdataVec(2048, false));

  const IVeloBadChannels* m_badChannels = tool<IVeloBadChannels> ( m_badChannelsTool , this ) ;
  const IVeloBadChannels::ChannelList& badChannels = m_badChannels->badTell1Channels() ;
  for ( int tell1=0; tell1<m_numOfTell1s; ++tell1 ) {
    int tell1Id = m_tell1List[tell1];
    for ( int channelId=0; channelId<2048; ++channelId ) {      
      if ( std::binary_search( badChannels.begin() , badChannels.end() , std::make_pair ( tell1Id, channelId ) ) ) {
        m_badChannelList[tell1][channelId] = true;
      }
    }    
  }

  // containers
  m_adcValue     = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));
  m_adcMean      = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));
  m_pedestal     = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));
  m_numEventPed  = idataVecVecVec(m_numOfTell1s, idataVecVec(64, idataVec(32, 0)));
  m_pedSubSignal = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));
  m_cmSupSignal  = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));

  m_cmNoise       = ddataVecVec(m_numOfTell1s, ddataVec(64, 0.0));
  m_numChannelCms = idataVecVec(m_numOfTell1s, idataVec(64, 0));

  m_chipPedestal           = ddataVecVec(m_numOfTell1s, ddataVec(16, 0.0));
  m_numChannelChipPedestal = idataVecVec(m_numOfTell1s, idataVec(16, 0));

  m_tell1NotFound = bdataVec(m_numOfTell1s, false);

  m_storedAdcLists.resize(m_numOfTell1s);
  for (int i=0; i<m_numOfTell1s; i++){
    m_storedAdcLists[i].resize(64);
    for (int j=0; j<64; j++) {
      m_storedAdcLists[i][j].resize(32);
      for(int k=0; k<32; k++){
        m_storedAdcLists[i][j][k] = new dlist();
      }
    }
  }
 
  return StatusCode::SUCCESS;
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloPedCmSubtraction::execute() {

  debug() << "==> Execute" << endmsg;
  StatusCode sc;

  m_evtNumber ++;
  debug() << "  Event counts : " << m_evtNumber << endmsg;

  // Find the ADC data blocks
  sc = getTesData();
  if (sc == StatusCode::FAILURE) {
    warning() << "Failed to extract ADC data from TES" << endmsg;
    return sc;
  }

  // Calculate pedestal, CM noise, and subtraction
  sc = calPedestalAndNoise();
  if (sc == StatusCode::FAILURE) {
    warning() << "Failed to calculate pedestal, cmNoise" << endmsg;
    return sc;
  }

  // Save pedestal, pedestal subtracted ADC & common mode suppressed ADC
  sc = saveData();
  if (sc == StatusCode::FAILURE) {
    warning() << "Failed to save pedestal subtracted ADCs" << endmsg;
    return sc;
  }

  // Generate monitor histograms
  //if ( m_generateHistogram &&  m_evtNumber>m_numInPedestalCalculation ){
  if ( m_generateHistogram ) {
    sc = getCleanChannels();
    if (sc == StatusCode::FAILURE) {
      warning() << "Fail to get clean channels" << endmsg;
      return sc;
    }

    sc = generateHistogram();
    if (sc == StatusCode::FAILURE) {
      warning() << "Fail to generate histograms" << endmsg;
      return sc;
    }
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Get TES stored raw ADC Values
//=============================================================================
StatusCode VeloPedCmSubtraction::getTesData() {
  debug()<< " ==> getTesData() " << endmsg;

  if ( ! exist<VeloTELL1Datas>( m_rawADCsLoc ) ){
    info()<< " ==> There is no decoded ADCs at: "
           << m_rawADCsLoc << endmsg;

    if ( exist<VeloTELL1Datas>(VeloTELL1DataLocation::ADCs ) )          info()<<" Exist Raw/Velo/DecodedADC"<< endmsg;
    if ( exist<VeloTELL1Datas>(VeloTELL1DataLocation::Pedestals ) )     info()<<" Exist Raw/Velo/DecodedPed"<< endmsg;
    if ( exist<VeloTELL1Datas>(VeloTELL1DataLocation::Headers ) )       info()<<" Exist Raw/Velo/DecodedHeaders"<< endmsg;
    if ( exist<VeloTELL1Datas>(VeloTELL1DataLocation::SimADCs ) )       info()<<" Exist Raw/Velo/SimulatedADC"<< endmsg;
    if ( exist<VeloTELL1Datas>(VeloTELL1DataLocation::SimPeds ) )       info()<<" Exist Raw/Velo/SimulatedPed"<< endmsg;
    if ( exist<VeloTELL1Datas>(VeloTELL1DataLocation::SubPeds ) )       info()<<" Exist Raw/Velo/SubtractedPed"<< endmsg;
    if ( exist<VeloTELL1Datas>(VeloTELL1DataLocation::PedSubADCs ) )    info()<<" Exist Raw/Velo/SubtractedPedADCs"<<endmsg;
    if ( exist<VeloTELL1Datas>(VeloTELL1DataLocation::FIRCorrectedADCs))info()<<" Exist Raw/Velo/FIRCorrected"<< endmsg;
    if ( exist<VeloTELL1Datas>(VeloTELL1DataLocation::BitLimitADCs ) )  info()<<" Exist Raw/Velo/ADC8Bit"<< endmsg;
    if ( exist<VeloTELL1Datas>(VeloTELL1DataLocation::ReorderedADCs ) ) info()<<" Exist Raw/Velo/ADCReordered"<< endmsg;
    if ( exist<VeloTELL1Datas>(VeloTELL1DataLocation::CMSuppressedADCs))info()<<" Exist Raw/Velo/ADCCMSuppressed"<< endmsg;
    if ( exist<VeloTELL1Datas>(VeloTELL1DataLocation::CMSNoise ) )      info()<<" Exist Raw/Velo/CMSNoise"<< endmsg;
    return ( StatusCode::FAILURE );
  }

  m_rawADCs = get<VeloTELL1Datas>( m_rawADCsLoc );
  debug()<< " ==> The data banks have been read-in from location: "
         << m_rawADCsLoc
         << ", size of data container (number of read-out TELL1s): "
         << m_rawADCs->size() << endmsg;

  // extract ADC values of all TELL1s
  int tell1;
  int link;
  int channel;

  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
    m_tell1NotFound[tell1] = true;
  }

  VeloTELL1Datas::const_iterator sensIt;
  for (sensIt=m_rawADCs->begin(); sensIt!=m_rawADCs->end(); ++sensIt){

    VeloTELL1Data* adcData = (*sensIt);

    const int tell1Id = adcData->key();
    tell1 = m_tell1Index[tell1Id];
    if ( tell1 >= 0 ) {
      m_tell1NotFound[tell1] = false;

      for (link=0; link<64; link++){
        VeloTELL1::ALinkPair iTPair=(*adcData)[link];
        VeloTELL1::scdatIt iT=iTPair.first;
        for (channel=0; channel<32; channel++) {
          m_adcValue[tell1][link][channel] = *(iT+channel);
        }
      }
    }
  }

  StatusCode sc = StatusCode::SUCCESS;
  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
    if ( m_tell1NotFound[tell1] ) {
      debug() << " Tell1 " << m_tell1List[tell1]
              << " not presents in this events " << endmsg;
      sc = StatusCode::FAILURE;
    }
  }
  return sc;
}


//=============================================================================
//  calculate pedestal, noise, cm noise, signal, and save events
//=============================================================================
StatusCode VeloPedCmSubtraction::calPedestalAndNoise()
{
  int tell1;
  int chip;
  int link;
  int channel;
  double adcSum;
  double adcMean;
  double pedestal;
  double cmNoise;
  int numOfSelected;
  double linkMean;
  double linkPedestal;
  double chipMean;
  double chipPedestal;

  

  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {

    for (link=0; link<64; link++){

      // Calculate ADC mean of 32 channels in one link using current event
      adcSum = 0.0;
      numOfSelected = 0;
      for (channel=1; channel<32; channel++) {
        if ( ! isBadChannel(tell1,link,channel) ) {
          adcSum += m_adcValue[tell1][link][channel];
          numOfSelected ++;
        }
      }
      linkMean = ( numOfSelected > 10 ) ? adcSum / double(numOfSelected) : 512.0;

      // Calculate rough pedestal of this link using current event
      adcSum = 0.0;
      numOfSelected = 0;
      for (channel=1; channel<32; channel++) {
        if ( ! isBadChannel(tell1,link,channel) &&
             fabs(m_adcValue[tell1][link][channel]-linkMean) < m_goodPedestalWindow ){
          adcSum += m_adcValue[tell1][link][channel];
          numOfSelected ++;
        }
      }
      linkPedestal = ( numOfSelected > 10 ) ? adcSum / double(numOfSelected) : linkMean;

      for (channel=0; channel<32; channel++) {
        dlist *storedPeds = m_storedAdcLists[tell1][link][channel];
        dlist::const_iterator it;

        // calculate ADC mean using stored events
        adcSum = 0.0;
        numOfSelected = 0;
        for( it=storedPeds->begin(); it!=storedPeds->end(); it++){
          adcSum += (*it);
          numOfSelected ++;
        }
        adcMean =  ( numOfSelected > 10 ) ? adcSum / double(numOfSelected) : linkPedestal;

        // calculate pedestal
        adcSum = 0.0;
        numOfSelected = 0;
        for( it=storedPeds->begin(); it!=storedPeds->end(); it++){
          if ( fabs((*it)-adcMean) < m_goodPedestalWindow ){
            adcSum += (*it);
            numOfSelected ++;
          }
        }
        pedestal = ( numOfSelected > 10 ) ? adcSum / double(numOfSelected) : adcMean;

        m_adcMean[tell1][link][channel] = adcMean;
        m_pedestal[tell1][link][channel] = pedestal;
        m_numEventPed[tell1][link][channel] = numOfSelected;
        m_pedSubSignal[tell1][link][channel] = m_adcValue[tell1][link][channel] - pedestal;
      }
    }
    

    // Calculate common mode noise
    for ( chip=0; chip<16; chip++ ) {

      // Step1: chip level, using channels with signal < m_cmGoodChannelWindow, to recover JC effect
      adcSum = 0.0;
      numOfSelected = 0;
      for ( link=chip*4; link<chip*4+4; link++ ) {
        for (channel=1; channel<32; channel++) {
          if ( ! isBadChannel(tell1,link,channel) &&
               m_pedSubSignal[tell1][link][channel] < m_cmGoodChannelWindow ) {
            adcSum += m_pedSubSignal[tell1][link][channel];
            numOfSelected ++;
          }
        }
      }
      chipMean = ( numOfSelected > 40 ) ? adcSum / double(numOfSelected) : 0.0;

      // Step2: chip level, channels with |signal-C| < m_cmGoodChannelWindow
      adcSum = 0.0;
      numOfSelected = 0;
      for ( link=chip*4; link<chip*4+4; link++ ) {
        for (channel=1; channel<32; channel++) {
          if ( ! isBadChannel(tell1,link,channel) &&
               fabs(m_pedSubSignal[tell1][link][channel]-chipMean) < m_cmGoodChannelWindow ) {
            adcSum += m_pedSubSignal[tell1][link][channel];
            numOfSelected ++;
          }
        }
      }
      chipPedestal = ( numOfSelected > 40 ) ? adcSum / double(numOfSelected) : 0.0;
      m_chipPedestal[tell1][chip]           = chipPedestal;
      m_numChannelChipPedestal[tell1][chip] = numOfSelected;
    

      // calculate common mode noise, step3: link level channels with |signal-C| < m_cmGoodChannelWindow
      for ( link=chip*4; link<chip*4+4; link++ ) {
        adcSum = 0.0;
        numOfSelected = 0;
        cmNoise = chipPedestal;
        
        for (channel=1; channel<32; channel++) {
          if ( ! isBadChannel(tell1,link,channel) &&
               fabs(m_pedSubSignal[tell1][link][channel] - cmNoise) < m_cmGoodChannelWindow ) {
            adcSum += m_pedSubSignal[tell1][link][channel];
            numOfSelected ++;
          }
        }
        cmNoise = ( numOfSelected > 10 ) ? adcSum / double(numOfSelected) : 0.0;
        m_cmNoise[tell1][link]       = cmNoise;
        m_numChannelCms[tell1][link] = numOfSelected;

        for (channel=0; channel<32; channel++) {
          m_cmSupSignal[tell1][link][channel] = m_pedSubSignal[tell1][link][channel] - cmNoise;
        }
      }


      // save event for future pedestal/noise calculation
      double signalTotal = 0.0;
      int numOfLargeChannel = 0;
      
      if ( m_rejectLargeSignal ) {
        for ( link=chip*4; link<chip*4+4; link++ ) {
          for (channel=0; channel<32; channel++) {
            if ( ! isBadChannel(tell1, link, channel) ) {
              signalTotal += m_cmSupSignal[tell1][link][channel];
              if ( m_cmSupSignal[tell1][link][channel] > m_largeChanSignalThr ) numOfLargeChannel++;
            }
          }
        }
      }
      
      if ( ! m_rejectLargeSignal ||
           ( signalTotal < m_largeChipSignalThr
             && numOfLargeChannel < m_numOfLargeChannelThr ) ) {
        for ( link=chip*4; link<chip*4+4; link++ ) {
          for (channel=0; channel<32; channel++) {
            dlist *storedPeds = m_storedAdcLists[tell1][link][channel];
            if ( storedPeds->size() >= (unsigned int) m_numInPedestalCalculation ) storedPeds->pop_front();
            storedPeds->push_back( m_adcValue[tell1][link][channel] );
          }
        }
      } else {
        debug() << " Rejected event " <<   m_evtNumber
                << "  tell1" << m_tell1List[tell1]
                << "  chip " << chip << endmsg;        
      }      
    }    
  }

  return StatusCode::SUCCESS;
}


//=============================================================================
//  save pedestals, pedestal subtracted ADCs and CM suppressed ADCs
//=============================================================================
StatusCode VeloPedCmSubtraction::saveData()
{
  m_subADCs = new VeloTELL1Datas();
  m_cmsADCs = new VeloTELL1Datas();
  m_subPeds = new VeloTELL1Datas();
  
  int tell1;
  int tell1Id;  
  int link;
  int channel;

  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
    tell1Id = m_tell1List[tell1];   

    sdataVec subADCsLink, cmsADCsLink, subPedsLink;

    for (link=0; link<64; link++){
      for (channel=0; channel<32; channel++) {
        subADCsLink.push_back( (int) m_pedSubSignal[tell1][link][channel] );
        cmsADCsLink.push_back( (int) m_cmSupSignal [tell1][link][channel] );
        subPedsLink.push_back( (int) m_pedestal    [tell1][link][channel] );
      }
    }

    VeloTELL1Data* subADCs = new VeloTELL1Data(tell1Id, VeloFull);
    subADCs->setDecodedData(subADCsLink);
    m_subADCs->insert(subADCs);

    VeloTELL1Data* cmsADCs = new VeloTELL1Data(tell1Id, VeloFull);
    cmsADCs->setDecodedData(cmsADCsLink);
    m_cmsADCs->insert(cmsADCs);

    VeloTELL1Data* subPeds = new VeloTELL1Data(tell1Id, VeloPedestal);
    subPeds->setDecodedData(subPedsLink);
    m_subPeds->insert(subPeds);
  }

  put(m_subADCs, m_subADCsLoc);
  put(m_cmsADCs, m_cmsADCsLoc);
  put(m_subPeds, m_subPedsLoc);

  return StatusCode::SUCCESS;
}


StatusCode VeloPedCmSubtraction::getCleanChannels()
{
  int tell1;
  int tell1Id;
  int strip;
  int link;
  int channel;
  int channelEid;
  int channelAll;

  m_cleanChannel = bdataVecVec (m_numOfTell1s, bdataVec(2048, true));

  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
    tell1Id = m_tell1List[tell1];
    const DeVeloSensor* sensor = m_velo->sensorByTell1Id(tell1Id);

    // remove channels on or near a cluster
    for ( channelAll=0; channelAll<2048; channelAll++ ) {
      strip = sensor->ChipChannelToStrip(channelAll);
      link = channelAll/32;
      channel = channelAll%32;
      if ( m_cmSupSignal[tell1][link][channel] > m_signalCutForCleanNoise ) {
        for ( int i=strip-1; i<strip+2; i++) {
          if ( i >=0 && i<2048 ) {
            channelEid = sensor->StripToChipChannel(i);
            m_cleanChannel[tell1][channelEid] = false;
          }
        }
      }
    }
  }
  
  return StatusCode::SUCCESS;
}


//=============================================================================
//  generate histograms
//=============================================================================
StatusCode VeloPedCmSubtraction::generateHistogram()
{
  char tell1Tag[80];
  string sTell1Tag;
  int tell1;
  int chip;
  int link;
  int channel;
  int channelAll;
  
  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
    sprintf(tell1Tag, "TELL1_%d/", m_tell1List[tell1]);
    string sTell1Tag = tell1Tag;

    // Chip level signal, remove large signal due to JC effect
    for ( chip=0; chip<16; chip++ ) {
      double signalTotal = 0.0;
      int numOfLargeChannel = 0;
      
      for ( link=chip*4; link<chip*4+4; link++ ) {
        for (channel=0; channel<32; channel++) {
          if ( ! isBadChannel(tell1, link, channel) ) {
            signalTotal += m_cmSupSignal[tell1][link][channel];
            if ( m_cmSupSignal[tell1][link][channel] > m_largeChanSignalThr ) numOfLargeChannel++;
            plot1D(m_cmSupSignal[tell1][link][channel], sTell1Tag+"hChanSignalDist",
                   "Channel Signal Distribution", -100.0, 400.0, 100);      
          }
        }
      }

      plot1D(signalTotal, sTell1Tag+"hChipSignalDist", "Chip Signal Distribution", -100.0, 3900.0, 80);

      plot1D(numOfLargeChannel, sTell1Tag+"hNumOfLargeChannel", "Number of Large Signal Channel", -0.5, 29.5, 30);

      if ( signalTotal >= m_largeChipSignalThr ||
           numOfLargeChannel >= m_numOfLargeChannelThr ) {
        plot1D(chip, sTell1Tag+"hLargeSignalEvent", "Large Signal Event vs Chip", -0.5, 15.5, 16);
      }
    }


    // channel level
    for (link=0; link<64; link++){
      for (channel=0; channel<32; channel++) {
        channelAll = link*32 + channel;

        plot2D(channelAll, m_adcValue[tell1][link][channel], sTell1Tag+"hAdcValue",
               "Raw ADC Value", -0.5, 2047.5, 450.5, 650.5, 2048, 200);

        if ( m_cleanChannel[tell1][channelAll] ) {
          plot2D(channelAll, m_adcValue[tell1][link][channel], sTell1Tag+"hsAdcValue",
                 "Raw ADC Value", -0.5, 2047.5, 450.5, 650.5, 2048, 200);
        }

        // Skip bad channels
        if ( m_skipBadChannel && m_badChannelList[tell1][channelAll] ) continue;

        plot2D(channelAll, m_pedestal[tell1][link][channel], sTell1Tag+"hPedestal",
               "Pedestal", -0.5, 2047.5, 490, 590.0, 2048, 200);

        plot2D(channelAll, m_pedSubSignal[tell1][link][channel], sTell1Tag+"hPedSubSignal",
               "Pedestal subtracted ADC value", -0.5, 2047.5, -40.0, 110.0, 2048, 300);

        plot1D(m_pedSubSignal[tell1][link][channel], sTell1Tag+"hPedSubSignalDist",
               "Pedestal subtracted ADC value", -40.0, 110.0, 300);

        if ( m_cleanChannel[tell1][channelAll] ) {
          plot2D(channelAll, m_pedSubSignal[tell1][link][channel], sTell1Tag+"hsPedSubSignal",
                 "Pedestal subtracted ADC value", -0.5, 2047.5, -40.0, 110.0, 2048, 300);

          plot1D(m_pedSubSignal[tell1][link][channel], sTell1Tag+"hsPedSubSignalDist",
                 "Pedestal subtracted ADC value", -40.0, 110.0, 300);
        
        }

        plot2D(channelAll, m_cmSupSignal[tell1][link][channel], sTell1Tag+"hCmSupSignal",
               "Common mode noise suppressed ADC value", -0.5, 2047.5, -40.0, 110.0, 2048, 300);

        plot1D(m_cmSupSignal[tell1][link][channel], sTell1Tag+"hCmSupSignalDist",
               "Common mode noise suppressed ADC value", -40.0, 110.0, 300);

        if ( m_cleanChannel[tell1][channelAll] ) {
          plot2D(channelAll, m_cmSupSignal[tell1][link][channel], sTell1Tag+"hsCmSupSignal",
                 "Common mode noise suppressed ADC value", -0.5, 2047.5, -40.0, 110.0, 2048, 300);

          plot1D(m_cmSupSignal[tell1][link][channel], sTell1Tag+"hsCmSupSignalDist",
                 "Common mode noise suppressed ADC value", -40.0, 110.0, 300);
        }

        plot2D(channelAll, m_numEventPed[tell1][link][channel], sTell1Tag+"hNumEventPed",
               "Number of events in pedestal calculation",
               -0.5, 2047.5, -0.5, m_numInPedestalCalculation+0.5, 2048, m_numInPedestalCalculation+1);

        plot1D(m_numEventPed[tell1][link][channel], sTell1Tag+"hNumEventPedDist",
               "Number of events in pedestal calculation",
               -0.5, m_numInPedestalCalculation+0.5, m_numInPedestalCalculation+1);

      }
      
      plot2D(link, m_cmNoise[tell1][link], sTell1Tag+"hCmNoise",
             "Common mode noise", -0.5, 63.5, -5.0, 5.0, 64, 50);

      plot1D(m_cmNoise[tell1][link], sTell1Tag+"hCmNoiseDist",
             "Common Mode Noise Distribution", -5.0, 5.0, 50);

      plot2D(link, m_numChannelCms[tell1][link], sTell1Tag+"hNumChannelCms",
             "Number of channels used in CM calculation", -0.5, 63.5, -0.5, 32.5, 2048, 33);

      plot1D(m_numChannelCms[tell1][link], sTell1Tag+"hNumChannelCmsDist",
             "Number of channels used in CM calculation", -0.5, 32.5, 33);
    }
    
    for ( chip=0; chip<16; chip++ ) {
      plot2D(chip, m_chipPedestal[tell1][chip], sTell1Tag+"hChipPedestal",
             "Chip pedestal shift", -0.5, 15.5, -110.0, 10.0, 16, 60);

      plot1D(m_chipPedestal[tell1][chip], sTell1Tag+"hChipPedestalDist",
             "Chip pedestal shift", -110.0, 10.0, 120);

      plot1D(m_numChannelChipPedestal[tell1][chip], sTell1Tag+"hNumChannelChipPedestal",
             "Number of channels used in chip pedestal shift calculation", -0.5, 129.5, 130);
    }
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloPedCmSubtraction::finalize() {

  debug() << "==> Finalize" << endmsg;

  for (int i=0; i<m_numOfTell1s; i++){
    for (int j=0; j<64; j++) {
      for(int k=0; k<32; k++){
        delete m_storedAdcLists[i][j][k];
      }
    }
  }

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}

//=============================================================================
