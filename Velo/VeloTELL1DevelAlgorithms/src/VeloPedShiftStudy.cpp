// $Id: VeloPedShiftStudy.cpp,v 1.2 2010-03-22 15:10:45 erodrigu Exp $
// Include files

#include <stdio.h>
#include <math.h>

#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 

// local
#include "VeloPedShiftStudy.h"

using namespace std;

//-----------------------------------------------------------------------------
// Implementation file for class : VeloPedShiftStudy
//
// 2007-06-01 : Jianchun Wang
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloPedShiftStudy )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloPedShiftStudy::VeloPedShiftStudy( const std::string& name,
                              ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator )
  , m_evtNumber(0)
  , m_rawADCsLoc ( LHCb::VeloTELL1DataLocation::ADCs )
  , m_subADCsLoc ( LHCb::VeloTELL1DataLocation::PedSubADCs )
  , m_cmsADCsLoc ( VeloTELL1DataLocation::CMSuppressedADCs )
  , m_subPedsLoc ( LHCb::VeloTELL1DataLocation::SubPeds )
  , m_badChannelsTool ( "VeloBadChannels:PUBLIC" )
  , m_veloTell1ListTool ( "VeloTell1List:PUBLIC" )
{
  declareProperty( "inputRawADCLocation",  m_rawADCsLoc);
  declareProperty( "inputPedsADCLocation", m_subADCsLoc);
  declareProperty( "inputCmsADCLocation",  m_cmsADCsLoc);
  declareProperty( "inputPedLocation",     m_subPedsLoc);
  
  declareProperty( "SkipBadChannel",       m_skipBadChannel = true) ;
  declareProperty( "InterestingEvents",    m_eventList);

  declareProperty( "BadChannelsTool" ,     m_badChannelsTool);
  declareProperty( "VeloTell1ListTool" ,   m_veloTell1ListTool);

}
//=============================================================================
// Destructor
//=============================================================================
VeloPedShiftStudy::~VeloPedShiftStudy() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloPedShiftStudy::initialize() {
  debug() << "==> Initialize" << endmsg;
  info() << "inputRawADCLocation  "  << m_rawADCsLoc << endmsg;
  info() << "inputPedsADCLocation  " << m_subADCsLoc << endmsg;
  info() << "inputCmsADCLocation  "  << m_cmsADCsLoc << endmsg;
  info() << "inputPedLocation   "    << m_subPedsLoc << endmsg;

  setHistoTopDir("Vetra/");
  
  m_velo = getDet<DeVelo>(DeVeloLocation::Default);

  // Tell1 list
  const IVeloTell1List* m_veloTell1List = tool<IVeloTell1List> ( m_veloTell1ListTool , this ) ;
  m_tell1List     = m_veloTell1List->veloTell1List();
  m_tell1Index    = m_veloTell1List->veloTell1Index();
  m_numOfTell1s   = (int) m_tell1List.size();

  // Bad strip list
  m_badChannelList = bdataVecVec(m_numOfTell1s, bdataVec(2048, false));

  const IVeloBadChannels* m_badChannels   = tool<IVeloBadChannels> ( m_badChannelsTool , this ) ;
  const IVeloBadChannels::ChannelList& badChannels = m_badChannels->badTell1Channels() ;
  for ( int tell1=0; tell1<m_numOfTell1s; ++tell1 ) {
    int tell1Id = m_tell1List[tell1];
    for ( int channelId=0; channelId<2048; ++channelId ) {      
      if ( std::binary_search( badChannels.begin() , badChannels.end() , std::make_pair ( tell1Id, channelId ) ) ) {
        m_badChannelList[tell1][channelId] = true;
      }
    }    
  }

  // Containers
  m_adcValue     = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));
  m_pedestal     = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));
  m_pedSubSignal = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));
  m_cmSupSignal  = ddataVecVecVec(m_numOfTell1s, ddataVecVec(64, ddataVec(32, 0.0)));
  m_pedShiftLink = ddataVecVec(m_numOfTell1s, ddataVec(64, 0.0));
  m_pedShiftChip = ddataVecVec(m_numOfTell1s, ddataVec(16, 0.0));

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloPedShiftStudy::execute() {

  debug() << "==> Execute" << endmsg;
  StatusCode sc;

  m_evtNumber ++;
  debug() << "  Event counts : " << m_evtNumber << endmsg;

  // Find the ADC data blocks
  sc = getTesData();
  if (sc == StatusCode::FAILURE) {
    warning() << "Failed to extract ADC data from the data" << endmsg;
    return sc;
  }

  // Check pedestal shift
  sc = pedestalShift();
  if (sc == StatusCode::FAILURE) {
    warning() << "Fail to study pedestal shift" << endmsg;
    return sc;
  }

  return StatusCode::SUCCESS;
}


//=============================================================================
//  Get TES ADC pedestal and so on
//=============================================================================
StatusCode VeloPedShiftStudy::getTesData() {
  debug()<< "==> getTesData() " << endmsg;

  int tell1;
  int link;
  int channel;
  VeloTELL1Datas::const_iterator sensIt;


  if ( ! exist<VeloTELL1Datas>( m_rawADCsLoc ) ){
    info() << m_rawADCsLoc << " bank missing " << endmsg;
    return StatusCode::FAILURE;
  }
  if ( ! exist<VeloTELL1Datas>( m_subADCsLoc ) ){
    info() << m_subADCsLoc << " bank missing " << endmsg;
    return StatusCode::FAILURE;
  }
  if ( ! exist<VeloTELL1Datas>( m_cmsADCsLoc ) ){
    info() << m_cmsADCsLoc << " bank missing " << endmsg;
    return StatusCode::FAILURE;
  }
  if ( ! exist<VeloTELL1Datas>( m_subPedsLoc ) ){
    info() << m_subPedsLoc << " bank missing " << endmsg;
    return StatusCode::FAILURE;
  }

  VeloTELL1Datas* m_rawADCs = get<VeloTELL1Datas>( m_rawADCsLoc );
  for (sensIt=m_rawADCs->begin(); sensIt!=m_rawADCs->end(); ++sensIt){
    VeloTELL1Data* adcData = (*sensIt);
    const int tell1Id = adcData->key();
    tell1 = m_tell1Index[tell1Id];
    if ( tell1 >= 0 ) {
      for (link=0; link<64; link++){
        VeloTELL1::ALinkPair iTPair=(*adcData)[link];
        VeloTELL1::scdatIt iT=iTPair.first;
        for (channel=0; channel<32; channel++) {
          m_adcValue[tell1][link][channel] = *(iT+channel);
        }
      }
    }
  }

  VeloTELL1Datas* m_subADCs = get<VeloTELL1Datas>( m_subADCsLoc );
  for (sensIt=m_subADCs->begin(); sensIt!=m_subADCs->end(); ++sensIt){
    VeloTELL1Data* adcData = (*sensIt);
    const int tell1Id = adcData->key();
    tell1 = m_tell1Index[tell1Id];
    if ( tell1 >= 0 ) {
      for (link=0; link<64; link++){
        VeloTELL1::ALinkPair iTPair=(*adcData)[link];
        VeloTELL1::scdatIt iT=iTPair.first;
        for (channel=0; channel<32; channel++) {
          m_pedSubSignal[tell1][link][channel] = *(iT+channel);
        }
      }
    }
  }

  VeloTELL1Datas* m_cmsADCs = get<VeloTELL1Datas>( m_cmsADCsLoc );
  for (sensIt=m_cmsADCs->begin(); sensIt!=m_cmsADCs->end(); ++sensIt){
    VeloTELL1Data* adcData = (*sensIt);
    const int tell1Id = adcData->key();
    tell1 = m_tell1Index[tell1Id];
    if ( tell1 >= 0 ) {
      for (link=0; link<64; link++){
        VeloTELL1::ALinkPair iTPair=(*adcData)[link];
        VeloTELL1::scdatIt iT=iTPair.first;
        for (channel=0; channel<32; channel++) {
          m_cmSupSignal[tell1][link][channel] = *(iT+channel);
        }
      }
    }
  }
  
  VeloTELL1Datas* m_subPeds = get<VeloTELL1Datas>( m_subPedsLoc );
  debug() << m_subPedsLoc << " has " << m_subPeds->size() << endmsg;
  for (sensIt=m_subPeds->begin(); sensIt!=m_subPeds->end(); ++sensIt){
    VeloTELL1Data* adcData = (*sensIt);
    const int tell1Id = adcData->key();
    tell1 = m_tell1Index[tell1Id];
    if ( tell1 >= 0 ) {
      for (link=0; link<64; link++){
        VeloTELL1::ALinkPair iTPair=(*adcData)[link];
        VeloTELL1::scdatIt iT=iTPair.first;
        for (channel=0; channel<32; channel++) {
          m_pedestal[tell1][link][channel] = *(iT+channel);
        }
      }
    }
  }

  return StatusCode::SUCCESS;
}


//=============================================================================
//  study pedestal shift
//=============================================================================
StatusCode VeloPedShiftStudy::pedestalShift()
{
  char tell1Tag[80];
  string sTell1Tag;
  int tell1;
  int link;
  int chip;
  int channelAll;
  int channel;
  int i;

  // I want to force the booking of some histograms, instead of plot1d
  if (  m_evtNumber == 1 ) {
    for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
      sprintf(tell1Tag, "TELL1_%d/", m_tell1List[tell1]);
      string sTell1Tag = tell1Tag;

      book1D(sTell1Tag+"hPosSignalVsLink",  "Signal (20 - 80) vs Link ID", -0.5, 63.5, 64);
      book1D(sTell1Tag+"hUpsetChanVsLink",  "Upset Channel vs Link ID", -0.5, 63.5, 64);
      book1D(sTell1Tag+"hUpsetLinkId",  "Upset Link ID", -0.5, 63.5, 64);
      book1D(sTell1Tag+"hUpsetChipId",  "Upset Chip ID", -0.5, 15.5, 16);
      book1D(sTell1Tag+"hNumUpsetLink",  "Number of Upset Links Per Chip",  -0.5, 4.5, 5);

      // all
      book1D(sTell1Tag+"hNumUpsetChan",  "Number of Upset Channels in A Chip",  -0.5, 128.5, 129);
      book1D(sTell1Tag+"hAllSignal",  "All Signal", -500.0, 500.0, 200);
      book1D(sTell1Tag+"hMaxSignal",  "Maximum Signal in A Chip", -100.0, 500.0, 120);
      book1D(sTell1Tag+"hMinSignal",  "Minimim Signal in A Chip", -100.0, 50.0, 50);
      book1D(sTell1Tag+"hNumHighChan",  "N Chanel (Signal>250) in A Chip", -0.5, 29.5, 30);

      book1D(sTell1Tag+"hAllAdcMean",  "ADC Mean of All Channels (Signal<20) in A Chip", -100.0, 50.0, 30);
      book1D(sTell1Tag+"hAllAdcRms",  "ADC Rms of All Channels (Signal<20) in A Chip", 0.0, 150.0, 50);
      book1D(sTell1Tag+"hUpsetAdcMean",  "ADC Mean of Upset Channels in A Chip", -100.0, 50.0, 30);
      book1D(sTell1Tag+"hUpsetAdcRms",  "ADC Rms of Upset Channels in A Chip", 0.0, 50.0, 50);

      book2D(sTell1Tag+"hNupsetVsNhigh",  "Number of Upset Channels vs Number of High in A Chip",
             0.5, 24.5, 24, -0.5, 128.5, 129);
      book2D(sTell1Tag+"hAdcRmsVsNhigh",  "ADC Rms vs Number of High in A Chip",
             0.5, 24.5, 24, 0.0, 150, 50);

      // Upset Chip
      book1D(sTell1Tag+"hNumUpsetChanUpset", "Number of Upset Channels in An Upset Chip",  -0.5, 128.5, 129);
      book1D(sTell1Tag+"hAllSignalUpset", "All Signal in An Upset Chip", -500.0, 500.0, 200);
      book1D(sTell1Tag+"hMaxSignalUpset", "Maximum Signal in An Upset Chip", -100.0, 500.0, 120);
      book1D(sTell1Tag+"hMinSignalUpset", "Minimim Signal in An Upset Chip", -100.0, 50.0, 50);
      book1D(sTell1Tag+"hNumHighChanUpset", "N Chanel (Signal>250) in An Upset Chip", -0.5, 29.5, 30);

      book1D(sTell1Tag+"hAllAdcMeanUpset",  "ADC Mean of All Channels (Signal<20) in An Upset Chip", -100.0, 50.0, 30);
      book1D(sTell1Tag+"hAllAdcRmsUpset",  "ADC Rms of All Channels (Signal<20) in An Upset Chip", 0.0, 150.0, 50);
      book1D(sTell1Tag+"hUpsetAdcMeanUpset",  "ADC Mean of Upset Channels in An Upset Chip", -100.0, 50.0, 30);
      book1D(sTell1Tag+"hUpsetAdcRmsUpset",  "ADC Rms of Upset Channels in An Upset Chip", 0.0, 50.0, 50);

      book2D(sTell1Tag+"hNupsetVsNhighUpset",  "Number of Upset Channels vs Number of High in An Upset Chip",
             0.5, 24.5, 24, -0.5, 128.5, 129);
      book2D(sTell1Tag+"hAdcRmsVsNhighUpset",  "ADC Rms vs Number of High in An Upset Chip",
             0.5, 24.5, 24, 0.0, 150, 50);

      // High signal
      book1D(sTell1Tag+"hNumUpsetChanHigh", "Number of Upset Channels in A High Chip",  -0.5, 128.5, 129);
      book1D(sTell1Tag+"hAllSignalHigh", "All Signal in A High Chip", -500.0, 500.0, 200);
      book1D(sTell1Tag+"hMaxSignalHigh", "Maximum Signal in A High Chip", -100.0, 500.0, 120);
      book1D(sTell1Tag+"hMinSignalHigh", "Minimim Signal in A High Chip", -100.0, 50.0, 50);
      book1D(sTell1Tag+"hNumHighChanHigh", "N Chanel (Signal>250) in A High Chip", -0.5, 29.5, 30);

      book1D(sTell1Tag+"hAllAdcMeanHigh",  "ADC Mean of All Channels (Signal<20) in A High Chip", -100.0, 50.0, 30);
      book1D(sTell1Tag+"hAllAdcRmsHigh",  "ADC Rms of All Channels (Signal<20) in A High Chip", 0.0, 150.0, 50);
      book1D(sTell1Tag+"hUpsetAdcMeanHigh",  "ADC Mean of Upset Channels in A High Chip", -100.0, 50.0, 30);
      book1D(sTell1Tag+"hUpsetAdcRmsHigh",  "ADC Rms of Upset Channels in A High Chip", 0.0, 50.0, 50);

      book2D(sTell1Tag+"hNupsetVsNhighHigh",  "Number of Upset Channels vs Number of High in A High Chip",
             0.5, 24.5, 24, -0.5, 128.5, 129);
      book2D(sTell1Tag+"hAdcRmsVsNhighHigh",  "ADC Rms vs Number of High in A High Chip",
             0.5, 24.5, 24, 0.0, 150, 50);

      book2D(sTell1Tag+"hAdcValueHigh",  "Raw ADC Value in A High Chip",
             -0.5, 2047.5, 2048, 400.5, 900.5, 500);
      book2D(sTell1Tag+"hPedSubAdcValueHigh",  "Pedestal Subtracted ADC Value in A High Chip",
             -0.5, 2047.5, 2048, -100.0, 300.0, 800);
    }
  }

  
  for ( tell1 = 0; tell1 < m_numOfTell1s; tell1++ ) {
    for ( chip=0; chip<16; chip++ ) {

      int nUpsetLinkChip = 0;
      int nUpsetChanChip = 0;
      int nAllChanChip = 0;
      int nHighSignalChip = 0;

      double adcMeanChipA = 0;
      double adcRmsChipA = 0;
      double adcMeanChipU = 0;
      double adcRmsChipU = 0;
      double maxSignal = -9999.0;
      double minSignal = 9999.0;
      
      int nUpsetChanLink[4]  = {0, 0, 0, 0};
      int nAllChanLink[4]    = {0, 0, 0, 0};
      int nHighSignalLink[4] = {0, 0, 0, 0};
      double adcMeanLinkA[4] = {0.0, 0.0, 0.0, 0.0};
      double adcRmsLinkA[4]  = {0.0, 0.0, 0.0, 0.0};
      double adcMeanLinkU[4] = {0.0, 0.0, 0.0, 0.0};
      double adcRmsLinkU[4]  = {0.0, 0.0, 0.0, 0.0};

      // Calculations
      for ( i=0; i<4; i++ ) {
        link = chip*4+i;
        for (channel=0; channel<32; channel++) {
          channelAll = link*32 + channel;
          if ( m_pedestal[tell1][link][channel] < 600 &&
               m_pedestal[tell1][link][channel] > 450 ) {
            if ( m_pedSubSignal[tell1][link][channel] < -20 ) {
              nUpsetChanLink[i]++;
              adcMeanLinkU[i] += m_pedSubSignal[tell1][link][channel];
              adcRmsLinkU[i]  += m_pedSubSignal[tell1][link][channel]*m_pedSubSignal[tell1][link][channel];
            }
            //            if ( m_pedSubSignal[tell1][link][channel] > 250 ) {
            if ( m_pedSubSignal[tell1][link][channel] > 100 ) {
              nHighSignalLink[i]++;
            }
            if ( m_pedSubSignal[tell1][link][channel] < 20 ) {
              nAllChanLink[i]++;
              adcMeanLinkA[i] += m_pedSubSignal[tell1][link][channel];
              adcRmsLinkA[i]  += m_pedSubSignal[tell1][link][channel]*m_pedSubSignal[tell1][link][channel];
            }
            if ( m_pedSubSignal[tell1][link][channel] > maxSignal )
              maxSignal = m_pedSubSignal[tell1][link][channel];
            if ( m_pedSubSignal[tell1][link][channel] < minSignal )
              minSignal = m_pedSubSignal[tell1][link][channel];
          }
        }
        nUpsetChanChip += nUpsetChanLink[i];
        nAllChanChip   += nAllChanLink[i];
        nHighSignalChip+= nHighSignalLink[i];
        adcMeanChipA   += adcMeanLinkA[i];
        adcRmsChipA    += adcRmsLinkA[i];
        adcMeanChipU   += adcMeanLinkU[i];
        adcRmsChipU    += adcRmsLinkU[i];
        if ( nAllChanLink[i] > 0 ) {
          adcMeanLinkA[i] = adcMeanLinkA[i]/nAllChanLink[i];
          adcRmsLinkA[i]  = sqrt(adcRmsLinkA[i]/nAllChanLink[i] - adcMeanLinkA[i]*adcMeanLinkA[i]);
          adcRmsLinkA[i]  = sqrt(adcRmsLinkA[i]/nAllChanLink[i]);
        }
        if ( nUpsetChanLink[i] > 0 ) {
          adcMeanLinkU[i] = adcMeanLinkU[i]/nUpsetChanLink[i];
          adcRmsLinkU[i]  = sqrt(adcRmsLinkU[i]/nUpsetChanLink[i] - adcMeanLinkU[i]*adcMeanLinkU[i]);
        }
        if ( nUpsetChanLink[i] > 16 ) {
          nUpsetLinkChip ++;
        }
      }
      if ( nAllChanChip > 0 ) {
        adcMeanChipA = adcMeanChipA / nAllChanChip;
        //        adcRmsChipA  = sqrt(adcRmsChipA/nAllChanChip - adcMeanChipA*adcMeanChipA);
        adcRmsChipA  = sqrt(adcRmsChipA/nAllChanChip);
      }
      if ( nUpsetChanChip > 0 ) {
        adcMeanChipU = adcMeanChipU / nUpsetChanChip;
        adcRmsChipU  = sqrt(adcRmsChipU/nUpsetChanChip - adcMeanChipU*adcMeanChipU);
      }
      

      // Print out
      if ( nUpsetChanChip > 64) {
        cout << "=c=Event " << m_evtNumber
             << "   Tell1 " << m_tell1List[tell1]
             << "   Chip " << chip
             << "   Nlink " << nUpsetLinkChip
             << "   NchanU " << nUpsetChanChip
             << "   MeanU " << adcMeanChipU
             << "   RmsU " << adcRmsChipU
             << "   MaxS " << maxSignal
             << "   NchanA " << nAllChanChip
             << "   MeanA " << adcMeanChipA
             << "   RmsA " << adcRmsChipA
             << "   Nhigh " << nHighSignalChip
             << "   MaxS " << maxSignal
             << "   MinS " << minSignal
             << endl;
      }
      
      for ( i=0; i<4; i++ ) {
        link = chip*4+i;
        if ( nUpsetChanLink[i] > 16 ) {
          cout << "=l=Event " << m_evtNumber
               << "   Tell1 " << m_tell1List[tell1]
               << "   Link " << link
               << "   NchanU " << nUpsetChanLink[i]
               << "   MeanU " << adcMeanLinkU[i]
               << "   RmsU " << adcRmsLinkU[i]
               << "   NchanA " << nAllChanLink[i]
               << "   MeanA " << adcMeanLinkA[i]
               << "   RmsA " << adcRmsLinkA[i]
               << "   Nhigh " << nHighSignalLink[i]
               << endl;
        }
        if ( nUpsetChanLink[i] <= 16 && nUpsetChanChip > 64) {
          cout << "-l-Event " << m_evtNumber
               << "   Tell1 " << m_tell1List[tell1]
               << "   Link " << link
               << "   NchanU " << nUpsetChanLink[i]
               << "   MeanU " << adcMeanLinkU[i]
               << "   RmsU " << adcRmsLinkU[i]
               << "   NchanA " << nAllChanLink[i]
               << "   MeanA " << adcMeanLinkA[i]
               << "   RmsA " << adcRmsLinkA[i]
               << "   Nhigh " << nHighSignalLink[i]
               << endl;
        }
      }

      if ( nUpsetChanChip > 64) {
        for ( i=0; i<4; i++ ) {
          link = chip*4+i;
          for (channel=0; channel<32; channel++) {
            channelAll = link*32 + channel;
            if ( m_pedestal[tell1][link][channel] < 600 &&
                 m_pedestal[tell1][link][channel] > 450 &&
                 m_pedSubSignal[tell1][link][channel] >= -20 ) {
              cout << "-1-Event " << m_evtNumber
                   << "   Tell1 " << m_tell1List[tell1]
                   << "   Chip " << chip
                   << "   Link " << link
                   << "   chan_L " << channel
                   << "   adc " << m_adcValue[tell1][link][channel]
                   << "   adc-ped " << m_pedSubSignal[tell1][link][channel]
                   << endl;
            }
            if ( m_pedestal[tell1][link][channel] >= 600 ||
                 m_pedestal[tell1][link][channel] <= 450 ) {
              cout << "-2-Event " << m_evtNumber
                   << "   Tell1 " << m_tell1List[tell1]
                   << "   Chip " << chip
                   << "   Link " << link
                   << "   chan_L " << channel
                   << "   adc " << m_adcValue[tell1][link][channel]
                   << "   ped " << m_pedestal[tell1][link][channel]
                   << endl;
            }
          }
        }
      }
        

      // Histogram
      // all
      sprintf(tell1Tag, "TELL1_%d/", m_tell1List[tell1]);
      string sTell1Tag = tell1Tag;

      plot1D(nUpsetLinkChip, sTell1Tag+"hNumUpsetLink",  "Number of Upset Links Per Chip",  -0.5, 4.5, 5);
      plot1D(nUpsetChanChip, sTell1Tag+"hNumUpsetChan", "Number of Upset Channels in A Chip", -0.5, 128.5, 129);
      plot1D(maxSignal, sTell1Tag+"hMaxSignal", "Maximum Signal in A Chip", -100.0, 500.0, 120);
      plot1D(minSignal, sTell1Tag+"hMinSignal", "Minimim Signal in A Chip", -100.0, 50.0, 50);
      plot1D(nHighSignalChip, sTell1Tag+"hNumHighChan", "N Chanel (Signal>250) in A Chip", -0.5, 29.5, 30);

      plot1D(adcMeanChipU, sTell1Tag+"hUpsetAdcMean", "ADC Mean of Upset Channels in A Chip", -100.0, 50.0, 30);
      plot1D(adcRmsChipU, sTell1Tag+"hUpsetAdcRms",  "ADC Rms of Upset Channels in A Chip", 0.0, 150.0, 50);
      plot1D(adcMeanChipA, sTell1Tag+"hAllAdcMean", "ADC Mean of All Channels (Signal<20) in A Chip", -100.0, 50.0, 30);
      plot1D(adcRmsChipA, sTell1Tag+"hAllAdcRms", "ADC Rms of All Channels (Signal<20) in A Chip", 0.0, 50.0, 50);

      plot2D(nHighSignalChip, nUpsetChanChip, sTell1Tag+"hNupsetVsNhigh",
             "Number of Upset Channels vs Number of High in A Chip",
             0.5, 20.5, -0.5, 128.5, 20, 129);
      plot2D(nHighSignalChip, adcRmsChipA, sTell1Tag+"hAdcRmsVsNhigh",
             "ADC Rms vs Number of High in A Chip",
             0.5, 20.5, 0.0, 150, 20, 50);

      // Upset Chip
      if ( nUpsetChanChip > 64 ) {
        plot1D(nUpsetChanChip, sTell1Tag+"hNumUpsetChanUpset", "Number of Upset Channels in An Upset Chip", -0.5, 128.5, 129);
        plot1D(maxSignal, sTell1Tag+"hMaxSignalUpset", "Maximum Signal in An Upset Chip", -100.0, 500.0, 120);
        plot1D(minSignal, sTell1Tag+"hMinSignalUpset", "Minimim Signal in An Upset Chip", -100.0, 50.0, 50);
        plot1D(nHighSignalChip, sTell1Tag+"hNumHighChanUpset", "N Chanel (Signal>250) in An Upset Chip", -0.5, 29.5, 30);

        plot1D(adcMeanChipU, sTell1Tag+"hUpsetAdcMeanUpset", "ADC Mean of Upset Channels in An Upset Chip", -100.0, 50.0, 30);
        plot1D(adcRmsChipU, sTell1Tag+"hUpsetAdcRmsUpset",  "ADC Rms of Upset Channels in An Upset Chip", 0.0, 50.0, 50);
        plot1D(adcMeanChipA, sTell1Tag+"hAllAdcMeanUpset",
               "ADC Mean of All Channels (Signal<20) in An Upset Chip", -100.0, 50.0, 30);
        plot1D(adcRmsChipA, sTell1Tag+"hAllAdcRmsUpset", "ADC Rms of All Channels (Signal<20) in An Upset Chip", 0.0, 150.0, 50);

        plot1D(chip, sTell1Tag+"hUpsetChipId", "Upset Chip ID", -0.5, 15.5, 16);

        plot2D(nHighSignalChip, nUpsetChanChip, sTell1Tag+"hNupsetVsNhighUpset",
               "Number of Upset Channels vs Number of High in An UpsetChip",
               0.5, 20.5, -0.5, 128.5, 20, 129);
        plot2D(nHighSignalChip, adcRmsChipA, sTell1Tag+"hAdcRmsVsNhighUpset",
               "ADC Rms vs Number of High in An Upset Chip",
               0.5, 20.5, 0.0, 150, 20, 50);
      }

      // High signal
      if ( nHighSignalChip > 0 ) {
        plot1D(nUpsetChanChip, sTell1Tag+"hNumUpsetChanHigh", "Number of Upset Channels in A High Chip", -0.5, 128.5, 129);
        plot1D(maxSignal, sTell1Tag+"hMaxSignalHigh", "Maximum Signal in A High Chip", -100.0, 500.0, 120);
        plot1D(minSignal, sTell1Tag+"hMinSignalHigh", "Minimim Signal in A High Chip", -100.0, 50.0, 50);
        plot1D(nHighSignalChip, sTell1Tag+"hNumHighChanHigh", "N Chanel (Signal>250) in A High Chip", -0.5, 29.5, 30);

        plot1D(adcMeanChipU, sTell1Tag+"hUpsetAdcMeanHigh", "ADC Mean of Upset Channels in A High Chip", -100.0, 50.0, 30);
        plot1D(adcRmsChipU, sTell1Tag+"hUpsetAdcRmsHigh",  "ADC Rms of Upset Channels in A High Chip", 0.0, 50.0, 50);
        plot1D(adcMeanChipA, sTell1Tag+"hAllAdcMeanHigh", 
               "ADC Mean of All Channels (Signal<20) in A High Chip", -100.0, 50.0, 30);
        plot1D(adcRmsChipA, sTell1Tag+"hAllAdcRmsHigh",
               "ADC Rms of All Channels (Signal<20) in A High Chip", 0.0, 150.0, 50);

        plot2D(nHighSignalChip, nUpsetChanChip, sTell1Tag+"hNupsetVsNhighHigh",
               "Number of Upset Channels vs Number of High in A High Chip",
               0.5, 20.5, -0.5, 128.5, 20, 129);
        plot2D(nHighSignalChip, adcRmsChipA, sTell1Tag+"hAdcRmsVsNhighHigh",  "ADC Rms vs Number of High in A High Chip",
               0.5, 20.5, 0.0, 150, 20, 50);
      }


      for ( i=0; i<4; i++ ) {
        link = chip*4+i;

        if ( nUpsetChanLink[i] > 16 ) {
          plot1D(link, sTell1Tag+"hUpsetLinkId",  "Upset Link ID", -0.5, 63.5, 64);
        }
        for (channel=0; channel<32; channel++) {
          channelAll = link*32 + channel;
          if ( m_pedestal[tell1][link][channel] < 600 &&
               m_pedestal[tell1][link][channel] > 450 ) {
            
            if ( m_pedSubSignal[tell1][link][channel] > 20 &&
                 m_pedSubSignal[tell1][link][channel] < 80 ) {
              plot1D(link, sTell1Tag+"hPosSignalVsLink",  "Signal (20 - 80) vs Link ID", -0.5, 63.5, 64);
            }
            
            if ( m_pedSubSignal[tell1][link][channel] < -20 ) {
              plot1D(link, sTell1Tag+"hUpsetChanVsLink",  "Upset Channel vs Link ID", -0.5, 63.5, 64);
            }
            
            plot1D(m_pedSubSignal[tell1][link][channel], sTell1Tag+"hAllSignal",
                   "All Signal", -200.0, 500.0, 140);
            if ( nUpsetChanChip > 64 ) {
              plot1D(m_pedSubSignal[tell1][link][channel], sTell1Tag+"hAllSignalUpset",
                     "All Signal in An Upset Chip", -200.0, 500.0, 140);
            }
            if ( nHighSignalChip > 0 ) {
              plot1D(m_pedSubSignal[tell1][link][channel], sTell1Tag+"hAllSignalHigh",
                     "All Signal in A High Chip", -200.0, 500.0, 140);
              plot2D(channelAll, m_adcValue[tell1][link][channel],
                     sTell1Tag+"hAdcValueHigh", "Raw ADC Value in A High Chip",
                     -0.5, 2047.5, 400.5, 900.5, 2048, 500);
              plot2D(channelAll, m_pedSubSignal[tell1][link][channel],
                     sTell1Tag+"hPedSubAdcValueHigh",  "Pedestal Subtracted ADC Value in A High Chip",
                     -0.5, 2047.5, -100.0, 300.0, 2048, 800);
            }
          }
        }
      }
    }
    
  }

return StatusCode::SUCCESS;
}
  

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloPedShiftStudy::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

//=============================================================================
