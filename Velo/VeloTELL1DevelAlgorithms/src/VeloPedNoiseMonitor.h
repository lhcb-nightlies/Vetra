// $Id: VeloPedNoiseMonitor.h,v 1.1.1.1 2007-12-07 14:58:25 szumlat Exp $
#ifndef VELOPEDESTALNOISE_H 
#define VELOPEDESTALNOISE_H 1

// Include files from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// from Event model
#include "VeloEvent/EvtInfo.h"
#include "VeloEvent/VeloTELL1Data.h"

#include "VeloEvent/VeloProcessInfo.h"

#include "VetraKernel/IVeloBadChannels.h"
#include "VetraKernel/IVeloTell1List.h"

#include "Tell1Kernel/VeloTell1Core.h"

class DeVelo;
class DeVeloSensor;

//namespaces
using namespace LHCb;
using namespace VeloTELL1;

/** @class VeloPedNoiseMonitor VeloPedNoiseMonitor.h
 *  
 *
 *  @author Jianchun Wang
 *  @date   2006-12-16
 */
class VeloPedNoiseMonitor : public GaudiTupleAlg {
public: 
  typedef std::vector< bool > bdataVec;
  typedef std::vector< bdataVec > bdataVecVec;
  typedef std::vector< double >      ddataVec;
  typedef std::vector< ddataVec >    ddataVecVec;
  typedef std::vector< ddataVecVec > ddataVecVecVec;

  /// Standard constructor
  VeloPedNoiseMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloPedNoiseMonitor( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;
protected:
  virtual StatusCode getTesData();
  virtual StatusCode getCleanChannels();
  virtual StatusCode generateHistogram();

private:
  int m_evtNumber;
  const DeVelo* m_velo;
  std::string m_rawADCsLoc;    // Raw ADC
  std::string m_subADCsLoc;    // pedestal subtracted ADC
  std::string m_cmsADCsLoc;    // ped subtracted and CM suppressed ADC
  std::string m_subPedsLoc;    // pedstal

  std::string m_badChannelsTool;
  std::string m_veloTell1ListTool;

  bool m_skipBadChannel;
  bdataVecVec m_badChannelList;

  int m_signalCutForCleanNoise;
  bdataVecVec m_cleanChannel;

  ddataVecVecVec  m_adcValue;
  ddataVecVecVec  m_pedestal;
  ddataVecVecVec  m_pedSubSignal;
  ddataVecVecVec  m_cmSupSignal;

  int m_numOfTell1s;
  sdataVec m_tell1List;
  sdataVec m_tell1Index;

  bool m_rawADCsFound;
  bool m_subADCsFound;
  bool m_cmsADCsFound;
  bool m_subPedsFound;

};
#endif // VELOPEDESTALNOISE_H
