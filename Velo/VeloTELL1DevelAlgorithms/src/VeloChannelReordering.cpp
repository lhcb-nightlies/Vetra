// $Id: VeloChannelReordering.cpp,v 1.2 2010-03-22 14:54:14 erodrigu Exp $
// Include files

#include <stdio.h>
#include <math.h>

#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h"

// local
#include "VeloChannelReordering.h"

using namespace std;


//-----------------------------------------------------------------------------
// Implementation file for class : VeloChannelReordering
//
// 2006-12-16 : Jianchun Wang
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloChannelReordering )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloChannelReordering::VeloChannelReordering( const std::string& name,
                                                ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator )
    ,  m_inputDataLoc ( VeloTELL1DataLocation::CMSuppressedADCs )
    ,  m_outputDataLoc ( VeloTELL1DataLocation::ReorderedADCs )
{
  declareProperty( "InputDataLocation",  m_inputDataLoc);
  declareProperty( "OutputDataLocation", m_outputDataLoc);
}
//=============================================================================
// Destructor
//=============================================================================
VeloChannelReordering::~VeloChannelReordering() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloChannelReordering::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiTupleAlg

  debug() << "==> Initialize" << endmsg;
  info() <<  "InputDataLocation   "  << m_inputDataLoc << endmsg;
  info() <<  "OutputDataLocation   " << m_outputDataLoc << endmsg;

  m_velo = getDet<DeVelo>(DeVeloLocation::Default);

  return StatusCode::SUCCESS;
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloChannelReordering::execute() {

  debug() << "==> Execute" << endmsg;

  // Get data from TES
  if ( ! exist<VeloTELL1Datas>( m_inputDataLoc ) ){
    info()<< " ==> There is no decoded ADCs at: "
          << m_inputDataLoc << endmsg;
    return ( StatusCode::FAILURE );
  }
  VeloTELL1Datas* m_inputData = get<VeloTELL1Datas>( m_inputDataLoc );
  VeloTELL1Datas* m_outputData = new VeloTELL1Datas();


  int tell1Id;  
  int link;
  int channel;
  int electronicId;
  int stripId;

  VeloTELL1Datas::const_iterator sensIt;
  for (sensIt=m_inputData->begin(); sensIt!=m_inputData->end(); ++sensIt){

    sdataVec m_signal = sdataVec(2048, 0);
    VeloTELL1Data* oldData = (*sensIt);
    tell1Id = oldData->key();
    const DeVeloSensor* sensor = m_velo->sensorByTell1Id(tell1Id);

    for (link=0; link<64; link++){
      VeloTELL1::ALinkPair iTPair=(*oldData)[link];
      VeloTELL1::scdatIt iT=iTPair.first;
      for (channel=0; channel<32; channel++) {
        electronicId = link*32 + channel;
        stripId = sensor->ChipChannelToStrip(electronicId);
        m_signal[stripId] = *(iT+channel);
      }
    }

    VeloTELL1Data* newData = new VeloTELL1Data(tell1Id, VeloFull);
    sdataVec newDataLink;
    for (link=0; link<64; ++link) {
      for (channel=0; channel<32; channel++) {
        stripId = link*32 + channel;
        newDataLink.push_back( m_signal[stripId] );
      }
    }
    newData->setDecodedData(newDataLink);
    m_outputData->insert(newData);
  }
  put(m_outputData, m_outputDataLoc);
  

  return StatusCode::SUCCESS;
}




//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloChannelReordering::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}

//=============================================================================
