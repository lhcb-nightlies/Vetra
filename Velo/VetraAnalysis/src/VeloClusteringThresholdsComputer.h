// $Id: VeloClusteringThresholdsComputer.h,v 1.2 2009-08-05 14:40:02 szumlat Exp $
#ifndef VELOCLUSTERINGTHRESHOLDSCOMPUTER_H 
#define VELOCLUSTERINGTHRESHOLDSCOMPUTER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "VeloEvent/VeloTELL1Data.h"

// std includes
#include <vector>
#include <map>

// ROOT
#include "TH2.h"
#include "TH1.h"

namespace AIDA{
  class IHistogram1D;
}

class DeVelo;

/** @class VeloClusteringThresholdsComputer VeloClusteringThresholdsComputer.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2008-08-01
 */

class VeloClusteringThresholdsComputer : public GaudiTupleAlg{
public: 

  typedef std::map<int, AIDA::IHistogram1D*> MAP_HIST_1D;
  typedef MAP_HIST_1D::iterator MH1D_IT;

  enum numbers{
    SENSOR_STRIPS=2048
  };

  /// Standard constructor
  VeloClusteringThresholdsComputer( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloClusteringThresholdsComputer( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getData();
  StatusCode noiseCollector();

private:

  int m_evtNumber;
  int m_convergenceLimit;
  float m_adcCut;
  bool m_isDebug;
  std::string m_cmsDataLoc;
  LHCb::VeloTELL1Datas* m_cmsData;     /// container for ADCs
  //std::map<int, TH1F*> m_thresholds;
  MAP_HIST_1D m_thresholds;
  std::map<int, std::vector<float> > m_noiseRMS;
  std::map<int, std::vector<int> > m_noiseRMSCnts;
  DeVelo* m_velo;  

};
#endif // VELOCLUSTERINGTHRESHOLDSCOMPUTER_H
