
// $Id: VeloClusterCompare.h,v 1.5 2009-11-04 12:06:08 szumlat Exp $
#ifndef VELOCLUSTERCOMPARE_H 
#define VELOCLUSTERCOMPARE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/SystemOfUnits.h"

// velo
#include "Event/VeloCluster.h"
#include "Event/VeloLiteCluster.h"

// stl
#include <map>

namespace AIDA {
  class IHistogram2D;
  class IHistogram1D;
}

/** @class VeloClusterCompare VeloClusterCompare.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2007-01-25
 */

class DeVelo;
class DeVeloRType;
class DeVeloPhiType;

class VeloClusterCompare : public GaudiTupleAlg {
public: 
  /// Standard constructor
  VeloClusterCompare( const std::string& name, ISvcLocator* pSvcLocator );

  enum processLabels{
    TELL1=0,
    EMU=1
  };

  enum actions{
    _DEFAULT_=0,
    _USE_=1,
    _EXCLUDE_=2
  };

  enum stat{
    _BP_=0,
    _TELL1_ONLY=1,
    _EMU_ONLY_=2,
    _NOT_BP_POS_R_=3,
    _NOT_BP_POS_PHI_=4,
    _NOT_BP_ADC_=5,
    _NOT_BP_SIZE_=6,
    _BP_POS_=7
  };

  typedef std::pair<double, double> dPair;
  // bit perfectness counters
  // [0] -> bit perfect clusters
  // [1] -> isTell1 only
  // [2] -> isEmu only
  // [3] -> notBPPos
  // [4] -> notBPADC
  // [5] -> notBPSize
  typedef std::vector<float> bpCntVec;
  typedef std::pair<AIDA::IHistogram1D*, AIDA::IHistogram1D*> H_1D;
  typedef std::pair<AIDA::IHistogram2D*, AIDA::IHistogram2D*> H_2D;  

  virtual ~VeloClusterCompare( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getData();
  void sortClusters();
  void clustersMonitor(LHCb::VeloClusters* inCont, const int procType);
  StatusCode emulatorPerformance();
  void copyClusters2Local();
  void resetMemory();
  StatusCode smartClusterFilter();
  void bookFilterHistos();
  
private:

  LHCb::VeloClusters* m_tell1Clusters;
  LHCb::VeloClusters* m_emuClusters;
  std::string m_tell1ClustersLoc;
  std::string m_emuClustersLoc;
  long m_evtNumber;
  DeVelo* m_veloDet;
  dPair m_nRawClusters;
  dPair m_nRawClusters2;
  dPair m_nOneStr;
  dPair m_nTwoStr;
  dPair m_nThreeStr;
  dPair m_nFourStr;
  std::vector<unsigned int> m_excludedSensorList;
  std::vector<unsigned int> m_usedSensorList;
  bool m_isDebug;
  std::map< int, AIDA::IHistogram2D* > m_mcmsDataHistos;
  LHCb::VeloClusters* m_localTell1;
  LHCb::VeloClusters* m_localEmu;
  LHCb::VeloClusters* m_bpClustersTell1_1;
  LHCb::VeloClusters* m_bpClustersEmu_1;
  LHCb::VeloClusters* m_bpClustersTell1_2;
  LHCb::VeloClusters* m_bpClustersEmu_2;
  LHCb::VeloClusters* m_tell1Only;
  LHCb::VeloClusters* m_emuOnly;
  bpCntVec m_bpStatistics;
  std::vector<LHCb::VeloChannelID> m_matchedPosChannels;
  std::vector<LHCb::VeloChannelID> m_matchedChargeChannels;
  std::vector<int> m_positionWindow;
  dPair m_unmatchedTell1;
  dPair m_unmatchedEmu;
  H_1D m_allMatched;
  H_1D m_allUnMatched;
  H_1D m_matchedEmuADC;
  H_1D m_matchedTell1ADC;
  H_1D m_unmatchedEmuADC;
  H_1D m_unmatchedTell1ADC;
  H_2D m_matchedEmuPos;
  H_2D m_matchedTell1Pos;
  H_2D m_unmatchedEmuPos;
  H_2D m_unmatchedTell1Pos;  
  H_2D m_unmatchedTell1Pos1;
  H_2D m_unmatchedTell1Pos2;
  H_2D m_unmatchedTell1Pos3;
  H_2D m_unmatchedTell1Pos4;
  H_2D m_unmatchedEmuPos1;
  H_2D m_unmatchedEmuPos2;
  H_2D m_unmatchedEmuPos3;
  H_2D m_unmatchedEmuPos4;
  H_2D m_adcDiffVsCharge;
  H_2D m_adcDiffVsPos;
  unsigned int m_spilloverBitThreshold;
  unsigned int m_actionType;
  std::pair<const DeVeloRType*, const DeVeloPhiType*> m_refSensors;

};
#endif // VELOCLUSTERCOMPARE_H
