// $id: VeloXTalkComputer.h,v 1.1.1.1 2006/07/13 17:37:16 jborel Exp $
#ifndef VELOXTALKCOMPUTER_H 
#define VELOXTALKCOMPUTER_H 1

// local
class DeVelo;
class DeVeloSensor;
class VeloXTalkComputer;


// #include <sstream>
/* #include "GaudiAlg/Tuple.h" */
/* #include "GaudiAlg/TupleObj.h" */

//#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "VeloEvent/VeloTELL1Data.h"
#include "VetraKernel/VeloTELL1Algorithm.h"


/*! \file VeloXTalkComputer something */
// ===============================================================
// ===============================================================

/*!  @author Jeremie Borel
 *  @date   2007-07-06
 * 
 * \brief Computes the x-talk in the Velo.
 *
 * The default config computes x-talk in the cable in the 4 neighboring chip
 * channels, that is from N-2 to N+2. It can be run on test-pulse or on
 * beam data. It does not inverse the system to get the FIR
 * coefficients, this is done by VeloTELL1CableFIRFilter ( which can
 * take the output of this algo ).
 * 
 * It is necessary to run on at least pedestal subtracted data or ped
 * and CM data. Actually, their is no problem running on other data
 * but then, you must well know what the algo is doing to understand
 * its results.
 *
 * All the properties are commented in the .cpp file, the most important are:
 * - InputDataLoc : where to take your data from.
 * - CableXTalk=n : compute the x-talk in the analog cables up to chip channel neighbor +-n. 
 *    n=0 disable this x-talk (default is 2).
 * - Tell1XTalk=n : compute the x-talk in the Tell1s up to chip channel neighbor +-n. 
 *    n=0 disable this x-talk (default is 0).
 * - StripXTalk=n : compute the x-talk every strip up to CHIP CHANNEL neighbor +-n. 
 *    n=0 disable this x-talk (default is 0).
 * - SensorXTalk=true/false: compute phi-sensor x-talk,
 *                       e.g non-consecutive chip channel but consecutive strips, etc...
 * - NumberOfSkippedEvents : should be set to the convergenceLimit value of the 
 *                         other TELL1 pedestal and CM mode algorithms.
 */
class VeloXTalkComputer : public /*VeloTELL1Algorithm*/ GaudiHistoAlg {

  /*! \class pedestalFollower
     \brief Small class to compute the pedestals when needed.

     It basically computes the mean value. If it is above some
     threshold (3 rms), the value has a weight of 0.1.
  */
  class pedestalFollower {
  public:
    pedestalFollower ();
    
    void fill( const double X );
    
    double n() const { return m_NN; };
    double mean() const { return m_sum/static_cast<double>(m_NN); };
    double rms() const;
    double variance() const ;
    double sum() const { return m_sum; };
    double sum2() const { return m_sum2; };
      
    void reset();
  private:
    double m_sum, m_sum2;
    unsigned int m_NN;
  };


  /*! \class stat2D 
    \brief 2D stat.
    
  Use the fill method (like for an histo)
  and ask, whenever you want for some stat on the dataset you
  filled. The data are not stored.
  */
  class stat2D {
  public:
    stat2D();
    
    void fill( const double X, const double Y );
    
    double n() const { return m_NN; };
    double xMean() const { return m_xSum/static_cast<double>(m_NN); };
    double yMean() const { return m_ySum/static_cast<double>(m_NN); };
    double xRms() const { return std::sqrt(sxx()); };
    double yRms() const { return std::sqrt(syy()); };
    double correlation() const;
    double slope() const;
    double intercept() const;

    double sigmaSlope() const;
    double sigmaIntercept() const;

    //! Actually, this is the function that is used to compute the x-talk.
    double meanRatio() const { return yMean()/xMean(); };

    //! Computes sum y_i/x_i
    double ratio() const { return m_ratio/n(); };

    //! Evaluates the function x*slope() + intercept()
    double eval( const double x ) const;

    void reset();
  protected:
    double sxx() const;
    double syy() const;
    double sxy() const;
    double s() const;

  private:
    double m_xSum, m_xSum2;
    double m_ySum, m_ySum2, m_xySum;
    unsigned int m_NN;
    double m_ratio;

  };
  friend std::ostream& operator<<( std::ostream&, const stat2D& );

  /*! small wrapper to print the x-talk value in an uniform way */
  std::string xTalkValue( const stat2D& );
  
  /*! \class QuickChannelMap
    \brief Sensors strip and channel mapping. 

  As the sensor strip to chip channel mapping is a map in
  DeVeloSensor, it is re-implemented here with a std::vector
  mapping. */
  class QuickChannelMap {
  public:
    QuickChannelMap( const DeVeloSensor* velo = 0 );
    int swtToStrip( const int ) const;
    int stripToSwt( const int ) const;

    const DeVeloSensor* getSensor() const { return m_sensor; };

    int getCableNeighbor( const int cc, const int offset ) const;
    int getStripNeighbor( const int cc, const int stripOffset ) const;
    int getOuterInnerNeighbor( const int cc ) const;

    bool isInner( const int cc ) const;
    bool isOuter( const int cc ) const;

    void init( const DeVeloSensor* velo );
    void createMap();
    bool exists() const { return m_isInit; };
  private:

    std::vector<int> swt2strip, strip2swt;
    const DeVeloSensor* m_sensor;
    bool m_isPhi, m_isInit;
  };

  // ===============================================================
  // ===============================================================
  // ===============================================================
  // ===============================================================
  /*! @name Test
   *  All these classes are small boolean test invoked to create a
   *  specific x-talk
   */
  //@{

  /*! \class Test 
    \brief Base interface for boolean test.

  Base class to test a pair of strip for the xtalk computation. All
  these public derivation of Test are used inside the code to create
  object that will compute a specific x-talk, see the xTester class
  for an example. */
  struct Test {
    Test(){};
    virtual bool operator()( const QuickChannelMap* 
			     , const int
			     , const int ) const = 0;
    // create a new and leaves the ownership to the caller.
    virtual Test* clone() const = 0;
    virtual ~Test(){};
    virtual std::string name() const = 0;
    //! this is not very clever, but it works for what I need...
    bool operator==( const Test& r ) const { return name() == r.name(); };
  };

  // --------------------------------------------------
  /*! \class Empty
    \brief A very trivial example of a concrete Test.
  */
  struct Empty : public Test {
    struct NotImplemented{};
    Empty()  {};
    Empty* clone() const override { return 0; };
    bool operator()( const QuickChannelMap* 
		     , const int 
		     , const int ) const override { 
      return false;
    }
    std::string name() const override { throw NotImplemented(); return "NULL"; }
  };
  // --------------------------------------------------
  /*! \class True
    \brief A very trivial example of a concrete Test.
  */
  struct True : public Test {
    struct NotImplemented{};
    True()  {};
    True* clone() const override { return (new True()); };
    bool operator()( const QuickChannelMap* 
		     , const int 
		     , const int ) const override { 
      return true;
    }
    std::string name() const override { return "True"; }
  };

  // --------------------------------------------------
  /*! \class ConstCCOffset 
     \brief Ensure that the offset between the
     signal and the noise is N in term of chip channel numbering.
  */
  struct ConstCCOffset : public Test {
    ConstCCOffset( const int N ) : m_N(N) {};
    ConstCCOffset* clone() const override {
      return ( new ConstCCOffset( m_N ) ); 
    }
    bool operator()( const QuickChannelMap* 
		     , const int 
		     , const int offset ) const override { 
      return m_N == offset;
    }
    std::string name() const override {
      std::stringstream s;
      s << "ccN" << std::showpos << m_N;
      return s.str(); 
    }
  private:
    int m_N;
  };

  // --------------------------------------------------
  /*! \class ConstStripOffset
     \brief Ensure that the offset between the
     signal and the noise is N in term of strip numbering.
  */
  struct ConstStripOffset : public Test {
    ConstStripOffset( const int N ) : m_N(N) {};
    ConstStripOffset* clone() const override { return ( new ConstStripOffset( m_N ) ); };
    bool operator()( const QuickChannelMap* sen
		     , const int chipChan
		     , const int offset ) const override { 
      const int s1 = sen->swtToStrip( chipChan );
      const int s2 = sen->swtToStrip( chipChan+offset );
      return s2 - s1 == m_N;
    };
    std::string name() const override {
      std::stringstream s;
      s << "stripN" << std::showpos << m_N;
      return s.str(); 
    };
  private:
    int m_N;
  };

  // --------------------------------------------------
  struct Not : public Test {
    Not( const Test& t1 ) : me( t1.clone() ) {};
    Not( const Not& n ) : Test() {
      me = n.me->clone();
    };
    ~Not(){ delete me; };
    Not* clone() const override { return ( new Not( *me ) ); };
    
    virtual bool operator()( const QuickChannelMap* ref
			     , const int chipChan
			     , const int offset ) const override { 
      return ! me->operator()( ref, chipChan, offset );
    }
    
    std::string name() const override {
      std::stringstream s;
      s << "Not<" <<  me->name() << ">"; 
      return s.str();
    };
  private:
    const Test* me;
  };

  // --------------------------------------------------
  struct Or : public Test {
    Or( const Test& t1, const Test& t2 ) 
      : me1( t1.clone() )
	, me2( t2.clone() ) 
    {};
    Or( const Or& n ) : Test() {
      me1 = n.me1->clone();
      me2 = n.me2->clone();
    };
  ~Or(){ delete me1; delete me2; };
    Or* clone() const override { return ( new Or( *this ) ); };
    
    virtual bool operator()( const QuickChannelMap* ref
			     , const int chipChan
			     , const int offset ) const override { 
      return ( me1->operator()( ref, chipChan, offset )
	       ||
	       me2->operator()( ref, chipChan, offset ) );
    }
    
    std::string name() const override {
      std::stringstream s;
      s << "<" <<  me1->name() << "_or_" << me2->name() << ">"; 
      return s.str();
    };
    
  private:
    const Test *me1, *me2;
  };

  // --------------------------------------------------
  /*! \class Cable
     \brief A specific analog cable.

  Ensure that both the signal and the noise are in the analog cable N
     (from 0 to 63 for one Tell1)
  */
  struct Cable : public Test {
    Cable( const int N ) : m_N(N) {};
    Cable* clone() const override { return ( new Cable( m_N ) ); };
    bool operator()( const QuickChannelMap* 
		     , const int chipChan
		     , const int offset ) const override { 
      return m_N == chipChan/32 && m_N==(chipChan+offset)/32;
    };
    std::string name() const override {
      std::stringstream s;
      s << "Cable" << m_N;
      return s.str(); 
    };
  private:
    int m_N;
  };

  // --------------------------------------------------
  /*! \class InnerOuter
     \brief Returns true if the signal and the noise lies on top of each other.

     In phi sensors, the inner routing lines goes over some outer
     strips. This Test returns true if the noise strip and the signal
     strip are in this situation.
  */
  struct InnerOuter : public Test {
    InnerOuter(){};
    InnerOuter* clone() const override { return ( new InnerOuter() ); };
    bool operator()( const QuickChannelMap* sen
		     , const int chipChan
		     , const int offset ) const override { 
      return sen->getOuterInnerNeighbor( chipChan ) == chipChan+offset;
    };
    std::string name() const override { return "InnerOuter" ; };
  };

  // --------------------------------------------------
  /*! \class ChipChannel
     \brief A single chip channel (0 to 2047)
  */
  struct ChipChannel : public Test {
    ChipChannel( const int N ) : m_N(N) {};
    ChipChannel* clone() const override { return ( new ChipChannel( m_N ) ); };
    bool operator()( const  QuickChannelMap* 
		     , const int chipChan
		     , const int ) const override { 
      return m_N == chipChan;
    };
    std::string name() const override {
      std::stringstream s;
      s << "ChipChannel" << m_N;
      return s.str(); 
    };
  private:
    int m_N;
  };

  // --------------------------------------------------
  /*! \class NeighborStrip
     \brief Equivalent to Or(
     ConstStripOffset(1), ConstStripOffset(-1), InnerOuter() )
  */
  struct NeighborStrip : public Test {
    NeighborStrip* clone() const override { return (new NeighborStrip());};
    bool operator()( const QuickChannelMap* ref 
		     , const int chipChan
		     , const int offset ) const override { 

      const int s1 = ref->swtToStrip( chipChan );
      const int s2 = ref->swtToStrip( chipChan+offset );
      if( abs(s1-s2)==1 ) return true;
      const int s3 = ref->getOuterInnerNeighbor( chipChan );
      if( s3>=0 && s3==chipChan+offset ) return true;
      return false;
    };
    std::string name() const override {
      return "NeighborStrip";
    };
  private:
  };
  
  // --------------------------------------------------
  /*! \class ConsecutiveCC
     \brief Equivalent to 
     Or( ConstCCOffset(1), ConstCCOffset(-1) )
  */
  struct ConsecutiveCC : public Test {
    ConsecutiveCC* clone() const override { return (new ConsecutiveCC());};
    bool operator()( const QuickChannelMap*
		     , const int 
		     , const int offset ) const override { 
      return ( offset==1 || offset==-1 );
    };
    std::string name() const override {
      return "ConsecutiveChipChannel";
    }
  };

  // --------------------------------------------------
  /*! \class InnerStrip
     \brief Strip number is < 683
  */
  struct InnerStrip : public Test {
    InnerStrip(){};
    virtual InnerStrip* clone() const override { return ( new InnerStrip() ); };
    bool operator()( const QuickChannelMap* ref
		     , const int beChan
		     , const int ) const override { 
      return ref->isInner( beChan ); 
    };
    std::string name() const override { return "InnerStrip"; };
  };

  // --------------------------------------------------
  /*! \class OuterStrip
     \brief Strip number is >= 683
  */
  struct OuterStrip : public Test {
    OuterStrip(){};
    OuterStrip* clone() const override { return ( new OuterStrip() ); };
    bool operator()( const QuickChannelMap* ref 
		     , const int beChan
		     , const int ) const override { 
      return ref->isOuter( beChan ); 
    };
    std::string name() const override { return "OuterStrip"; };
  };

  //@}

  // ===============================================================
  // ===============================================================
  // ===============================================================
  // ===============================================================

  /*! \class xTester
    \brief Handle a group of Test public derivation.

  You can construct object like \n
  xTester( Not(OuterStrip()), Or(Cable(2), Cable(8)), ConstCCOffset(+2) ) \n
  which will return true if
  the current signal-noise pair in in cable 2 or 8, the signal strip
  is an inner strip and the noise chip channel is N+2 with respect
  to signal cc N.
  */
  class xTester {
  public:
    xTester( const Test& t1=True(), const Test& t2=Empty()
	     , const Test& t3=Empty(), const Test& t4=Empty()
	     , const Test& t5=Empty(), const Test& t6=Empty() );
    ~xTester();
    xTester( const xTester& );
    xTester& operator=( const xTester& );
    
    bool operator==( const xTester& ) const;

    //! Adding a constraint to the test loop
    void addTest( const Test& r );
    //! Adding a test and takes ownership of the object.
    void addTest( Test* );
    //! Destroy all the tests
    void clearTests();
    
    //! appends all the test names.
    std::string getName(const char* pre="", const char* post="") const ;
    
    //! true is all the test returns true for this cc, offset combination.
    bool test( const int chipChan, const int offset ) const;
    
    //@{
    //! The number of combinations tested and accepted.
    int getNTested() const { return m_totalTested; };
    int getNAccepted() const { return  m_totalAccepted; };
    //@}

    //! Resets the internal counters
    void reset();
    //! initialize the internal pointer to the quick channel-map.
    void setQuickMap( const QuickChannelMap* );
  protected:
    unsigned int size() const { return tests.size(); };
  private:
    const QuickChannelMap* m_quickCM;
    std::vector< Test* > tests;
    mutable int m_totalTested, m_totalAccepted;
  };

  // ===============================================================
  /*! \class xTalk
    \brief A glorified xTester with the stat2D member that computes the x-talk. 

    An x-talk is basically the ratio of the means on a 2D stat: some
    strips versus some others. The xTester class is used to defined
    which strips are concerned, if a strip pair is accepted, the pair
    values are filled into the stat2D object.
  */
  class xTalk : public xTester {
  public:
    xTalk( const Test& t1=True(), const Test& t2=Empty(),
	   const Test& t3=Empty(), const Test& t4=Empty(),
	   const Test& t5=Empty(), const Test& t6=Empty() );
    ~xTalk();
    
    xTalk( const xTalk& );
    xTalk& operator=( const xTalk& );

    void reset();
    stat2D data;
  };

  // ===============================================================
  /*! \class Tell1Handler
    \brief  Handles the xtalk objects for one Tell1. */
  class Tell1Handler {
  public: 
    Tell1Handler(); // should not be used but std::map requires it.

    Tell1Handler( const VeloXTalkComputer*
		  , const int tell1Id );

    
    void add( const VeloXTalkComputer::xTalk& );

    int getTell1Id() const { return m_tell1Number; };
    const VeloTELL1::sdataVec* getData() const { return m_data; };
    std::vector< pedestalFollower >& getPedestal(){ return m_ped; };

    QuickChannelMap* getQuickMap(){ return m_quickCM; };
    const QuickChannelMap* getQuickMap() const { return m_quickCM; };
    void setQuickMap( const DeVeloSensor* sen );

    const DeVeloSensor* getSensor() const { return m_quickCM->getSensor(); };

    //!< All the different xtalk to compute on this sensor
    std::vector< VeloXTalkComputer::xTalk > xtalks;

    StatusCode analyseData( const VeloTELL1::sdataVec* data );
    bool prepareSignalSeek( const VeloTELL1::sdataVec* data );

    /*! look for the next chip channel with a pulse, all the
      thresholds are taken through the m_parent member. */
    bool seekNextSignal( int& signalCC ) const;
    /*! Test all the xTalk object, if one is ok, fill its stat. */
    bool testXTalk( const int xtalk_vector_idx, const int cc, const int offset ) const;

    /*! Returns an object[offset][cable] x-talk values */
    std::vector< std::vector< double > > getCableXTalk() const;
    /*! Returns an object[offset][cable] x-talk values but identical for all cables of one Tell1...*/
    std::vector< std::vector< double > > getTell1XTalk() const;

    void computePedestal( const VeloTELL1::sdataVec& data );

    /*! Sets the maximum offset to explore for any x-talk in term of
      cc numbering. Its value is automatically sets. */
    int maxVisitedCableNeighbor;
  protected:

    bool testForSignalInCableNeighbors( const int signalCC 
					,const int cc
					,const float noiseThreshold
					,const bool shiftMean ) const;
    bool testForSignalInStripNeighbors( const int signalCC 
					,const int cc
					,const float noiseThreshold
					,const bool shiftMean ) const;


    void testNoisesCC( const int signalCC );

   
  private:
    const VeloXTalkComputer* m_parent;  //! Essentially for the MessageSvc

    int m_tell1Number;
    std::vector< pedestalFollower > m_ped; //!< 'Pedestal' follower
    const VeloTELL1::sdataVec* m_data;    //!< Has to be set to the current data to analyze.

    /*! Sensor associated channel map, in principle it should be owned
      by the class, but, its a rather heavy object and it exists only
      2 maps (R and phi), no need to duplicate it 88 times. */
    QuickChannelMap* m_quickCM;
    
  };

  // ===============================================================
  // ===============================================================

 public: 
  /// Standard constructor
  VeloXTalkComputer( const std::string& name, ISvcLocator* pSvcLocator );
  
  virtual ~VeloXTalkComputer( ); ///< Destructor
  
  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization
  
 protected:
  StatusCode getData();

  std::vector<VeloXTalkComputer::pedestalFollower >& getPedestal( const int tell1Id );
  
  StatusCode getAnalysedTell1List( const DeVelo* velo
				   , std::vector<unsigned int>& t1List );

  /*! Prints, if DebugMode is on, the possible chip channel and strips
    pair that will be considered by a given x-talk object. */
  void dumpPossibleXTalkCombination() const ;

  /*! Writes some text file (with name OutputOptionFilename) with the
    x-talk coeficients. Works yet only for cable x-talk. */
  void dumpXTalkCoeficientForOptionsFile() const ;
  void dumpForGnuplot() const ;
 private:

  VeloXTalkComputer::QuickChannelMap* getQuickMap( const DeVeloSensor* sen ) const;

  //! Transform the m_tpMask into a vector of 0 and 1 describing the test pulses.
  void readTestPulsePattern();

  //! Calls for all the T1 the initalizeXXXTalkers below.
  void initializeXTalkers( const std::vector<unsigned int>& t1List );
  //! Cable x-talk up to neighbor +- maxN
  bool initializeCableXTalkers( VeloXTalkComputer::Tell1Handler&, const int maxN );
  //! Tell1 x-talk up to neighbor +- maxN
  bool initializeTell1XTalkers( VeloXTalkComputer::Tell1Handler&, const int maxN );
  //! Strip x-talk up to CHIP CHANNEL neighbor +- maxN
  bool initializeStripXTalkers( VeloXTalkComputer::Tell1Handler&, const int maxN );
  //! 'Exotic' x-talks like "Neighbor strips but not neighbors chip chan."
  bool initializePhiSensorXTalkers( VeloXTalkComputer::Tell1Handler& );

  const DeVelo* m_velo;
  
  LHCb::VeloTELL1Datas* m_inputData;  ///< Incoming data 

  std::string m_inputDataLoc; //< Where to get the data.  
  //! Mapping tell1 id to the x-talk object.
  std::map< unsigned int, VeloXTalkComputer::Tell1Handler > m_sensorxt;
  
  
  // Skip some events to let the pedestal converge.
  unsigned int m_eventCounter, m_nSkippedEvents;

  // Which tell1s must be analyzed.
  std::vector<unsigned int> m_forceAcceptedT1, m_forceRejectedT1;

  /// if true, the signal and noise thresh. are considered relative to the mean value of the strip.
  bool m_shiftThreshold; 
  float m_signalMin; /// minimum signal amplitude in the signal strip
  float m_signalMax; /// maximum signal amplitude in the signal strip, -1 means no max.
  float m_noiseMax;  /// maximum noise amplitude in the noise strip
  //! For the signal channel, decides whether to keep negative and/or positive pulse.
  bool m_discardNegative, m_discardPositive;

  /// if true, checks that the neighboring cable resp. strip channels have
  /// not seen any signal according to m_noiseMax and m_shiftThreshold.
  bool m_avoidCableDoubleHit, m_avoidSensorDoubleHit;

  //! Cable x-talk up to neighbor +- maxN
  unsigned int m_cableXTalk;
  //! Tell1 x-talk up to neighbor +- maxN
  unsigned int m_tell1XTalk;
  //! Strip x-talk up to CHIP CHANNEL neighbor +- maxN
  unsigned int m_stripXTalk;
  //! 'Exotic' x-talks like "Neighbor strips but not neighbors chip chan."
  bool m_sensorXTalk;


  //! cable channel 0 is never a possible noise channel.
  bool m_skipFirstChannel;

  //! The file name to dump the coeficients to.
  std::string m_outputOptionFileName;

  //! the values in this vector describe the chip channel that are pulsed.
  std::vector<unsigned int> m_tpMask;
  //! The m_tpMask pattern is modulo this variable.
  int m_testPulsePatternModulo;

  //! analyze only x-talk in this cable !
  int m_uniqueCable;
  //! analyze only one offset x-talk with respect to chip channels !
  int m_uniqueOffset;

  //! Any x-talk computed on less points than m_minXTalkPoints will be set to zero.
  int m_minXTalkPoints;

  //! Spits out a lot. Do not use for normal run.
  bool m_debugMode;

  //! Filename to dump a gnuplot script.
  std::string m_gnuplotDump;

  mutable VeloXTalkComputer::QuickChannelMap m_phisensor, m_rsensor;

};

#endif // VELOXTALKCOMPUTER_H
