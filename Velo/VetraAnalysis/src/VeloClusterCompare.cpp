// $Id: VeloClusterCompare.cpp,v 1.5 2009-11-04 12:06:08 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "GaudiKernel/AlgFactory.h"

// AIDA
#include "AIDA/IHistogram2D.h"
#include "AIDA/IHistogram1D.h"

// local
#include "VeloClusterCompare.h"

// velo
#include "VeloDet/DeVelo.h"

// Kernel
#include "Kernel/VeloEventFunctor.h"

// boost
#include "boost/lexical_cast.hpp"
#include "boost/format.hpp"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloClusterCompare
//
// 2007-01-25 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloClusterCompare )

using namespace Gaudi::Units;
using namespace LHCb;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloClusterCompare::VeloClusterCompare( const std::string& name,
                                        ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator ),
    m_tell1Clusters ( 0 ),
    m_emuClusters ( 0 ),
    m_tell1ClustersLoc ( LHCb::VeloClusterLocation::Default ),
    m_emuClustersLoc ( LHCb::VeloClusterLocation::Emulated ),
    m_evtNumber ( 0 ),
    m_nRawClusters ( 0., 0. ),
    m_nRawClusters2 ( 0., 0.  ),
    m_nOneStr ( 0., 0. ),
    m_nTwoStr ( 0., 0. ),
    m_nThreeStr ( 0., 0. ),
    m_nFourStr ( 0., 0. ),
    m_excludedSensorList ( ),
    m_usedSensorList ( ),
    m_localTell1 ( ),
    m_localEmu ( ),
    m_bpClustersTell1_1 ( ),
    m_bpClustersEmu_1 ( ),
    m_bpClustersTell1_2 ( ),
    m_bpClustersEmu_2 ( ),
    m_tell1Only ( ),
    m_emuOnly ( ),
    m_bpStatistics ( 8, 0. ),
    m_matchedPosChannels ( ),
    m_unmatchedTell1 ( 0., 0. ),
    m_unmatchedEmu ( 0., 0. )
{
  declareProperty("ExcludedSensorList", m_excludedSensorList);
  declareProperty("UsedSensorList", m_usedSensorList);
  declareProperty("SOBitThreshold", m_spilloverBitThreshold=20);
}
//=============================================================================
// Destructor
//=============================================================================
VeloClusterCompare::~VeloClusterCompare() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloClusterCompare::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;
  //
  setHistoTopDir("Velo/");
  m_veloDet=getDet<DeVelo>( DeVeloLocation::Default );
  bookFilterHistos();

  // matching position window
  for(int pos=1; pos<=4; ++pos){

    m_positionWindow.push_back(pos);
    m_positionWindow.push_back(-pos);

  }
  // determine action type - use/exclude
  m_excludedSensorList.size() ? m_actionType=_EXCLUDE_ :
  m_usedSensorList.size() ? m_actionType=_USE_ : m_actionType=_DEFAULT_;

  // initialize the reference sensors
  const DeVeloSensor* sens_r=m_veloDet->sensor(0);
  m_refSensors.first=dynamic_cast<const DeVeloRType*>(sens_r);
  const DeVeloSensor* sens_phi=m_veloDet->sensor(64);
  m_refSensors.second=dynamic_cast<const DeVeloPhiType*>(sens_phi);

  //
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloClusterCompare::execute() {

  debug() << "==> Execute" << endmsg;
  //
  m_evtNumber++;
  StatusCode inputDataStatus=getData();
  if(inputDataStatus.isSuccess()){
    sortClusters();
    if(m_tell1Clusters!=NULL) clustersMonitor(m_tell1Clusters, TELL1);
    if(m_emuClusters!=NULL) clustersMonitor(m_emuClusters, EMU);
    copyClusters2Local();
    StatusCode filterStatus=smartClusterFilter();
    // can ignore this
    filterStatus.ignore();
    resetMemory();
  }
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloClusterCompare::finalize() {

  debug() << "==> Finalize" << endmsg;
  //
  info()<< " ======================================== " <<endmsg;
  info()<< "  All reconstructed clusters - Tell1: "
        << (m_nRawClusters.first) <<endmsg;
  info()<< "  All reconstructed clusters - Emu: "
        << (m_nRawClusters.second) <<endmsg;
  info()<< "  Position matched clusters: " 
        << (m_bpStatistics[_BP_POS_]) <<endmsg;
  info()<< "  Bit Perfect clusters: "
        << (m_bpStatistics[_BP_]) <<endmsg;
  info()<< " ---------------------------------- " <<endmsg;
  info()<< "   -> Unmatched Emu: " <<endmsg;
  info()<< "        Unmatched on R sensors: " 
        << m_unmatchedEmu.first <<endmsg;
  info()<< "        Unmatched on Phi sensors: " 
        << m_unmatchedEmu.second
        <<endmsg;
  info()<< "   -> Unmatched Tell1: " <<endmsg;
  info()<< "        Unmatched on R sensors: " 
        << m_unmatchedTell1.first <<endmsg;
  info()<< "        Unmatched on Phi sensors: " 
        << m_unmatchedTell1.second
        <<endmsg;  
  info()<< " ---------------------------------- " <<endmsg;
  info()<< " Matched within window: " <<endmsg;
  info()<< "  -> R matched: " << m_bpStatistics[_NOT_BP_POS_R_]
        <<endmsg;
  info()<< "  -> Phi matched: " << m_bpStatistics[_NOT_BP_POS_PHI_]
        <<endmsg;  
  info()<< " ======================================== " <<endmsg;
  //
  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode VeloClusterCompare::getData()
{
  debug()<< " ==> getData() " <<endmsg;
  // take the TELL1 clusters
  bool tell1Clus=false, emuClus=false;
  if(!exist<LHCb::VeloClusters>(m_tell1ClustersLoc)){
    Warning(" ==> There is no Tell1 VeloClusters in TES ");
  }else{
    m_tell1Clusters=get<LHCb::VeloClusters>(m_tell1ClustersLoc);
    tell1Clus=true;
  }
  // take the emulated clusters
  if(!exist<LHCb::VeloClusters>(m_emuClustersLoc)){
    Warning(" ==> There is no emulated VeloClusters in TES ");
  }else{
    debug()<< " retrieving " <<endmsg;
    m_emuClusters=get<LHCb::VeloClusters>(m_emuClustersLoc);
    debug()<< " retrieved " <<endmsg;
    emuClus=true;
  }
  //
  debug()<< " size of emu cont: " << m_emuClusters->size() <<endmsg;
  if(tell1Clus)
  debug()<< " ==> Number of the tell1 clusters found in TES at: "
         << m_tell1ClustersLoc << " is:"
         << m_tell1Clusters->size() <<endmsg;
  //
  if(emuClus)
  debug()<< " ==> Number of emulated clusters found in TES at: "
         << m_emuClustersLoc << " is: " 
         << m_emuClusters->size() <<endmsg;
  if(!tell1Clus&&!emuClus) return ( StatusCode::FAILURE );
  //
  return ( StatusCode::SUCCESS );
}
//============================================================================
void VeloClusterCompare::sortClusters()
{
  debug()<< " ==> sortClusters() " <<endmsg;
  //
  if(m_tell1Clusters!=NULL){
    debug()<< " size of tell1 clu container: "
          << m_tell1Clusters->size() <<endmsg;
    std::stable_sort(m_tell1Clusters->begin(),m_tell1Clusters->end(),
         VeloEventFunctor::Less_by_key<const LHCb::VeloCluster*>());
  }
  if(m_emuClusters!=NULL){
    debug()<< " size of emu clu container: "
          << m_emuClusters->size() <<endmsg;
    std::stable_sort(m_emuClusters->begin(),m_emuClusters->end(),
         VeloEventFunctor::Less_by_key<const LHCb::VeloCluster*>());
  }
  //
}
//============================================================================
void VeloClusterCompare::clustersMonitor(
           LHCb::VeloClusters* inCont, const int procType)
{
  debug()<< " ==> rawVeloClusterMonitor() " <<endmsg;
  //
  int contSize=inCont->size();
  if(procType==TELL1){
    m_nRawClusters.first+=double(contSize);
    m_nRawClusters2.first+=double(contSize*contSize);
  }
  if(procType==EMU){
    m_nRawClusters.second+=double(contSize);
    m_nRawClusters2.second+=double(contSize*contSize);
  }
  plot(contSize, 100+procType*100,
       "Number of Velo Clusters/event"+
       boost::lexical_cast<std::string>(procType),
       0., 200., 100);
  //  
  LHCb::VeloClusters::const_iterator cluIt;
  //
  for(cluIt=inCont->begin(); cluIt!=inCont->end(); cluIt++){
    // local cluster
    LHCb::VeloCluster* cluster=(*cluIt);
    LHCb::VeloChannelID channel=cluster->channelID();
    unsigned int sensNumber=channel.sensor();

    if(_DEFAULT_!=m_actionType){
      
      std::vector<unsigned int>::const_iterator iT;

      if(_EXCLUDE_==m_actionType){
        
        iT=find(m_excludedSensorList.begin(),
                m_excludedSensorList.end(),
                sensNumber
        );

        if(iT!=m_excludedSensorList.end()) continue;

      }else if(_USE_==m_actionType){

        iT=find(m_usedSensorList.begin(),
                m_usedSensorList.end(),
                sensNumber
        );

        if(iT==m_usedSensorList.end()) continue;        
      }

    }

    // cluster size - number of strips in cluster
    unsigned int clusterSize=cluster->size();
    debug()<< " ==> Cluster size: " << clusterSize <<endmsg;
    // adc values
    debug()<< " The given cluster has: " << clusterSize << " strip(s) "
          << " the ADC value(s) is/are: " <<endmsg;
    //
    if(procType==TELL1){
      switch(int(clusterSize)){
      case 1: m_nOneStr.first++; break;
      case 2: m_nTwoStr.first++; break;
      case 3: m_nThreeStr.first++; break;
      case 4: m_nFourStr.first++; break;
      default : debug()<< " ==> Cluster has more than four strips! " <<endmsg;
      }
    }
    if(procType==EMU){
      switch(int(clusterSize)){
      case 1: m_nOneStr.second++; break;
      case 2: m_nTwoStr.second++; break;
      case 3: m_nThreeStr.second++; break;
      case 4: m_nFourStr.second++; break;
      default : debug()<< " ==> Cluster has more than four strips! " <<endmsg;
      }
    }
    //
    // method adcValue(stripNumber) returns value of the ADC count
    // from given strip, adcValue() calls stripValues() method which
    // returns ADCVector of pairs (stripNumber, adcValue)
    //
    for(unsigned int i=0; i<clusterSize; i++){
      debug()<< " Strip from cluster [" << i 
             << "] = " << cluster->adcValue(i) 
             << ", number of the strip on sensor: " 
             << cluster->strip(i) <<endmsg;
    }
    //
    // channelID info (this channel is used to create VeloLightCluster)
    //
    const DeVeloSensor* sensor=m_veloDet->sensor(sensNumber);
    plot2D(sensor->z()/cm, sensNumber, 101+procType*100,
           "Z position and sensor number of the cluster"+
           boost::lexical_cast<std::string>(procType),
           -50., 100., 0, 135, 150, 135);
    //
    debug()<< " Central channel of given cluster: " 
          << channel << ", sensor number: " << sensNumber
          << ", strip: " << channel.strip() <<endmsg;
    //
    // total charge colected on the strips
    double tCharge=cluster->totalCharge();
    if(tCharge>126.&&tCharge<128.){
      debug()<< "clusters ADC: " << tCharge
            << "this clu strip: " << cluster->channelID().strip()
            << "and sensor: " << cluster->channelID().sensor() <<endmsg;
    }
    debug()<< " Total charge of the cluster: " << tCharge <<endmsg;
    plot(tCharge, 102+procType*100,
         "ADC sum for all clusters"+
         boost::lexical_cast<std::string>(procType),
         -0.5, 549.5, 550);
    plot2D(sensNumber, cluster->firstChannel().strip(), 103+procType*100,
           "Sensor and first strip number"+
           boost::lexical_cast<std::string>(procType),
           0., 135., 0., 2050., 135, 2050);
    plot(clusterSize, 104+procType*100,
         "Number of strips in cluster"+
         boost::lexical_cast<std::string>(procType),
         -0.5, 5.5, 6);
    //
    // fractional position of the centre of the given cluster
    double fracPos=cluster->interStripFraction();
    debug()<< " Fractional position: " << fracPos <<endmsg;
    plot(fracPos, 114+procType*100,
         "Inter strip position of cluster centre"+
         boost::lexical_cast<std::string>(procType),
         0., 1., 100);
    //
    // pseudo size of the cluster
    unsigned int pseudoSize=cluster->pseudoSize();
    debug()<< " Pseudo size: " << pseudoSize <<endmsg;
    plot(pseudoSize, 115+procType*100,
         "Cluster pseudo size"+
         boost::lexical_cast<std::string>(procType),
         -0.5, 5.5, 6);
    //
    // high threshold bit
    bool thresholdBit=cluster->highThreshold();
    debug()<< " Signal bit: " << thresholdBit <<endmsg;
    plot(thresholdBit, 116+procType*100, 
         "Cluster high threshold"+
         boost::lexical_cast<std::string>(procType),
         -1., 2., 30);
    //
    // first and last channels
    LHCb::VeloChannelID firstChan=cluster->firstChannel();
    LHCb::VeloChannelID lastChan=cluster->lastChannel();
    debug()<< " First channel info: strip - " << firstChan.strip()
          << ", sensor - " << firstChan.sensor() <<endmsg;
    debug()<< " Last channel info: strip - " << lastChan.strip()
          << ", sensor - " << lastChan.sensor() <<endmsg;
    // channels vector
    std::vector<LHCb::VeloChannelID> channels=cluster->channels();
    debug()<< " Number of VeloChannelIDs inside the channel vector: "
          << channels.size() <<endmsg;
    // type of the cluster
    if(cluster->isRType()) debug()<< " RType cluster " <<endmsg;
    if(cluster->isPhiType()) debug()<< " PhiType cluster " <<endmsg;
    if(cluster->isPileUp()) debug()<< " PileUpType cluster" <<endmsg;
    // ADC signals
    LHCb::VeloCluster::ADCVector localADC;
    localADC=cluster->stripValues();
    for(unsigned int i=0; i<localADC.size(); i++){
      int stripNum=localADC[i].first;
      unsigned int adcValue=localADC[i].second;
      debug()<< " ADC value of the strip " << stripNum << " is: "
             << adcValue <<endmsg;
    }
    // look at the clusers from the deposited charge point of view
    if(cluster->isRType()||cluster->isPileUp()){
      if(tCharge<=m_spilloverBitThreshold){
        plot(tCharge, 117+procType*100,
             "Cluster charge for R sensor below SO cut"+
             boost::lexical_cast<std::string>(procType),
             -5., 25, 30);
      }else if(tCharge>m_spilloverBitThreshold){
        plot(tCharge, 118+procType*100,
             "Cluster charge for R sensor above SO cut"+
             boost::lexical_cast<std::string>(procType),
             10., 550., 540);
      }
    }else if(cluster->isPhiType()){
      if(tCharge<=m_spilloverBitThreshold){
        plot(tCharge, 119+procType*100,
             "Cluster charge for Phi sensor below SO cut"+
             boost::lexical_cast<std::string>(procType),
             -5., 25, 30);
      }else if(tCharge>m_spilloverBitThreshold){
        plot(tCharge, 120+procType*100,
             "Cluster charge for Phi sensor above SO cut"+
             boost::lexical_cast<std::string>(procType),
             10., 550, 540);
      }
    }   
    // do not split clusers into R and Phi
    if(tCharge<=m_spilloverBitThreshold){
        plot(tCharge, 121+procType*100,
             "Cluster charge below SO cut"+
             boost::lexical_cast<std::string>(procType),
             -5., 25, 30);
    }else if(tCharge>m_spilloverBitThreshold){
        plot(tCharge, 122+procType*100,
             "Cluster charge above SO cut"+
             boost::lexical_cast<std::string>(procType),
             10., 550, 540);
    }
  }
}
//============================================================================
void VeloClusterCompare::resetMemory()
{
  debug()<< " ==> resetMemory() " <<endmsg;

  // -> clear local copy of the tell1 clusters
  if(m_localTell1->size()){
    m_localTell1->erase(m_localTell1->begin(), m_localTell1->end());
    delete m_localTell1;
  }

  // -> clear local copy of the emu clusters  
  if(m_localEmu->size()){
    m_localEmu->erase(m_localEmu->begin(), m_localEmu->end());
    delete m_localEmu;
  }

  // -> clear matched channels id vector
  if(!m_matchedPosChannels.empty()){
    m_matchedPosChannels.clear();
  }

  return;
}
//============================================================================
void VeloClusterCompare::copyClusters2Local()
{
  debug()<< " ==> copyClusters2Local() " <<endmsg;
  // -> first Tell1 clusters
  LHCb::VeloClusters::iterator cIT=m_tell1Clusters->begin();
  m_localTell1=new LHCb::VeloClusters();

  for( ; cIT!=m_tell1Clusters->end(); ++cIT){

    LHCb::VeloCluster* master=(*cIT);

    if(_DEFAULT_==m_actionType){

      LHCb::VeloCluster* copy=new LHCb::VeloCluster(master->liteCluster(),
                                                    master->stripValues()
      );
      m_localTell1->insert(copy, copy->channelID());

    }else if(_EXCLUDE_==m_actionType){
      
      std::vector<unsigned int>::const_iterator iT;
      iT=find(m_excludedSensorList.begin(),
              m_excludedSensorList.end(),
              master->channelID().sensor()
      );
      if(iT!=m_excludedSensorList.end()) continue;

    }else if(_USE_==m_actionType){
      
      std::vector<unsigned int>::const_iterator iT;
      iT=find(m_usedSensorList.begin(),
              m_usedSensorList.end(),
              master->channelID().sensor()
      );

      if(iT!=m_usedSensorList.end()){

        LHCb::VeloCluster* copy=new LHCb::VeloCluster(master->liteCluster(),
                                                      master->stripValues()
        );
        m_localTell1->insert(copy, copy->channelID());

      }

    }

  }

  // -> and emulated ones
  cIT=m_emuClusters->begin();
  m_localEmu=new LHCb::VeloClusters();

  for( ; cIT!=m_emuClusters->end(); ++cIT){

    LHCb::VeloCluster* master=(*cIT);

    if(_DEFAULT_==m_actionType){

      LHCb::VeloCluster* copy=new LHCb::VeloCluster(master->liteCluster(),
                                                    master->stripValues()
      );
      m_localEmu->insert(copy, copy->channelID());
    }else if(_EXCLUDE_==m_actionType){
      
      std::vector<unsigned int>::const_iterator iT;
      iT=find(m_excludedSensorList.begin(),
              m_excludedSensorList.end(),
              master->channelID().sensor()
      );
      if(iT!=m_excludedSensorList.end()) continue;

    }else if(_USE_==m_actionType){
      
      std::vector<unsigned int>::const_iterator iT;
      iT=find(m_usedSensorList.begin(),
              m_usedSensorList.end(),
              master->channelID().sensor()
      );

      if(iT!=m_usedSensorList.end()){

        LHCb::VeloCluster* copy=new LHCb::VeloCluster(master->liteCluster(),
                                                      master->stripValues()
        );
        m_localTell1->insert(copy, copy->channelID());

      }

    }    

  } 

  // -> check if the copying is right
  if(_DEFAULT_==m_actionType){
    
    assert(m_tell1Clusters->size()==m_localTell1->size());
    assert(m_emuClusters->size()==m_localEmu->size());

  }
  
  //
  return;
}
//============================================================================
StatusCode VeloClusterCompare::smartClusterFilter()
{
  debug()<< " ==> smartClusterFilter() " <<endmsg;
  // -> step one check bit perfectness
  if(!m_localEmu->empty()){

    LHCb::VeloClusters::iterator emIT=m_localEmu->begin();
    LHCb::VeloClusters::iterator tell1IT;

    for( ; emIT!=m_localEmu->end(); ++emIT){

      LHCb::VeloCluster* emClu=(*emIT);
      LHCb::VeloChannelID key=emClu->channelID();
      LHCb::VeloCluster* tell1Clu=m_localTell1->object(key);

      if(tell1Clu!=NULL){

        debug()<< " Found position match! " <<endmsg;
        debug()<< " Key: " << key <<endmsg;
        debug()<< " matched: " << (tell1Clu->channelID()) <<endmsg;
        ++m_bpStatistics[_BP_POS_];
        m_matchedPosChannels.push_back(key);

        if(emClu->totalCharge()!=tell1Clu->totalCharge()){
          
          if(emClu->size()==tell1Clu->size()){

            double diff=(emClu->totalCharge())-(tell1Clu->totalCharge());

            if(tell1Clu->isRType()){

              m_adcDiffVsCharge.first->fill(emClu->totalCharge(), diff);
              m_adcDiffVsPos.first->fill(emClu->channelID().strip(), diff);

            }else if(tell1Clu->isPhiType()){

              m_adcDiffVsCharge.second->fill(emClu->totalCharge(), diff);
              m_adcDiffVsPos.second->fill(emClu->channelID().strip(), diff);

            }            

          }

        }else{

          ++m_bpStatistics[_BP_];

        }

      }

    } // -> end of loop over emulated clusters

    // -> clear matched clusters from both containers
    debug()<< " emu cont size: " << (m_localEmu->size()) <<endmsg;
    debug()<< " matched cont size: " << (m_matchedPosChannels.size()) <<endmsg;
    std::vector<LHCb::VeloChannelID>::iterator chIT=m_matchedPosChannels.begin();
    unsigned int strip=0, sensor=0;
    for( ; chIT!=m_matchedPosChannels.end(); ++chIT){

      LHCb::VeloCluster* e_matched=m_localEmu->object(*chIT);
      LHCb::VeloCluster* t_matched=m_localTell1->object(*chIT);

      if((e_matched!=NULL)&&(t_matched!=NULL)){
 
        e_matched->isRType() ?
          m_matchedEmuADC.first->fill(e_matched->totalCharge()) :
          m_matchedEmuADC.second->fill(e_matched->totalCharge());

        m_allMatched.first->fill(e_matched->totalCharge());

        t_matched->isRType() ?
          m_matchedTell1ADC.first->fill(t_matched->totalCharge()) :
          m_matchedTell1ADC.second->fill(t_matched->totalCharge());

        m_allMatched.second->fill(t_matched->totalCharge());

        // pos histos matched Emu
        strip=e_matched->channelID().strip();
        sensor=e_matched->channelID().sensor();        
        e_matched->isRType() ?
          m_matchedEmuPos.first->fill(sensor, strip) :
          m_matchedEmuPos.second->fill(sensor, strip);

        // pos histos matched Tell1
        strip=t_matched->channelID().strip();
        sensor=t_matched->channelID().sensor();        
        t_matched->isRType() ?
          m_matchedTell1Pos.first->fill(sensor, strip) :
          m_matchedTell1Pos.second->fill(sensor, strip);        

        // update containers
        m_localEmu->remove(e_matched);
        m_localTell1->remove(t_matched);

      }else{

        debug()<< " Problem with updating containers! " <<endmsg;

      }

    }

    debug()<< " emu cont size(2): " << (m_localEmu->size()) <<endmsg;

    // -> make summary for the unmatched clusters
    // emu
    emIT=m_localEmu->begin();
    for( ; emIT!=m_localEmu->end(); ++emIT){
      
      LHCb::VeloCluster* clu=(*emIT);
      clu->isRType() ? ++m_unmatchedEmu.first : ++m_unmatchedEmu.second;

      clu->isRType() ?
        m_unmatchedEmuADC.first->fill(clu->totalCharge()) :
        m_unmatchedEmuADC.second->fill(clu->totalCharge());

      m_allUnMatched.first->fill(clu->totalCharge());

      // pos histos un-matched Emu
      strip=clu->channelID().strip();
      sensor=clu->channelID().sensor();
      clu->isRType() ?
        m_unmatchedEmuPos.first->fill(sensor, strip) :
        m_unmatchedEmuPos.second->fill(sensor, strip);

      // now the same but divide into different cluster size
      if(clu->isRType()){

        switch(clu->size()){
          case 1:
            m_unmatchedEmuPos1.first->fill(sensor, strip);
            break;
          case 2:
            m_unmatchedEmuPos2.first->fill(sensor, strip);
            break;
          case 3:
            m_unmatchedEmuPos3.first->fill(sensor, strip);
            break;
          case 4:
            m_unmatchedEmuPos4.first->fill(sensor, strip);
            break;
          default:
            info()<< " --> Wrong size? " <<endmsg;

        }

      }else{

        switch(clu->size()){
          case 1:
            m_unmatchedEmuPos1.second->fill(sensor, strip);
            break;
          case 2:
            m_unmatchedEmuPos2.second->fill(sensor, strip);
            break;
          case 3:
            m_unmatchedEmuPos3.second->fill(sensor, strip);
            break;
          case 4:
            m_unmatchedEmuPos4.second->fill(sensor, strip);
            break;
          default:
            info()<< " --> Wrong size? " <<endmsg;

        }

      }
      
    }
    
    // tell1
    tell1IT=m_localTell1->begin();
    for( ; tell1IT!=m_localTell1->end(); ++tell1IT){
      
      LHCb::VeloCluster* clu=(*tell1IT);
      clu->isRType() ? ++m_unmatchedTell1.first : ++m_unmatchedTell1.second;

      clu->isRType() ?
        m_unmatchedTell1ADC.first->fill(clu->totalCharge()) :
        m_unmatchedTell1ADC.second->fill(clu->totalCharge());

      m_allUnMatched.second->fill(clu->totalCharge());

      // pos histos un-matched Emu
      strip=clu->channelID().strip();
      sensor=clu->channelID().sensor();
      clu->isRType() ?
        m_unmatchedTell1Pos.first->fill(sensor, strip) :
        m_unmatchedTell1Pos.second->fill(sensor, strip);

      // now the same but divide into different cluster size
      if(clu->isRType()){

        switch(clu->size()){
          case 1:
            m_unmatchedTell1Pos1.first->fill(sensor, strip);
            break;
          case 2:
            m_unmatchedTell1Pos2.first->fill(sensor, strip);
            break;
          case 3:
            m_unmatchedTell1Pos3.first->fill(sensor, strip);
            break;
          case 4:
            m_unmatchedTell1Pos4.first->fill(sensor, strip);
            break;
          default:
            info()<< " --> Wrong size? " <<endmsg;

        }

      }else{

        switch(clu->size()){
          case 1:
            m_unmatchedTell1Pos1.second->fill(sensor, strip);
            break;
          case 2:
            m_unmatchedTell1Pos2.second->fill(sensor, strip);
            break;
          case 3:
            m_unmatchedTell1Pos3.second->fill(sensor, strip);
            break;
          case 4:
            m_unmatchedTell1Pos4.second->fill(sensor, strip);
            break;
          default:
            info()<< " --> Wrong size? " <<endmsg;

        }

      }
      
    }

    // -> filtering step 2
    emIT=m_localEmu->begin();
    for( ; emIT!=m_localEmu->end(); ++emIT){

      LHCb::VeloCluster* clu=(*emIT);
      LHCb::VeloChannelID key=clu->channelID();

      std::vector<int>::const_iterator wIT=m_positionWindow.begin();
      int str=0;
      for( ; wIT!=m_positionWindow.end(); ++wIT){
        str+=(*wIT);
        if(str<0) str=0;
        LHCb::VeloChannelID newKey=LHCb::VeloChannelID(key.sensor(), str);
        LHCb::VeloCluster* tell1Clu=m_localTell1->object(newKey);

        if(tell1Clu!=NULL){

          if(tell1Clu->size()==clu->size()){

            info()<< " ------------------------- " <<endmsg;
            info()<< " Pos Match, diff = " << (*wIT) <<endmsg;
            info()<< " Matched to: strip " << (newKey.strip())
                  << " on sensor: " << newKey.sensor() <<endmsg;          
            info()<< " ------------------------- " <<endmsg;

            tell1Clu->isRType() ? ++m_bpStatistics[_NOT_BP_POS_R_] :
              ++m_bpStatistics[_NOT_BP_POS_PHI_];
            m_localTell1->remove(tell1Clu);

          }

        }

      }

    }

    // dump remaining un-matched clusters position
    if(0!=m_localTell1->size()){
      
      LHCb::VeloClusters::iterator finIT=m_localTell1->begin();
      unsigned int cluCnt=0;
      
      for( ; finIT!=m_localTell1->end(); ++finIT, ++cluCnt){
        
        info()<< " un-matched cluster Tell1 number:  [" << cluCnt
              << "] position (sensor, strip):  " 
              << "( " << (*finIT)->channelID().sensor() << ", " 
              << (*finIT)->channelID().strip() << " )" <<endmsg;

        unsigned int chan=0, strip=0;
        strip=(*finIT)->channelID().sensor();

        if((*finIT)->isRType()){

          chan=m_refSensors.first->StripToChipChannel(strip);
          info()<< " chip channel R sens: " << chan <<endmsg;

        }else{

          chan=m_refSensors.second->StripToChipChannel(strip);
          info()<< " chip channel Phi sens: " << chan <<endmsg;

        }
        
      }
      
    }   

    if(0!=m_localTell1->size()){
      
      LHCb::VeloClusters::iterator finIT=m_localEmu->begin();
      unsigned int cluCnt=0;
      
      for( ; finIT!=m_localEmu->end(); ++finIT, ++cluCnt){
        
        info()<< " un-matched cluster Emu number:  [" << cluCnt
              << "] position (sensor, strip):  " 
              << "( " << (*finIT)->channelID().sensor() << ", " 
              << (*finIT)->channelID().strip() << " )" <<endmsg;

        unsigned int chan=0, strip=0;
        strip=(*finIT)->channelID().sensor();

        if((*finIT)->isRType()){

          chan=m_refSensors.first->StripToChipChannel(strip);
          info()<< " chip channel: " << chan <<endmsg;

        }else{

          chan=m_refSensors.second->StripToChipChannel(strip);
          info()<< " chip channel: " << chan <<endmsg;

        }
        
      }
      
    }   

  }
  return ( StatusCode::SUCCESS );
}
//============================================================================
void VeloClusterCompare::bookFilterHistos()
{
  debug()<< " ==> bookFilterHistos() " <<endmsg;
  // -> Matched clusters
  // 1) ADC for matched Tell1/Emu and R/Phi (4 histos)
  const std::string tell1="Tell1";
  const std::string emu="Emu";
  const std::string R="R";
  const std::string Phi="Phi";
  
  boost::format h_name("Matched_ADC_%s_%s");
  boost::format h_title("Matched_ADC_Dist_For_%s_%s");

  //--
  // -> Matched Emu/R
  h_name % emu % R;
  h_title % emu % R;
  m_matchedEmuADC.first=book1D(h_name.str(), h_title.str(),
                               0., 500., 500
                        );

  // -> Matched Emu/Phi
  h_name % emu % Phi;
  h_title % emu % Phi;
  m_matchedEmuADC.second=book1D(h_name.str(), h_title.str(),
                                0., 500., 500
                         );

  // -> Matched Tell1/R
  h_name % tell1 % R;
  h_title % tell1 % R;
  m_matchedTell1ADC.first=book1D(h_name.str(), h_title.str(),
                                 0., 500., 500
                          );

  // -> Matched Tell1/Phi
  h_name % tell1 % Phi;
  h_title % tell1 % Phi;
  m_matchedTell1ADC.second=book1D(h_name.str(), h_title.str(),
                                  0., 500., 500
                           );
  //--

  boost::format h_name_1("Un-Matched_ADC_%s_%s");
  boost::format h_title_1("Un-Matched_ADC_Dist_For_%s_%s");

  //--
  // -> Un-Matched Emu/R
  h_name_1 % emu % R;
  h_title_1 % emu % R;
  m_unmatchedEmuADC.first=book1D(h_name_1.str(), h_title_1.str(),
                                 0., 500., 500
                          );

  // -> Un-Matched Emu/Phi
  h_name_1 % emu % Phi;
  h_title_1 % emu % Phi;
  m_unmatchedEmuADC.second=book1D(h_name_1.str(), h_title_1.str(),
                                  0., 500., 500
                           );

  // -> Un-Matched Tell1/R
  h_name_1 % tell1 % R;
  h_title_1 % tell1 % R;
  m_unmatchedTell1ADC.first=book1D(h_name_1.str(), h_title_1.str(),
                                   0., 500., 500
                            );

  // -> Un-Matched Tell1/Phi
  h_name_1 % tell1 % Phi;
  h_title_1 % tell1 % Phi;
  m_unmatchedTell1ADC.second=book1D(h_name_1.str(), h_title_1.str(),
                                    0., 500., 500
                             );
  //--

  // 2D plots

  boost::format h_name_2("Matched_Strip_vs_Sensor_%s_%s");
  boost::format h_title_2("Matched_Strip_vs_Sensor_Dist_For_%s_%s");

  //--
  // -> Matched Emu/R
  h_name_2 % emu % R;
  h_title_2 % emu % R;
  m_matchedEmuPos.first=book2D(h_name_2.str(), h_title_2.str(),
                               -0.5, 138.5, 139, -0.5, 2047.5, 2048
                        );

  // -> Matched Emu/Phi
  h_name_2 % emu % Phi;
  h_title_2 % emu % Phi;
  m_matchedEmuPos.second=book2D(h_name_2.str(), h_title_2.str(),
                                -0.5, 138.5, 139, -0.5, 2047.5, 2048
                         );

  // -> Matched Tell1/R
  h_name_2 % tell1 % R;
  h_title_2 % tell1 % R;
  m_matchedTell1Pos.first=book2D(h_name_2.str(), h_title_2.str(),
                                 -0.5, 138.5, 139, -0.5, 2047.5, 2048
                          );

  // -> Matched Tell1/Phi
  h_name_2 % tell1 % Phi;
  h_title_2 % tell1 % Phi;
  m_matchedTell1Pos.second=book2D(h_name_2.str(), h_title_2.str(),
                                  -0.5, 138.5, 139, -0.5, 2047.5, 2048
                           );
  //--

  // 2D plots
  boost::format h_name_3("Un-Matched_Strip_vs_Sensor_%s_%s");
  boost::format h_title_3("Un-Matched_Strip_vs_Sensor_Dist_For_%s_%s");

  //--
  // -> Un-Matched Emu/R
  h_name_3 % emu % R;
  h_title_3 % emu % R;
  m_unmatchedEmuPos.first=book2D(h_name_3.str(), h_title_3.str(),
                                 -0.5, 138.5, 139, -0.5, 2047.5, 2048
                          );

  // -> Un-Matched Emu/Phi
  h_name_3 % emu % Phi;
  h_title_3 % emu % Phi;
  m_unmatchedEmuPos.second=book2D(h_name_3.str(), h_title_3.str(),
                                  -0.5, 138.5, 139, -0.5, 2047.5, 2048
                           );

  // -> Un-Matched Tell1/R
  h_name_3 % tell1 % R;
  h_title_3 % tell1 % R;
  m_unmatchedTell1Pos.first=book2D(h_name_3.str(), h_title_3.str(),
                                   -0.5, 138.5, 139, -0.5, 2047.5, 2048
                            );

  // -> Matched Tell1/Phi
  h_name_3 % tell1 % Phi;
  h_title_3 % tell1 % Phi;
  m_unmatchedTell1Pos.second=book2D(h_name_3.str(), h_title_3.str(),
                                    -0.5, 138.5, 139, -0.5, 2047.5, 2048
                             );
  //--

  // All Matched
  boost::format h_name_4("Matched_ADC_All_%s");
  boost::format h_title_4("Matched_ADC_Dist_All_For_%s");

  // emu
  h_name_4 % emu;
  h_title_4 % emu;  
  m_allMatched.first=book1D(h_name_4.str(), h_title_4.str(), 0., 500., 500);

  // tell1
  h_name_4 % tell1;
  h_title_4 % tell1;  
  m_allMatched.second=book1D(h_name_4.str(), h_title_4.str(), 0., 500., 500);

  // All Un-Matched
  boost::format h_name_5("Un-Matched_ADC_All_%s");
  boost::format h_title_5("Un-Matched_ADC_Dist_All_For_%s");

  // emu
  h_name_5 % emu;
  h_title_5 % emu;  
  m_allUnMatched.first=book1D(h_name_5.str(), h_title_5.str(), 0., 500., 500);

  // tell1
  h_name_5 % tell1;
  h_title_5 % tell1;  
  m_allUnMatched.second=book1D(h_name_5.str(), h_title_5.str(), 0., 500., 500);

  // adc un-matched
  boost::format h_name_6("Delta_ADC_vs_Total_Charge_R");
  boost::format h_title_6("Delta_ADC_vs_Total_Cluster_Charge_R");
  m_adcDiffVsCharge.first=book2D(h_name_6.str(), h_title_6.str(),
                                 -0.5, 499.5, 500, -20., 20., 40);
  boost::format h_name_7("Delta_ADC_vs_Total_Charge_Phi");
  boost::format h_title_7("Delta_ADC_vs_Total_Cluster_Charge_Phi");
  m_adcDiffVsCharge.second=book2D(h_name_7.str(), h_title_7.str(),
                                  -0.5, 499.5, 500, -20., 20., 40);

  boost::format h_name_8("Delta_ADC_vs_Position_R");
  boost::format h_title_8("Delta_ADC_vs_Position_on_Sensor_R");
  m_adcDiffVsPos.first=book2D(h_name_8.str(), h_title_8.str(),
                              -0.5, 2047.5, 2048, -20., 20., 40);
  boost::format h_name_9("Delta_ADC_vs_Position_Phi");
  boost::format h_title_9("Delta_ADC_vs_Position_on_Sensor_Phi");
  m_adcDiffVsPos.second=book2D(h_name_9.str(), h_title_9.str(),
                               -0.5, 2047.5, 2048, -20., 20., 40);

  // take a look at cluster size dep
  // -> Un-Matched Emu/R 1Str clusters
  boost::format h_name_10("Un-Matched_1Str_Strip_vs_Sensor_%s_%s");
  boost::format h_title_10("Un-Matched_1Str_Strip_vs_Sensor_Dist_For_%s_%s");

  h_name_10 % tell1 % R;
  h_title_10 % tell1 % R;
  m_unmatchedTell1Pos1.first=book2D(h_name_10.str(), h_title_10.str(),
                                    -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  h_name_10 % tell1 % Phi;
  h_title_10 % tell1 % Phi;
  m_unmatchedTell1Pos1.second=book2D(h_name_10.str(), h_title_10.str(),
                                     -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  h_name_10 % emu % R;
  h_title_10 % emu % R;
  m_unmatchedEmuPos1.first=book2D(h_name_10.str(), h_title_10.str(),
                                  -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  h_name_10 % emu % Phi;
  h_title_10 % emu % Phi;
  m_unmatchedEmuPos1.second=book2D(h_name_10.str(), h_title_10.str(),
                                   -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  // -> Un-Matched emu/R 2Str clusters
  boost::format h_name_11("Un-Matched_2Str_Strip_vs_Sensor_%s_%s");
  boost::format h_title_11("Un-Matched_2Str_Strip_vs_Sensor_Dist_For_%s_%s");

  h_name_11 % tell1 % R;
  h_title_11 % tell1 % R;
  m_unmatchedTell1Pos2.first=book2D(h_name_11.str(), h_title_11.str(),
                                    -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  h_name_11 % tell1 % Phi;
  h_title_11 % tell1 % Phi;
  m_unmatchedTell1Pos2.second=book2D(h_name_11.str(), h_title_11.str(),
                                     -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  h_name_11 % emu % R;
  h_title_11 % emu % R;
  m_unmatchedEmuPos2.first=book2D(h_name_11.str(), h_title_11.str(),
                                  -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  h_name_11 % emu % Phi;
  h_title_11 % emu % Phi;
  m_unmatchedEmuPos2.second=book2D(h_name_11.str(), h_title_11.str(),
                                   -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  // -> Un-Matched Emu/R 3Str clusters
  boost::format h_name_12("Un-Matched_3Str_Strip_vs_Sensor_%s_%s");
  boost::format h_title_12("Un-Matched_3Str_Strip_vs_Sensor_Dist_For_%s_%s");

  h_name_12 % tell1 % R;
  h_title_12 % tell1 % R;
  m_unmatchedTell1Pos3.first=book2D(h_name_12.str(), h_title_12.str(),
                                    -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  h_name_12 % tell1 % Phi;
  h_title_12 % tell1 % Phi;
  m_unmatchedTell1Pos3.second=book2D(h_name_12.str(), h_title_12.str(),
                                     -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  h_name_12 % emu % R;
  h_title_12 % emu % R;
  m_unmatchedEmuPos3.first=book2D(h_name_12.str(), h_title_12.str(),
                                  -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  h_name_12 % emu % Phi;
  h_title_12 % emu % Phi;
  m_unmatchedEmuPos3.second=book2D(h_name_12.str(), h_title_12.str(),
                                   -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  // -> Un-Matched Emu/R 4Str clusters
  boost::format h_name_13("Un-Matched_4Str_Strip_vs_Sensor_%s_%s");
  boost::format h_title_13("Un-Matched_4Str_Strip_vs_Sensor_Dist_For_%s_%s");

  h_name_13 % tell1 % R;
  h_title_13 % tell1 % R;
  m_unmatchedTell1Pos4.first=book2D(h_name_13.str(), h_title_13.str(),
                                    -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  h_name_13 % tell1 % Phi;
  h_title_13 % tell1 % Phi;
  m_unmatchedTell1Pos4.second=book2D(h_name_13.str(), h_title_13.str(),
                                     -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  h_name_13 % emu % R;
  h_title_13 % emu % R;
  m_unmatchedEmuPos4.first=book2D(h_name_13.str(), h_title_13.str(),
                                  -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );

  h_name_13 % emu % Phi;
  h_title_13 % emu % Phi;
  m_unmatchedEmuPos4.second=book2D(h_name_13.str(), h_title_13.str(),
                                   -0.5, 138.5, 139, -0.5, 2047.5, 2048
  );
  //
  return;
}
