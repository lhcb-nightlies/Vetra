// $Id: VeloBeetleHeaderXTalkComputer.h,v 1.7 2009-09-18 16:17:06 szumlat Exp $
#ifndef VELOBEETLEHEADERXTALKCOMPUTER_H 
#define VELOBEETLEHEADERXTALKCOMPUTER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "VeloEvent/VeloTELL1Data.h"

// std includes
#include <vector>
#include <map>

// ROOT
#include "TH2.h"
#include "TH1.h"

/** @class VeloBeetleHeaderXTalkComputer VeloBeetleHeaderXTalkComputer.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2008-05-19
 */

// forward declarations
namespace AIDA{
  class IHistogram1D;
  class IHistogram2D;
}

namespace BHXT_TYPES{
  typedef std::map<int, AIDA::IHistogram1D*> HIST_MAP_1D;
  typedef HIST_MAP_1D::iterator HM1D_IT;
  typedef std::map<int, HIST_MAP_1D > MAP_HM1D;
  typedef MAP_HM1D::iterator MHM1D_IT;
  typedef std::map<std::string, AIDA::IHistogram1D*> CHAR_HIST_MAP_1D;
  typedef CHAR_HIST_MAP_1D::iterator CHM1D_IT;
  typedef std::map<int, CHAR_HIST_MAP_1D > MAP_CHM1D;
  typedef MAP_CHM1D::iterator MCHM1D_IT;
  typedef std::map<int, AIDA::IHistogram2D*> HIST_MAP_2D;
  typedef HIST_MAP_2D::iterator HM2D_IT;
  typedef std::vector<AIDA::IHistogram1D*> HIST_VEC_1D;
  typedef HIST_VEC_1D::iterator HV1D_IT;
  typedef std::vector<AIDA::IHistogram2D*> HIST_VEC_2D;
  typedef HIST_VEC_2D::iterator HV2D_IT;
  typedef std::map<int, HIST_VEC_2D > ALINK_MAP_2D;
  typedef ALINK_MAP_2D::iterator AM2D_IT;
  typedef std::map<int, ALINK_MAP_2D > SENSOR_MAP_2D;  
  typedef SENSOR_MAP_2D::iterator SM2D_IT;
  typedef std::map<int, HIST_MAP_2D > MAP_HM2D;
  typedef MAP_HM2D::iterator MHM2D_IT;
}

class DeVelo;

class VeloBeetleHeaderXTalkComputer : public GaudiTupleAlg{
public: 

  // -> be mindful of locations, there are many of them
  enum channels{
    CH0_H=0,
    CH1_H=1,
    CH0_HH=2,
    CH1_HH=3,
    CH0_HL=4,
    CH1_HL=5,
    CH0_L=6,
    CH1_L=7,
    CH0_LH=8,
    CH1_LH=9,
    CH0_LL=10,
    CH1_LL=11,
    CH15_H=12,
    CH15_L=13
  };

  enum HeadersInDataLoc{
    HEADER_3_LOCATION=1,
    HEADER_2_LOCATION=2
  };
  
  enum DataLocation{
    DATA_1_LOCATION=1,
    DATA_15_LOCATION=15
  };
  
  enum BXTalkConst{
    FULL_ALGO=0,
    BASE_LINE_ALGO=1,
    CORR_HIST_ALINK=8
  };
  
  /// Standard constructor
  VeloBeetleHeaderXTalkComputer(const std::string& name, ISvcLocator* pSvcLocator);

  virtual ~VeloBeetleHeaderXTalkComputer( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization
  
protected:

  StatusCode getData();
  StatusCode getHeader();
  void bookHeadersMoniHistograms(const int tell1);
  void bookCorrelationsHistograms(const int tell1);
  void plotHeaders();  
  void bookCorrelationsPlots(const int tell1);
  StatusCode analyzeBeetleXTalk();
  void globalHeadersBaseLine();
  
private:

  int m_evtNumber;
  int m_headerCentreValue;
  int m_baseLine;
  int m_correlationsBins1;
  int m_correlationsBins2;
  int m_upperHeaderHigh;
  int m_lowerHeaderHigh;
  int m_upperHeaderLow;
  int m_lowerHeaderLow;
  int m_upperADC;
  int m_lowerADC;
  int m_clockLatency;
  int m_convergenceLimit;
  int m_hitThreshold;
  int m_plotHeaders;
  bool m_runWithHeaderInData;
  bool m_isDebug;
  bool m_plotCorrel;
  bool m_plotCorrelAll;
  int m_BHXTCorrActivated;
  LHCb::VeloTELL1Datas* m_adcs;     /// container for ADCs
  LHCb::VeloTELL1Datas* m_headers;  /// container for ADC headers
  std::string m_adcsLoc;
  std::string m_headersLoc;
  std::vector<int> m_srcIdList;
  BHXT_TYPES::HIST_MAP_2D m_sensHeaders;
  std::vector<std::string> m_meanTypes;
  std::vector<int> m_noiseTell1s;
  std::vector<int> m_noiseALinks;
  std::map<int, std::map<int, std::map<std::string, int> > > m_counters;
  BHXT_TYPES::MAP_CHM1D m_means;
  BHXT_TYPES::MAP_CHM1D m_variances;
  BHXT_TYPES::MAP_CHM1D m_rms;  
  BHXT_TYPES::MAP_HM1D m_noise;
  BHXT_TYPES::HIST_MAP_1D m_rmsAll;
  std::map<std::string, int> m_correlationTypes;
  std::vector<std::string> m_headerTypes;
  BHXT_TYPES::SENSOR_MAP_2D m_correlations;
  BHXT_TYPES::MAP_HM2D m_correlationsAll;
  std::map<int, std::map<int, std::vector<float> > > m_meanInChannels;
  std::map<int, std::map<int, std::vector<float> > > m_varianceInChannels;
  std::map<int, std::map<int, std::vector<float> > > m_varianceAllInChannels;
  std::map<int, std::map<int, std::vector<float> > > m_aLinkNoise;
  std::map<int, std::map<int, std::vector<int> > > m_aLinkNoiseCnts;
  std::map<int, float> m_globalHeaderBaseLine;
  BHXT_TYPES::HIST_MAP_1D m_globalHeaderThresholds;
  DeVelo* m_velo;
  unsigned int m_baseLineAlgo;

};
#endif // VELOBEETLEHEADERXTALKCOMPUTER_H
