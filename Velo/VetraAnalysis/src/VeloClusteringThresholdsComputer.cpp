// $Id: VeloClusteringThresholdsComputer.cpp,v 1.5 2010-03-25 15:39:04 krinnert Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
#include "AIDA/IHistogram1D.h"
#include "GaudiUtils/Aida2ROOT.h"

// local
#include "VeloClusteringThresholdsComputer.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "Tell1Kernel/VeloDecodeCore.h"

// boost
#include "boost/format.hpp"

// VeloDet
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloClusteringThresholdsComputer
//
// 2008-08-01 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloClusteringThresholdsComputer )

using namespace Gaudi::Utils;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloClusteringThresholdsComputer::VeloClusteringThresholdsComputer(
                  const std::string& name, ISvcLocator* pSvcLocator)
  : GaudiTupleAlg(name, pSvcLocator),
    m_evtNumber ( 0 ),
    m_isDebug ( msgLevel( MSG::DEBUG ) ),
    m_cmsDataLoc ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ),
    m_cmsData ( 0 ),
    m_velo ( 0 )
{
  declareProperty("ADCCut", m_adcCut=15.);
  declareProperty("ConvergenceLimit", m_convergenceLimit=4098);
}
//=============================================================================
// Destructor
//=============================================================================
VeloClusteringThresholdsComputer::~VeloClusteringThresholdsComputer() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloClusteringThresholdsComputer::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if(m_isDebug) debug() << "==> Initialize" << endmsg;
  setHistoTopDir( "Vetra/" );
  
  m_velo=getDet<DeVelo>(DeVeloLocation::Default);

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloClusteringThresholdsComputer::execute() {

  if(m_isDebug) debug() << "==> Execute" << endmsg;
  //
  ++m_evtNumber;
  if(m_evtNumber>m_convergenceLimit){
    StatusCode dataStatus=getData();
    if(dataStatus.isSuccess()){
      StatusCode noiseCollectorStatus=noiseCollector();
      if(noiseCollectorStatus.isFailure()) return ( noiseCollectorStatus );
    }
    //}else{
    //  return ( dataStatus );
    //}
  }
  return ( StatusCode::SUCCESS );
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloClusteringThresholdsComputer::finalize() {

  if(m_isDebug) debug() << "==> Finalize" << endmsg;
  //
  info()<< " --> Calculating seeding thresholds " <<endmsg;

  //char name[80], title[80];
  std::map<int, std::vector<float> >::iterator noiseIT;
  noiseIT=m_noiseRMS.begin();
  TH1D* HIST_HANDLE=NULL;
  for( ; noiseIT!=m_noiseRMS.end(); ++noiseIT){

    const DeVeloSensor* sensor=m_velo->sensorByTell1Id(noiseIT->first);
    if(sensor==0){
      return ( Error(" --> Cannot map tell1 to sensor number!", 
               StatusCode::FAILURE) );
    }

    info()<< " --> Now calculating thresholds for Tell1: "
          << (noiseIT->first) <<endmsg;
    boost::format name("TELL1_%03d/Noise_RMS");
    boost::format title("Noise_RMS_All_Channels_Sensor_%d");
    name % noiseIT->first;
    title % sensor->sensorNumber();
    m_thresholds[noiseIT->first]=book1D(name.str(), title.str(),
                                        -0.5, VeloTELL1::ALL_STRIPS-0.5,
                                        VeloTELL1::ALL_STRIPS
    );
    HIST_HANDLE=Aida2ROOT::aida2root(m_thresholds[noiseIT->first]);

    std::vector<float>::iterator rmsIT=noiseIT->second.begin();
    int cnt=0;
    int currentCnt=0;
    float noise=0.;

    for( ; rmsIT!=noiseIT->second.end(); ++rmsIT, ++cnt){

      currentCnt=m_noiseRMSCnts[noiseIT->first][cnt];
      currentCnt>0?noise=(*rmsIT)/static_cast<float>(currentCnt):noise=0;
      HIST_HANDLE->SetBinContent(cnt+1, sqrt(noise));

    }

  }
  /*
  // write histos and clear memory
  std::map<int, TH1F*>::iterator histIT=m_thresholds.begin();
  for( ; histIT!=m_thresholds.end(); ++histIT){
    histIT->second->Write();
    delete histIT->second;
  }
  */
  //
  return ( GaudiTupleAlg::finalize() );  //must be called after all other actions
}
//=============================================================================
StatusCode VeloClusteringThresholdsComputer::getData()
{
  if(m_isDebug) debug()<< " --> getData() " <<endmsg;
  //
  if(!exist<LHCb::VeloTELL1Datas>(m_cmsDataLoc)){
    //info()<< " ==> There is no decoded ADCs at: "
    //       << m_cmsDataLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{  
    // get data banks from default TES location
    m_cmsData=get<LHCb::VeloTELL1Datas>(m_cmsDataLoc);
    if(m_isDebug) debug()<< " --> ADCs read in from location: "
                         << m_cmsDataLoc
                         << ", size of data container: "
                         << m_cmsData->size() <<endmsg;
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloClusteringThresholdsComputer::noiseCollector()
{
  if(m_isDebug) debug()<< " --> noiseCollector " <<endmsg;
  //
  LHCb::VeloTELL1Datas::const_iterator dataIT;
  dataIT=m_cmsData->begin();
  // loop over sensors
  for( ; dataIT!=m_cmsData->end(); ++dataIT){
    LHCb::VeloTELL1Data* adcs=(*dataIT);
    assert(adcs->isReordered()==true);
    const int tell1=adcs->key();
    VeloTELL1::sdataVec data=adcs->data();
    // reserve memory for noise collection
    std::map<int, std::vector<float> >::iterator noiseIT;
    noiseIT=m_noiseRMS.find(tell1);
    if(noiseIT==m_noiseRMS.end()){
      m_noiseRMS[tell1].resize(VeloTELL1::ALL_STRIPS);
    }
    // the same for counters
    std::map<int, std::vector<int> >::iterator cntsIT;
    cntsIT=m_noiseRMSCnts.find(tell1);
    if(cntsIT==m_noiseRMSCnts.end()){
      m_noiseRMSCnts[tell1].resize(VeloTELL1::ALL_STRIPS);
    }
    //
    float adc=0.;
    int strip=0;
    VeloTELL1::sdataVec::const_iterator dIT=data.begin();
    for( ; dIT!=data.end(); ++dIT, ++strip){
      //adc=static_cast<float>(adcs->stripADC(strip));
      adc=static_cast<float>(*dIT);
      if(adc>=m_adcCut){
        continue;
      }else{
        m_noiseRMS[tell1][strip]+=adc*adc;
        m_noiseRMSCnts[tell1][strip]++;
      }
    }
  }
  //
  return ( StatusCode::SUCCESS );
}

