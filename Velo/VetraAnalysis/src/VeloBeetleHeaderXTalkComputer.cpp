// $Id: VeloBeetleHeaderXTalkComputer.cpp,v 1.8 2010-03-25 15:39:04 krinnert Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "GaudiUtils/Aida2ROOT.h"

// local
#include "VeloBeetleHeaderXTalkComputer.h"

// boost
#include "boost/lexical_cast.hpp"
#include "boost/format.hpp"

// Kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"

// VeloDet
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloBeetleHeaderXTalkComputer
//
// 2008-05-19 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloBeetleHeaderXTalkComputer )

using namespace Gaudi::Utils;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloBeetleHeaderXTalkComputer::VeloBeetleHeaderXTalkComputer(
  const std::string& name, ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator ),
    m_evtNumber ( 0 ),
    m_headerCentreValue ( 0 ),
    m_baseLine ( 0 ),
    m_correlationsBins1 ( 60 ),
    m_correlationsBins2 ( 40 ),
    m_upperHeaderHigh ( 80 ),
    m_lowerHeaderHigh ( 40 ),
    m_upperHeaderLow ( -40 ),
    m_lowerHeaderLow ( -80 ),
    m_upperADC ( 20 ),
    m_lowerADC ( -20 ),
    m_isDebug ( msgLevel( MSG::DEBUG ) ),
    m_adcs ( 0 ),
    m_headers ( 0 ),
    m_adcsLoc ( LHCb::VeloTELL1DataLocation::PedSubADCs ),
    m_headersLoc ( LHCb::VeloTELL1DataLocation::Headers ),
    m_srcIdList ( ),
    m_sensHeaders ( ),
    m_meanTypes ( ),
    m_means ( ),
    m_variances ( ),
    m_rms ( ),
    m_rmsAll ( ),
    m_correlationTypes ( ),
    m_headerTypes ( ),
    m_correlations ( ),
    m_correlationsAll ( ),
    m_meanInChannels ( ),
    m_varianceInChannels ( ),
    m_varianceAllInChannels ( ),
    m_globalHeaderBaseLine ( ),
    m_velo ( 0 )
{
  declareProperty("RunWithHeaderInData", m_runWithHeaderInData=false);
  declareProperty("ClockLatency", m_clockLatency=5);
  declareProperty("ConvergenceLimit", m_convergenceLimit=1000);
  declareProperty("CorrelationBinsHeaders", m_correlationsBins1);
  declareProperty("CorrelationBinsADC", m_correlationsBins2);
  declareProperty("UpperLimitHeaderHigh", m_upperHeaderHigh);
  declareProperty("LowerLimitHeaderHigh", m_lowerHeaderHigh);
  declareProperty("UpperLimitHeaderLow", m_upperHeaderLow);
  declareProperty("LowerLimitHeaderLow", m_lowerHeaderLow);
  declareProperty("UpperLimitADC", m_upperADC);
  declareProperty("LowerLimitADC", m_lowerADC);
  declareProperty("HitThreshold", m_hitThreshold=15);
  declareProperty("PlotCorrelations", m_plotCorrel=false);
  declareProperty("PlotCorrelationsAll", m_plotCorrelAll=false);
  declareProperty("NoiseSensors", m_noiseTell1s);
  declareProperty("NoiseALinks", m_noiseALinks);
  declareProperty("PlotHeaders", m_plotHeaders=false);
  declareProperty("BHXTCorrActivated", m_BHXTCorrActivated=0);
  declareProperty("BaseLineAlgo", m_baseLineAlgo=BASE_LINE_ALGO);
}
//=============================================================================
// Destructor
//=============================================================================
VeloBeetleHeaderXTalkComputer::~VeloBeetleHeaderXTalkComputer() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloBeetleHeaderXTalkComputer::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  setHistoTopDir( "Vetra/" );
  // initialize correlation and header type maps
  // description:
  // __ __ __ __ | __ __ ... __ ... __
  // H0 H1 H2 H3   D0 D1     D15    D31
  // one alink - 36 samples = 4 headers + 32 data
  // correlations we are looking at:
  // 30 - between header 3 and channel 0 in data section
  // first H (or L) means that header 3 is high (or low)
  // second H (or L) means that header 2 is high (or low)
  m_correlationTypes["30HH"]=0;
  m_correlationTypes["30HL"]=1;
  m_correlationTypes["30LH"]=2;
  m_correlationTypes["30LL"]=3;
  m_correlationTypes["31HH"]=4;
  m_correlationTypes["31HL"]=5;
  m_correlationTypes["31LH"]=6;
  m_correlationTypes["31LL"]=7;
  m_correlationTypes["H"]=8;
  m_correlationTypes["L"]=9;
  //
  m_headerTypes.push_back("30");
  m_headerTypes.push_back("20");
  m_headerTypes.push_back("H3H2");
  //
  m_meanTypes.push_back("H");
  m_meanTypes.push_back("L");
  if(!m_baseLineAlgo){
    m_meanTypes.push_back("HH");
    m_meanTypes.push_back("HL");
    m_meanTypes.push_back("LH");
    m_meanTypes.push_back("LL");
  }
  m_meanTypes.push_back("BaseH");
  m_meanTypes.push_back("BaseL");

  m_velo=getDet<DeVelo>(DeVeloLocation::Default);  

  //
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloBeetleHeaderXTalkComputer::execute() {

  if(m_isDebug) debug() << "==> Execute" << endmsg;

  //
  ++m_evtNumber;

  //  calculate headers global base line to classify the 3rd header states
  if(m_evtNumber<=m_convergenceLimit){
    
    globalHeadersBaseLine();

  }
  
  if(m_evtNumber>m_convergenceLimit){

    StatusCode dataStatus=getData();

    if(dataStatus.isSuccess()){

      if(m_plotHeaders) plotHeaders();
      StatusCode analyzeStatus=analyzeBeetleXTalk();

      if(analyzeStatus.isFailure()) return ( analyzeStatus );

    }
    //}else{

    //  return ( dataStatus );

    //}

  }
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloBeetleHeaderXTalkComputer::finalize() {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  // dont be offended by the code below this is actually a root macro...

  // book and fill mean histos
  const int refLevelCnt=m_evtNumber-m_convergenceLimit;
  info()<< " ------------------------------------------------ " <<endmsg;
  info()<< " --> BHXT params calculated using " << refLevelCnt << " events "
        <<endmsg;
  info()<< " ------------------------------------------------ " <<endmsg;
  
  std::map<int, std::map<int, std::vector<float> > >::iterator meanIT, varIT, rmsIT;
  meanIT=m_meanInChannels.begin();
  varIT=m_varianceInChannels.begin();

  boost::format name("");
  boost::format title("");

  // -> a handle object to retrieve root histo from AIDA
  TH1D* HIST_HANDLE=NULL;
  
  info()<< " --> Computing the correction coefficients for the Beetle X-Talk algo " <<endmsg;
  info()<< " --> Size of mean container: " << (m_meanInChannels.size()) <<endmsg;
  
  for( ; meanIT!=m_meanInChannels.end(); ++meanIT, ++varIT){
    // iterate over sensors
    std::map<int, std::vector<float> >::iterator vecMap=meanIT->second.begin();
    std::map<int, std::vector<float> >::iterator vecMap1=varIT->second.begin();
    // book histograms to store means
    std::vector<std::string>::const_iterator tIT=m_meanTypes.begin();

    const DeVeloSensor* sensor=m_velo->sensorByTell1Id(meanIT->first);
    if(sensor==0){
      return ( Error(" --> Cannot map tell1 to sensor number!", 
               StatusCode::FAILURE) );
    }

    for( ; tIT!=m_meanTypes.end(); ++tIT){

      if("BaseH"==(*tIT)||"BaseL"==(*tIT)) continue;

      name=boost::format("TELL1_%03d/Mean_Values_%s");
      title=boost::format("Means_Sensor%d_%s");
      name % meanIT->first %  (*tIT);
      title % sensor->sensorNumber() % (*tIT);

      m_means[meanIT->first][(*tIT)]=book1D(name.str(), title.str(), 
                                            -0.5, 191.5, 192
      );

      name=boost::format("TELL1_%03d/Variance_Values_%s");
      title=boost::format("Variance_Sensor%d_%s");
      name % meanIT->first %  (*tIT);
      title % sensor->sensorNumber() % (*tIT);      

      m_variances[varIT->first][(*tIT)]=book1D(name.str(), title.str(),
                                               -0.5, 191.5, 192
      );

      name=boost::format("TELL1_%03d/RMS_Values_%s");
      title=boost::format("RMS_Sensor%d_%s");
      name % meanIT->first % (*tIT);
      title % sensor->sensorNumber() % (*tIT);      
      
      m_rms[varIT->first][(*tIT)]=book1D(name.str(), title.str(),
                                         -0.5, 191.5, 192
      );

    }

    // iterate over aLinks
    int counter=0;

    for( ; vecMap!=meanIT->second.end(); ++vecMap, ++vecMap1, ++counter){
      std::vector<float> vecMean=vecMap->second;
      std::vector<float> vecVar=vecMap1->second;
      float mean=0., var=0., rms=0.;
      int currentCnt=0;

      // use the same counters - variances are collected in the same places
      currentCnt=m_counters[meanIT->first][counter]["BaseH"];
      float refMeanH=0., refVarH=0.;

      //  ref mean and var for H
      currentCnt>0?
      refMeanH=vecMean[12]/(static_cast<float>(currentCnt)):
      refMeanH=0.;
      currentCnt>0?
      refVarH=vecVar[12]/(static_cast<float>(currentCnt))-refMeanH*refMeanH:
      refVarH=0.;

      //  ref mean and var for L
      currentCnt=m_counters[meanIT->first][counter]["BaseL"];
      float refMeanL=0., refVarL=0.;
      currentCnt>0?
      refMeanL=vecMean[13]/(static_cast<float>(currentCnt)):
      refMeanL=0.;
      currentCnt>0?
      refVarL=vecVar[13]/(static_cast<float>(currentCnt))-refMeanL*refMeanL:
      refVarL=0.;

      // iterate over mean values
      // ---------------------------------------------------------
      //  mean calculated with header3 H criterion only
      // -> first mean for chan0 with header3 H
      //----------------------------------------------------------      
      currentCnt=m_counters[meanIT->first][counter]["H"];

      currentCnt>0?
      mean=(vecMean[0])/(static_cast<float>(currentCnt)):
      mean=0;
      HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["H"]);
      HIST_HANDLE->SetBinContent((3*counter+1), mean);

      currentCnt>0?
      var=(vecVar[0])/(static_cast<float>(currentCnt))-mean*mean:
      var=0;
      HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["H"]);
      HIST_HANDLE->SetBinContent((3*counter+1), sqrt(var));

      currentCnt>0?
      rms=(vecVar[0])/(static_cast<float>(currentCnt)):
      rms=0;
      HIST_HANDLE=Aida2ROOT::aida2root(m_rms[varIT->first]["H"]);
      HIST_HANDLE->SetBinContent((3*counter+1), sqrt(rms));

      // -----------------------------------------------------------
      // -> mean for chan1 with header3 H
      // -----------------------------------------------------------
      currentCnt>0?
      mean=(vecMean[1])/(static_cast<float>(currentCnt)):
      mean=0;
      HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["H"]);
      HIST_HANDLE->SetBinContent((3*counter+2), mean);

      currentCnt>0?
      var=(vecVar[1])/(static_cast<float>(currentCnt))-mean*mean:
      var=0;
      HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["H"]);
      HIST_HANDLE->SetBinContent((3*counter+2), sqrt(var));

      currentCnt>0?
      rms=(vecVar[1])/(static_cast<float>(currentCnt)):
      rms=0;
      HIST_HANDLE=Aida2ROOT::aida2root(m_rms[varIT->first]["H"]);
      HIST_HANDLE->SetBinContent((3*counter+2), sqrt(rms));

      // -> mean for chan15 - reference noise
      HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["H"]);
      HIST_HANDLE->SetBinContent((3*counter+3), refMeanH);

      HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["H"]);
      HIST_HANDLE->SetBinContent((3*counter+3), sqrt(refVarH));

      HIST_HANDLE=Aida2ROOT::aida2root(m_rms[varIT->first]["H"]);
      HIST_HANDLE->SetBinContent((3*counter+3), sqrt(refVarH));

      // -------------------------------------------------------------
      //  mean calculated with header3 L criterion only
      // -> mean for chan0 - with h3 L
      // -------------------------------------------------------------
      currentCnt=m_counters[meanIT->first][counter]["L"];

      currentCnt>0?
      mean=(vecMean[6])/(static_cast<float>(currentCnt)):
      mean=0;
      HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["L"]);
      HIST_HANDLE->SetBinContent((3*counter+1), mean);

      currentCnt>0?
      var=(vecVar[6])/(static_cast<float>(currentCnt))-mean*mean:
      var=0;
      HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["L"]);
      HIST_HANDLE->SetBinContent((3*counter+1), sqrt(var));

      currentCnt>0?
      rms=(vecVar[6])/(static_cast<float>(currentCnt)):
      rms=0;
      HIST_HANDLE=Aida2ROOT::aida2root(m_rms[varIT->first]["L"]);
      HIST_HANDLE->SetBinContent((3*counter+1), sqrt(rms));

      // -------------------------------------------------------------
      // -> mean for chan1 - with h3 L
      // -------------------------------------------------------------
      currentCnt>0?
      mean=(vecMean[7])/(static_cast<float>(currentCnt)):
      mean=0;
      HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["L"]);
      HIST_HANDLE->SetBinContent((3*counter+2), mean);

      currentCnt>0?
      var=(vecVar[7])/(static_cast<float>(currentCnt))-mean*mean:
      var=0;
      HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["L"]);
      HIST_HANDLE->SetBinContent((3*counter+2), sqrt(var));

      currentCnt>0?
      rms=(vecVar[7])/(static_cast<float>(currentCnt)):
      rms=0;
      HIST_HANDLE=Aida2ROOT::aida2root(m_rms[varIT->first]["L"]);
      HIST_HANDLE->SetBinContent((3*counter+2), sqrt(rms));

      // -> mean for chan15 - reference noise
      HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["L"]);
      HIST_HANDLE->SetBinContent((3*counter+3), refMeanL);

      HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["L"]);
      HIST_HANDLE->SetBinContent((3*counter+3), sqrt(refVarL));

      HIST_HANDLE=Aida2ROOT::aida2root(m_rms[varIT->first]["L"]);
      HIST_HANDLE->SetBinContent((3*counter+3), sqrt(refVarL));

      if(!m_baseLineAlgo){        

        // -------------------------------------------------------------
        //  mean calculated with header3 H && header2 H
        // -> mean for chan0 - with h3 H && h2 H
        // -------------------------------------------------------------
        currentCnt=m_counters[meanIT->first][counter]["HH"];

        currentCnt>0?
          mean=(vecMean[2])/(static_cast<float>(currentCnt)):
          mean=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["HH"]);
        HIST_HANDLE->SetBinContent((3*counter+1), mean);

        currentCnt>0?
          var=(vecVar[2])/(static_cast<float>(currentCnt))-mean*mean:
          mean=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["HH"]);
        HIST_HANDLE->SetBinContent((3*counter+1), sqrt(var));

        // -> mean for chan1 - with h3 H && h2 H
        currentCnt>0?
          mean=(vecMean[3])/(static_cast<float>(currentCnt)):
          mean=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["HH"]);
        HIST_HANDLE->SetBinContent((3*counter+2), mean);

        currentCnt>0?
          var=(vecMean[3])/(static_cast<float>(currentCnt))-mean*mean:
          var=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["HH"]);
        HIST_HANDLE->SetBinContent((3*counter+2), sqrt(var));

        // --------------------------------------------------------------
        // -> mean for chan15 - reference noise
        // --------------------------------------------------------------
        HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["HH"]);
        HIST_HANDLE->SetBinContent((3*counter+3), refMeanH);

        HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["HH"]);
        HIST_HANDLE->SetBinContent((3*counter+3), refVarH);

        // --------------------------------------------------------------
        //  mean calculated with header3 H && header2 L
        // -> mean for chan0 - with h3 H && h2 L
        // --------------------------------------------------------------
        currentCnt=m_counters[meanIT->first][counter]["HL"];

        currentCnt>0?
          mean=(vecMean[4])/(static_cast<float>(currentCnt)):
          mean=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["HL"]);
        HIST_HANDLE->SetBinContent((3*counter+1), mean);

        currentCnt>0?
          var=(vecVar[4])/(static_cast<float>(currentCnt))-mean*mean:
          var=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["HL"]);
        HIST_HANDLE->SetBinContent((3*counter+1), sqrt(var));

        // -> mean for chan1 - with h3 H && h2 L
        currentCnt>0?
          mean=(vecMean[5])/(static_cast<float>(currentCnt)):
          mean=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["HL"]);
        HIST_HANDLE->SetBinContent((3*counter+2), mean);

        currentCnt>0?
          var=(vecVar[5])/(static_cast<float>(currentCnt))-mean*mean:
          var=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["HL"]);
        HIST_HANDLE->SetBinContent((3*counter+2), sqrt(var));

        // -> mean for chan15 - reference noise
        HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["HL"]);
        HIST_HANDLE->SetBinContent((3*counter+3), refMeanH);

        HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["HL"]);
        HIST_HANDLE->SetBinContent((3*counter+3), sqrt(refVarH));

        // --------------------------------------------------------------
        //  mean calculated with header3 L && header2 H
        // -> mean for chan0 - with h3 L && h2 H
        // --------------------------------------------------------------
        currentCnt=m_counters[meanIT->first][counter]["LH"];

        currentCnt>0?
          mean=(vecMean[8])/(static_cast<float>(currentCnt)):
          mean=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["LH"]);
        HIST_HANDLE->SetBinContent((3*counter+1), mean);

        currentCnt>0?
          var=(vecVar[8])/(static_cast<float>(currentCnt))-mean*mean:
          mean=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["LH"]);
        HIST_HANDLE->SetBinContent((3*counter+1), sqrt(var));

        // -> mean for chan1 - with h3 L && h2 H
        currentCnt>0?
          mean=(vecMean[9])/(static_cast<float>(currentCnt)):
          mean=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["LH"]);
        HIST_HANDLE->SetBinContent((3*counter+2), mean);

        currentCnt>0?
          var=(vecVar[9])/(static_cast<float>(currentCnt))-mean*mean:
          mean=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["LH"]);
        HIST_HANDLE->SetBinContent((3*counter+2), sqrt(var));

        // -> mean for chan15 - reference noise
        HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["LH"]);
        HIST_HANDLE->SetBinContent((3*counter+3), refMeanL);

        HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["LH"]);
        HIST_HANDLE->SetBinContent((3*counter+3), sqrt(refVarL));

        // -------------------------------------------------------------
        //  mean calculated with header3 L && header2 L
        // -> mean for chan0 - with h3 L && h2 L
        // -------------------------------------------------------------
        currentCnt=m_counters[meanIT->first][counter]["LL"];

        currentCnt>0?
          mean=(vecMean[10])/(static_cast<float>(currentCnt)):
          mean=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["LL"]);
        HIST_HANDLE->SetBinContent((3*counter+1), mean);

        currentCnt>0?
          var=(vecVar[10])/(static_cast<float>(currentCnt))-mean*mean:
          var=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["LL"]);
        HIST_HANDLE->SetBinContent((3*counter+1), sqrt(var));

        // -> mean for chan1 - with h3 L && h2 L
        currentCnt>0?
          mean=(vecMean[11])/(static_cast<float>(currentCnt)):
          mean=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["LL"]);
        HIST_HANDLE->SetBinContent((3*counter+2), mean);

        currentCnt>0?
          var=(vecVar[11])/(static_cast<float>(currentCnt))-mean*mean:
          var=0;
        HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["LL"]);
        HIST_HANDLE->SetBinContent((3*counter+2), sqrt(var));

        // -> mean for chan15 - reference noise
        HIST_HANDLE=Aida2ROOT::aida2root(m_means[meanIT->first]["LL"]);
        HIST_HANDLE->SetBinContent((3*counter+3), refMeanL);

        HIST_HANDLE=Aida2ROOT::aida2root(m_variances[varIT->first]["LL"]);
        HIST_HANDLE->SetBinContent((3*counter+3), sqrt(refVarL));

      }

    }

  }

  //  global headers thresholds
  std::map<int, float>::const_iterator thIT=m_globalHeaderBaseLine.begin();

  for( ; thIT!=m_globalHeaderBaseLine.end(); ++thIT){

     name=boost::format("HeaderThresholds/Global_Header_Thresholds_Sens_%d");
     title=boost::format("Header_Thresholds_Sensor_%d");
     name % (thIT->first);
     title % (thIT->first);

     float value=thIT->second;
     m_globalHeaderThresholds[thIT->first]=book1D(name.str(), title.str(),
                                                  value-10, value+10, 20);
     m_globalHeaderThresholds[thIT->first]->fill(value);

  } 

  //  rms for all data - no header type selection
  std::map<int, std::map<int, std::vector<float> > >::iterator cIT;
  float varAll=0.;
  int evt=m_evtNumber-m_convergenceLimit;
  cIT=m_varianceAllInChannels.begin();  

  for( ; cIT!=m_varianceAllInChannels.end(); ++cIT){

    const DeVeloSensor* sensor=m_velo->sensorByTell1Id(cIT->first);
    if(sensor==0){
      return ( Error(" --> Cannot map tell1 to sensor number!", 
               StatusCode::FAILURE) );
    }
    
    name=boost::format("TELL1_%03d/RMS_Values_All_Sensor%d");
    title=boost::format("RMS_All_Sensor%d");
    name % (cIT->first) % sensor->sensorNumber();
    title % (cIT->first);
    m_rmsAll[cIT->first]=book1D(name.str(), title.str(),
                                -0.5, 191.5, 192);

    for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){

      varAll=(cIT->second[aLink][0])/static_cast<float>(evt);
      HIST_HANDLE=Aida2ROOT::aida2root(m_rmsAll[cIT->first]);
      HIST_HANDLE->SetBinContent((3*aLink+1), sqrt(varAll));

      varAll=(cIT->second[aLink][1])/static_cast<float>(evt);
      HIST_HANDLE->SetBinContent((3*aLink+2), sqrt(varAll));

      varAll=(cIT->second[aLink][2])/static_cast<float>(evt);
      HIST_HANDLE->SetBinContent((3*aLink+3), sqrt(varAll));

    }

  }

  // noise of the chosen analogue links  
  if(!m_noiseTell1s.empty()&&!m_noiseALinks.empty()){

    std::map<int, std::map<int, std::vector<float> > >::iterator nIT;

    if(!m_aLinkNoise.empty()){

      nIT=m_aLinkNoise.begin();
      int tell1=nIT->first;
      std::map<int, std::vector<float> >::iterator vIT=nIT->second.begin();

      for( ; vIT!=nIT->second.end(); ++vIT){

        int aLink=vIT->first;      
        name=boost::format("Noise_Values_Sensor%d_ALink%d");
        title=boost::format("Noise_Sensor%d_ALink%d");
        name %  tell1 % aLink;
        title % tell1 % aLink;
        m_noise[tell1][aLink]=book1D(name.str(), title.str(), -0.5, 31.5, 32);
        std::vector<float>::iterator fIT=vIT->second.begin();
        float noise=0.;     
        int cnt=0, chan=0;

        for( ; fIT!=vIT->second.end(); ++fIT, ++chan){

          cnt=m_aLinkNoiseCnts[tell1][aLink][chan];       
          noise=(*fIT)/static_cast<float>(cnt);
          HIST_HANDLE=Aida2ROOT::aida2root(m_noise[tell1][aLink]);
          HIST_HANDLE->SetBinContent((chan+1), sqrt(noise));

        }

      }

    }

  } 

  //
  return GaudiTupleAlg::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode VeloBeetleHeaderXTalkComputer::getData()
{
  if(m_isDebug) debug()<< " --> getData() " <<endmsg;
  //
  if(!exist<LHCb::VeloTELL1Datas>(m_adcsLoc)){
    //info()<< " ==> There is no decoded ADCs at: "
    //       << m_adcsLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{  
    // get data banks from default TES location
    m_adcs=get<LHCb::VeloTELL1Datas>(m_adcsLoc);
    if(m_isDebug) debug()<< " --> ADCs read in from location: "
                         << m_adcsLoc
                         << ", size of data container: "
                         << m_adcs->size() <<endmsg;
    if(m_BHXTCorrActivated&&m_globalHeaderBaseLine.size()==0){
      LHCb::VeloTELL1Datas::const_iterator dIT=m_adcs->begin();
      for( ; dIT!=m_adcs->end(); ++dIT){
        m_globalHeaderBaseLine[(*dIT)->key()]=514.;
      }
    }
  }
  // check and get decoded ADC headers
  if(!exist<LHCb::VeloTELL1Datas>(m_headersLoc)){
    //info()<< " --> There is no decoded ADC headers at: "
    //       << m_headersLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{  
    // get the headers from default TES location
    m_headers=get<LHCb::VeloTELL1Datas>(m_headersLoc);
    if(m_isDebug) debug()<< " --> ADC headers have been read-in from location: "
                         << m_headersLoc
                         << ", size of ADCH container: "
                         << m_headers->size() <<endmsg;
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloBeetleHeaderXTalkComputer::bookHeadersMoniHistograms(const int tell1)
{
  if(m_isDebug) debug()<< " ==> bookHeadersMoniHistograms() " <<endmsg; 
  //
  if(m_sensHeaders.end()==m_sensHeaders.find(tell1)){

    boost::format h_name("TELL1_%03d/Headers_Sensor_%03d");
    boost::format h_title("Headers_Bits_For_Sensor_%03d");
    h_name % tell1 % tell1;
    h_title % tell1;

    m_sensHeaders[tell1]=book2D(h_name.str(), h_title.str(),
                                -0.5, 255.5, 256, -0.5, 1023.5, 1024
    );
  }
  //
  return;
}
//=============================================================================
void VeloBeetleHeaderXTalkComputer::bookCorrelationsHistograms(
       const int tell1)
{
  if(m_isDebug) debug()<< " ==> bookCorrelationsMoniHistograms() " <<endmsg;
  
  // -> get sensor number
  const DeVeloSensor* sensor=m_velo->sensorByTell1Id(tell1);
  if(sensor==0){
    info()<< " --> Problem with mapping Tell1 to sensor number ";
  }
  unsigned int sens=sensor->sensorNumber();

  boost::format glo_name=boost::format("");
  boost::format glo_title=boost::format("");

  if(m_correlations.end()==m_correlations.find(tell1)&&m_plotCorrel){

    m_srcIdList.push_back(tell1);

    for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){

      m_correlations[tell1][aLink].resize(8);

      // 1) Header 3 strip 0, header 3 high, header 2 high
      glo_name=boost::format("TELL1_%03d/Corr_30HH_High_Head3_Sens%d_ALink%d");
      glo_name % tell1 % sens % aLink;
      glo_title=boost::format("Corr_30HH_High_Head3_Sens%d_ALink%d");
      glo_title % sens % aLink;

      m_correlations[tell1][aLink][0]=book2D(glo_name.str(), glo_title.str(),
                                             m_lowerHeaderHigh,
                                             m_upperHeaderHigh,
                                             m_correlationsBins1,
                                             m_lowerADC,
                                             m_upperADC,
                                             m_correlationsBins2
      );

      // 2) Header 3 strip 0, header 3 high, header 2 low
      glo_name=boost::format("TELL1_%03d/Corr_30HL_High_Head3_Sens%d_ALink%d");
      glo_name % tell1 % sens % aLink;
      glo_title=boost::format("Corr_30HL_High_Head3_Sens%d_ALink%d");
      glo_title % sens % aLink;

      m_correlations[tell1][aLink][1]=book2D(glo_name.str(), glo_title.str(),
                                             m_lowerHeaderHigh,
                                             m_upperHeaderHigh,
                                             m_correlationsBins1,
                                             m_lowerADC,
                                             m_upperADC,
                                             m_correlationsBins2
      );

      // 3) Hedaer 3 strip 0, header 3 low, header 2 high
      glo_name=boost::format("TELL1_%03d/Corr_30LH_High_Head3_Sens%d_ALink%d");
      glo_name % tell1 % sens % aLink;
      glo_title=boost::format("Corr_30LH_High_Head3_Sens%d_ALink%d");
      glo_title % sens % aLink;

      m_correlations[tell1][aLink][2]=book2D(glo_name.str(), glo_title.str(),
                                             m_lowerHeaderLow,
                                             m_upperHeaderLow,
                                             m_correlationsBins1,
                                             m_lowerADC,
                                             m_upperADC,
                                             m_correlationsBins2
      );

      // 4) Header 3 strip 0, header 3 low, header 2 low
      glo_name=boost::format("TELL1_%03d/Corr_30LL_High_Head3_Sens%d_ALink%d");
      glo_name % tell1 % sens % aLink;
      glo_title=boost::format("Corr_30LL_High_Head3_Sens%d_ALink%d");
      glo_title % sens % aLink;

      m_correlations[tell1][aLink][3]=book2D(glo_name.str(), glo_title.str(),
                                             m_lowerHeaderLow,
                                             m_upperHeaderLow,
                                             m_correlationsBins1,
                                             m_lowerADC,
                                             m_upperADC,
                                             m_correlationsBins2
      );

    }

    info()<< " --> Histograms for Tell1: " << tell1 << " booked " <<endmsg;

  }

  if(m_correlationsAll.end()==m_correlationsAll.find(tell1)&&m_plotCorrelAll){

    for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){

      boost::format nam("Corr_All_Sens%d_ALink%d");
      nam % sens % aLink;
      boost::format tit("Correlations_for_Sensor_%d_and_Alink_%d_All");
      tit % sens % aLink;

      m_correlationsAll[tell1][aLink]=book2D(nam.str(), tit.str(),
                                             -100., 100., 200, -20., 20., 40
      );

    }

  }

  //
  return;
}
//=============================================================================
StatusCode VeloBeetleHeaderXTalkComputer::analyzeBeetleXTalk()
{
  if(m_isDebug) debug()<< " --> analyzeBeetleXTalk() " <<endmsg;

  //
  LHCb::VeloTELL1Datas::const_iterator hIT, dIT;
  VeloTELL1::ALinkPair headerITPair, dataITPair;
  //

  assert(m_headers->size()==m_adcs->size());
  hIT=m_headers->begin();
  dIT=m_adcs->begin();

  // loop over sensors
  for( ; hIT!=m_headers->end(), dIT!=m_adcs->end(); ++hIT, ++dIT){

    LHCb::VeloTELL1Data* headers=(*hIT);
    LHCb::VeloTELL1Data* adcs=(*dIT);
    const int tell1D=adcs->key();
    //const int tell1H=headers->key();# warning for a non-debug compile

    // if you want to make some quick tests use the line below
    //if(tell1D!=61) continue;

    // be sure that data and headers come from the same sensor!
    //assert(tell1D==tell1H); # warning for a non-debug compile
    //
    BHXT_TYPES::SM2D_IT mapIT=m_correlations.find(tell1D);

    // this will allow to book histograms once per job
    if(mapIT==m_correlations.end()){

      this->bookCorrelationsHistograms(tell1D);
      mapIT=m_correlations.find(tell1D);

    }

    //
    //std::map<int, std::map<int, TH2D*> >::iterator mapAllIT;
    
    BHXT_TYPES::MHM2D_IT mapAllIT=m_correlationsAll.find(tell1D);
    if(mapAllIT==m_correlationsAll.end()&&m_plotCorrelAll){

      return ( Error("Problem with booking all correlations plots!",
               StatusCode::FAILURE) );

    }

    // loop over all alinks for current sensor
    BHXT_TYPES::HIST_VEC_2D hiVec;
    VeloTELL1::scdatIt hiT, diT;

    for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){

      // get vector of correlation histos for a given aLink
      if(m_plotCorrel){

         hiVec=mapIT->second.find(aLink)->second;

      }

      // get current alink from data - in this case we have both
      int header3=0, header2=0;
      int data0=0, data1=0, data15=0;
      // fetch the values for correlations - mind the type of data
      // this is special type of data -> headers are delayed and put in data
      dataITPair=(*adcs)[aLink];

      if(m_runWithHeaderInData){

        header3=(*(dataITPair.first+m_clockLatency-HEADER_3_LOCATION));
        header3-=m_baseLine;
        header2=(*(dataITPair.first+m_clockLatency-HEADER_2_LOCATION));
        header2-=m_baseLine;
        data0=(*(dataITPair.first+m_clockLatency));
        data1=(*(dataITPair.first+m_clockLatency+DATA_1_LOCATION));
        data15=(*(dataITPair.first+m_clockLatency+DATA_15_LOCATION));

      }else{

        headerITPair=(*headers)[aLink];
        // very crude pedestal correction
        header3=(*(headerITPair.first+3));
        header2=(*(headerITPair.first+2));
        if(header3==0)
          if (m_isDebug) debug()<< " --> Problem with Beetle PCN headers!" <<endmsg;
        // check if headers are OK
        data0=*(dataITPair.first);
        data1=*(dataITPair.first+DATA_1_LOCATION);
        data15=*(dataITPair.first+DATA_15_LOCATION);

      }

      // do not take into account detector hits
      if(data0>=m_hitThreshold||data1>=m_hitThreshold) continue;
      // determine position of each correlation hist
      int pos=0;
      std::string strHType3, strHType2;
      // book histo with all types of correlations
      //if(header2>0) --> when you want to check influence of second header

      if(m_plotCorrelAll){
        
        mapAllIT->second[aLink]->fill(header3, data0);

      }
      
      int baseLine=static_cast<int>(m_globalHeaderBaseLine[tell1D]);
      header3>=baseLine?strHType3="H":strHType3="L";
      header2>=baseLine?strHType2="H":strHType2="L";

      // fill correlation plots <--
      // correl between header 3 and adc 0
      const std::string type=m_headerTypes[0]+strHType3+strHType2;
      pos=m_correlationTypes[type];

      if(m_plotCorrel){

        hiVec[pos]->fill(header3, data0);

      }

      // -->--> calculate mean values in chosen channels
      std::map<int, std::map<int, std::vector<float> > >::iterator meanIT;
      meanIT=m_meanInChannels.find(tell1D);

      // -->--> reserve memory for means
      if(meanIT==m_meanInChannels.end()){

        for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){

          m_meanInChannels[tell1D][aLink].resize(14);

        }

      }

      // -->..., and for variances
      std::map<int, std::map<int, std::vector<float> > >::iterator varIT;
      varIT=m_varianceInChannels.find(tell1D);

      if(varIT==m_varianceInChannels.end()){

        for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){

          m_varianceInChannels[tell1D][aLink].resize(14);

        }

      }

      // -->--> and now do not separate signals, calculate var for all
      std::map<int, std::map<int, std::vector<float> > >::iterator vAllIT;
      vAllIT=m_varianceAllInChannels.find(tell1D);

      if(vAllIT==m_varianceAllInChannels.end()){

        for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){

          m_varianceAllInChannels[tell1D][aLink].resize(3);

        }

      }

      //  prepare memory for noise if necessary
      if(!m_noiseTell1s.empty()&&!m_noiseALinks.empty()){      

        std::vector<int>::iterator aLIT;
        std::vector<int>::iterator senIT;
        senIT=std::find(m_noiseTell1s.begin(), m_noiseTell1s.end(), tell1D);

        if(senIT!=m_noiseTell1s.end()){

          std::map<int, std::map<int, std::vector<float> > >::iterator noiseIT;
          noiseIT=m_aLinkNoise.find(tell1D);

          if(noiseIT==m_aLinkNoise.end()){

            aLIT=m_noiseALinks.begin();

            for( ; aLIT!=m_noiseALinks.end(); ++aLIT){

              m_aLinkNoise[tell1D][(*aLIT)].resize(32);

            }

          }

          // initialize memory for counters
          std::map<int, std::map<int, std::vector<int> > >::iterator noiseCntIT;
          noiseCntIT=m_aLinkNoiseCnts.begin();

          if(noiseCntIT==m_aLinkNoiseCnts.end()){

            aLIT=m_noiseALinks.begin();

            for( ; aLIT!=m_noiseALinks.end(); ++aLIT){

              m_aLinkNoiseCnts[tell1D][(*aLIT)].resize(32);

            }

          }

        }      

        // collect the noise information from specified aLinks
        std::vector<int>::iterator srcIT, tell1IT;
        srcIT=std::find(m_noiseALinks.begin(), m_noiseALinks.end(), aLink);
        tell1IT=std::find(m_noiseTell1s.begin(), m_noiseTell1s.end(), tell1D);

        if(srcIT!=m_noiseALinks.end()&&tell1IT!=m_noiseTell1s.end()){

          VeloTELL1::scdatIt noiseDatIT=dataITPair.first;
          int pos=0;

          for( ; noiseDatIT!=dataITPair.second; ++noiseDatIT, ++pos){

            m_aLinkNoise[tell1D][aLink][pos]+=
              static_cast<float>((*noiseDatIT)*(*noiseDatIT));
            m_aLinkNoiseCnts[tell1D][aLink][pos]++;           

          }

        }

      }

      // fill the information
      m_varianceAllInChannels[tell1D][aLink][0]+=
        static_cast<float>(data0*data0);
      m_varianceAllInChannels[tell1D][aLink][1]+=
        static_cast<float>(data1*data1);
      m_varianceAllInChannels[tell1D][aLink][2]+=
        static_cast<float>(data15*data15);

      // initialize appropriate number of counters
      std::map<int, std::map<int, std::map<std::string, int> > >::iterator cntMap;
      std::vector<std::string>::const_iterator typeIT=m_meanTypes.begin();
      cntMap=m_counters.find(tell1D);

      if(cntMap==m_counters.end()){

        for( ; typeIT!=m_meanTypes.end(); ++typeIT){

          m_counters[tell1D][aLink][*typeIT]=0;

        }

      }

      // update sum - this will be the mean at the finalize()
      // MAP:
      // Memory Location | Data(Header Type)
      //------------------------------------
      //       0         |    chan0(H)
      //       1         |    chan1(H)
      //       2         |    chan0(HH)
      //       3         |    chan1(HH)
      //       4         |    chan0(HL)
      //       5         |    chan1(HL)
      //       6         |    chan0(L)
      //       7         |    chan1(L)
      //       8         |    chan0(LH)
      //       9         |    chan1(LH)
      //      10         |    chan0(LL)
      //      11         |    chan1(LL)
      //      12         |    chan15(H)
      //      13         |    chan15(L)
      //------------------------------------
      if(strHType3=="H"){

        m_counters[tell1D][aLink]["BaseH"]++;
        m_meanInChannels[tell1D][aLink][CH15_H]+=static_cast<float>(data15);
        m_varianceInChannels[tell1D][aLink][CH15_H]+=static_cast<float>(data15*data15);
        m_counters[tell1D][aLink]["H"]++;
        m_meanInChannels[tell1D][aLink][CH0_H]+=static_cast<float>(data0);
        m_meanInChannels[tell1D][aLink][CH1_H]+=static_cast<float>(data1);
        m_varianceInChannels[tell1D][aLink][CH0_H]+=static_cast<float>(data0*data0);
        m_varianceInChannels[tell1D][aLink][CH1_H]+=static_cast<float>(data1*data1);

        if(strHType2=="H"&&!m_baseLineAlgo){

          m_counters[tell1D][aLink]["HH"]++;
          m_meanInChannels[tell1D][aLink][CH0_HH]+=static_cast<float>(data0);
          m_meanInChannels[tell1D][aLink][CH1_HH]+=static_cast<float>(data1);
          m_varianceInChannels[tell1D][aLink][CH0_HH]+=static_cast<float>(data0*data0);
          m_varianceInChannels[tell1D][aLink][CH1_HH]+=static_cast<float>(data1*data1);

        }else if(strHType2=="L"&&!m_baseLineAlgo){

          m_counters[tell1D][aLink]["HL"]++;
          m_meanInChannels[tell1D][aLink][CH0_HL]+=static_cast<float>(data0);
          m_meanInChannels[tell1D][aLink][CH1_HL]+=static_cast<float>(data1);
          m_varianceInChannels[tell1D][aLink][CH0_HL]+=static_cast<float>(data0*data0);
          m_varianceInChannels[tell1D][aLink][CH1_HL]+=static_cast<float>(data1*data1);

        }

      }else{

        m_counters[tell1D][aLink]["BaseL"]++;
        m_meanInChannels[tell1D][aLink][CH15_L]+=static_cast<float>(data15);
        m_varianceInChannels[tell1D][aLink][CH15_L]+=static_cast<float>(data15*data15);
        m_counters[tell1D][aLink]["L"]++;
        m_meanInChannels[tell1D][aLink][CH0_L]+=static_cast<float>(data0);
        m_meanInChannels[tell1D][aLink][CH1_L]+=static_cast<float>(data1);
        m_varianceInChannels[tell1D][aLink][CH0_L]+=static_cast<float>(data0*data0);
        m_varianceInChannels[tell1D][aLink][CH1_L]+=static_cast<float>(data1*data1);

        if(strHType2=="H"&&!m_baseLineAlgo){

          m_counters[tell1D][aLink]["LH"]++;
          m_meanInChannels[tell1D][aLink][CH0_LH]+=static_cast<float>(data0);
          m_meanInChannels[tell1D][aLink][CH1_LH]+=static_cast<float>(data1);
          m_varianceInChannels[tell1D][aLink][CH0_LH]+=static_cast<float>(data0*data0);
          m_varianceInChannels[tell1D][aLink][CH1_LH]+=static_cast<float>(data1*data1);

        }else if(strHType2=="L"&&!m_baseLineAlgo){

          m_counters[tell1D][aLink]["LL"]++;
          m_meanInChannels[tell1D][aLink][CH0_LL]+=static_cast<float>(data0);
          m_meanInChannels[tell1D][aLink][CH1_LL]+=static_cast<float>(data1);
          m_varianceInChannels[tell1D][aLink][CH0_LL]+=static_cast<float>(data0*data0);
          m_varianceInChannels[tell1D][aLink][CH1_LL]+=static_cast<float>(data1*data1);

        }

      }

    }

  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloBeetleHeaderXTalkComputer::plotHeaders()
{
  if(m_isDebug) debug()<< " --> plotHeaders() " <<endmsg;

  //
  LHCb::VeloTELL1Datas::const_iterator sensIt;
  VeloTELL1::ALinkPair headerITPair;
  // 

  for(sensIt=m_headers->begin(); sensIt!=m_headers->end(); ++sensIt){

    LHCb::VeloTELL1Data* headers=(*sensIt);
    const int tell1=headers->key();
    //std::map<int, TH2D*>::iterator headIT=m_sensHeaders.find(tell1);
    BHXT_TYPES::HM2D_IT headIT=m_sensHeaders.find(tell1);

    // this will allow to book histograms once per job
    if(headIT==m_sensHeaders.end()){

      this->bookHeadersMoniHistograms(tell1);
      headIT=m_sensHeaders.find(tell1);

    }

    VeloTELL1::scdatIt iT;
    unsigned int channel=0;

    for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){

      headerITPair=(*headers)[aLink];
      iT=headerITPair.first;
      //if(*(iT+2)<510){
      //  channel+=3;
      //  continue;
      //}

      for( ; iT!=headerITPair.second; ++iT, ++channel){
        headIT->second->fill(channel, (*iT));

      }

    }

  }
  //
  return;
}
//=============================================================================
void VeloBeetleHeaderXTalkComputer::globalHeadersBaseLine()
{
  if(m_isDebug) debug()<< " --> calculateGlobalBaseLine() " <<endmsg;

  //  get the headers
  if(!exist<LHCb::VeloTELL1Datas>(m_headersLoc)){
    info()<< " --> There is no decoded ADC headers at: "
           << m_headersLoc <<endmsg;
  }else{  
    // get the headers from default TES location
    m_headers=get<LHCb::VeloTELL1Datas>(m_headersLoc);
  }

  LHCb::VeloTELL1Datas::const_iterator hIT;
  VeloTELL1::ALinkPair headerITPair;
  hIT=m_headers->begin();

  // loop over sensors
  for( ; hIT!=m_headers->end(); ++hIT){

    LHCb::VeloTELL1Data* headers=(*hIT);
    const int tell1=headers->key();
    int header3=0;
    
    //  initialize memory for the global headers base line
    std::map<int, float>::iterator blIT;
    blIT=m_globalHeaderBaseLine.find(tell1);
    if(blIT==m_globalHeaderBaseLine.end()){

      m_globalHeaderBaseLine[tell1]=0;

    }

    for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){

      headerITPair=(*headers)[aLink];
      header3=(*(headerITPair.first+3));
      m_globalHeaderBaseLine[tell1]+=header3;

    }
    
  }
  
  if(m_evtNumber==m_convergenceLimit){

    std::map<int, float>::iterator IT;
    IT=m_globalHeaderBaseLine.begin();
    float mean=0.;

    for( ; IT!=m_globalHeaderBaseLine.end(); ++IT){

      mean=IT->second;
      mean/=m_evtNumber*VeloTELL1::NumberOfALinks;
      IT->second=mean;
      if(m_isDebug) debug()<< " Global Header Base Line for Tell1: " 
                           << (IT->first) << " is: " << mean <<endmsg;

    }

  }
  
  //
  return;
}
//--
