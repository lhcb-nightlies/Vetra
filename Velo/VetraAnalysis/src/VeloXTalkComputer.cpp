// $Id: VeloXTalkComputer.cpp,v 1.3 2010-03-25 15:39:04 krinnert Exp $
// Include files 

#include <cmath>
// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

// local
#include "VeloXTalkComputer.h"
// #include <boost/format.hpp>
//-----------------------------------------------------------------------------
// Implementation file for class : VeloXTalkComputer
//
// 2006-07-06 : 
//-----------------------------------------------------------------------------

#include <fstream>

// ===============================================================
// ===============================================================
// ===============================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloXTalkComputer )

using namespace LHCb;
using namespace Gaudi;

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
// body of the veloFIRComputer namespace

VeloXTalkComputer::pedestalFollower::pedestalFollower( ) 
  : m_sum(0)
  , m_sum2(0)
  , m_NN(0)
{}

void VeloXTalkComputer::pedestalFollower::fill( const double X ){
  if( m_NN>100 ){
    if( ( X-mean() )*( X-mean() )/ variance() < 9 ){// less than 3 sigma.
      m_sum += X;
      m_sum2 += X*X;
    } else {
      const double add = 0.*X + 0.9*mean();
      m_sum += add;   
      m_sum2 += add*add;
    }
  } else {
    m_sum += X;   
    m_sum2 += X*X;
  }
  ++m_NN;
}

double VeloXTalkComputer::pedestalFollower::rms() const {
  return std::sqrt( variance() );
}
double VeloXTalkComputer::pedestalFollower::variance() const {
  double n = static_cast<double>(m_NN);
  return m_sum2/n - m_sum*m_sum/(n*n);
}

void VeloXTalkComputer::pedestalFollower::reset(){
  m_sum = m_sum2 = 0;
  m_NN = 0;
}

// ===============================================================
// ===============================================================

VeloXTalkComputer::stat2D::stat2D()
  : m_xSum(0)
  , m_xSum2(0)
  , m_ySum(0)
  , m_ySum2(0)
  , m_xySum(0)
  , m_NN(0)
  , m_ratio(0)
{}

void VeloXTalkComputer::stat2D::fill( const double X, const double Y ){
  m_xSum += X;
  m_xSum2 += X*X;
  m_xySum += X*Y;
  m_ySum += Y;
  m_ySum2 += Y*Y;
  if( X!=0 ) m_ratio += Y/X;
  ++m_NN;
}

double VeloXTalkComputer::stat2D::sxx() const {
  double xm = xMean();
  return m_xSum2 - n() * xm*xm;
} 
double VeloXTalkComputer::stat2D::syy() const {
  double ym = yMean();
  return m_ySum2 - n()* ym*ym;
} 

double VeloXTalkComputer::stat2D::sxy() const {
  return m_xySum - n()*xMean()*yMean();
}

double VeloXTalkComputer::stat2D::correlation() const {
  return sxy()/std::sqrt( sxx() * syy() );
}

double VeloXTalkComputer::stat2D::slope() const {
  return sxy() / sxx();
}


double VeloXTalkComputer::stat2D::intercept() const {
  return yMean() - slope() * xMean();
}

void VeloXTalkComputer::stat2D::reset(){
  m_xSum = m_xSum2 = m_xySum = m_ySum = m_ySum2 =0;
  m_NN = 0;
}

double VeloXTalkComputer::stat2D::s() const {
  if( n() > 2 ){
    return std::sqrt( (syy() - sxy()*sxy()/sxx()) / ( n()-2 ) );
  }
  return std::numeric_limits<double>::quiet_NaN();
}

double VeloXTalkComputer::stat2D::sigmaSlope() const {
  return s() / std::sqrt( sxx() );
}
double VeloXTalkComputer::stat2D::sigmaIntercept() const {
  return s() * std::sqrt( 1.0/n() + xMean()*xMean() / sxx() );
}

double VeloXTalkComputer::stat2D::eval( const double x ) const {
  return x*slope() + intercept();
}

std::ostream& operator<<( std::ostream& os,
			  const VeloXTalkComputer::stat2D& R ){
  os << std::setprecision(3) 
     << "Lin. reg. on " << R.n() << " points.\n" 
     << "  slope: (" << R.slope() << " +- " << R.sigmaSlope() 
     << "); intercept: (" << R.intercept() << " +- " << R.sigmaIntercept() 
     << "), correlation: " << R.correlation() << ", ratio: " << R.ratio() 
     << " means ratio= " << R.yMean() / R.xMean() ;
  return os;
}

std::string VeloXTalkComputer::xTalkValue( const stat2D& r ) {
  std::stringstream str;
  str << std::setprecision(3) << std::fixed << std::showpos;
  double val = r.meanRatio();
  //  if( fabs(val) < 0.001 
  str << val;
  return str.str();
}

// ===============================================================
// ===============================================================
// =============== QuickChannelMap ===============================
// ===============================================================

VeloXTalkComputer::QuickChannelMap::QuickChannelMap( const DeVeloSensor* sen )
  : m_sensor( 0 )
  , m_isInit(false)
{
  if( sen ) init( sen );
}

void VeloXTalkComputer::QuickChannelMap::init( const DeVeloSensor* sen ){
  m_sensor = sen;
  assert( sen && 2048 == sen->numberOfStrips() );
  if( !m_isInit || m_isPhi != sen->isPhi() ) createMap();
}

void VeloXTalkComputer::QuickChannelMap::createMap(){
  swt2strip.resize(2048);
  strip2swt.resize(2048);
  
  for( int cc=0; cc<2048; ++cc ){
    swt2strip[cc] = m_sensor->ChipChannelToStrip( cc );
  }
  // reverse mapping:
  for( int s=0; s<2048; ++s ){
    assert( swt2strip[s]<2048 && swt2strip[s]>=0 );
    strip2swt[ swt2strip[s] ] = s;
  }
  m_isInit = true;
}

// ---------------------------------------------------------------
int VeloXTalkComputer::QuickChannelMap::swtToStrip( const int swtId ) const {
  assert( swtId>=0 && swtId<2048 );
  return swt2strip[ swtId ];
}
int VeloXTalkComputer::QuickChannelMap::stripToSwt( const int strip ) const {
  assert( strip>=0 && strip<2048 );
  return strip2swt[ strip ];
}

// ---------------------------------------------------------------
bool VeloXTalkComputer::QuickChannelMap::isInner( const int cc ) const {
  if( getSensor()->isR() ) return true;
  return swtToStrip( cc ) < 683;
}
bool VeloXTalkComputer::QuickChannelMap::isOuter( const int cc ) const {
  if( getSensor()->isR() ) return true;
  return swtToStrip( cc ) >= 683;
}

// ---------------------------------------------------------------
int VeloXTalkComputer::QuickChannelMap::getCableNeighbor( const int cc
							,const int offset ) const {
  if( cc/32 != (cc+offset)/32 ){
    // the cable is not the same, can one wants to compute xtalk then ???
    return -1;
  }
  
  if( cc+offset > 0 && cc+offset < 2048 ) return cc+offset;
  return -1;

}

// ---------------------------------------------------------------
int VeloXTalkComputer::QuickChannelMap::getStripNeighbor( const int cc
							,const int stripOffset ) const {
  assert( cc >=0 && cc < 2048 );
  const int s1 = swtToStrip( cc );
  

  const int s2 = s1+stripOffset;
  if( s2 < 0 || s2 >= 2048 ) return -1;
  const int cc2 = stripToSwt( s2 );

  if( getSensor()->isPhi() ) return cc2;

  if( s1/512 == s2/512 ){ // it is an R sensor and the same sector
    return cc2;
  }
  return -1;
}

// ---------------------------------------------------------------
int VeloXTalkComputer::QuickChannelMap::getOuterInnerNeighbor( const int cc ) const {
  assert( cc >=0 && cc < 2048 );
  if( getSensor()->isR() ) return -1; // R sensor does not have inner/outer pattern

  const int s1 = swtToStrip( cc );
  if( s1 < 683 ){ // an inner strip always has an outer strip neighbor
    return stripToSwt( 2*s1 + 683 );
  }

  if( !(s1%2) ) return -1; // even outer strip have no inner strip neighbor.
  return stripToSwt( (s1-683)/2 );
}

// ===============================================================
// ===============================================================
// ===============================================================

VeloXTalkComputer::xTester::xTester( const Test& t1, const Test& t2
		  , const Test& t3, const Test& t4
		  , const Test& t5, const Test& t6 )
  : m_quickCM(0)
  , m_totalTested(0)
  , m_totalAccepted(0)
{
  Test* tt;
  if( ( tt = t1.clone() ) ) addTest( tt );
  if( ( tt = t2.clone() ) ) addTest( tt );
  if( ( tt = t3.clone() ) ) addTest( tt );
  if( ( tt = t4.clone() ) ) addTest( tt );
  if( ( tt = t5.clone() ) ) addTest( tt );
  if( ( tt = t6.clone() ) ) addTest( tt );
}
VeloXTalkComputer::xTester::~xTester(){
  clearTests();
}

VeloXTalkComputer::xTester::xTester( const xTester& X )
  : m_quickCM( X.m_quickCM )
  , m_totalTested( X.m_totalTested )
  , m_totalAccepted( X.m_totalAccepted )
{
  clearTests();
  // adding the new tests
  for( int i=0; i<(int)X.tests.size(); ++i ){
    this->addTest( X.tests[i]->clone() );
  }
}

bool VeloXTalkComputer::xTester::operator==( const xTester& r ) const {
  if( tests.size() != r.tests.size() ) return false;
  const int size = tests.size();

  std::set<int> target;

  for( int i=0; i<size; ++i ){
    bool flag = false;
    for( int j=0; j<size; ++j ){
      if( *(tests[i]) == *(r.tests[j]) 
	  &&
	  tests[i]->name() == r.tests[j]->name() ){
	flag=true;
	target.insert(j);
	break;
      }
    }
    if( !flag ) return false;
  }

  if( size != (int)target.size() ) return false;
  return true;
}

// ---------------------------------------------------------------
VeloXTalkComputer::xTester& VeloXTalkComputer::xTester::operator=( const xTester& X ) {
  clearTests();
  // adding the new tests
  for( int i=0; i<(int)X.tests.size(); ++i ){
    this->addTest( X.tests[i]->clone() );
  }
  m_quickCM = X.m_quickCM;
  m_totalTested = X.m_totalTested;
  m_totalAccepted = X.m_totalAccepted;

  return *this;
}

// ---------------------------------------------------------------
void VeloXTalkComputer::xTester::clearTests(){
  for( int i=0; i<(int)tests.size(); ++i ){ 
    delete tests[i]; 
  }
  tests.clear();
}

// ---------------------------------------------------------------
void VeloXTalkComputer::xTester::setQuickMap( const QuickChannelMap* q ){
  m_quickCM = q;
}

// ---------------------------------------------------------------
std::string VeloXTalkComputer::xTester::getName( const char* pre
					       ,const char* post ) const {
  std::stringstream s;
  if( pre ) s << pre << '_';
  for( int i=0; i<(int)tests.size(); ++i ){
    s << tests[i]->name();
    if( i+1<(int)tests.size() ) s << '_';
  }
  if( post ) s << post;
  return s.str();
}

// ---------------------------------------------------------------
bool VeloXTalkComputer::xTester::test( const int beChan
				       ,const int offset ) const {
  ++m_totalTested;
  std::vector<Test*>::const_iterator it;
  for( it=tests.begin(); it!=tests.end(); ++it ){
    if( ! (*it)->operator()( m_quickCM,beChan,offset ) ) return false;
  }
  ++m_totalAccepted;
  return true;
}

// ---------------------------------------------------------------
void VeloXTalkComputer::xTester::reset(){
  m_totalTested = 0;
  m_totalAccepted = 0;
}

// ---------------------------------------------------------------
void VeloXTalkComputer::xTester::addTest( const Test& r ){
  tests.push_back( r.clone() );
}
void VeloXTalkComputer::xTester::addTest( Test* r ){
  // takes the ownership of r.
  tests.push_back( r );
}

// --------------------------------------------------
// int VeloXTalkComputer::xTester::getNPossibleTopologies( const int maxN
// 							,const int tpModulo
// 							,const std::vector<int>& tp ) const {
//   xTester tmp = *this;
//   for( int cc=0; cc<2048; ++cc ){
//     for( int offset=-maxN; offset<=maxN; ++offset ){
//       if( cc!=offset && cc+offset>=0 && cc+offset<2048 ){
// 	if( !tpModulo || tp[ cc%tpModulo ] ){
// 	  if( tmp.test( cc, offset ) ){
// 	  }
// 	}
//       }
//     }
//   }
//   return tmp.getNAccepted();
// }

// ===============================================================
// ===============================================================
// ===============================================================

VeloXTalkComputer::xTalk::xTalk( const Test& t1, const Test& t2
				 ,const Test& t3, const Test& t4
				 ,const Test& t5, const Test& t6 )
  : xTester( t1,t2,t3,t4,t5,t6 )
  , data()
{}

VeloXTalkComputer::xTalk::~xTalk(){}

VeloXTalkComputer::xTalk::xTalk( const xTalk& X ) 
  : xTester( X )
  , data( X.data )
{}

VeloXTalkComputer::xTalk& VeloXTalkComputer::xTalk::operator=( const xTalk& X ){
  this->xTester::operator=( X );
  this->data = X.data;
  return *this;
}

void VeloXTalkComputer::xTalk::reset(){
  xTester::reset();
  data.reset();
}

// ===============================================================
// ===============================================================
// ===============================================================

VeloXTalkComputer::Tell1Handler::Tell1Handler()
  : maxVisitedCableNeighbor(0)
  , m_parent(0)
  , m_tell1Number( 0 )
  , m_data(0)
  , m_quickCM(0)
{}

VeloXTalkComputer::Tell1Handler::Tell1Handler( const VeloXTalkComputer* parent
					     ,const int tell1Id )
  : maxVisitedCableNeighbor( 0 )
  , m_parent(parent)
  , m_tell1Number( tell1Id )
  , m_data(0)
  , m_quickCM(0)
{
  if( m_parent && m_parent->m_shiftThreshold ) m_ped.resize( 2048 );
}

// ---------------------------------------------------------------
void VeloXTalkComputer::Tell1Handler::computePedestal( const VeloTELL1::sdataVec& data ) {
  if( data.size() == m_ped.size() ){
    const int size = m_ped.size();
    for( int i=0; i<size; ++i ){
      m_ped[i].fill( data[i] );
    }
    return;
  }
  m_parent->error() << "Pedestal and data vector do not have the same size!" 
		    << " Giving up ped computation." << endmsg;
}

// ---------------------------------------------------------------
void VeloXTalkComputer::Tell1Handler::add( const VeloXTalkComputer::xTalk& x ){
  xtalks.push_back( x );
  xtalks.back().setQuickMap( m_quickCM );
}

// ---------------------------------------------------------------
// ---------------------------------------------------------------

bool VeloXTalkComputer::Tell1Handler::prepareSignalSeek( const VeloTELL1::sdataVec* data ){

  assert( m_parent && data ); // should never happend...
  m_parent->verbose() << "Preparing Tell1Handler " << getTell1Id() 
		      << " data size is " << data->size() << endmsg;

  if( m_parent->m_shiftThreshold && m_ped.size() != data->size() ){
    m_parent->error() << "Pedestal and data vector are not the same size "
		      << "for Tell1 " << getTell1Id() <<  endmsg;
    return false;
  }
  
  m_data = data;

  return true;
}
// ---------------------------------------------------------------
// ---------------------------------------------------------------

void VeloXTalkComputer::Tell1Handler::setQuickMap( const DeVeloSensor* sen ){
  // check the sensor type. Init the map if it is the first call. Associate the ptr.
  m_quickCM = m_parent->getQuickMap( sen );

}

// ---------------------------------------------------------------
bool VeloXTalkComputer::Tell1Handler::testForSignalInCableNeighbors( const int signalCC
								   , const int cc
								   , const float noiseThreshold
								   , const bool shiftMean ) const {
  int cc1;
  double mean;
  cc1 = m_quickCM->getCableNeighbor( cc, -1 );
  if( cc1 >= 0 && signalCC!=cc1 ){
    if( shiftMean ) mean =  m_ped[cc1].mean();
    else mean = 0;
    if( fabs( m_data->operator[](cc1) - mean ) > noiseThreshold ) return true;
  }
  
  cc1 = m_quickCM->getCableNeighbor( cc, +1 );
  if( cc1 >= 0 && signalCC!=cc1 ){
    if( shiftMean ) mean =  m_ped[cc1].mean();
    else mean = 0;

    if( fabs( m_data->operator[](cc1) - mean ) > noiseThreshold ) return true;
  }
  return false;
}

// ---------------------------------------------------------------
bool VeloXTalkComputer::Tell1Handler::testForSignalInStripNeighbors( const int signalCC 
								   , const int cc
								   , const float noiseThreshold
								   , const bool shiftMean ) const {
  int cc1;
  double mean;
  cc1  = m_quickCM->getStripNeighbor( cc, -1 );
  if( cc1 >= 0 && signalCC!=cc1 ){
    if( shiftMean ) mean =  m_ped[cc1].mean();
    else mean = 0;
    if( fabs( m_data->operator[](cc1) - mean ) > noiseThreshold ) return true;
  }

  cc1  = m_quickCM->getStripNeighbor( cc, +1 );
  if( cc1 >= 0 && signalCC!=cc1 ){
    if( shiftMean ) mean =  m_ped[cc1].mean();
    else mean = 0;
    if( fabs( m_data->operator[](cc1) - mean ) > noiseThreshold ) return true;
  }

  cc1 = m_quickCM->getOuterInnerNeighbor( cc );
  if( cc1 >= 0 && signalCC!=cc1 ){
    if( shiftMean ) mean =  m_ped[cc1].mean();
    else mean = 0;
    if( fabs( m_data->operator[](cc1) - mean ) > noiseThreshold ) return true;
  }
  return false;
}

// ---------------------------------------------------------------
bool VeloXTalkComputer::Tell1Handler::testXTalk( const int idx, const int cc, const int offset ) const {
  assert( idx>=0 && idx<(int)xtalks.size() );
  return xtalks[idx].test( cc, offset );
}

// ---------------------------------------------------------------
bool VeloXTalkComputer::Tell1Handler::seekNextSignal( int& signalCC ) const {

  const VeloTELL1::sdataVec& data = *m_data;
  const int size = data.size();
 
  // loop on the remaining chip channels
  for( int cc=signalCC+1; cc<size; ++cc ){

    // test if the strip is read out:
    if( getQuickMap()->getSensor()->OKStrip( getQuickMap()->swtToStrip(cc) ) ){

      // test the signal window
      float testVal = data[cc];
      const bool shift = m_parent->m_shiftThreshold;
      if( shift ) testVal -= m_ped[ cc ].mean();
      if(
	 ( !m_parent->m_discardPositive 
	   && testVal >= m_parent->m_signalMin
	   && testVal <= m_parent->m_signalMax )
	 ||
	 ( !m_parent->m_discardNegative
	 && testVal <= -m_parent->m_signalMin
	   && testVal >= -m_parent->m_signalMax )
	 ) {
	
	if( m_parent->m_testPulsePatternModulo
	    &&
	    m_parent->m_tpMask[ cc % m_parent->m_testPulsePatternModulo ] ){ 
	  // testpulse always true
	  signalCC = cc;
	  return true;
	}
	
	// test that no signal occures in neighbor channels
	if( !m_parent->m_testPulsePatternModulo ){
	  // m_parent->verbose() << "Channel " << cc << " testing ADC val " << testVal << endmsg;
	  bool cableOk(false), sensorOk(false);
	  cableOk = ( !m_parent->m_avoidCableDoubleHit 
		      ||
		      !testForSignalInCableNeighbors( cc, cc,m_parent->m_noiseMax, shift ) );
	  
	  // m_parent->verbose() << "Cable OK : " << cableOk << endmsg;
	  
	  if( cableOk ){
	    sensorOk = ( !m_parent->m_avoidSensorDoubleHit
			 || 
			 !testForSignalInStripNeighbors( cc, cc,m_parent->m_noiseMax, shift ) );
	    // m_parent->verbose() << "Sensor OK : " << cableOk << endmsg;
	    if( sensorOk ){
	      m_parent->verbose() << "Found a valid signal in chip channel " << cc << endmsg;
	      signalCC = cc;
	      return true;
	    }
	  }
	}
      }
    }
  }
  return false;
}

// ---------------------------------------------------------------
void VeloXTalkComputer::Tell1Handler::testNoisesCC( const int signalCC ) {
  
  std::vector< VeloXTalkComputer::xTalk >::iterator xtIt;
  const int maxN = this->maxVisitedCableNeighbor;

  // loop on the noise channel
  for( xtIt = xtalks.begin(); xtalks.end() != xtIt; ++xtIt ){

    for( int offset = -maxN; maxN>=offset; ++offset ){
      if( offset!=0 && signalCC+offset>=0 && signalCC+offset<2048 ){
	// test if this pair config (signalCC,signalCC+offset) must be
	// considered by the xTalk object.

	// test if the strip is read out.
	if( getQuickMap()->getSensor()->OKStrip( getQuickMap()->swtToStrip(signalCC+offset) ) ){
	  
	  if( !m_parent->m_skipFirstChannel || (signalCC+offset)%32 != 0 ){
	    
	    float noise = m_data->operator[](signalCC+offset);
	    const bool shift = m_parent->m_shiftThreshold;
	    if( shift ) noise -= m_ped[ signalCC+offset ].mean();
	    
	    if( fabs(noise) < m_parent->m_noiseMax 
		&& 
		xtIt->test( signalCC, offset ) ){
	      
	      // ensure no signal in the neighbors around the noise
	      bool cableOk(false), sensorOk(false);
	      cableOk = ( !m_parent->m_avoidCableDoubleHit 
			  ||
			  !testForSignalInCableNeighbors( signalCC, signalCC+offset
							  ,m_parent->m_noiseMax 
							  ,shift ) 
			  );
	      if( cableOk ){
		
		sensorOk = ( !m_parent->m_avoidSensorDoubleHit
			     || 
			     !testForSignalInStripNeighbors( signalCC, signalCC+offset
							     ,m_parent->m_noiseMax
							     ,shift ) 
			     );
		if( sensorOk ){
		  
		  // we have a valid combination... 
		  if( m_parent->m_debugMode ){
		    m_parent->debug() << "Found a valid pair "
				      << "signal[" << signalCC << "] = " 
				      << m_data->operator[]( signalCC ) 
				      << ", noise[" << signalCC+offset << "] = "
				      << m_data->operator[]( signalCC+offset )
				      <<  endmsg;
		  }
		  
		  xtIt->data.fill( m_data->operator[]( signalCC )
				   , m_data->operator[]( signalCC+offset ) ) ;
		  
		  // do some plot in debug mode:
		  if( m_parent->m_debugMode ){ 
		    int o=0;
		    if( shift ) o=512;
		    
		    std::stringstream name;
		    name << "Tell1_" << getTell1Id() << xtIt->getName();
		    
		    double minX = static_cast<int>(m_parent->m_signalMin-5 +o);
		    double maxX = static_cast<int>(m_parent->m_signalMax+5 +o);
		    int binX = static_cast<int>(maxX - minX+1);
		    
		    double minY = static_cast<int>(-m_parent->m_noiseMax - 5 +o);
		    double maxY = minY + maxX-minX;
		    
		    int binY = static_cast<int>(maxY - minY+1);
		    
		    m_parent->plot2D( m_data->operator[]( signalCC )
				      , m_data->operator[]( signalCC+offset )
				      , name.str(), name.str(), minX, maxX
				      , minY, maxY, binX, binY );
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

// ---------------------------------------------------------------


StatusCode VeloXTalkComputer::Tell1Handler::analyseData( const VeloTELL1::sdataVec* data ) {

  if( !prepareSignalSeek( data ) ){
    return StatusCode::FAILURE;
  }

  //   // ---------------------------------------------------------------
  //   // cheat the signal values:
  //   sdataVec& ss = const_cast<sdataVec&>( *data );
  //   std::fill( ss.begin(), ss.end(), 0 );
  //   assert( ss.size() == 2048 );
  //   double xt[] = { 0.01, -0.02, 0.03, -0.08, 1, 0.08, -0.03, 0.02, -0.01 };
  //   //                -4    -3    -2     -1   0   1      2     3       4
  
  //   for( int i=15; i<2048; i+=32 ){
  //     //int adcVal = static_cast<int>( (double)(rand())/(double)(RAND_MAX)*15+30);
  //     int adcVal = 35;
  //     for( int kk = i-4; kk<=i+4; ++kk ){
  //       if( kk>=0 && kk<2048 ){
  // 	//	std::cout << " offset " << kk-i+4 << " : " << xt[kk-i+4] << std::endl;
  // 	ss[ kk ] = static_cast<int>( roundl(adcVal * xt[kk-i+4]) ); 
  //       }
  //     }
  //   }
  //   // ---------------------------------------------------------------

  int signalCC = -1;
  while( seekNextSignal( signalCC ) ){
    // found a valid signal
    // will test all the xtalk and the noise.
    testNoisesCC( signalCC );
  }
  return StatusCode::SUCCESS;
}

// ---------------------------------------------------------------
std::vector< std::vector< double > > VeloXTalkComputer::Tell1Handler::getCableXTalk() const {
  
  const int nMax = this->maxVisitedCableNeighbor;

  std::vector< std::vector< double > > ret;
  ret.resize( 2*nMax+1, std::vector<double>(64) );
  std::vector< VeloXTalkComputer::xTalk >::const_iterator cit;;

  for( int off = -nMax; off<=nMax; ++off ){
    for( int cbl=0; cbl<64; ++cbl ){
      cit = std::find( xtalks.begin(), xtalks.end(), xTalk( Cable(cbl), ConstCCOffset(off) ) );
      if( cit != xtalks.end() ){
	if( cit->data.n() > m_parent->m_minXTalkPoints ){
	  ret[off+nMax][cbl] = cit->data.meanRatio();
	}
      }
    }
  }
  return ret;
}
// ---------------------------------------------------------------
std::vector< std::vector< double > > VeloXTalkComputer::Tell1Handler::getTell1XTalk() const {
  const int nMax = this->maxVisitedCableNeighbor;

  std::vector< std::vector< double > > ret;
  ret.resize( 2*nMax+1, std::vector<double>(64) );
  std::vector< VeloXTalkComputer::xTalk >::const_iterator cit;;

  for( int off = -nMax; off<=nMax; ++off ){
    cit = std::find( xtalks.begin(), xtalks.end(), xTalk( ConstCCOffset(off) ) );
    if( cit != xtalks.end() ){
      if( cit->data.n() > m_parent->m_minXTalkPoints ){
	for(int cbl=0; cbl<64; ++cbl ) ret[off+nMax][cbl] = cit->data.meanRatio();
      }
    }
  }
  return ret;
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloXTalkComputer::VeloXTalkComputer( const std::string& name,
				  ISvcLocator* pSvcLocator)
  //: VeloTELL1Algorithm( name , pSvcLocator )
  //: GaudiAlgorithm( name , pSvcLocator )
  : GaudiHistoAlg( name , pSvcLocator )
  , m_inputData(0)
  , m_eventCounter(0)
  , m_nSkippedEvents(0)
{

  // Min amplitude to have a signal
  declareProperty( "SignalMin", m_signalMin = 20 );
  // Max amplitude to have a signal
  declareProperty( "SignalMax", m_signalMax = 80 );
  // Max amplitude allowed in the non-pulsed strips.
  declareProperty( "NoiseMax" , m_noiseMax = 10 );

  // If true, SignalMin/Max and NoiseMax are considered as offset wrt the pedestal.
  // If false, they are absolute ADC scales.
  declareProperty( "ShiftThresholdsToMeanStripValue" , m_shiftThreshold = false );

  // Allow or reject negative pulse as signal.
  declareProperty( "DiscardNegativeSignal", m_discardNegative = true );
  declareProperty( "DiscardPositiveSignal", m_discardPositive = false );

  // The place to grab the data. Look at $VELOTELL1EVENTROOT/Event/VeloTELL1Data.h 
  // for other typical locations
  declareProperty("InputDataLoc", m_inputDataLoc = VeloTELL1DataLocation::CMSuppressedADCs );
  
  // Number of events requires to let the pedestal value converge.
  // one day, this should be taken directly from the VeloTELL1Pedestal object somewhere.
  declareProperty("ConvergenceLimit", m_nSkippedEvents = 3000 );

  // These two properties are mutually exclusive. Setting both of them 
  // in one job throw a fatal error.
  // If none of them are set, all the available Tell1 are analysed.
  declareProperty("AcceptedTell1s", m_forceAcceptedT1 );
  declareProperty("RejectedTell1s", m_forceRejectedT1 );

  // Define the testpulse mask. Write is like xx.TestPulses = { 3,12 } to 
  // see chip channel 3 and 12 pulsed.
  declareProperty( "TestPulses", m_tpMask );

  // In case you use a test pulses on a full Beetle (128)
  declareProperty( "TestPulsesAreModulo", m_testPulsePatternModulo=32 );

  // Skip the cable channel 0, it avoid considering the header x-talk. 
  declareProperty( "SkipFirstCableChannel", m_skipFirstChannel=false );

  // Check for every noise - signal pair that their neighbors in the
  // cable have no signal
  declareProperty( "AvoidCableDoubleHit", m_avoidCableDoubleHit=true );
  // Check for every noise and signal pair that their neighbors in the
  // sensor have no signal
  declareProperty( "AvoidSensorDoubleHit", m_avoidSensorDoubleHit=true );

  // StripXTalk : x-talk N-4,...,N+4 on a per-strip basis.
  // PhiSensorXTalk : test combination like neighbor strip, non-neighboring chip channels.
  //              the names at the end are supposed to be self-explanatory

  // CableXTalk = n: Analyses the x-talk in the cable for N-n,N-n+1...,N-1,N+1,...N+n
  //              per-cable based.
  declareProperty( "CableXTalk",  m_cableXTalk = 2 );
  // Tell1XTalk = n: Analyses the x-talk in the tell1s for N-n,N-n+1...,N-1,N+1,...N+n
  //              per-tell1 based (in fact a mean value of the per-cable base).
  declareProperty( "Tell1XTalk",  m_tell1XTalk = 0 );
  // SensorXTalk : Analyses the x-talk in a phi-sensor.
  declareProperty( "PhiSensorXTalk", m_sensorXTalk = false );
  // StripXTalk=n: will create 2047*2*n x-talk watcher, use with care. 
  declareProperty( "StripXTalk",  m_stripXTalk = 0 );

  // Write a text files (with filename OutputOptionFilename) that can
  // be cut and pasted to make a VeloTELL1FIRCableFIRFilter instance
  // with the correct x-talk coef. Only the cable x-talk that are
  // available will be considered. E.g, if CableXTalk==0 then all the
  // x-talk coef will be 0.
  declareProperty( "OutputOptionFilename", m_outputOptionFileName );

  // The minimum number of points to trust an x-talk value. 
  // Values based on less points are set to 0.
  declareProperty( "MinXTalkPoints", m_minXTalkPoints=50 );

  // Produces plots noise versus signal for all the analyzed
  // x-talks. Spits a lot during the run. Takes quite a lot of resources.
  declareProperty( "DebugMode", m_debugMode=false );

  // For debug purpose, analyses only one cable (from 0 to 63)
  declareProperty( "UniqueCable", m_uniqueCable=-1 );
  // For debug purpose, analyses only one offset wrt the signal chip channel
  declareProperty( "UniqueOffset", m_uniqueOffset=0 );

  // Dump a gnuplot script to plot the cable x-talk that have been
  // computed.
  declareProperty( "GnuplotFile", m_gnuplotDump );
}

//=============================================================================
// Destructor
//=============================================================================
VeloXTalkComputer::~VeloXTalkComputer() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloXTalkComputer::initialize() {
  //  StatusCode sc = VeloTELL1Algorithm::initialize(); // must be executed first
  //  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  // getting the velo
  m_velo = getDet<DeVelo>( DeVeloLocation::Default );
  if( !m_velo ) Error("Cannot retrieve the velo description.");

  std::vector<unsigned int> t1List;
  sc = getAnalysedTell1List( m_velo, t1List );
  if( !sc ) return StatusCode::SUCCESS; // nothing to do.

  // interpert the test-pulse mask: put a 1 at every place we have a pulse.
  readTestPulsePattern();

  initializeXTalkers( t1List );

  if( m_debugMode ) dumpPossibleXTalkCombination();
  
  debug() << "Initialize done." << endmsg;
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloXTalkComputer::finalize() {

  info() << "==> Finalize" << endmsg;

  // well must print out all the info ...
  int nodata = 0;

  std::stringstream str;
  str << "Results for the x-talks computation\n";

  std::map< unsigned int, VeloXTalkComputer::Tell1Handler >::const_iterator cit;
  for( cit = m_sensorxt.begin(); cit != m_sensorxt.end(); ++cit ){
    const VeloXTalkComputer::Tell1Handler* t1 = &(cit->second);
    str << "**************************************************\n";
    str << "Results for Tell1 " << cit->first << "\n";
    str << std::fixed;
    
    for( int i=0; i<(int)(t1->xtalks.size()); ++i ){
      std::stringstream name;
      name << "   Tell1_" << cit->first << t1->xtalks[i].getName();
      if( t1->xtalks[i].data.n() > 0 ){
	str << std::setw(25) << std::left << name.str() << ": " 
	    << xTalkValue( t1->xtalks[i].data );
	str << " (" << static_cast<int>(t1->xtalks[i].data.n()) << " points)";
	if( t1->xtalks[i].data.n() <= m_minXTalkPoints ) str << " <- discarded";
	str << "\n";

      } else {
	if( msgLevel( MSG::DEBUG ) ){
	  str << name.str() << " had no data (0 points)\n";
	}
	++nodata;
      }
    }
  }

  info() << str.str() << endmsg;

  if( nodata ){
    warning() << nodata 
	      << " xtalk coeficient in this Tell1 have not selected any event" << endmsg;
  }
  

  dumpXTalkCoeficientForOptionsFile();
  if( !m_gnuplotDump.empty() ) dumpForGnuplot();

  return GaudiHistoAlg::finalize();  // must be called after all other actions
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloXTalkComputer::execute() {

  debug() << "==> Execute" << endmsg;

  ++m_eventCounter;

  StatusCode sc = StatusCode::SUCCESS;
  sc = getData();
  if( !sc ) return StatusCode::SUCCESS;

  // std::vector< Tell1Handler >::iterator it;

  // loopping on each TELL1 data:
  LHCb::VeloTELL1Datas::const_iterator cit;
  for( cit = m_inputData->begin(); m_inputData->end()!=cit; ++cit ){

    const int currentTell1Id = (*cit)->key();
    verbose() << "Current Tell1 is " << currentTell1Id << endmsg;
    const DeVeloSensor* sen = m_velo->sensorByTell1Id( currentTell1Id );
    if( !sen ){
      error() << "Can't retrieve the sensor associated to Tell1 "
	      << currentTell1Id << endmsg;
      return StatusCode::FAILURE;
    }

    if( m_sensorxt.count( currentTell1Id ) == 0 ) continue;
    Tell1Handler& XT = m_sensorxt[currentTell1Id];

    verbose() << "Tell1Handler " << currentTell1Id << " successfully retrieved" << endmsg;

    VeloTELL1::sdataVec& data = (*cit)->data(); // all the data for this Tell1

    if( m_shiftThreshold ) XT.computePedestal( data );
    
    if( m_eventCounter < m_nSkippedEvents ){
      // just computes some base stat for hit threshold.
      verbose() << "Skipping the events accroding to NumberOfSkippedEvents=" 
		<< m_nSkippedEvents << endmsg;
      continue;
    }

    XT.analyseData( &data );
  }

  verbose() << "Execute done!" << endmsg;
  
  return StatusCode::SUCCESS;
}


// ===============================================================
// ===============================================================

StatusCode VeloXTalkComputer::getData() {
  debug()<< " ==> getData() " <<endmsg;
  //
  if(!exist<LHCb::VeloTELL1Datas>(m_inputDataLoc)){
    info()<< " ==> There is no input data: "
	  << m_inputDataLoc <<endmsg;
    return ( StatusCode::FAILURE );
  } else {  
    // get data banks from default TES location
    m_inputData = get<LHCb::VeloTELL1Datas>(m_inputDataLoc);
    verbose()<< " ==> The data have been read-in from location: "
           << m_inputDataLoc
           << ", size of the data container: "
           << m_inputData->size() <<endmsg;
 
  }
  //
  return ( StatusCode::SUCCESS );
}

// ===============================================================
// ===============================================================

StatusCode VeloXTalkComputer::getAnalysedTell1List( const DeVelo* velo
						  , std::vector<unsigned int>& t1List ) {
  if( !m_forceAcceptedT1.empty() && !m_forceRejectedT1.empty() ){
    fatal() << "One cannot set simultaneously the AcceptedTell1Ids and the RejectedTell1Ids properties" << endmsg;
    return StatusCode::FAILURE;
  }
  t1List.clear();

  std::vector<unsigned int> existingT1;
  std::vector< DeVeloSensor* >::const_iterator it;
  for( it = velo->sensorsBegin(); it != velo->sensorsEnd(); ++it ){
    unsigned int tell1Id;
    if( !velo->tell1IdBySensorNumber( (*it)->sensorNumber(), tell1Id ) ){
      warning() << "No Tell1 associated to sensor " << (*it)->sensorNumber() 
		<< ". Skipping it!" << endmsg;
      continue;
    };
    if( ! (*it)->isReadOut() ){
      warning() << "Sensor " << (*it)->sensorNumber() << " (tell1 " 
		<< tell1Id 
		<< ") is not read out. Skipping it!" << endmsg;
      continue;
    };
    existingT1.push_back( tell1Id );
  }

  // work with sorted range:
  std::sort( existingT1.begin(), existingT1.end() );
  std::sort( m_forceAcceptedT1.begin(), m_forceAcceptedT1.end() );
  std::sort( m_forceRejectedT1.begin(), m_forceRejectedT1.end() );

  std::back_insert_iterator< std::vector<unsigned int> > ii( t1List );

  if( !m_forceAcceptedT1.empty() ){
    std::set_intersection( existingT1.begin(), existingT1.end()
			   ,m_forceAcceptedT1.begin(), m_forceAcceptedT1.end()
			   ,ii ); 
  }

  if( !m_forceRejectedT1.empty() ){
    std::set_difference( existingT1.begin(), existingT1.end()
			 ,m_forceRejectedT1.begin(), m_forceRejectedT1.end()
			 ,ii );
  }

  if(  m_forceAcceptedT1.empty() && m_forceRejectedT1.empty() ){
    t1List = existingT1;
  }

  if( t1List.empty() ){
    std::stringstream s;
    std::copy(existingT1.begin(), existingT1.end(), std::ostream_iterator<int>(s, " ") );
    warning() << "No Tell1 to analyze! The following Tell1 exists in these data: " << endmsg;
    warning() << s.str() << endmsg;
    warning() << "The algorithm will do nothing in a rather inefficient way..." << endmsg;
    return StatusCode::FAILURE;
  }

  std::vector<unsigned int>::const_iterator t1;
  std::stringstream s;
  for( t1=t1List.begin(); t1!=t1List.end(); ++t1 ){
    s << *t1 << "(" << (velo->sensorByTell1Id( *t1 )->isPhi()?"P":"R") << ") ";
  }
  info() << "The following Tell1 will be included in the x-talk computation:" << endmsg;
  info() << s.str() << endmsg;

  return StatusCode::SUCCESS;
}

// ===============================================================
// ===============================================================
void VeloXTalkComputer::readTestPulsePattern() {
  // interpert the test-pulse mask: put a 1 at every place we have a pulse.
  if( !m_tpMask.empty() ){
    if( m_testPulsePatternModulo<1 || m_testPulsePatternModulo>2047 ) 
      m_testPulsePatternModulo = 2047;
    std::vector<unsigned int> temp;
    temp.resize( m_testPulsePatternModulo );
    std::fill( temp.begin(), temp.end(), 0 ); // initialize with 0
    for( int i=0; i<(int)m_tpMask.size(); ++i ){
      temp[ m_tpMask[i] % m_testPulsePatternModulo ] = 1;
    }
    m_tpMask = temp;
  } else {
    m_testPulsePatternModulo=0;
  }

  if( m_testPulsePatternModulo ){
    std::stringstream s;
    std::copy( m_tpMask.begin(), m_tpMask.end(), std::ostream_iterator<int>(s) );
    info() << "The test pulse pattern is " << s.str() << endmsg;
  } else {
    debug() << "No test pulse pattern given" << endmsg;
  }
    
}

// ===============================================================
// ===============================================================

VeloXTalkComputer::QuickChannelMap* VeloXTalkComputer::getQuickMap( const DeVeloSensor* sen ) const {
  QuickChannelMap* cm(0); // will create the quick map the at the first call.
  if( sen->isR() ){
    m_rsensor.init( sen );
    cm = & m_rsensor;
  } else {
    m_phisensor.init( sen );
    cm = & m_phisensor;
  }
  return cm;
}

void VeloXTalkComputer::dumpPossibleXTalkCombination() const {
  info() << "Here are the pairs of chip channels and strips"
	 << " that will be accepted by your x-talk config." << endmsg;

  std::map< unsigned int, VeloXTalkComputer::Tell1Handler >::const_iterator cit;
  for( cit=m_sensorxt.begin(); cit!=m_sensorxt.end(); ++cit ){
    const VeloXTalkComputer::Tell1Handler& TH = cit->second;

    info() << endmsg;
    info() << "***** For Tell1 " << TH.getTell1Id() 
	   << ", sensor " << TH.getSensor()->sensorNumber() 
	   << ", " << "isPhi=" << ((TH.getSensor()->isPhi())?"true":"false")
	   << endmsg;

    const int maxN = TH.maxVisitedCableNeighbor;    

    for( int idx=0; idx<(int)TH.xtalks.size(); ++idx ){
      xTalk xt = TH.xtalks[idx];

      info()  << "***** Pair possibilities (signal_cc,noise_cc),(signal_strip,noise_strip) for  "
	      << xt.getName() << endmsg;

      for( int cc=0; cc<2048; ++cc ){
        for( int offset=-maxN; offset<=maxN; ++offset ){
          if( cc!=offset && cc+offset>=0 && cc+offset<2048 ){
            if( !m_testPulsePatternModulo || m_tpMask[ cc%m_testPulsePatternModulo ] ){
	      if( !m_skipFirstChannel || (cc+offset)%32!=0 ){
		if( xt.test( cc, offset ) ){
		  info() << " |  (cc " << std::setw(4) << cc << ", "
			 << std::setw(4) << cc+offset << "), (strip "
			 << std::setw(4) << TH.getQuickMap()->swtToStrip(cc)
			 << ", " 
			 << std::setw(4) << TH.getQuickMap()->swtToStrip(cc+offset)
			 << ")" << endmsg;
		}
	      }
            }
          }
        }
      }
      std::string str;
      if( m_testPulsePatternModulo ) str = " (including the tp mask)";
      info() << "This xTalk config has " << xt.getNAccepted() 
	     << " topological possible combinations" << str << endmsg;
    }
  }
}

// ===============================================================
void VeloXTalkComputer::initializeXTalkers( const std::vector<unsigned int>& t1List ) {

  for( int i=0; i<(int)t1List.size(); ++i ){
    unsigned int tell1Id = t1List[i];    
    const DeVeloSensor* sen = m_velo->sensorByTell1Id( tell1Id );

    if( ! sen->isReadOut() ){
      // has actually already been checked.
      warning() << "Sensor " << sen->sensorNumber() << "(tell1 " << tell1Id
		<< ") is not read out. Skipping it." << endmsg;
      continue;
    }
    
    debug() << "Initializing sensor " << std::setw(3) <<  sen->sensorNumber()
	    << " (tell1 " << std::setw(3) << tell1Id << ")" << endmsg;

    if( m_cableXTalk==0 && m_tell1XTalk==0 && m_stripXTalk==0 && m_sensorXTalk==false ){
      error() << "No x-talk to compute, Check your options." << endmsg;
      return;
    }

    // must have one Tell1Handler per sensor/tell1 couple.
    Tell1Handler oneSensor( this, tell1Id );
    oneSensor.setQuickMap( sen );

    bool test = false;
    if( m_cableXTalk > 0 ) test |= initializeCableXTalkers( oneSensor, m_cableXTalk );
    if( m_tell1XTalk > 0 ) test |= initializeTell1XTalkers( oneSensor, m_tell1XTalk );
    if( m_stripXTalk > 0 ) test |= initializeStripXTalkers( oneSensor, m_stripXTalk );
    if( m_sensorXTalk && sen->isPhi() ) test |= initializePhiSensorXTalkers( oneSensor );

    if( !test ){
      warning() << "One of the x-talk options could not create any x-talk watcher in Tell1 "
		<< tell1Id << ", probably some incoherence like asking for phi-sensor x-talk with"
		<< " only r-sensor in the Tell1 list." << endmsg; 
    }
    
    // insert the object into the map.
    m_sensorxt[tell1Id] = oneSensor;
  }
}

// ===============================================================
bool VeloXTalkComputer::initializeCableXTalkers( VeloXTalkComputer::Tell1Handler& t
						 ,const int mN ){
  if( t.maxVisitedCableNeighbor < mN ) t.maxVisitedCableNeighbor = mN;
  
  verbose() << "Initializing cable x-talk watcher up to neighbor +-"
	    << mN << " for Tell1 " << t.getTell1Id() << endmsg;

  bool flag = false;
  for( int cable=0; cable<64; ++cable ){
    if( m_uniqueCable>=0 &&  m_uniqueCable != cable ) continue;
    for( int i=-mN; i<=mN; ++i ){
      if( i!=0 
	  &&
	  ( m_uniqueOffset == 0 || m_uniqueOffset == i ) ){
	t.add( xTalk( Cable(cable), ConstCCOffset(i) ) );
	flag = true;
      }
    }
  }
  return flag;
}

// --------------------------------------------------
bool VeloXTalkComputer::initializeStripXTalkers( VeloXTalkComputer::Tell1Handler& t
						 ,const int mN ){
  if( t.maxVisitedCableNeighbor < mN ) t.maxVisitedCableNeighbor = mN;

  bool flag = false;
  for( int cc=0; cc<2048; ++cc ){
    if( m_uniqueCable>=0 &&  m_uniqueCable != static_cast<int>(cc/32) ) continue;
    for( int i=-mN; i<=mN; ++i ){
      if( i!=0 
	  &&
	  ( m_uniqueOffset == 0 || m_uniqueOffset == i )
	  &&
	  cc+i>=0 && cc+i<2048 ){
	t.add( xTalk( ChipChannel(cc) , ConstCCOffset(i) ) );
	flag = true;
      }
    }
  }
  return flag;
}

// --------------------------------------------------
bool VeloXTalkComputer::initializeTell1XTalkers( VeloXTalkComputer::Tell1Handler& t
						 ,const int mN ){
  if( t.maxVisitedCableNeighbor < mN ) t.maxVisitedCableNeighbor = mN;
  
  bool flag = false;
  for( int i=-mN; i<=mN; ++i ){
    if( i!=0 
	&&
	( m_uniqueOffset == 0 || m_uniqueOffset == i ) ){
      t.add( xTalk( ConstCCOffset(i) ) );
      flag = true;
    }
  }
  return flag;
}

// --------------------------------------------------
bool VeloXTalkComputer::initializePhiSensorXTalkers( VeloXTalkComputer::Tell1Handler& t ) {
  if( t.maxVisitedCableNeighbor < 4 ) t.maxVisitedCableNeighbor = 4;
  int mN = 4;

  for( int i=-mN; i<=mN; ++i ){
    if( i!=0 
	&&
	( m_uniqueOffset == 0 || m_uniqueOffset == i ) ){
      t.add( xTalk( ConstCCOffset(i), Not(NeighborStrip()) ) );
    }
  }
  
  t.add( xTalk( OuterStrip(), Not(ConsecutiveCC()), Or( ConstStripOffset(1), ConstStripOffset(-1) ) ) );
  t.add( xTalk( InnerStrip(), Not(ConsecutiveCC()), Or( ConstStripOffset(1), ConstStripOffset(-1) ) ) );
  t.add( xTalk( Not(ConsecutiveCC()), InnerStrip(), InnerOuter() ) );

  //   t.add( xTalk( ConstCCOffset(2), Or( ConstStripOffset(2), ConstStripOffset(-2) ) ) );
  //   t.add( xTalk( ConstCCOffset(-2), Or( ConstStripOffset(2), ConstStripOffset(-2) ) ) );

  return true;
}

// ===============================================================
void VeloXTalkComputer::dumpXTalkCoeficientForOptionsFile() const {
  if( m_outputOptionFileName.empty() ) return;
  std::ofstream out( m_outputOptionFileName.c_str() );

  always() << "Writing the x-talk coefficients to " << m_outputOptionFileName << endmsg;

  time_t ttt = time(0);

  bool cable=true;
  if( m_tell1XTalk!=0 && m_cableXTalk==0 ) cable = false;

  if( cable ){
    out << "// This is a dump of the ANALOG CABLE x-talk coefficients. \n";
  } else {
    out << "// This is a dump of the TELL1 x-talk coefficients. \n";
  }
  out << "// Dump done " << ctime( &ttt ) << "\n\n" << std::flush;
  
  std::vector< unsigned int > t1Ids;
  std::vector< std::vector< double > > coefList; // -> coefList[Ki][cable]

  t1Ids.resize( m_sensorxt.size() );

  int globalNMax = 0;
  std::map< unsigned int, VeloXTalkComputer::Tell1Handler >::const_iterator cit;
  for( cit=m_sensorxt.begin(); cit!=m_sensorxt.end(); ++cit ){
    if( globalNMax < cit->second.maxVisitedCableNeighbor ) 
      globalNMax = cit->second.maxVisitedCableNeighbor;
  }
  if( globalNMax>4 ) globalNMax = 4;

  coefList.resize( 2*globalNMax+1 , std::vector<double>( 64*t1Ids.size(), 0 ) );

  int t1Idx = 0;
  for( cit=m_sensorxt.begin(); cit!=m_sensorxt.end(); ++cit ){
    const VeloXTalkComputer::Tell1Handler& TH = cit->second;
    int nMax = TH.maxVisitedCableNeighbor;
    std::vector< std::vector<double> > vals;
    if( cable ) vals = TH.getCableXTalk();
    else vals = TH.getTell1XTalk();

    int maxOffset = nMax;
    if( nMax>4 ) nMax = 4;
    for( int k = -nMax; k<=nMax; ++k ){
      for( int cbl=0; cbl<64; ++cbl ){
	coefList[ k+globalNMax ][64*t1Idx + cbl] = vals[ k+maxOffset ][cbl];
      }
    }
    t1Ids[ t1Idx ] = cit->first;
    t1Idx++;
  }

  // now I have the coeficient, let's try to print them out.
  out << "fir.Tell1List = {";
  for( int i=0; i<(int)t1Ids.size(); ++i ){
    out << t1Ids[i];
    if( i+1 != (int)t1Ids.size() ) out << ", ";
  }
  out << " };\n\n";

  out.precision(3);
  out << std::fixed;

  for (int k=-globalNMax; k<=globalNMax; ++k ){
    if( k==0 ) continue;
    out << "fir.K" << (k<0?"m":"p") << abs(k) << " = {";
    for( int i=0; i<(int)coefList[ k+globalNMax ].size()-1; ++i ){
      out << coefList[ k+globalNMax ][i] << ", ";
    }
    out << coefList[ k+globalNMax ].back() << " };\n\n";
  }

}

// ===============================================================

void VeloXTalkComputer::dumpForGnuplot() const {
  if( m_gnuplotDump.empty() ) return;
  
  std::string dat = m_gnuplotDump;
  dat.append(".dat");
  std::ofstream out( dat.c_str() );
  std::ofstream script( m_gnuplotDump.c_str() );

  always() << "Writing the gnuplot script to " << m_gnuplotDump << endmsg;

  time_t ttt = time(0);
  //  const int nMax = Tell1Handler::maxVisitedCableNeighbor;
 
  bool cable=true;
  if( m_tell1XTalk!=0 && m_cableXTalk==0 ) cable = false;
  
  if( cable ) out << "# This is a dump for gnuplot of the CABLE x-talk coeficients.\n";
  else out << "# This is a dump for gnuplot of the TELL1 x-talk coeficients.\n";
  out << "# Dump done " << ctime( &ttt ) << "\n\n";

  script << "#!/usr/bin/env gnuplot\n";
  if( cable ) script << "# This is a gnuplot script to plot the x-talk from the CABLES\n";
  else        script << "# This is a gnuplot script to plot the x-talk from the TELL1S\n";
  script << "# Dump done " << ctime( &ttt ) << "\n\n";
  script << "reset\n\n\n";

  script << "##### uncomment these lines to produce an eps file\n";
  script << "# set term postscript eps enhanced color \"Courrier\" 18\n";
  script << "# set output 'filename.eps'\n\n\n";

  script << "set multiplot\n";

  script << "set size 1,0.5\n";
  script << "set origin 0,0.5\n\n";

  script << "set nokey\n";
  script << "set xrange [ " << -5 << ":" << 5 << "]\n";
  script << "set xlab 'chip channel offset'\n";
  script << "set ylab 'x-talk (%)'\n";
  script << "plot \\\n";

  int globalNmax = 0;
  int cbloff = 0;
  std::map< unsigned int, VeloXTalkComputer::Tell1Handler >::const_iterator cit;
  for( cit=m_sensorxt.begin(); cit!=m_sensorxt.end(); ++cit ){
    const VeloXTalkComputer::Tell1Handler& TH = cit->second;
    
    std::vector< std::vector<double> > vals;
    if( cable ) vals = TH.getCableXTalk();
    else vals = TH.getTell1XTalk();

    out << "# Data for Tell1 " << cit->first << "\n";
    int nMax = (vals.size()-1 )/2;
    if( globalNmax<nMax ) globalNmax=nMax;

    for( int cbl=0; cbl<64; ++cbl ){
      if( !cable && cbl>0 ) break;
      double sum=0; 
      for( int k=0; k<2*nMax+1; ++k ){
	sum += fabs( vals[k][cbl] );
      }
      if( sum == 0 ) continue; // discard empty sets

      out << "\n\n#Tell11\tcable\toffset\tx-talk(%)\n";

      for( int k=0; k<2*nMax+1; ++k ){
	assert( vals[k].size() == 64 );
	if( k==nMax ) continue;
	out << cit->first << "\t" << cbl << "\t" << k-nMax << "\t" << vals[k][cbl]*100 << "\n";
      }

      script << "\t" << (cbloff!=0?", '":"  '") << dat << "' index " << cbloff 
	     << " using 3:4 "
	     << "title 'Tell1 " << cit->first;
      if( cable ) script << ", cable " << cbl;
      script << "' with lp \\\n";

      ++cbloff;
    }
  }
  
  // doing the per analog cable plot:
  script << "\n\n\n";
  script << "set size 1,0.5\n";
  script << "set origin 0,0\n";
  script << "set key top right\n";
  script << "set autoscale x\n";
  script << "set xlab 'analog cable'\n\n";
  script << "plot \\\n";

  for( int k=0; k<2*globalNmax+1; ++k ){
    if( k==globalNmax ) continue;
    script << "\t" << (k!=0?", '":"  '") << dat << "' using 2:($3==" 
	   << k-globalNmax << "?$4:1/0) tit 'offset " << k-globalNmax << "' with lp \\\n";
  }

  script << "\n\n\n";
  script << "# use the second line if you use version 4.x \n";
  script << "set nomultiplot\n";
  script << "# unset multiplot\n\n\n";


  script << "set terminal X11\n";
  script << "set output\n";

}

// ===============================================================
// ===============================================================
// ===============================================================

