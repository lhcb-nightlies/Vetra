!-----------------------------------------------------------------------------
! Package     : Velo/VeloTELL1Tools
! Responsible : Tomasz Szumlak
! Purpose     : tools for TELL1 analysis and monitoring
!-----------------------------------------------------------------------------

!======================== VeloTELL1Tools v1r10 2015-11-06 =====================
! 2014-08-29 - David Hutchcroft
 - removed ; from DECLARE_TOOL_FACTORY lines (fixes cmake warnings)

!======================== VeloTELL1Tools v1r9 2014-04-03 =====================
! 2014-03-18 - Marco Clemencic
 - Added CMake configuration file.
 - Removed obsolete files.

!======================== VeloTELL1Tools v1r8 2011-10-11 =====================
! 2012-10-09 - David Hutchcroft
 - Fixed compilation warnings

!======================== VeloTELL1Tools v1r7 2011-10-03 =====================
! 2011-09-26 - Marius Bjoernstad
 - Register conditions with UpdateManagerSvc separately to get correct 
   conditions for time interval

!======================== VeloTELL1Tools v1r6 2010-08-06 =====================
! 2010-08-05 - Kurt Rinnert
 - added ability to use hit thresholds for signal filtering to NoiseEvaluator
   tool
 - added tool implementation for TELL1 conditions cache

! ======= 2009-11-20 - T Szumlak - v1r5 ======================================
! 2009-11-20 - Tomasz Szumlak
 - release candidate for Vetra v8r0

! 2009-11-19 - Kurt Rinnert
 - added implementation of reorder flag access and explicit reset to noise
   evaluator tool

! ======= 2008-09-16 - T Szumlak - v1r4 ======================================
! 2009-09-16 - Tomasz Szumlak
 - release candidate for Vetra v7r2
 
! 2009-09-15 - Kurt Rinnert
 - removed TimeStamps and LivDBTELL1SensorMap tool implementations that now
   reside in VeloTools

! 2009-08-27 - Kurt Rinnert
 - change of string format in time stamp tool
 - added missing finalize() methods to new tools

! 2009-08-21 - Kurt Rinnert
 - switch off cache in case we are behind a proxy in Liverpool DB TELL1
   mapping tool.
 - Added new tool implementation to provide uniform time stamps to algorithms.

! 2009-08-17 - Kurt Rinnert
 - the TELL1 mapping tool can now be initialized from a user supplied map
   file.

! 2009-08-11 - Kurt Rinnert
 - added implmentation for TELL1 ID to sensor number mapping tool. This
   implementation is initialized from the Liverpool hardware DB. It is not
   available on Windows(tm).

! 2008-07-15 - T Szumlak - v1r3
 - fix for Win - remove obsolete strcasecmp function use boost to_lower()
   instead

! 2008-05-23 - T Szumlak
 - updated location of packages for Vetra v6r0

! 2007-12-07 - Tomasz Szumlak
 - updates for new release of the Vetra v5r1

