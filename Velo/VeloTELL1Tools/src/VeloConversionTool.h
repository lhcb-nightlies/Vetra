#ifndef MCACCEPTANCETOOL_H 
#define MCACCEPTANCETOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "VetraKernel/IVeloConversionTool.h"            // Interface


/** @class VeloConversionTool VeloConversionTool.h src/VeloConversionTool.h
 * 
 *
 *  @author Tomasz Szumlak
 *  @date   2006-05-20
 */

class DeVelo;

class VeloConversionTool : public GaudiTool, virtual public IVeloConversionTool {
public: 
  /// Standard constructor
  VeloConversionTool( const std::string& type, 
                    const std::string& name,
                    const IInterface* parent);

  virtual ~VeloConversionTool( ); ///< Destructor

  virtual unsigned int strip2ChipChannel(
                                           const unsigned int strip,
                                           const unsigned int sensNumber) override;
  virtual unsigned int chipChannel2Strip(
                                           const unsigned int channel,
                                           const unsigned int sensNumber) override;
  virtual StatusCode initialize() override;
  
protected:

private:
 
  DeVelo* m_veloDet;

};
#endif //
