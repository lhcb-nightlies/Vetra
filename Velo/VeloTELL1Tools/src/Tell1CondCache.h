// $Id: $
#ifndef TELL1CONDCACHE_H 
#define TELL1CONDCACHE_H 1

#include <map>
#include <vector>

#include "GaudiAlg/GaudiTool.h"
#include "VeloDet/DeVelo.h"
#include "VetraKernel/ITell1CondCache.h" 

namespace Velo
{
  /** @class Tell1CondCache Tell1CondCache.h
   *
   *  Implementation of ITell1CondCache interface.
   *
   *  Provides access to cached Tell1 configuration from CondDB.
   *  Without CondDB reasonable defaults are provided.
   *
   *  @author Kurt Rinnert
   *  @date   2010-07-29
   */
  class Tell1CondCache : public GaudiTool, virtual public Velo::ITell1CondCache {

    private:

      friend class ToolFactory<Velo::Tell1CondCache>;

    public: 
      /// Standard constructor
      Tell1CondCache( const std::string& type, 
          const std::string& name,
          const IInterface* parent);

      virtual ~Tell1CondCache( ); ///< Destructor

      virtual StatusCode initialize() override; 
      virtual StatusCode finalize() override; 

      virtual const std::vector<unsigned char>& hitThresholds( unsigned int sensorNumber ) override; ///< hit thresholds, including dummies
      virtual const std::vector<unsigned char>& hitThresholdsNoDummy( unsigned int sensorNumber ) override; ///< hit thresholds, no dummies

    private:

      void clearConditionsCache();
      void cacheDefaults();
      StatusCode cacheConditions();
      StatusCode cacheSrcIdList();

    private:

      // configuration
      bool m_useCondDB;
      std::string m_condPath;
      unsigned int m_hitThresholdValue;

      // geometry
      const DeVelo* m_velo;

      // conditions chache
      std::vector<unsigned int> m_srcIdList;
      std::map<unsigned int, std::vector<unsigned char> > m_hitThresholds; 
      std::map<unsigned int, std::vector<unsigned char> > m_hitThresholdsNoDummy; 

  };
}
#endif // TELL1CONDCACHE_H 
