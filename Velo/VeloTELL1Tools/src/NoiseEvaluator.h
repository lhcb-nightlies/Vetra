// $Id: NoiseEvaluator.h,v 1.2 2009-11-19 17:24:05 krinnert Exp $
// ============================================================================
#ifndef NOISEEVALUATOR_H 
#define NOISEEVALUATOR_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/ToolFactory.h" 
// ============================================================================
// GaudiAlg 
// ============================================================================
#include "GaudiAlg/GaudiTool.h"

#include "VetraKernel/ITell1CondCache.h"          

// ============================================================================
// Local 
// ============================================================================
#include "VetraKernel/INoiseEvaluator.h"          
#include "VetraKernel/TELL1Noise.h"
// ============================================================================
namespace Velo 
{
  namespace Monitoring 
  {
    /** @class NoiseEvaluator NoiseEvaluator.h
     *  The most simple implementation 
     *  of the interface Velo::Monitoring::INoiseEvaluator 
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-10-25
     */
    class NoiseEvaluator 
      : public virtual Velo::Monitoring::INoiseEvaluator
      , public GaudiTool
    {
      // the friend factory for instantiation 
      friend class ToolFactory<Velo::Monitoring::NoiseEvaluator> ;

    public:      
      
      virtual StatusCode initialize () override;

      // ======================================================================
      /** process the data from one TELL1 board
       *  @see LHCb::VeloTELL1data
       *  @param data Velo TELL1 data 
       *  @param tell1 ID of TELL1 board 
       *  @param headers the container with link headers (if needed)
       *  @return status code 
       */
      virtual StatusCode process 
      ( const LHCb::VeloTELL1Data::Container* data    , 
        const int                             tell1   ,
        const LHCb::VeloTELL1Data::Container* headers ) override;
      // ======================================================================
      /** process the data from several TELL1 boards 
       *  @see LHCb::VeloTELL1data
       *  @param data Velo TELL1 data 
       *  @param tell1 ID of TELL1 board 
       *  @param headers the container with link headers (if needed)
       *  @return status code 
       */
      virtual StatusCode process 
      ( const LHCb::VeloTELL1Data::Container*            data    , 
        const Velo::Monitoring::INoiseEvaluator::TELL1s& tell1s  ,
        const LHCb::VeloTELL1Data::Container*            headers ) override;
      // ======================================================================
      /** process the data from all TELL1 boards
       *  @see LHCb::VeloTELL1data
       *  @param data Velo TELL1 data 
       *  @param headers the container with link headers (if needed)
       *  @return status code 
       */
      virtual StatusCode process 
      ( const LHCb::VeloTELL1Data::Container* data    ,
        const LHCb::VeloTELL1Data::Container* headers ) override;
      // ======================================================================
      /** process the data from one TELL1 board
       *  @see LHCb::VeloTELL1data
       *  @param data Velo TELL1 data 
       *  @param headers the container with link headers (if needed)
       *  @return status code 
       */
      virtual StatusCode process 
      ( const LHCb::VeloTELL1Data*            data    ,
        const LHCb::VeloTELL1Data::Container* headers ) override;
      // ======================================================================
      /** process the data from one TELL1 board
       *  @see LHCb::VeloTELL1data
       *  @param data Velo TELL1 data 
       *  @param headers the link headers (if needed)
       *  @return status code 
       */
      virtual StatusCode process 
      ( const LHCb::VeloTELL1Data*             data    ,
        const LHCb::VeloTELL1Data*             headers ) override;
      // ======================================================================
    public:
      // ======================================================================
      /** get all noise information for the given TELL1 
       *  @param tell1 the ID of the TELL1 board 
       *  @return pointer to TELL1 noise information 
       */
      const Velo::Monitoring::TELL1Noise* noise ( const int tell1  ) const override;
      // ======================================================================
      /** get all noise information for some TELL1 boards 
       *  @param tell1s the list of IDs for the TELL1 boards 
       *  @return container of TELL1 noise information
       */
      Velo::Monitoring::INoiseEvaluator::INoiseMap 
      noise ( const TELL1s& tell1s ) const override;
      // ======================================================================
      /** get all noise information for all TELL1 boards 
       *  @return container of TELL1 noise information
       */
      Velo::Monitoring::INoiseEvaluator::INoiseMap 
      noise () const override;

      /** check whether the TES container the tool is operating on
       * is redordered.
       * @return reorder flag
       */
      virtual bool isReordered() const override;

      /** reset all counters for a given TELL1.
       */
      virtual void reset( const unsigned int TELL1 ) override;
      
      // ======================================================================
    protected:
      /// the actual type of the noise map      
      typedef std::map<int,Velo::Monitoring::NoisePair> NoiseMap ;
    protected:
      /** Standard constructor
       *  @param type tool type(?)
       *  @param name tool instance name 
       *  @param parent tool parent 
       */
      NoiseEvaluator 
      ( const std::string& type, 
        const std::string& name,
        const IInterface* parent);
      // destructor 
      virtual ~NoiseEvaluator( ); ///< Destructor  
    private :      
      // the actual storage of noise information 
      NoiseMap     m_map   ; ///< the actual storage of noise information 
      // run m_reset events before reset
      unsigned int m_reset ; ///< run m_reset events before reset 
      // threshold to skip the large signals (in sigmas)
      double       m_sigma ; ///< threshold to skip the large signals (in sigmas)
      // do not skip the first m_min events 
      unsigned int m_min   ; // do not skip the first m_min events
      // the header level 
      int          m_headerLevel ;
      // reorder flag
      bool         m_reordered;
      // use hit thresholds to filter signal?
      bool         m_useHitThresholds;

      // access to conditions for thresholds
      ITell1CondCache* m_condCache;
    };
  } // end of namespace Velo::Monitoring 
} // end of namespace Velo 

// ============================================================================
// The END 
// ============================================================================
#endif // NOISEEVALUATOR_H
// ============================================================================
