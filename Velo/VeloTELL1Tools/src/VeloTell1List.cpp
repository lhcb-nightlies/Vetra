// $Id: VeloTell1List.cpp,v 1.1 2007-12-07 17:33:27 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/ToolFactory.h" 

#include "VeloDet/DeVelo.h"

// local
#include "VeloTell1List.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTell1List
//
// 2007-10-12 : Jianchun Wang
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( VeloTell1List )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTell1List::VeloTell1List( const std::string& type,
                              const std::string& name,
                              const IInterface* parent )
  : GaudiTool ( type, name , parent )
{
  declareInterface<IVeloTell1List>(this);
}

//=============================================================================
// Destructor
//=============================================================================
VeloTell1List::~VeloTell1List() {
  debug() << "==> Destructor" << endmsg;
} 

//=============================================================================
StatusCode VeloTell1List::initialize () 
{
  debug() << "==> Initialize " << endmsg;

  int tell1Id;
  int minTell1Id = 10;
  int sensorId;
  int maxTell1Id = 0;
  int maxSensorId = 0;
  int num;
  const DeVelo* m_velo = getDet<DeVelo>(DeVeloLocation::Default);
  std::vector<DeVeloSensor*>::const_iterator it;

  // Global info
  debug() << " ====  Sensor/TELL1 Information ==== " << endmsg;
  debug() << " Number of sensors = " << m_velo->numberSensors()
          << "  R sensors = "        << m_velo->numberRSensors()
          << "  Phi sensors ="       << m_velo->numberPhiSensors()
          << endmsg;

  // Get sensor type and sensor ID from data base
  num = 0;
  debug() << "Index  Sensor   Tell1    isR   isRight    Z" << endmsg;
  for ( it=m_velo->sensorsBegin(); it!=m_velo->sensorsEnd(); it++){
    sensorId=(*it)->sensorNumber();
    if ( sensorId > maxSensorId ) maxSensorId = sensorId;

    if ( ! m_velo->tell1IdBySensorNumber(sensorId, (unsigned int&) tell1Id) ) {
      debug() << " -- sensor " << sensorId
              << " does not have a tell1 connected "
              << endmsg;
       continue;
    }

    if ( tell1Id < minTell1Id ) {
      debug() << " NA";
    } else {
      debug() << format("%3d", num);
      num++;
      if ( tell1Id > maxTell1Id ) maxTell1Id = tell1Id;
    }
    debug() << format("%8d", sensorId)
            << format("%8d", tell1Id)
            << format("%8d", (*it)->isR())
            << format("%8d", (*it)->isRight())
            << format("%11.2f", (*it)->z())
            << endmsg;
  }
  
  // create index array
  m_numOfTell1s  = num;
  m_tell1List    = sdataVec (m_numOfTell1s, 0);
  m_sensorList   = sdataVec (m_numOfTell1s, 0);
  m_tell1Index   = sdataVec (maxTell1Id+1,  0);
  m_sensorIndex  = sdataVec (maxSensorId+1, 0);

  // save index
  num = 0;
  for ( it=m_velo->sensorsBegin(); it!=m_velo->sensorsEnd(); it++){
    
    sensorId=(*it)->sensorNumber();
    if ( ! m_velo->tell1IdBySensorNumber(sensorId, (unsigned int&) tell1Id) ) {
       continue;
    }
    if ( tell1Id < minTell1Id ) {
      continue;
    }

    m_tell1List[num]        = tell1Id;
    m_sensorList[num]       = sensorId;
    m_tell1Index[tell1Id]   = num;
    m_sensorIndex[sensorId] = num;

    num++;
  }

  return StatusCode::SUCCESS;
}

