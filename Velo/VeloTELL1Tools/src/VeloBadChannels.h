// $Id: VeloBadChannels.h,v 1.2 2009-09-15 17:47:03 szumlat Exp $
#ifndef VELOBADCHANNELS_H 
#define VELOBADCHANNELS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "VetraKernel/IVeloBadChannels.h"            // Interface


/** @class VeloBadChannels VeloBadChannels.h
 *  
 *
 *  @author Jianchun Wang
 *  @date   2007-10-11
 */
class VeloBadChannels : public GaudiTool, virtual public IVeloBadChannels 
{
public:
  
  virtual const ChannelList& badTell1Channels()  const override { return m_tChannList; }
  virtual const ChannelList& badTell1Strips()    const override { return m_tStripList; }
  virtual const ChannelList& badSensorChannels() const override { return m_sChannList; }
  virtual const ChannelList& badSensorStrips()   const override { return m_sStripList; }
  
  virtual StatusCode initialize () override;
  
public: 
  /// Standard constructor
  VeloBadChannels( const std::string& type, 
                   const std::string& name,
                   const IInterface* parent);

  virtual ~VeloBadChannels( ); ///< Destructor

protected:

private:
  
  IVeloBadChannels::ChannelList m_tChannList ;
  IVeloBadChannels::ChannelList m_tStripList ;
  IVeloBadChannels::ChannelList m_sChannList ;
  IVeloBadChannels::ChannelList m_sStripList ;
  std::string                   m_fileName ;
  
};
#endif // VELOBADCHANNELS_H
