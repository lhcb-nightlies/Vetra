// $Id: ConvTELL1ToSensor.cpp,v 1.2 2007-12-12 14:03:50 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/ToolFactory.h" 

// local
#include "ConvTELL1ToSensor.h"

// DeVelo
#include "VeloDet/DeVelo.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ConvTELL1ToSensor
//
// 2006-08-25 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( ConvTELL1ToSensor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ConvTELL1ToSensor::ConvTELL1ToSensor( const std::string& type,
                                      const std::string& name,
                                      const IInterface* parent )
  : GaudiTool ( type, name , parent )
{
  declareInterface<IConvTELL1ToSensor>(this);
}
//=============================================================================
// Destructor
//=============================================================================
ConvTELL1ToSensor::~ConvTELL1ToSensor() {}
//=============================================================================
StatusCode ConvTELL1ToSensor::initialize()
{
  debug()<< " initialize() " << endmsg;
  m_veloDet=getDet<DeVelo>(DeVeloLocation::Default);
  return ( StatusCode::SUCCESS );
}
//=============================================================================
unsigned int ConvTELL1ToSensor::tell1ToSensor(unsigned int tell1)
{
  debug()<< " ==> tell1ToSensor() " <<endmsg;
  //
  const DeVeloSensor* sens=0;
  sens=m_veloDet->sensorByTell1Id(tell1);
  unsigned int sensNumber=0;
  sensNumber=sens->sensorNumber();
  //
  return ( sensNumber );
}
//

