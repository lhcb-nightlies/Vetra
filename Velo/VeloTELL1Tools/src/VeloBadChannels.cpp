// $Id: VeloBadChannels.cpp,v 1.2 2008-07-15 14:40:11 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/ToolFactory.h" 

#include "VeloDet/DeVelo.h"

// local
#include "VeloBadChannels.h"

// from boost
#include <boost/algorithm/string.hpp>

using namespace boost;

//-----------------------------------------------------------------------------
// Implementation file for class : VeloBadChannels
//
// 2007-10-11 : Jianchun Wang
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( VeloBadChannels )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloBadChannels::VeloBadChannels( const std::string& type,
                                  const std::string& name,
                                  const IInterface* parent )
  : GaudiTool ( type, name , parent )
  , m_tChannList()
  , m_tStripList()
  , m_sChannList()
  , m_sStripList()
  , m_fileName() 
{
  declareInterface<IVeloBadChannels>(this);

  declareProperty( "BadStripFileName", m_fileName ) ;
}
//=============================================================================
// Destructor
//=============================================================================
VeloBadChannels::~VeloBadChannels() {
  //  info() << "==> Destructor" << endmsg;  
} 

//=============================================================================

StatusCode VeloBadChannels::initialize () 
{
  debug() << "==> Initialize " << endmsg;
  
  StatusCode sc = GaudiTool::initialize() ;
  if ( sc.isFailure() ) { return sc ; }
  
  if ( m_fileName.empty() ) {
    debug() << " No bad strip/channel/link defined" << endmsg;
    return sc;
  }

  info() << " Bad strip/channel/link is defined in "
         << m_fileName
         << endmsg;

  FILE *fm = fopen( m_fileName.c_str(), "r");
  if ( fm == NULL ) {
    info() << " Bad strip file open error: "
           << m_fileName
           << endmsg;
    return StatusCode::FAILURE;
  }

  const DeVelo* m_velo = getDet<DeVelo>(DeVeloLocation::Default);
  const DeVeloSensor* sensor;

  int tell1Id;
  int sensorId;
  int linkId;
  int channelId;
  int stripId;
  int itmp1;
  int itmp2;
  char tag1[50];
  char tag2[50];
  int numBadStrip = 0;

  debug() << "    Sensor  Tell1    Link  Channel  Strip" << endmsg;
  while ( 1 ) {
    int np = fscanf(fm, "%s%d%s%d", tag1, &itmp1, tag2, &itmp2);
    if ( np < 0 ) break;

    if ( np > 3 ) {
      debug() << " -- bad cell:   " << tag1
              << "  " << itmp1
              << "  " << tag2
              << "  " << itmp2
              << endmsg;

      // First pair: TELL1 ID or sensor ID
      to_lower(tag1);
      //if ( strcasecmp(tag1, "tell1") == 0 ) {
      //if ( tag1=="tell1" ) {
      if( strcmp(tag1,"tell1") == 0 ) {
        tell1Id  = itmp1;
        sensor   = m_velo->sensorByTell1Id(tell1Id);
        sensorId = sensor->sensorNumber();
        //      } else if ( tag1=="sensor" ) {
      } else if ( strcmp(tag1,"sensor") == 0 ) {
        sensorId = itmp1;
        sensor   = m_velo->sensor(sensorId);
        if ( ! m_velo->tell1IdBySensorNumber((unsigned int) sensorId, (unsigned int&) tell1Id) ) {
          debug() << " -- sensor " << sensor
                  << " does not have a tell1 connected "
                  << endmsg;
          continue;
        }
      } else {
        debug() << " -- first tag " << tag1
                << "   is neither TELL1 nor Sensor. Skipped "
                << endmsg;
        continue;
      }

      // Second pair: link, electronic channel or strip
      to_lower(tag2);
      if( strcmp(tag2,"link") == 0 ){
        linkId    = itmp2;
        for ( channelId = linkId*32; channelId < linkId*32+32; channelId++ ) {
          stripId   = sensor->ChipChannelToStrip(channelId);
          debug() << format("%8d", sensorId)
                  << format("%8d", tell1Id)
                  << format("%8d", linkId)
                  << format("%8d", channelId)
                  << format("%8d", stripId)
                  << endmsg;

          m_tChannList.push_back( std::make_pair( tell1Id,  channelId ) ) ;
          m_tStripList.push_back( std::make_pair( tell1Id,  stripId ) ) ;
          m_sChannList.push_back( std::make_pair( sensorId, channelId ) ) ;
          m_sStripList.push_back( std::make_pair( sensorId, stripId ) ) ;
          numBadStrip ++;
        }
      } else {
        if( strcmp(tag2,"channel") == 0 ){

          channelId = itmp2;
          stripId   = sensor->ChipChannelToStrip(channelId);
          linkId    = channelId/32;          
        } else if ( strcmp(tag2,"strip")==0 ) {
          stripId   = itmp2;
          channelId = sensor->StripToChipChannel(stripId);
          linkId    = channelId/32;
        } else {
          debug() << " -- second tag " << tag2
                  << "   is not channel/strip/link. Skipped"
                  << endmsg;
          continue;
        }
        debug() << format("%8d", sensorId)
                << format("%8d", tell1Id)
                << format("%8d", linkId)
                << format("%8d", channelId)
                << format("%8d", stripId)
                << endmsg;

        m_tChannList.push_back( std::make_pair( tell1Id,  channelId ) ) ;
        m_tStripList.push_back( std::make_pair( tell1Id,  stripId ) ) ;
        m_sChannList.push_back( std::make_pair( sensorId, channelId ) ) ;
        m_sStripList.push_back( std::make_pair( sensorId, stripId ) ) ;
        numBadStrip ++;
      }
    }
  }
  std::sort( m_tChannList.begin() , m_tChannList.end() ) ;
  std::sort( m_tStripList.begin() , m_tStripList.end() ) ;
  std::sort( m_sChannList.begin() , m_sChannList.end() ) ;
  std::sort( m_sStripList.begin() , m_sStripList.end() ) ;

  fclose(fm);
  info() << " Total number of bad strips removed:  "
         << numBadStrip
         << endmsg;

  return StatusCode::SUCCESS ;
}


