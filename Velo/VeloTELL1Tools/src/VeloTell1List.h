// $Id: VeloTell1List.h,v 1.2 2009-09-15 17:47:03 szumlat Exp $
#ifndef VELOTELL1LIST_H 
#define VELOTELL1LIST_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "VetraKernel/IVeloTell1List.h"            // Interface


/** @class VeloTell1List VeloTell1List.h
 *  
 *
 *  @author Jianchun Wang
 *  @date   2007-10-12
 */
class VeloTell1List : public GaudiTool, virtual public IVeloTell1List {
public:
  virtual const sdataVec& veloTell1List()   const override {return m_tell1List;}
  virtual const sdataVec& veloTell1Index()  const override {return m_tell1Index;}
  virtual const sdataVec& veloSensorList()  const override {return m_sensorList;}
  virtual const sdataVec& veloSensorIndex() const override {return m_sensorIndex;}
  virtual int veloNumberOfTell1s() const override {return m_numOfTell1s;}
  virtual StatusCode initialize () override;

public: 
  /// Standard constructor
  VeloTell1List( const std::string& type, 
                 const std::string& name,
                 const IInterface* parent);

  virtual ~VeloTell1List( ); ///< Destructor

protected:

private:
  int m_numOfTell1s;
  sdataVec m_tell1List;
  sdataVec m_sensorList;
  sdataVec m_tell1Index;
  sdataVec m_sensorIndex;

};
#endif // VELOTELL1LIST_H
