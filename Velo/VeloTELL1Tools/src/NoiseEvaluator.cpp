// $Id: NoiseEvaluator.cpp,v 1.2 2009-11-19 17:24:04 krinnert Exp $
// =============================================================================
// Include files 
// =============================================================================
// STD & STL 
// =============================================================================
#include <map>
// =============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/ToolFactory.h" 
#include "GaudiKernel/ToStream.h" 
// ============================================================================
// local
// ============================================================================
#include "VetraKernel/TELL1Noise.h"
#include "NoiseEvaluator.h"

// ============================================================================
/** @file 
 *  Implementation file for class Velo::Monitoring::NoiseEvaluator
 *  @date 2007-10-25 
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
//=============================================================================

  
  
Velo::Monitoring::NoiseEvaluator::NoiseEvaluator
( const std::string& type   ,
  const std::string& name   ,
  const IInterface*  parent )
  : GaudiTool ( type, name , parent )
  // the actual storage of noise information 
  , m_map         (    ) ///< the actual storage of noise information 
  // run m_reset events before reset
  , m_reset       ( 0  ) ///< run m_reset events before reset
  // threshold to skip the large signals (in sigmas)
  , m_sigma       ( -1 ) ///< threshold to skip the large signals (in sigmas)
  // do not skip the first m_min events 
  , m_min         ( 50 ) ///< do not skip the first m_min events 
  // the header level 
  , m_headerLevel ( 0  ) ///< the header level 
  , m_reordered (false)
{
  declareInterface<Velo::Monitoring::INoiseEvaluator>(this);
  //
  declareProperty 
    ( "Reset"  , 
      m_reset  ,
      "Reset frequency for the counters"        ) ;
  //
  declareProperty 
    ( "nSigma" , 
      m_sigma  , 
      "Threshold for large signals (in sigmas)" ) ;
  //
  declareProperty 
    ( "MinEvt" , 
      m_min    , 
      "Never skip the first events" ) ;
  //  
  declareProperty 
    ( "HeaderLevel" , 
      m_headerLevel , 
      "The Header Level:  -1 : LOW, 0 : ALL , +1 : HIGH" ) ;
  //  
  declareProperty 
    ( "UseHitThresholds" , 
      m_useHitThresholds=false , 
      "use hit thresholds to filter out signal" ) ;
}
// ============================================================================
// destructor 
// ============================================================================
Velo::Monitoring::NoiseEvaluator::~NoiseEvaluator(){}


// ============================================================================
// initialization
// ============================================================================
StatusCode Velo::Monitoring::NoiseEvaluator::initialize()
{
  StatusCode sc = GaudiTool::initialize();
  if ( !sc ) {
    return sc;
  } 

  m_condCache = tool<Velo::ITell1CondCache>("Velo::Tell1CondCache", "Tell1CondCache:PUBLIC");

  return StatusCode::SUCCESS;
}



// ============================================================================
/*  retrieve reorder flag for underlying TES container
 *  @return reorder flag
 */
// ============================================================================
bool Velo::Monitoring::NoiseEvaluator::isReordered() const
{
  return m_reordered;
}
// ============================================================================
/*  get all noise information for the given TELL1 
 *  @param tell1 the ID of the TELL1 board 
 *  @return pointer to TELL1 noise information 
 */
// ============================================================================
const Velo::Monitoring::TELL1Noise* 
Velo::Monitoring::NoiseEvaluator::noise ( const int tell1  ) const 
{
  // find the appropriate TELL1 
  NoiseMap::const_iterator ifind = m_map.find ( tell1 ) ;
  if ( m_map.end() == ifind ) 
  {
    Warning ( "Missing TELL1 identifier: " + 
              Gaudi::Utils::toString ( tell1 ) ) ;
    return 0 ;
  }
  // get the most statistically significant noise information 
  return ifind->second.best() ;
}
// ============================================================================
/*  get all noise information for some TELL1 boards 
 *  @param tell1s the list of IDs for the TELL1 boards 
 *  @return container of TELL1 noise information
 */
// ============================================================================
Velo::Monitoring::INoiseEvaluator::INoiseMap 
Velo::Monitoring::NoiseEvaluator:: noise 
( const Velo::Monitoring::INoiseEvaluator::TELL1s& tell1s ) const 
{
  Velo::Monitoring::INoiseEvaluator::INoiseMap imap ;
  for ( Velo::Monitoring::INoiseEvaluator::TELL1s::const_iterator it1 = 
          tell1s.begin() ; tell1s.end() != it1 ; ++it1 ) 
  {
    const Velo::Monitoring::TELL1Noise* n = noise ( *it1 )  ;
    if ( 0 != n ) { imap [ (*it1) ] = n ; }
  }
  return imap ;
}
// ============================================================================
/*  get all noise information for all TELL1 boards 
 *  @return container of TELL1 noise information
 */
// ============================================================================
Velo::Monitoring::INoiseEvaluator::INoiseMap 
Velo::Monitoring::NoiseEvaluator::noise () const 
{
  Velo::Monitoring::INoiseEvaluator::TELL1s tells1 ;
  for ( NoiseMap::const_iterator it1 =  m_map.begin() ; 
        m_map.end() != it1 ; ++it1 ) { tells1.push_back ( it1->first ) ; }   
  return noise ( tells1 ) ;
}
// ============================================================================
/*  process the data from one TELL1 board
 *  @see LHCb::VeloTELL1data
 *  @param data Velo TELL1 data 
 *  @param tell1 ID of TELL1 board 
 *  @return status code 
 */
// ============================================================================
StatusCode Velo::Monitoring::NoiseEvaluator::process 
( const LHCb::VeloTELL1Data::Container* data    , 
  const int                             tell1   ,
  const LHCb::VeloTELL1Data::Container* headers )  
{
  if ( 0 == data ) 
  { return Error ( "LHCb::VeloTELL1Data::Container* points to NULL") ; }
  // get the data:
  const LHCb::VeloTELL1Data* d = data->object ( tell1 ) ;
  //get the headers 
  const LHCb::VeloTELL1Data* h = 0  ; 
  if ( 0 != headers ) { h = headers -> object ( tell1 ) ; }
  // process one TELL1 board 
  return process ( d , h ) ;
}
// ============================================================================
/*  process the data from several TELL1 boards 
 *  @see LHCb::VeloTELL1data
 *  @param data Velo TELL1 data 
 *  @param tell1 ID of TELL1 board 
 *  @return status code 
 */
// ============================================================================
StatusCode Velo::Monitoring::NoiseEvaluator::process 
( const LHCb::VeloTELL1Data::Container*            data    , 
  const Velo::Monitoring::INoiseEvaluator::TELL1s& tell1s  ,
  const LHCb::VeloTELL1Data::Container*            headers )  
{
  StatusCode sc = StatusCode::SUCCESS ;
  for ( Velo::Monitoring::INoiseEvaluator::TELL1s::const_iterator it1 = 
          tell1s.begin() ; tell1s.end() != it1 && sc.isSuccess() ; ++it1 ) 
  {
    sc = process ( data , *it1 , headers ) ;  ///< process one TELL1 board  
  }
  return sc ;
}
// ======================================================================
/*  process the data from all TELL1 boards
 *  @see LHCb::VeloTELL1data
 *  @param data Velo TELL1 data 
 *  @return status code 
 */
// ======================================================================
StatusCode Velo::Monitoring::NoiseEvaluator::process 
( const LHCb::VeloTELL1Data::Container* data    ,
  const LHCb::VeloTELL1Data::Container* headers )  
{
  if ( 0 == data ) 
  { return Error ( "LHCb::VeloTELL1Data::Container* points to NULL") ; }
  StatusCode sc = StatusCode::SUCCESS ;
  for ( LHCb::VeloTELL1Data::Container::const_iterator it1 = data->begin() ; 
        data->end() != it1 ; ++it1 ) 
  {
    // process one TELL1 board 
    sc = process ( *it1 , headers ) ; ///< process one TELL1 board
  }
  return sc ;
}
// ======================================================================
/*  process the data from one TELL1 board
 *  @see LHCb::VeloTELL1data
 *  @param data Velo TELL1 data 
 *  @return status code 
 */
// ======================================================================
StatusCode Velo::Monitoring::NoiseEvaluator::process 
( const LHCb::VeloTELL1Data*            data    , 
  const LHCb::VeloTELL1Data::Container* headers )  
{
  if ( 0 == data ) 
  { return Error ( "LHCb::VeloTELL1Data* points to NULL") ; }
  //get the headers 
  const LHCb::VeloTELL1Data* h = 0  ; 
  if ( 0 != headers ) { h = headers -> object ( data->key() ) ; }  
  // process TELL1 board with headers 
  return process ( data , h ) ;
}
// ======================================================================
/*  process the data from one TELL1 board
 *  @see LHCb::VeloTELL1data
 *  @param data Velo TELL1 data 
 *  @return status code 
 */
// ======================================================================
StatusCode Velo::Monitoring::NoiseEvaluator::process 
( const LHCb::VeloTELL1Data* data    , 
  const LHCb::VeloTELL1Data* headers )  
{
  if ( 0 == data ) 
  { return Error ( "LHCb::VeloTELL1Data* points to NULL") ; }
  //
  if ( 0 != headers && data->key() != headers->key() ) 
  { return Error ( "Mismatch in the keys for DATA and HEADERS" ) ; }
  
  debug () << "Process TELL1 id=" << data->key() << endmsg ;
  //
  
  m_reordered = data->isReordered();
  
  NoisePair& p = m_map [ data->key() ] ;
 
  if ( m_useHitThresholds ) {
    p.first()->fill ( *data, 
        m_condCache->hitThresholdsNoDummy(data->key()),
        m_reset );
  } else { 
    // always fill the first object
    p.first()->fill ( *data   , 
        m_reset , 
        m_sigma , 
        m_min   ) ;
  }

  // activate the second counter (if needed)
  if ( 0   < m_reset && !p.active() && 
       0.5 * m_reset <   p.first()->meanEntries() ) { p.activate() ; }
  //
  if ( p.active() ) 
  {
    if ( m_useHitThresholds ) {
      p.second()->fill ( *data, 
          m_condCache->hitThresholdsNoDummy(data->key()),
          m_reset );
    } else { 
      p.second()->fill ( *data   , 
          m_reset , 
          m_sigma , 
          m_min   ) ;
    }
  }

  //
  return StatusCode::SUCCESS ;
}
// ======================================================================
/*  reset all counters for one TELL1 board
 */
// ======================================================================
void Velo::Monitoring::NoiseEvaluator::reset( const unsigned int tell1 )
{
  NoiseMap::iterator inp = m_map.find(static_cast<int>(tell1));
  if ( m_map.end() == inp )
  {
    return;
  }
  inp->second.reset();    
}
// ============================================================================
// Declaration of the Tool Factory
// ============================================================================
DECLARE_NAMESPACE_TOOL_FACTORY(Velo::Monitoring,NoiseEvaluator )
// ============================================================================
// The END 
// ============================================================================
