// $Id: VeloConversionTool.cpp,v 1.2 2007-12-12 14:03:50 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 

// local
#include "VeloConversionTool.h"

// DeVelo
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ConversionTool
//
// 2006-05-25 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( VeloConversionTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloConversionTool::VeloConversionTool(const std::string& type,
                               const std::string& name,
                               const IInterface* parent)
  : GaudiTool ( type, name , parent )
{
  declareInterface<IVeloConversionTool>(this);
}
//=============================================================================
// Destructor
//=============================================================================
VeloConversionTool::~VeloConversionTool() {}
//=============================================================================
StatusCode VeloConversionTool::initialize()
{
  debug()<< " initialize() " <<endmsg;
  m_veloDet=getDet<DeVelo>(DeVeloLocation::Default);
  return ( StatusCode::SUCCESS );
}
//=============================================================================
unsigned int VeloConversionTool::strip2ChipChannel(
                                 const unsigned int strip,
                                 const unsigned int sensNumber)
{
  debug()<< " ==> strip2ChipCannel() " <<endmsg;
  //
  const DeVeloSensor* sensor=m_veloDet->sensor(sensNumber);
  unsigned int chipChannel=0;
  chipChannel=sensor->StripToChipChannel(strip);
  //
  return ( chipChannel );
}
//=============================================================================
unsigned int VeloConversionTool::chipChannel2Strip(
                                 const unsigned int channel,
                                 const unsigned int sensNumber)
{
  debug()<< " ==> chipChannel2Strip() " <<endmsg;
  //
  const DeVeloSensor* sensor=m_veloDet->sensor(sensNumber);
  unsigned int strip=0;
  strip=sensor->ChipChannelToStrip(channel);
  //
  return ( strip );
}
//
