// $Id: $
#include <sstream>

#include "GaudiKernel/ToolFactory.h" 
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "VeloEvent/VeloTELL1Data.h"
#include "DetDesc/Condition.h"

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

#include "Tell1CondCache.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Tell1CondCache
//
// 2010-07-29 : Kurt Rinnert
//-----------------------------------------------------------------------------

namespace Velo
{
  
  DECLARE_TOOL_FACTORY( Tell1CondCache )
  
  //=============================================================================
  // Standard constructor
  //=============================================================================
  Tell1CondCache::Tell1CondCache( const std::string& type,
      const std::string& name,
      const IInterface* parent )
    : GaudiTool ( type, name , parent )
    {
      declareInterface<Velo::ITell1CondCache>(this);
      declareProperty("UseCondDB",m_useCondDB=true);
      declareProperty("CondDBPath",m_condPath="VeloCondDB");
      declareProperty("HitThresholdValue", m_hitThresholdValue=10);
    }

  //=============================================================================
  // Destructor
  //=============================================================================
  Tell1CondCache::~Tell1CondCache() {}

  //=============================================================================
  // Initializes init time stamp
  //=============================================================================
  StatusCode Tell1CondCache::initialize()
  {
    StatusCode sc = GaudiTool::initialize();
    if ( !sc ) return sc;

    m_velo=getDet<DeVelo>(DeVeloLocation::Default);

    if(m_useCondDB){
      // Register all TELL1 conditions and the Tell1Map with the UpdateManagerSvc
      IUpdateManagerSvc* mgrSvc=svc<IUpdateManagerSvc>("UpdateManagerSvc", true);

      mgrSvc->registerCondition(this, m_condPath + "/Tell1Map",
          &Tell1CondCache::cacheSrcIdList); // PU is not in DB! It will be forced to 128-131!
      mgrSvc->registerCondition(this, m_condPath + "/Tell1Map",
          &Tell1CondCache::cacheConditions);

      // R sensors
      for (int sens=0; sens<41; ++sens)
      {
        boost::format cond ("%s/VeloTELL1Board%d");
        cond % m_condPath % sens;
        mgrSvc->registerCondition(this, cond.str(),
                  &Tell1CondCache::cacheConditions);
      }

      // Phi sensors
      for (int sens=64; sens<106; ++sens)
      {
        boost::format cond ("%s/VeloTELL1Board%d");
        cond % m_condPath % sens;
        mgrSvc->registerCondition(this, cond.str(),
                  &Tell1CondCache::cacheConditions);
      }


      StatusCode mgrSvcStatus=mgrSvc->update(this);
      if(mgrSvcStatus.isFailure()){
        return ( Error("Failed first UMS update", mgrSvcStatus) );
      }
    } else {
      cacheDefaults();
    } 

    return StatusCode::SUCCESS;
  }

  //=============================================================================
  // Finalize
  //=============================================================================
  StatusCode Tell1CondCache::finalize()
  {
    return GaudiTool::finalize();
  }

  //=============================================================================
  // Access to strip-ordered hit thresholds, including dummies
  //=============================================================================
  const std::vector<unsigned char>& Tell1CondCache::hitThresholds( unsigned int sensorNumber )
  {
    return m_hitThresholds[sensorNumber];
  }

  //=============================================================================
  // Access to strip-ordered hit thresholds, no dummies
  //=============================================================================
  const std::vector<unsigned char>& Tell1CondCache::hitThresholdsNoDummy( unsigned int sensorNumber )
  {
    return m_hitThresholdsNoDummy[sensorNumber];
  }

  //=============================================================================
  // clear the conditions cache
  //=============================================================================
  void Tell1CondCache::clearConditionsCache()
  {
    m_hitThresholds.clear();
    m_hitThresholdsNoDummy.clear();

    return;
  }

  //=============================================================================
  // set defaults in case of no CondDB
  //=============================================================================
  void Tell1CondCache::cacheDefaults()
  {
    // get source ids from detector element (i.e. set them to sensor numbers)
    m_srcIdList.clear();
    for (std::vector<DeVeloSensor*>::const_iterator si=m_velo->sensorsBegin(); si != m_velo->sensorsEnd(); ++si) {
      m_srcIdList.push_back((*si)->sensorNumber());
    }

    // set defaults for all source ids
    clearConditionsCache();
    for (std::vector<unsigned int>::iterator iSrcId=m_srcIdList.begin(); iSrcId != m_srcIdList.end(); ++iSrcId) {
      m_hitThresholds[*iSrcId] = std::vector<unsigned char>(2304,static_cast<unsigned char>(m_hitThresholdValue));
      m_hitThresholdsNoDummy[*iSrcId] = std::vector<unsigned char>(2048,static_cast<unsigned char>(m_hitThresholdValue));
    }

    return;
  }

  //=============================================================================
  // Cache source ids
  //=============================================================================
  StatusCode Tell1CondCache::cacheSrcIdList()
  {
    std::string tell1Path=m_condPath+"/Tell1Map";
    Condition* tell1Cond=getDet<Condition>(tell1Path);
    std::vector<int> tmpList=tell1Cond->param<std::vector<int> >("tell1_map");
    
    if(tmpList.empty()) {
      return ( Error(" --> Cannot retrieve Tell1 Map!", StatusCode::FAILURE) );
    }

    m_srcIdList.clear();
    std::vector<int>::const_iterator iTmp=tmpList.begin();
    for( ; iTmp!=tmpList.end(); ++iTmp){
      m_srcIdList.push_back(static_cast<unsigned int>(*iTmp));
    }
   
    return StatusCode::SUCCESS;
  }

  //=============================================================================
  // Cache conditions
  //=============================================================================
  StatusCode Tell1CondCache::cacheConditions()
  {
    clearConditionsCache();
    if(m_srcIdList.empty()){
      return ( Error("No Tell1 List cached!", StatusCode::FAILURE) );
    }

    // loop over all tell1s and cache conditions
    for( std::vector<unsigned int>::iterator iSrcId=m_srcIdList.begin(); iSrcId!=m_srcIdList.end(); ++iSrcId){

      unsigned int tell1=*(iSrcId);
      std::string stell1=boost::lexical_cast<std::string>(tell1);
      const DeVeloSensor* sensor = m_velo->sensorByTell1Id(tell1);
      if ( 0 == sensor ) {
        std::string message = "Could not map source ID '" + stell1 + "' to sensor.";
        return ( Error(message, StatusCode::FAILURE) );
      }
      std::string condName=m_condPath+"/VeloTELL1Board"+stell1;
      Condition* cond=getDet<Condition>(condName);

      if ( 0 == cond ) continue;

      std::vector<int> local;
      std::vector<int>::const_iterator iT;

      // hit thresholds
      local=cond->param<std::vector<int> >("hit_threshold");
      std::vector<unsigned char>& thresholds = m_hitThresholds[tell1] = std::vector<unsigned char>();
      thresholds.reserve(2304);
      for( iT=local.begin(); iT!=local.end(); ++iT){
        thresholds.push_back(static_cast<unsigned char>(*iT));
      }
      // hit thresholds without dummy strips. This is not trivial,
      // so we (ab)use the VeloTELL1Data implementation to achieve this.
      LHCb::VeloTELL1Data data(tell1,LHCb::VeloTELL1Data::VeloFull);
      unsigned int sensorType = sensor->isR()*LHCb::VeloTELL1Data::R_SENSOR
        + sensor->isPhi()*LHCb::VeloTELL1Data::PHI_SENSOR
        + sensor->isPileUp()*LHCb::VeloTELL1Data::PILE_UP;
      data.setSensorType(sensorType);
      data.setDecodedData(local);
      data.setIsReordered(true);
      std::vector<unsigned char>& thresholdsNoDummy = m_hitThresholdsNoDummy[tell1] = std::vector<unsigned char>(2048);
      for (unsigned int strip=0; strip<2048; ++strip) {
        thresholdsNoDummy[strip] = static_cast<unsigned char>(data.stripADC(strip));
      }

    }
    return StatusCode::SUCCESS;
  }

}
