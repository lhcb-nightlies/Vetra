// $Id: ConvTELL1ToSensor.h,v 1.3 2007-12-12 14:03:50 szumlat Exp $
#ifndef CONVTELL1TOSENSOR_H 
#define CONVTELL1TOSENSOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "VetraKernel/IConvTELL1ToSensor.h"            // Interface


/** @class ConvTELL1ToSensor ConvTELL1ToSensor.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2006-08-25
 */

class DeVelo;

class ConvTELL1ToSensor : public GaudiTool, virtual public IConvTELL1ToSensor{
public: 
  /// Standard constructor
  ConvTELL1ToSensor( const std::string& type, 
                     const std::string& name,
                     const IInterface* parent);

  virtual ~ConvTELL1ToSensor( ); ///< Destructor

  virtual unsigned int tell1ToSensor(unsigned int tell1) override;
  virtual StatusCode initialize() override;
  
protected:

private:

  DeVelo* m_veloDet;

};
#endif // CONVTELL1TOSENSOR_H
