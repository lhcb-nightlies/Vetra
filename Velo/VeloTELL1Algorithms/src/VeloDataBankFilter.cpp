// $Id: VeloDataBankFilter.cpp,v 1.1 2010-09-16 17:55:00 gersabec Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "VetraKernel/ContPrinter.h"
#include "Event/ProcStatus.h"

// local
#include "VeloDataBankFilter.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : VeloDataBankFilter
//
// 2010-09-16 : Marco Gersabeck
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloDataBankFilter )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloDataBankFilter::VeloDataBankFilter( const std::string& name,
                                ISvcLocator* pSvcLocator)
  : VeloTELL1Algorithm (name, pSvcLocator),
    m_evtNumber ( 0 )
{
  declareProperty("FilterNZS",      m_filterNZS      = true );
  declareProperty("PrescaleFactor", m_prescaleFactor = 1. );
}
//=============================================================================
// Destructor
//=============================================================================
VeloDataBankFilter::~VeloDataBankFilter() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloDataBankFilter::initialize() {
  StatusCode sc = VeloTELL1Algorithm::initialize(); // must be executed first
  // error printed already by VeloTELL1Algorithm
  if ( sc.isFailure() ) return sc;

  m_rsvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  m_rand.initialize( m_rsvc , Rndm::Flat(0.,1.0) ).ignore();

  m_isDebug = msgLevel( MSG::DEBUG );
  if(m_isDebug) debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloDataBankFilter::execute() {

  if(m_isDebug) debug() << "==> Execute" << endmsg;
  ++m_evtNumber;

  if ( m_filterNZS ) {
    StatusCode nzsSvcStatus=this->getNZSSvc();
    if(!nzsSvcStatus.isSuccess()) {
      error() << "Failed to get NZS service" << endmsg;
      setProcStat( MissingNZSBank, true );
      return ( StatusCode::SUCCESS );
    }
    if(!(this->nzsSvc()->isNZSStreamPresent())){
      setProcStat( MissingNZSBank, true );
      return StatusCode::SUCCESS;
    }
  }

  if ( ( m_prescaleFactor >= 0. ) && ( m_prescaleFactor < 1. ) ) {
    double random = m_rand();
    if ( random > m_prescaleFactor ) {
      setProcStat( Prescale, true );
      return StatusCode::SUCCESS;
    }
  } 
  
  setProcStat( OK, false );
  return StatusCode::SUCCESS;
}
//=============================================================================
// Set ProcStatus
//=============================================================================
void VeloDataBankFilter::setProcStat( int status, bool fail ) {

  LHCb::ProcStatus *pStat = getOrCreate<LHCb::ProcStatus,LHCb::ProcStatus>(LHCb::ProcStatusLocation::Default);
  pStat->addAlgorithmStatus( "VeloDataBankFilter", "VELO", "NZSPrescaleFilter", status, fail );

}
 
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloDataBankFilter::finalize() {

  if(m_isDebug) debug() << "==> Finalize" << endmsg;
  // must be called after all other actions
  return VeloTELL1Algorithm::finalize();
}
//=============================================================================
