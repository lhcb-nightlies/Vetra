// $Id: VeloTELL1ClusterMaker.h,v 1.10 2008-05-23 13:19:22 szumlat Exp $
#ifndef VELOTELL1CLUSTERMAKER_H 
#define VELOTELL1CLUSTERMAKER_H 1

// Include files
#include "VetraKernel/VeloTELL1Algorithm.h"
#include "DetDesc/Condition.h"
#include "GaudiKernel/IUpdateManagerSvc.h"

// event
#include "VeloEvent/VeloProcessInfo.h"
#include "Event/RawEvent.h"

// Si
#include "SiDAQ/SiHeaderWord.h"
#include "SiDAQ/SiRawBufferWord.h"

// local
#include "ClusterMakerThresholds.h"

/** @class VeloTELL1ClusterMaker VeloTELL1ClusterMaker.h
 *  
 *  The algorithm is an implementation of the tell1 cluster
 *  maker that is a part of the tell1lib library. The input 
 *  of the algorithm is the data after reordering and the output
 *  is the RawEvent structure with encoded clusters.
 *  Values of the thresholds are defined inside ClusterMakerThresholds.h
 *  the settings are most critical for the algorithm since
 *  the values are used to serch for seeding strips.
 *  At the end of the algorithm RawEvent structure with RawBanks
 *  containing zero suppressed data is created. The RawEvent is
 *  subsequently stored in TES and decoded to VeloLite and VeloClusters
 *  by VeloDAQ package.
 *
 *  @author Tomasz Szumlak
 *  @date   2006-10-06
 */

//using namespace LHCb;

class TELL1ZSProcessEngine;

class VeloTELL1ClusterMaker : public VeloTELL1Algorithm{

public:

  //typedef VeloTELL1::u_int32_t buffer_word;

  enum version{
    V2=2,
    V3=3
  };

  enum positionConst{
    POS_SHIFT=3,
    IS_POS_MASK=0x7,     /// inter strip position mask
    POS_MASK=0x7FF,
    BIT12_MAX=0x1000,
    BIT13_MAX=8192,
    BIT16_MAX=65536
  };

  /// Standard constructor
  VeloTELL1ClusterMaker(const std::string& name, ISvcLocator* pSvcLocator);

  virtual ~VeloTELL1ClusterMaker( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode runClusterMaker();
  StatusCode veloDataSuppression();
  void calculateClusterPosition(int chan, int cluSize, int firstStripInBlock);
  void addADCToList(int chan, int cluSize);
  StatusCode veloClusterization(int firstStripInBlock);
  void flushMemory();
  StatusCode fillAndWriteRawEvent();
  StatusCode storeBank();
  StatusCode createRawEvent();
  StatusCode validationRun();
  void prepareEngineForValidation();
  StatusCode createAndConfigureEngines();
  StatusCode i_cacheConditions() override;

private:

  LHCb::RawEvent* m_rawEvent;
  std::string m_rawEventLoc;
  int m_hitThresholdValue;
  int m_lowThresholdValue;
  VeloTELL1::u_int32_t m_sumThresholdValue;
  VeloTELL1::ThresholdsVec m_hitThresholds;
  VeloTELL1::ThresholdsVec m_lowThresholds;
  VeloTELL1::SumThresholdsVec m_sumThresholds;
  VeloTELL1::TELL1ADCVec m_adcs;
  VeloTELL1::ADC_MEMORY m_adcsMem;   /// list of adcs contributed to the clusters
  VeloTELL1::BoundaryStripVec m_boundaryStripsR;
  VeloTELL1::BoundaryStripVec m_boundaryStripsPhi;
  unsigned int m_zsProcessEnable;
  VeloTELL1::IntCondMap m_zsEnableMap;
  VeloTELL1::ThresholdsMap m_hitThresholdMap;
  VeloTELL1::ThresholdsMap m_lowThresholdMap;
  VeloTELL1::SumThresholdsMap m_sumThresholdMap;
  VeloTELL1::BoundaryStripMap m_boundaryStripMap;
  unsigned int m_eventNumber;
  VeloTELL1::dataVec m_sensors;      /// container with source numbers
  std::vector<SiDAQ::buffer_word> m_bankBody;
  int m_bankVersion;
  unsigned int m_bankBodySize;
  bool m_printInfo;
  bool m_validationRun;
  std::map<unsigned int, TELL1ZSProcessEngine*> m_zsEngines;
  VeloTELL1::TELL1ClusterVec m_clusters;
  VeloTELL1::CLUSTER_MEMORY m_clustersMem;  /// list of created clusters

};
#endif // VELOTELL1CLUSTERMAKER_H
