// $Id: VeloTELL1EmulatorInit.cpp,v 1.12 2010-05-10 16:36:21 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 

// local
#include "VeloTELL1EmulatorInit.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTELL1EmulatorInit
//
// 2006-07-10 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloTELL1EmulatorInit )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTELL1EmulatorInit::VeloTELL1EmulatorInit( const std::string& name,
                                              ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator ),
    m_evtInfos ( 0 ),
    m_evtInfoLoc ( EvtInfoLocation::Default ),
    m_mode ( 99 ),
    m_initInfo ( 0 ),
    m_initInfoLoc ( VeloProcessInfoLocation::Default ),
    m_realProc ( ),
    m_simProc ( ),
    m_devProc ( )
{
  declareProperty("SimProcessing", m_simProc);
  declareProperty("DevProcessing", m_devProc);
  declareProperty("PrintInfo", m_printInfo=true);
  declareProperty("FastPedSubtractor", m_fastSubtractor=1);
  declareProperty("ConvergenceLimit", 
                  m_convergenceLimit=VeloTELL1::FAST_CONVERGENCE_LIMIT);  
  declareProperty("RunType", m_runType=VeloTELL1::PROCESSING_RUN);
  declareProperty("DevRun", m_devRun=0);
}
//=============================================================================
// Destructor
//=============================================================================
VeloTELL1EmulatorInit::~VeloTELL1EmulatorInit() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloTELL1EmulatorInit::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloTELL1EmulatorInit::execute() {

  debug() << "==> Execute" << endmsg;
  //
  StatusCode dataStatus=getData();
  StatusCode initStatus;
  if(dataStatus.isSuccess()) initStatus=emulatorInit();
  //
  return initStatus;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloTELL1EmulatorInit::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode VeloTELL1EmulatorInit::getData()
{
  debug()<< " ==> getData() " <<endmsg;
  //
  if(!exist<EvtInfos>(m_evtInfoLoc)){
    // there is no evt bank, so we deal with simulation
    m_mode=VeloTELL1::SIM_DATA;
  }else{
    if(m_devRun){
      m_mode=VeloTELL1::DEV_DATA;
    }else{
      m_mode=VeloTELL1::REAL_DATA;
    }
    m_evtInfos=get<EvtInfos>(m_evtInfoLoc);
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1EmulatorInit::emulatorInit()
{
  debug()<< " ==> emulatorInit() " <<endmsg;
  //
  if(m_mode==VeloTELL1::SIM_DATA){
    if(m_simProc.size()){
      m_initInfo=new VeloProcessInfos();
      VeloProcessInfo* initInfo=new VeloProcessInfo();
      initInfo->setSimProcessInfo(m_simProc);
      initInfo->setProcType(m_mode);
      if(m_convergenceLimit<0){
        Error("Convergence Limit not set!", StatusCode::FAILURE);
      }
      initInfo->setConvLimit(m_convergenceLimit);
      // no test runs for simulation
      initInfo->setRunType(VeloTELL1::PROCESSING_RUN);
      m_initInfo->push_back(initInfo);
      put(m_initInfo, m_initInfoLoc);
    }else{
      debug()<< " ==> Processing info for SIM_DATA are not set! " <<endmsg;
      return ( StatusCode::FAILURE );
    }
  }else if(m_mode==VeloTELL1::REAL_DATA||m_mode==VeloTELL1::DEV_DATA){
    debug()<< " Process Infos for real data " <<endmsg;
    if(0==m_evtInfos->size()){
        debug()<< " --> No NZS data bank - postpone initialization " <<endmsg;
        return ( StatusCode::SUCCESS );
    }
    m_initInfo=new VeloProcessInfos();
    VeloProcessInfo* initInfo=new VeloProcessInfo();
    EvtInfos::const_iterator evtIt=m_evtInfos->begin();
    EvtInfo* evtInfo=(*evtIt);
    m_realProc=evtInfo->processInfo();
    initInfo->setDataProcessInfo(m_realProc);
    initInfo->setProcType(m_mode);
    //
    if(m_printInfo){
      debug()<< " ==> Process Info bits: ";
      for(int i=0; i<8; ++i){
        if((m_realProc>>i)&1){ debug()<< 1; }else{ debug()<< 0; }
      }
      debug()<<endmsg;
    }
    //
    if(m_convergenceLimit<0){
      Error("Convergence Limit not set!", StatusCode::FAILURE);
    }
    initInfo->setConvLimit(m_convergenceLimit);
    initInfo->setRunType(m_runType);

    if(m_devRun){
      initInfo->setDevProcessInfo(m_devProc);
      initInfo->setProcType(m_mode);
      debug()<< " mode: " << m_mode <<endmsg;
      debug()<< " dev proc size: " << m_devProc.size() <<endmsg;
    }

    m_initInfo->push_back(initInfo);
    put(m_initInfo, m_initInfoLoc);
  }else{
    error()<< " ==> Unknow data type! " <<endmsg;
    return ( StatusCode::FAILURE );
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloTELL1EmulatorInit::writeData()
{
  debug()<< " ==> writeData() " <<endmsg;
  //
  if(m_initInfo!=0) put(m_initInfo, m_initInfoLoc);
}
//
