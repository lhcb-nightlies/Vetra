// $Id: VeloTELL1LCMS.cpp,v 1.9 2009-11-17 19:28:25 szumlat Exp $
// Include files

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 

// stl
#include <vector>
#include <algorithm>

// local
#include "VeloTELL1LCMS.h"
#include "VetraKernel/ContPrinter.h"

// engine classes
#include "TELL1Engine/TELL1LCMSProcessEngine.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTELL1LCMS
//
// 2007-02-01 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloTELL1LCMS )

using namespace boost::assign;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTELL1LCMS::VeloTELL1LCMS( const std::string& name,
                              ISvcLocator* pSvcLocator)
  : VeloTELL1Algorithm( name , pSvcLocator ),
    m_evtNumber ( 0 ),
    m_lcmsEnableMap ( ),
    m_lcmsEngines ( )
{
  setTELL1Process(COMMON_MODE_SUBTRACTION);
  setAlgoType("TELL1 Linear Common Mode Subtraction");
  declareProperty("InputDataLoc",
                  m_inputDataLoc=LHCb::VeloTELL1DataLocation::ReorderedADCs);
  declareProperty("OutputDataLocation",
                  m_outputDataLoc=LHCb::VeloTELL1DataLocation::CMSuppressedADCs);
  declareProperty("AlgorithmName",
                  m_algorithmName="LCMS - after reordering");
  declareProperty("ValidationRun", m_validationRun=false);
  declareProperty("LCMSProcessEnable", m_lcmsProcessEnable=1);
  declareProperty("DBConfig", m_dbConfig=VeloTELL1::STATIC);
  declareProperty("SrcIdList", m_srcIdList);
}
//=============================================================================
// Destructor
//=============================================================================
VeloTELL1LCMS::~VeloTELL1LCMS() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloTELL1LCMS::initialize() {
  StatusCode sc = VeloTELL1Algorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;
  //
  if(m_dbConfig==VeloTELL1::DYNAMIC&&!m_validationRun){
    regUpdateTell1Conds();
    StatusCode mgrSvcStatus=runUpdate();
    if(mgrSvcStatus.isFailure()){
      return ( Error("Failed first UMS update", mgrSvcStatus) );
    }
  }else if(m_dbConfig==VeloTELL1::STATIC&&!m_validationRun){
    // initialize maps with static values
    m_lcmsEnableMap[0]=m_lcmsProcessEnable;
  }
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloTELL1LCMS::execute() {

  debug() << "==> Execute" << endmsg;
  //
  if(m_validationRun){
    StatusCode initStatus =  INIT();
    if ( !initStatus ) {
      debug() << "Unable to initialize.  NZS data missing?" << endmsg;
      return StatusCode::SUCCESS;
    } 
    prepareEngineForValidation();
    StatusCode exitStatus=validationRun();
    return ( exitStatus );
  }else{
    m_evtNumber++;
    StatusCode engineStatus;
    if(!isInitialized()){
      StatusCode initStatus =  INIT();
      if ( !initStatus ) {
        debug() << "Unable to initialize.  NZS data missing?" << endmsg;
        return StatusCode::SUCCESS;
      } 
      engineStatus=createAndConfigureEngines();
      if(m_forceEnable) this->setIsEnable(m_lcmsProcessEnable);
      if(isEnable()){
        info()<< " --> Algorithm " << (this->algoName())
              << " of type: " << (this->algoType())
              << " is enabled and ready to process data --" <<endmsg;
      }else{
        info()<< " --> Algorithm " << (this->algoName())
              << " is disabled! " <<endmsg;
      }
    }
    StatusCode dataStatus;
    if(isEnable()&&(m_evtNumber>convergenceLimit())){
      dataStatus=getData();
      if(dataStatus.isSuccess()&&engineStatus.isSuccess()){
        dataStatus=inputStream(inputData());
        if(dataStatus.isSuccess()){
          runLCMSuppression();
          writeData();
        }else{
          return ( StatusCode::SUCCESS );
        }
      }else{
        return StatusCode::SUCCESS;
      }
    }else{
      dataStatus=getData();
      if(dataStatus.isSuccess()){
        cloneData();
      }else{
        return StatusCode::SUCCESS;
      }
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloTELL1LCMS::finalize() {

  debug() << "==> Finalize" << endmsg;
  //
  if(0!=m_lcmsEngines.size()){
    
    std::map<unsigned int, TELL1LCMSProcessEngine*>::iterator lIt;
    lIt=m_lcmsEngines.begin();
    for( ; lIt!=m_lcmsEngines.end(); ++lIt){
      delete lIt->second;
    }
    
  }
  //
  return VeloTELL1Algorithm::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode VeloTELL1LCMS::runLCMSuppression()
{
  debug()<< " runLCMSuppression() " <<endmsg;
  //
  LHCb::VeloTELL1Datas::const_iterator sensIt;
  sensIt=inputData()->begin();
  // 
  for( ; sensIt!=inputData()->end(); ++sensIt){
    // get pointer to the velo sensor related with given TELL1
    const unsigned int tell1=static_cast<unsigned int>((*sensIt)->key());
    const DeVeloSensor* sens=deVelo()->sensorByTell1Id(tell1);
    if(sens==0){
      Error(" ==> No match between TELL1 and sensor found! ");
      Error(" ==> Check your XML conditions file! ");
      return ( StatusCode::FAILURE );
    }

    StatusCode nzsSvcStatus=this->getNZSSvc();
    if(!nzsSvcStatus.isSuccess()) return ( StatusCode::FAILURE );
    if(this->nzsSvc()->counter(tell1)<=convergenceLimit()) continue;

    unsigned int sensNb=sens->sensorNumber();
    debug()<< " sensor number: " << sensNb << " for TELL1 number: "
          << tell1 <<endmsg;
    // check if the sensor is read out
    if(sens->isReadOut()){
      // create a new VeloTELL1Data object for cm suppressesd data
      LHCb::VeloTELL1Data* lcmsData=
        new LHCb::VeloTELL1Data(tell1, VeloTELL1::VeloFull);
      if((*sensIt)->isReordered()){
        propagateReorderingInfo(*sensIt, lcmsData);
      }
      // data from current sensor
      VeloTELL1::sdataVec inData=(*sensIt)->data();
      m_lcmsEngines[tell1]->setInData(inData);
      m_lcmsEngines[tell1]->runLCMS();
      lcmsData->setDecodedData(m_lcmsEngines[tell1]->outData());
      // store the corected data
      outputData(lcmsData);
    }
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1LCMS::validationRun()
{
  info()<< " validationRun() " <<endmsg;
  // get test data from files
  if(isEnable()){
    info()<< " enabled " <<endmsg;
  }
  VeloTELL1::sdataVec inData, outData;
  StatusCode dataStatus=readDataFromFile("d_source.txt", inData);
  if(!dataStatus.isSuccess()){
    return ( dataStatus );
  }
  m_lcmsEngines[0]->setInData(inData);
  m_lcmsEngines[0]->runLCMS();
  outData=m_lcmsEngines[0]->outData();
  writeDataToFile("d_lcms_sub.txt", outData);
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloTELL1LCMS::prepareEngineForValidation()
{
  m_lcmsEngines[0]=new TELL1LCMSProcessEngine();
  m_lcmsEngines[0]->setProcessEnable(m_lcmsProcessEnable);
}
//=============================================================================
StatusCode VeloTELL1LCMS::createAndConfigureEngines()
{
  debug()<< " ==> prepareEngine() "<<endmsg;
  // pedestal engine
  VeloTELL1::AListPair idList=this->getSrcIdList();
  VeloTELL1::ucIT iT=idList.first;
  if(iT==idList.second){
    return Error(" ==> Empty list with src Ids!");
  }else{
    // create engines and put them into the map with tell1 as keys
    if(m_dbConfig==VeloTELL1::STATIC){
      info()<< " --> Creating static config for LCMS! " <<endmsg;
      for( ; iT!=idList.second; ++iT){
        m_lcmsEngines[*iT]=new TELL1LCMSProcessEngine();
        m_lcmsEngines[*iT]->setProcessEnable(m_lcmsEnableMap[0]);
      }
    }else if(m_dbConfig==VeloTELL1::DYNAMIC){
      // all parameters are already cached - use them to config engines
      info()<< " --> Creating dynamic config for LCMS! " <<endmsg;
      for( ; iT!=idList.second; ++iT){
        // pedestal processing objects
        m_lcmsEngines[*iT]=new TELL1LCMSProcessEngine();
        m_lcmsEngines[*iT]->setProcessEnable(m_lcmsEnableMap[*iT]);
      }
    }else{
      return ( Error(" ==> Unknown configuration mode! ") );
    }
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1LCMS::i_cacheConditions()
{
  debug()<< " ==> i_cacheCondition() " <<endmsg;
  VeloTELL1::AListPair iniList=getSrcIdList();
  if(iniList.first==iniList.second){
    return ( Error(" --> No Tell1 List cached!", StatusCode::FAILURE) );
  }
  VeloTELL1::ucIT srcId=iniList.first;
  // loop over all tell1s and initialize conditions
  for( ; srcId!=iniList.second; ++srcId){
    unsigned int tell1=*(srcId);
    std::string stell1=boost::lexical_cast<std::string>(tell1);
    std::string condName=m_condPath+"/VeloTELL1Board"+stell1;
    Condition* cond=getDet<Condition>(condName);
    // enable flag for pedestal process
    m_lcmsEnableMap[tell1]=cond->param<int>("cm_enable");
  }
  return ( StatusCode::SUCCESS );
}
//--
