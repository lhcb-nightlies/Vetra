// $Id: VeloTELL1FIRFilter.h,v 1.4 2008-05-23 13:19:22 szumlat Exp $
#ifndef VELOTELL1FIRFILTER_H 
#define VELOTELL1FIRFILTER_H 1

// Include files
// TELL1 algorithm base class
#include "VetraKernel/VeloTELL1Algorithm.h"

/** @class VeloTELL1FIRFilter VeloTELL1FIRFilter.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2007-01-29
 */

class TELL1FIRProcessEngine;

class VeloTELL1FIRFilter: public VeloTELL1Algorithm {
public: 

  /// Standard constructor
  VeloTELL1FIRFilter( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloTELL1FIRFilter( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode createAndConfigureEngines();
  StatusCode runFIRFilter();
  StatusCode i_cacheConditions() override;

private:
  
  unsigned int m_firProcessEnable;
  VeloTELL1::FIRCoeffVec m_firCoeffVec;
  VeloTELL1::IntCondMap m_firEnableMap; 
  VeloTELL1::FIRCoeffMap m_firCoeffMap;
  std::map<unsigned int, TELL1FIRProcessEngine*> m_firEngines;

};
#endif // VELOTELL1FIRFILTER_H
