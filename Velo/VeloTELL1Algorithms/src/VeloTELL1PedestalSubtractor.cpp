// $Id: VeloTELL1PedestalSubtractor.cpp,v 1.20 2009-12-11 12:21:06 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 

// stl
#include <vector>
#include <algorithm>

// local
#include "VetraKernel/ContPrinter.h"
#include "VeloTELL1PedestalSubtractor.h"

// engine classes
#include "TELL1Engine/TELL1PedestalProcessEngine.h"
#include "TELL1Engine/TELL1UpdateProcessEngine.h"

#include "VetraKernel/TableInOut.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTELL1PedestalSubtractor
//
// 2007-02-08 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloTELL1PedestalSubtractor )

using namespace boost::assign;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTELL1PedestalSubtractor::VeloTELL1PedestalSubtractor(
                           const std::string& name, ISvcLocator* pSvcLocator)
  : VeloTELL1Algorithm ( name , pSvcLocator ),
    m_headersCont ( 0 ),
    m_headersLoc ( LHCb::VeloTELL1DataLocation::Headers ),
    m_sensorData ( ),
    m_pedestalLine ( 2048, PEDESTAL ),
    m_pedestalSumMemory ( ),
    m_subPeds ( 0 ),
    m_subPedsLoc ( LHCb::VeloTELL1DataLocation::SubPeds ),
    m_pedestalEngines ( ),
    m_linkPedestal ( 2048, PEDESTAL ),
    m_linkMask ( 2048, 0 ),
    m_headers ( ),
    m_updateEngines ( ),
    m_pedestalEnableMap ( ),
    m_scalingModeMap ( ),
    m_zsEnableMap ( ),
    m_headerCorrEnableMap ( ),
    m_linkMaskMap ( ),
    m_updateEnableMap ( ),
    m_headerThresholdsMap ( ),
    m_headerCorrValuesMap ( ),
    m_excludedTell1List ( ),
    m_memoryBuilt ( false ),
    m_scramblerBuffer ( )
{
  setTELL1Process(PED_SUBTRACTION);
  setAlgoName("PEDESTAL_SUBTRACTION");
  setAlgoType("TELL1 Pedestal Subtraction");
  m_headerThresholdsVec+=530, 494;
  declareProperty("SubtractConstPedestal", m_subtractConstPedestal=false);
  declareProperty("SubtractPedestal", m_subtractPedestal=true);
  declareProperty("InputDataLoc", m_inputDataLoc="Raw/Velo/PreparedADCs");
  declareProperty("OutputDataLoc",
    m_outputDataLoc=LHCb::VeloTELL1DataLocation::PedSubADCs);
  declareProperty("ScalingMode", m_scalingMode=CUT_MSB);
  declareProperty("ValidationRun", m_validationRun=false);
  declareProperty("HeaderCorrection", m_headerCorrection=0);
  declareProperty("DBConfig", m_dbConfig=VeloTELL1::STATIC);
  declareProperty("PedestalProcessEnable", m_pedestalProcessEnable=1);
  declareProperty("UpdateProcessEnable", m_updateProcessEnable=1);
  declareProperty("CheckALink", m_checkALink=0);
  declareProperty("SrcIdList", m_srcIdList);
  declareProperty("ExcludedTell1List", m_excludedTell1List);
  declareProperty("PileUp", m_pileUp=false);
  m_scramblerPattern+=3, 2, 1, 0;
  declareProperty("IgnoreMasks", m_ignoreMasks = false);
}
//=============================================================================
// Destructor
//=============================================================================
VeloTELL1PedestalSubtractor::~VeloTELL1PedestalSubtractor() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloTELL1PedestalSubtractor::initialize() {
  StatusCode sc = VeloTELL1Algorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if(m_isDebug) debug() << "==> Initialize" << endmsg;
  //
  if(m_headerCorrValuesVec.size()==0){
    for(unsigned int cnt=0; cnt<64; ++cnt){
      m_headerCorrValuesVec.push_back(1);
      m_headerCorrValuesVec.push_back(-1);
    }
  }
  //
  if(m_dbConfig==VeloTELL1::DYNAMIC&&!m_validationRun){
    regUpdateTell1Conds();
    StatusCode mgrSvcStatus=runUpdate();
    if(mgrSvcStatus.isFailure()){
      return ( Error("Failed first UMS update", mgrSvcStatus) );
    }
  }else if(m_dbConfig==VeloTELL1::STATIC&&!m_validationRun){
    // initialize maps with static values
    m_pedestalEnableMap[0]=m_pedestalProcessEnable;
    m_scalingModeMap[0]=m_scalingMode;
    m_zsEnableMap[0]=m_pedestalProcessEnable;
    m_headerCorrEnableMap[0]=m_headerCorrection;
    m_linkMaskMap[0]=m_linkMask;
    m_headerThresholdsMap[0]=m_headerThresholdsVec;
    m_headerCorrValuesMap[0]=m_headerCorrValuesVec;
    m_updateEnableMap[0]=m_updateProcessEnable;
  }
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloTELL1PedestalSubtractor::execute() {

  if(m_isDebug) debug() << "==> Execute" << endmsg;
  if(m_validationRun){
    prepareEngineForValidation();
    StatusCode exitStatus=validationRun();
    return ( exitStatus );
  }else{
    m_evtNumber++;
    StatusCode engineStatus;
    StatusCode initStatus;
    if(!isInitialized()){
      initStatus=INIT();
      if(!initStatus.isSuccess()){
        if(m_isDebug) debug() << "Unable to initialize.  NZS data missing?" << endmsg;
        return ( StatusCode::SUCCESS );
      } 
      engineStatus=createAndConfigureEngines();
      if(m_forceEnable) this->setIsEnable(m_pedestalProcessEnable);
      if(this->isEnable()){
        info()<< " --> Algorithm " << (this->algoName())
              << " of type: " << (this->algoType())
              << " is enabled and ready to process data --" <<endmsg;
        info()<< " -------------------------------------------------- " <<endmsg;
        info()<< " --> The Convergence Limit is set to: " << this->convergenceLimit()   
              << " events \n" <<endmsg;
        info()<< " -------------------------------------------------- " <<endmsg;
      }else{
        info()<< " --> Algorithm " << (this->algoName())
              << " is disabled! " <<endmsg;
      }
    }
    StatusCode dataStatus=getData();
    if(dataStatus.isSuccess()){      
      StatusCode nzsSvcStatus=this->getNZSSvc();
      if(!nzsSvcStatus.isSuccess()) return ( StatusCode::FAILURE );
      StatusCode headStatus=getHeaders();
      StatusCode bankStatus=checkBanks();
      StatusCode pedStatus;
      if(bankStatus.isFailure())
        // this change has been introduced to account for the unstable DAQ
        // once the system is stable it should go back to the previous state
        // return ( Error(" ==> NUmber of NZS banks changed! ", bankStatus) );
        return ( Warning(" ==> File content and DB description is not the same! ",
                         bankStatus) );
      if(this->isEnable()&&engineStatus.isSuccess()){
        if(dataStatus.isSuccess()&&headStatus.isSuccess()){
          dataStatus=inputStream(inputData());
          if(dataStatus.isSuccess()){          /// check if not empty
            pedStatus=runPedestalSub();
            if(pedStatus.isFailure()) return ( pedStatus );
            writeData();
            writePedestals();
          }else{
            return ( StatusCode::SUCCESS );
          }
        }else{
          return StatusCode::SUCCESS;
        }
      }else{
        if(dataStatus.isSuccess()){
          cloneData();
        }else{
          return StatusCode::SUCCESS;
        }
      }
    }
  }
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloTELL1PedestalSubtractor::finalize() {

  if(m_isDebug) debug() << "==> Finalize" << endmsg;
  // delete all engine objects
  // create engines and put them into the map with tell1 as keys
  if(0!=m_pedestalEngines.size()){

    std::map<unsigned int, TELL1PedestalProcessEngine*>::iterator pEIt;
    pEIt=m_pedestalEngines.begin();
    for( ; pEIt!=m_pedestalEngines.end(); ++pEIt){
      delete pEIt->second;
    }

  }

  if(0!=m_updateEngines.size()){
    
    std::map<unsigned int, TELL1UpdateProcessEngine*>::iterator uEIt;
    uEIt=m_updateEngines.begin();
    for( ; uEIt!=m_updateEngines.end(); ++uEIt){
      delete uEIt->second;
    }
    
  }
  
  // must be called after all other actions
  return VeloTELL1Algorithm::finalize();
}
//=============================================================================
StatusCode VeloTELL1PedestalSubtractor::getHeaders()
{
  if(m_isDebug) debug()<< " ==> getHeaders() " <<endmsg;
  if(!exist<LHCb::VeloTELL1Datas>(m_headersLoc)){
    if(m_isDebug) debug()<< " ==> There is no header data at: "
           << m_headersLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{  
    // get data banks from default TES location
    m_headersCont=get<LHCb::VeloTELL1Datas>(m_headersLoc);
    if(m_isDebug) debug()<< " ==> The headers have been read-in from location: "
           << m_headersLoc
           << ", size of the header container: "
           << m_headersCont->size() <<endmsg;
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1PedestalSubtractor::runPedestalSub()
{
  if(m_isDebug) debug()<< " runPedestalSub() " <<endmsg;
  //
  LHCb::VeloTELL1Datas::const_iterator sensIt;
  sensIt=inputData()->begin();
  // variable used to count sensors
  unsigned int sensCnt=0;
  // create output container for pedestals if ped algorithm in use
  // and prepare memory for pedestal sum
  if(m_subtractPedestal){
    if(!m_memoryBuilt) prepareMemory();
    m_subPeds=new LHCb::VeloTELL1Datas();
    if(m_isDebug) debug()<< " memory prepared " <<endmsg;
  }
  //
  for( ; sensIt!=inputData()->end(); ++sensIt){
    // get pointer to the velo sensor related with given TELL1
    //const unsigned int tell1=static_cast<unsigned int>((*sensIt)->key());
    const int tell1=((*sensIt)->key());
    const DeVeloSensor* sens=deVelo()->sensorByTell1Id(tell1);
    if(tell1>VELO_LAST_SENSOR) continue;
    if(m_isDebug) debug()<< " TELL1 number: " << tell1 <<endmsg;
    if(m_isDebug) debug()<< " TELL1 number: " << tell1 <<endmsg;
    if(sens==0){
      Warning(" ==> No match between TELL1 and sensor found! ");
      Warning(" ==> Check your XML conditions file! ");
      return ( StatusCode::FAILURE );
      //continue;
    }
    unsigned int sensNb=sens->sensorNumber();
    if(m_isDebug) debug()<< " sensor number: " << sensNb << " for TELL1 number: "
          << tell1 <<endmsg;
    // check if the sensor is read out
    VeloTELL1::dataVec::const_iterator iT;
    iT=std::find(m_excludedTell1List.begin(),
                 m_excludedTell1List.end(), tell1);
    if(iT!=m_excludedTell1List.end()) continue;
    if(sens->isReadOut()){
      // data from current sensor
      m_sensorData.clear();
      m_sensorData=(*sensIt)->data();
      // Beetle headers
      m_headers.clear();
      LHCb::VeloTELL1Data* aHeader=headers()->object(tell1);
      m_headers=aHeader->data();
      assert(m_sensorData.size()==ALL_STRIPS);
      // pedestal subtraction - const pedestal value
      if(m_subtractConstPedestal) subtractConstPedestal(tell1);
      // pedestal subtraction - const pedestal value
      ///
      // pedestal subtraction - weighted average
      if(m_subtractPedestal) subtractPedestal(tell1);
      // pedestal subtraction - weighted average
    }
    ++sensCnt;
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1PedestalSubtractor::subtractConstPedestal(
                                        const unsigned int tell1)
{
  if(m_isDebug) debug()<< " subtractConstPedestal() " <<endmsg;
  // for the time being just const pedestal subtraction
  // is implemented according to tell1lib_v1.9.2
  sdataVec subPedestals (2304, 0);
  std::transform(m_sensorData.begin(), m_sensorData.end(),
                 m_pedestalLine.begin(),
                 subPedestals.begin(),
                 std::minus<signed int>());
  // data scaling -> mode 0
  sdatIt iT=subPedestals.begin();
  for( ; iT!=subPedestals.end(); ++iT){
    if((*iT)>MAX_ADC_8_BIT) (*iT)=MAX_ADC_8_BIT;
    else if((*iT)<(-MIN_ADC_8_BIT)) (*iT)=(-MAX_ADC_8_BIT);
  }
  // create a new VeloTELL1Data object for subtracted data
  LHCb::VeloTELL1Data* subData=new LHCb::VeloTELL1Data(tell1, VeloTELL1::VeloFull);
  subData->setDecodedData(subPedestals);
  outputData(subData);
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1PedestalSubtractor::subtractPedestal(
           const unsigned int tell1)
{
  if(m_isDebug) debug()<< " ==> subractPedestal() " <<endmsg;
  //
  LHCb::VeloTELL1Data* subPeds=new LHCb::VeloTELL1Data(tell1, VeloTELL1::VeloPedestal);
  LHCb::VeloTELL1Data* subData=new LHCb::VeloTELL1Data(tell1, VeloTELL1::VeloFull);
  // subPedADCs - data after correction
  // peds       - subtracted pedestals
  VeloTELL1::sdataVec peds(2048, 0);
  // get sum of the pedestals
  //lpIT lIT=m_linkPedestal.begin();
  m_linkPedestal.clear();
  sdataVec& pedestalSum=m_pedestalSumMemory[tell1];
  sdatIt psIT=pedestalSum.begin();
  // transform pedestal sum into pedestals
  int chan=0;
  for( ; psIT!=pedestalSum.end(); ++psIT){
    m_linkPedestal.push_back((*psIT)>>PED_SUM_SHIFT);
    chan++;
  }

  if(tell1==30&&m_evtNumber>=convergenceLimit()){
    debug()<< " evt number: " << m_evtNumber <<endmsg;
    debug()<< " Subtracted pedestals " <<endmsg;
    for(int chan=0; chan<32; ++chan){
      debug()<< " ped: " << m_linkPedestal[chan] << " for chan: " << chan <<endmsg;
    }
  }

  // subtract pedestals that were calculated for the previous event
  // pass input data to the engine object
  m_pedestalEngines[tell1]->setInData(m_sensorData);
  // insert dummy channels into headers vector - engine requirement
  VeloTELL1::sdataVec engineHeaders=m_headers;
  VeloTELL1::sdataVec::iterator start;
  VeloTELL1::sdataVec dummyHeaders(8, 0);
  for(int pp=0; pp<4; ++pp){
    start=engineHeaders.begin()+(pp+1)*64+pp*8;
    engineHeaders.insert(start, dummyHeaders.begin(), dummyHeaders.end());
  }
  m_pedestalEngines[tell1]->setHeaders(engineHeaders);
  m_pedestalEngines[tell1]->setLinkPedestal(m_linkPedestal);

  // the last resort is dump the engine guts - it will be a lot of stuff
  if(0){
    m_pedestalEngines[tell1]->setPrintDetails(1);
    m_pedestalEngines[tell1]->state();
  }

  m_pedestalEngines[tell1]->runSubtraction();
  // translate pedestals from u_int16_t into signed int
  lpIT lIT=m_linkPedestal.begin();
  VeloTELL1::sdataVec::iterator pIT=peds.begin();
  for( ; lIT!=m_linkPedestal.end(); ++lIT){
    *pIT++=static_cast<signed int>(*lIT);
  }
  // switch off the data storage if in validation mode
  if(!m_validationRun){
    subData->setDecodedData(m_pedestalEngines[tell1]->outData());
    if(m_checkALink){
      for(unsigned int sample=0; sample<2; ++sample){
        VeloTELL1::ALinkPair begEnd=(*subData)[sample];
        info()<< " Retrieved data from module " <<endmsg;
        unsigned int chan=0;
        VeloTELL1::scdatIt iT;
        for(iT=begEnd.first; iT!=begEnd.second; ++iT, ++chan){
          std::cout<< (*iT);
          std::cout<< ", ";
          if(chan%16==15&&chan!=0) std::cout<<std::endl;
        }
      }
    }
    subPeds->setDecodedData(peds);
    outputData(subData);
    m_subPeds->insert(subPeds);
  }
  // calculate pedestals for the next event by refreshing ped sum
  //if(m_evtNumber<=convergenceLimit()){
  if(this->nzsSvc()->counter(tell1)<=convergenceLimit()){
    m_updateEngines[tell1]->setInData(m_sensorData);
    m_updateEngines[tell1]->setHeaders(m_headers);
    m_updateEngines[tell1]->setPedestalSum(pedestalSum);
    m_updateEngines[tell1]->runUpdate();
  }
  // write out text files if running in validation mode
  if(m_validationRun){
    writeDataToFile("p_file.txt", m_pedestalSumMemory[tell1]);
    writeDataToFile("d_ped_sub.txt", m_pedestalEngines[tell1]->outData());
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1PedestalSubtractor::prepareMemory()
{
  if(m_isDebug) debug()<< " ==> prepareMemory() " <<endmsg;
  if(m_validationRun){
    // create storage space just for data from one sensor
    sensorRAM pedSumRAM;
    m_pedestalSumMemory[0]=pedSumRAM;
  }else{
    VeloTELL1::AListPair idList=this->getSrcIdList();
    std::string head=" ------------------ Mapping Table ------------------ ";
    VeloTELL1::ucIT srcId=idList.first;
    
    info() << head << endmsg;
    info() << " --> There are " << (inputData()->size()) << " NZS banks"
             << " in the input file. "<< endmsg;
    
    for( ; srcId!=idList.second; ++srcId){
      // get pointer to the velo sensor related with given TELL1
      const DeVeloSensor* sens=deVelo()->sensorByTell1Id((*srcId));
    
      info() << " --> TELL1 number: " << (*srcId);
    
      if(sens==0){
        Error(" ==> No match between TELL1 and sensor found! ");
        Error(" ==> Check your XML conditions file! ");
        return ( StatusCode::FAILURE );
      }
      unsigned int sensNb=sens->sensorNumber();
    
      info() << " mapped to sensor number: " << sensNb << endmsg;
    
      if(m_dbConfig==VeloTELL1::STATIC){
        sensorRAM pedSumRAM(2048, SUM_RAM_INIT);
        // add dummy strips
        //prepareData(pedSumRAM);
        m_pedestalSumMemory[*srcId]=pedSumRAM;
      }
    }
 
    info() << head << endmsg;
  }
  m_memoryBuilt=true;
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1PedestalSubtractor::writePedestals()
{
  if(m_isDebug) debug()<< " ==> writePedestals() " <<endmsg;
  //
  put(m_subPeds, m_subPedsLoc);
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1PedestalSubtractor::validationRun()
{
  if(m_isDebug) debug()<< " validationRun() " <<endmsg;
  // prepare memory for generated data - we will emulate just one sensor
  // the member variable m_validationRun will be checked
  prepareMemory();
  // read data and pedestals
  m_sensorData.clear();
  StatusCode dataStatus;
  dataStatus=readDataFromFile("d_source.txt", m_sensorData);
  if(!dataStatus.isSuccess()){
    return ( dataStatus );
  }
  dataStatus=readDataFromFile("pp_file.txt", m_pedestalSumMemory[0]);
  if(!dataStatus.isSuccess()){
    return ( dataStatus );
  }
  StatusCode testRunStatus=subtractPedestal(0);
  //
  return ( testRunStatus );
}
//=============================================================================
void VeloTELL1PedestalSubtractor::extractPedestals()
{
  if(m_isDebug) debug()<< " ==> extractPedestals() " <<endmsg;
  // use this method to extract pedestals - validation run
  unsigned int proc=0;
  lpIT iT1, iT2;
  LinkPedestalVec temp;
  while(proc<NumberOfPPFPGA){
    iT1=m_linkPedestal.begin()+proc*(STRIPS_IN_PPFPGA+DUMMY_STRIPS);
    iT2=iT1+(STRIPS_IN_PPFPGA);
    for( ; iT1!=iT2; ++iT1){
      temp.push_back(*iT1);
    }
    ++proc;
  }
  assert(temp.size()==SENSOR_CHANNELS);
  m_linkPedestal=temp;
}
//=============================================================================
StatusCode VeloTELL1PedestalSubtractor::createAndConfigureEngines()
{
  if(m_isDebug) debug()<< " ==> createAndConfigureEngines() "<<endmsg;
  // pedestal engine
  VeloTELL1::AListPair idList=this->getSrcIdList();
  VeloTELL1::ucIT iT=idList.first;
  if(iT==idList.second){
    return Error(" ==> Empty list with src Ids!");
  }else{
    // create engines and put them into the map with tell1 as keys
    if(m_dbConfig==VeloTELL1::STATIC){
      info()<< " --> Creating static config for Pedestal Following! " <<endmsg;
      for( ; iT!=idList.second; ++iT){
        m_pedestalEngines[*iT]=new TELL1PedestalProcessEngine();
        m_pedestalEngines[*iT]->setProcessEnable(m_pedestalEnableMap[0]);
        m_pedestalEngines[*iT]->setScalingMode(m_scalingModeMap[0]);
        m_pedestalEngines[*iT]->setHeaderCorrectionFlag(m_headerCorrEnableMap[0]);
        m_pedestalEngines[*iT]->setZSEnable(m_zsEnableMap[0]);
        m_pedestalEngines[*iT]->setLinkMask(m_linkMaskMap[0]);
        m_pedestalEngines[*iT]->setHeaderThresholds(m_headerThresholdsMap[0]);
        m_pedestalEngines[*iT]->setHeaderCorrValues(m_headerCorrValuesMap[0]);
        // update engine - end of pedestal following cycle
        m_updateEngines[*iT]=new TELL1UpdateProcessEngine();
        m_updateEngines[*iT]->setProcessEnable(m_updateEnableMap[0]);
        m_updateEngines[*iT]->setHeaderCorrectionFlag(m_headerCorrEnableMap[0]);
        m_updateEngines[*iT]->setLinkMask(m_linkMaskMap[0]);
        m_updateEngines[*iT]->setHeaderThresholds(m_headerThresholdsMap[0]);
        m_updateEngines[*iT]->setHeaderCorrValues(m_headerCorrValuesMap[0]);
      }
    }else if(m_dbConfig==VeloTELL1::DYNAMIC){
      // all parameters are already cached - use them to config engines
      info()<< " --> Creating dynamic config for Pedestal Following! " <<endmsg;
      for( ; iT!=idList.second; ++iT){
        // pedestal processing objects
        m_pedestalEngines[*iT]=new TELL1PedestalProcessEngine();
        m_pedestalEngines[*iT]->setProcessEnable(m_pedestalEnableMap[*iT]);
        m_pedestalEngines[*iT]->setScalingMode(m_scalingModeMap[*iT]);
        m_pedestalEngines[*iT]->setHeaderCorrectionFlag(m_headerCorrEnableMap[*iT]);
        m_pedestalEngines[*iT]->setZSEnable(m_zsEnableMap[*iT]);
        m_pedestalEngines[*iT]->setLinkMask(m_linkMaskMap[*iT]);
        m_pedestalEngines[*iT]->setHeaderThresholds(m_headerThresholdsMap[*iT]);
        m_pedestalEngines[*iT]->setHeaderCorrValues(m_headerCorrValuesMap[*iT]);
        // update processing objects
        m_updateEngines[*iT]=new TELL1UpdateProcessEngine();
        m_updateEngines[*iT]->setProcessEnable(m_updateEnableMap[*iT]);
        m_updateEngines[*iT]->setHeaderCorrectionFlag(m_headerCorrEnableMap[*iT]);
        m_updateEngines[*iT]->setHeaderThresholds(m_headerThresholdsMap[*iT]);
        m_updateEngines[*iT]->setHeaderCorrValues(m_headerCorrValuesMap[*iT]);
      }
    }else{
      return ( Error(" ==> Unknown configuration mode! ") );
    }
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloTELL1PedestalSubtractor::prepareEngineForValidation()
{
  if(m_isDebug) debug()<< " ==> prepareEngine() "<<endmsg;
  // create and prepare only one engine - this will be used to check
  // c module only - no dynamic configuration forseen
  // pedestal engine
  m_pedestalEngines[0]=new TELL1PedestalProcessEngine();
  m_pedestalEngines[0]->setProcessEnable(m_pedestalProcessEnable);
  m_pedestalEngines[0]->setScalingMode(m_scalingMode);
  m_pedestalEngines[0]->setHeaderCorrectionFlag(m_headerCorrection);
  m_pedestalEngines[0]->setZSEnable(this->isEnable());
  m_pedestalEngines[0]->setLinkMask(m_linkMask);
  // update engine - end of pedestal following cycle
  m_updateEngines[0]=new TELL1UpdateProcessEngine();
  m_updateEngines[0]->setProcessEnable(this->isEnable());
  m_updateEngines[0]->setHeaderCorrectionFlag(0);
}
//=============================================================================
inline LHCb::VeloTELL1Datas* VeloTELL1PedestalSubtractor::headers() const
{
  if(m_isDebug) debug()<< " ==> headers() " <<endmsg;
  //
  return ( m_headersCont );
}
//=============================================================================
inline StatusCode VeloTELL1PedestalSubtractor::checkBanks() const
{
  if(m_isDebug) debug()<< " ==> checkBanks() " <<endmsg;
  //
  StatusCode sc;
  if(inputData()->size()==m_srcIdList.size()){
    sc=StatusCode::SUCCESS;
  }else{
    if(inputData()->size()>m_srcIdList.size()+PILE_UP_SENSORS){
      info()<< " --> Input data contains more banks than the Cond DB! " 
            <<endmsg;
      sc=StatusCode::FAILURE;
    }else{
      // this will be the normal case during the round robin data taking
      if(m_isDebug) debug()<< " --> Difference between the number of banks in ";
      if(m_isDebug) debug()<< "data and Cond DB! " <<endmsg;
      sc=StatusCode::SUCCESS;
    }
  }
  //
  return ( sc );
}
//=============================================================================
StatusCode VeloTELL1PedestalSubtractor::i_cacheConditions()
{
  if(m_isDebug) debug()<< " ==> i_cacheCondition() " <<endmsg;
  // get the src id list first if empty fill it using VELOCOND DB
  VeloTELL1::AListPair iniList=this->getSrcIdList();
  if(iniList.first==iniList.second){
    return ( Error(" --> No Tell1 List cached!", StatusCode::FAILURE) );
  }
  VeloTELL1::ucIT srcId=iniList.first;
  // loop over all tell1s and initialize conditions
  std::vector<int> local, inputBuffer;
  std::vector<int>::const_iterator iT;
  std::vector<int>::iterator iTBuff;

  for( ; srcId!=iniList.second; ++srcId){
    unsigned int tell1=*(srcId);
    std::string stell1=boost::lexical_cast<std::string>(tell1);
    std::string condName=m_condPath+"/VeloTELL1Board"+stell1;
    Condition* cond=getDet<Condition>(condName);
    // enable flag for pedestal process
    m_pedestalEnableMap[tell1]=cond->param<int>("pedestal_enable");
    // scaling mode
    m_scalingModeMap[tell1]=cond->param<int>("scaling_mode");
    // zero suppression enable mask
    m_zsEnableMap[tell1]=cond->param<int>("zs_enable");
    // Beetle header X-Talk correction enable flag
    m_headerCorrEnableMap[tell1]=cond->param<int>("header_correction_enable");
    // thresholds for Beetle X-Talk
    local=cond->param<std::vector<int> >("header_corr_threshold");
    VeloTELL1::HeaderThresholdsVec thVec;
    iT=local.begin();
    for( ; iT!=local.end(); ++iT){
      thVec.push_back(static_cast<VeloTELL1::u_int32_t>(*iT));
    }
    m_headerThresholdsMap[tell1]=thVec;
    local.clear();
    // pedestal sum
    local=cond->param<std::vector<int> >("pedestal_sum");
    iT=local.begin();
    inputBuffer.resize(local.size());
    iTBuff=inputBuffer.begin();
    for( ; iT!=local.end(); iT++, iTBuff++){
      (*iTBuff)=(*iT)<<10;
    }
    m_pedestalSumMemory[tell1]=this->inputScrambler(inputBuffer);
    local.clear();
    inputBuffer.clear();
    // correction values for Beetle X-Talk
    local=cond->param<std::vector<int> >("header_corr_value");
    m_headerCorrValuesMap[tell1]=this->inputScrambler(local);
    // link mask
    local=cond->param<std::vector<int> >("link_mask");
    VeloTELL1::LinkMaskVec aLinkMaskVec(local.size());
    if (!m_ignoreMasks) {
      // swap cable order, cast & copy
      for (unsigned int tell1Channel=0; tell1Channel<local.size(); ++tell1Channel) {
	unsigned int chipChannel = (3 - tell1Channel/512)*512 + tell1Channel%512;
	aLinkMaskVec[chipChannel] = static_cast<VeloTELL1::u_int8_t>(local[tell1Channel]);
      } 
    }
    m_linkMaskMap[tell1] = m_ignoreMasks ? m_linkMask : aLinkMaskVec;
    // update enable map
    m_updateEnableMap[tell1]=cond->param<int>("pedestal_auto_update_enable");
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
std::vector<int>& VeloTELL1PedestalSubtractor::inputScrambler(std::vector<int>& input)
{
  if(m_isDebug) debug()<< " ==> inputScrambler() " <<endmsg;
  //
  const unsigned int size=static_cast<unsigned int>(input.size());
  const unsigned int cycle=size/4;
  m_scramblerBuffer.clear();
  m_scramblerBuffer.resize(size);
  std::vector<int>::const_iterator iT = input.begin();
  int counter=0;
  for( ; iT!=input.end(); ++iT){
    int tell1chan=counter;
    tell1chan=m_scramblerPattern[tell1chan/cycle]*cycle+tell1chan%cycle;
    m_scramblerBuffer[tell1chan]=(*iT);
    counter++;
  }
  
  return ( m_scramblerBuffer );
}

//--
