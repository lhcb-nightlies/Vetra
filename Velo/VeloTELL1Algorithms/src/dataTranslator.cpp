// $Id: dataTranslator.cpp,v 1.6 2009-11-17 19:28:25 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "VetraKernel/ContPrinter.h"

// local
#include "dataTranslator.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : dataTranslator
//
// 2007-08-14 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( dataTranslator )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
dataTranslator::dataTranslator( const std::string& name,
                                ISvcLocator* pSvcLocator)
  : VeloTELL1Algorithm (name, pSvcLocator),
    m_evtNumber ( 0 )
{
  declareProperty("InsertDummyAtEnd", m_insertDummyAtEnd=false);
  declareProperty("InputDataLoc", m_inputDataLoc=VeloTELL1DataLocation::ADCs);
  declareProperty("OutputDataLoc", m_outputDataLoc="Raw/Velo/PreparedADCs");
}
//=============================================================================
// Destructor
//=============================================================================
dataTranslator::~dataTranslator() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode dataTranslator::initialize() {
  StatusCode sc = VeloTELL1Algorithm::initialize(); // must be executed first
  // error printed already by VeloTELL1Algorithm
  if ( sc.isFailure() ) return sc;
  m_isDebug = msgLevel( MSG::DEBUG );
  if(m_isDebug) debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode dataTranslator::execute() {

  if(m_isDebug) debug() << "==> Execute" << endmsg;
  ++m_evtNumber;

  // --> check the presence of the NZS data, if not present exit
  if(!isInitialized()){
    StatusCode initStatus=INIT();
    if(!initStatus.isSuccess()){
      if(m_isDebug) debug() << "Unable to initialize.  NZS data missing?" << endmsg;
      return ( StatusCode::SUCCESS );
    }
  }
    
  // create input data that will conform to c-modules
  StatusCode dataStatus=getData();
  if(!dataStatus.isSuccess()){
    return ( StatusCode::SUCCESS );
  }

  LHCb::VeloTELL1Datas::const_iterator sensIt;
  sensIt=inputData()->begin();
  if(m_isDebug) debug()<< " input data size: " << ((*sensIt)->data().size()) <<endmsg;
    
  if(dataStatus.isSuccess()&&m_insertDummyAtEnd){

    prepareData(m_insertDummyAtEnd);

  }else if(dataStatus.isSuccess()){

    for( ; sensIt!=inputData()->end(); ++sensIt){

      // --> create new VeloTELL1Data object
      const unsigned int tell1=static_cast<unsigned int>((*sensIt)->key());
      LHCb::VeloTELL1Data* preparedData=NULL;
      preparedData=new LHCb::VeloTELL1Data(tell1, VeloTELL1::VeloFull);
      VeloTELL1::sdataVec& data=(*sensIt)->data();
      prepareData(data);
      assert(data.size()==VeloTELL1::ALL_STRIPS);
      preparedData->setDecodedData(data);
      outputData(preparedData);

    }

  }
  writeData();
  
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode dataTranslator::finalize() {

  if(m_isDebug) debug() << "==> Finalize" << endmsg;
  // must be called after all other actions
  return VeloTELL1Algorithm::finalize();
}
//=============================================================================
