// $Id: VeloTELL1FIRFilter.cpp,v 1.9 2009-11-17 19:28:25 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 

// local
#include "VeloTELL1FIRFilter.h"

// Kernel
#include "VetraKernel/ContPrinter.h"

// stl
#include <vector>
#include <algorithm>

// engine classes
#include "TELL1Engine/TELL1FIRProcessEngine.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTELL1FIRFilter
//
// 2007-01-30 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloTELL1FIRFilter )

using namespace boost::assign;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTELL1FIRFilter::VeloTELL1FIRFilter( const std::string& name,
                                        ISvcLocator* pSvcLocator)
  :  VeloTELL1Algorithm( name , pSvcLocator ),
     m_firCoeffVec ( 128, 0 ),
     m_firEnableMap ( ),
     m_firCoeffMap ( ),
     m_firEngines ( )
{
  setTELL1Process(FIR_FILTER);
  setAlgoName("FIR_FILTER");
  setAlgoType("TELL1 FIR Filter");
  declareProperty("InputDataLoc",
    m_inputDataLoc=LHCb::VeloTELL1DataLocation::PedSubADCs);
  declareProperty("OutputDataLoc",
    m_outputDataLoc=LHCb::VeloTELL1DataLocation::FIRCorrectedADCs);
  declareProperty("DBConfig", m_dbConfig=VeloTELL1::STATIC);
  declareProperty("FIRProcessEnable", m_firProcessEnable=0);
  declareProperty("SrcIdList", m_srcIdList);
  declareProperty("FIRCoeffVec", m_firCoeffVec);
}
//=============================================================================
// Destructor
//=============================================================================
VeloTELL1FIRFilter::~VeloTELL1FIRFilter() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloTELL1FIRFilter::initialize() {
  StatusCode sc=VeloTELL1Algorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if(m_isDebug) debug() << "==> Initialize" << endmsg;
  //
    if(m_dbConfig==VeloTELL1::DYNAMIC){
    regUpdateTell1Conds();
    StatusCode mgrSvcStatus=runUpdate();
    if(mgrSvcStatus.isFailure()){
      return ( Error("Failed first UMS update", mgrSvcStatus) );
    }
  }else if(m_dbConfig==VeloTELL1::STATIC){
    // initialize maps with static values
    m_firEnableMap[0]=m_firProcessEnable;
    m_firCoeffMap[0]=m_firCoeffVec;
  }
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloTELL1FIRFilter::execute() {

  if(m_isDebug) debug() << "==> Execute" << endmsg;
  //
  m_evtNumber++;
  StatusCode engineStatus;
  if(!isInitialized()){ 
    StatusCode initStatus = INIT();
    if ( !initStatus ) {
      if(m_isDebug) debug() << "Unable to initialize.  NZS data missing?" << endmsg;
      return StatusCode::SUCCESS;
    } 
    engineStatus=createAndConfigureEngines();
    if(m_forceEnable) this->setIsEnable(m_firProcessEnable);
    if(isEnable()){
      if(m_isDebug) debug()<< " --> Algorithm " << (this->algoName())
            << " of type: " << (this->algoType())
            << " is enabled and ready to process data --" <<endmsg;
    }else{
      if(m_isDebug) debug()<< " --> Algorithm " << (this->algoName())
            << " is disabled! " <<endmsg;
    }
  }
  StatusCode dataStatus=getData();
  if(isEnable()&&(m_evtNumber>convergenceLimit())){
    if(dataStatus.isSuccess()&&engineStatus.isSuccess()){
      dataStatus=inputStream(inputData());
      if(dataStatus.isSuccess()){          /// check if not empty
        runFIRFilter();
        writeData();
      }else{
        return ( StatusCode::SUCCESS );
      }
    }else{
      return StatusCode::SUCCESS;
    }
  }else{
    if(dataStatus.isSuccess()){
      cloneData();
    }else{
      return StatusCode::SUCCESS;
    }
  }
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloTELL1FIRFilter::finalize() {

  if(m_isDebug) debug() << "==> Finalize" << endmsg;
  //
  if(0!=m_firEngines.size()){
    
    std::map<unsigned int, TELL1FIRProcessEngine*>::iterator fIt;
    fIt=m_firEngines.begin();
    for( ; fIt!=m_firEngines.end(); ++fIt){
      delete fIt->second;
    }
    
  }
  // must be called after all other actions
  return VeloTELL1Algorithm::finalize();
}
//=============================================================================
StatusCode VeloTELL1FIRFilter::runFIRFilter()
{
  if(m_isDebug) debug()<< " ==> runFIRFilter() " <<endmsg;
  // iterate over all sensors
  LHCb::VeloTELL1Datas::const_iterator sensIt;
  sensIt=inputData()->begin();
  // 
  for( ; sensIt!=inputData()->end(); ++sensIt){
    // get pointer to the velo sensor related with given TELL1
    const unsigned int tell1=static_cast<unsigned int>((*sensIt)->key());

    StatusCode nzsSvcStatus=this->getNZSSvc();
    if(!nzsSvcStatus.isSuccess()) return ( StatusCode::FAILURE );
    if(this->nzsSvc()->counter(tell1)<=convergenceLimit()) continue;

    const DeVeloSensor* sens=deVelo()->sensorByTell1Id(tell1);
    if(sens==0){
      Error(" ==> No match between TELL1 and sensor found! ");
      Error(" ==> Check your XML conditions file! ");
      return ( StatusCode::FAILURE );
    }
    unsigned int sensNb=sens->sensorNumber();
    if(m_isDebug) debug()<< " sensor number: " << sensNb << " for TELL1 number: "
          << tell1 <<endmsg;
    // check if the sensor is read out
    if(sens->isReadOut()){
      // create a new VeloTELL1Data object for cm suppressesd data
      LHCb::VeloTELL1Data* firData=
        new LHCb::VeloTELL1Data(tell1, VeloTELL1::VeloFull);
      if((*sensIt)->isReordered()){
        propagateReorderingInfo(*sensIt, firData);
      }
      // data from current sensor
      VeloTELL1::sdataVec inData=(*sensIt)->data();
      m_firEngines[tell1]->setInData(inData);
      m_firEngines[tell1]->runFilter();
      firData->setDecodedData(m_firEngines[tell1]->outData());      
      // store the fir data
      outputData(firData);
    }
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1FIRFilter::createAndConfigureEngines()
{
  if(m_isDebug) debug()<< " prepareEngine() " <<endmsg;
  //
  VeloTELL1::AListPair idList=this->getSrcIdList();
  VeloTELL1::ucIT iT=idList.first;
  if(iT==idList.second){
    if(m_isDebug) debug() << " ==> Empty list with src Ids!" << endmsg;
    return StatusCode::FAILURE;
  }else{
    // create engines and put them into the map with tell1 as keys
    if(m_dbConfig==VeloTELL1::STATIC){
      info()<< " --> Creating static config for FIR Filter! " <<endmsg;
      for( ; iT!=idList.second; ++iT){
        m_firEngines[*iT]=new TELL1FIRProcessEngine();
        m_firEngines[*iT]->setProcessEnable(m_firEnableMap[0]);
        m_firEngines[*iT]->setFIRCoeff(m_firCoeffMap[0]);
      }
    }else if(m_dbConfig==VeloTELL1::DYNAMIC){
      // all parameters are already cached - use them to config engines
      info()<< " --> Creating dynamic config for FIR Filter! " <<endmsg;
      for( ; iT!=idList.second; ++iT){
        // pedestal processing objects
        m_firEngines[*iT]=new TELL1FIRProcessEngine();
        m_firEngines[*iT]->setProcessEnable(m_firEnableMap[*iT]);
        m_firEngines[*iT]->setFIRCoeff(m_firCoeffMap[*iT]);
      }
    }else{
      return ( Error(" ==> Unknown configuration mode! ") );
    }
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1FIRFilter::i_cacheConditions()
{
  if(m_isDebug) debug()<< " ==> i_cacheCondition() " <<endmsg;
  VeloTELL1::AListPair iniList=getSrcIdList();
  if(iniList.first==iniList.second){
    return ( Error(" --> No Tell1 List cached!", StatusCode::FAILURE) );
  }
  VeloTELL1::ucIT srcId=iniList.first;
  // loop over all tell1s and initialize conditions
  for( ; srcId!=iniList.second; ++srcId){
    unsigned int tell1=*(srcId);
    std::string stell1=boost::lexical_cast<std::string>(tell1);
    std::string condName=m_condPath+"/VeloTELL1Board"+stell1;
    Condition* cond=getDet<Condition>(condName);
    // enable flag for fir process
    m_firEnableMap[tell1]=cond->param<int>("fir_enable");
    // fir coefficients
    m_firCoeffMap[tell1]=cond->param<std::vector<int> >("fir_coefficient");
  }
  //
  return ( StatusCode::SUCCESS );
}
//--
