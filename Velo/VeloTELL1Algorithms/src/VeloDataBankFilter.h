// $Id: VeloDataBankFilter.h,v 1.1 2010-09-16 17:55:00 gersabec Exp $
#ifndef VELODATABANKFILTER_H 
#define VELODATABANKFILTER_H 1

// Include files
// from Gaudi
#include "VetraKernel/VeloTELL1Algorithm.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"

// form TELL1 Kernel
#include "Tell1Kernel/VeloDecodeCore.h"

/** @class VeloDataBankFilter VeloDataBankFilter.h
 *  
 *  This algorithm filters on the presence of NZS banks
 *  Alternatively or in addition it can be used to prescale the processing of a set of algorithms
 *  It can be used to execute NZS and ZS monitoring on the same events
 *
 *  @author Marco Gersabeck
 *  @date   2010-09-16
 */

class VeloDataBankFilter: public VeloTELL1Algorithm{
public: 
  /// Standard constructor
  VeloDataBankFilter( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloDataBankFilter( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

  enum AlgStatusType{ 
    OK = 0,
    MissingNZSBank = 1,
    Prescale = 2,
    Other = 99
  };

protected:

private:

  void setProcStat( int status, bool fail );

  bool m_isDebug;
  int m_evtNumber;
  IRndmGenSvc* m_rsvc;
  Rndm::Numbers m_rand;
  bool m_filterNZS;
  double m_prescaleFactor;

};
#endif // VELODATABANKFILTER_H
