// $Id: VeloTELL1LCMS.h,v 1.5 2008-05-23 13:19:22 szumlat Exp $
#ifndef VELOTELL1LCMS_H 
#define VELOTELL1LCMS_H 1

// Include files
#include "VetraKernel/VeloTELL1Algorithm.h"

/** @class VeloTELL1LCMS VeloTELL1LCMS.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2007-02-01
 */

class TELL1LCMSProcessEngine;

class VeloTELL1LCMS: public VeloTELL1Algorithm {

public: 

  enum lcms{
    CENTRE_CHANNEL=16
  };
  
  /// Standard constructor
  VeloTELL1LCMS( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloTELL1LCMS( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  void prepareEngineForValidation();
  StatusCode createAndConfigureEngines();
  StatusCode runLCMSuppression();
  StatusCode validationRun();
  StatusCode i_cacheConditions() override;

private:

  unsigned int m_evtNumber;
  unsigned int m_lcmsProcessEnable;
  unsigned int m_validationRun;
  VeloTELL1::IntCondMap m_lcmsEnableMap;
  std::map<unsigned int, TELL1LCMSProcessEngine*> m_lcmsEngines;

};
#endif // VELOTELL1LCMS_H
