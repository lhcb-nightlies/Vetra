// $Id: VeloTELL1Reordering.cpp,v 1.12 2009-11-17 19:28:25 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "DetDesc/Condition.h"
#include "GaudiKernel/IUpdateManagerSvc.h"

// stl
#include <vector>
#include <algorithm>

// local
#include "VetraKernel/ContPrinter.h"
#include "VeloTELL1Reordering.h"

// engine classes
#include "TELL1Engine/TELL1ReorderProcessEngine.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTELL1Reordering
//
// 2007-02-08 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloTELL1Reordering )

using namespace boost::assign;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTELL1Reordering::VeloTELL1Reordering( const std::string& name,
                                          ISvcLocator* pSvcLocator)
  : VeloTELL1Algorithm ( name , pSvcLocator ),
    m_reorderedData ( ),
    m_printInfo ( false ),
    m_reorderEnableMap ( ),
    m_reorderEngines ( )
{
  setTELL1Process(CHANNEL_REORDERING);
  setAlgoName("CHANNEL_REORDERING");
  setAlgoType("TELL1 channel reordering");
  declareProperty("ValidationRun", m_validationRun=false);
  declareProperty("InputDataLoc",
    m_inputDataLoc=LHCb::VeloTELL1DataLocation::MCMSCorrectedADCs);
  declareProperty("OutputDataLoc",
    m_outputDataLoc=LHCb::VeloTELL1DataLocation::ReorderedADCs);
  declareProperty("SectorCorrection", m_sectorCorr=APPLY_SECTOR_CORR);
  declareProperty("DBConfig", m_dbConfig=VeloTELL1::STATIC);
  declareProperty("ReorderProcessEnable", m_reorderProcessEnable=1);
  declareProperty("SrcIdList", m_srcIdList);
}
//=============================================================================
// Destructor
//=============================================================================
VeloTELL1Reordering::~VeloTELL1Reordering() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloTELL1Reordering::initialize() {
  StatusCode sc = VeloTELL1Algorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;
  //
  if(m_dbConfig==VeloTELL1::DYNAMIC&&!m_validationRun){
    regUpdateTell1Conds();
    StatusCode mgrSvcStatus=runUpdate();
    if(mgrSvcStatus.isFailure()){
      return ( Error("Failed first UMS update", mgrSvcStatus) );
    }
  }else if(m_dbConfig==VeloTELL1::STATIC&&!m_validationRun){
    // initialize maps with static values
    m_reorderEnableMap[0]=m_reorderProcessEnable;
  }
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloTELL1Reordering::execute() {

  debug() << "==> Execute" << endmsg;
  //
  m_evtNumber++;
  StatusCode engineStatus;
  if(!isInitialized()){
    StatusCode initStatus =  INIT();
    if ( !initStatus ) {
      debug() << "Unable to initialize.  NZS data missing?" << endmsg;
      return StatusCode::SUCCESS;
    } 
    engineStatus=createAndConfigureEngines();
    if(m_forceEnable) this->setIsEnable(m_reorderProcessEnable);
    if(isEnable()){
      info()<< " --> Algorithm " << (this->algoName())
            << " of type: " << (this->algoType())
            << " is enabled and ready to process data --" <<endmsg;
    }else{
      info()<< " --> Algorithm " << (this->algoName())
            << " is disabled! " <<endmsg;
    }
  }
  StatusCode dataStatus;
  if(isEnable()&&(m_evtNumber>convergenceLimit())){
    if(engineStatus.isSuccess()){
      dataStatus=getData();
    }else{
      return ( engineStatus );
    }
    if(dataStatus.isSuccess()){              /// check if data is in TES
      dataStatus=inputStream(inputData());
      if(dataStatus.isSuccess()){            /// check if not empty
        topologicalChannelReordering();
        writeData();
      }else{
        return ( StatusCode::SUCCESS );
      }
    }else{
	    return StatusCode::SUCCESS;
    }
  }else{
    dataStatus=getData();
    if(dataStatus.isSuccess()){
      cloneData();
    }else{
	    return StatusCode::SUCCESS;
    }
  }
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloTELL1Reordering::finalize() {

  debug() << "==> Finalize" << endmsg;
  // must be called after all other actions
  if(0!=m_reorderEngines.size()){
    
    std::map<unsigned int, TELL1ReorderProcessEngine*>::iterator rIt;
    rIt=m_reorderEngines.begin();
    for( ; rIt!=m_reorderEngines.end(); ++rIt){
      delete rIt->second;
    }
    
  }
  //
  return VeloTELL1Algorithm::finalize();
}
//=============================================================================
StatusCode VeloTELL1Reordering::topologicalChannelReordering()
{
  debug()<< " ==> topologicalChannelReordering() " <<endmsg;
  //
  
  LHCb::VeloTELL1Datas::iterator sensIt=inputData()->begin();
  // loop over all sensors
  for( ; sensIt!=inputData()->end(); ++sensIt){
    // get info about current sensor
    const int tell1=(*sensIt)->key();
    unsigned int uTELL1=static_cast<unsigned int>(tell1);
    // !!for reordering studies!!
    const DeVeloSensor* sens=deVelo()->sensorByTell1Id(uTELL1);
    debug()<< " tell1: " << tell1 <<endmsg;
    // check if the sensor pointer is valid
    if(sens==0){
      Error(" ==> No match between TELL1 and sensor found! ");
      Error(" ==> Check your XML conditions file! ");
      return ( StatusCode::FAILURE );
    }

    StatusCode nzsSvcStatus=this->getNZSSvc();
    if(!nzsSvcStatus.isSuccess()) return ( StatusCode::FAILURE );
    if(this->nzsSvc()->counter(tell1)<=convergenceLimit()) continue;

    unsigned int sensNb=sens->sensorNumber();
    debug()<< " the sensor number: " << sensNb << " for TELL1 number: "
          << uTELL1 <<endmsg;
    unsigned int sensType=0;
    if(sens->isReadOut()){
      sdataVec inData=(*sensIt)->data();
      // check the sensor type and set the reordering mode
      if(sens->isR()||sens->isPileUp()){
        if(sens->isPileUp()){
          sensType=VeloTELL1::PILE_UP;
        }else{
          sensType=VeloTELL1::R_SENSOR;
        }
        m_reorderEngines[tell1]->setReorderMode(VeloTELL1::R_SENSOR);
        debug()<< " R sensor " <<endmsg;
        if(m_sectorCorr) cableErrorCorrection(inData);
      }else if(sens->isPhi()){
        sensType=VeloTELL1::PHI_SENSOR;
        m_reorderEngines[tell1]->setReorderMode(VeloTELL1::PHI_SENSOR);
        debug()<< " Phi sensor " <<endmsg;
        if(m_sectorCorr) cableErrorCorrection(inData);
      }
      m_reorderEngines[tell1]->setInData(inData);
      m_reorderEngines[tell1]->runReordering();
      // store and write out reordered data
      LHCb::VeloTELL1Data* reorderedData=
        new LHCb::VeloTELL1Data(tell1, VeloTELL1::VeloFull);
      reorderedData->setIsReordered(true);
      reorderedData->setSensorType(sensType);
      sdataVec outData=m_reorderEngines[tell1]->outData();
      reorderedData->setDecodedData(outData);
      outputData(reorderedData);
    }
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloTELL1Reordering::cableErrorCorrection(sdataVec& inVec)
{
  debug()<< " ==> cableErrorCorrection() " <<endmsg;
 
  // We have four PPFPGAs. To correct for the cabling error we have to
  // reverse their order in the data container.
  // Something is utterly wrong if the size of the input 
  // container is not divisble by four.
  if(inVec.size() % VeloTELL1::PP_FPGA){
    Error(" ==> Size of input data should be divisible by %d! ", VeloTELL1::PP_FPGA);
    Error(" ==> Cable error correction aborted! ");
    return;
  }

  // reverse order of data for the four PPFPGAs 
  const sdataVec tmp = inVec;
  const unsigned int n = tmp.size()/VeloTELL1::PP_FPGA;
  for (unsigned int i=0; i<VeloTELL1::PP_FPGA; ++i) {
    std::copy(tmp.begin()+((VeloTELL1::PP_FPGA-1-i)*n), tmp.begin()+((VeloTELL1::PP_FPGA-i)*n), inVec.begin()+(i*n));
  }
  
}
//=============================================================================
StatusCode VeloTELL1Reordering::createAndConfigureEngines()
{
  debug()<< " ==> prepareEngine() " <<endmsg;
  //
  VeloTELL1::AListPair idList=this->getSrcIdList();
  VeloTELL1::ucIT iT=idList.first;
  if(iT==idList.second){
    return Error(" ==> Empty list with src Ids!");
  }else{
    // create engines and put them into the map with tell1 as keys
    if(m_dbConfig==VeloTELL1::STATIC){
      info()<< " --> Creating static config for Reordering! " <<endmsg;
      for( ; iT!=idList.second; ++iT){
        m_reorderEngines[*iT]=new TELL1ReorderProcessEngine();
        // the engine runs actually with mode flag not process enable
        // flag, the mode flag is set depending on type of sensor
        m_reorderEngines[*iT]->setCableCorrection(NO_SECTOR_CORR);
      }
    }else if(m_dbConfig==VeloTELL1::DYNAMIC){
      // all parameters are already cached - use them to config engines
      info()<< " --> Creating dynamic config for Reordering! " <<endmsg;
      for( ; iT!=idList.second; ++iT){
        // reorder processing objects
        m_reorderEngines[*iT]=new TELL1ReorderProcessEngine();
        m_reorderEngines[*iT]->setCableCorrection(NO_SECTOR_CORR);
      }
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1Reordering::i_cacheConditions()
{
  debug()<< " ==> i_cacheCondition() " <<endmsg;
  VeloTELL1::AListPair iniList=getSrcIdList();
  if(iniList.first==iniList.second){
    return ( Error(" --> No Tell1 List cached!", StatusCode::FAILURE) );
  }
  VeloTELL1::ucIT srcId=iniList.first;
  // loop over all tell1s and initialize conditions
  for( ; srcId!=iniList.second; ++srcId){
    unsigned int tell1=*(srcId);
    std::string stell1=boost::lexical_cast<std::string>(tell1);
    std::string condName=m_condPath+"/VeloTELL1Board"+stell1;
    Condition* cond=getDet<Condition>(condName);
    // enable flag for pedestal process
    m_reorderEnableMap[tell1]=cond->param<int>("reorder_mode");
    debug()<< " --> Reorder mode: " << (m_reorderEnableMap[tell1]) <<endmsg;
  }
  return ( StatusCode::SUCCESS );
}
//--
