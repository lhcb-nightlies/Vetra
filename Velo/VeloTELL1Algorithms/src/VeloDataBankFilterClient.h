// $Id: VeloDataBankFilterClient.h,v 1.1 2010-09-16 17:55:00 gersabec Exp $
#ifndef VELODATABANKFILTERCLIENT_H
#define VELODATABANKFILTERCLIENT_H 1

// Include files
// from Gaudi
#include "VetraKernel/VeloTELL1Algorithm.h"

// form TELL1 Kernel
#include "Tell1Kernel/VeloDecodeCore.h"

/** @class VeloDataBankFilterClient VeloDataBankFilterClient.h
 *  
 *  This algorithm filters on the presence of NZS banks
 *  It can be used to execute NZS and ZS monitoring on the same events
 *
 *  @author Marco Gersabeck
 *  @date   2010-09-16
 */

class VeloDataBankFilterClient: public VeloTELL1Algorithm{
public: 
  /// Standard constructor
  VeloDataBankFilterClient( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloDataBankFilterClient( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:

  bool m_isDebug;
  int m_evtNumber;
  bool m_filterNZS;
  double m_filterPrescale;

};
#endif // VELODATABANKFILTERCLIENT_H
