// $Id: VeloTELL1MCMS.h,v 1.1 2008-05-23 13:19:22 szumlat Exp $
#ifndef VELOTELL1MCMS_H 
#define VELOTELL1MCMS_H 1

// Include files
// from Gaudi
#include "VetraKernel/VeloTELL1Algorithm.h"

// stl
#include <vector>
#include <algorithm>

/** @class VeloTELL1MCMS VeloTELL1MCMS.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2008-03-11
 */

class TELL1MCMSProcessEngine;

class VeloTELL1MCMS: public VeloTELL1Algorithm{
public: 
  /// Standard constructor
  VeloTELL1MCMS( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloTELL1MCMS( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode createAndConfigureEngines();
  StatusCode runMCMSuppression();
  StatusCode i_cacheConditions() override;

private:

  unsigned int m_evtNumber;
  unsigned int m_mcmsProcessEnable;
  VeloTELL1::IntCondMap m_mcmsEnableMap;
  std::map<unsigned int, TELL1MCMSProcessEngine*> m_mcmsEngines;

};
#endif // VELOTELL1MCMS_H
