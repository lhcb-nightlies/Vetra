// $Id: VeloDataBankFilterClient.cpp,v 1.1 2010-09-16 17:55:00 gersabec Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "VetraKernel/ContPrinter.h"
#include "Event/ProcStatus.h"

// local
#include "VeloDataBankFilterClient.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : VeloDataBankFilterClient
//
// 2010-09-16 : Marco Gersabeck
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloDataBankFilterClient )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloDataBankFilterClient::VeloDataBankFilterClient( const std::string& name,
                                ISvcLocator* pSvcLocator)
  : VeloTELL1Algorithm (name, pSvcLocator),
    m_evtNumber ( 0 )
{
  declareProperty("FilterNZS",      m_filterNZS      = true );
  declareProperty("FilterPrescale", m_filterPrescale = true );
}
//=============================================================================
// Destructor
//=============================================================================
VeloDataBankFilterClient::~VeloDataBankFilterClient() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloDataBankFilterClient::initialize() {
  StatusCode sc = VeloTELL1Algorithm::initialize(); // must be executed first
  // error printed already by VeloTELL1Algorithm
  if ( sc.isFailure() ) return sc;

  m_isDebug = msgLevel( MSG::DEBUG );
  if(m_isDebug) debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloDataBankFilterClient::execute() {

  if(m_isDebug) debug() << "==> Execute" << endmsg;
  ++m_evtNumber;

  LHCb::ProcStatus *pStat = get<LHCb::ProcStatus>(LHCb::ProcStatusLocation::Default);
  std::string compare_str = "ABORTED:VELO:NZSPrescaleFilter:VeloDataBankFilter";
  std::string str;
  int status;
  for( unsigned int i = 0; pStat->algs().size() != i; i++ ) {
    str = pStat->algs()[i].first;
    if ( compare_str == str ) {
      status = pStat->algs()[i].second;
      if ( 1 == status ) {
        if ( m_filterNZS ) {
          setFilterPassed(false);
          return StatusCode::SUCCESS;
        }
      }
      else if ( 2 == status ) {
        if ( m_filterPrescale ) {
          setFilterPassed(false);
          return StatusCode::SUCCESS;
        }
      }
    }
  }

  setFilterPassed(true);
  return StatusCode::SUCCESS;
}
 
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloDataBankFilterClient::finalize() {

  if(m_isDebug) debug() << "==> Finalize" << endmsg;
  // must be called after all other actions
  return VeloTELL1Algorithm::finalize();
}
//=============================================================================
