// $Id: ClusterMakerThresholds.h,v 1.3 2007-03-22 09:24:28 szumlat Exp $
#ifndef CLUSTERMAKERTHRESHOLDS_H 
#define CLUSTERMAKERTHRESHOLDS_H 1

namespace VeloTELL1
{
  enum thresholds{
    LOW_THRESHOLD=5,
    HIGH_THRESHOLD=10,
    ADC_SUM_THRESHOLD=20
  };
}
#endif // CLUSTERMAKERTHRESHOLDS_H
