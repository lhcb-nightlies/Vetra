// $Id: VeloTELL1CableFIRFilter.h,v 1.6 2008-05-27 10:19:10 szumlat Exp $
#ifndef VELOTELL1CABLEFIRFILTER_H 
#define VELOTELL1CABLEFIRFILTER_H 1

// Include files
// TELL1 algorithm base class
#include "VetraKernel/VeloTELL1Algorithm.h"

// form TELL1 Kernel
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloTell1Core.h"

// from TELL1 event
#include "VeloEvent/VeloTELL1FData.h"
#include "VeloEvent/VeloProcessInfo.h"
#include "VeloEvent/VeloTELL1FData.h"

#include <vector>



/** @class VeloTELL1CableFIRFilter VeloTELL1CableFIRFilter.h
 *  
 *
 *  @author Tomasz Szumlak<br/>
 *  re-written by Jeremie Borel 2007-06-19
 *  @date   2007-01-29
 *
 * \brief Apply a FIR correction on a analog cable basis.
 *
 * This algorithm apply a FIR correction. A full description is given under 
 * https://twiki.cern.ch/twiki/bin/view/LHCb/VeloTELL1CableFIRFilter
 * (e.g. VeloTELL1CableFIRFilter under Velo TWIKI in case the page moves)
 *
 * A short summary of the options comes:
 * 
 * - InputDataLoc, OutputDataLoc gives the places to read and writes
 *   the data. If a Tell1 or cable is not touched by the FIR, its data
 *   are duplicated to the OutputDataLoc.
 * - Tell1List is an ordered vector of unsigned int of the Tell1s that are considered.
 * - Kso (where s is either 'm' for minus or 'p' for plus and o is and offset between 1 and 4)
 *   is a vector of double describing the x-talk (not the FIR) coefficients for this particular offset.
 *   It has exactly (#tell1)*64 elements. <br/>
 *   As an example, if only Km1 = {...}; and Kp1 = {...} are given, you compute a FIR whom input order is 3.
 *   If you give Km2 and Kp2, you compute a FIR or input order 5, etc...
 * - M is the 'output' order of the FIR, can be 3,5,7,9.
 * - DebugCable print out, for this cable the value before and after FIR correction.

 */

class DeVelo;

class VeloTELL1CableFIRFilter : public VeloTELL1Algorithm {

  typedef signed int coefType;
  
public: 

  /// Standard constructor
  VeloTELL1CableFIRFilter( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloTELL1CableFIRFilter( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode runFIRFilter();
  StatusCode FIRCorrection( const std::vector< coefType >& );

  //! Reformat the coefficients given by the user.
  bool initCoef();
  //! Executed once at the beginning of the algorithm.
  void computeFIRCoef( const std::vector<double>& h ,std::vector<double>& g ) const;

private:

  DeVelo* m_veloDet;

  //sdataVec m_inputALink;
  VeloTELL1::sdataVec m_outputALink;
  VeloTELL1::ALinkPair m_inputALink;
  //ALinkPair m_outputALink;
  VeloTELL1::VeloTELL1FData m_floatData;
  
  unsigned int m_eventNumber;

  std::vector< std::vector< double > > m_xtalkCoefList;
  std::vector< unsigned int > m_T1List;
  std::map< unsigned int, std::vector< std::vector< coefType > > > m_coefList;

  int m_dumpDebugCable;

  int m_orderN, m_orderM;

};
#endif // VELOTELL1CABLEFIRFILTER_H
