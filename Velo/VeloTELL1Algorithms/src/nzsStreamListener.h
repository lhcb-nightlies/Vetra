// $Id: $
#ifndef NZSSTREAMLISTENER_H 
#define NZSSTREAMLISTENER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "VetraKernel/VeloNZSInfo.h"

#include <string>

// from TELL1 event
//#include "VeloEvent/VeloTELL1Data.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"

/** @class nzsStreamListener nzsStreamListener.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2010-07-07
 */

namespace VeloTELL1{
  typedef std::pair<unsigned int, unsigned int> cntElement;
}

class nzsStreamListener : public GaudiAlgorithm {
public: 
  /// Standard constructor

  enum VELO{
    R_SENS_START=0,
    R_SENS_END=41,
    PHI_SENS_START=64,
    PHI_SENS_END=105,
    PU_SENS_START=128,
    PU_SENS_END=131
  };
  
  nzsStreamListener( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~nzsStreamListener( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getNZSStream();
  void createInfoService();
  void nzsInfoReport() const;
  void printInfo(const VeloTELL1::cntElement) const;

private:

  bool m_isDebug;
  unsigned int m_evtNumber;
  std::string m_nzsInfoLoc;
  VeloNZSInfos* m_nzsInfo;
  VeloTELL1::Tell1CountersMap m_cntMapLocal;
  //LHCb::VeloTELL1Datas* m_nzsStream;
  LHCb::RawEvent* m_nzsStream;
  std::string m_nzsStreamLoc;

};
#endif // NZSSTREAMLISTENER_H
