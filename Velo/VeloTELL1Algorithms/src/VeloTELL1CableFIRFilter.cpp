// $Id: VeloTELL1CableFIRFilter.cpp,v 1.7 2010-05-12 17:05:08 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 

// VeloDet
#include "VeloDet/DeVelo.h"

// local
#include "VeloTELL1CableFIRFilter.h"

// Kernel
#include "VetraKernel/ContPrinter.h"
#include "LHCbMath/LHCbMath.h"

// gsl
#include "CLHEP/Matrix/Matrix.h"
#include "CLHEP/Matrix/Vector.h"

// stl
#include <vector>
#include <algorithm>
#include <cmath>

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTELL1CableFIRFilter
//
// 2007-01-30 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloTELL1CableFIRFilter )

//static unsigned int _COUNTER_=1;

using namespace LHCb;
using namespace LHCb::Math;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTELL1CableFIRFilter::VeloTELL1CableFIRFilter( const std::string& name,
                                        ISvcLocator* pSvcLocator)
  :  VeloTELL1Algorithm( name , pSvcLocator ),
     m_veloDet ( 0 ),
     m_floatData ( ),
     m_eventNumber ( 0 )
{

  
  declareProperty("InputDataLoc", m_inputDataLoc = LHCb::VeloTELL1DataLocation::ADCs );
  declareProperty("OutputDataLoc", m_outputDataLoc = LHCb::VeloTELL1DataLocation::FIRCorrectedADCs );
  setInputDataLoc( m_inputDataLoc );
  setOutputDataLoc( m_outputDataLoc );

  setTELL1Process(FIR_FILTER);
  setAlgoName("FIR_FILTER");
  setAlgoType("TELL1 FIR Filter");

  declareProperty("Tell1List", m_T1List );


  m_xtalkCoefList.resize(9);

  //  m_xtalkCoefList[4] is not used, that's fine.
  declareProperty("Km4", m_xtalkCoefList[0] );
  declareProperty("Km3", m_xtalkCoefList[1] );
  declareProperty("Km2", m_xtalkCoefList[2] );
  declareProperty("Km1", m_xtalkCoefList[3] );
  declareProperty("Kp1", m_xtalkCoefList[5] );
  declareProperty("Kp2", m_xtalkCoefList[6] );
  declareProperty("Kp3", m_xtalkCoefList[7] );
  declareProperty("Kp4", m_xtalkCoefList[8] );

  declareProperty("DebugCable", m_dumpDebugCable=-1 );

  declareProperty("M", m_orderM = 9 );

  m_outputALink.resize( 32,0 );

}
//=============================================================================
// Destructor
//=============================================================================
VeloTELL1CableFIRFilter::~VeloTELL1CableFIRFilter() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloTELL1CableFIRFilter::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;
  //
  m_veloDet=getDet<DeVelo>(DeVeloLocation::Default);
  if( !m_veloDet ){
    Error("Cannot retrieve the velo description.");
    return StatusCode::FAILURE;
  }

  // compute order N, i.e. the number of coeficient that are given.
  m_orderN = 9;
  if( m_xtalkCoefList[0].empty() && m_xtalkCoefList[8].empty() ){
    m_orderN = 7;
    if( m_xtalkCoefList[1].empty() && m_xtalkCoefList[7].empty() ){
      m_orderN = 5;
      if( m_xtalkCoefList[2].empty() && m_xtalkCoefList[6].empty() ){
	m_orderN = 3;
	if( m_xtalkCoefList[3].empty() && m_xtalkCoefList[5].empty() ){
	  m_orderN = 1; // this actually implies no FIR.
	}
      }
    }
  }
  
  if( m_orderN<2 ){
    error() << "N order 1 FIR filter makes no sens. Check your x-talk coeficients" << endmsg;
    return StatusCode::FAILURE;
  }
  if( m_orderM<3 ){
    error() << "M order 1 FIR filter makes no sens. Check your M order." << endmsg;
    return StatusCode::FAILURE;
  }
  if( !m_orderN%2 || !m_orderM%2 ){
    error() << "This FIR filter is restricted to odd number of x-talk coef and odd M order." << endmsg;
    return StatusCode::FAILURE;
  }

  if( ! initCoef() ) {
    error() << "Could not parse the input coeficients" << endmsg;
    return StatusCode::FAILURE;
  };
//   m_dummyT1List.clear();
//   m_dummyCable.clear();
//   m_xtalkCoefList.clear();

  //
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloTELL1CableFIRFilter::execute() {

  debug() << "==> Execute" << endmsg;
  //
  m_eventNumber++;
  if(!isInitialized()) {
      StatusCode initStatus =  INIT();
      if ( !initStatus ) {
        debug() << "Unable to initialize.  NZS data missing?" << endmsg;
        return StatusCode::SUCCESS;
      } 
  }
  StatusCode dataStatus=getData();
  //  if(isEnable()&&(m_eventNumber>convergenceLimit())){
  if(dataStatus.isSuccess()){
    runFIRFilter();
    writeData();
  }
  
  return StatusCode::SUCCESS;
  
  //   }else{
  //     if(dataStatus.isSuccess()){
  //       cloneData();
  //     }else{
  //       return ( dataStatus );
  //     }
  //   }
  //  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloTELL1CableFIRFilter::finalize() {

  debug() << "==> Finalize" << endmsg;

  return VeloTELL1Algorithm::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode VeloTELL1CableFIRFilter::runFIRFilter()
{
  debug()<< " ==> Starting the FIR filter() " <<endmsg;
  // iterate over all sensors
  VeloTELL1Datas::const_iterator sensIt;
  sensIt=inputData()->begin();
  // 
  for( ; sensIt!=inputData()->end(); ++sensIt){
    // get pointer to the velo sensor related with given TELL1
    const unsigned int tell1=static_cast<unsigned int>((*sensIt)->key());
    const DeVeloSensor* sens=m_veloDet->sensorByTell1Id(tell1);
    if(sens==0){
      Error(" ==> No match between TELL1 and sensor found! ");
      Error(" ==> Check your XML conditions file! ");
      return ( StatusCode::FAILURE );
    }
    unsigned int sensNb=sens->sensorNumber();
    debug()<< " sensor number: " << sensNb << " for TELL1 number: "
          << tell1 <<endmsg;

    std::map< unsigned int, std::vector< std::vector< coefType > > >::const_iterator it;
    it = m_coefList.find( tell1 );
    if( it == m_coefList.end() ){
      std::stringstream s;
      s << "Tell1 " << tell1 << " has no coef. Passive duplication of the data.";
      Warning( s.str() );

      // duplicate the tell1 data:
      //      VeloTELL1Data* inData=(*sensIt);
      LHCb::VeloTELL1Data* out=new LHCb::VeloTELL1Data(tell1, VeloFull);
      sdataVec replica=(*sensIt)->data();
      verbose() << "Duplicated data size is " << replica.size() << endmsg;
      out->setDecodedData(replica);
      outputData(out);
      continue;
    }
    const std::vector< std::vector< coefType > >& tell1Coef = it->second;

    // check if the sensor is read out
    if(sens->isReadOut()){
      // create a new VeloTELL1Data object
      VeloTELL1Data* firData=new VeloTELL1Data(tell1, VeloFull);
      // data from current sensor
      VeloTELL1Data* inData=(*sensIt);

      //       // ---------------------------------------------------------------
      //       // cheat the signal:
      //       sdataVec& ss = const_cast<sdataVec&>( inData->data() );
      //       std::fill( ss.begin(), ss.end(), 0 );
      //       //      double xt[] = { 0.01, -0.02, 0.03, -0.08, 1, 0.08, -0.03, 0.02, -0.01 };
      //       double xt[] = { 0.00, -0.00, 0.01, -0.08, 1, 0.08, 0.01, 0.00, -0.00 };
      //       //                -4    -3    -2     -1   0   1      2     3       4
      
      //       for( int i=4; i<2048; i+=29 ){
      // 	// int adcVal = static_cast<int>( (double)(rand())/(double)(RAND_MAX)*15+30);
      // 	int adcVal = 100;
      // 	for( int kk = i-4; kk<=i+4; ++kk ){
      // 	  if( kk>=0 && kk<2048 ){
      // 	    ss[ kk ] = static_cast<int>( round(adcVal * xt[kk-i+4]) ); 
      // 	  }
      // 	}
      //       }
      //       // ---------------------------------------------------------------
      

      for(int aLink=0; aLink<NumberOfALinks; ++aLink){
        //m_inputALink.clear();
        //m_inputALink=(*inData)[aLink];
        m_inputALink=(*inData)[aLink];
        // check vectors passed to the filtering
        //Assert(m_inputALink.size()==CHANNELS);
        assert(distance(m_inputALink.first, m_inputALink.second)==CHANNELS);

        // FIR filter
	verbose()<< " FIRCorrection() for TELL1 " << tell1 << " cable " << aLink <<endmsg;
        FIRCorrection( tell1Coef[aLink] );

	if( m_dumpDebugCable == aLink ){
	  std::stringstream s;
	  s << "ADC values for cable " << aLink 
	    << ": ";
	  std::copy( tell1Coef[aLink].begin(), tell1Coef[aLink].end()
		     , std::ostream_iterator<coefType>( s, " " ) ); 
 
	  s << "\nbefore FIR: ";
	  //for( int i=0; i<CHANNELS; ++i ) s << std::setw(4) << m_inputALink[i];
    for(int i=0; i<CHANNELS; ++i) s << std::setw(4) << *(m_inputALink.first+i);
	  s << "\nafter  FIR: " ;
	  for(int i=0; i<CHANNELS; ++i) s << std::setw(4) << m_outputALink[i];
	  info() << s.str() << endmsg;

	}

        // FIR filter
        firData->addALink(m_outputALink);

      }
      // store the fir data
      outputData(firData);
    }
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1CableFIRFilter::FIRCorrection( const std::vector< coefType >& K )
{
  if( ! msgLevel( MSG::VERBOSE ) ){
    verbose() << " FIRCorrection() " <<endmsg;
    std::stringstream str;
    std::copy( K.begin(), K.end(), std::ostream_iterator<coefType>( str, " " ) ); 
    verbose() << "Coefs are: " << str.str() << endmsg;
  }
  
  const int MM = (m_orderM-1)/2;

  // -------------------------------------------------------
  //   sdataVec& in = m_inputALink;
  //   std::fill( in.begin(), in.end(), 0 );
  
  //   in[11] = 6;
  //   in[12] = 100;
  //   in[13] = 12;

  // --------------------------------------------------------

  for( int i=0;i<CHANNELS; ++i ){
    //m_outputALink[i] = 0;
    m_outputALink[i]=0;
    // first loop on 0,1,2,3,4, second loop on -1,0,1,2,3,4, etc.. until -4,..,4
    // and then -4,...,0,..3, and -4,...0 for i==31
    for( int k= -MM ; k<= MM ; ++k ){
      if( 0!=k && 0<=k+i && 32>k+i ){
	      m_outputALink[i]+= 
          //static_cast<signed int>( round( (m_inputALink[ k+i ]>>2) * K[ k+MM ] ) );
        //static_cast<signed int>(round(static_cast<float>(((*(m_inputALink.first+k+i))>>2)*K[ k+MM ])));
        std::lround(static_cast<float>(((*(m_inputALink.first+k+i))>>2)*K[ k+MM ]));
      }
    }
    //m_outputALink[i]=((m_outputALink[i] +64)>>7) + static_cast<signed int>( m_inputALink[i] );
    m_outputALink[i]=((m_outputALink[i]+64)>>7)+static_cast<signed int>((*(m_inputALink.first+i)));
    
    // saturate the value to signed 12 bits.
    if( m_outputALink[i] > 511 ) m_outputALink[i] = 511;
    if( m_outputALink[i] < -512 ) m_outputALink[i] = -512;
  }

  return StatusCode::SUCCESS;
}



bool VeloTELL1CableFIRFilter::initCoef(){

  debug() << "Initializing the FIR coeficients" << endmsg;
  // consistency check
  const int size = 64* m_T1List.size();
  // check coef vectors size:
  for( int i=0; i<(int) m_xtalkCoefList.size(); ++i ){
    // a coef list is either 0 size of 64*t1.size()
    if( !m_xtalkCoefList[i].empty() ){
      if( (int)m_xtalkCoefList[i].size() != size ){
	error() << "The coefficient lists have to be either 0 sized or 64*Tell1List.size()" << endmsg;
	error() << "While current size is " << m_xtalkCoefList[i].size() << endmsg;
	return false;
      }
    } 
    // else {
    //       coef[i].resize( size );
    //       std::fill( coef[i].begin(), coef[i].end(), 0 ); // ensure every coef is 0
    // }
  }

  const int NN = (m_orderN-1)/2;
  const int MM = (m_orderM-1)/2;

  for( int t = 0; t < (int)m_T1List.size(); ++t ){
    //unsigned int tell1Idx = m_T1List[t];
    
    verbose() << "Initializing the FIR coeficients for Tell1 " << m_T1List[t] << endmsg;

    std::vector< std::vector< coefType > > coefMat;

    std::vector< double > h(m_orderN),g(m_orderM);

    //    std::cout << "NN is " << NN << " MM is " << MM << std::endl;

    coefMat.clear();
    coefMat.resize( 64 );
    for( int cbl=0; cbl<64; ++cbl ){
      coefMat[ cbl ].resize( m_orderM );

      double sum = 0;
      for( int kk= -NN; kk<=NN ; ++kk ){
	int idx = kk+NN;
	if( kk==0 ){
	  h[idx] = 0;
	  continue;
	}
	h[idx] = m_xtalkCoefList[ kk+4 ][ t*64+cbl ];
	sum += h[idx];
      }
      //      h[ NN ] = 1.0 - sum; // ensure normalization.
      h[ NN ] = 1.0;
      std::fill( g.begin(), g.end(), 0 );// ensure g is initialized.
      computeFIRCoef( h, g );
      
//       	std::stringstream s;
//       	s << "FIR COEF ";
//       	std::copy( g.begin(), g.end(), std::ostream_iterator<double>( s, ", " ) );
//       	info() << s.str() << endmsg;
	
      for( int i=0; i<m_orderM; ++i ){
	// convert to signed integer, 8 bit precision (TELL1 format)
	coefMat[cbl][i] = static_cast<signed int>( g[i] * 512 );
	if( coefMat[cbl][i] < -128 ) coefMat[cbl][i] = -128;
	if( coefMat[cbl][i] > 127 )  coefMat[cbl][i] = 127;
      }
    }
    m_coefList[ m_T1List[t] ] = coefMat;
  }
  
  if( ! msgLevel( MSG::VERBOSE ) ) return true;
  
  verbose() << "FIR coeficient list as inside the Tell1s." << endmsg;
  verbose() << "Must be divided by 512 to have a physical meaning." << endmsg;

  std::stringstream s;
  s << std::right << std::showpos;
  std::map< unsigned int, std::vector< std::vector< coefType > > >::const_iterator mcit;
  for( mcit=m_coefList.begin(); mcit!=m_coefList.end(); ++mcit ){
    s << " **** Coef mapping for Tell1 " << mcit->first << "\n";
    std::stringstream tmp;
    tmp << "Tell1 " << mcit->first << "  ";
    s << std::left <<std::setw( 16 ) << tmp.str() << std::right ;
    for( int i=-MM; i<=MM; ++i ){
      if( i==0 ) continue;
      s << std::setw( 3 ) << "K" << std::setw(2) << i;
    }
    s << "\n";

    for( int c=0; c<64; ++c ){
      s << "  |  cable " << std::noshowpos << std::setw(2) << c << " : " << std::showpos;
      for( int i=0; i<m_orderM; ++i ){
	if( i== (m_orderM-1)/2 ) continue;
	s << std::setw(5) << (mcit->second)[c][i];
      }
      s << "\n";
    }
  }
  
  verbose() << s.str() << endmsg;

  return true;
  

}


//

// ===============================================================
// ===============================================================
// ===============================================================


void VeloTELL1CableFIRFilter::computeFIRCoef( const std::vector<double>& h
					      ,std::vector<double>& g ) const {

  //
  // h: impulse response to compensate, with length N
  // g: FIR coefficients, lenght M
  //
  // Calculates the coefficients for a FIR filter, assuming that the
  // impulse response h[n] is known and has the length N
  //
  // Uses least square method to determine coefficients that reconstruct
  // the original input pulse. 

  const int N = static_cast<int>(h.size());
  const int M = static_cast<int>(g.size());
    
  Assert( N%2 && M%2 && M>2 && N>2 && N == m_orderN && M == m_orderM );

  // Define matrices
  CLHEP::HepMatrix Y(M+N-1,1);
  CLHEP::HepMatrix A(M+N-1,M);
    
//   // Calculate signal
//   for(int i=0;i<N;i++) {
//     signal+=h[i];
//   }
    
  // Fill matrices
  for(int i=0;i<M+N-1;i++) {
    Y[i][0] = 0;    
    for(int j=0;j<M;j++) {
      int n = i - j - (N-1)/2;
      if( n>(N-1)/2 || n<-(N-1)/2 ) {
	A[i][j] = 0;
      } else {
	//	A[i][j] = h[n+(int)((N-1)/2)];
	A[i][j] = h[(int)((N-1)/2)-n];
      }
    }
  }

  //  std::cout << "***********************************************333\n" << A << std::endl;

  Y[((M+N)/2-1)][0] = 1; //signal;
    
    
  // Calculate FIR Coefficients
  
  CLHEP::HepMatrix At = A.T();
  CLHEP::HepMatrix AtA = At * A;
  int errFlag = 0;
  CLHEP::HepMatrix AtAInv = AtA.inverse( errFlag );
  if( errFlag != 0 ){
    error() << "I guess an error occured during the matrix inversion..." << endmsg;
  }
  //  CLHEP::HepMatrix AtY = At * Y;
  CLHEP::HepMatrix G = AtAInv * (At * Y);

//   TMatrix At(TMatrix::kTransposed, A);
//   TMatrix AtA(At, TMatrix::kMult, A);
//   TMatrix AtAinv(TMatrix::kInverted, AtA);
//   TMatrix AtY(At, TMatrix::kMult, Y);
//   TMatrix G(AtAinv, TMatrix::kMult, AtY);
    
  // Fill return vector
  // Reversed since y[n]=Sum(g[k]*h[k-n], {k, -(M-1/2, (M-1)/2})
  
  for(int i=(M-1);i>=0;i--) g[i] = G[i][0];
    
  // Print matrices
//   if(debug){
//     for(i=0;i<M;i++) {
//       sum_g  += g[i];
//       sum_g2 += g[i]*g[i];
//     }
//     printf("Matrix Y\n");
//     Y.Print();
//     printf("Matrix A\n");
//     A.Print();
//     printf("Matrix G\n");
//     G.Print();
//     printf("Total signal:   %6.3f\n", signal);
//     printf("Sum of g[n]   = %6.3f\n", sum_g);
//     printf("Sum of g^2[n] = %6.3f\n", sum_g2);
//   }
}
