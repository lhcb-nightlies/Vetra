// $Id: VeloTELL1EmulatorInit.h,v 1.10 2008-09-08 16:36:29 szumlat Exp $
#ifndef VELOTELL1EMULATORINIT_H 
#define VELOTELL1EMULATORINIT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "VeloEvent/EvtInfo.h"
#include "VeloEvent/VeloProcessInfo.h"

/** @class VeloTELL1EmulatorInit VeloTELL1EmulatorInit.h
 *  
 *  The algoritm is meant to facilitate using the TELL1 emulator
 *  inside Vetra. It is essential to run the algoritm as first one
 *  during the TELL1Processing sequence. In the case of real NZS data
 *  algorithm gets the information of processing sequence performed
 *  on TELL1 board from EvtInfo raw bank. In the case of simulated 
 *  data one needs to set up properly processing via options file
 *  Please look at the VetraSimNZS.opts for examples.
 *
 *  @author Tomasz Szumlak
 *  @date   2006-07-10
 */

class VeloTELL1EmulatorInit : public GaudiAlgorithm {
public: 
  /// Standard constructor
  VeloTELL1EmulatorInit( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloTELL1EmulatorInit( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getData();
  void writeData();
  StatusCode emulatorInit();

private:

  EvtInfos* m_evtInfos;           /// container for Event Info blocks
  std::string m_evtInfoLoc;       /// TES location of container with EvtInfo
  unsigned int m_mode;            /// type of data to process (sim or real)
  unsigned int m_devRun;          /// set the flag for user defined algos
  VeloProcessInfos* m_initInfo;   /// init info for emulator
  std::string m_initInfoLoc;      /// TES location of init info. container
  unsigned int m_realProc;        /// processing sequence for real data
  VeloTELL1::dataVec m_simProc;   /// processing sequence for sim data
  VeloTELL1::dataVec m_devProc;   /// user defined sequence of algos
  bool m_printInfo;
  unsigned int  m_fastSubtractor; /// set the flag if fast pedSub is in use
  int m_convergenceLimit;
  unsigned int m_runType;         /// process data or test algorithm

};
#endif // VELOTELL1EMULATORINIT_H
