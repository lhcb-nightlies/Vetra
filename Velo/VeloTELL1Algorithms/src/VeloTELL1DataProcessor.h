// $Id: VeloTELL1DataProcessor.h,v 1.3 2007-12-10 02:12:43 szumlat Exp $
#ifndef VELOTELL1DATAPROCESSOR_H 
#define VELOTELL1DATAPROCESSOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// form TELL1
#include "VeloEvent/VeloTELL1Data.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloDecodeConf.h"

// from LHCb event
#include "Event/MCVeloFE.h"

/** @class VeloTELL1DataProcessor VeloTELL1DataProcessor.h
 *  
 * the class implements emulator of the FADC cards on
 * TELL1 read-out board, input stream consists of container
 * with MCVeloFEs objects, output stream has both digitized
 * ADCs for each read-out sensor as well as digitized
 * pedestal values
 *
 *  @author Tomasz Szumlak
 *  @date   2006-05-25
 */

using namespace VeloTELL1;
using namespace LHCb;

class VeloTELL1DataProcessor : public GaudiAlgorithm {
public:

  typedef LHCb::MCVeloFEs::const_iterator feConstIt;
  typedef LHCb::MCVeloFEs::iterator feIt;
  /// Standard constructor
  VeloTELL1DataProcessor(const std::string& name, ISvcLocator* pSvcLocator);

  virtual ~VeloTELL1DataProcessor( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getData();
  StatusCode writeDataForSensors();
  void writeOutput();

private:

  std::string m_fesContainer;   /// input simulated Front End objects
  LHCb::MCVeloFEs* m_fes;       /// container with FE objects
  std::string m_simADCs;        /// container's name for simulated NZS data
  VeloTELL1Datas* m_adcs;       /// data container
  std::string m_simPeds;        /// container's name for simulated pedestals
  VeloTELL1Datas* m_peds;       /// pedestal container
  sdataVec m_adcsCharge;        /// container with total simulated charge
  sdataVec m_pedsCharge;        /// container with pedestals sim. charge
  double m_adcScale;            /// 10 bit bandwidth adc count scale
  double m_elScale;             /// electron equivalent of the adc scale max

};
#endif // VELOTELL1DATAPROCESSOR_H
