// $Id: dataTranslator.h,v 1.6 2009-11-17 19:28:25 szumlat Exp $
#ifndef DATATRANSLATOR_H 
#define DATATRANSLATOR_H 1

// Include files
// from Gaudi
#include "VetraKernel/VeloTELL1Algorithm.h"

// form TELL1 Kernel
#include "Tell1Kernel/VeloDecodeCore.h"

/** @class dataTranslator dataTranslator.h
 *  
 *  This algorithm should be executed ALWAYS as first in
 *  TELL1 emulator for Velo (after the emulator initialization).
 *  It will insert sequences of dummy strips within all decoded data.
 *
 *  @author Tomasz Szumlak
 *  @date   2007-08-14
 */

class dataTranslator: public VeloTELL1Algorithm{
public: 
  /// Standard constructor
  dataTranslator( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~dataTranslator( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:

  bool m_insertDummyAtEnd;
  bool m_isDebug;
  int m_evtNumber;

};
#endif // DATATRANSLATOR_H
