// $Id: VeloTELL1MCMS.cpp,v 1.5 2009-11-17 19:28:25 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// stl
#include <vector>
#include <algorithm>

// local
#include "VeloTELL1MCMS.h"
#include "VetraKernel/ContPrinter.h"

// engine classes
#include "TELL1Engine/TELL1MCMSProcessEngine.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTELL1MCMS
//
// 2008-03-19 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloTELL1MCMS )

using namespace boost::assign;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTELL1MCMS::VeloTELL1MCMS( const std::string& name,
                              ISvcLocator* pSvcLocator)
  : VeloTELL1Algorithm( name , pSvcLocator ),
    m_evtNumber ( 0 ),
    m_mcmsEnableMap ( ),
    m_mcmsEngines ( )
{
  setTELL1Process(MEAN_COMMON_MODE_SUBTRACTION);
  setAlgoType("TELL1 Mean Common Mode Subtraction");
  declareProperty("InputDataLoc",
    m_inputDataLoc=LHCb::VeloTELL1DataLocation::FIRCorrectedADCs);
  declareProperty("OutputDataLocation",
    m_outputDataLoc=LHCb::VeloTELL1DataLocation::MCMSCorrectedADCs);
  declareProperty("AlgorithmName",
                  m_algorithmName="MCMS");
  declareProperty("MCMSProcessEnable", m_mcmsProcessEnable=1);
  declareProperty("DBConfig", m_dbConfig=VeloTELL1::STATIC);
  declareProperty("SrcIdList", m_srcIdList);
}
//=============================================================================
// Destructor
//=============================================================================
VeloTELL1MCMS::~VeloTELL1MCMS() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloTELL1MCMS::initialize() {
  StatusCode sc=VeloTELL1Algorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  //
  if(m_dbConfig==VeloTELL1::DYNAMIC){
    regUpdateTell1Conds();
    StatusCode mgrSvcStatus=runUpdate();
    if(mgrSvcStatus.isFailure()){
      return ( Error("Failed first UMS update", mgrSvcStatus) );
    }
  }else if(m_dbConfig==VeloTELL1::STATIC){
    // initialize maps with static values
    m_mcmsEnableMap[0]=m_mcmsProcessEnable;
  }
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloTELL1MCMS::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  //
  m_evtNumber++;
  StatusCode engineStatus;
  if(!isInitialized()){
    StatusCode initStatus=INIT();
    if ( !initStatus ) {
      debug() << "Unable to initialize.  NZS data missing?" << endmsg;
      return StatusCode::SUCCESS;
    } 
    engineStatus=createAndConfigureEngines();
    if(m_forceEnable) this->setIsEnable(m_mcmsProcessEnable);
    if(isEnable()){
      info()<< " --> Algorithm " << (this->algoName())
            << " of type: " << (this->algoType())
            << " is enabled and ready to process data --" <<endmsg;
    }else{
      info()<< " --> Algorithm " << (this->algoName())
            << " is disabled! " <<endmsg;
    }
  }
  StatusCode dataStatus;
  if(isEnable()&&(m_evtNumber>convergenceLimit())){
    dataStatus=getData();
    if(dataStatus.isSuccess()&&engineStatus.isSuccess()){
      dataStatus=inputStream(inputData());
      if(dataStatus.isSuccess()){
        runMCMSuppression();
        writeData();
      }else{
        return ( StatusCode::SUCCESS );
      }
    }else{
	    return StatusCode::SUCCESS;
    }
  }else{
    dataStatus=getData();
    if(dataStatus.isSuccess()){
      cloneData();
    }else{
	    return StatusCode::SUCCESS;
    }
  }
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloTELL1MCMS::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return VeloTELL1Algorithm::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode VeloTELL1MCMS::runMCMSuppression()
{
  debug()<< " runMCMSuppression() " <<endmsg;
  //
  LHCb::VeloTELL1Datas::const_iterator sensIt;
  sensIt=inputData()->begin();
  // 
  for( ; sensIt!=inputData()->end(); ++sensIt){
    // get pointer to the velo sensor related with given TELL1
    const unsigned int tell1=static_cast<unsigned int>((*sensIt)->key());
    const DeVeloSensor* sens=deVelo()->sensorByTell1Id(tell1);

    StatusCode nzsSvcStatus=this->getNZSSvc();
    if(!nzsSvcStatus.isSuccess()) return ( StatusCode::FAILURE );
    if(this->nzsSvc()->counter(tell1)<=convergenceLimit()) continue;

    if(sens==0){
      Error(" ==> No match between TELL1 and sensor found! ");
      Error(" ==> Check your XML conditions file! ");
      return ( StatusCode::FAILURE );
    }
    unsigned int sensNb=sens->sensorNumber();
    debug()<< " sensor number: " << sensNb << " for TELL1 number: "
          << tell1 <<endmsg;
    // check if the sensor is read out
    if(sens->isReadOut()){
      // create a new VeloTELL1Data object for cm suppressesd data
      LHCb::VeloTELL1Data* mcmsData=
        new LHCb::VeloTELL1Data(tell1, VeloTELL1::VeloFull);
      if((*sensIt)->isReordered()){
        propagateReorderingInfo(*sensIt, mcmsData);
      }
      // data from current sensor
      VeloTELL1::sdataVec inData=(*sensIt)->data();
      m_mcmsEngines[tell1]->setInData(inData);
      m_mcmsEngines[tell1]->runMCMS();
      mcmsData->setDecodedData(m_mcmsEngines[tell1]->outData());
      // store the corected data
      outputData(mcmsData);
    }
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1MCMS::createAndConfigureEngines()
{
  debug()<< " ==> createAndConfigureEngine() "<<endmsg;
  // pedestal engine
  VeloTELL1::AListPair idList=this->getSrcIdList();
  VeloTELL1::ucIT iT=idList.first;
  if(iT==idList.second){
    return Error(" ==> Empty list with src Ids!");
  }else{
    // create engines and put them into the map with tell1 as keys
    if(m_dbConfig==VeloTELL1::STATIC){
      info()<< " --> Creating static config for MCMS! " <<endmsg;
      for( ; iT!=idList.second; ++iT){
        m_mcmsEngines[*iT]=new TELL1MCMSProcessEngine();
        m_mcmsEngines[*iT]->setProcessEnable(m_mcmsEnableMap[0]);
      }
    }else if(m_dbConfig==VeloTELL1::DYNAMIC){
      // all parameters are already cached - use them to config engines
      info()<< " --> Creating dynamic config for MCMS! " <<endmsg;
      for( ; iT!=idList.second; ++iT){
        // pedestal processing objects
        m_mcmsEngines[*iT]=new TELL1MCMSProcessEngine();
        m_mcmsEngines[*iT]->setProcessEnable(m_mcmsEnableMap[*iT]);
      }
    }else{
      return ( Error(" ==> Unknown configuration mode! ") );
    }
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1MCMS::i_cacheConditions()
{
  debug()<< " ==> i_cacheCondition() " <<endmsg;
  VeloTELL1::AListPair iniList=getSrcIdList();
  if(iniList.first==iniList.second){
    return ( Error(" --> No Tell1 List cached!", StatusCode::FAILURE) );
  }
  VeloTELL1::ucIT srcId=iniList.first;
  // loop over all tell1s and initialize conditions
  for( ; srcId!=iniList.second; ++srcId){
    unsigned int tell1=*(srcId);
    std::string stell1=boost::lexical_cast<std::string>(tell1);
    std::string condName=m_condPath+"/VeloTELL1Board"+stell1;
    Condition* cond=getDet<Condition>(condName);
    // enable flag for pedestal process
    m_mcmsEnableMap[tell1]=cond->param<int>("mcms_enable");
  }
  return ( StatusCode::SUCCESS );
}
//--
