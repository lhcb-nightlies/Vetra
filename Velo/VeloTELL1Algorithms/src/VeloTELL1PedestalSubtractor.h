// $Id: VeloTELL1PedestalSubtractor.h,v 1.11 2009-12-11 12:21:06 szumlat Exp $
#ifndef VELOTELL1PEDESTALSUBTRACTOR_H 
#define VELOTELL1PEDESTALSUBTRACTOR_H 1

// Include files
#include "VetraKernel/VeloTELL1Algorithm.h"
#include "DetDesc/Condition.h"
#include "GaudiKernel/IUpdateManagerSvc.h"

/** @class VeloTELL1PedestalSubtractor VeloTELL1PedestalSubtractor.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2007-02-08
 */

class TELL1PedestalProcessEngine;
class TELL1UpdateProcessEngine;

class VeloTELL1PedestalSubtractor: public VeloTELL1Algorithm{
public:

  enum pedLineValue{
    VARIATION_THRESHOLD=15,
    PEDESTAL=512
  };

  enum ram{
    PED_SUM_SHIFT=10,
    RAM_INIT=512,
    SIGNAL_ADC=590,
    CNT_LIMIT=1024,
    TRAINING_CYCLE=4000,
    SUM_RAM_INIT=524288 // 512*1024
  };

  enum sens{
    PILE_UP_SENSORS=4,
    VELO_LAST_SENSOR=105
  };

  typedef std::vector<int> sensorRAM;
  typedef std::map<unsigned int, sensorRAM> RAM;
  
  /// Standard constructor
  VeloTELL1PedestalSubtractor(
  const std::string& name, ISvcLocator* pSvcLocator);

  virtual ~VeloTELL1PedestalSubtractor( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:
 
  StatusCode getHeaders();
  StatusCode validationRun();
  StatusCode runPedestalSub();
  StatusCode subtractConstPedestal(const unsigned int tell1);
  StatusCode subtractPedestal(const unsigned int tell1);
  StatusCode prepareMemory();
  StatusCode pedestalSumUpdate(sdataVec& adcs, const unsigned int sensCnt);
  StatusCode saturateSubtractedData(sdataVec& inVector);
  StatusCode writePedestals();
  void extractPedestals();
  void prepareEngineForValidation();
  StatusCode createAndConfigureEngines();
  LHCb::VeloTELL1Datas* headers() const;
  StatusCode checkBanks() const;
  StatusCode i_cacheConditions() override;
  std::vector<int>& inputScrambler(std::vector<int>& input);

private:

  LHCb::VeloTELL1Datas* m_headersCont;
  std::string m_headersLoc;
  VeloTELL1::sdataVec m_sensorData;
  VeloTELL1::sdataVec m_pedestalLine;
  bool m_subtractConstPedestal;
  bool m_subtractPedestal;
  bool m_ignoreMasks;
  RAM m_pedestalSumMemory;           /// this one stores adc sum
  LHCb::VeloTELL1Datas* m_subPeds;   /// output - subtracted pedestals
  std::string m_subPedsLoc;          /// TES location of the subtracted peds
  unsigned int m_scalingMode;
  unsigned int m_checkALink;
  std::map<unsigned int, TELL1PedestalProcessEngine*> m_pedestalEngines;
  VeloTELL1::LinkPedestalVec m_linkPedestal;
  VeloTELL1::LinkMaskVec m_linkMask;
  VeloTELL1::sdataVec m_headers;
  VeloTELL1::HeaderThresholdsVec m_headerThresholdsVec;
  VeloTELL1::HeaderCorrValuesVec m_headerCorrValuesVec;
  std::map<unsigned int, TELL1UpdateProcessEngine*> m_updateEngines;
  unsigned int m_headerCorrection;
  unsigned int m_pedestalProcessEnable;
  unsigned int m_updateProcessEnable;
  VeloTELL1::IntCondMap m_pedestalEnableMap;
  VeloTELL1::IntCondMap m_scalingModeMap;
  VeloTELL1::IntCondMap m_zsEnableMap;
  VeloTELL1::IntCondMap m_headerCorrEnableMap;
  VeloTELL1::LinkMaskMap m_linkMaskMap;
  VeloTELL1::IntCondMap m_updateEnableMap;
  VeloTELL1::HeaderThresholdsMap m_headerThresholdsMap;
  VeloTELL1::HeaderCorrValuesMap m_headerCorrValuesMap;
  VeloTELL1::dataVec m_excludedTell1List;
  bool m_memoryBuilt;
  bool m_pileUp;
  std::vector<int> m_scramblerBuffer;
  std::vector<int> m_scramblerPattern;
  
};
#endif // VELOTELL1PEDESTALSUBTRACTOR_H
