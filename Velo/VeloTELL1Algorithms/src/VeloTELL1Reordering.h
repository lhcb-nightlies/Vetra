// $Id: VeloTELL1Reordering.h,v 1.6 2008-05-28 08:12:32 szumlat Exp $
#ifndef VELOTELL1REORDERING_H 
#define VELOTELL1REORDERING_H 1

// Include files
#include "VetraKernel/VeloTELL1Algorithm.h"

/** @class VeloTELL1Reordering VeloTELL1Reordering.h
 *    
 * The algorithm implements TELL1 topological channel reordering.
 *
 * Reordering modes: by default there is no reordering performed!
 * If there is problem with data or any else difficulties algorithm
 * just copy the input data. In other case reordering type will be 
 * set dynamically using VeloDet info about sensor type
 *
 * One mandatory variable that should be set up by the user is
 * reordering direction i.e. old position -> new position or
 * new position -> old position (Option file ReorderingDirection)
 *
 *  @author Tomasz Szumlak
 *  @date   2007-02-08
 */

class TELL1ReorderProcessEngine;

class VeloTELL1Reordering : public VeloTELL1Algorithm {

public: 

  enum sectorCorr{
    NO_SECTOR_CORR=0,
    APPLY_SECTOR_CORR=1
  };

  /// Standard constructor
  VeloTELL1Reordering( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloTELL1Reordering( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode createAndConfigureEngines();
  void cableErrorCorrection(VeloTELL1::sdataVec& inVec);
  StatusCode topologicalChannelReordering();
  StatusCode reorder();
  StatusCode i_cacheConditions() override;

private:

  LHCb::VeloTELL1Datas* m_reorderedData;
  int m_sectorCorr;
  bool m_printInfo;
  /// mode:1==PHI, 2==R, 0 nothing to do
  VeloTELL1::IntCondMap m_reorderEnableMap;
  std::map<unsigned int, TELL1ReorderProcessEngine*> m_reorderEngines;
  unsigned int m_reorderProcessEnable;
  
};
#endif // VELOTELL1REORDERING_H
