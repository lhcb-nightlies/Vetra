// $Id: $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// local
#include "nzsStreamListener.h"

// boost
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>

//-----------------------------------------------------------------------------
// Implementation file for class : nzsStreamListener
//
// 2010-07-07 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( nzsStreamListener )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
nzsStreamListener::nzsStreamListener( const std::string& name,
                                      ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator ),
    m_isDebug ( false ),
    m_evtNumber ( 0 ),
    m_nzsInfo ( 0 ),
    m_cntMapLocal ( ),
    m_nzsStream ( 0 )
{
  declareProperty("NZSStreamLocation", m_nzsStreamLoc=LHCb::RawEventLocation::Default);
  declareProperty("NZSInfoLocation", m_nzsInfoLoc="Raw/Velo/NZSInfo");
}
//=============================================================================
// Destructor
//=============================================================================
nzsStreamListener::~nzsStreamListener() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode nzsStreamListener::initialize() {
  m_isDebug=msgLevel(MSG::DEBUG);
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if(m_isDebug) debug() << "==> Initialize" << endmsg;

  // -> build counters map for the full VELO
  for(unsigned int rsens=R_SENS_START; rsens<=R_SENS_END; ++rsens){
    m_cntMapLocal.insert(VeloTELL1::cntElement(rsens, 0));
  }
  for(unsigned int psens=PHI_SENS_START; psens<=PHI_SENS_END; ++psens){
    m_cntMapLocal.insert(VeloTELL1::cntElement(psens, 0));
  }
  for(unsigned int pusens=PU_SENS_START; pusens<=PU_SENS_END; ++pusens){
    m_cntMapLocal.insert(VeloTELL1::cntElement(pusens, 0));
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode nzsStreamListener::execute() {

  if(m_isDebug) debug() << "==> Execute" << endmsg;

  m_evtNumber++;
  StatusCode nzsStreamStatus=getNZSStream();
  if(nzsStreamStatus.isSuccess()){
    createInfoService();
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode nzsStreamListener::finalize() {

  if(m_isDebug) debug() << "==> Finalize" << endmsg;

  this->nzsInfoReport();

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

//=============================================================================
StatusCode nzsStreamListener::getNZSStream()
{

  if(m_isDebug) debug()<< " --> getNZSStream() " <<endmsg;

  if(!exist<LHCb::RawEvent>(m_nzsStreamLoc)){
    if(m_isDebug) debug()<< " ==> There is no RawEvent at: "
                         <<  m_nzsStreamLoc  <<endmsg;
    return ( StatusCode::FAILURE );
  }else{  
    // get the RawEvent from default TES location
    m_nzsStream=get<LHCb::RawEvent>(m_nzsStreamLoc);
    if(m_isDebug) debug()<< " ==> The RawEvent has been read-in from location: "
                         << m_nzsStreamLoc  <<endmsg; 
    m_nzsInfo=new VeloNZSInfos(); 
  }
  //
  return ( StatusCode::SUCCESS );
}

//=============================================================================
void nzsStreamListener::nzsInfoReport() const
{
  if(m_isDebug) debug()<< " --> nzsInfoReport()" <<endmsg;
  //
  VeloTELL1::Tell1CountersMap::const_iterator cntIT;
  cntIT=m_cntMapLocal.begin();
  if(m_isDebug) debug()<< " --> Map size: " << (m_cntMapLocal.size()) <<endmsg;
  info()<< " ============================================================= ";
  info()<<endmsg;
  info()<< " +++++++ NZS Stream Summary Info ++++++++ " <<endmsg;
  info()<< " ============================================================= ";
  info()<<endmsg;
  info()<< " -->  Number of processed events seen by the emulator: " 
        << m_evtNumber <<endmsg;
  info()<< " --> NZS data banks present in the data " <<endmsg;
  for( ; cntIT!=m_cntMapLocal.end(); ++cntIT){
    this->printInfo(*cntIT);
  }
  info()<< " ============================================================= ";
  info()<<endmsg;
  //
  return;
}

//=============================================================================
void nzsStreamListener::createInfoService()
{
  if(m_isDebug) debug()<< " --> createInfoService()" <<endmsg;
  //
  const std::vector<LHCb::RawBank*>& fullStream=
        m_nzsStream->banks(LHCb::RawBank::VeloFull);

  if(m_isDebug) debug()<< " raw bank size: " << fullStream.size() <<endmsg;
  VeloNZSInfo* infoSvc=new VeloNZSInfo();

  if(0!=fullStream.size()){
    
    std::vector<LHCb::RawBank*>::const_iterator bankIT=fullStream.begin();
    for( ; bankIT!=fullStream.end(); ++bankIT){

      const unsigned int tell1=static_cast<unsigned int>((*bankIT)->sourceID());
      if(m_isDebug) debug()<< " --> nzs bank id: " << tell1 <<endmsg;
      m_cntMapLocal[tell1]++;

    }

    infoSvc->setIsNZSStreamPresent(true);
    
  }else{
    
    infoSvc->setIsNZSStreamPresent(false);

  }

  infoSvc->setTell1CountersMap(m_cntMapLocal);
  m_nzsInfo->push_back(infoSvc);
  put(m_nzsInfo, m_nzsInfoLoc);
  //
  return;
}

//=============================================================================
void nzsStreamListener::printInfo(const VeloTELL1::cntElement cnt) const
{
  if(m_isDebug) debug()<< " --> printInfo()" <<endmsg;
  //
  info()<< " Bank id: " << cnt.first << " detected: " << cnt.second
        << " time(s) " <<endmsg;
  //
  return;
}
//@-
