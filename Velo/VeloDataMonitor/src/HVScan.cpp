// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/AlgFactory.h" 
#include "Event/VeloODINBank.h"
#include "VeloEvent/VeloTELL1Data.h"
#include "Event/ODIN.h"
#include "CommonFunctions.h"
#include "boost/format.hpp"
//-----------------------------------------------------------------------------
// Class : HVScan
//
// 2011-08-04: J. Garofoli
//             production version
//-----------------------------------------------------------------------------

using namespace LHCb;
using namespace VeloTELL1;
namespace Velo
{
  namespace Monitoring
  {
    class  HVScan : public GaudiHistoAlg {
    public:
      /// Standard constructor
      HVScan( const std::string& name, ISvcLocator* pSvcLocator );
      virtual ~HVScan( ); ///< Destructor
      virtual StatusCode initialize() override;    ///< Algorithm initialization
      virtual StatusCode execute   () override;    ///< Algorithm execution
      virtual StatusCode finalize  () override;    ///< Algorithm finalization

    private  :
      VeloTELL1Datas* m_tell1DataContainer;
      std::vector<int> m_evtCounter;
      std::vector<int> m_evtProcessed;
      std::vector<int> m_excludedTell1s;

      bool   m_plotOneLink;
      bool   m_plotOneChan;
      bool   m_useodin;

      bool   m_processRAW;
      bool   m_processPedSub;
      bool   m_processMCMS;
      bool   m_processFIR;
      bool   m_processPedCmSub;

      int    m_OneLink;
      int    m_OneTELL1;
      int    m_OneChan;
      int    m_skipFirstChansInLink;
      int    m_skipFirstEventsInStep;
      int    m_skipFirstEvents;
      int    m_evtCounterTot;
      int    m_hvstep;
      int    m_odinstep;
      int    m_ProcessEventsPerStep;
      virtual StatusCode loopover(const std::string& mytype);

      //Returns a vector with the link ADCs.
      std::vector<int> getLink(VeloTELL1::ALinkPair begEnd) {   
          std::vector<int>::const_iterator iT; //iterator to vector of unsigned int.
          std::vector<int> linkVector;
          linkVector.clear();
          //Loop over ADC values in link
          for(iT=begEnd.first; iT!=begEnd.second; ++iT){    
              linkVector.push_back(*iT); //Push back ADC value to vector
          }
          return linkVector;
      }
    };
  }
}



//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::HVScan::HVScan( const std::string& name,
				  ISvcLocator* pSvcLocator)
  :  GaudiHistoAlg( name , pSvcLocator )
{
  declareProperty( "ExcludedTELL1s",            m_excludedTell1s             );
  declareProperty( "SkipFirstEventsInStep",     m_skipFirstEventsInStep =   0);    // skip the first n events in each step
  declareProperty( "SkipFirstEvents",           m_skipFirstEvents       =   0);    // skip the first n events in the job (for pedestal calculation)
  declareProperty( "ProcessEventsPerStep",      m_ProcessEventsPerStep =  500);    // number of events to use in the histos per step  (if negative number, process all events after the skipped ones)
  declareProperty( "PlotOneLink",               m_plotOneLink =          true);    // plot all channels of one link in one tell1
  declareProperty( "OneLink",                   m_OneLink =                11);    // the link the plot the channels of
  declareProperty( "OneTELL1",                  m_OneTELL1 =               14);    // the tell1 of the link
  declareProperty( "PlotOneChan",               m_plotOneChan =         false);    // make histos for all steps but only one channel from each link and tell1
  declareProperty( "OneChan",                   m_OneChan =                11);    // which channel to use for the OneChan histos
  declareProperty( "SkipFirstChansInLink",      m_skipFirstChansInLink =    0);    // how many channels to skip from cross talk at the beginning of each link
  declareProperty( "UseODIN",                   m_useodin =              true);    // use the odin->calibrationstep number for the hvstep
  declareProperty( "ODINStep",                  m_odinstep =                0);    // manually set the hvstep, for historical data, if m_useodin=false
  declareProperty( "ProcessRAW",                m_processRAW =          false);    // draw histos for the RAW ADCs
  declareProperty( "ProcessPedSub",             m_processPedSub =       false);    // draw histos for the PedSub ADCs
  declareProperty( "ProcessMCMS",               m_processMCMS =          true);    // draw histos for the MCMS ADCs
  declareProperty( "ProcessFIR",                m_processFIR =          false);    // draw histos for the FIR ADCs (don't use)
  declareProperty( "ProcessPedCmSub",           m_processPedCmSub =     false);    // draw histos for the PedCmSub ADCs (don't know how to use yet, but the hook is there)

}
//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::HVScan::~HVScan() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::HVScan::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  debug() << "==> Initialize" << endmsg;
  setHistoTopDir( "Velo/" );

  int i;
  for (i=0;i<200;++i){
      m_evtCounter.push_back(0);
      m_evtProcessed.push_back(0);
  }
  m_evtCounterTot = 0;

  return StatusCode::SUCCESS;

}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::HVScan::finalize() {
  debug() << "==> Finalize" << endmsg;
  int i;
  for (i=0;i<200;++i){
      plot1D((double)(i),"extra/evtCounter","events run over;step;N",-0.5,199.5,200,(double)(m_evtCounter[i]));
      plot1D((double)(i),"extra/evtProcessed","events included in histos;step;N",-0.5,199.5,200,(double)(m_evtProcessed[i]));
  }
  return GaudiHistoAlg::finalize();  // must be called after all other actions
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::HVScan::execute() {
    debug() << "==> Execute" << endmsg;

    if (m_useodin == true){
        LHCb::ODIN* odin=0;
        if (exist<LHCb::ODIN>((LHCb::ODINLocation::Default))){
            odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
        }
        m_hvstep = odin->calibrationStep();
    }else{
        m_hvstep = m_odinstep;
    }
    m_evtCounter[m_hvstep]++;
    m_evtCounterTot++;

    if (m_evtCounter[m_hvstep] < m_skipFirstEventsInStep) {
        return StatusCode::SUCCESS;
    }else if (m_evtCounter[m_hvstep] > (m_skipFirstEventsInStep+m_ProcessEventsPerStep) && m_ProcessEventsPerStep > 0) {
        return StatusCode::SUCCESS;
    }

    if (m_evtCounterTot < m_skipFirstEvents) {
        return StatusCode::SUCCESS;
    }

    m_evtProcessed[m_hvstep]++;

    if (m_processRAW == true) {
        loopover("RAW");
    }

    if (m_processPedSub == true) {
        loopover("PedSub");
    }

    if (m_processMCMS == true) {
        loopover("MCMS");
    }

    if (m_processFIR == true) {
        loopover("FIR");
    }

    if (m_processPedCmSub == true){
        loopover("PedCmSub");
    }

    return StatusCode::SUCCESS;
}


StatusCode Velo::Monitoring::HVScan::loopover(const std::string& mytype) {
    int xlow = -100.5;
    int xhigh = 100.5;
    int nbins = 201;
    if (mytype == "RAW" && m_processRAW == true) {
        if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::ADCs  )){ //put in location here.
            debug()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::ADCs  <<endmsg;
            return StatusCode::FAILURE;
        }else{
            ////info() << "Bank gotten!" << endmsg;
            m_tell1DataContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::ADCs );
            debug() << "m_tell1dataContainer size is " << sizeof(m_tell1DataContainer) << endmsg;
            xlow = -0.5;
            xhigh = 1024.5;
            nbins = 1025;
        }
    }else if (mytype == "PedSub" && m_processPedSub == true) {
        if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::PedSubADCs  )){ //put in location here.
            debug()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::PedSubADCs  <<endmsg;
            return StatusCode::FAILURE;
        }else{
            ////info() << "Bank gotten!" << endmsg;
            m_tell1DataContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::PedSubADCs );
            debug() << "m_tell1dataContainerPedSub size is " << sizeof(m_tell1DataContainer) << endmsg;
        }
    }else if (mytype == "MCMS" && m_processMCMS == true) {
        if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::MCMSCorrectedADCs  )){ //put in location here.
            debug()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::MCMSCorrectedADCs  <<endmsg;
            return StatusCode::FAILURE;
        }else{
            ////info() << "Bank gotten!" << endmsg;
            m_tell1DataContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::MCMSCorrectedADCs );
            debug() << "m_tell1dataContainer size is " << sizeof(m_tell1DataContainer) << endmsg;
        }
    }else if (mytype == "FIR" && m_processFIR == true) {
        if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::FIRCorrectedADCs  )){ //put in location here.
            debug()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::FIRCorrectedADCs  <<endmsg;
            return StatusCode::FAILURE;
        }else{
            ////info() << "Bank gotten!" << endmsg;
            m_tell1DataContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::FIRCorrectedADCs );
            debug() << "m_tell1dataContainer size is " << sizeof(m_tell1DataContainer) << endmsg;
        }
    }else if (mytype == "PedCmSub" && m_processPedCmSub == true) {
        if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::CMSuppressedADCs  )){ //put in location here.
            debug()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::CMSuppressedADCs  <<endmsg;
            return StatusCode::FAILURE;
        }else{
            ////info() << "Bank gotten!" << endmsg;
            m_tell1DataContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::CMSuppressedADCs );
            debug() << "m_tell1dataContainer size is " << sizeof(m_tell1DataContainer) << endmsg;
        }
    }else{
        // if somebody isn't to be processed, e.g. m_processMCMS == false, quit and go on w/o breaking
        return StatusCode::SUCCESS;
    }

    VeloTELL1Datas::const_iterator sensIt;
    for (sensIt=m_tell1DataContainer->begin();sensIt!=m_tell1DataContainer->end();++sensIt){
        VeloTELL1Data* dataOneTELL1=(*sensIt);
        int TELL1_number=dataOneTELL1->key();
        if(ExcludeTELL1(m_excludedTell1s,TELL1_number))  continue; // get rid of unecessary tell1s.
        for(int link = 0; link<NumberOfALinks; ++link){
            boost::format dir ( "%s/steps/step%d/TELL1_%03d/");
            boost::format shortname ( "%sstep_%d_TELL1_%03d_link_%d" );
            boost::format tell1 ("TELL1_%03d");
            boost::format fulltitle ( "%s entries for step %d, TELL1 %03d, link %d" );

            tell1     % TELL1_number;
            dir       % mytype % m_hvstep % TELL1_number; 
            shortname % mytype % m_hvstep % TELL1_number % link;
            fulltitle % mytype % m_hvstep % TELL1_number % link;

            std::vector<int> linkADCs = getLink( (*dataOneTELL1)[link] );
            std::vector<int>::iterator it;
            int count =-1;

            for(it=linkADCs.begin();it!=linkADCs.end() ; ++it){
                ++count;
                if ( count == m_OneChan && m_plotOneChan){
                    boost::format myname0 ("%s/onechan/step%d/TELL1_%03d/link_%d_chan_%d");
                    boost::format mytitle0("step_%d_TELL1_%03d_link_%d_chan_%d");

                    myname0 % mytype % m_hvstep % TELL1_number % link % count;
                    mytitle0 % m_hvstep % TELL1_number % link % count;

                    plot1D((double)(*it),myname0.str(),mytitle0.str(),xlow,xhigh,nbins);
                }
                if ( TELL1_number == m_OneTELL1 && link == m_OneLink && m_plotOneLink){
                    boost::format myname ("%s/onelink/step%d/TELL1_%03d/link_%d_chan_%d");
                    boost::format mytitle("step_%d_TELL1_%03d_link_%d_chan_%d");

                    myname % mytype % m_hvstep % TELL1_number % link % count;
                    mytitle % m_hvstep % TELL1_number % link % count;

                    plot1D((double)(*it),myname.str(),mytitle.str(),xlow,xhigh,nbins);
                }

                if (count <= m_skipFirstChansInLink) {  // skip the first strip
                    continue;
                }
                plot1D((double)(*it),dir.str() + shortname.str(),fulltitle.str(),xlow,xhigh,nbins);
            }
        }
    }
    return StatusCode::SUCCESS;
}

// Declaration of the Algorithm Factory

DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, HVScan )

