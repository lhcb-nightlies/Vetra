// $Id: PUClustersMoni.h,v 1.2 2010-03-10 15:10:27 serena Exp $
#ifndef PUCLUSTERSMONI_H 
#define PUCLUSTERSMONI_H 1

// std includes
#include <vector>
#include <map>

// ROOT
#include "TH2.h"
#include "TH1.h"
#include "TFile.h"
#include "TROOT.h"
#include "TMath.h"

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// from event
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"
#include "VeloEvent/VeloFullBank.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/L0DUReport.h"
#include "Event/L0DUBase.h"

// Interfaces for L0DU
#include "L0Interfaces/IL0DUFromRawTool.h"

// Velo
#include "Event/VeloCluster.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

// from TELL1 Kernel:
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "Tell1Kernel/VeloDecodeConf.h"

//#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
//#include "AIDA/IProfile1D.h"



namespace AIDA {
  class IHistogram2D;
  class IHistogram1D;
  class IProfile1D;
}

namespace LHCb
{
  class RawEvent;
  class RawBank;
}
class TTree;
class TFile;

/** @class PUClustersMoni PUClustersMoni.h
 *  
 * implementation of the basic monitor for VeloPedestalSubtractor
 * algorithm, one can produce a set of histograms with 
 * ADC samples and sebtracted pedestals
 *
 *  @author Tomasz Szumlak
 *  @date   2006-06-16
 */

class DeVelo;

class PUClustersMoni : public GaudiTupleAlg{

public:

  PUClustersMoni( const std::string& name, ISvcLocator* pSvcLocator );
 
  virtual ~PUClustersMoni( ); 
  virtual StatusCode initialize() override;    
  virtual StatusCode execute   () override;    
  virtual StatusCode finalize  () override; 
  StatusCode getData();
  StatusCode getL0DU();
  StatusCode getRawEvent();
  StatusCode rawPUClusterMonitor();
  StatusCode rawAnalogClusterMonitor();
  void bookTree();
  void bookTreeAdcNZS();
  std::vector<int> getLink(VeloTELL1::ALinkPair begEnd);
 
protected:

private:
   std::string	  m_rawEventLoc;
   LHCb::RawEvent *	  m_rawEvent;
   std::string 	  m_PUclusterContZS;
   std::string	  m_PUclusterContNZS;
   std::string    m_PUanalogclusterContNZS;
   std::string    m_PUanalogheaderContNZS;
   LHCb::VeloClusters * m_rawPUClustersZS;
   LHCb::VeloClusters * m_rawPUClustersNZS;
   int m_triggerType;
   std::string m_fromRawTool; ////L0DU
   IL0DUFromRawTool* m_fromRaw; //// L0DU

   // tree variables
   int m_numOfEvents;
   int m_numOfGoodEvents; 
   int m_numHitsZS, m_numHitsNZS;
   int m_stepNum;
   int m_bunchId;
   int m_evtNum;
   int m_bankSize;
   int m_bxBeamCrossing;
   int m_bxBeam1;
   int m_bxBeam2;
   int m_bxUnknown;
   int m_bxNoBeam;
   int m_stepNumTmp;
   int m_bunchIdTmp;
   int m_CaloTrg;
   int m_HcalTrg;
   int m_SpdTrg;
   int m_PUTrg;
   int m_MuonTrg;
   int m_PUmult;
   int m_PUpeak1;
   int m_PUpeak2;
   int m_PUstatus1;
   int m_PUstatus2;
   int m_PUmultFake;

   bool m_DoAnalogNZS;
   bool m_DoBinaryNZS;
   bool m_DoOnLine;
   IEventTimeDecoder* m_odinDecoder;
   TTree*        m_OutputTree; 
   TTree*        m_OutputTreeAnalog; 
   TFile*        m_OutputFile;
   std::string   m_OutputFileName;
   
   const DeVelo* m_velo;
  
   std::vector< IHistogram2D* > m_VeloNZSPlot;
   std::vector< IHistogram2D* > m_VeloNZSPlotH;
   std::vector< IHistogram1D* > m_numberHitsPlot;
   AIDA::IHistogram1D* m_numberPUClustersNZS;
   AIDA::IHistogram1D* m_numberPUClustersZS;
   AIDA::IHistogram2D* m_numberHitsMap2D;
   AIDA::IHistogram1D* m_bxId;
   AIDA::IHistogram1D* m_multHisto;
   AIDA::IHistogram1D* m_trgType;
   AIDA::IHistogram1D* m_banksizeHisto;
   AIDA::IHistogram1D* m_peak1Histo;
   AIDA::IHistogram1D* m_peak2Histo;

   //std::vector< IHistogram1D* > m_numberHitsPlot;

   //std::map<int, TH1F*> m_numberHitsPlot;
   /*
   TH2D*         m_VeloNZSPlot_128;
   TH2D*         m_VeloNZSPlot_129;
   TH2D*         m_VeloNZSPlot_130;
   TH2D*         m_VeloNZSPlot_131;
   TH2D*         m_VeloNZSPlotH_128;
   TH2D*         m_VeloNZSPlotH_129;
   TH2D*         m_VeloNZSPlotH_130;
   TH2D*         m_VeloNZSPlotH_131;
   */

   // tree variables
   int * sensorNZS_ptr;
   int * sensorZS_ptr;
   int * stripNZS_ptr;
   int * stripZS_ptr;  

   int * sensor_Velo;
   int * strip_Velo;
   int * channel_Velo;
   int * link_Velo;
   float * adc_Velo;
   float * head_Velo;
   int m_numHitsVelo, m_numHeadVelo, m_numHitsHeadVelo;
   int * stripZS_128_ptr;
   int * stripZS_130_ptr;
   int m_numHitsZS_128, m_numHitsZS_130;
   int * stripZS_129_ptr;
   int * stripZS_131_ptr;
   int m_numHitsZS_129, m_numHitsZS_131;
};
#endif // PUCLUSTERSMONI_H
