// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/AlgFactory.h" 
// Event
#include "VeloEvent/EvtInfo.h"
#include "Event/VeloODINBank.h"
#include "VeloEvent/VeloTELL1Data.h"
#include "GaudiKernel/IEventTimeDecoder.h"            // Interface
#include "Event/ODIN.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
// local
#include "CommonFunctions.h"
//velo
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"
// from TELL1 Kernel:
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "Tell1Kernel/VeloDecodeConf.h" 
// Boost 
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
//-----------------------------------------------------------------------------
// Class : ADCDelayScan
//
// 2009-07-07 : Kazu Akiba  ADC delay scan analysis. backwards compatible with the macros.
//-----------------------------------------------------------------------------
using namespace LHCb;
using namespace VeloTELL1;
namespace Velo
{
  namespace Monitoring
  {
	class  ADCDelayScan : public GaudiHistoAlg {
	  public:
		/// Standard constructor
		ADCDelayScan( const std::string& name, ISvcLocator* pSvcLocator );
		virtual ~ADCDelayScan( ); ///< Destructor
		virtual StatusCode initialize() override;    ///< Algorithm initialization
		virtual StatusCode execute   () override;    ///< Algorithm execution
		virtual StatusCode finalize  () override;    ///< Algorithm finalization
		int getSamplingPhase();
		std::vector<double> SubtractCommonMode( std::vector<int> headers, std::vector<int> data, double cut, int howManyPasses );
		int m_method;
	  private  :
		unsigned long int m_samplingPhaseFirstEvent;
		unsigned long int m_samplingPhaseBurstSize;
		unsigned long int m_samplingPhaseNBursts;
		unsigned long int m_samplingPhaseBurstId;
		unsigned long int m_runNumber;
		unsigned long long m_gpsTime;
		unsigned int m_newphasedeltaT;
		unsigned long long m_l0EventID;
		unsigned int m_l0EventCounter;
		VeloTELL1Datas* m_tell1DataContainer;
		VeloTELL1Datas* m_headerContainer;
		EvtInfos*       m_evtInfos;
		IEventTimeDecoder* m_odinDecoder; ///< Pointer to tool to decode ODIN bank
		unsigned long int  m_evtNumber;
		int m_refresh;
		std::vector<int> m_chosenTell1s;
		int m_plot2d;
		bool m_inverted;
		int m_plotcms;
		int m_plotmonitor;
		int m_plotraw;
		int m_plotprofile;
		double m_commonModeCut;
		unsigned int   m_nEventsToSkip;
		int   m_firsttime;
		double   m_nStat;
		int   m_lastSamplingPhase;
		std::map<int, std::vector<std::vector<float> > >  m_mean;//tell1id, [64][36];
		std::map<int, std::vector<std::vector<float> > >  m_rms;//tell1id, [64][36];
		bool m_evenodd;
		int l0EventCounter() { return   m_l0EventCounter; };
		void l0EventCounter( int id) { m_l0EventCounter = id; };
		bool OddEvent() { return ( m_l0EventCounter%2==1 ? true : false ); }
		bool EvenEvent() { return ( m_l0EventCounter %2==0 ? true : false ); }
		unsigned long long EventNumber() { return m_evtNumber; }
		//Returns a vector with the link ADCs.
		std::vector<int> getLink(VeloTELL1::ALinkPair begEnd) {   
		  std::vector<int>::const_iterator iT; //iterator to vector of unsigned int.
		  std::vector<int> linkVector;
		  linkVector.clear();
		  //Loop over ADC values in link
		  for(iT=begEnd.first; iT!=begEnd.second; ++iT) linkVector.push_back(*iT); //Push back ADC value to vector
		  return linkVector;
		}
	};
  }
}



//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::ADCDelayScan::ADCDelayScan( const std::string& name,
	ISvcLocator* pSvcLocator)
  :  GaudiHistoAlg( name , pSvcLocator )
  ,  m_l0EventCounter (0)
,  m_evtNumber ( 0 )
{
  declareProperty( "FirstEvent" ,                   m_samplingPhaseFirstEvent  =        1); 
  declareProperty( "BurstSize" ,                    m_samplingPhaseBurstSize =       1000);
  declareProperty( "NBursts" ,                      m_samplingPhaseNBursts =           25);
  declareProperty( "BurstId" ,                      m_samplingPhaseBurstId =            0);
  declareProperty( "PhaseChangeMethod",             m_method =                          2);
  declareProperty( "NewPhaseDeltaTcut",             m_newphasedeltaT =               1000);
  declareProperty( "TELL1s",                        m_chosenTell1s                       );
  declareProperty( "InvertedDelaySequence",         m_inverted = false                   );
  declareProperty( "Plot2D",                        m_plot2d = 0                         );
  declareProperty( "PlotRaw",                       m_plotraw = 0                        );
  declareProperty( "PlotProfile",                   m_plotprofile = 0                    );
  declareProperty( "PlotCMS",                       m_plotcms = 0                        );
  declareProperty( "PlotCMSMonitor",                m_plotmonitor =1                     );
  declareProperty( "CommonModeCut",                 m_commonModeCut =               16  );
  declareProperty( "NEventsToSkip",                 m_nEventsToSkip =               10  );
}

//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::ADCDelayScan::~ADCDelayScan() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::ADCDelayScan::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  debug() << "==> Initialize" << endmsg;
  m_odinDecoder = tool<IEventTimeDecoder>( "OdinTimeDecoder" );
  setHistoTopDir( "Vetra/" );
  m_nStat= m_samplingPhaseBurstSize-m_nEventsToSkip;
  m_lastSamplingPhase =-1;
  m_firsttime=0;
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::ADCDelayScan::finalize() {
  debug() << "==> Finalize" << endmsg;
  return GaudiHistoAlg::finalize();  // must be called after all other actions
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::ADCDelayScan::execute() {
  m_evtNumber++;
  debug() << "==> Execute" << endmsg;
  m_nStat= m_samplingPhaseBurstSize-m_nEventsToSkip;
  std::vector<int> arxCard;
  std::vector<int> iBeetle;
  std::vector<int> iLink;
  if(!exist<EvtInfos>( EvtInfoLocation::Default  )){
	debug()<< " ==> There is no Event info in the data. QUITTING. " <<endmsg;
	return ( StatusCode::SUCCESS );
  }else{
	m_evtInfos       = get<EvtInfos>      ( EvtInfoLocation::Default       );
  }
  m_odinDecoder->getTime();
  LHCb::ODIN *odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
  int iSamplingPhase=0;

  if(odin && m_method == 0){
	iSamplingPhase = odin->calibrationStep() ;
	if (m_samplingPhaseBurstId == 0) {
	  plot(iSamplingPhase,"hSamplingPhase","sampling phase",-0.5,(m_samplingPhaseNBursts-0.5),(m_samplingPhaseNBursts+2),1.);
	} else {
	  plot(iSamplingPhase,"hSamplingPhase","sampling phase",-1.5,(m_samplingPhaseNBursts+1.-0.5),(m_samplingPhaseNBursts+2),1.);
	}
  } else {
	debug() << "ODIN bank does not exist!" << endmsg;
	iSamplingPhase = getSamplingPhase();
  }
  if(m_evtNumber%100 == 0) info() << " this is the step: " <<  odin->calibrationStep() << "iSamplingPhase == "<< iSamplingPhase <<endmsg; 
  if (m_samplingPhaseBurstId == 0) {
	if(EventNumber() < m_samplingPhaseFirstEvent ||
		EventNumber() >=m_samplingPhaseFirstEvent 
		+ m_samplingPhaseBurstSize * m_samplingPhaseNBursts ) return StatusCode::SUCCESS ;
  }
  if((iSamplingPhase < 0) || (static_cast<unsigned int>(iSamplingPhase) >= m_samplingPhaseNBursts)) return StatusCode::SUCCESS ;

  if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::ADCs  )){ //put in location here.
	info()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::ADCs  <<endmsg;
	return StatusCode::FAILURE;

  }else{
	m_tell1DataContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::ADCs );
  }
  if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers )){ //put in location here.
	info()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::Headers  <<endmsg;
	return StatusCode::FAILURE;
  }else{
	m_headerContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers );
  }
  VeloTELL1Datas::const_iterator sensIt;
  VeloTELL1Datas::const_iterator headerIt= m_headerContainer->begin();

  if(m_inverted) iSamplingPhase = m_samplingPhaseNBursts - 1 - iSamplingPhase; 

  if( m_evtNumber%m_samplingPhaseBurstSize < m_nEventsToSkip) return StatusCode::SUCCESS ;
  if(m_firsttime ==-1) m_firsttime = 1;
  for(sensIt=m_tell1DataContainer->begin();sensIt!=m_tell1DataContainer->end();++sensIt){
	if(sensIt!=m_tell1DataContainer->begin()) ++headerIt;
	VeloTELL1Data* dataOneTELL1=(*sensIt);
	VeloTELL1Data* headerOneTELL1 = (*headerIt);
	int TELL1_number=dataOneTELL1->key();
	if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue;
	EvtInfo* anInfo=m_evtInfos->object(TELL1_number);
	l0EventCounter(anInfo->l0EventID(0));
	std::string polarity = (EvenEvent()==true ) ? "Even" : "Odd";
	for(int link = 0; link<NumberOfALinks; ++link){
	  //Set up directory structure and histogram names:    
	  boost::format dir ( "TELL1_%03d/");
	  dir % TELL1_number; 
	  //boost::format name ( "TELL1_%03d/hSamplingPhaseScan%s_Link_%d");
	  boost::format name ( "hSamplingPhaseScan%s_Link_%d");
	  name % polarity % link;           
	  boost::format subtractedcommonmode ( "TELL1_%03d/subtractedCommonmode%s_%d");
	  subtractedcommonmode % TELL1_number% polarity %link;           
	  boost::format title ( "SamplingPhaseScan%s for Link %d" );
	  title % polarity % link;
	  int nChannelsPlusHeaders=36;
	  int nxbins  = m_samplingPhaseNBursts*nChannelsPlusHeaders;
	  int nybins  = 256; 
	  std::vector<int> linkADCs = getLink( (*dataOneTELL1)[link] );
	  std::vector<int> linkHeaders = getLink( (*headerOneTELL1)[link] );
	  std::vector<int>::iterator it;
	  std::vector<double>::iterator itf;
	  std::vector<double> linkCMSub;
	  std::vector<double> HeadersCMSub;
	  double average = 0;
	  linkCMSub.resize(32);
	  HeadersCMSub.resize(4);
	  int count =0, chanelsinthesum =0;
	  for(it=linkADCs.begin();it!=linkADCs.end() ; ++it, count++){
		if((count >= 3) ){
		  average += (double)(*it);
		  chanelsinthesum++;
		}
	  }
	  average /= float(chanelsinthesum);
	  if(m_plotcms && m_plotmonitor) plot1D(average, subtractedcommonmode.str(),subtractedcommonmode.str(), 512-128.5,512+127.5, nybins);
	  int chanWithoutHits = 0;
	  float averageWithoutHits = 0;
	  count = 0;
	  for(it=linkADCs.begin();it!=linkADCs.end() ; it++, count++){
		linkCMSub[count] = float(*it) - average;
		if(count < 4) HeadersCMSub[count] = (float(linkHeaders[count]) - average);
	  }
	  count = 0; 
	  for(itf=linkCMSub.begin();itf!=linkCMSub.end() ; ++itf, count++){
		if(count >= 3){ 
		  if(fabs(*itf) < m_commonModeCut )  {
			averageWithoutHits += *itf;
			chanWithoutHits++;
		  }
		}
	  }
	  if(chanWithoutHits >0 ) averageWithoutHits /= float(chanWithoutHits);
	  else averageWithoutHits = 0;
	  count = 0;  
	  for(itf=linkCMSub.begin();itf!=linkCMSub.end() ; ++itf, count++){
		linkCMSub[count] = *itf - averageWithoutHits;
		if(count < 4) HeadersCMSub[count] = (HeadersCMSub[count] - averageWithoutHits);
	  }
	  count = 0;
	  for(itf=HeadersCMSub.begin(), it=linkHeaders.begin();itf!=HeadersCMSub.end() ; ++itf, ++it, count++){
		double j = float(count);
		double xval = (j*m_samplingPhaseNBursts + iSamplingPhase)/float(m_samplingPhaseNBursts) - 4.5+0.001;
		if((*it>512-3*128) && (*it<512+3*127)) if(m_plotcms&&m_plotprofile) profile1D(xval,*itf , dir.str()+"cms"+name.str(),
			title.str()+ " cms",-4.5,31.5,nxbins);
		if(m_plotraw && m_plotprofile )profile1D(xval,*it , dir.str()+name.str(),title.str(),-4.5,31.5,nxbins, "s");
		if(m_plot2d && m_plotraw) plot2D(xval,*it, dir.str()+"raw"+name.str()+"2d" ,"raw"+title.str(),-4.5,31.5,-1,1024,nxbins,2*nybins);
		if(m_plot2d && m_plotcms) plot2D(xval,*itf, dir.str()+name.str()+"2d",title.str(),-4.5,31.5,-128.5,+127.5,nxbins,nybins);
	  }   
	  count = 0;
	  for(itf=linkCMSub.begin(), it=linkADCs.begin(); itf!=linkCMSub.end() ; ++itf, it++, count++){
		double j = (double)(count);
		double xval = (j*m_samplingPhaseNBursts + iSamplingPhase)/float(m_samplingPhaseNBursts) - 0.5+0.001;
		if((*it>512-3*128) && (*it<512+3*127)) if(m_plotcms&&m_plotprofile) 
		  profile1D(xval,*itf , dir.str()+"cms"+name.str(),title.str()+ " cms",-4.5,31.5,nxbins, "s");
		if(m_plotraw &&  m_plotprofile) profile1D(xval,*it, dir.str()+name.str(),title.str(),-4.5,31.5,nxbins, "s");
		if(m_plot2d && m_plotraw ) plot2D(xval,*it, dir.str()+"raw"+name.str()+"2d","raw"+title.str(),-4.5,31.5,-1,1024,nxbins,2*nybins);
		if(m_plot2d && m_plotcms) plot2D(xval,*itf, dir.str()+name.str()+"2d",title.str(),-4.5,31.5,-128.5,+127.5,nxbins,nybins);
	  }
	}

  }
  m_lastSamplingPhase = iSamplingPhase;
  return StatusCode::SUCCESS;
}

int Velo::Monitoring::ADCDelayScan::getSamplingPhase() 
{

  //this method works for separating phases with the run number, a la assembly lab
  if(m_method==1){
	if (m_samplingPhaseBurstId == 0) {
	  int eventNumber = EventNumber() - m_samplingPhaseFirstEvent;
	  return ((int) (eventNumber/m_samplingPhaseBurstSize));
	} else {
	  if (m_runNumber == 0) {
		return (-1);
	  } else if (m_runNumber == 1) {
		return (m_samplingPhaseNBursts);
	  } else {
		return ((int) (m_runNumber%100000));  
	  } 
	}
  }

  if(m_method==2){
	static int phase=0;
	static unsigned long long timeprevious=0;
	static unsigned long long deltaT=0;

	deltaT = m_gpsTime - timeprevious;

	plot1D(deltaT,"deltaT","deltaT",0.,10.,10);
	debug() << "Time difference: " <<  m_gpsTime << " - " << timeprevious << " = " << deltaT << endmsg;

	timeprevious=m_gpsTime;

	if(deltaT>m_newphasedeltaT){
	  phase++;
	  info() << "Phase change! New phase is " << phase << endmsg;
	}
	return phase; 
  }
  if(m_method==3){
	return ((int) (EventNumber() / m_samplingPhaseBurstSize)); 	
  }
  return 0;
}
// Declaration of the Algorithm Factory

DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, ADCDelayScan )



