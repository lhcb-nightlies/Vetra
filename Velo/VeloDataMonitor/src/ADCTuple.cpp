#include <algorithm>

#include "Event/ODIN.h"

#include "GaudiKernel/AlgFactory.h" 
#include "GaudiKernel/IEventTimeDecoder.h"
// ============================================================================

// from Event model
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloErrorBank.h"

#include "Event/RawEvent.h"
#include "Event/RawBank.h"

//velo
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

// local
//#include "CommonFunctions.h"
#include "ADCTuple.h"

// AIDA 
//#include "AIDA/IHistogram1D.h"
//#include "AIDA/IHistogram2D.h"
//#include "AIDA/IProfile1D.h"

// from TELL1 Kernel:
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "Tell1Kernel/VeloDecodeConf.h" 

// Boost 
// ============================================================================
#include "boost/format.hpp"
// ============================================================================

#include "TFile.h"
#include <iostream>
#include <fstream>
#include "TH2F.h"
#include "TVectorT.h"
#include "TGraph.h"
#include "TAxis.h"


//-----------------------------------------------------------------------------
//  Class : Velo::Monitoring::ADCTuple
//
//  @author Chiara Farinelli   <chiara.farinelli@cern.ch>   
//  @date 03-02-2011 
//   
//  -> Plots ADC counts for 4 header bits and 32 channels for different Tell1s
//     (includes option to select Tell1s and Links to plot)
//  -> Creates an ntuple from the raw file which contains header adc values  
//     channel adc values for each Tell1
//  -> Creates text file with list of all events with an NZS bank  
//
//  Usage: gaudirun.py Vetra-Default-NZS_Data-Emul.py VetraMoni_ADC_NZS.py   
//   
//-----------------------------------------------------------------------------

using namespace LHCb;
using namespace VeloTELL1;

DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring,ADCTuple)

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::ADCTuple::ADCTuple( const std::string& name,
    ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
  ,  m_evtNumber(0)
  ,  m_refresh (5000)
  ,  m_veloDet ( 0 )
  ,  EB_Histograms( 0 )
  ,  m_tell1(0)
  ,  m_locations ()
{
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::ADCs             ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::PedSubADCs       ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::FIRCorrectedADCs ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ) ;
  
  declareProperty("RawEventLocation",m_rawEventLocation=LHCb::RawEventLocation::Default);
  declareProperty("ErrorBankLocation", m_errorBankLoc=VeloErrorBankLocation::Default);

  declareProperty( "Containers" ,          m_locations  ,  "The list of TES locations to be monitored") ;
  declareProperty( "TELL1s",               m_chosenTell1s);
  declareProperty( "Links",                m_chosenLinks);
  declareProperty( "RefreshRate",          m_refresh);
  declareProperty( "Plot2DHistos",         m_plot2DADCs=false);
  declareProperty( "SplitInEvenOdd",       m_evenodd=false);
  declareProperty( "LinkPlot",             m_LinkPlot=false);
  declareProperty( "NtupleLocation",       m_NtupleLocation="/tmp/Ntuple.root"); 

  setHistoTopDir( "Vetra/" );

}
//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::ADCTuple::~ADCTuple() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::ADCTuple::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  debug() << "==> Initialize" << endmsg;
  
  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );

  EB_Histograms = new TFile("NZSdata_plots.root","RECREATE");
 
  m_evtList.open("NZS_Events.txt",std::ios_base::out);
  
  //f = TFile::Open("/scratch/everyone/cfarinel/NTuple_81811_file03.root","RECREATE");
  f = TFile::Open( m_NtupleLocation.c_str() ,"RECREATE");
  
  t = new TTree("tree","this is a test tree");
  
  t->Branch("header_vector",   &header_vector);
  t->Branch("channel_vector",  &channel_vector);
  t->Branch("Tell1_vector",    &Tell1_vector);
  t->Branch("L0ID_vector",    &L0ID_vector);

  if(m_veloDet == 0) Error("Can't fint detector element. Exiting with Failure",StatusCode::FAILURE);
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::ADCTuple::finalize() {
  debug() << "==> Finalize" << endmsg;  

  EB_Histograms->Close(); 
  m_evtList.close();

  delete EB_Histograms;

  //t->Write();

  f->WriteTObject(t,t->GetName());
  f->Close();

  //delete t ;
  delete f ;

  return GaudiHistoAlg::finalize();  // must be called after all other actions
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::ADCTuple::execute() {

  debug() << "==> Execute" << endmsg;  
  //info() << "==> We're running!" << endmsg;  
  m_evtNumber++;    

  //info() << "event= " << m_evtNumber << endmsg;
  
  plotADCs();  
  
  return StatusCode::SUCCESS;
}
//==============================================================================================================
//==============================================================================================================
StatusCode Velo::Monitoring::ADCTuple::plotADCs(){

  VeloTELL1Datas* m_headerContainer;
  VeloTELL1Datas* m_tell1DataContainer;
 

  //==============================================================================
  //================== Looping over input containers =============================
  //==============================================================================
  
  L0ID_vector.clear();

  for ( std::vector<std::string>::const_iterator il = m_locations.begin() ;  m_locations.end() != il ; ++il ) {
    
    LHCb::ODIN* odin=0;
    
    if(exist<LHCb::ODIN>((LHCb::ODINLocation::Default))){
      odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
    }
    else{
      odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
    }
    
    int L0_evtID = odin->eventNumber(); // this is the LO event ID
    
    L0ID_vector.push_back(L0_evtID);

    if(!exist<VeloTELL1Datas>( *il)){
      info()<< " ==> There is no bank at " << *il <<endmsg;
    }   
    if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers )){
      debug()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::Headers  <<endmsg;
      return StatusCode::SUCCESS;
    }
    else{
      m_tell1DataContainer = get<VeloTELL1Datas>( *il );
      m_headerContainer    = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers );
    }
    
    VeloTELL1Datas::const_iterator headerIt;
    
    int link;
    char histo_ADC[300];
    char histo_ADC_all[300];
    char histo_header[300];
    //char histo_ALLheaders[300];
    char histo_Tell1s[300];
  
    TH2F *ADC_all       = 0;   
    TH2F *ADC           = 0;   
    TH2F *headers_all   = 0;
    TH2F *Tell1_headers = 0;
    //TH2F *H4_perLink  = 0;
    //TH2F *C0_perLink  = 0;
 
    //const int n = 64;
    //double x[n];
    //double y[n];

    //==============================================================================
    //============= Plotting the ADC counts for header bits ========================
    //==============================================================================
    
    for(headerIt=m_headerContainer->begin(); headerIt!=m_headerContainer->end(); ++headerIt){

      //Tell1_vector.clear();
      
      VeloTELL1Data* headerOneTELL1=(*headerIt);
      int TELL1_number=headerOneTELL1->key();
      
      Tell1_vector.push_back(TELL1_number);
      
      //info() << "Event= " << m_evtNumber << " L0_ID= "<< L0_evtID << " Tell1= " << TELL1_number <<endmsg;
      m_evtList << "Event= " << m_evtNumber << " L0_ID= "<< L0_evtID << " Tell1= " << TELL1_number <<std::endl;	
      
      if(ExcludeTELL1(m_chosenTell1s,TELL1_number)) continue;
      
      //header_vector.clear();
      
      for(link=0; link<64; ++link){
	
	if((0 == ADC_all) || (0 == ADC) || (0 == headers_all) ){
	  
	  sprintf(histo_ADC_all, "tell1_%d_event_%lu_ADC_value", TELL1_number,m_evtNumber);
	  sprintf(histo_header, "tell1_%d_event_%lu_header", TELL1_number, m_evtNumber);
	  sprintf(histo_ADC, "tell1_%d_link_%d_event_%lu_ADC_value", TELL1_number,link,m_evtNumber);
	  sprintf(histo_Tell1s, "Headers for all Tell1s");
		  
	  ADC_all       = new TH2F(histo_ADC_all, histo_ADC_all, 2304, -0.5, 2303.5, 1024, -0.5, 1023.5);
	  ADC           = new TH2F(histo_ADC, histo_ADC, 36, -0.5, 35.5, 1024, -0.5, 1023.5);
	  headers_all   = new TH2F(histo_header, histo_header, 384, -0.5, 383.5, 1024, -0.5, 1023.5);
	  Tell1_headers = new TH2F(histo_Tell1s, histo_Tell1s, 106, -0.5, 105.5, 1024, -0.5, 1023.5);
	 		  
	}

	std::vector<int> linkHeaders =  getLink( (*headerOneTELL1)[link]);
	for(int iheader=0; iheader<4; iheader++){
	  int iH=(iheader+(36*link));
	  int ih=(iheader+(6*link));
	  if(m_LinkPlot==true){
	    ADC_all->Fill(iH, linkHeaders[iheader]);
	    headers_all->Fill(ih, linkHeaders[iheader]);
	    Tell1_headers->Fill(TELL1_number, linkHeaders[iheader]);

	    header_vector.push_back(linkHeaders[iheader]);
	    
	    //x[link]=linkHeaders[3];
	  
	      //plot2D(TELL1_number,linkHeaders[iheader],histo_ALLheaders,histo_ALLheaders,-0.5,105.5,-0.5,1023.5,106,1024);
	  }
	 
	  else{
	    if(ExcludeLink(m_chosenLinks,link)) continue;
	    
	    //ADC->Fill(iheader, linkHeaders[iheader]);
	   	  
	  }
	}
      }
    }
       
    
    //==============================================================================
    //============= Plotting the ADC counts for all channels =======================
    //==============================================================================
    
    VeloTELL1Datas::const_iterator sensIt;
    
    for(sensIt=m_tell1DataContainer->begin();sensIt!=m_tell1DataContainer->end();++sensIt){
      VeloTELL1Data* dataOneTELL1=(*sensIt);
      int TELL1_number=dataOneTELL1->key();
      if(ExcludeTELL1(m_chosenTell1s,TELL1_number)) continue;

      //Get sensor from detector element:  
      
      const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(TELL1_number);
      if(sensor == 0 ){
	error() << "Can't find a sensor associated with TELL1 " << TELL1_number << ". Aborting..." << endmsg;
	return StatusCode::FAILURE;
      }         
            
      //channel_vector.clear();

     	
      for(link=0; link<64; ++link){
	for(int index=0;index<32;index++){
	  
	  unsigned int channel=index+32*link;
	  unsigned int bin_1link=index+4;
	  unsigned int bin_alllinks=((bin_1link)+(36*link));
	  //unsigned int strip = sensor->ChipChannelToStrip(channel);
	  signed int value   =dataOneTELL1->channelADC(channel);

	  if(m_LinkPlot==true){
	    ADC_all->Fill(bin_alllinks, value);

	    channel_vector.push_back(value);

	    //y[link]= dataOneTELL1->channelADC(0);
	  }
	  else{
	    if(ExcludeLink(m_chosenLinks,link)) continue;
	    //ADC->Fill(bin_1link, value);
	  }
	}
	
      }
    }


    if ( 0 != ADC_all ) {
      EB_Histograms->WriteObject(ADC_all,ADC_all->GetName());
      EB_Histograms->WriteObject(ADC,ADC->GetName());
      EB_Histograms->WriteObject(headers_all,headers_all->GetName());
      EB_Histograms->WriteObject(Tell1_headers,Tell1_headers->GetName());

      delete ADC;
      delete ADC_all;
      delete headers_all;
      delete Tell1_headers;

    }
  } 

  t->Fill();

  info() << "tree filled for event= " << m_evtNumber << endmsg;

  L0ID_vector.clear();
  Tell1_vector.clear();
  header_vector.clear();
  channel_vector.clear();

  return StatusCode::SUCCESS;
  
}


// Declaration of the Algorithm Factory

DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, ADCTuple )



