#ifndef ARXTEST_H 
#define ARXTEST_H 1

// from Event model
#include "VeloEvent/VeloTELL1Data.h"

// local
#include "CommonFunctions.h"

// AIDA 
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"

// from TELL1 Kernel:
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "Tell1Kernel/VeloDecodeConf.h" 

// from ROOT
#include "TH1D.h"
#include "TF1.h"

// local
//#include "Event/VeloODINBank.h"

/**
 *  @class ArxTest
 *
 *  Algorithm to fill histograms from A-rx test data.
 *  
 *  @author Olaf Behrendt
 *  @author Kurt Rinnert
 *  @date   2007-06-12
 */

using namespace LHCb;
using namespace VeloTELL1;
namespace Velo
{
  namespace Monitoring
  {
    class ArxTest : public GaudiHistoAlg  {
      public: 
        /// Standard constructor
        ArxTest( const std::string& name, ISvcLocator* pSvcLocator );

        virtual ~ArxTest(); ///< Destructor

        virtual StatusCode initialize() override;    ///< Algorithm initialization
        virtual StatusCode execute   () override;    ///< Algorithm execution
        virtual StatusCode finalize  () override;    ///< Algorithm finalization

      private:

        StatusCode getData();

        void runArxTest();

        int sumAdcCountsLink(int iLink);
        int getActiveLink(int nLinks);
        void fitSine();

      private:

        bool m_plotArxTest;
        bool m_plotHistos;
        bool m_plotHistosDebug;  
        int m_globalPedestal;
        int m_thresholdActiveLink;
        int m_thresholdNoisyLink;
        std::vector<int> m_maskedLinks;
        int m_numberDebugLink;

        TH1D* m_hfit;
        TF1* m_sineFit;
        int m_lastActiveLink;
        int m_nEvtsDebug;
        std::vector < std::vector< int > > m_adcValuesInOneTELL1;
        std::vector < std::vector< int > > m_hdrValuesInOneTELL1;
        int m_tell1Number;
        std::vector<int> m_linkOrder;
        std::string m_adcDataLocation;
        std::string m_hdrDataLocation;

        const unsigned int m_nChannelsOnTell1;
        const unsigned int m_nChannelsOnFPGA;
        const unsigned int m_nHeadersOnTell1;
        const unsigned int m_nHeadersOnFPGA;

    };
  }
}
#endif // ARXTEST_H
