#include "GaudiKernel/AlgFactory.h" 
#include "GaudiAlg/GaudiHistoAlg.h"

#include "Event/RawEvent.h"
#include "Event/RawBank.h"

#include "GaudiUtils/Aida2ROOT.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "TH1D.h"
#include "TH2D.h"

#include <vector>
#include <string>

#include <string.h>
#include <stdio.h>


//-----------------------------------------------------------------------------
//  Class : Velo::Monitoring::HeaderMon
//
//  @author Kurt Rinnert <kurt.rinnert@cern.ch>
//  @date 02-05-2014 
//-----------------------------------------------------------------------------
using namespace std;

namespace Velo
{
  namespace Monitoring
  {
    class HeaderMon : public GaudiHistoAlg {
      public:

        HeaderMon( const string& name, ISvcLocator* pSvcLocator );

        virtual ~HeaderMon( );              ///< Destructor
        virtual StatusCode initialize() override;    ///< Algorithm initialization
        virtual StatusCode execute   () override;    ///< Algorithm execution
        virtual StatusCode finalize  () override;    ///< Algorithm finalization

        StatusCode  decodeHeaders();
        void  plotHeaders();
        void  plotFHS();

        void findThresholds( int sensor_idx, int link );

        void bookHistograms();

      private:

        int m_sensor_has_data[84];      ///< per event data presence

        int m_headers[84][64][4];       ///< per event data buffer
        int m_thresholds[84][64][2];    ///< header decoding thresholds, low/high

        TH2D *m_fhsHisto;
        vector< vector<TH1D*> > m_headerHistos;
        vector< vector<double*> > m_headerValues;
        vector< vector<unsigned long> > m_entries;

        string m_rawEventLocation;  
        string m_thresholdsFile;
        bool m_writeThresholdsFile;
        int m_minFHSHeader;
        int m_maxFHSHeader;
    };
  }
}


DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring,HeaderMon)

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::HeaderMon::HeaderMon( const string& name,
    ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
  , m_headerHistos(vector< vector<TH1D*> >(84, vector<TH1D*>(64)))
  , m_headerValues(vector< vector<double*> >(84, vector<double*>(64)))
  , m_entries(vector< vector<unsigned long> >(84, vector<unsigned long>(64,0)))
{
  declareProperty("RawEventLocation",m_rawEventLocation=LHCb::RawEventLocation::Default);
  declareProperty("ThresholdsFile",m_thresholdsFile="PseudoHeaderThresholds.txt");
  declareProperty("WriteThresholdsFile",m_writeThresholdsFile=false);
  declareProperty("MinFHSHeader",m_minFHSHeader=400,"Lower limit for FHS calculation.");
  declareProperty("MaxFHSHeader",m_maxFHSHeader=750,"Upper limit for FHS calculation.");
  setHistoTopDir( "Velo/" );
}

//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::HeaderMon::~HeaderMon() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::HeaderMon::initialize() {

  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;
  
  memset(m_headers,0,84*64*4*sizeof(int));
  memset(m_thresholds,0,84*64*2*sizeof(int));

  bookHistograms();

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::HeaderMon::finalize() {
  debug() << "==> Finalize" << endmsg;  

  for ( int sensor_idx=0; sensor_idx<84; ++sensor_idx ) {
    for ( int link=0; link <64; ++link ) { 
      m_headerHistos[sensor_idx][link]->SetEntries(m_entries[sensor_idx][link]);
      findThresholds(sensor_idx,link);
    }
  }

  plotFHS();

  if ( m_writeThresholdsFile ) {
    FILE* thresh_file = fopen(m_thresholdsFile.c_str(),"w");

    if ( 0 == thresh_file ) {
      error() << "Can not open file '" << m_thresholdsFile << "' for writing." << endmsg;
      return StatusCode::FAILURE;
    }

    for ( int sensor_idx=0; sensor_idx<84; ++sensor_idx ) {
      int sensor = 64*(sensor_idx/42) + sensor_idx%42;

      for ( int link=0; link<64; ++link ) {
        fprintf(thresh_file,"%3d\t%2d\t%3d\t%3d\n",sensor,link,m_thresholds[sensor_idx][link][0],m_thresholds[sensor_idx][link][1]);
      }
    }
    fclose(thresh_file);
  }


  return GaudiHistoAlg::finalize();  // must be called after all other actions
}

//=============================================================================
// Book histograms
//=============================================================================
void Velo::Monitoring::HeaderMon::bookHistograms() {

  char hist_name[256];
  char hist_title[256];

  for ( int sensor_idx=0; sensor_idx<84; ++sensor_idx ) {

    int sensor = 64*(sensor_idx/42) + sensor_idx%42;
    
    for ( int link=0; link <64; ++link ) {
    
      sprintf(hist_name,"Header_ADC_Sensor_%03d_Link_%02d",sensor,link);
      sprintf(hist_title,"Header ADC Sensor %03d Link %02d",sensor,link);

      m_headerHistos[sensor_idx][link] = Gaudi::Utils::Aida2ROOT::aida2root(book1D(hist_name,hist_title,-0.5,1023.5,1024));
      m_headerHistos[sensor_idx][link]->SetXTitle("Header ADC"); 
      m_headerValues[sensor_idx][link] = m_headerHistos[sensor_idx][link]->GetArray();
    
    }
  } 

  m_fhsHisto = Gaudi::Utils::Aida2ROOT::aida2root(book2D("FHS","Full Header Swing",-0.5,105.5,106,-0.5,63.5,64));
  m_fhsHisto->SetXTitle("Sensor");
  m_fhsHisto->SetYTitle("Link");
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::HeaderMon::execute() {

  debug() << "==> Execute" << endmsg;  
  
  if ( decodeHeaders() ) { plotHeaders(); } 
  
  return StatusCode::SUCCESS;
}

//==============================================================================================================
// Fast header decoding
//==============================================================================================================
StatusCode Velo::Monitoring::HeaderMon::decodeHeaders() {

  /*
   * The NZS raw bank is organized in 4 blocks (one per PPFPGA) and three sections
   * in each block. The sections are parallel streams of 3 32 bit words:
   *
   *  - the first word encodes three links, each link as ten bits starting from the LSB
   *  - the second encodes three links, each link as ten bits starting from the LSB
   *  - the third encodes two links, each link as ten bits starting from the LSB
   *
   * The first 36 words in a section encode even links; the next 36 words encode odd links.
   *
   * Each block ends 8 32 bit words containing the event info.
   *
   * This gives a total block size of 2*3*36 + 8 u_int32.
   *
   * For more details see EDMS 692431 V 2.0.
   *
   */

  // reset per event data presence flags
  memset(m_sensor_has_data ,0,sizeof(int)*84);

  // check for raw event, get it and fetch the raw VELO banks.

  if ( !exist<LHCb::RawEvent>(m_rawEventLocation) ) {
    error() << "No RawEvent at " <<  m_rawEventLocation << "." << endmsg;
    return StatusCode::FAILURE;
  }

  LHCb::RawEvent* raw_event = get<LHCb::RawEvent>(m_rawEventLocation);

  const vector<LHCb::RawBank*>&  raw_banks = raw_event->banks(LHCb::RawBank::VeloFull);

  if ( raw_banks.empty() ) return StatusCode::FAILURE;

  // we have some raw data, decode it.
  for ( vector<LHCb::RawBank*>::const_iterator bi = raw_banks.begin(); bi != raw_banks.end(); ++bi ) {

    const LHCb::RawBank* raw_bank = *bi;

    if ( LHCb::RawBank::MagicPattern != raw_bank->magic() ) { continue; } // this bank is corrupted

    int sensor = raw_bank->sourceID();
    int sensor_idx = 42*(sensor/64) + sensor%64;
    m_sensor_has_data[sensor_idx] = 1;

    const unsigned int *bank_begin = raw_bank->data();
    int block_size = 2*3*36 + 8; // see EDMS 692431 V 2.0

    for ( int hw_block = 0; hw_block < 4; ++hw_block ) { 
      
      for ( int odd=0; odd<2; ++odd ) { // even numbered links come first

        const unsigned int *bank = bank_begin + hw_block*block_size + odd*3*36;

        for ( int bit=0; bit<4; ++bit ) { 

          int bank_idx = bit*3; 

          // chip 0 + block*4, link [01], link [23]; chip 1 + block*4, link [01]:

          int link = (3 - hw_block)*16 + odd; // reverse blocks to correct for cable order
          unsigned int word = bank[bank_idx++];
          m_headers[sensor_idx][link][bit] = word & 0x000003FFU;

          link += 2;
          word = word >> 10;
          m_headers[sensor_idx][link][bit] =  word & 0x000003FFU;

          link += 2;
          word = word >> 10;
          m_headers[sensor_idx][link][bit] =  word & 0x000003FFU;


          // chip 1 + block*4, link [23]; chip 2 + block*4, link [01], link[23]:

          link += 2;
          word = bank[bank_idx++];
          m_headers[sensor_idx][link][bit] = word & 0x000003FFU;

          link += 2;
          word = word >> 10;
          m_headers[sensor_idx][link][bit] =  word & 0x000003FFU;

          link += 2;
          word = word >> 10;
          m_headers[sensor_idx][link][bit] =  word & 0x000003FFU;


          // chip 3 + block*4, link [01], link [23]:

          link += 2;
          word = bank[bank_idx++];
          m_headers[sensor_idx][link][bit] = word & 0x000003FFU;

          link += 2;
          word = word >> 10;
          m_headers[sensor_idx][link][bit] =  word & 0x000003FFU;

        } // bit loop
      } // odd/even link loop
    } // PPFPGA block loop
  } // bank loop

  return StatusCode::SUCCESS;
}

//==============================================================================================================
// Fill header histograms
//==============================================================================================================
void Velo::Monitoring::HeaderMon::plotHeaders(){

  for ( int sensor_idx=0; sensor_idx<84; ++sensor_idx ) {  

    if ( 0 == m_sensor_has_data[sensor_idx] ) { continue; } // no data on this sensor in this event

    for(unsigned int link=0; link<64; ++link){
      double* values = m_headerValues[sensor_idx][link];
      unsigned long& entries = m_entries[sensor_idx][link];

      for( unsigned int header_bit=0; header_bit<4; ++header_bit ) {
        ++values[m_headers[sensor_idx][link][header_bit]+1];
        ++entries;
      }
    }
  }

  return;
}

//==============================================================================================================
// Fill FHS histogram
//==============================================================================================================
void Velo::Monitoring::HeaderMon::plotFHS(){

  for ( int sensor_idx=0; sensor_idx<84; ++sensor_idx ) {
 
    int sensor = 64*(sensor_idx/42) + sensor_idx%42;

    for ( int link=0; link<64; ++link ) {

      double avg_low=0.0, n_low=0.0, avg_high=0.0, n_high=0.0;
      double *headers = m_headerValues[sensor_idx][link];

      int low_limit = 4*m_thresholds[sensor_idx][link][0] + 1;
      for ( int l=m_minFHSHeader+1; l<low_limit; ++l ) {
        avg_low += l * headers[l];
        n_low += headers[l];
      }
      avg_low /= n_low;

      int high_limit = 4*m_thresholds[sensor_idx][link][1] + 1;
      for ( int h=high_limit+1; h<m_maxFHSHeader+1; ++h ) {
        avg_high += h * headers[h];
        n_high += headers[h];
      }
      avg_high /= n_high;

      m_fhsHisto->SetBinContent(sensor+1, link+1, avg_high-avg_low );
    }
  }

  return;
}

//==============================================================================================================
// Find gap and determine high and low pseudo header thresholds
//==============================================================================================================
void Velo::Monitoring::HeaderMon::findThresholds( int sensor_idx, int link ) 
{
  int gapmin = -1;
  int gapmax = -1;
  int gaplen = 0;
  int maxgaplen = 0;
  int startbin = -1; 
  int stopbin = -1;

  int low, high;

  double *headers  = m_headerValues[sensor_idx][link];

  // find last bin with a non-zero value
  for(int i=1024; i>=0 && stopbin < 0; --i){
    if(headers[i] > 0){
      stopbin = i;
    }
  }

  for(int i=1; i<stopbin; ++i){

    // find first non-zero bin
    // and start counting gap size
    if(headers[i] > 0){
      startbin = i;
      gaplen = 0;
    } else {
      if(startbin > -1){
        gaplen++;
      }
    }

    // reasonable gap? largest gap?  save.
    if( gaplen > maxgaplen && i-gaplen > 400 && i < 624 ){
      maxgaplen = gaplen;
      gapmax = i;
      gapmin = i-gaplen;
    }
  }

  // we are off by one because of the underflow bin
  --gapmin;
  --gapmax;

  // pseudo thresholds are the limits divided by four
  int tmp_low  = gapmin/4;
  int tmp_high = gapmax/4;
  int full_gap = tmp_high - tmp_low;

  if ( full_gap < 4 ) {
    low  = tmp_low;
    high = tmp_high;
  } else { 
    // narrow the gap a bit, but not below 4
    int margin = ( full_gap < 12 ? (full_gap-4)/2 : full_gap/3 );
    low  = tmp_low  + margin;
    high = tmp_high - margin;
  }

  m_thresholds[sensor_idx][link][0] = low;
  m_thresholds[sensor_idx][link][1] = high;

  return;
}

DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, HeaderMon )



