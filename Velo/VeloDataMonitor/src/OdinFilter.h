// $Id: OdinFilter.h,v 1.1 2009-07-15 17:58:45 kakiba Exp $
#ifndef VELODATAMONITOR_PULSESHAPEFITTERTED 
#define VELODATAMONITOR_PULSESHAPEFITTERTED 1

// Include files
// -------------
#include "VeloDet/DeVelo.h"

// from VeloEvent
#include "VeloEvent/VeloTELL1Data.h"

// from DigiEvent
#include "Event/VeloCluster.h"

// local
#include "GaudiAlg/GaudiAlgorithm.h"


#include "Math/ParamFunctor.h"
#include "GaudiKernel/IEventTimeDecoder.h"            // Interface for odin

#include "Event/ODIN.h"

/** @class OdinFilter OdinFilter.h
 *  
 *
 *  @author Kazu  
 *  @date   2008-10-22
 */

namespace Velo {



  class OdinFilter : public GaudiAlgorithm{
  public: 
    /// Standard constructor
    OdinFilter( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~OdinFilter( );    ///< Destructor
    virtual StatusCode initialize() override;    ///< Algorithm initialization
    virtual StatusCode execute   () override;    ///< Algorithm execution
    virtual StatusCode finalize  () override;    ///< Algorithm finalization
    long          m_nEvents;
    double        m_threshold;
    double m_cmsCut;
    double m_integralCut;
    double m_timeWindow;
    double m_expectedPeakTime;
    double m_nTimeBins;
    bool  m_runWithOdin; // 
    bool  m_fixedOffset;
    bool  m_tryCluster;
    bool  m_plotSeparateStrips;
    bool m_includePileUp;
    bool m_skipStepZero;
    double m_amplitudeCut;
    double m_offsetCut;
    long m_eventNumber;
    bool  m_useLogLikeFit;
    int           m_nSteps;
    int           m_shift;
    int           m_stepSize;
    int m_prevRunNumber;
    int  m_eventsPerFile;
    int  m_eventsThisFile;
    int m_takeNeighbours;
    int m_wrapPoint;
    int  m_wrappedSteps;
    int m_currentCalStep;
    std::vector<int> m_runNumbers;
    std::vector<double> m_runNumbersDelay;
    std::vector<int> m_chosenBunches;
    std::vector<int> m_skipSteps;
    bool m_useBCNT;
    std::map<int, double> m_runToDelay;
    int  m_nEventsPerStep;
    int  m_numberOfCMSIterations;
    int  m_analyseOnlyStepNumber;

    std::map<int, int> m_eventPerStepCounter;

    double        m_thresholdForCMS;

  protected:
  private:
    IEventTimeDecoder* m_timeDecoder; ///< Pointer to tool to decode ODIN bank
    // Retrieve the VeloTell1Data
    // Monitor the VeloTell1Data
    //void MakePulseShape( std::string samplingLocation, LHCb::VeloTELL1Datas* tell1Datas );
    // Data members
    const DeVelo* m_velo;
    // Job options
    std::vector<std::string> m_samplingLocations;
    std::string m_dataLocation;
  };
}
#endif // VELODATAMONITOR_PULSESHAPEFITTERTED
