// $Id: $

#include <cctype>
#include <cassert>
#include <cstdlib>
#include <cstdarg>
#include <cstring>
#include <cstdio>
#include <ios>
#include <string>

#include <exception>

#include <algorithm>

#include <boost/foreach.hpp>
#include <boost/scoped_array.hpp>

#include "Event/ODIN.h"

#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/IEventTimeDecoder.h"

#include "VeloEvent/EvtInfo.h"

#include "GaudiUtils/Aida2ROOT.h"

#include "SEUMonitor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Velo::SEUMonitor
//
// 2012-01-26 : Manuel Tobias Schiller <manuel.schiller@nikhef.nl>
//-----------------------------------------------------------------------------

namespace Velo {
DECLARE_ALGORITHM_FACTORY( SEUMonitor )
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::SEUMonitor::SEUMonitor(const std::string& name, ISvcLocator* pSvcLocator) :
    Velo::VeloMonitorBase(name, pSvcLocator),
    m_odinDecoder(0), m_evtime(0u), m_evtnr(0u), m_runnr(0u), m_evtctr(0u),
    m_hseuctr2d(0), m_hseuctr1d(0), m_hdesyncctr2d(0), m_hdesyncctr1d(0),
    m_hevstat(0)
{
    // where to get the EvtInfos from the TES
    declareProperty("EventInfoLocation", m_eventInfoLocation = EvtInfoLocation::Default);
    // we can dump the EvtInfos for debugging purposes
    declareProperty("Dump2Info", m_dump2Info = false);
    // set to true in online monitoring, otherwise, keep false to save some
    // time
    declareProperty("MonitoringMode", m_monitoringMode = true);
}

//=============================================================================
// Destructor
//=============================================================================
Velo::SEUMonitor::~SEUMonitor() {;}

//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::SEUMonitor::initialize()
{
    StatusCode sc = Velo::VeloMonitorBase::initialize(); // must be executed first
    if (sc.isFailure()) return sc;  // error printed already by VeloMonitorBase

    debug() << "==> Initialize" << endmsg;

    m_odinDecoder = tool<IEventTimeDecoder>("OdinTimeDecoder");
    {
	// initialise single event upset counters
	std::vector<SEUCtr> lctr(16u * 132u, SEUCtr());
	m_seuctrs.swap(lctr);
    }
    m_evtime = 0u;
    m_evtnr = 0u;
    m_runnr = 0u;
    m_evtctr = 0u;
    // book histograms
    m_hseuctr2d = Gaudi::Utils::Aida2ROOT::aida2root(book2D("seuctr2d",
		"number of single event upsets;sensor;Beetle number",
		-0.5, 131.5, 132, -0.5, 15.5, 16));
    m_hseuctr1d = Gaudi::Utils::Aida2ROOT::aida2root(book1D("seuctr1d",
		"number of single event upsets;sensor;number of SEUs",
		-0.5, 131.5, 132));
    m_hdesyncctr2d = Gaudi::Utils::Aida2ROOT::aida2root(book2D("desyncctr2d",
		"number of desyncs in single event upset counter;sensor;Beetle number",
		-0.5, 131.5, 132, -0.5, 15.5, 16));
    m_hdesyncctr1d = Gaudi::Utils::Aida2ROOT::aida2root(book1D("desyncctr1d",
		"number of desyncs in single event upset counter;sensor;# SEU counter desyncs",
		-0.5, 131.5, 132));
    m_hevstat = Gaudi::Utils::Aida2ROOT::aida2root(book1D("eventstat",
		"event statistics;event type;number of events",
		-0.5, 4.5, 5));
    // set labels for bins
    m_hevstat->GetXaxis()->SetBinLabel(1 + NoEvtInfo,     "NoEvtInfo");
    m_hevstat->GetXaxis()->SetBinLabel(1 + RunChange,     "RunChange");
    m_hevstat->GetXaxis()->SetBinLabel(1 + LargeGap,      "LargeGap");
    m_hevstat->GetXaxis()->SetBinLabel(1 + TimeBackwards, "TimeBackwards");
    m_hevstat->GetXaxis()->SetBinLabel(1 + OK, 	          "OK");

    return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::SEUMonitor::execute()
{
    debug() << "==> Execute" << endmsg;

    // ignore events without event info banks
    if (!exist<EvtInfos>(m_eventInfoLocation)) {
	m_hevstat->Fill(NoEvtInfo);
	return StatusCode::SUCCESS;
    }
    const EvtInfos* einfos = get<EvtInfos>(m_eventInfoLocation);
    if (einfos->empty()) {
	m_hevstat->Fill(NoEvtInfo);
	return StatusCode::SUCCESS;
    }
    // count events with NZS EvtInfos
    ++m_evtctr;
    // trigger odin decoding
    m_odinDecoder->getTime();
    // get odin
    const LHCb::ODIN* odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
    const unsigned long long evtime = odin->gpsTime(); // us
    const unsigned long long minute = 60000000ull; // us
    const unsigned runnr = odin->runNumber();
    const unsigned long long evtnr = odin->eventNumber(); // us
    // figure out if we need to reset SEU counters because of a gap/run change
    // in the data
    bool resetSEUCtrs = false;
    if (runnr != m_runnr) {
	// run change, get fresh value for SEU counters
	m_hevstat->Fill(RunChange);
	resetSEUCtrs = true;
    } else {
	if (static_cast<unsigned long long>(
		    std::abs(static_cast<long long>(m_evtime) -
			static_cast<long long>(evtime))) >= minute) {
	    // large gap in time between events (potentially files read in
	    // wrong order), get fresh value for SEU counters
	    m_hevstat->Fill(LargeGap);
	    resetSEUCtrs = true;
	} else {
	    if (m_evtime >= evtime) {
		// time is going backwards (events not written in order they
		// were taken) - ignore event
		m_hevstat->Fill(TimeBackwards);
		return StatusCode::SUCCESS;
	    } else {
		// time moving forward, all ok
		m_hevstat->Fill(OK);
	    }
	}
    }
    if (resetSEUCtrs) {
	// ok, we have a longer discontinuity due to file or run change, so
	// reinitialise the SEU counters from scratch
	BOOST_FOREACH(SEUCtr& ctr, m_seuctrs) {
	    ctr.setInitialised(false);
	}
    }
    m_runnr = runnr;
    m_evtnr = evtnr;
    m_evtime = evtime;
    // dump event info banks to info (if we're supposed to do that)
    if (m_dump2Info) dump2Info(einfos);
    // update SEU counters
    std::vector<unsigned> ihdr;
    std::vector<unsigned> updateHistos;
    BOOST_FOREACH(const EvtInfo* einfo, *einfos) {
	const unsigned sensor = einfo->key();
	for (unsigned ppfpga = 0; ppfpga < 4; ++ppfpga) {
	    ihdr = einfo->IHeader(ppfpga);
	    for (unsigned beetle = 0; beetle < ihdr.size(); ++beetle) {
		unsigned idx = index(sensor, ppfpga, beetle);
		SEUCtr& ctr = m_seuctrs[idx];
		// update the SEU counter with the new value, remembering to
		// reverse the bits
		if ((ctr += (ihdr[beetle] >> 6))) {
		    // counter changed state
		    if (ctr.willOverflow() || m_monitoringMode) {
			// set flag to copy state of of tiny counters to full
			// blown histograms
			updateHistos.push_back(idx);
		    }
		} // SEU/desync counter changed
	    } // beetle
	} // ppfpga
    } // sensor
    if (!updateHistos.empty())
	processCarryToHisto(updateHistos);

    return StatusCode::SUCCESS;
}

//=============================================================================
// Finalize
//=============================================================================
StatusCode Velo::SEUMonitor::finalize()
{
    debug() << "==> Finalize" << endmsg;

    {
	// forcibly process carry in all SEUCtrs
	std::vector<unsigned> indices;
	for (unsigned i = 0; i < m_seuctrs.size(); ++i)
	    indices.push_back(i);
	processCarryToHisto(indices);
    }

    {
	// free counters
	std::vector<SEUCtr> lctr;
	m_seuctrs.swap(lctr);
    }

    return Velo::VeloMonitorBase::finalize();  // must be called after all other actions
}

void Velo::SEUMonitor::processCarryToHisto(
	const std::vector<unsigned>& indices)
{
    const double nentries = m_hseuctr2d->GetEntries() + double(m_evtctr);
    m_evtctr = 0u;
    // get values left in counters
    BOOST_FOREACH(unsigned index, indices) {
	unsigned sensor = index / 16, beetle = index % 16;
	SEUCtr& ctr = m_seuctrs[index];
	// skip empty counters
	if (!ctr) continue;
	// carry counter over to histograms
	const unsigned int ibin2d =
	    m_hseuctr2d->FindBin(sensor, beetle);
	const unsigned int ibin1d =
	    m_hseuctr1d->FindBin(sensor);
	m_hseuctr2d->SetBinContent(ibin2d,
		m_hseuctr2d->GetBinContent(ibin2d) +
		double(ctr.nSEU()));
	m_hseuctr1d->SetBinContent(ibin1d,
		m_hseuctr1d->GetBinContent(ibin1d) +
		double(ctr.nSEU()));
	m_hdesyncctr2d->SetBinContent(ibin2d,
		m_hdesyncctr2d->GetBinContent(ibin2d) +
		double(ctr.nDesyncs()));
	m_hdesyncctr1d->SetBinContent(ibin1d,
		m_hdesyncctr1d->GetBinContent(ibin1d) +
		double(ctr.nDesyncs()));
	ctr.zeroCounters();
    }
    m_hseuctr2d->SetEntries(nentries);
    m_hseuctr1d->SetEntries(nentries);
    m_hdesyncctr2d->SetEntries(nentries);
    m_hdesyncctr1d->SetEntries(nentries);
}

/// formatting tools (boost::format does not agree with me for POD)
namespace fmt {
    /// safe sprintf variant returning a string
    /** usage example:
      * std::cout << fmt::fmt("%5d %5x %5o", 42, 42, 42) << std::endl;
      */
    std::string fmt(const char *fmt, ...)
    {
	// exception to throw when vsnprintf fails
	class _vsnprintfException
	{ const char* what() const throw() { return "Error during vsnprintf."; } };

	assert(0 != fmt);
	boost::scoped_array<char> str;
	std::size_t sz = 0;
	int retVal = 2 * std::strlen(fmt);

	do {
	    str.reset(new char[sz = 1 + retVal]);
	    va_list ap;
	    va_start(ap, fmt);
	    retVal = std::vsnprintf(str.get(), sz, fmt, ap);
	    va_end(ap);
	    if (0 > retVal) throw _vsnprintfException();
	} while (std::size_t(retVal) >= sz);

	return std::string(str.get());
    }

    /// define type to be returned by setfmt io manipulator defined below
    struct _SetFmt {
        /// constructor for _SetFmt
        inline _SetFmt(const char* f) : fmt(f) { }
        const char* fmt; ///< member to hold the format string
    };

    /// setfmt io manipulator - takes printf-style string to set ostream flags
    /** usage example:
      * std::cout << fmt::setfmt("%3.4f") << M_PI << std::endl;
      */
    inline _SetFmt setfmt(const char* fmt) { return _SetFmt(fmt); }

    /// setfmt io manipulator - sets ostream flags from printf conversion specifier
    /** usage example:
      * fmt::setfmt(std::cout, "%3.4f");
      * std::cout << M_PI << std::endl;
      *
      * note that there may be slight differences to what you're used from
      * printf and friends
      *
      * the routine understands the following format specifiers (check with
      * some documentation of printf for the meaning):
      * - a '%'
      * - an additional '%' to print a literal '%'
      * - optional: one of the flag characters [+-0# ]
      * - optional: a width (positive interger)
      * - optional: a dot ('.') followed by a precision (positive integer)
      * - one of the following conversion specifiers [cdefgiousxEFGX]
      * 
      * for convenience, anything besides conversion specifiers is printed
      * in verbatim.
      *
      * do not use more than one conversion specifier.
      *
      * length specifiers like in "%ld" do not make sense here and are not
      * supported.
      */
    template<class OS> OS& setfmt(OS& str, const char* fmt)
    {
        assert(0 != fmt);
        while (*fmt) {
            // print anything that is no conversion specifier
            if ('%' != *fmt) { str << *fmt++; continue; }
            // handle "%%" to print literal "%"
            if ('%' == *++fmt) { str << *fmt++; continue; }
            // ok, try to parse conversion specifier, and set up defaults
            const char* fmtstart = fmt;
            std::ios::fmtflags flags = std::ios::fmtflags(0);
            int width = 0, prec = 6;
            bool alt = false;
            char fill = ' ';
            // consume leading [+-0# ]*
            for (bool more = true; more; ++fmt) {
                switch (*fmt) {  
                    case '+': flags |= std::ios::showpos; break;
                    case '-': flags |= std::ios::left; break;
                    case '0': flags |= std::ios::internal; fill = '0'; break;
                    case '#': alt = true; break;
                    case ' ': break;
                    default: more = false; break;
                }
            }
            // consume a %d or a %d.%d specifier
            if (isdigit(*fmt)) { width = atoi(fmt); while (isdigit(*++fmt)) {}; }
            if ('.' == *fmt) { prec = atoi(++fmt); while (isdigit(*fmt)) ++fmt; }
            // distinguish between %e and %E etc
            if (isupper(*fmt)) {
                switch (*fmt) {
                    case 'C': case 'D': case 'I': case 'O': case 'S':
                        goto notok;
                    default: flags |= std::ios::uppercase; break;
                }
            }
            // set output format (decimal/hex/scientific etc.)
            switch (tolower(*fmt)) {  
                case 'd': case 'i': flags |= std::ios::dec; break;
                case 'e': flags |= std::ios::scientific; break;
                case 'f': flags |= std::ios::fixed; break;
                case 'c': case 'g': case 's': break;
                case 'o': flags |= std::ios::oct; break;
                case 'x': flags |= std::ios::hex; break;
                default: goto notok;
            }
            if (alt) { // handle alternate form
                switch (tolower(*fmt)) {
                    case 'x': case 'o': flags |= std::ios::showbase; break;
                    case 'e': case 'f': case 'g':
                        flags |= std::ios::showpoint; break;
                    default: break;
                }
            }
            // only one conversion specifier per setfmt invocation, take
            // the last, print the earlier ones
            if (0 != *++fmt) goto notok;
            str.unsetf(std::ios::adjustfield | std::ios::basefield |
                    std::ios::floatfield);
            str.setf(flags);
            str.width(width);
            str.precision(prec);
            str.fill(fill);
            break;
notok:
            fmt = fmtstart;
        }
        return str;
    }

    /// operator<< to make output streams work with setfmt io manipulator
    template<class OS> inline OS& operator<<(OS& ostr, const _SetFmt& fmt)
    { return setfmt(ostr, fmt.fmt); }    
}

void Velo::SEUMonitor::dump2Info(const EvtInfos* einfos) const
{
    // process available event info banks
    BOOST_FOREACH(const EvtInfo* einfo, *einfos) {
	info() << fmt::fmt("Dumping information for TELL1 %3u (EvtInfo bank %p)",
		einfo->key(), einfos) << endmsg;
	for (unsigned ppfpga = 0; ppfpga < 4; ++ppfpga) {
	    info() << fmt::fmt("    event info PPFPGA %u dump:", ppfpga) << endmsg;
	    info() << fmt::fmt("        %-28s %5u",
		    "eventInformation", einfo->eventInformation(ppfpga)) << endmsg;
	    info() << fmt::fmt("        %-28s 0x%02x",
		    "bankList", einfo->bankList(ppfpga)) << endmsg;
	    info() << fmt::fmt("        %-28s 0x%01x",
		    "detectorID", einfo->detectorID(ppfpga)) << endmsg;
	    info() << fmt::fmt("        %-28s %4u",
		    "bunchCounter", einfo->bunchCounter(ppfpga)) << endmsg;
	    info() << fmt::fmt("        %-28s 0x%08x",
		    "l0EventID", einfo->l0EventID(ppfpga)) << endmsg;
	    info() << fmt::fmt("        %-28s 0x%02x",
		    "processInfo", einfo->processInfo(ppfpga)) << endmsg;
	    info() << fmt::fmt("        %-28s 0x%02x",
		    "FEMPCN", einfo->FEMPCN(ppfpga)) << endmsg;
	    info() << fmt::fmt("        %-28s %u",
		    "chipAddress", einfo->chipAddress(ppfpga)) << endmsg;
	    info() << fmt::fmt("        %-28s 0x%01x",
		    "channelError", einfo->channelError(ppfpga)) << endmsg;
	    info() << fmt::fmt("        %-28s 0x%04x",
		    "adcFIFOError", einfo->adcFIFOError(ppfpga)) << endmsg;
	    info() << fmt::fmt("        %-28s 0x%04x",
		    "headerPseudoErrorFlag", einfo->headerPseudoErrorFlag(ppfpga)) << endmsg;
	    info() << fmt::fmt("        %-28s 0x%01x",
		    "PCNError", einfo->PCNError(ppfpga)) << endmsg;
	    info() << fmt::fmt("        %-28s 0x%02x",
		    "FEMPCNG", einfo->FEMPCNG(ppfpga)) << endmsg;
	    dumpVectUI("        ", "IHeader", einfo->IHeader(ppfpga));
	    dumpVectUI("        ", "PCNBeetle", einfo->PCNBeetle(ppfpga));
	    info() << fmt::fmt("        %-28s %u",
		    "checker", einfo->checker(ppfpga)) << endmsg;
	}
    }
}

void Velo::SEUMonitor::dumpVectUI(const std::string& pfx,
	const std::string& name,
	const std::vector<unsigned>& v) const
{
    info() << fmt::fmt("%s%s = [", pfx.c_str(), name.c_str()) << endmsg;
    unsigned i = 0;
    BOOST_FOREACH(unsigned val, v) {
	if (0 == (i & 7))
	    info() << pfx << "    ";
	else if (7 > (i & 7))
	    info() << " ";
	info() << fmt::fmt("0x%02x", val);
	if (7 == (i & 7))
	    info() << endmsg;
	++i;
    }
    if (7 != (i & 7) && 0 != (i & 7))
	info() << endmsg;
    info() << pfx << "    ]" << endmsg;
}

// vim: sw=4:tw=78:ft=cpp
