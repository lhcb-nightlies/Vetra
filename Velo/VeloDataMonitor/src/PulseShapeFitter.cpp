// $Id: PulseShapeFitter.cpp,v 1.3 2010-03-18 14:22:35 kakiba Exp $
// Include files
// -------------

#include <iostream>
#include <fstream>

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// from Tell1Kernel
#include "Tell1Kernel/VeloDecodeConf.h" 

// local
#include "PulseShapeFitter.h"
#include "CommonFunctions.h"

// from Boost
#include <boost/assign/list_of.hpp>
#include "boost/lexical_cast.hpp"

// from Gaudi
#include "AIDA/IHistogram2D.h"
// local
// Kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"


using namespace VeloTELL1;

//-----------------------------------------------------------------------------
// Implementation file for class : PulseShapeFitter
//
// 2008-08-20 : Kazu Akiba
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
namespace Velo {
  DECLARE_ALGORITHM_FACTORY( PulseShapeFitter )
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  Velo::PulseShapeFitter::PulseShapeFitter( const std::string& name, ISvcLocator* pSvcLocator)
  : Velo::VeloMonitorBase ( name , pSvcLocator )
,m_nEvents(0)
{
  std::vector<std::string> tmpLocations = 
	boost::assign::list_of("Prev7")("Prev6")("Prev5")("Prev4")("Prev3")("Prev2")("Prev1")
	("")("Next1")("Next2")("Next3")("Next4")("Next5")("Next6")("Next7");
  declareProperty( "SamplingLocations", m_samplingLocations = tmpLocations );
  declareProperty( "NSteps", m_nSteps= 25 );
  declareProperty( "ChisqCut", m_chisqCut= 100 );
  declareProperty( "StepSize", m_stepSize= 100 );
  declareProperty( "TELL1s",m_chosenTell1s     );
  declareProperty( "SignalThreshold" , m_threshold=20);
  declareProperty( "TakeNeighbours", m_takeNeighbours=1 );
  declareProperty( "Offset" ,m_offset=0);
  declareProperty( "WidthBefore", m_width1=13.68);
  declareProperty( "WidthAfter", m_width2=26.76);
  declareProperty( "DetailedOutput" , m_detailedOutput=true);
}

//=============================================================================
// Destructor
//=============================================================================
Velo::PulseShapeFitter::~PulseShapeFitter() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::PulseShapeFitter::initialize() {
  StatusCode sc = VeloMonitorBase::initialize(); // must be executed first
  printProps();
  if(m_nSteps==1) m_stepSize=1000000000; 


  if ( sc.isFailure() ) return sc;
  m_velo = getDet<DeVelo>( DeVeloLocation::Default );  

  const int strips=2048;
  int strip_tmp, channel_tmp;
  std::ifstream ifsR,ifsP;
  char dummy[100];
  ifsR.open("rccChannel-mapping.twocol");

  ifsR.getline(dummy,100);
  for(int chan = 0; chan<strips; chan++){
	ifsR>>strip_tmp>>channel_tmp;
	stripFromChannel[0][channel_tmp]=strip_tmp;  
	channelFromStrip[0][strip_tmp]=channel_tmp;                          

  }
  ifsR.close();

  ifsP.open("pccChannel-mapping.twocol");
  for(int chan = 0; chan<strips; chan++){
	ifsP>>strip_tmp>>channel_tmp;
	stripFromChannel[1][channel_tmp]=strip_tmp;  
	channelFromStrip[1][strip_tmp]=channel_tmp;  
  }
  ifsP.close();

  return StatusCode::SUCCESS;
}

//=============================================================================
/// Main execution
//=============================================================================
StatusCode Velo::PulseShapeFitter::execute() {
  counter( "# events" ) += 1;
  m_nEvents++;
  //info() << "running here"  << endmsg;
  // Loop over the sampling locations and produce the monitoring plots
  // -----------------------------------------------------------------
  //int iphase = (int)(m_nEvents/m_stepSize);
  //double binsize =  (25./float(m_nSteps));
  //double realPhase = float(iphase)*(binsize)+0.000001; 
  int RorPhi;

  std::vector<std::string>::const_iterator itL;

  //check middle sample for the tell1Data:
  const int midSampleIndex = floor(m_samplingLocations.size()/2.);
  const int nSamples = m_samplingLocations.size();
  for(int isample = 0; isample < nSamples ; isample++){
	LHCb::VeloTELL1Datas *SampleData = veloTell1Data(m_samplingLocations[isample]);
	if( SampleData == 0 ) return StatusCode::SUCCESS;
  }


  LHCb::VeloTELL1Datas *midSampleData = veloTell1Data(m_samplingLocations[midSampleIndex]);
  if( midSampleData == 0 ) return StatusCode::SUCCESS; 
  LHCb::VeloTELL1Datas::const_iterator itD;
  //int currentTell1 = -999;
  
#ifndef WIN32
  pulse = new TH1D("pulse","pulse",nSamples,-(nSamples/2+0.5)*25.,(nSamples/2+0.5)*25.);
#endif
  
  double (*fittest)(double *, double *) = NULL;
  fittest = &Velo::fitfsimple;
  func = new TF1("dtfitfunc",fittest,-200.,200.,5);
  func->SetParameters(0,20,-10,m_width1,m_width2);
  func->FixParameter(0,0);
  func->SetParLimits(3,m_width1,m_width1);
  func->SetParLimits(4,m_width2,m_width2);
  char fitvalues_offset[150]; 
  char fitvalues_ampltiude[150]; 
  char fitvalues_peak[150];
  char finepulseshape[150];
  char pulseshape[150];
  int finebin = m_nEvents/m_stepSize;

#ifndef WIN32
  for ( itD = midSampleData -> begin(); itD != midSampleData -> end(); ++itD ) {
	debug() << " Data not found " << endmsg;
	LHCb::VeloTELL1Data* data = (*itD);  
	std::vector<LHCb::VeloTELL1Data* > allSamplesTell1Data;
	allSamplesTell1Data.resize(nSamples); 
	int TELL1_number=data->key();
	if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue;
	const DeVeloSensor* sens=0;
	sens=m_veloDet->sensorByTell1Id(TELL1_number);
	unsigned int sensNumber=0;
	sensNumber=sens->sensorNumber();
	//currentTell1 = TELL1_number;
	// if tell1 is there, try to get the data for all the locations:
	int count =0;
	for ( itL = m_samplingLocations.begin(); itL != m_samplingLocations.end(); ++itL, count++ ) {
	  LHCb::VeloTELL1Datas* tell1Datas = veloTell1Data( (*itL) );  
	  allSamplesTell1Data[count] = tell1Datas->object(TELL1_number);
	}
	std::vector<bool> signalDetected(2048,false);
	for(int iSample = 0; iSample<nSamples; iSample++){
	  ALinkPair begEnd;
	  for(int aLink=0; aLink<NumberOfALinks; ++aLink){
		begEnd=(*allSamplesTell1Data[iSample])[aLink];
		scdatIt iT;
		int channel =0;
		for(iT=begEnd.first; iT!=begEnd.second; ++iT, ++channel){
		  //int adc = *iT;
		  int channelNumber = channel + 32*aLink;
		  char chnumber[100]; sprintf(chnumber, "%04d", channelNumber );
		  char t1number[100]; sprintf(t1number, "%03d_sens_%03d", TELL1_number, sensNumber);
		  std::string TPshapeTitle;
		  double cmsadc = subtractCommonMode(channelNumber,  allSamplesTell1Data[iSample]);
		  //if((fabs(cmsadc)>m_threshold)&&channelNumber<512) info()<<m_nEvents<<"  "<<channelNumber<<"  "<<signalDetected[channelNumber]<<endmsg;
		  if((fabs(cmsadc) > m_threshold)&&!signalDetected[channelNumber]){
			//if(channelNumber<512) info()<<m_nEvents<<"  "<<channelNumber<<"  We're in! "<<endmsg;
			if(TELL1_number<64) RorPhi=0;
			else RorPhi=1;

			sprintf(pulseshape,"pulse_Tell1%03d_ch%04d_timebin%02d_peak_sens%d",TELL1_number,channelNumber,finebin, sensNumber);


			int centralStrip=0 ,tmpSample=0;
			std::vector<double> maxadc(m_takeNeighbours,0.);
			double centralTime=0. ,centralADC=0. ,tmpADC=0.;

			for(int kStrip=0;kStrip<m_takeNeighbours*2+1;kStrip++){

			  for(int jSample =0; jSample<nSamples; jSample++){
				if((stripFromChannel[RorPhi][channelNumber]+kStrip-m_takeNeighbours)/128!=stripFromChannel[RorPhi][channelNumber]/128){
				  maxadc[kStrip]=0;
				  continue;
				}
				tmpADC=subtractCommonMode(channelFromStrip[RorPhi][stripFromChannel[RorPhi][channelNumber]+kStrip-m_takeNeighbours],  allSamplesTell1Data[jSample]);
				if(jSample==0||fabs(tmpADC)>fabs(maxadc[kStrip])){
				  maxadc[kStrip]=tmpADC;
				  tmpSample=jSample;
				}
			  }
			  if(kStrip==0||fabs(maxadc[kStrip])>fabs(centralADC)){
				centralADC=maxadc[kStrip];
				centralStrip=stripFromChannel[RorPhi][channelNumber]+kStrip-m_takeNeighbours;
				centralTime=25*(tmpSample - (int) nSamples/2);	
			  }
			}


			for(int kStrip=0;kStrip<m_takeNeighbours*2+1;kStrip++){  

			  if((centralStrip+kStrip-m_takeNeighbours)/128==centralStrip/128){
				for(int jSample =0; jSample<nSamples; jSample++){
				  double time = 25*(jSample - (int) nSamples/2);		
				  double adcforfit= subtractCommonMode(channelFromStrip[RorPhi][centralStrip+kStrip-m_takeNeighbours],  allSamplesTell1Data[jSample]);
				  if(centralADC>0){
					pulse->Fill(time,adcforfit);
					//info()<<m_nEvents<<"  "<<kStrip-m_takeNeighbours<<"  "<<adcforfit<<endmsg;
				  }else{
					pulse->Fill(time,-adcforfit);	      
					//info()<<m_nEvents<<"  "<<kStrip-m_takeNeighbours<<"  "<<-adcforfit<<endmsg;
				  }	

				  signalDetected[channelFromStrip[RorPhi][centralStrip+kStrip-m_takeNeighbours]]=true;	

				}
			  }
			}
			func->SetParameter(1,centralADC);
			func->SetParameter(2,centralTime);
			pulse->Fit("dtfitfunc","QN");
			// if(channelNumber<32){
			debug()<<"Channel: "<<channelNumber<<" ("<<channelNumber%32<<")  Chi^2: "<<func->GetChisquare()/((double) func->GetNDF())
			  <<"  Offset: "<<func->GetParameter(0)<<" +- "<<func->GetParError(0)<<"  Amplitude: "<<func->GetParameter(1)
			  <<" +- "<<func->GetParError(1)<<"  Peak: "<<func->GetParameter(2)<<" +- "<<func->GetParError(2)<<endmsg;
			for(int lEntry=1; lEntry<=nSamples;lEntry++){
			  debug()<<"Channel: "<<channelNumber<<" Time: "<<(pulse->GetBinCenter(lEntry))<<" ADC: "<<(pulse->GetBinContent(lEntry))<<endmsg;
			}
			//}
			if(func->GetChisquare()/(func->GetNDF()+0.0001)< m_chisqCut && fabs(func->GetParameter(1))>m_threshold&&fabs (func->GetParameter(1))<1000){
               char chi2plotname[200];
			  //if(channelNumber<32) debug()<<"ACCEPTED"<<endmsg;
			  if(m_detailedOutput){
				sprintf(fitvalues_offset,"fit_Tell1%03d_ch%04d_timebin%02d_offset_sens%d",TELL1_number,channelFromStrip[RorPhi][centralStrip],finebin,sensNumber);
				sprintf(fitvalues_ampltiude,"fit_Tell1%03d_ch%04d_timebin%02d_amplitude_sens%d",TELL1_number,channelFromStrip[RorPhi][centralStrip],finebin,sensNumber);
				sprintf(fitvalues_peak,"fit_Tell1%03d_ch%04d_timebin%02d_peak_sens%d",TELL1_number,channelFromStrip[RorPhi][centralStrip],finebin,sensNumber);	 
			  }else{
				sprintf(fitvalues_offset,"fit_Tell1_%03d_timebin%02d_offset_sens%d",TELL1_number,finebin,sensNumber);
				sprintf(fitvalues_ampltiude,"fit_Tell1_%03d_timebin%02d_amplitude_sens%d",TELL1_number,finebin,sensNumber);
				sprintf(fitvalues_peak,"fit_Tell1_%03d_timebin%02d_peak_sens%d",TELL1_number,finebin,sensNumber);	
				sprintf(chi2plotname,"Chisq_Tell1_%03d_sens_%d",TELL1_number,sensNumber);	
			  }
              plot1D(func->GetChisquare()/((double) func->GetNDF()), chi2plotname, chi2plotname, -1.5,100.5, 102);
			  plot1D(func->GetParameter(0),fitvalues_offset,fitvalues_offset,-15.5,15.5,31);	    
			  plot1D(func->GetParameter(1),fitvalues_ampltiude,fitvalues_ampltiude,-60.5,60.5,121);
			  plot1D(func->GetParameter(2),fitvalues_peak,fitvalues_peak,-200.25,200.25,801);		   

			  //if(channelNumber==1463) info()<<m_nEvents<<"  "<<channelFromStrip[RorPhi][centralStrip]<<"  "<<centralStrip<<"  "<<centralADC<<"  "<<signalDetected[channelFromStrip[RorPhi][centralStrip]]<<endmsg;

			  /*
				 if((channelNumber%32==4||channelNumber%32==23)){
				 plot1D(func->GetParameter(0),"offset_testpulse","offset_testpulse",-60.5,60.5,121);	      
				 plot1D(func->GetParameter(1),"amplitude_testpulse","amplitude_testpulse",-240.5,240.5,481);	
				 plot1D(func->GetParameter(2),"peak_testpulse","peak_testpulse",-200.25,200.25,801);	
				 plot1D(func->GetChisquare()/(func->GetNDF()+0.0001),"Chi2_testpulse","Chi2_testpulse",-0.1,100.1,501);
				 }else{
				 plot1D(func->GetParameter(0),"offset_noise","offset_noise",-60.5,60.5,121);	      
				 plot1D(func->GetParameter(1),"amplitude_noise","amplitude_noise",-240.5,240.5,481);	
				 plot1D(func->GetParameter(2),"peak_noise","peak_noise",-200.2,200.2,1001);	
				 plot1D(func->GetChisquare()/(func->GetNDF()+0.0001),"Chi2_noise","Chi2_noise",-0.1,100.1,501);
				 }
			   */
			}
			for(int jSample =0; jSample<nSamples; jSample++){
			  double time = 25*(jSample - (int) nSamples/2)+m_nEvents/m_stepSize;		
			  double adcforfit= subtractCommonMode(channelFromStrip[RorPhi][centralStrip],  allSamplesTell1Data[jSample]);
			  if(m_detailedOutput) sprintf(finepulseshape,"pulseshapeStrip_Tell1%03d_ch%04d",TELL1_number,channelFromStrip[RorPhi][centralStrip]);
			  else sprintf(finepulseshape,"pulseshapeStrip_Tell1%03d",TELL1_number);
			  if(centralADC>0){
				profile1D(time,adcforfit,finepulseshape,finepulseshape,-25*(nSamples/2)-0.5,25*(nSamples/2+1)-0.5,25*nSamples);		
			  }else{
				profile1D(time,-adcforfit,finepulseshape,finepulseshape,-25*(nSamples/2)-0.5,25*(nSamples/2+1)-0.5,25*nSamples);
				//plot2D(time,-adcforfit,finepulseshape,finepulseshape,-25*(nSamples/2)-0.5,25*(nSamples/2+1)-0.5,-128.5, 127.5,25*nSamples,128);	       	
			  }

			}


			//bool positive;
			for(int jSample =0; jSample<nSamples; jSample++){
			  double time = 25*(jSample - (int) nSamples/2)+m_nEvents/m_stepSize;
			  tmpADC=0;
			  for(int kStrip=0;kStrip<m_takeNeighbours*2+1;kStrip++){  
				if((centralStrip+kStrip-m_takeNeighbours)/128==centralStrip/128){		
				  tmpADC += subtractCommonMode(channelFromStrip[RorPhi][centralStrip+kStrip-m_takeNeighbours],  allSamplesTell1Data[jSample]);
				}
			  }
			  if(centralADC>0){
				if(m_detailedOutput) sprintf(finepulseshape,"pulseshapeCluster_Tell1%03d_ch%04d_%s",TELL1_number,channelFromStrip[RorPhi][centralStrip],"Even");
				else sprintf(finepulseshape,"pulseshapeCluster_Tell1%03d_%s",TELL1_number,"Even");
				//positive=true;
				profile1D(time,tmpADC,finepulseshape,finepulseshape,-25*(nSamples/2)-0.5,25*(nSamples/2+1)-0.5,25*nSamples);			
			  }else{
				if(m_detailedOutput) sprintf(finepulseshape,"pulseshapeCluster_Tell1%03d_ch%04d_%s",TELL1_number,channelFromStrip[RorPhi][centralStrip],"Odd");
				else sprintf(finepulseshape,"pulseshapeCluster_Tell1%03d_%s",TELL1_number,"Odd");
				//positive=false;
				profile1D(time,tmpADC,finepulseshape,finepulseshape,-25*(nSamples/2)-0.5,25*(nSamples/2+1)-0.5,25*nSamples);		  	       	
			  }
			}

			//if(channelFromStrip[RorPhi][centralStrip]<32) info()<<m_nEvents<<" Channel "<<channelFromStrip[RorPhi][centralStrip]<<" ADC "<<centralADC<<" Positive? "<<positive<<endmsg;
			pulse->Reset();

		  }


		}
	  }
	}
  }
  delete pulse;
#endif

  delete func;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::PulseShapeFitter::finalize() {
  if ( m_debugLevel ) debug() << "==> Finalize" << endmsg;
  return VeloMonitorBase::finalize(); // must be called after all other actions
}

//=============================================================================
// Retrieve the VeloTell1Data
//=============================================================================
LHCb::VeloTELL1Datas*
Velo::PulseShapeFitter::veloTell1Data( std::string samplingLocation ) {
  //std::string tesPath = "Raw/Velo/" + samplingLocation + "/ADCCMSuppressed";
  //std::string tesPath = samplingLocation + "Raw/Velo/"+  "/ADCCMSuppressed" ;
  std::string tesPath;

  if(samplingLocation == "") tesPath = samplingLocation + "Raw/Velo/DecodedADC"; 
  // info() << "samplingLocation  " << samplingLocation  << endmsg;
  else  tesPath = samplingLocation + "/Raw/Velo/DecodedADC";

  size_t pos = tesPath.find( "//" );
  if ( pos != std::string::npos )    tesPath.replace( pos, 2, "/" );
  //if (m_nEvents %100==1  )info() << "tesPath = " << tesPath << endmsg; 
  if ( m_debugLevel )
	debug() << "Retrieving VeloTell1Data from " << tesPath << endmsg;
  if ( !exist<LHCb::VeloTELL1Datas>( tesPath ) ) {
	debug() << "No VeloTell1Data container found for this event !" << endmsg; 
	if (m_nEvents %100==1  )  info() << "No VeloTell1Data container found for this event !   " << tesPath << endmsg; 
	return 0;
  }
  else {
	LHCb::VeloTELL1Datas* tell1Data = get<LHCb::VeloTELL1Datas>( tesPath );
	if ( m_debugLevel ) debug() << "  -> number of VeloTell1Data found in TES: "
	  << tell1Data->size() <<endmsg;
	return tell1Data;
  }
}

//=============================================================================
// Monitor the VeloTell1Data
//=============================================================================
/*
   void Velo::PulseShapeFitter::MakePulseShape( std::string samplingLocation,  LHCb::VeloTELL1Datas* tell1Datas ) 
   { 

   unsigned int nSamples = m_samplingLocations.size();
   int halfTAEWindow = (int)(floor(m_samplingLocations.size()/2.));
// Loop over the VeloTell1Data
// ==========================+
LHCb::VeloTELL1Datas::const_iterator itD;
for ( itD = tell1Datas -> begin(); itD != tell1Datas -> end(); ++itD ) {
LHCb::VeloTELL1Data* data = (*itD);
int TELL1_number=data->key();
if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue;
// get all the samples for this particular tell1:
//std::vector<LHCb::VeloTELL1Data* > tell1samples;
//LHCb::VeloTELL1Data* data = (*itD);  
//info() << "Running for tell1 "  <<  TELL1_number <<  endmsg;
// Charge for each channel
// -----------------------
//coarsepulse[TELL1_number][channel + 32*aLink][samplingIndex+halfTAEWindow]=adc;

}
}
}

}
 */
//=============================================================================
double Velo::fitfsimple(double *x, double *par){
  double fitval;
  if(x[0]<par[2]) fitval = par[0]+par[1]*(exp(-((x[0]-par[2])/par[3])*((x[0]-par[2])/par[3])));
  else fitval = par[0]+par[1]*(exp(-((x[0]-par[2])/par[4])*((x[0]-par[2])/par[4])));
  return fitval;
}

double Velo::PulseShapeFitter::subtractCommonMode(int channelNumber, LHCb::VeloTELL1Data *tell1data){
  // identifying which link
  ALinkPair begEnd;
  int aLink = (int)(channelNumber/32.);
  //info() << " channelNumber "  << channelNumber << " alink = " << aLink << endmsg; 
  begEnd=(*tell1data)[aLink];
  scdatIt iT;
  double commonmode=0;
  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
	commonmode += *iT;  
  }
  commonmode /= 32.0;
  int count = 0;
  //info()<< " common mode " << commonmode<< endmsg;
  double commonmodewithoutsignal= 0.0;
  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
	double cmsadc = *iT - commonmode; 
	//info()<< "cmsadc >>>>>>>>>>> " << cmsadc << endmsg;
	if(fabs(cmsadc) < m_threshold){ 
	  commonmodewithoutsignal += *iT;
	  count++;
	}
  }
  commonmodewithoutsignal /= float(count+0.00001);
  //info()<< " common mode without signal " << commonmodewithoutsignal << " count = "  << count << endmsg;
  double cmsadc = tell1data->channelADC(channelNumber) - commonmodewithoutsignal;
  //double cmsadcwithsignal = tell1data->channelADC(channelNumber) - commonmode;
  //char cmsplotname[100]; sprintf(cmsplotname, "cmsplot_%02d_Tell1_%02d",aLink, tell1data->key());
  //plot1D(cmsadc,cmsplotname,cmsplotname, -100,100,201);

  //info()<< " cms adc with signal " << cmsadcwithsignal << endmsg;
  return cmsadc;

}
