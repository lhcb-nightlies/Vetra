// $Id: $
#ifndef INCLUDE_SEUMONITOR_H
#define INCLUDE_SEUMONITOR_H 1

#include <string>
#include <vector>

#include <TH1D.h>
#include <TH2D.h>

#include "VeloMonitorBase.h"

// save some memory with gcc/icc by packing the SEUCtr class into two bytes
#if defined __GNUC__ || defined __INTEL_COMPILER
#define PACKED __attribute__ ((packed))
#else
#define PACKED
#endif

// forward decl.
class IEventTimeDecoder;

namespace Velo {

/** @class SEUMonitor SEUMonitor.h
 * 
 * monitor single event upsets via lowest two bits in Velo error banks
 *
 * @author Manuel Tobias Schiller <manuel.schiller@nikhef.nl>
 * @date   2012-01-26
 *
 * The idea is based on work by Martin van Beuzekom.
 *
 * The idea of the algorithm is to monitor the Single Event Upset counter
 * inside the Beetle chips. With each NZS bank, an EvtInfo structure is sent
 * which contains the lowest two bits of the SEU counter (called S0 and S1 in
 * the Beetle reference manual) in the upper two bits of the IHeader byte of
 * each Beetle. Valid increments in this two bit counter are used to count the
 * number of Single Event Upsets. Invalid transitions in this two-bit counter
 * can also happen (e.g. if two SEUs happened in between two NZS banks), this
 * is treated as an SEU counter desynchronisation. The Algorithm monitors how
 * often this happens, and it continues to look for valid SEU counter
 * increments after updating to the new value of the two-bit SEU counter after
 * the desync. event.
 */
class SEUMonitor : public Velo::VeloMonitorBase {
    private:
	// forward declaration
	class SEUCtr;
    public:

	/// Standard Constructor
	SEUMonitor(const std::string& name, ISvcLocator* pSvcLocator);

	virtual ~SEUMonitor(); ///< Destructor

	virtual StatusCode initialize() override; ///< Algorithm initialization
	virtual StatusCode    execute() override; ///< Algorithm event execution
	virtual StatusCode   finalize() override; ///< Algorithm finalize

    private:
	// tool to decode the ODIN
	IEventTimeDecoder* m_odinDecoder;
	/// error bank location
	std::string m_eventInfoLocation;
	/// dump error banks to info?
	bool m_dump2Info;
	/** monitoring mode will propagate every change in SEU counters
	 * directly to histograms */
	bool m_monitoringMode;
	/// single event upset counter per sensor, ppFPGA and Beetle
	std::vector<SEUCtr> m_seuctrs;
	/// last valid event time in ns since the epoch
	unsigned long long m_evtime;
	/// last valid event number
	unsigned long long m_evtnr;
	/// last valid run number
	unsigned m_runnr;
	/// count events with EvtInfos
	unsigned m_evtctr;
	/// 2D histogram SEU counter as function of sensor and (4*ppFPGA + beetle)
	TH2D *m_hseuctr2d;
	/// 1D histogram SEU counter as function of sensor
	TH1D *m_hseuctr1d;
	/// 2D histogram SEU desync counter as function of sensor and (4*ppFPGA + beetle)
	TH2D *m_hdesyncctr2d;
	/// 2D histogram SEU desync counter as function of sensor
	TH1D *m_hdesyncctr1d;
	/// 1D histogram to record the number of time jumps etc.
	TH1D *m_hevstat;

	/// event states
	enum {
	    NoEvtInfo = 0,
	    RunChange,
	    LargeGap,
	    TimeBackwards,
	    OK
	};

	using Algorithm::index;

	/// calculate index in m_seuctrs
	virtual unsigned index(unsigned sensor, unsigned ppfpga, unsigned beetle) const
	{ return 16u * sensor + 4u * (3u - ppfpga) + beetle; }

	/// helper to process the carry from SEUCtr to real histograms
	void processCarryToHisto(const std::vector<unsigned>& indices);

	/// helper for pretty-printing a vector of unsigned ints
	void dumpVectUI(const std::string& pfx,
		const std::string& name,
		const std::vector<unsigned>& v) const;
	/// helper to dump error banks to info()
	void dump2Info(const EvtInfos* errorbanks) const;

	/// class to keep track of state of SEU counter
	class SEUCtr {
	    private:
		union {
		    unsigned short m_int;
		    struct {
			unsigned m_ctr:2; ///< lowest two bits of SEU counter
			unsigned:1; ///< reserved
			unsigned m_initialised:1; ///< true if counter is valid
			unsigned m_nSEU:6; ///< number of SEUs detected
			unsigned m_desyncs:6; ///< number of desync. transitions
		    } PACKED m_struct;
		} PACKED m_data;
	    public:
		/// constructor - zeros all counters, state uninitialised
		SEUCtr() { m_data.m_int = 0u; }
		/// set the initialised flag
		void setInitialised(bool initialised = true)
		{ m_data.m_struct.m_initialised = initialised; }
		/// return current counter of detected SEUs
		unsigned nSEU() const { return m_data.m_struct.m_nSEU; }
		/// return current counter of detected desyncs
		unsigned nDesyncs() const { return m_data.m_struct.m_desyncs; }
		// true if at least one of the counters is non-zero
		operator bool() const { return nSEU() || nDesyncs(); }
		/// zero SEU and desync counters
		void zeroCounters()
		{ m_data.m_struct.m_nSEU = m_data.m_struct.m_desyncs = 0u; }
		/** @brief accumulate a SEU counter transition
		 * @param ctr	new state of SEU counter
		 * 		(only lowest two bits are significant)
		 * @returns	true if SEU/desync counters changed as a
		 * 		result of the transition
		 */
		bool operator+=(unsigned ctr)
		{
		    ctr &= 3u;
		    if (!m_data.m_struct.m_initialised) {
			// if we're not initialised yet, we just latch the SEU
			// counter
			setInitialised();
			m_data.m_struct.m_ctr = ctr;
			return false;
		    }
		    // no transition in SEU counter
		    if (ctr == (m_data.m_struct.m_ctr & 3u)) {
			// no need to read out counters and update histograms
			return false;
		    }
		    if (ctr == ((1u + m_data.m_struct.m_ctr) & 3u)) {
			// valid SEU counter transition: found SEU
			++m_data.m_struct.m_nSEU;
		    } else {
			// invalid SEU counter transition, lost sync.
			++m_data.m_struct.m_desyncs;
		    }
		    m_data.m_struct.m_ctr = ctr;
		    return true;
		}
		/// return if we need to update counters to prevent overflow
		bool willOverflow() const
		{
		    return ((1u << 6) - 1u) == m_data.m_struct.m_nSEU ||
			((1u << 6) - 1u) == m_data.m_struct.m_desyncs;
		}
	};
};

} // namespace Velo

#endif // INCLUDE_SEUMONITOR_H

// vim: sw=4:tw=78:ft=cpp
