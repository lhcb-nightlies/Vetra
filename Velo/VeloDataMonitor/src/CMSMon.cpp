#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/AlgFactory.h" 
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"
#include "CommonFunctions.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "Tell1Kernel/VeloDecodeConf.h" 
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
// Class : Velo::Monitoring::CMSMon
// author: kazu akiba  
//-----------------------------------------------------------------------------
using namespace LHCb;
using namespace VeloTELL1;
using namespace std;
namespace Velo
{
  namespace Monitoring
  {
	class CMSMon : public GaudiHistoAlg {
	  public:
		/// Standard constructor
		CMSMon( const string& name, ISvcLocator* pSvcLocator );
		virtual ~CMSMon( ); ///< Destructor
		virtual StatusCode initialize() override;    ///< Algorithm initialization
		virtual StatusCode execute   () override;    ///< Algorithm execution
		virtual StatusCode finalize  () override;    ///< Algorithm finalization
		StatusCode plotMCMSubtractedMonitor();
		StatusCode plotLCMSubtractedMonitor();
	  private  :
		long int  m_evtNumber;
		int m_refresh;
		int m_plotsimple;
		int m_plotall;
		int m_plotsignals;
		int m_plotlcms;
		DeVelo* m_veloDet;
		vector<int> m_chosenTell1s;
		bool m_plot2DADCs;
		int m_maxEvts;
		vector<string>  m_locations;
		bool m_evenodd;
	};
  }
}


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::CMSMon::CMSMon( const string& name,
	ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
  ,  m_evtNumber(0)
  ,  m_refresh  (5000)
  ,  m_veloDet  (0)
  ,  m_locations( )
{
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::ADCs             ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::PedSubADCs       ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::ReorderedADCs    ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ) ;
  declareProperty( "Containers" ,   m_locations  ,  "The list of TES locations to be monitored") ;
  declareProperty( "TELL1s",               m_chosenTell1s);
  declareProperty( "RefreshRate",          m_refresh);
  declareProperty( "PlotAll",              m_plotall=1);
  declareProperty( "PlotSignals",          m_plotsignals=1);
  declareProperty( "PlotLCMS",             m_plotlcms=1);
  declareProperty( "PlotSimple",           m_plotsimple =1);
  declareProperty( "Plot2DHistos",         m_plot2DADCs=false);
  declareProperty( "MaxEvts",              m_maxEvts = -1);
  setProperty ( "PropertiesPrint", true ).ignore();
}
//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::CMSMon::~CMSMon() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::CMSMon::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( propsPrint() ) printProps();
  debug() << "==> Initialize" << endmsg;
  setHistoTopDir( "Vetra/" );
  
  
  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );
  if(m_veloDet == 0) Error("Can't fint detector element. Exiting with Failure",StatusCode::FAILURE);

  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::CMSMon::finalize() {
  debug() << "==> Finalize" << endmsg;  
  return GaudiHistoAlg::finalize();  // must be called after all other actions
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::CMSMon::execute() {
  debug() << "==> Execute" << endmsg;  
  if((m_evtNumber < m_maxEvts) || (m_maxEvts == -1)){
	plotMCMSubtractedMonitor();
	plotLCMSubtractedMonitor();
  }
  m_evtNumber++;    
  return StatusCode::SUCCESS;
}
//==============================================================================================================
//==============================================================================================================

//==============================================================================================================
StatusCode Velo::Monitoring::CMSMon::plotMCMSubtractedMonitor(){
  string  pedSubAdcLocation = LHCb::VeloTELL1DataLocation::PedSubADCs;
  string  reorderAdcLocation = LHCb::VeloTELL1DataLocation::ReorderedADCs; 
 
  if(!exist<VeloTELL1Datas>( pedSubAdcLocation)){
      error()<< " could not get the ped sub data, bailing out of MCMS monitor" << endmsg;
      return StatusCode::SUCCESS;
  }
  if(!exist<VeloTELL1Datas>( reorderAdcLocation )){
      error()<< " could not get the reordered data, bailing out of MCMS monitor" << endmsg;
     return StatusCode::SUCCESS;
  }
  VeloTELL1Datas *pedsubADCs =  get<VeloTELL1Datas>( pedSubAdcLocation );
  //VeloTELL1Datas *reorderADCs =  get<VeloTELL1Datas>( reorderAdcLocation );

  VeloTELL1Datas::const_iterator pedsubsensIt,  reordersensIt;

  for(pedsubsensIt=pedsubADCs->begin();pedsubsensIt!=pedsubADCs->end();++pedsubsensIt, ++reordersensIt){
	VeloTELL1Data* pedsubdataOneTELL1=(*pedsubsensIt);
	VeloTELL1Data* reorderdataOneTELL1=(*reordersensIt);
	int TELL1_number=pedsubdataOneTELL1->key();
	if(ExcludeTELL1(m_chosenTell1s,TELL1_number)) continue;
	if(pedsubdataOneTELL1->key() != reorderdataOneTELL1->key() ) 
	  error() << " FUCK! tell1s not ordered? pedsub " << pedsubdataOneTELL1->key()<< "  cms"  << reorderdataOneTELL1->key() << endmsg;
	const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(TELL1_number);
	if(sensor == 0 ){
	  return StatusCode::FAILURE;
	}         
	boost::format fmt ( "%s/TELL1_%03d/cmssubtracted");
	fmt %  "MCMSMon" % TELL1_number;           
	boost::format title ( ", TELL1 %d, sensor %d" );
	title %  TELL1_number % sensor->sensorNumber();
	double ylow=-128.5;
	double yhigh=127.5;
	//Reset the profile histograms after m_refresh events.
	//This is handy for online monitoring.
	//Book memory consuming 2D histograms:
	//If the bank is non-pedestal subtracted the y-axis should have a different range.
	//Fill all histograms:
	AIDA::IHistogram2D* hchipsubtractedMcms=0;
	AIDA::IHistogram2D* hchipsubtractedMcmsWithHits=0;
	AIDA::IHistogram2D* hstripsubtractedMcms=0;
	AIDA::IHistogram2D* hsimplechipsubtractedMcms=0;
	//AIDA::IHistogram2D* hsimplestripsubtractedMcms=0;
	AIDA::IHistogram1D* hsubtractedMcms = book1D(fmt.str() +"Mcms"  , "subtracted Mcms" + title.str(), ylow,yhigh,256);

	if(m_plotall ==1){ 
	  hchipsubtractedMcms= book2D(fmt.str() + "ChipChannelMcms"  , "subtracted cms " + title.str(), 
		  -0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS,ylow,yhigh,256);
	  hstripsubtractedMcms = book2D(fmt.str() +"StripMcms"  , "subtracted cms" + title.str(), 
		  -0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS, ylow,yhigh,256);
	}
	if(m_plotsignals ==1){ 
	  hchipsubtractedMcmsWithHits= book2D(fmt.str() + "ChipChannelMcmsWithHits"  , "subtracted cms With Hits" + title.str(), 
		  -0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS,ylow,yhigh,256);
    }
	if(m_plotsimple ==1){
	  hsimplechipsubtractedMcms= book2D(fmt.str() + "SimpleChipChannelMcms"  , "subtracted cms " + title.str(), 
		  -0.5,64-0.5,  64,ylow,yhigh,256);
	  //hsimplestripsubtractedMcms = book2D(fmt.str() +"SimpleStripMcms"  , "subtracted cms" + title.str(), 
          //-0.5,64-0.5,  64, ylow,yhigh,256);
	}
	for(int index=0;index<SENSOR_CHANNELS;++index){
	  unsigned int channel=index;
	  unsigned int strip = sensor->ChipChannelToStrip(channel);
	  int pedsubvalue =-1000; // after pedestal subtration
	  int reordervalue =-1000; // mcms after reordering
	  pedsubvalue=pedsubdataOneTELL1->channelADC(channel);
	  //channel=sensor->StripToChipChannel(static_cast<unsigned int> (index));
	  reordervalue=reorderdataOneTELL1->stripADC(strip);
	  //error() << " channel"  << channel <<" ped sub val =  " << pedsubvalue << " reordered val == MCMS "   << reordervalue << endmsg;
      if(m_plotsignals && reordervalue > 10){
		hchipsubtractedMcmsWithHits->fill ( channel, reordervalue - pedsubvalue );
      }
      double value = reordervalue - pedsubvalue ;
	  if(m_plotall == 1){
		hchipsubtractedMcms->fill ( channel, reordervalue - pedsubvalue );
		hstripsubtractedMcms->fill ( strip, reordervalue - pedsubvalue) ; 
	  }
      if(m_plotsimple ==1 && (channel%32 ==16) ){
		hsimplechipsubtractedMcms->fill((channel/32), reordervalue - pedsubvalue );
      }
	  hsubtractedMcms->fill(value);
	}
  }            
  return StatusCode::SUCCESS;
}
//====================================================================================
StatusCode Velo::Monitoring::CMSMon::plotLCMSubtractedMonitor(){
  string  reorderAdcLocation = LHCb::VeloTELL1DataLocation::ReorderedADCs; 
  string  cmsSubAdcLocation = LHCb::VeloTELL1DataLocation::CMSuppressedADCs;

  if(!exist<VeloTELL1Datas>(cmsSubAdcLocation  )){
      error()<< " could not get the cms sub data, bailing out of LCMS monitor" << endmsg;
	  return StatusCode::SUCCESS;
  }
  if(!exist<VeloTELL1Datas>( reorderAdcLocation )){
      error()<< " could not get the reordered data, bailing out of LCMS monitor" << endmsg;
        return StatusCode::SUCCESS;
  }
  //VeloTELL1Datas *cmssubADCs =  get<VeloTELL1Datas>( cmsSubAdcLocation );
  VeloTELL1Datas *reorderADCs =  get<VeloTELL1Datas>( reorderAdcLocation );
  VeloTELL1Datas::const_iterator cmssubsensIt, reordersensIt;
  for(reordersensIt=reorderADCs->begin();reordersensIt!=reorderADCs->end();++cmssubsensIt, ++reordersensIt){
	VeloTELL1Data* cmssubdataOneTELL1=(*cmssubsensIt);
	VeloTELL1Data* reorderdataOneTELL1=(*reordersensIt);
	int TELL1_number=reorderdataOneTELL1->key();
	if(ExcludeTELL1(m_chosenTell1s,TELL1_number)) continue;
	if(cmssubdataOneTELL1->key() != reorderdataOneTELL1->key() ){
	  error() << " FUCK! tell1s not ordered? lcmssub " 
              <<  cmssubdataOneTELL1->key()
              << "  mcms"  << reorderdataOneTELL1->key() << endmsg;
    }
	//Get sensor from detector element:  
	const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(TELL1_number);
	if(sensor == 0 ){
	  error() << "Can't find a sensor associated with TELL1 " << TELL1_number << ". Aborting..." << endmsg;
	  return StatusCode::FAILURE;
	}         
	//Set up directory structure and histogram names:    
	boost::format fmt ( "%s/TELL1_%03d/cmssubtracted");
	//fmt %  strippedContainerName(cmsSubAdcLocation) % TELL1_number;           
	fmt %  "LCMSMon" % TELL1_number;           
	boost::format title ( ", TELL1 %d, sensor %d" );
	title %  TELL1_number % sensor->sensorNumber();
	//Book histograms explicitly. This gives easy access to the histogram 
	//pointer so that the histogram can be reset.        
	double ylow=-128.5;
	double yhigh=127.5;
	AIDA::IHistogram2D* hchipsubtractedLcms=0;
	AIDA::IHistogram2D* hstripsubtractedLcms=0;
	AIDA::IHistogram2D* hstripsubtractedLcmsWithHits=0;
	AIDA::IHistogram1D* hsubtractedLcms = book1D(fmt.str() +"subtractedLcms"  , "subtracted Lcms" + title.str(), ylow,yhigh,256);
	if(m_plotlcms ==1){ 
	  hchipsubtractedLcms= book2D(fmt.str() + "ChipChannelLcms"  , "subtracted Lcms " + title.str(), 
		  -0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS,ylow,yhigh,256);
	  hstripsubtractedLcms = book2D(fmt.str() +"StripLcms"  , "subtracted Lcms" + title.str(), 
		  -0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS, ylow,yhigh,256);
	}
	if(m_plotsignals ==1){ 
	  hstripsubtractedLcmsWithHits = book2D(fmt.str() +"StripLcmsWithHits"  , "subtracted Lcms With Hits  " + title.str(), 
		  -0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS, ylow,yhigh,256);
	}
	//Reset the profile histograms after m_refresh events.
	//This is handy for online monitoring.
	//Book memory consuming 2D histograms:
	//If the bank is non-pedestal subtracted the y-axis should have a different range.
	//Fill all histograms:
	for(int index=0;index<SENSOR_CHANNELS;++index){
	  unsigned int channel=index;
	  unsigned int strip = sensor->ChipChannelToStrip(channel);
	  int cmssubvalue =-1000; // after pedestal subtration
	  int reordervalue =-1000; // mcms after reordering
	  // channel=sensor->StripToChipChannel(static_cast<unsigned int> (index));
	  cmssubvalue=cmssubdataOneTELL1->stripADC(strip);
	  reordervalue=reorderdataOneTELL1->stripADC(strip);
      double value = cmssubvalue - reordervalue;
	  //error() << " channel"  << channel <<" cms sub val =  " << cmssubvalue << " reordered val == MCMS "   << reordervalue << endmsg;
      if(m_plotsignals && cmssubvalue > 10){
		hstripsubtractedLcmsWithHits->fill ( channel, value );
      }
	  if(m_plotall){
		hchipsubtractedLcms->fill ( channel,value );
		hstripsubtractedLcms->fill ( strip,value ) ;
	  }
      hsubtractedLcms->fill(value);
	  //cmssubvalue=cmssubdataOneTELL1->stripADC(static_cast<unsigned int> (index)); 
	  //reordervalue=reorderdataOneTELL1->stripADC(static_cast<unsigned int> (index)); 
	}
  }            
  return StatusCode::SUCCESS;
}

// Declaration of the Algorithm Factory
DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, CMSMon )
