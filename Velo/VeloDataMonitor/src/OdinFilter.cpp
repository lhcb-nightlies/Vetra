// $Id: OdinFilter.cpp,v 1.2 2009-09-20 18:45:17 szumlat Exp $
// Include files
// -------------
#include <iostream>
#include <fstream>
// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
#include "GaudiAlg/GaudiAlgorithm.h" 
// from Tell1Kernel
#include "Tell1Kernel/VeloDecodeConf.h" 
#include "Event/ODIN.h"
#include "Event/VeloODINBank.h"
// local
#include "OdinFilter.h"
#include "CommonFunctions.h"
// from Boost
#include <boost/assign/list_of.hpp>
#include "boost/lexical_cast.hpp"
// from Gaudi
#include "AIDA/IHistogram2D.h"
// local
// Kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"

#include "Event/ODIN.h"


using namespace VeloTELL1;

// Declaration of the Algorithm Factory
namespace Velo {
  DECLARE_ALGORITHM_FACTORY( OdinFilter )
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  Velo::OdinFilter::OdinFilter( const std::string& name, ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
	,m_nEvents(0)
{
  std::vector<std::string> tmpLocations = 
	boost::assign::list_of("Prev7")("Prev6")("Prev5")("Prev4")("Prev3")("Prev2")("Prev1")
	("")("Next1")("Next2")("Next3")("Next4")("Next5")("Next6")("Next7");
  declareProperty( "SamplingLocations", m_samplingLocations= tmpLocations );
  declareProperty( "DataLocation", m_dataLocation = "Raw/Velo/PreparedADCs");
  declareProperty( "NSteps", m_nSteps= 1 );
  declareProperty( "StepSize", m_stepSize= 100 );
  declareProperty( "CMSCut", m_cmsCut= 10 );
  declareProperty( "RunWithOdin" , m_runWithOdin = true ); // 
  declareProperty( "RunNumbers", m_runNumbers );
  declareProperty( "RunNumbersDelay", m_runNumbersDelay );
  declareProperty( "ChosenBunchCounters", m_chosenBunches );
  declareProperty( "RunWithChosenBunchCounters", m_useBCNT = true );
  declareProperty( "EventsPerFile", m_eventsPerFile=-1 );
  declareProperty( "SkipStepZero", m_skipStepZero=false );
  declareProperty( "SkipSteps",    m_skipSteps);
  declareProperty( "NEventsPerStep", m_nEventsPerStep= -1  );
  declareProperty( "AnalyseOnlyStepNumber", m_analyseOnlyStepNumber= -1  );
  declareProperty( "NumberOfCMSIterations", m_numberOfCMSIterations = 1);
  m_runNumbers.push_back(-1);
  m_runNumbersDelay.push_back(0);
}
//=============================================================================
// Destructor
//=============================================================================
Velo::OdinFilter::~OdinFilter() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::OdinFilter::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  printProps();
  if(m_nSteps==1) m_stepSize=1000000000; 
  m_timeDecoder = tool<IEventTimeDecoder>( "OdinTimeDecoder" );
  if ( sc.isFailure() ) return sc;
  if(m_runNumbers.size() == m_runNumbersDelay.size() &&  m_runNumbersDelay.size() >0  ){
	std::vector<int>::const_iterator irunNumber; int i =0;
	for( irunNumber =m_runNumbers.begin(); irunNumber!=m_runNumbers.end(); irunNumber++, i++){
	  m_runToDelay[*irunNumber] = m_runNumbersDelay[i]; 
	  info() << " run number = " <<  *irunNumber << " corresponds to delay " << m_runToDelay[*irunNumber] << endmsg;
	}
  }
  else info() << " check yout option file for the delays and runs cause something seems wrong" << endmsg;
  m_eventsThisFile=0;
  return StatusCode::SUCCESS;
  
}

//=============================================================================
/// Main execution
//=============================================================================
StatusCode Velo::OdinFilter::execute() {
  counter( "# events" ) += 1;
  m_nEvents++;
  setFilterPassed(false) ;

  LHCb::ODIN* odin=0;
  if (m_runWithOdin){
	if(exist<LHCb::ODIN>((LHCb::ODINLocation::Default))) {
	  odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
	}
	else{
	  m_timeDecoder->getTime(); // in case we wants to get other info from odin.
	  odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
	}	
  }

  else m_runNumbers[0] = -1;
  if(odin ){
	int bunchCounter =  odin->bunchId();
	debug() << " >>> bunch id: " <<  odin->bunchId() << endmsg;
	std::vector<int>::iterator ibunch;
	int isitright=0;
	if(m_chosenBunches.size() !=0){
	  for(ibunch = m_chosenBunches.begin(); ibunch != m_chosenBunches.end(); ibunch++ ){
		if( bunchCounter == (*ibunch) ) isitright =1;
	  }
	}
	else isitright = 1;
	if(isitright==0){ 
	  debug() << " This is not the  bunch id you were looking for " <<  odin->bunchId() << endmsg;
	  return StatusCode::SUCCESS;
	}
	m_currentCalStep=odin->calibrationStep();
	if(m_skipStepZero && m_currentCalStep==0){
	  setFilterPassed(false) ;
	  info()<<"Skipping step 0"<<endmsg;
	  return StatusCode::SUCCESS;
	}
    if(m_skipSteps.size() > 0){
      std::vector<int>::const_iterator jStep;
      for (  jStep  = m_skipSteps.begin();
             jStep != m_skipSteps.end(); 
             jStep++ )  {

        if (m_currentCalStep == *jStep) {
          setFilterPassed(false) ;
          info()<<"Skipping step "<< m_currentCalStep << endmsg;
          return StatusCode::SUCCESS;
        }
      }
    }

	if(m_analyseOnlyStepNumber >0 ){
	  if( m_analyseOnlyStepNumber == m_currentCalStep ){
		setFilterPassed(true) ;
		info()<<"Enough stats! Skipping the rest of step " << m_currentCalStep << endmsg;
		return StatusCode::SUCCESS;
	  }
    }
	
	if( m_nEvents % 99 == 1) info()<< " this is step " << odin->calibrationStep()  << endmsg;

 	m_eventPerStepCounter[m_currentCalStep] +=1;
 
	if(m_nEventsPerStep >0 ){
	  if(  m_eventPerStepCounter[m_currentCalStep] > m_nEventsPerStep ){
		setFilterPassed(false) ;
		info()<<"Enough stats! Skipping the rest of step " << m_currentCalStep << endmsg;
		return StatusCode::SUCCESS;
	  }
    }
  }
  setFilterPassed(true) ;
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::OdinFilter::finalize() {
  debug() << "==> Finalize" << endmsg;
  return GaudiAlgorithm::finalize(); // must be called after all other actions
}


