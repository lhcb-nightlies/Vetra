// $Id: PulseShapeFitterPedSub.cpp,v 1.2 2010-03-18 14:22:35 kakiba Exp $
// Include files
// -------------
#include <iostream>
#include <fstream>
// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
// from Tell1Kernel
#include "Tell1Kernel/VeloDecodeConf.h" 
#include "Event/ODIN.h"
#include "Event/VeloODINBank.h"
// local
#include "PulseShapeFitterPedSub.h"
#include "CommonFunctions.h"
// from Boost
#include <boost/assign/list_of.hpp>
#include "boost/lexical_cast.hpp"
// from Gaudi
#include "AIDA/IHistogram2D.h"
// local
// Kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"

#include <TProfile.h>

#include "Event/ODIN.h"


using namespace VeloTELL1;

//-----------------------------------------------------------------------------
// Implementation file for class : PulseShapeFitterPedSub
//
//-----------------------------------------------------------------------------
// Declaration of the Algorithm Factory
namespace Velo {
  DECLARE_ALGORITHM_FACTORY( PulseShapeFitterPedSub )
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  Velo::PulseShapeFitterPedSub::PulseShapeFitterPedSub( const std::string& name, ISvcLocator* pSvcLocator)
  : Velo::VeloMonitorBase ( name , pSvcLocator )
,m_nEvents(0)
{
  std::vector<std::string> tmpLocations = 
    boost::assign::list_of("Prev7")("Prev6")("Prev5")("Prev4")("Prev3")("Prev2")("Prev1")
    ("")("Next1")("Next2")("Next3")("Next4")("Next5")("Next6")("Next7");
  declareProperty( "SamplingLocations", m_samplingLocations = tmpLocations);
  declareProperty( "NSteps", m_nSteps= 1 );
  declareProperty( "StepSize", m_stepSize= 100 );
  declareProperty( "ChisqCut", m_chisqCut= 10 );
  declareProperty( "CMSCut", m_cmsCut= 10 );
  declareProperty( "MaxAmplitudeCut", m_amplitudeCut= 300);
  declareProperty( "OffsetCut", m_offsetCut= 10 );
  declareProperty( "IntegralCut", m_integralCut= 10 );
  declareProperty( "UseLogLikeFit", m_useLogLikeFit= true );
  declareProperty( "TimeWindow", m_timeWindow= 150 );
  declareProperty( "ExpectedPeakTime", m_expectedPeakTime= 0 );
  declareProperty( "NTimeBins", m_nTimeBins = 201 );
  declareProperty( "FixOffset", m_fixedOffset= false );
  declareProperty( "TryToClusterize", m_tryCluster= false );
  declareProperty( "TELL1s",m_chosenTell1s     );
  declareProperty( "SignalThreshold" , m_threshold=20);
  declareProperty( "TakeNeighbours", m_takeNeighbours=1 );
  declareProperty( "PlotSeparateStrips", m_plotSeparateStrips=false );
  declareProperty( "Offset" ,m_offset=0);
  declareProperty( "WidthBefore", m_width1=13.68);
  declareProperty( "WidthAfter", m_width2=26.76);
  declareProperty( "DetailedOutput" , m_detailedOutput=true);
  declareProperty( "RunWithOdin" , m_runWithOdin = true ); // 
  declareProperty( "RunNumbers", m_runNumbers );
  declareProperty( "RunNumbersDelay", m_runNumbersDelay );
  declareProperty( "ChosenBunchCounters", m_chosenBunches );
  declareProperty( "RunWithChosenBunchCounters", m_useBCNT = true );
  declareProperty( "InputPedFileName", m_pedFileName = "PedCalc.root");
  declareProperty( "PedDirectory", m_pedDirectory= "Velo/PedCalc");
  m_runNumbers.push_back(-1);
  //  m_runNumbers.push_back(-2);
  m_runNumbersDelay.push_back(0);
  m_chosenTell1s.push_back(-1);

}
//=============================================================================
// Destructor
//=============================================================================
Velo::PulseShapeFitterPedSub::~PulseShapeFitterPedSub() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::PulseShapeFitterPedSub::initialize() {
  StatusCode sc = VeloMonitorBase::initialize(); // must be executed first
  printProps();
  if(m_nSteps==1) m_stepSize=1000000000; 
  m_timeDecoder = tool<IEventTimeDecoder>( "OdinTimeDecoder" );
  if ( sc.isFailure() ) return sc;
  m_velo = getDet<DeVelo>( DeVeloLocation::Default );  

  if(m_plotSeparateStrips && m_chosenTell1s[0]==-1){
	m_chosenTell1s.erase(m_chosenTell1s.begin());
	m_chosenTell1s.push_back(0);
	m_chosenTell1s.push_back(1);
	m_chosenTell1s.push_back(2);
	m_chosenTell1s.push_back(3);
	//If seperate histograms are made for each strip, we quickly run out of memory
	//Hence we limit the number of tell1s
  }
  //const int strips=2048;
  int strip_tmp=0,channel_tmp=0;
  std::ifstream ifs("/home/kakiba/pccChannel-mapping.twocol");
  std::ifstream ifsP("/home/kakiba/rccChannel-mapping.twocol");
  char dummy[100];
  //ifs.open("/home/kakiba/pccChannel-mapping.twocol");
  ifs.getline(dummy,100);
  for(int ichan = 0; ichan<2048; ichan++){
	ifs>>strip_tmp>>channel_tmp;
	m_stripFromChannel[1][channel_tmp]=strip_tmp;  
	m_channelFromStrip[1][strip_tmp]=channel_tmp;  
	debug() << " getting reordering data Phi strip " << strip_tmp <<  " -> channel " << channel_tmp<< endmsg;
  }

  //ifsP.open("/home/kakiba/rccChannel-mapping.twocol");
  ifsP.getline(dummy,100);
  for(int jchan = 0; jchan<2048; jchan++){
	ifsP>>strip_tmp>>channel_tmp;
	m_stripFromChannel[0][channel_tmp]=strip_tmp;  
	m_channelFromStrip[0][strip_tmp]=channel_tmp;                          
	debug() << " getting reordering data R  strip " << strip_tmp <<  " -> channel " << channel_tmp<< endmsg;
  }
  ifsP.close();
  ifs.close();
  if(m_runNumbers.size() == m_runNumbersDelay.size() &&  m_runNumbersDelay.size() >0  ){
	std::vector<int>::const_iterator irunNumber; int i =0;
	for( irunNumber =m_runNumbers.begin(); irunNumber!=m_runNumbers.end(); irunNumber++, i++){
	  m_runToDelay[*irunNumber] = m_runNumbersDelay[i]; 
	  info() << " run number = " <<  *irunNumber << " corresponds to delay " << m_runToDelay[*irunNumber] << endmsg;
	}
  }else{
	info() << " check yout option file for the delays and runs cause something seems wrong" << endmsg;
  }
  m_pedFile = TFile::Open(m_pedFileName.c_str());
  if(m_pedFile == 0) {
    error() << " impossible to open pedestal file" << endmsg;
	return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
/// Main execution
//=============================================================================
StatusCode Velo::PulseShapeFitterPedSub::execute() {
  counter( "# events" ) += 1;
  int finebin = 0;
  int currentRunNumber;
  double delay=0;
  std::vector<std::string>::const_iterator itL;
  //check middle sample for the tell1Data:
  const int midSampleIndex = (int)floor(m_samplingLocations.size()/2.);
  const int nSamples = m_samplingLocations.size();
  for(int isample = 0; isample < nSamples ; isample++){
	LHCb::VeloTELL1Datas *SampleData = veloTell1Data(m_samplingLocations[isample]);
	if( SampleData == 0 ) return StatusCode::SUCCESS;
  }
  LHCb::VeloTELL1Datas *midSampleData = veloTell1Data(m_samplingLocations[midSampleIndex]);
  LHCb::VeloTELL1Datas::const_iterator itD;
  //  pulse = new TH1D("pulse","pulse",nSamples*m_nSteps,-(nSamples/2 +0.5/(double)m_nSteps)*25.,(nSamples/2+0.5/(double)m_nSteps)*25.);
  //pulse = new TH1D("pulse","pulse",nSamples*50,-(nSamples/2 +0.1)*25.,(nSamples/2+0.01)*25.); //For fixed bins of 0.5 ns
  //For bins of 0.5 ns nSteps has to be ==50;
  if(m_stepSize >10000 ) m_nSteps = 50; 
  int nTimeBins = (nSamples+1)*m_nSteps+1;
  double lowerTimeEdge = (- 1.)*( (int)((nSamples+1)/2) + (0.5/(double)(m_nSteps)) )*25.;
  double upperTimeEdge = ( 1 + (int)((nSamples+1)/2) + (0.5/(double)(m_nSteps)) )*25.; // 1+ because of the central sample.
  
#ifndef WIN32
  pulse = new TH1D("pulse","pulse",nTimeBins,lowerTimeEdge,upperTimeEdge); //For fixed bins of 0.5 ns
#endif

  double (*fittest)(double *, double *) = NULL;
  fittest = &Velo::fitsimpleped;
  func = new TF1("dtfitfunc",fittest,-200.,200.,5);
  func->SetParameters(0,20,-10,m_width1,m_width2);
  func->SetParLimits(3,m_width1,m_width1);
  func->SetParLimits(4,m_width2,m_width2);
  func->FixParameter(3,m_width1);
  func->FixParameter(4,m_width2);
  func->SetParLimits(2, m_expectedPeakTime - m_timeWindow,m_expectedPeakTime + m_timeWindow);
  func->SetParLimits(0, -10,10);
  func->SetParLimits(1, 0,200);
  if(m_fixedOffset) func->FixParameter(0,0);

  char fitvalues_offset[150]; 
  char fitvalues_ampltiude[150]; 
  char fitvalues_peak[150];
  char finepulseshape[150];
  char fitpulseshape[150];
  char pulseshape[150];

  LHCb::ODIN *odin=0;
  if (m_runWithOdin){
	if(exist<LHCb::ODIN>((LHCb::ODINLocation::Default))) {
	  odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
	}else{
	  m_timeDecoder->getTime(); // in case one wants to get other kinds of info from odin.
	  odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
	}	
  }else{ m_runNumbers[0]=-1;}
  if(m_runNumbers[0]>0){
	currentRunNumber= odin->runNumber();
	delay = m_runToDelay[currentRunNumber];
	info() << " delay = " << delay  << " currentRunNumber " << currentRunNumber << endmsg;
  }
  int bunchCounter =  odin->bunchId();
  info() << " >>> bunch id: " <<  odin->bunchId() << endmsg;
  std::vector<int>::iterator ibunch;
  int isitright=0;
  if(m_chosenBunches.size() !=0){
	for(ibunch = m_chosenBunches.begin(); ibunch != m_chosenBunches.end(); ibunch++ ){
	  if( bunchCounter == (*ibunch) ) isitright =1;
	}
  }
  else  isitright =1;
  if(isitright==0){ 
    warning() << " This is not the  bunch id you were looking for " <<  odin->bunchId() << endmsg;
#ifndef WIN32
    delete pulse;   
#endif
    return StatusCode::SUCCESS;
  }

#ifndef WIN32
  for ( itD = midSampleData -> begin(); itD != midSampleData -> end(); ++itD ){
	LHCb::VeloTELL1Data* data = (*itD);  
	std::vector<LHCb::VeloTELL1Data* > allSamplesTell1Data;
	allSamplesTell1Data.resize(nSamples); 
	int TELL1_number=data->key();
	if(ExcludeTELL1(m_chosenTell1s,TELL1_number)&&m_chosenTell1s[0]!=-1)  continue;
	const DeVeloSensor* sens=0;
	sens=m_veloDet->sensorByTell1Id(TELL1_number);
	unsigned int sensNumber=0;
	sensNumber=sens->sensorNumber();
	if(sensNumber<64) m_sensorType=0; else m_sensorType=1;
	int count =0;
	for ( itL = m_samplingLocations.begin(); itL != m_samplingLocations.end(); ++itL, count++ ) {
	  LHCb::VeloTELL1Datas* tell1Datas = veloTell1Data( (*itL) );  
	  allSamplesTell1Data[count] = tell1Datas->object(TELL1_number);
	}
	std::vector<bool> signalDetected(2048,false);
	for(int iSample = 0; iSample<nSamples; iSample++){
	  for(int aLink=0; aLink<NumberOfALinks; ++aLink){
        if(TELL1_number == 5 && aLink == 41) {
          if (m_nEvents < 20) info() << " sensor 5 link 41, skipping " << endmsg;
          continue;
        }
		std::vector<int> linkADCs = getLink( (*allSamplesTell1Data[iSample])[aLink]);
		std::vector<double> linkPedSub = SubtractPedestal(linkADCs, TELL1_number, aLink,iSample);
		std::vector<double> linkCMS = SubtractFloatCommonMode(linkPedSub, m_cmsCut); 
		std::vector<double>::iterator itf;
		int channel =0;
		for(itf=linkCMS.begin();itf!=linkCMS.end() ; ++itf, channel++){
		  info() << "link adcs >> chan: " << channel << " raw: " << linkADCs[channel] 
                 << " pedsub: " << linkPedSub[channel] 
                 << " csm : " << linkCMS[channel] << endmsg;
		}
        channel = 0;
		for(itf=linkCMS.begin();itf!=linkCMS.end() ; ++itf, channel++){
		  int channelNumber = channel + 32*aLink;
		  double cmsadc = *itf; 
		  char chnumber[100]; sprintf(chnumber, "%04d", channelNumber );
		  char t1number[100]; sprintf(t1number, "%03d_sens_%03d", TELL1_number, sensNumber);
		  std::string TPshapeTitle;
		  if(((cmsadc) > m_threshold)&& signalDetected[channelNumber] == false){
        if(pulse != 0) pulse->Reset();
			//info() << " signal detected! channel "  << channelNumber << "channel " << channel  << " on sensor " << sensNumber << " Let's fit!" << endmsg; 
			//info() << " channel adc = " << cmsadc << endmsg;
			//double clusteradcs = GetClusterADC(channelNumber, allSamplesTell1Data[iSample]);
			//info() << " cluster adc? " << clusteradcs<< endmsg;
			signalDetected[channelNumber] = true;
			sprintf(pulseshape,"pulse_Tell1%03d_ch%04d_timebin%02d_peak_sens%d",TELL1_number,channelNumber,finebin, sensNumber);
			for(int jSample =0; jSample<nSamples; jSample++){
			  double time =0; 
			  if(m_stepSize >10000 ) time =  25*(jSample - (int)(nSamples/2) ) + delay ;
			  else time =  25*(jSample - (int)(nSamples/2) ) + delay + (m_nEvents/m_stepSize)*(25./((double) m_nSteps));
			  std::vector<int> templinkADCs = getLink( (*allSamplesTell1Data[jSample])[aLink]);
			  std::vector<double> templinkPedSub = SubtractPedestal(templinkADCs, TELL1_number, aLink,jSample);
			  std::vector<double> templinkCMS = SubtractFloatCommonMode(templinkPedSub, m_cmsCut);
              int xbin = pulse->FindBin(time);
			  if(m_tryCluster==true){
				double clustersum = GetClusterADC(channelNumber, allSamplesTell1Data[jSample]);
				pulse->SetBinContent(xbin, clustersum);
			  }
			  else if(m_tryCluster==false){
				pulse->SetBinContent(xbin, templinkCMS[channel]);
				//pulse->Fill(time,templinkCMS[channel]);
				//info ()  <<  " time "  << time << " adc val " << templinkCMS[channel] << " run number " << currentRunNumber << " delay "  << delay << endmsg;
			  }
              //info() << " time??? " << time << endmsg; 
			  //sprintf(finepulseshape,"pulseshapeCluster_Tell1%03d_ch%04d_%d",TELL1_number,channelNumber,sensNumber);
			}
			debug()<<"Pulse histogram filled"<<endmsg;
			double amplitude = pulse->GetMaximum();
            
			double peaktime = pulse->GetXaxis()->GetBinCenter(pulse->GetMaximumBin());
			double integral = (double) pulse->Integral(); 
            //info() << " amplitude = " << amplitude << " peak = " << peaktime << " integral = " << integral << endmsg;
			func->SetParameter(0,0);
			func->SetParameter(1,amplitude);
			func->SetParameter(2,peaktime);
			if(m_useLogLikeFit) pulse->Fit("dtfitfunc","LMQNW");
			//else if(m_useEqualWeights) pulse->Fit("dtfitfunc","QNMWW");
			//else pulse->Fit("dtfitfunc","QNMWWE");
			else pulse->Fit("dtfitfunc","QNW");
			double chisqperndof = func->GetChisquare()/((double) func->GetNDF());
			double fitoffset = func->GetParameter(0);
			double fitamplitude = func->GetParameter(1);
			double fitpeaktime = func->GetParameter(2);
			debug()<<"Pulse histogram fitted"<<endmsg;

			if( (integral > m_integralCut) &&  (fabs(peaktime - m_expectedPeakTime )< m_timeWindow) && (amplitude > m_threshold) && (amplitude < m_amplitudeCut) ){
			  if(m_plotSeparateStrips) sprintf(finepulseshape,"ClustersVsTime_%d_ch%04d",sensNumber,channelNumber);
			  else sprintf(finepulseshape,"ClustersVsTime_%d",sensNumber);
			  std::string ffinepulseshape = finepulseshape;
			  debug()<<"Beginning filling of profiles"<<endmsg;

			  for(int isample =0; isample <nSamples; isample++){
				double time =0; 
				if(m_stepSize >10000 ) time =  25*(isample - (int)(nSamples/2) ) + delay ;
				else  time = 25*(isample - (int)(nSamples/2) ) + delay + (m_nEvents/m_stepSize)*(25./((double) m_nSteps));			    
				int binnumber = pulse->FindBin(time);
				double adcvalue = pulse->GetBinContent(binnumber);
				plot2D(time,adcvalue,ffinepulseshape,ffinepulseshape,
					lowerTimeEdge,upperTimeEdge, -127.5, 128.5, nTimeBins, 256);
				//profile1D(time,adcvalue, (ffinepulseshape+"prof"),
				//	lowerTimeEdge, upperTimeEdge, nTimeBins);	
				profile1D(time,adcvalue,ffinepulseshape+"prof",
					lowerTimeEdge, upperTimeEdge, nTimeBins, "s");	
				//info()<<" time  " << time<<" adc  "<< adcvalue<< " name "<< ffinepulseshape<< " ntime bins " << nTimeBins << endmsg;
                
                //info() << " time  << " << time << " ADC cms << " << adcvalue << " nbins << " << nTimeBins << endmsg; 
				//      -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25,  50*(nSamples+2));			
				//info ()  <<  " bin "  << binnumber << " wrong? " << pulse->GetBinContent(binnumber) << " adc val  " << adcvalue << " delay "  << delay << endmsg;
				//warning()<<sensNumber<<"  "<<channelNumber<<"  "<<chnumber<<endmsg;
				/*
				   if(m_plotSeparateStrips){
				   sprintf(finepulseshape,"ClustersVsTime_%d_ch%04d",sensNumber,channelNumber);
				   ffinepulseshape = finepulseshape;
				   plot2D(time,adcvalue,ffinepulseshape,ffinepulseshape,
				   -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25, -127.5, 128.5, 50*(nSamples+2), 256);			
				   profile1D(time,adcvalue,ffinepulseshape+"prof",ffinepulseshape+"prof",
				   -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25,  50*(nSamples+2));	
				   }
				 */
			  }
			}   
			// conditions to be satisfied
			if(  (fitamplitude >m_threshold)           &&  
				(fitamplitude < m_amplitudeCut)      && 
				(fabs(fitpeaktime- m_expectedPeakTime) < m_timeWindow)   &&
				(integral   >   m_integralCut)       &&
				(fabs(fitoffset)< m_offsetCut)  ) {
			  if( chisqperndof< m_chisqCut ) {
				//info()<<"Channel: "<<channelNumber<<" ("<<channelNumber%32<<")  Chi^2/ndf: "<< chisqperndof
				  //<<" Offset: "<< fitoffset <<" +- "<<func->GetParError(0) << " Amplitude: "<< fitamplitude <<" +- "<<func->GetParError(1)
				 // <<"  Peak: "<< fitpeaktime <<" +- "<<func->GetParError(2) << endmsg;
				//info() << " pulse integral = "  << integral << endmsg;

				//char chi2plotname[200];
				/*
				   if(m_plotSeparateStrips){
				   sprintf(fitvalues_offset,"fit_Tell1%03d_ch%04d_timebin%02d_offset_sens%d", TELL1_number, channelNumber, finebin, sensNumber);
				   plot1D(fitoffset, fitvalues_offset, fitvalues_offset,-15.5,15.5,31);	    
				   sprintf(fitvalues_ampltiude,"fit_Tell1%03d_ch%04d_timebin%02d_amplitude_sens%d", TELL1_number, channelNumber, finebin, sensNumber);
				   plot1D(fitamplitude, fitvalues_ampltiude, fitvalues_ampltiude,-60.5,60.5,121);
				   sprintf(fitvalues_peak,"fit_Tell1%03d_ch%04d_timebin%02d_peak_sens%d", TELL1_number, channelNumber, finebin, sensNumber);
				   plot1D(fitpeaktime, fitvalues_peak, fitvalues_peak,-200.25,200.25,801);
				   sprintf(chi2plotname,"Chisq_Tell1_%03d_sens_%d",TELL1_number,sensNumber);	
				   plot1D(chisqperndof, chi2plotname, chi2plotname, -1.5,100.5, 102);
				   sprintf(fitvalues_peak,"Weightedfit_Tell1%03d_ch%04d_timebin%02d_peak_sens%d",TELL1_number, channelNumber, finebin, sensNumber);
				   plot1D(fitpeaktime, fitvalues_peak, fitvalues_peak,-200.25,200.25,801, 1.0/func->GetParError(2));
				   }*/
				//sprintf(fitvalues_peak,"fit_Tell1%03d_peak_sens%d", TELL1_number, sensNumber);
				sprintf(fitvalues_peak,  "fit_peak_sens%d",  sensNumber);
				plot1D(fitpeaktime, fitvalues_peak, fitvalues_peak,lowerTimeEdge,upperTimeEdge,m_nTimeBins);	
				// sprintf(fitvalues_peak,"Weightedfit_Tell1%03d_peak_sens%d",  TELL1_number,  sensNumber);
				sprintf(fitvalues_peak,"Weightedfit_peak_sens%d",   sensNumber);
				plot1D(fitpeaktime, fitvalues_peak, fitvalues_peak,lowerTimeEdge,upperTimeEdge,m_nTimeBins, 1.0/func->GetParError(2));
				sprintf(fitvalues_peak,"WeightedSqrdfit_peak_sens%d",   sensNumber);
				plot1D(fitpeaktime, fitvalues_peak, fitvalues_peak,lowerTimeEdge,upperTimeEdge,m_nTimeBins, 1.0/func->GetParError(2)/func->GetParError(2));
				sprintf(fitvalues_peak,"fit_peak_sens%d_delay%d",  sensNumber, (int) delay );
				plot1D(fitpeaktime, fitvalues_peak, fitvalues_peak,lowerTimeEdge,upperTimeEdge,m_nTimeBins);	
				// sprintf(fitvalues_peak,"Weightedfit_Tell1%03d_peak_sens%d",  TELL1_number,  sensNumber);
				sprintf(fitvalues_peak,"Weightedfit_peak_sens%d_delay%d",   sensNumber, (int) delay);
				plot1D(fitpeaktime, fitvalues_peak, fitvalues_peak,lowerTimeEdge,upperTimeEdge,m_nTimeBins, 1.0/func->GetParError(2));
				//sprintf(fitvalues_peak,"Chi2Weightedfit_Tell1%03d_peak_sens%d", TELL1_number, sensNumber);
				sprintf(fitvalues_peak,"Chi2Weightedfit_peak_sens%d",  sensNumber);
				plot1D(fitpeaktime, fitvalues_peak, fitvalues_peak,lowerTimeEdge,upperTimeEdge,m_nTimeBins, 1.0/chisqperndof);
				sprintf(fitvalues_offset,"fit_offset_sens%d", sensNumber);
				plot1D(fitoffset, fitvalues_offset, fitvalues_offset,-15.5,15.5,31);	    
				sprintf(fitvalues_ampltiude,"fit_amplitude_sens%d", sensNumber);
				plot1D(fitamplitude, fitvalues_ampltiude, fitvalues_ampltiude,0,300,m_nTimeBins);
                
				sprintf(fitpulseshape,"FittedClustersVsTime_%d",sensNumber);
				std::string ffitpulseshape =  fitpulseshape;
				for(int isample =0; isample <nSamples; isample++){
				  //	  double time = 25*(isample - (int) nSamples/2)+delay+m_nEvents/m_stepSize;
				  double time =0; 
				  if(m_stepSize >10000 ) time =  25*(isample - (int)(nSamples/2) ) + delay ;
				  else time =  25*(isample - (int)(nSamples/2) ) + delay + (m_nEvents/m_stepSize)*(25./((double) m_nSteps));			    

				  int binnumber = pulse->FindBin(time);
				  double adcvalue = pulse->GetBinContent(binnumber);
				  plot2D(time,adcvalue,ffitpulseshape,ffitpulseshape,
					  lowerTimeEdge,upperTimeEdge, -127.5, 128.5, nTimeBins, 256);	
				  //	  -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25, -127.5, 128.5, 50*(nSamples+2), 256);			
				  profile1D(time,adcvalue,ffitpulseshape+"prof",
					  lowerTimeEdge, upperTimeEdge, nTimeBins, "s");	
				  //	  -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25,  50*(nSamples+2));			
				}

			  }		   
			}
			//sprintf(finepulseshape,"pulseshapeCluster_Tell1%03d_ch%04d",TELL1_number,channelNumber);
		  }
		  pulse->Reset();
		}
	  }
	}
  }  
  debug()<<"Event done"<<endmsg;
  
  delete pulse;
#endif

  delete func; 

  m_nEvents++;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::PulseShapeFitterPedSub::finalize() {
  if ( m_debugLevel ) debug() << "==> Finalize" << endmsg;

  return VeloMonitorBase::finalize(); // must be called after all other actions

}

//=============================================================================
// Retrieve the VeloTell1Data
//=============================================================================
LHCb::VeloTELL1Datas*
Velo::PulseShapeFitterPedSub::veloTell1Data( std::string samplingLocation ) {
  std::string tesPath;
  if(samplingLocation == "") tesPath = samplingLocation + "Raw/Velo/DecodedADC"; 
  else  tesPath = samplingLocation + "/Raw/Velo/DecodedADC";
  size_t pos = tesPath.find( "//" );
  if ( pos != std::string::npos )    tesPath.replace( pos, 2, "/" );
  if ( m_debugLevel ) 	debug() << "Retrieving VeloTell1Data from " << tesPath << endmsg;
  if ( !exist<LHCb::VeloTELL1Datas>( tesPath ) ) {
	debug() << "No VeloTell1Data container found for this event !" << endmsg; 
	if (m_nEvents %100==0  )  info() << "No VeloTell1Data container found for this event !   " << tesPath << endmsg; 
	return 0;
  }
  else {
	LHCb::VeloTELL1Datas* tell1Data = get<LHCb::VeloTELL1Datas>( tesPath );
	if ( m_debugLevel ) debug() << "  -> number of VeloTell1Data found in TES: "
	  << tell1Data->size() <<endmsg;
	return tell1Data;
  }
}
//=============================================================================
double Velo::fitsimpleped(double *x, double *par){
  double fitval;
  if(x[0]<par[2]) fitval = par[0]+par[1]*(exp(-((x[0]-par[2])/par[3])*((x[0]-par[2])/par[3])));
  else fitval = par[0]+par[1]*(exp(-((x[0]-par[2])/par[4])*((x[0]-par[2])/par[4])));
  return fitval;
}

double Velo::PulseShapeFitterPedSub::subtractCommonMode(int channelNumber, LHCb::VeloTELL1Data *tell1data){
  // identifying which link
  ALinkPair begEnd;
  int aLink = (int)(channelNumber/32.);
  //info() << " channelNumber "  << channelNumber << " alink = " << aLink << endmsg; 
  begEnd=(*tell1data)[aLink];
  scdatIt iT;
  double commonmode=0;
  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
	commonmode += *iT;  
  }
  commonmode /= 32.0;
  int count = 0;
  //info()<< " common mode " << commonmode<< endmsg;
  double commonmodewithoutsignal= 0.0;
  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
	double cmsadc = *iT - commonmode; 
	//info()<< "cmsadc >>>>>>>>>>> " << cmsadc << endmsg;
	if(fabs(cmsadc) < m_cmsCut){ 
	  commonmodewithoutsignal += *iT;
	  count++;
	}
  }
  commonmodewithoutsignal /= float(count+0.00001);
  //info()<< " common mode without signal " << commonmodewithoutsignal << " count = "  << count << endmsg;
  double cmsadc = tell1data->channelADC(channelNumber) - commonmodewithoutsignal;
  //double cmsadcwithsignal = tell1data->channelADC(channelNumber) - commonmode;
  //char cmsplotname[100]; sprintf(cmsplotname, "cmsplot_%02d_Tell1_%02d",aLink, tell1data->key());
  //plot1D(cmsadc,cmsplotname,cmsplotname, -100,100,201);
  //info()<< " cms adc with signal " << cmsadcwithsignal << endmsg;
  return cmsadc;
}
std::vector<double>  Velo::PulseShapeFitterPedSub::SubtractCommonMode( std::vector<int> data, double cut = 15.0)
{
  int howManyPasses =4;
  std::vector<double> commonModeSubtractedData;
  std::vector<int>::iterator it;
  std::vector<double>::iterator itf;
  double average = 0;
  int count = 0, chanWithoutHits = 0;  double averageWithoutHits = 0;
  for(it=data.begin();it!=data.end() ; ++it){
	average += (double)(*it);
  }
  average /= (double)(data.size());
  for(it=data.begin();it!=data.end() ; it++){
	commonModeSubtractedData.push_back((double)(*it) - average);
  }
  int pass=0;
  while(pass < howManyPasses){
	chanWithoutHits=0; averageWithoutHits =0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf){
	  if(fabs(*itf) < cut ){
		averageWithoutHits += *itf;
		chanWithoutHits++;
	  }
	}
	if(chanWithoutHits > 0) averageWithoutHits /= (double)(chanWithoutHits);
	else averageWithoutHits = 0;
	count = 0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf, count++){
	  commonModeSubtractedData[count]  = *itf - averageWithoutHits;
      if( commonModeSubtractedData[count] > 1023) {
		warning() << "something very wrong is going on!! cmsadc == " 
		  << commonModeSubtractedData[count] << " data size = " << data.size()  << endmsg;
        warning() <<  "original data == " << data[count] <<  " chan without hits ==  " << chanWithoutHits <<   endmsg;
      }
	}
	pass++;
  }
  return commonModeSubtractedData;
}
double Velo::PulseShapeFitterPedSub::GetClusterADC( int seedChannel, LHCb::VeloTELL1Data *tell1data){
  if(seedChannel < 0 || seedChannel >2047) {
	info() << "channel does not exist! " << endmsg;
  }
  double clusterADC = 0;
  int stripNumber = m_stripFromChannel[m_sensorType][seedChannel];
  for(int ineighbour = -m_takeNeighbours; ineighbour <= m_takeNeighbours; ineighbour++ ){ 
	if(stripNumber+ineighbour >= 0 && stripNumber +ineighbour  <2048){
	  int currentstrip =  stripNumber+ineighbour;
	  int currentchannel = m_channelFromStrip[m_sensorType][currentstrip]; 
	  int channelnumber = (int)(currentchannel%32);
	  int aLink = (int)(currentchannel/32.);
	  std::vector<int> linkADCs = getLink( (*tell1data)[aLink]);

	  std::vector<double> linkCMS = SubtractCommonMode(linkADCs, m_cmsCut); 
	  double channeladc = linkCMS[channelnumber];
	  if(ineighbour ==0 ) debug() << "seedADC == " <<  channeladc <<  endmsg ;  
	  clusterADC += channeladc; 
	} 
  }
  // info() << " total cluster adc = " << clusterADC << endmsg;
  return clusterADC;
}

std::vector<double>  Velo::PulseShapeFitterPedSub::SubtractPedestal(std::vector<int> data, 
                                                                    int tell1number, 
                                                                    int linknumber, 
                                                                    int sample)
{
  std::vector<double> pedestalSubtractedData;
#ifndef WIN32
  std::vector<int>::iterator it;
  std::vector<double>::iterator itf;
  //double average = 0;
  int chanOffset = 32*linknumber;
  //get pedestals 
  // 
  char histoname[200];
  sprintf(histoname, "%s/hpedestals_sens_%d_sample%01d",m_pedDirectory.c_str(), tell1number, sample);
  TProfile *hpedtmp = (TProfile*) m_pedFile->Get(histoname);
  if(hpedtmp ==0 ){
     info() << " cant find the hsitogram named " << histoname <<" returning raw data instead... " << endmsg;
     
     int count =0;
	 for(it=data.begin();it!=data.end() ; ++it, count++){
	   pedestalSubtractedData.push_back(*it);
     }
     return pedestalSubtractedData;
  }
  int count = 0; // chanWithoutHits = 0;  double averageWithoutHits = 0;
  for(it=data.begin();it!=data.end() ; ++it, count++){
    double ped = hpedtmp->GetBinContent(chanOffset+count+1);
    double pedsubadc = *it - ped; 
    pedestalSubtractedData.push_back(pedsubadc);   
  }
#endif
  return pedestalSubtractedData;
}

std::vector<double>  Velo::PulseShapeFitterPedSub::SubtractFloatCommonMode( std::vector<double> data, double cut = 15.0)
{
  int howManyPasses =4;
  std::vector<double> commonModeSubtractedData;
  std::vector<double>::iterator it;
  std::vector<double>::iterator itf;
  double average = 0;
  int count = 0, chanWithoutHits = 0;  double averageWithoutHits = 0;
  for(it=data.begin();it!=data.end() ; ++it){
	average += (double)(*it);
  }
  average /= (double)(data.size());
  for(it=data.begin();it!=data.end() ; it++){
	commonModeSubtractedData.push_back((double)(*it) - average);
  }
  int pass=0;
  while(pass < howManyPasses){
	chanWithoutHits=0; averageWithoutHits =0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf){
	  if(fabs(*itf) < cut ){
		averageWithoutHits += *itf;
		chanWithoutHits++;
	  }
	}
	if(chanWithoutHits > 0) averageWithoutHits /= (double)(chanWithoutHits);
	else averageWithoutHits = 0;
	count = 0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf, count++){
	  commonModeSubtractedData[count]  = *itf - averageWithoutHits;
      if( commonModeSubtractedData[count] > 1023) {
		warning() << "something very wrong is going on!! cmsadc == " 
		  << commonModeSubtractedData[count] << " data size = " << data.size()  << endmsg;
        warning() <<  "original data == " << data[count] <<  " chan without hits ==  " << chanWithoutHits <<   endmsg;
      }
	}
	pass++;
  }
  return commonModeSubtractedData;
}
