// $Id: ConsecutivePedestalCalculator.cpp,v 1.4 2010-03-18 14:22:35 kakiba Exp $
// Include files
// -------------
#include <iostream>
#include <fstream>
// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
// from Tell1Kernel
#include "Tell1Kernel/VeloDecodeConf.h" 
#include "Event/ODIN.h"
#include "Event/VeloODINBank.h"
// local
#include "ConsecutivePedestalCalculator.h"
#include "CommonFunctions.h"
// from Boost
#include <boost/assign/list_of.hpp>
#include "boost/lexical_cast.hpp"
// from Gaudi
#include "AIDA/IHistogram2D.h"
// local
// Kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"

#include "Math/ParamFunctor.h"

#include "GaudiKernel/IEventTimeDecoder.h"  
#include "Event/ODIN.h"

#include <TH1D.h>


using namespace VeloTELL1;

//-----------------------------------------------------------------------------
// Implementation file for class : ConsecutivePedestalCalculator
//
//-----------------------------------------------------------------------------
// Declaration of the Algorithm Factory
namespace Velo {
  DECLARE_ALGORITHM_FACTORY( ConsecutivePedestalCalculator )
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  Velo::ConsecutivePedestalCalculator::ConsecutivePedestalCalculator( const std::string& name, ISvcLocator* pSvcLocator)
  : Velo::VeloMonitorBase ( name , pSvcLocator )
,m_nEvents(0)
{
  std::vector<std::string> tmpLocations = 
	boost::assign::list_of("Prev7")("Prev6")("Prev5")("Prev4")("Prev3")("Prev2")("Prev1")
	("")("Next1")("Next2")("Next3")("Next4")("Next5")("Next6")("Next7");
  declareProperty( "SamplingLocations", m_samplingLocations = tmpLocations );
  declareProperty( "NSteps", m_nSteps= 1 );
  declareProperty( "StepSize", m_stepSize= 100 );
  declareProperty( "ChisqCut", m_chisqCut= 10 );
  declareProperty( "CMSCut", m_cmsCut= 10 );
  declareProperty( "MaxAmplitudeCut", m_amplitudeCut= 300);
  declareProperty( "OffsetCut", m_offsetCut= 10 );
  declareProperty( "IntegralCut", m_integralCut= 10 );
  declareProperty( "UseLogLikeFit", m_useLogLikeFit= true );
  declareProperty( "TimeWindow", m_timeWindow= 150 );
  declareProperty( "ExpectedPeakTime", m_expectedPeakTime= 0 );
  declareProperty( "NTimeBins", m_nTimeBins = 201 );
  declareProperty( "FixOffset", m_fixedOffset= false );
  declareProperty( "TryToClusterize", m_tryCluster= false );
  declareProperty( "TELL1s",m_chosenTell1s     );
  declareProperty( "SignalThreshold" , m_threshold=20);
  declareProperty( "TakeNeighbours", m_takeNeighbours=1 );
  declareProperty( "PlotSeparateStrips", m_plotSeparateStrips=false );
  declareProperty( "Offset" ,m_offset=0);
  declareProperty( "WidthBefore", m_width1=13.68);
  declareProperty( "WidthAfter", m_width2=26.76);
  declareProperty( "DetailedOutput" , m_detailedOutput=true);
  declareProperty( "RunWithOdin" , m_runWithOdin = true ); // 
  declareProperty( "RunNumbers", m_runNumbers );
  declareProperty( "RunNumbersDelay", m_runNumbersDelay );
  declareProperty( "ChosenBunchCounters", m_chosenBunches );
  declareProperty( "RunWithChosenBunchCounters", m_useBCNT = true );
  declareProperty( "MakeCMSPedestals", m_plotCmsPed = 1 );
  m_runNumbers.push_back(-1);
  m_runNumbersDelay.push_back(0);


}
//=============================================================================
// Destructor
//=============================================================================
Velo::ConsecutivePedestalCalculator::~ConsecutivePedestalCalculator() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::ConsecutivePedestalCalculator::initialize() {
  StatusCode sc = VeloMonitorBase::initialize(); // must be executed first
  printProps();
  m_timeDecoder = tool<IEventTimeDecoder>( "OdinTimeDecoder" );
  if ( sc.isFailure() ) return sc;
  m_velo = getDet<DeVelo>( DeVeloLocation::Default );  
  //const int strips=2048;
  return StatusCode::SUCCESS;
}

//=============================================================================
/// Main execution
//=============================================================================
StatusCode Velo::ConsecutivePedestalCalculator::execute() {
  counter( "# events" ) += 1;
  //int finebin = 0;
  int currentRunNumber;
  double delay=0;
  std::vector<std::string>::const_iterator itL;
  const int midSampleIndex = (int)floor(m_samplingLocations.size()/2.);
  const int nSamples = m_samplingLocations.size();
  for(int isample = 0; isample < nSamples ; isample++){
	LHCb::VeloTELL1Datas *SampleData = veloTell1Data(m_samplingLocations[isample]);
	if( SampleData == 0 ) return StatusCode::SUCCESS;
  }
  LHCb::VeloTELL1Datas *midSampleData = veloTell1Data(m_samplingLocations[midSampleIndex]);
  LHCb::VeloTELL1Datas::const_iterator itD;

#ifndef WIN32
  TH1D *hpulse = new TH1D("hpulse","hpulse",nSamples,-(nSamples/2 +0.5)*25.,(nSamples/2+0.5)); 
#endif

  LHCb::ODIN *odin =0;
  if (m_runWithOdin){
    if(exist<LHCb::ODIN>((LHCb::ODINLocation::Default))) {
      odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
    }
    else{
      m_timeDecoder->getTime(); 
      odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
    }	
  }else{ m_runNumbers[0]=-1;}
  if(m_runNumbers[0]>0){
    currentRunNumber= odin->runNumber();
    delay = m_runToDelay[currentRunNumber];
    info() << " delay = " << delay  << " currentRunNumber " << currentRunNumber << endmsg;
  }
  int bunchCounter =  odin->bunchId();
  info() << " >>> bunch id: " <<  odin->bunchId() << endmsg;
  std::vector<int>::iterator ibunch;
  int isitright=0;
  if(m_chosenBunches.size() !=0){
    for(ibunch = m_chosenBunches.begin(); ibunch != m_chosenBunches.end(); ibunch++ ){
      if( bunchCounter == (*ibunch) ) isitright =1;
    }
  }
  else  isitright =1;
  if(isitright==0){ 
    warning() << " This is not the  bunch id you were looking for " <<  odin->bunchId() << endmsg;
#ifndef WIN32
    delete hpulse;
#endif
    return StatusCode::SUCCESS;
  }
  for ( itD = midSampleData -> begin(); itD != midSampleData -> end(); ++itD ) {
	LHCb::VeloTELL1Data* data = (*itD);  
	int TELL1_number=data->key(); if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue;
	std::vector<LHCb::VeloTELL1Data* > allSamplesTell1Data;
	allSamplesTell1Data.resize(nSamples); 
	const DeVeloSensor* sens=0;
	sens=m_veloDet->sensorByTell1Id(TELL1_number);
	unsigned int sensNumber=0;
	sensNumber=sens->sensorNumber();
	if(sensNumber<64) m_sensorType=0; else m_sensorType=1;
	int count =0;
	for ( itL = m_samplingLocations.begin(); itL != m_samplingLocations.end(); ++itL, count++ ) {
	  LHCb::VeloTELL1Datas* tell1Datas = veloTell1Data( (*itL) );  
	  allSamplesTell1Data[count] = tell1Datas->object(TELL1_number);
	}
	std::vector<bool> signalDetected(2048,false);
#ifndef WIN32
	for(int iSample = 0; iSample<nSamples; iSample++){
	  for(int aLink=0; aLink<NumberOfALinks; ++aLink){
      std::vector<int> linkADCs = getLink( (*allSamplesTell1Data[iSample])[aLink]);
      std::vector<double> linkCMS = SubtractCommonMode(linkADCs, m_cmsCut); 
      std::vector<double>::iterator itf;
      int channel =0;
      for(itf=linkCMS.begin();itf!=linkCMS.end() ; ++itf, channel++){
        int channelNumber = channel + 32*aLink;
        double cmsadc = *itf;
        std::string TPshapeTitle;
        if(((cmsadc) > m_threshold)&& signalDetected[channelNumber] == false){
          for(int jSample =0; jSample<nSamples; jSample++){
            double time = 25*(jSample - (int) nSamples/2); 
            std::vector<int> templinkADCs = getLink( (*allSamplesTell1Data[jSample])[aLink]);
            std::vector<double> templinkCMS = SubtractCommonMode(templinkADCs, m_cmsCut);
            hpulse->Fill(time,templinkCMS[channel]);
          }
        }
        double amplitude = hpulse->GetMaximum();
        double peaktime = hpulse->GetXaxis()->GetBinCenter(hpulse->GetMaximumBin());
        double integral = (double) hpulse->Integral(); 
        if(integral > m_integralCut &&  fabs(peaktime) < m_timeWindow && amplitude > m_threshold){
          signalDetected[channelNumber] == true;
          debug()<< signalDetected[channelNumber] <<endmsg;
        }
      }   
	  }
	}
#endif
	for(int jSample=0; jSample<nSamples; jSample++){
	  char plotpedestalname[200]; 
	  sprintf(plotpedestalname,"hpedestals_sens_%03d_sample%01d", sensNumber, jSample);
	  char cmspedestalname[200]; 
	  sprintf(cmspedestalname,"hcmspedestals_sens_%03d_sample%01d", sensNumber, jSample);
	  for(int aLink=0; aLink<NumberOfALinks; ++aLink){
      std::vector<int> templinkADCs = getLink( (*allSamplesTell1Data[jSample])[aLink]);
      int ichannel=0; 
      for(ichannel=0;ichannel< 32; ichannel++){
        profile1D(ichannel+aLink*32, templinkADCs[ichannel],plotpedestalname,plotpedestalname, -0.5,2047.5, 2048);
      }
      if(m_plotCmsPed){ 
        int kchannel=0; 
        std::vector<double> linkCMS = SubtractCommonMode(templinkADCs, m_cmsCut); 
        for(kchannel=0;kchannel< 32; kchannel++){
          profile1D(kchannel+aLink*32, linkCMS[kchannel],cmspedestalname,cmspedestalname, -0.5,2047.5, 2048);
        }
      }
#ifndef WIN32
      hpulse->Reset();
#endif
	  }
	}
  }
#ifndef WIN32
  delete hpulse;
#endif
  
  m_nEvents++;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::ConsecutivePedestalCalculator::finalize() {
  if ( m_debugLevel ) debug() << "==> Finalize" << endmsg;
  return VeloMonitorBase::finalize(); // must be called after all other actions
}
//=============================================================================
// Retrieve the VeloTell1Data
//=============================================================================
LHCb::VeloTELL1Datas*
Velo::ConsecutivePedestalCalculator::veloTell1Data( std::string samplingLocation ) {
  std::string tesPath;
  if(samplingLocation == "") tesPath = samplingLocation + "Raw/Velo/DecodedADC"; 
  else  tesPath = samplingLocation + "/Raw/Velo/DecodedADC";
  size_t pos = tesPath.find( "//" );
  if ( pos != std::string::npos )    tesPath.replace( pos, 2, "/" );
  if ( m_debugLevel ) 	debug() << "Retrieving VeloTell1Data from " << tesPath << endmsg;
  if ( !exist<LHCb::VeloTELL1Datas>( tesPath ) ) {
	debug() << "No VeloTell1Data container found for this event !" << endmsg; 
	if (m_nEvents %100==0  )  info() << "No VeloTell1Data container found for this event !   " << tesPath << endmsg; 
	return 0;
  }
  else {
	LHCb::VeloTELL1Datas* tell1Data = get<LHCb::VeloTELL1Datas>( tesPath );
	if ( m_debugLevel ) debug() << "  -> number of VeloTell1Data found in TES: "
	  << tell1Data->size() <<endmsg;
	return tell1Data;
  }
}
//=============================================================================
std::vector<double>  Velo::ConsecutivePedestalCalculator::SubtractCommonMode( std::vector<int> data, double cut = 15.0)
{
  int howManyPasses =3;
  std::vector<double> commonModeSubtractedData;
  std::vector<int>::iterator it;
  std::vector<double>::iterator itf;
  double average = 0;
  int count = 0, chanWithoutHits = 0;  double averageWithoutHits = 0;
  for(it=data.begin();it!=data.end() ; ++it){
	average += float(*it);
  }
  average /= float(data.size());
  for(it=data.begin();it!=data.end() ; it++){
	commonModeSubtractedData.push_back((double)(*it) - average);
  }
  int pass=0;
  while(pass < howManyPasses){
	chanWithoutHits=0; averageWithoutHits =0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf){
	  if(fabs(*itf) < cut )  {
		averageWithoutHits += *itf;
		chanWithoutHits++;
	  }
	}
	if(chanWithoutHits > 0) averageWithoutHits /= double(chanWithoutHits);
	else averageWithoutHits = 0;
	count = 0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf, count++){
	  commonModeSubtractedData[count]  = *itf - averageWithoutHits;
	}
	pass++;
  }
  return commonModeSubtractedData;
}
double Velo::ConsecutivePedestalCalculator::GetClusterADC( int seedChannel, LHCb::VeloTELL1Data *tell1data){
  if(seedChannel < 0 || seedChannel >2047) 	info() << "channel does not exist! " << endmsg;
  double clusterADC = 0;
  int stripNumber = m_stripFromChannel[m_sensorType][seedChannel];
  for(int ineighbour = -m_takeNeighbours; ineighbour <= m_takeNeighbours; ineighbour++ ){ 
	if(stripNumber+ineighbour >= 0 && stripNumber +ineighbour  <2048){
	  int currentstrip =  stripNumber+ineighbour;
	  int currentchannel = m_channelFromStrip[m_sensorType][currentstrip]; 
	  int channelnumber = (int)(currentchannel%32);
	  int aLink = (int)(currentchannel/32.);
	  std::vector<int> linkADCs = getLink( (*tell1data)[aLink]);
	  std::vector<double> linkCMS = SubtractCommonMode(linkADCs, m_cmsCut); 
	  double channeladc = linkCMS[channelnumber];
	  if(ineighbour ==0 ) debug() << "seedADC == " <<  channeladc <<  endmsg ;  
	  clusterADC += channeladc; 
	} 
  }
  return clusterADC;
}














