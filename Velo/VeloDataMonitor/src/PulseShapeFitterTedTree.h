// $Id$
#ifndef VELODATAMONITOR_PULSESHAPEFITTERTEDTREE 
#define VELODATAMONITOR_PULSESHAPEFITTERTEDTREE 1

// Include files
// -------------
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

// from VeloEvent
#include "VeloEvent/VeloTELL1Data.h"

// from DigiEvent
#include "Event/VeloCluster.h"

// local
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/AlgFactory.h" 

#include <TH1D.h>
#include <TF1.h>

#include "Math/ParamFunctor.h"
#include "GaudiKernel/IEventTimeDecoder.h"            // Interface for odin

#include "Event/ODIN.h"

#include "Event/RawEvent.h"
#include "DetDesc/Condition.h"

/** @class PulseShapeFitterTedTree PulseShapeFitterTedTree.h
 *  
 *
 *  @author Veerle Heijne
 *  @date   2011-08-25
 */

namespace Velo {

  double fitsimple(double *x, double *par);
  //class PulseShapeFitterTedTree : public VeloMonitorBase {
  class PulseShapeFitterTedTree : public GaudiTupleAlg {

  public: 
    /// Standard constructor
    PulseShapeFitterTedTree( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~PulseShapeFitterTedTree( );    ///< Destructor
    virtual StatusCode initialize() override;    ///< Algorithm initialization
    virtual StatusCode execute   () override;    ///< Algorithm execution
    virtual StatusCode finalize  () override;    ///< Algorithm finalization
    long   m_nEvents;
    double m_threshold;
    double m_offset;
    double m_width1;
    double m_width2;
    double m_chisqCut;
    double m_cmsCut;
    double m_integralCut;
    double m_timeWindow;
    double m_expectedPeakTime;
    double m_nTimeBins;
    double m_amplitudeCut;
    double m_offsetCut;
    double m_thresholdForCMS;
    bool   m_skipStepZero;
    bool   m_onlyStepZero;
    bool   m_runWithOdin; 
    bool   m_fixedOffset;
    bool   m_tryCluster;
    bool   m_plotSeparateStrips;
    bool   m_removeHeaders;
    bool   m_useBCNT;
    bool   m_useLogLikeFit;
    bool   m_selectBunchCrossing;
    int    m_nEventsPerStep;
    int    m_currentCalStep;
   //int   m_nSteps;
    int    m_shift;
    //int   m_stepSize;
    int    m_takeNeighbours;
    int    m_wrapPoint;
    int    m_wrappedSteps;
    int    tell1smatch[5],tell1sorder[88];
    int    m_stripFromChannel[2][2048];
    int    m_channelFromStrip[2][2048];
    int    m_sensorType;
    //double coarsepulse[88][2048][7];
    bool   m_inverted, m_detailedOutput;
    std::vector<int> m_chosenTell1s;
    std::vector<int> m_pulsedChannels;
    std::vector<int> m_runNumbers;
    //std::vector<double> m_runNumbersDelay;
    std::vector<int> m_chosenBunches;
    std::map<int, int> m_eventPerStepCounter;

    std::map<int, double> m_runToDelay;
    //std::vector<std::vector<bool>>; 
    double subtractCommonMode(int channelNumber,  LHCb::VeloTELL1Data *tell1data);
    std::vector<double> SubtractCommonMode( std::vector<int> data, double cut );
    double GetClusterADC( int seedChannel, LHCb::VeloTELL1Data *tell1data);
    //  StatusCode i_cacheFillingData();  

    // pointer to the VELO detector element
    DeVelo* m_veloDet;

    // flag for DEBUG level
    bool m_debugLevel;


  protected:

  private:
    IEventTimeDecoder* m_timeDecoder; ///< Pointer to tool to decode ODIN bank
    // Retrieve the VeloTell1Data
    LHCb::VeloTELL1Datas* veloTell1Data( std::string samplingLocation );
    // Monitor the VeloTell1Data
    //void MakePulseShape( std::string samplingLocation, LHCb::VeloTELL1Datas* tell1Datas );
    TF1 *func;
    TH1D *pulse;
    std::vector<int> getLink(VeloTELL1::ALinkPair begEnd) {
      std::vector<int>::const_iterator iT; //iterator to vector of unsigned int.
      std::vector<int> linkVector;
      linkVector.clear();
      //Loop over ADC values in link
      for(iT=begEnd.first; iT!=begEnd.second; ++iT){
        linkVector.push_back(*iT); //Push back ADC value to vector
      }
      return linkVector;
    }    

    // Data members
    const DeVelo* m_velo;
    // Job options
    std::vector<std::string> m_samplingLocations;


    Condition *m_fillingschemecond;
    std::string m_B1FillingScheme;      
    std::string m_B2FillingScheme;      


  };
}
#endif // VELODATAMONITOR_PULSESHAPEFITTERTEDTREE
