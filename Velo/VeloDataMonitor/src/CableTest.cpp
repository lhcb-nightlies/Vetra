// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 

// local
#include "CableTest.h"

//velo
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"
#include "VeloEvent/VeloProcessInfo.h"

//TELL1 kernel
#include "Tell1Kernel/VeloDecodeConf.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CableTest
//
// 2007-10-09 :  Aras Papadelis, arasp@nikhef.nl
//
// Monitoring class for the cable test during commissioning.
// Should be run over data where the link and hybrid number have been encoded
// into the chip channels in the following fashion:
// Link #47 --> Add 1, multiply by 2--> (47+1) * 2  = 0110 0000 bin 
// hybrid #112 = 0111 0000 bin
// 
// Channel numbers written backwards: |31 ... 24|23 ... 16|15 ....  8|7 ... 0  | 
// Encoding:                          | hybrid2 |  link2  | hybrid1  |  link1  |
//                                    |01110000 |01100000 | 01110000 | 01100000|
// The channels with '1' will have a testpulse.
//
// The reason for the (+1 ) *2 operation on the link number is two-fold
// 1) We want link 0 to have a test pulse in it to separate it from a dead link --> +1
// 2) We don't want to use the first channel in the link (header xtalk) --> *2
// The hybrid number is encoded without any extra steps.
//
// This algorithm decodes the link and hybrid information using the auxilliary 
// class LinkInfo defined in the header file. The threshold for testpulse finding 
// can be changed there. After decoding, a set of correlation plots are filled. 
// Pedestal subtracted ADC values form the emulator are used. The first channel after
// the header is always omitted from the decoding.
//
// The decoding in LinkInfo takes the (+1) *2 encoding into account, so the bit pattern 
// for link 0 (which is 00000010 = 2) will be decoded to 0. Thus, a dead link will be 
// decoded as -1 (0*2 -1 = -1).
// 
//
// The appropriate VetraTELL1_CableTest.opts file can be found in the Vetra
// options directory.
// 
//
// By default, all TELL1s available in the data will have plots. The user can 
// override this with the option ChosenTell1List = { 1, 5, 14 ... } which only
// fills plots for the TELL1s in the list.
//
// Other options are  
// *NEvents = 100. How many events do we want to process?
// *SignalTheshold = ADC threshold used by the decoding to tell a low bit from a high bit.
// *PrintInfo = false. Toggle info printouts.
// 
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( CableTest )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CableTest::CableTest( const std::string& name,
						    ISvcLocator* pSvcLocator)
  :  GaudiTupleAlg( name , pSvcLocator )
  ,m_veloDet ( 0 )
  ,m_evtNumber(0)
  , m_isInit    (0)
  ,  m_ADCsLoc ( LHCb::VeloTELL1DataLocation::ADCs )
  ,  m_FIRADCsLoc ( LHCb::VeloTELL1DataLocation::FIRCorrectedADCs )
  ,  m_subADCsLoc ( LHCb::VeloTELL1DataLocation::PedSubADCs )
  ,  m_LCMSADCsLoc ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs )
  ,  m_reorderedLoc ( LHCb::VeloTELL1DataLocation::ReorderedADCs )

     
{

  declareProperty("ChosenTell1List",m_chosenTell1s);
  declareProperty("NEvents",m_lastevent=100);
  declareProperty("SignalThreshold",m_threshold=20);
  declareProperty("PrintInfo",m_printinfo=false);

}
//=============================================================================
// Destructor
//=============================================================================
CableTest::~CableTest() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode CableTest::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  
  debug() << "==> Initialize" << endmsg;
  setHistoTopDir( "Vetra/" );
  
  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CableTest::finalize() {

  debug() << "==> Finalize" << endmsg;
  return GaudiTupleAlg::finalize();  

}


//=============================================================================
// Main execution
//=============================================================================
StatusCode CableTest::execute() {

  debug() << "==> Execute" << endmsg;

  
  if(m_isInit==0){
    VeloProcessInfos* procInfos;  
    if(!exist<VeloProcessInfos>(VeloProcessInfoLocation::Default)){      
      warning() << " ==> No init info available. Assuming pedestal convergence at 5000 events." << endmsg;
      m_convergenceLimit=5000;
    }else{
      procInfos=get<VeloProcessInfos>(VeloProcessInfoLocation::Default);
      VeloProcessInfos::const_iterator procIt=procInfos->begin();
      if((*procIt)->convLimit()==0){
        debug()<< "Convergence Limit not set!" <<endmsg;
        m_convergenceLimit=(*procIt)->convLimit();
      }else{
      	m_convergenceLimit=(*procIt)->convLimit();
      }    
      debug() << "Convergence limit set to " << m_convergenceLimit << endmsg;

    }
    m_isInit=1;
    
  }

  StatusCode sc;

  if ( m_evtNumber>=m_convergenceLimit && m_evtNumber < (m_convergenceLimit+m_lastevent) ) {
    sc = plotCorrelation();
  }
  
  m_evtNumber++;

  return StatusCode::SUCCESS;


}
//=============================================================================
StatusCode CableTest::plotCorrelation()
{
  debug()<< " ==> plotCorrelation() " <<endmsg;

  if(!exist<VeloTELL1Datas>( m_subADCsLoc )){
    info()<< " ==> There are no pedestal subtracted ADCs in TES! " <<endmsg;
    return ( StatusCode::FAILURE );
  }else{
    m_subADCs = get<VeloTELL1Datas>( m_subADCsLoc );
  }

  //Loop over all TELL1 banks:
  VeloTELL1Datas::const_iterator sensIt;
  for(sensIt=m_subADCs->begin();sensIt!=m_subADCs->end();++sensIt){
    VeloTELL1Data* dataOneTELL1=(*sensIt);
    
    int TELL1_number=dataOneTELL1->key();
    //If user has specified TELL1s, only look at those. Otherwise fill plots for all:
    if(m_chosenTell1s.size()>0 && 
       find(m_chosenTell1s.begin(), m_chosenTell1s.end(), TELL1_number)==m_chosenTell1s.end()){
      continue;}

    

    //Loop over all link in the TELL1:
    for(int link=0;link<NumberOfALinks;link++){
      
      char hname0[100];
      char hname[100];


      //Fill plots



      //Decode the bitpattern:
      LinkInfo bitpattern((*dataOneTELL1)[link], m_threshold);
	
	  
      sprintf(hname,"LinkNumberVsBitPattern1_Tell1_%d",TELL1_number);
      plot2D(bitpattern.link1(),link,hname,hname
	     ,-1.5,64.5,-1.5,64.5,66,66);
	  
      sprintf(hname,"LinkNumberVsBitPattern2_Tell1_%d",TELL1_number);
      plot2D(bitpattern.link2(),link,hname,hname
	     ,-1.5,64.5,-1.5,64.5,66,66);

	  
      sprintf(hname,"BitPattern2VsBitPattern1_Tell1_%d",TELL1_number);
      plot2D(bitpattern.link1(),bitpattern.link2(),hname,hname
	     ,-1.5,64.5,-1.5,64.5,66,66);

	  
      sprintf(hname,"HybridNumber1VsLink_Tell1_%d",TELL1_number);
      plot2D(link,bitpattern.hybrid1(),hname,hname
	     ,-1.5,64.5,-1.5,200.5,66,202);
	  
      sprintf(hname,"HybridNumber2VsLink_Tell1_%d",TELL1_number);
      plot2D(link,bitpattern.hybrid2(),hname,hname
	     ,-1.5,64.5,-1.5,200.5,66,202);

	  
      sprintf(hname,"HybridNumber2VsHybridNumber1_Tell1_%d",TELL1_number);
      plot2D(bitpattern.hybrid1(),bitpattern.hybrid2(),hname,hname
	     ,-1.5,64.5,-1.5,200.5,66,202);
	
	

      //Check for discrepancies and warn the used:
      if(m_printinfo==true){
	if(bitpattern.link1()!=bitpattern.link2() || bitpattern.link1()!=link || bitpattern.link2()!=link ){
	  info() << "Discrepancy within Tell1 #" << TELL1_number << ", link " << link << ": Bits 0-7 = " << bitpattern.link1()
		 << " Bits 16-23 = " << bitpattern.link2() << endmsg;
	    
	}
	if(bitpattern.hybrid1()!=bitpattern.hybrid2()){
	  info() << "Discrepancy within Tell1 #" << TELL1_number << ", link " << link << ": Bits 8-15 = " << bitpattern.hybrid1()
		 << " Bits 24-31 = " << bitpattern.hybrid2() << endmsg;
	}
	

	debug() << "Link info: " << bitpattern.link1() 
		<< " " << bitpattern.hybrid1() 
		<< " " << bitpattern.link2()
		<< " " << bitpattern.hybrid2()
		<< endmsg;

	
      }

      //Fill a simple ped sub ADC vs channel plot:
      sprintf(hname0,"PedSubADC_Tell1_%d",TELL1_number);
      
      for(int chan=0;chan<32;chan++){
	  int ADC=(*dataOneTELL1).channelADC(link*32+chan);
	  plot2D(chan+link*32,ADC,hname0,hname0,-1,2048,-512,512,2049,1024);
	  
      }	
      
    }
  }
  
  return ( StatusCode::SUCCESS );
}




