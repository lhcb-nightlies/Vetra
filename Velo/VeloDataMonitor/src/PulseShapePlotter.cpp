// $Id: PulseShapePlotter.cpp,v 1.2 2009-09-20 18:45:17 szumlat Exp $
// Include files
// -------------
#include <iostream>
#include <fstream>
// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
// from Tell1Kernel
#include "Tell1Kernel/VeloDecodeConf.h" 
#include "Event/ODIN.h"
#include "Event/VeloODINBank.h"
// local
#include "PulseShapePlotter.h"
#include "CommonFunctions.h"
// from Boost
#include <boost/assign/list_of.hpp>
#include "boost/lexical_cast.hpp"
// from Gaudi
#include "AIDA/IHistogram2D.h"
// local
// Kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"

#include "Event/ODIN.h"


using namespace VeloTELL1;

//-----------------------------------------------------------------------------
// Implementation file for class : PulseShapePlotter
// Author:  Ivan Mous and Kazu 
// This code should fit pulse shapes to
// Non zero suppressed data and find the peak time.
// the rising and falling times have to be externally  measured...
// the code can also cope with a relative delay input and plot the 
// "clusters" found against time.
// This is a simplified version of PulseShapeFitterTed (without per-event fit)
//-----------------------------------------------------------------------------
// Declaration of the Algorithm Factory
namespace Velo {
  DECLARE_ALGORITHM_FACTORY( PulseShapePlotter )
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  Velo::PulseShapePlotter::PulseShapePlotter( const std::string& name, ISvcLocator* pSvcLocator)
  : Velo::VeloMonitorBase ( name , pSvcLocator )
	,m_nEvents(0)
{
  std::vector<std::string> tmpLocations = 
	boost::assign::list_of("Prev7")("Prev6")("Prev5")("Prev4")("Prev3")("Prev2")("Prev1")
	("")("Next1")("Next2")("Next3")("Next4")("Next5")("Next6")("Next7");
  declareProperty( "SamplingLocations", m_samplingLocations = tmpLocations );
  declareProperty( "DataLocation", m_dataLocation = "Raw/Velo/PreparedADCs");
  //declareProperty( "NSteps", m_nSteps= 1 );// Not used, take info from odin
  //declareProperty( "StepSize", m_stepSize= 100 );
  declareProperty( "CMSCut", m_cmsCut= 10 );
  declareProperty( "MaxAmplitudeCut", m_amplitudeCut= 300);
  declareProperty( "OffsetCut", m_offsetCut= 10 );
  declareProperty( "IntegralCut", m_integralCut= 10 );
  declareProperty( "NTimeBins", m_nTimeBins = 25 );
  declareProperty( "TELL1s",m_chosenTell1s     );
  declareProperty( "SignalThreshold" , m_threshold=20);
  declareProperty( "RunWithOdin" , m_runWithOdin = true ); // 
  declareProperty( "RunNumbers", m_runNumbers );
  declareProperty( "RunNumbersDelay", m_runNumbersDelay );
  declareProperty( "ChosenBunchCounters", m_chosenBunches );
  //declareProperty( "RunWithChosenBunchCounters", m_useBCNT = true );
  //declareProperty( "EventsPerFile", m_eventsPerFile=-1 );
  declareProperty( "IncludePileUp", m_includePileUp=false );
  declareProperty( "SkipStepZero", m_skipStepZero=false );
  declareProperty( "NEventsPerStep", m_nEventsPerStep= -1  );
  declareProperty( "NumberOfCMSIterations", m_numberOfCMSIterations = 1);
  declareProperty( "RemoveHeaders", m_removeHeaders=false );
  declareProperty( "SelectBunchCrossing", m_selectBunchCrossing =false );
  declareProperty( "RejectHighADCOuterSamples", m_rejectHighADCOuterSamples =false );
  declareProperty( "MaxADCOuterSamples", m_maxADCOuterSamples = -1 );

  m_runNumbers.push_back(-1);
  m_runNumbersDelay.push_back(0);
  m_chosenTell1s.push_back(-1);
  
}
//=============================================================================
// Destructor
//=============================================================================
Velo::PulseShapePlotter::~PulseShapePlotter() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::PulseShapePlotter::initialize() {
  StatusCode sc = VeloMonitorBase::initialize(); // must be executed first
  printProps();
  //if(m_nSteps==1) m_stepSize=1000000000; 
  m_timeDecoder = tool<IEventTimeDecoder>( "OdinTimeDecoder" );
  if ( sc.isFailure() ) return sc;
  m_velo = getDet<DeVelo>( DeVeloLocation::Default );  
  if(m_runNumbers.size() == m_runNumbersDelay.size() &&  m_runNumbersDelay.size() >0  ){
	std::vector<int>::const_iterator irunNumber; int i =0;
	for( irunNumber =m_runNumbers.begin(); irunNumber!=m_runNumbers.end(); irunNumber++, i++){
	  m_runToDelay[*irunNumber] = m_runNumbersDelay[i]; 
	  info() << " run number = " <<  *irunNumber << " corresponds to delay " << m_runToDelay[*irunNumber] << endmsg;
	}
  }
  else info() << " check out option file for the delays and runs cause something seems wrong" << endmsg;
  m_eventsThisFile=0;
  return StatusCode::SUCCESS;
}

//=============================================================================
/// Main execution
//=============================================================================
StatusCode Velo::PulseShapePlotter::execute() {
  counter( "# events" ) += 1;

  LHCb::ODIN* odin=0;
  if (m_runWithOdin){
	if(exist<LHCb::ODIN>((LHCb::ODINLocation::Default))) {
	  odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
	}
	else{
	  m_timeDecoder->getTime(); // in case we wants to get other info from odin.
	  odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
	}	
  }
  else m_runNumbers[0] = -1;
  debug()<<"Bunch ID: "<< odin->bunchId()<<", bunch crossing type: "<< odin->bunchCrossingType() << endmsg;

  if(odin ){
	int bunchCounter =  odin->bunchId();
	debug() << " >>> bunch id: " <<  odin->bunchId() << endmsg;
	std::vector<int>::iterator ibunch;
	int isitright=0;
	if(m_chosenBunches.size() !=0){
	  for(ibunch = m_chosenBunches.begin(); ibunch != m_chosenBunches.end(); ibunch++ ){
		if( bunchCounter == (*ibunch) ) isitright =1;
	  }
	}
	else isitright = 1;
	if(isitright==0){ 
	  debug() << " This is not the  bunch id you were looking for " <<  odin->bunchId() << endmsg;
	  return StatusCode::SUCCESS;
	}
	m_currentCalStep=odin->calibrationStep();
	if(m_skipStepZero && m_currentCalStep==0){
	  debug()<<"Skipping step 0"<<endmsg;
	  return StatusCode::SUCCESS;
	}
  
	m_eventPerStepCounter[m_currentCalStep] +=1; 
	if(m_nEventsPerStep >0 ){
	  if(  m_eventPerStepCounter[m_currentCalStep] > m_nEventsPerStep ){
		info()<<"Enough stats! Skipping the rest of step " << m_currentCalStep << endmsg;
		return StatusCode::SUCCESS;
	  }
    }
  }
  const int nSamples = m_samplingLocations.size();
  if( m_selectBunchCrossing ){
    // Retrieve Fillingscheme to select suitable bunchcrossings
    int bunchCounter =  odin->bunchId();
    std::vector<int> bunchCrossingSample(nSamples,-1);

    Condition* xx = getDet<Condition>(detSvc(),"/dd/Conditions/Online/LHCb/LHCFillingScheme") ;
    std::string fs1str = xx->param<std::string>("B1FillingScheme");
    std::string fs2str = xx->param<std::string>("B2FillingScheme");
    char* fs1 = (char*)fs1str.c_str();
    char* fs2 = (char*)fs2str.c_str();  
    std::vector<int> fillingScheme(fs2str.size()+1);

    for( unsigned int i=0; i<fs2str.size(); i++){
      if (fs1[i]=='1' && fs2[i]=='1')fillingScheme[i+1]=3;
      if (fs1[i]=='1' && fs2[i]=='0')fillingScheme[i+1]=1;
      if (fs1[i]=='0' && fs2[i]=='1')fillingScheme[i+1]=2;
      if (fs1[i]=='0' && fs2[i]=='0')fillingScheme[i+1]=0; 
    }

    for (int i=-2; i<=2; i++){
      debug()<<"Scheme for "<<i<<" : "<<fillingScheme[(bunchCounter+i)]<<endmsg;  
      bunchCrossingSample[i+2] = fillingScheme[(bunchCounter+i)];
    }
    // select only events which are beam-beam, and have non-beam-beam crossings in  prev1, prev2 and next1
    if ( bunchCrossingSample[2] == 3 &&
         bunchCrossingSample[1] !=3 &&
         bunchCrossingSample[3] !=3 &&
         bunchCrossingSample[0] !=3){
      debug()<<"Bunch-crossing ok"<<endmsg;
    }
    else {
      debug()<<"Bunch-crossing not ok"<<endmsg;
      return StatusCode::SUCCESS;
    }
  }
  int currentRunNumber;
  double delay=0;
  double step=0;
  
  std::vector<std::string>::const_iterator itL;
  //check middle sample for the tell1Data:
  const int midSampleIndex = (int)floor(m_samplingLocations.size()/2.);
  for(int jsample = 0; jsample < nSamples ; jsample++){
    LHCb::VeloTELL1Datas *SampleData = veloTell1Data(m_samplingLocations[jsample]);
    if( SampleData == 0 ) {
      debug() << "Sampledata of sample " << jsample << " is empty! RETURN "<< endmsg;
      return StatusCode::SUCCESS;
    }    
  }
  LHCb::VeloTELL1Datas *midSampleData = veloTell1Data(m_samplingLocations[midSampleIndex]);
  LHCb::VeloTELL1Datas::const_iterator itD;
  step=m_currentCalStep;
  debug() << "All samples found, total N events: "<< m_nEvents << ", now in calibstep " <<   m_currentCalStep<< endmsg;

  // Add delay for specific run
  if(m_runNumbers[0] > 0){
	currentRunNumber = odin->runNumber();
	delay = m_runToDelay[currentRunNumber];
	debug() << " delay = " << delay  << " currentRunNumber " << currentRunNumber << endmsg;
  }
  
  if(m_prevRunNumber!=odin->runNumber()){
	m_eventsThisFile=0;
	m_prevRunNumber=odin->runNumber();
  }

  // Loop over samples
  for ( itD = midSampleData -> begin(); itD != midSampleData -> end(); ++itD ) {
    LHCb::VeloTELL1Data* data = (*itD);  
    std::vector<LHCb::VeloTELL1Data* > allSamplesTell1Data;
    allSamplesTell1Data.resize(nSamples); 
    int TELL1_number=data->key();
    
    //Select a particular tell1
    if(ExcludeTELL1(m_chosenTell1s,TELL1_number)&&m_chosenTell1s[0]!=-1)  continue;
    if(TELL1_number>110 && !m_includePileUp) continue;
    const DeVeloSensor* sens=0;
    sens=m_veloDet->sensorByTell1Id(TELL1_number);
    unsigned int sensNumber=0;
    sensNumber=sens->sensorNumber();
    int count =0;
    for ( itL = m_samplingLocations.begin(); itL != m_samplingLocations.end(); ++itL, count++ ) {
      LHCb::VeloTELL1Datas* tell1Datas = veloTell1Data( (*itL) );  
      if (tell1Datas->object(TELL1_number) ==0){
        debug() << " this should not happen!" << endmsg;
        return StatusCode::SUCCESS;
      }
      allSamplesTell1Data[count] = tell1Datas->object(TELL1_number);
    }
    std::vector<bool> signalDetected(2048,false);
    
    // Loop over samples 
    for(int iSample = 0; iSample<nSamples; iSample++){
      debug() << "iSample: " << iSample  << endmsg;
      for(int aLink=0; aLink<NumberOfALinks; ++aLink){
        //debug() << "aLink: " << aLink  << endmsg;
        std::vector<int> linkADCs = getLink( (*allSamplesTell1Data[iSample])[aLink]);
        std::vector<double> linkCMS = SubtractCommonMode(linkADCs, m_cmsCut); 
        std::vector<double>::iterator itf;
        int channel=0;
        
        // Evaluate the data per channel
        for(itf=linkCMS.begin();itf!=linkCMS.end() ; ++itf, channel++){
          int channelNumber = channel + 32*aLink;
          if (m_removeHeaders){
            if (channelNumber%32==31) continue;
          }
          
          double cmsadc = *itf;
          //plot2D(25*(iSample - (int) nSamples/2)+delay,cmsadc,"cmsADC_BeforeThres","cmsADC_BeforeThres",
          //	  -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25, -511.5, 512.5, 50*(nSamples+2), 1024);	
          //plot2D(channel,cmsadc,"ADCvsChannel","ADCvsChannel",-0.5,32.5,-127.5,128.5,33,256);
          if(cmsadc > m_threshold && !signalDetected[channelNumber] ){
            debug()<<"Now at step: "<< step <<endmsg;
            debug() << "  signal detected! channel "  << channelNumber << " on sensor " << sensNumber << " Let's fit!" << endmsg; 
            debug() << " channel adc = " << cmsadc << endmsg;	    

            signalDetected[channelNumber] = true;
            //double integral == thissampleADC+next+prev;
            char finepulseshape[200];
            sprintf(finepulseshape,"ClustersVsTime_%d",sensNumber);
            std::string ffinepulseshape = finepulseshape;
            //debug()<<"Beginning filling of profiles"<<endmsg;
            //plot2D(25*(iSample - (int) nSamples/2)+delay,cmsadc,"cmsADC_AfterThres","cmsADC_AfterThres",
            //	-25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25, -511.5, 512.5, 50*(nSamples+2), 1024);	
            double adcvalue[5] = {0};  
            double integral = 0;
            
            // Store ADC and time values per sample
            for(int isample =0; isample <nSamples; isample++){
              double time = 25*(isample - (int) nSamples/2)+delay+step;			    
              std::vector<int> templinkADCs = getLink( (*allSamplesTell1Data[isample])[aLink]);
              std::vector<double> templinkCMS = SubtractCommonMode(templinkADCs, m_cmsCut);
              adcvalue[isample] = templinkCMS[channel];
              integral += templinkCMS[channel];
              debug()<<"In processed event: "<<m_nEvents<<" delay: "<<delay<<" step: "<<step
                     <<" isample: "<<isample<<" time: "<<time<<" adcvalue: "<<adcvalue[isample]<<endmsg;
              //debug()<<time<<" "<<adcvalue[isample]<<" "<<finepulseshape<<" "<<finepulseshape<<" "
              //<<-25*((nSamples+2)/2)-0.25<<" "<<25*((nSamples+2)/2+1)-0.25<<" "<< -127.5<<" "<< 128.5<<" "<< 50*(nSamples+2)<<" "<< 256<<endmsg;
              //plot2D(channel,adcvalue,"ADCvsChannelInLoop","ADCvsChannelInLoop",-0.5,32.5,-127.5,128.5,33,256);
            }
            
            // Select only channels with sufficient total ADC in all samples 
            // Fill final "ClustersVsTime" histos
            if(integral > m_integralCut ){
              // Optional: select the pulse around 0 by applying a cut on the ADC values in the outer TAE samples:
              if( m_rejectHighADCOuterSamples ){
                if( adcvalue[4] < m_maxADCOuterSamples && adcvalue[0] < m_maxADCOuterSamples ){            
                  for(int isample =0; isample <nSamples; isample++){
                    double time = 25*(isample - (int) nSamples/2)+delay+step;
                    plot2D(time,adcvalue[isample],finepulseshape,finepulseshape,
                           -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25, -127.5+50, 128.5+150, 50*(nSamples+2), 256+100);	
                    profile1D(time,adcvalue[isample],ffinepulseshape+"prof",ffinepulseshape+"prof",
                              -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25,  50*(nSamples+2));	
                  }// end loop over samples
                }//end if maxADC
              }// end if selectmiddlesample        
              
              else{
                for(int isample =0; isample <nSamples; isample++){
                  double time = 25*(isample - (int) nSamples/2)+delay+step;
                  plot2D(time,adcvalue[isample],finepulseshape,finepulseshape,
                         -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25, -127.5+50, 128.5+150, 50*(nSamples+2), 256+100);	
                  profile1D(time,adcvalue[isample],ffinepulseshape+"prof",ffinepulseshape+"prof",
                            -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25,  50*(nSamples+2));	
                }// end loop over samples
              }      
            }// end if integralcut
          }// end if CMSADCcut
        }// end loop over channels
      }// end loop over links
    }// end loop over samples
    debug()<<"Event done"<<endmsg;
  }//end loop over samples
  m_nEvents++;
  m_eventsThisFile++;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::PulseShapePlotter::finalize() {
  if ( m_debugLevel ) debug() << "==> Finalize" << endmsg;
  return VeloMonitorBase::finalize(); // must be called after all other actions
}

//=============================================================================
// Retrieve the VeloTell1Data
//=============================================================================
LHCb::VeloTELL1Datas*
Velo::PulseShapePlotter::veloTell1Data( std::string samplingLocation ) {
  std::string tesPath;
  if(samplingLocation == "") tesPath =  m_dataLocation;// "Raw/Velo/DecodedADCs"; 
  else  tesPath = samplingLocation + "/"+ m_dataLocation; // "/Raw/Velo/DecodedADCs"; 
  size_t pos = tesPath.find( "//" );
  if ( pos != std::string::npos )    tesPath.replace( pos, 2, "/" );
  if ( m_debugLevel ) 	debug() << "Retrieving VeloTell1Data from " << tesPath << endmsg;
  if ( !exist<LHCb::VeloTELL1Datas>( tesPath ) ) {
	debug() << "No VeloTell1Data container found for this event !" << endmsg; 
	if (m_nEvents %100==0  )  info() << "No VeloTell1Data container found for this event !   " << tesPath << endmsg; 
	return 0;
  }
  else {
	LHCb::VeloTELL1Datas* tell1Data = get<LHCb::VeloTELL1Datas>( tesPath );
	if ( m_debugLevel ) debug() << "  -> number of VeloTell1Data found in TES: "
	  << tell1Data->size() <<endmsg;
	return tell1Data;
  }
}
//=============================================================================
// simple link common mode subtraction
std::vector<double>  Velo::PulseShapePlotter::SubtractCommonMode( std::vector<int> data, double cut = 15.0)
{
  int howManyPasses = m_numberOfCMSIterations;
  std::vector<double> commonModeSubtractedData;
  std::vector<int>::iterator it;
  std::vector<double>::iterator itf;
  double average = 0;
  int count = 0, chanWithoutHits = 0;  double averageWithoutHits = 0;
  for(it=data.begin();it!=data.end() ; ++it){
	average += float(*it);
  }
  average /= float(data.size());
  for(it=data.begin();it!=data.end() ; it++){
	commonModeSubtractedData.push_back((double)(*it) - average);
  }
  int pass=0;
  while(pass < howManyPasses){
	chanWithoutHits=0; averageWithoutHits =0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf){
	  if(fabs(*itf) < cut )  {
		averageWithoutHits += *itf;
		chanWithoutHits++;
	  }
	}
	if(chanWithoutHits > 0) averageWithoutHits /= double(chanWithoutHits);
	else averageWithoutHits = 0;
	count = 0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf, count++){
	  commonModeSubtractedData[count]  = *itf - averageWithoutHits;
	}
	pass++;
  }
  return commonModeSubtractedData;
}
// attempt of clusterizing the data.


//simple mean common mode subtraction

double Velo::PulseShapePlotter::subtractCommonMode(int channelNumber, LHCb::VeloTELL1Data *tell1data){
  ALinkPair begEnd;
  int aLink = (int)(channelNumber/32.);
  begEnd=(*tell1data)[aLink];
  scdatIt iT;
  double commonmode=0;
  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
	commonmode += *iT;  
  }
  commonmode /= 32.0;
  int count = 0;
  double commonmodewithoutsignal= 0.0;
  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
	double cmsadc = *iT - commonmode; 
	if(fabs(cmsadc) < m_threshold){ 
	  commonmodewithoutsignal += *iT;
	  count++;
	}
  }
  commonmodewithoutsignal /= float(count+0.00001);
  double cmsadc = tell1data->channelADC(channelNumber) - commonmodewithoutsignal;
  return cmsadc;
}

std::vector<int> Velo::PulseShapePlotter::getLink(VeloTELL1::ALinkPair begEnd) {
  //std::vector<int> Velo::PulseShapePlotter::getLink() {  
  std::vector<int>::const_iterator iT; //iterator to vector of unsigned int.
  std::vector<int> linkVector;
  linkVector.clear();
  //Loop over ADC values in link
  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
	linkVector.push_back(*iT); //Push back ADC value to vector
  }
  return linkVector;

  //std::vector<int> linkADCs(32,1);
  //return linkADCs;
}
