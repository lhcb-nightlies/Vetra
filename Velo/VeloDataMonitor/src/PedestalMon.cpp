
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/AlgFactory.h" 
// ============================================================================

// from Event model
#include "VeloEvent/VeloTELL1Data.h"

//velo
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"


// local
#include "CommonFunctions.h"

// AIDA 
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"


// from TELL1 Kernel:
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "Tell1Kernel/VeloDecodeConf.h" 

// Boost 
// ============================================================================
#include "boost/format.hpp"
// ============================================================================

//-----------------------------------------------------------------------------
// Class : Velo::Monitoring::PedestalMon
//
// 2006-11-23   Aras Papadelis. arasp@nikhef.nl
// Class for monitoring the subtracted pedestals from the emulator.
// Option:
//          TELL1s={1,2,3} choses TELL1s (default is all TELL1s).
//          PublishRate=5000 sets how often the histogram should be updated.
// 
//-----------------------------------------------------------------------------


using namespace LHCb;
using namespace VeloTELL1;

namespace Velo
{
  
  namespace Monitoring 
  {
    class PedestalMon : public GaudiHistoAlg {
    public:
      /// Standard constructor
      PedestalMon( const std::string& name, ISvcLocator* pSvcLocator );

      virtual ~PedestalMon( ); ///< Destructor

      virtual StatusCode initialize() override;    ///< Algorithm initialization
      virtual StatusCode execute   () override;    ///< Algorithm execution
      virtual StatusCode finalize  () override;    ///< Algorithm finalization

      StatusCode plotSubtractedPedestal();
  

    private  :
      unsigned long int  m_evtNumber;
      int m_publish;

      DeVelo* m_veloDet;
      std::string m_subPedsLoc;
      std::vector<int> m_chosenTell1s;
  
    };

  }
}



//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::PedestalMon::PedestalMon( const std::string& name,
                                                ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
    ,  m_evtNumber(0)
    ,  m_publish (5000)
    ,  m_veloDet ( 0 )
  
    ,  m_subPedsLoc ( LHCb::VeloTELL1DataLocation::SubPeds )



{
  declareProperty( "TELL1s",               m_chosenTell1s);
  declareProperty( "PublishRate",           m_publish);

  setProperty ( "PropertiesPrint", true ).ignore();
}

//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::PedestalMon::~PedestalMon() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::PedestalMon::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( propsPrint() ) printProps();
  debug() << "==> Initialize" << endmsg;
  setHistoTopDir( "Vetra/" );
  
  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::PedestalMon::finalize() {

  debug() << "==> Finalize" << endmsg;  
  if(produceHistos() ) { plotSubtractedPedestal(); } // Make the plots
  return GaudiHistoAlg::finalize();  // must be called after all other actions
}

//=============================================================================
//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::PedestalMon::execute() {

  //  debug() << "==> Execute" << endmsg;
  
  if(m_evtNumber%m_publish == 0) plotSubtractedPedestal();
  
  m_evtNumber++;  
  
  return StatusCode::SUCCESS;
}


//==============================================================================================================
//==============================================================================================================
StatusCode Velo::Monitoring::PedestalMon::plotSubtractedPedestal()
{

  VeloTELL1Datas* m_tell1DataContainer;
  
  if(!exist<VeloTELL1Datas>( m_subPedsLoc )){
    debug()<< " ==> There are no subtracted pedestals in TES! " <<endmsg;
    return ( StatusCode::FAILURE );
  }else{
    m_tell1DataContainer = get<VeloTELL1Datas>( m_subPedsLoc );
  }


  VeloTELL1Datas::const_iterator sensIt;
  for(sensIt=m_tell1DataContainer->begin();sensIt!=m_tell1DataContainer->end();++sensIt){
    VeloTELL1Data* dataOneTELL1=(*sensIt);
    int TELL1_number=dataOneTELL1->key();


    if(ExcludeTELL1(m_chosenTell1s,TELL1_number))
      continue;

    //Get sensor from detector element:  
    const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(TELL1_number);
    if(sensor == 0 ){
      error() << "Can't find a sensor associated with TELL1 " << TELL1_number << ". Aborting..." << endmsg;
      return StatusCode::FAILURE;
    }         
    
    boost::format fmt ( "TELL1_%03d/SubtractedPedestals_vs_ChipChannel" ) ;
    fmt %  TELL1_number;           
    boost::format title ( "Subtracted pedestals vs chip channel, TELL1 %d, sensor %d" );
    title %  TELL1_number % sensor->sensorNumber();
    
    AIDA::IHistogram1D* histo = book 
      ( fmt.str()  , title.str()  , -0.5,SENSOR_CHANNELS-0.5,SENSOR_CHANNELS);
    if ( 0 != histo ) { histo -> reset() ; } // ATTENTION: RESET 

    ALinkPair begEnd;
    int channel=0;

    for(int aLink=0; aLink<NumberOfALinks; ++aLink){
      begEnd=(*dataOneTELL1)[aLink];
      for(scdatIt iT=begEnd.first; iT!=begEnd.second; ++iT){
	if( 0 != histo ) { histo->fill ( channel, (*iT)) ; }	
	channel++;
      }
    }
  
  }
  
  return StatusCode::SUCCESS;
}

// Declaration of the Algorithm Factory


DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring,PedestalMon)
