// $Id: ErrorMon.h,v 1.5 2009/10/01 15:54:09 krinnert Exp $
#ifndef INCLUDE_ERRORMON_H
#define INCLUDE_ERRORMON_H 1

#include <map>
#include <string>

#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/IMonitorSvc.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "SiDAQ/SiClusterWord.h"
#include "SiDAQ/SiADCBankTraits.h"
#include "SiDAQ/SiRawBankDecoder.h"
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"
#include "Event/RawEvent.h"
#include "Event/RawBank.h"
#include "VeloEvent/EvtInfo.h"
#include "VeloEvent/VeloErrorBank.h"

namespace Velo
{
  namespace Monitoring
  {

    /** @class ErrorMon ErrorMon.h
     *  
     *
     * @author Kurt Rinnert <kurt.rinnert@cern.ch>
     * @date   2008-06-30
     *
     * @author Chiara Farinelli <chiara.farinelli@cern.ch>
     * @date   2010-03-29
     *
     */
    class ErrorMon : public GaudiHistoAlg {

      public:

        /// Standard Constructor
        ErrorMon(const std::string& name, ISvcLocator* pSvcLocator);

        virtual ~ErrorMon(); ///< Destructor

        virtual StatusCode initialize() override; ///< Algorithm initialization
        virtual StatusCode    execute() override; ///< Algorithm event execution
        virtual StatusCode   finalize() override; ///< Algorithm finalize

      private:

        void countErrorsAndMissingEvents(const std::vector<LHCb::RawBank*>& banks);
        void monitorErrorBanks(const VeloErrorBanks* banks);
        void printSummary();

      private:

        // Private s for raw bank decoder instantiation.
        // TODO: To put them here is a temporary solution. They should be
        // exported by another package to avoid code duplication.


        /** Velo raw bank bit pattern
         *
         *  This class is a type parameter for the SiClusterWord
         *  template class that allows to share bit-packing code
         *  with the ST software.
         *  The bit pattern is an implementation of the Velo raw
         *  bank specification in EDMS 637676, Version 2.0 as of
         *  December 1, 2005.
         *  
         * @see SiClusterWord
         *
         * @author Kurt Rinnert
         * @date   2006-02-02
         */
        class VeloBitsPolicy
        {
          public:

            typedef SiDAQ::adc_only_bank_tag adc_bank_type;

            enum bits {
              positionBits        =  0,
              channelBits         =  3,
              sizeBits            = 14,
              thresBits           = 15,
              interStripPrecision =  3 
            };

            enum mask {
              positionMask = 0x0007,
              channelMask  = 0x3ff8,
              sizeMask     = 0x4000,
              thresMask    = 0x8000
            };
        };

        typedef SiClusterWord<VeloBitsPolicy> VeloClusterWord;
        typedef SiRawBankDecoder<VeloClusterWord> VeloRawBankDecoder;

        typedef enum { PSEUDOERR=0, PCNERR=1, FIFOERR=2, IHEADERR=3 } ErrorTypes; 
        typedef enum { PCN=0, IHEAD=1 } ErrorDouble; 

      private:

        std::string m_rawEventLocation;
        std::string m_errorBankLoc;

        IMonitorSvc *m_ErrorMonitorSvc;

        const DeVelo* m_velo;


        std::vector<unsigned int> m_expectedTell1Ids;  ///< configurable, all if empty (default)
        std::vector<unsigned int> m_tell1Ids; ///< either identical with configurable expected list or all

        std::map<unsigned int, unsigned long> m_nErrorsByTell1;  
        std::map<unsigned int, unsigned long> m_nEventsByTell1;  
        std::map<unsigned int, int> m_bankCounter;  
        unsigned long m_nEvts;
        unsigned int  m_evtsPerTell1PubRate;
        unsigned int  m_errsPerTell1PubRate;
        unsigned int  m_evtNumber;
        long m_sensors;
        bool m_debugmode;        

        std::map<int,int>  m_pcn;

        AIDA::IHistogram1D* m_evtsPerTell1; 
        AIDA::IHistogram1D* m_errsPerTell1; 
        AIDA::IHistogram2D* m_overviewpseudo; 
        AIDA::IHistogram2D* m_overviewpcn; 
        AIDA::IHistogram2D* m_overviewadcfifo; 
        AIDA::IHistogram2D* m_overviewchannel; 
        AIDA::IHistogram2D* m_overviewiheader;    
        AIDA::IHistogram1D* m_errNum; 
        AIDA::IHistogram2D* m_errFreq; 
        AIDA::IHistogram2D* m_errorType; 
        AIDA::IHistogram2D* m_errorDouble; 
        AIDA::IHistogram2D* m_asyncTells;
    };
  }
}
#endif // INCLUDE_ERRORMON_H

