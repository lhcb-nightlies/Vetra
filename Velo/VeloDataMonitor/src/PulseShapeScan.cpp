// $Id: PulseShapeScan.cpp,v 1.2 2009-09-20 18:45:17 szumlat Exp $
// Include files
// -------------
#include <iostream>
#include <fstream>
// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
// from Tell1Kernel
#include "Tell1Kernel/VeloDecodeConf.h" 
// local
#include "PulseShapeScan.h"
#include "CommonFunctions.h"
// from Boost
#include <boost/assign/list_of.hpp>
#include "boost/lexical_cast.hpp"
// from Gaudi
#include "AIDA/IHistogram2D.h"
// local
// Kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"

#include "Math/ParamFunctor.h"
#include "GaudiKernel/IEventTimeDecoder.h"            // Interface for odin
#include "Event/ODIN.h"

using namespace VeloTELL1;

//-----------------------------------------------------------------------------
// Implementation file for class : PulseShapeScan
//
//-----------------------------------------------------------------------------
// Declaration of the Algorithm Factory
namespace Velo {
  DECLARE_ALGORITHM_FACTORY( PulseShapeScan )
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::PulseShapeScan::PulseShapeScan( const std::string& name, ISvcLocator* pSvcLocator)
  : Velo::VeloMonitorBase ( name , pSvcLocator )
    ,m_nEvents(0)
{
  std::vector<std::string> tmpLocations = 
    boost::assign::list_of("Prev7")("Prev6")("Prev5")("Prev4")("Prev3")("Prev2")("Prev1")
    ("")("Next1")("Next2")("Next3")("Next4")("Next5")("Next6")("Next7");
  m_runSteps.push_back(0);
  m_delayList.push_back(0);
  declareProperty( "SamplingLocations", m_samplingLocations = tmpLocations );
  //  declareProperty( "EventNumberSeparators", m_separators );
  declareProperty( "ChisqCut", m_chisqCut= 10 );
  declareProperty( "CMSCut", m_cmsCut= 10 );
  declareProperty( "MaxAmplitudeCut", m_amplitudeCut= 300);
  declareProperty( "OffsetCut", m_offsetCut= 10 );
  declareProperty( "IntegralCut", m_integralCut= 10 );
  declareProperty( "UseLogLikeFit", m_useLogLikeFit= true );
  declareProperty( "TimeWindow", m_timeWindow= 150 );
  declareProperty( "FixOffset", m_fixedOffset= false );
  declareProperty( "TryToClusterize", m_tryCluster= false );
  declareProperty( "StepSize", m_stepSize= 100 );
  declareProperty( "TELL1s",m_chosenTell1s     );
  declareProperty( "SignalThreshold" , m_threshold=20);
  declareProperty( "TakeNeighbours", m_takeNeighbours=1 );
  declareProperty( "PlotSeparateStrips", m_plotSeparateStrips=false );
  declareProperty( "Offset" ,m_offset=0);
  declareProperty( "WidthBefore", m_width1=13.68);
  declareProperty( "WidthAfter", m_width2=26.76);
  declareProperty( "DetailedOutput" , m_detailedOutput=true);
  declareProperty( "ListOfRuns" , m_runSteps );
  declareProperty( "ListOfDelays" , m_delayList );
  declareProperty( "FakeTED" , m_fakeit = true ); // true means  to fake it.
  declareProperty( "RunWithOdin" , m_runWithOdin = true ); // 
  declareProperty( "TestWithNEvents", m_runOverFewEvents= 2 );
  declareProperty( "TestSkipNEvents", m_skipEvents= 500 );

  //for(int i = 0; i < 25; i++){
  //	m_separators.push_back(100);
  //}
}

//=============================================================================
// Destructor
//=============================================================================
Velo::PulseShapeScan::~PulseShapeScan() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::PulseShapeScan::initialize()
{
  StatusCode sc = VeloMonitorBase::initialize(); // must be executed first
  printProps();

  if(m_nSteps==1) m_stepSize=1000000000; 
  m_timeDecoder = tool<IEventTimeDecoder>( "OdinTimeDecoder" );
  
  if ( sc.isFailure() ) return sc;
  m_velo = getDet<DeVelo>( DeVeloLocation::Default );  

  //const int strips=2048;
  int strip_tmp=0,channel_tmp=0;
  std::ifstream ifs("/home/kakiba/pccChannel-mapping.twocol");
  std::ifstream ifsP("/home/kakiba/rccChannel-mapping.twocol");
  char dummy[100];

  //ifs.open("/home/kakiba/pccChannel-mapping.twocol");
  ifs.getline(dummy,100);
  for(int ichan = 0; ichan<2048; ++ichan){
	ifs>>strip_tmp>>channel_tmp;
	m_stripFromChannel[1][channel_tmp]=strip_tmp;  
	m_channelFromStrip[1][strip_tmp]=channel_tmp;  
  }

  //ifsP.open("/home/kakiba/rccChannel-mapping.twocol");
  ifsP.getline(dummy,100);
  for(int jchan = 0; jchan<2048; ++jchan){
	ifsP>>strip_tmp>>channel_tmp;
	m_stripFromChannel[0][channel_tmp]=strip_tmp;  
	m_channelFromStrip[0][strip_tmp]=channel_tmp;                          
  }
  ifsP.close();
  ifs.close();

  return StatusCode::SUCCESS;
}

//=============================================================================
/// Main execution
//=============================================================================
StatusCode Velo::PulseShapeScan::execute() {
  counter( "# events" ) += 1;
  m_nEvents++;
  //int finebin = 0;
  double delayValue=0;
  if(m_skipEvents == 0)  ;
  else if(m_nEvents%m_skipEvents >= m_runOverFewEvents) return StatusCode::SUCCESS;

 //  m_phase =
  LHCb::ODIN* odin=0;
  if (exist<LHCb::ODIN>((LHCb::ODINLocation::Default))){
	odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
  }
  else if(m_runWithOdin){
	m_timeDecoder->getTime(); // in case one wants to get other kinds of info from odin.
	odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
	//odin = OdinTimeDecoder::getODIN();
  }
  if(odin != 0){
    m_runNumber = odin->runNumber(); // -1 because the first step we skip...
    std::vector<int>::const_iterator irunsteps; 
    int pos =0;
    for(irunsteps = m_runSteps.begin(); irunsteps <  m_runSteps.end(); irunsteps++, ++pos){
      if(m_runNumber == *irunsteps){ 
		m_stepId = pos;
        info()<< " the run number is " << m_runNumber << " and hence the step number is " << m_stepId <<endmsg;
        delayValue = m_delayList[m_stepId];
      }
      else m_stepId = m_stepId;
    } 
  } 
  if(m_fakeit) delayValue = (int) m_nEvents/m_stepSize;
  info() << " delayValue === " << delayValue << endmsg;
  
  std::vector<std::string>::const_iterator itL;
  //check middle sample for the tell1Data:
  const int midSampleIndex = (int)floor(m_samplingLocations.size()/2.);
  const int nSamples = m_samplingLocations.size();
  for(int isample = 0; isample < nSamples ; ++isample){
	LHCb::VeloTELL1Datas *SampleData = veloTell1Data(m_samplingLocations[isample]);
	if( SampleData == 0 ) return StatusCode::SUCCESS;
  }
  LHCb::VeloTELL1Datas *midSampleData = veloTell1Data(m_samplingLocations[midSampleIndex]);
  LHCb::VeloTELL1Datas::const_iterator itD;

#ifndef WIN32
  pulse = new TH1D("pulse","pulse",nSamples,-(nSamples/2+0.5)*25.,(nSamples/2+0.5)*25.);
#endif

  char finepulseshape[150];
  //char pulseshape[150];
  for ( itD = midSampleData -> begin(); itD != midSampleData -> end(); ++itD ) {
	LHCb::VeloTELL1Data* data = (*itD);  
	std::vector<LHCb::VeloTELL1Data* > allSamplesTell1Data;
	allSamplesTell1Data.resize(nSamples); 
	int TELL1_number=data->key();
	if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue;
	const DeVeloSensor* sens=0;
	sens=m_veloDet->sensorByTell1Id(TELL1_number);
	unsigned int sensNumber=0;
	sensNumber=sens->sensorNumber();
	if(sensNumber<64) m_sensorType=0; else m_sensorType=1;
	int count =0;
	for ( itL = m_samplingLocations.begin();
        itL != m_samplingLocations.end(); ++itL, ++count ) {
	  LHCb::VeloTELL1Datas* tell1Datas = veloTell1Data( (*itL) );  
	  allSamplesTell1Data[count] = tell1Datas->object(TELL1_number);
	}
	std::vector<bool> signalDetected(2048,false);
#ifndef WIN32
	for(int iSample = 0; iSample<nSamples; ++iSample){
	  for(int aLink=0; aLink<NumberOfALinks; ++aLink){
		std::vector<int> linkADCs = getLink( (*allSamplesTell1Data[iSample])[aLink]);
		std::vector<double> linkCMS = SubtractCommonMode(linkADCs, m_cmsCut); 
		std::vector<double>::iterator itf;
		int channel =0;
		for(itf=linkCMS.begin();itf!=linkCMS.end() ; ++itf, ++channel){
		  int channelNumber = channel + 32*aLink;
          double cmsadc = *itf;
		  if(((cmsadc) > m_threshold)&& signalDetected[channelNumber] == false){
            double clusteradcs = GetClusterADC(channelNumber, allSamplesTell1Data[iSample]);
            debug()<< " clusteradcs: " << clusteradcs <<endmsg;
            signalDetected[channelNumber] == true;
            debug()<< signalDetected[channelNumber] <<endmsg;
			for(int jSample =0; jSample<nSamples; ++jSample){
			  double time = 25*(jSample - (int) nSamples/2);
			  std::vector<int> templinkADCs = getLink( (*allSamplesTell1Data[jSample])[aLink]);
			  std::vector<double> templinkCMS = SubtractCommonMode(templinkADCs, m_cmsCut);
        if(m_tryCluster==true){
          double clustersum = GetClusterADC(channelNumber, allSamplesTell1Data[jSample]);
          pulse->Fill(time,clustersum);
        }
        else if(m_tryCluster==false){
          pulse->Fill(time,templinkCMS[channel]);
        }
				//info() << " time == " << time << " adc " <<  templinkCMS[channel] << endmsg;
			}
      double amplitude = pulse->GetMaximum();
      double peaktime = pulse->GetXaxis()->GetBinCenter(pulse->GetMaximumBin());
      double integral = (double) pulse->Integral(); 
      if(integral > m_integralCut &&  fabs(peaktime) < m_timeWindow && amplitude > m_threshold){
			  sprintf(finepulseshape,"PulseShapeClustersVsTime_%d",sensNumber);
        std::string ffinepulseshape = finepulseshape;
			  for(int isample =0; isample <nSamples; ++isample){
          double time = 25*(isample - (int) nSamples/2) + delayValue;
          //info() << " time2 == " << time << " adc2 " << pulse->GetBinContent(isample+1) << endmsg;
          
          plot2D(time,pulse->GetBinContent(isample+1),ffinepulseshape,ffinepulseshape,
                 -25*(nSamples/2)-0.25,25*(nSamples/2 + 1)+0.25, -127.5, 128.5, 50*nSamples+1, 256);			
          profile1D(time,pulse->GetBinContent(isample+1),ffinepulseshape+"prof",ffinepulseshape+"prof",
                    -25*(nSamples/2)-0.25,25*(nSamples/2 + 1)+0.25,  50*nSamples+1);		
          profile1D(isample*25. +delayValue, pulse->GetBinContent(isample+1), ffinepulseshape+"check", 
                    ffinepulseshape+"check", -0.25-((int) nSamples/2)*25, - 0.25 +((int) nSamples/2)*25, 50*nSamples);
			  }
			}   
		  }
		  pulse->Reset();
		}
	  }
	}
#endif
  }
#ifndef WIN32
  delete pulse;
#endif
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::PulseShapeScan::finalize() {
  if ( m_debugLevel ) debug() << "==> Finalize" << endmsg;
  return VeloMonitorBase::finalize(); // must be called after all other actions
}

//=============================================================================
// Retrieve the VeloTell1Data
//=============================================================================
LHCb::VeloTELL1Datas*
Velo::PulseShapeScan::veloTell1Data( std::string samplingLocation ) {
  std::string tesPath;
  if(samplingLocation == "") tesPath = samplingLocation + "Raw/Velo/DecodedADC"; 
  else  tesPath = samplingLocation + "/Raw/Velo/DecodedADC";
  size_t pos = tesPath.find( "//" );
  if ( pos != std::string::npos )    tesPath.replace( pos, 2, "/" );
  if ( m_debugLevel ) 	debug() << "Retrieving VeloTell1Data from " << tesPath << endmsg;
  if ( !exist<LHCb::VeloTELL1Datas>( tesPath ) ) {
	debug() << "No VeloTell1Data container found for this event !" << endmsg; 
	if (m_nEvents %100==1  )  info() << "No VeloTell1Data container found for this event !   " << tesPath << endmsg; 
	return 0;
  }
  else {
	LHCb::VeloTELL1Datas* tell1Data = get<LHCb::VeloTELL1Datas>( tesPath );
	if ( m_debugLevel ) debug() << "  -> number of VeloTell1Data found in TES: "
	  << tell1Data->size() <<endmsg;
	return tell1Data;
  }
}
//=============================================================================
std::vector<double>  Velo::PulseShapeScan::SubtractCommonMode( std::vector<int> data, double cut = 15.0)
{
  int howManyPasses =3;
  std::vector<double> commonModeSubtractedData;
  std::vector<int>::iterator it;
  std::vector<double>::iterator itf;
  double average = 0;
  int count = 0, chanWithoutHits = 0;  double averageWithoutHits = 0;
  for(it=data.begin();it!=data.end() ; ++it){
	average += float(*it);
  }
  average /= float(data.size());
  for(it=data.begin();it!=data.end() ; ++it){
	commonModeSubtractedData.push_back((double)(*it) - average);
  }
  int pass=0;
  while(pass < howManyPasses){
	chanWithoutHits=0; averageWithoutHits =0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf){
	  if(fabs(*itf) < cut )  {
		averageWithoutHits += *itf;
		chanWithoutHits++;
	  }
	}
	if(chanWithoutHits > 0) averageWithoutHits /= double(chanWithoutHits);
	else averageWithoutHits = 0;
	count = 0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf, ++count){
	  commonModeSubtractedData[count]  = *itf - averageWithoutHits;
	}
	pass++;
  }
  return commonModeSubtractedData;
}
double Velo::PulseShapeScan::GetClusterADC( int seedChannel, LHCb::VeloTELL1Data *tell1data){
  if(seedChannel < 0 || seedChannel >2047) {
	info() << "channel does not exist! " << endmsg;
  }
  double clusterADC = 0;
  int stripNumber = m_stripFromChannel[m_sensorType][seedChannel];
  for(int ineighbour = -m_takeNeighbours; ineighbour <= m_takeNeighbours; ++ineighbour ){ 
	if(stripNumber+ineighbour >= 0 && stripNumber +ineighbour  <2048){
	  int currentstrip =  stripNumber+ineighbour;
	  int currentchannel = m_channelFromStrip[m_sensorType][currentstrip]; 
	  int channelnumber = (int)(currentchannel%32);
	  int aLink = (int)(currentchannel/32.);
	  std::vector<int> linkADCs = getLink( (*tell1data)[aLink]);
	  std::vector<double> linkCMS = SubtractCommonMode(linkADCs, m_cmsCut); 
	  double channeladc = linkCMS[channelnumber];
      ///if(ineighbour ==0 ) info() << "seedADC == " <<  channeladc <<  endmsg ;  
	  clusterADC += channeladc; 
	} 
  }
 // info() << " total cluster adc = " << clusterADC << endmsg;
  return clusterADC;
}
 













