// $Id: PulseShapeScan.h,v 1.1 2009-07-15 18:00:04 kakiba Exp $
#ifndef VELORECMONITORS_VELOSAMPLINGMONITOR_H 
#define VELORECMONITORS_VELOSAMPLINGMONITOR_H 1
#include "GaudiKernel/IEventTimeDecoder.h"            // Interface for odin
#include "Event/ODIN.h"

// Include files
// -------------
#include "VeloDet/DeVelo.h"

// from VeloEvent
#include "VeloEvent/VeloTELL1Data.h"

// from DigiEvent
#include "Event/VeloCluster.h"

// local
#include "VeloMonitorBase.h"

#ifndef WIN32
#include <TH1D.h>
#include <TF1.h>
#endif

#include "Math/ParamFunctor.h"


/** @class PulseShapeScan PulseShapeScan.h
 *  
 *
 *  @author Kazu  
 *  @date   2008-10-22
 */

namespace Velo {


  double fitsimple(double *x, double *par);

  class PulseShapeScan : public VeloMonitorBase {
  public: 
    /// Standard constructor
    PulseShapeScan( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~PulseShapeScan( );    ///< Destructor
    virtual StatusCode initialize() override;    ///< Algorithm initialization
    virtual StatusCode execute   () override;    ///< Algorithm execution
    virtual StatusCode finalize  () override;    ///< Algorithm finalization
    long          m_nEvents;
    double        m_threshold;
    double m_offset;
    double m_width1;
    double m_width2;
    double m_chisqCut;
    double m_cmsCut;

    double m_integralCut;
    double m_timeWindow;
    int m_runNumber;
    bool  m_fixedOffset;
    bool  m_tryCluster;
    bool  m_plotSeparateStrips;
    double m_amplitudeCut;
    double m_offsetCut;
    bool  m_useLogLikeFit;
    bool  m_fakeit;
    bool  m_runWithOdin;
    int           m_nSteps;
    int           m_shift;
    int           m_stepSize;
    int           m_runOverFewEvents;
    int           m_skipEvents;
    
    int m_takeNeighbours;
    int m_wrapPoint;
    int  m_wrappedSteps;
    int tell1smatch[5],tell1sorder[88];
    int m_stripFromChannel[2][2048];
    int m_channelFromStrip[2][2048];
    int m_sensorType;
    //double coarsepulse[88][2048][7];
    bool m_inverted, m_detailedOutput;
    std::vector<int> m_chosenTell1s;
    std::vector<int> m_pulsedChannels;
    //std::vector<std::vector<bool>>; 
	double subtractCommonMode(int channelNumber,  LHCb::VeloTELL1Data *tell1data);
	std::vector<double> SubtractCommonMode( std::vector<int> data, double cut );
    double GetClusterADC( int seedChannel, LHCb::VeloTELL1Data *tell1data);

 

    double        m_thresholdForCMS;

  protected:
  private:
      std::vector<int> m_runSteps;
      std::vector<double> m_delayList;
      int m_stepId;

	  IEventTimeDecoder* m_timeDecoder; ///< Pointer to tool to decode ODIN bank

    // Retrieve the VeloTell1Data
    LHCb::VeloTELL1Datas* veloTell1Data( std::string samplingLocation );
    // Monitor the VeloTell1Data
    //void MakePulseShape( std::string samplingLocation, LHCb::VeloTELL1Datas* tell1Datas );
#ifndef WIN32
    TF1 *func;
    TH1D *pulse;
#endif
	std::vector<int> getLink(VeloTELL1::ALinkPair begEnd) {
	  std::vector<int>::const_iterator iT; //iterator to vector of unsigned int.
	  std::vector<int> linkVector;
	  linkVector.clear();
	  //Loop over ADC values in link
	  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
		linkVector.push_back(*iT); //Push back ADC value to vector
	  }
	  return linkVector;
	} 

    // Data members
    const DeVelo* m_velo;
    // Job options
    std::vector<std::string> m_samplingLocations;
  };
}

#endif // VELORECMONITORS_VELOSAMPLINGMONITOR_H
