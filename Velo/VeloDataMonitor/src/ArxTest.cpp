// Include files 
#include <math.h>

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/AlgFactory.h" 

// from ROOT
#include "TMath.h"

// local
#include "ArxTest.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ArxTest
//
// 2006-07-06 : 
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, ArxTest )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::ArxTest::ArxTest( const std::string& name,
    ISvcLocator* pSvcLocator)
  : GaudiHistoAlg( name , pSvcLocator )
  , m_adcValuesInOneTELL1( 64, std::vector<int>(32,0))  
  , m_hdrValuesInOneTELL1( 64, std::vector<int>(4,0))  
  , m_nChannelsOnTell1(VeloTELL1Data::SENSOR_ALINKS*VeloTELL1Data::ADC_PER_ALINK)
  , m_nChannelsOnFPGA(m_nChannelsOnTell1/(VeloTELL1Data::SENSOR_ALINKS/VeloTELL1Data::FPGA_ALINKS))
  , m_nHeadersOnTell1(VeloTELL1Data::SENSOR_ALINKS*VeloTELL1Data::HEADERS)
  , m_nHeadersOnFPGA(m_nHeadersOnTell1/(VeloTELL1Data::SENSOR_ALINKS/VeloTELL1Data::FPGA_ALINKS))
{
  declareProperty("PlotHistos", m_plotHistos=false);
  declareProperty("PlotHistosDebug", m_plotHistosDebug=false);
  declareProperty("PlotArxTest", m_plotArxTest=false);
  declareProperty("ThresholdActiveLink", m_thresholdActiveLink=0);
  declareProperty("ThresholdNoisyLink", m_thresholdNoisyLink=0);
  declareProperty("GlobalPedestal", m_globalPedestal=512);
  declareProperty("ListMaskedLinks",m_maskedLinks);
  declareProperty("NumberDebugLink", m_numberDebugLink=-1);
  declareProperty("ChosenTell1Number", m_tell1Number=-1);
  declareProperty("LinkOrder", m_linkOrder);
  declareProperty("ADCDataLocation", m_adcDataLocation=LHCb::VeloTELL1DataLocation::ADCs );
  declareProperty("HeaderDataLocation", m_hdrDataLocation=LHCb::VeloTELL1DataLocation::Headers );
  m_lastActiveLink = -1;
  m_nEvtsDebug = 0;
  m_hfit = new TH1D("m_hfit","link",36,-4.5,31.5);
  double xFitMin = m_hfit->GetXaxis()->GetXmin();
  double xFitMax = m_hfit->GetXaxis()->GetXmax();
  m_sineFit = new TF1("sineFit","[0]*sin([1]*x+[2])+[3]",xFitMin,xFitMax);//"sineFit","[0]*sin([1]*x+[2])+[3]
}

//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::ArxTest::~ArxTest() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::ArxTest::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  // link order is reversed by default
  if ( m_linkOrder.empty() ) {
    m_linkOrder.push_back(3);
    m_linkOrder.push_back(2);
    m_linkOrder.push_back(1);
    m_linkOrder.push_back(0);
  }

  if ( m_linkOrder.size() != 4 ) {
    error() << "LinkOrder property must have exactly 4 entries!" << endmsg;
    return StatusCode::FAILURE;
  }
  
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::ArxTest::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiHistoAlg::finalize();  // must be called after all other actions
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::ArxTest::execute() {

  debug() << "==> Execute" << endmsg;

  StatusCode sc = getData();
  if ( sc.isFailure() ) return sc;

  if(m_plotHistos){
    if (m_plotArxTest) runArxTest();
  }
  //
  return StatusCode::SUCCESS;
}

//=============================================================================
// Fetch data from TES
//=============================================================================
StatusCode Velo::Monitoring::ArxTest::getData()
{
  StatusCode sc = StatusCode::SUCCESS;
  
  VeloTELL1Datas* adcContainer;
  VeloTELL1Datas* hdrContainer;

  // retrieve input containers, bail out if they can't be found
  if(!exist<VeloTELL1Datas>(m_adcDataLocation)){
    info()<< " ==> There is no bank at " << m_adcDataLocation <<endmsg;
    return StatusCode::FAILURE; // this is an expert test algorithm, ok to fail. 
  }else{
    adcContainer = get<VeloTELL1Datas>( m_adcDataLocation );
  }
  
  if(!exist<VeloTELL1Datas>(m_hdrDataLocation)){
    info()<< " ==> There is no bank at " << m_hdrDataLocation <<endmsg;
    return StatusCode::FAILURE; // this is an expert test algorithm, ok to fail.
  }else{
    hdrContainer = get<VeloTELL1Datas>( m_hdrDataLocation );
  }
  
  // retrieve channel ADC values
  bool tell1FoundADC = false;
  VeloTELL1Datas::const_iterator sensIt;
  for(sensIt=adcContainer->begin();sensIt!=adcContainer->end();++sensIt){

    VeloTELL1Data* dataOneTELL1=(*sensIt);

    int tell1Number=dataOneTELL1->key();
    debug() << "Found channel ADC container for TELL1 " << tell1Number << endmsg;
    if( m_tell1Number != tell1Number) continue;

    tell1FoundADC = true;
    
    //debug() << "Channel ADC values:"; 
    for(unsigned int channel=0; channel<m_nChannelsOnTell1; ++channel){
      int value=dataOneTELL1->decodedData()[channel];
      //int value=dataOneTELL1->channelADC(channel);
      //int link = (m_linkOrder[channel/m_nChannelsOnFPGA])*VeloTELL1Data::FPGA_ALINKS 
      //  + (channel%m_nChannelsOnFPGA)/VeloTELL1Data::ADC_PER_ALINK;
      //int linkChannel = channel%VeloTELL1Data::ADC_PER_ALINK;
      //m_adcValuesInOneTELL1[link][linkChannel] = value;
      int link = channel/32;
      int linkChannel = channel%32;
      m_adcValuesInOneTELL1[link][linkChannel] = value;
      //debug() << " " << value;
    }
    //debug() << endmsg;
  }

  // retrieve header ADC values
  bool tell1FoundHdr = false;
  for(sensIt=hdrContainer->begin();sensIt!=hdrContainer->end();++sensIt){

    VeloTELL1Data* dataOneTELL1=(*sensIt);

    int tell1Number=dataOneTELL1->key();
    debug() << "Found header ADC container for TELL1 " << tell1Number << endmsg;
    if( m_tell1Number != tell1Number) continue;

    tell1FoundHdr = true;
   
    //debug() << "Header ADC values:"; 
    for(unsigned int headerIndex=0; headerIndex<m_nHeadersOnTell1; ++headerIndex){
      int value=dataOneTELL1->decodedData()[headerIndex];
      //int link = (m_linkOrder[headerIndex/m_nHeadersOnFPGA])*VeloTELL1Data::FPGA_ALINKS 
      //  + (headerIndex%m_nHeadersOnFPGA)/VeloTELL1Data::HEADERS;
      //int linkHeaderByte = headerIndex%VeloTELL1Data::HEADERS;
      //m_hdrValuesInOneTELL1[link][linkHeaderByte] = value;
      int link = headerIndex/4;
      int linkHeaderByte = headerIndex%4;
      m_hdrValuesInOneTELL1[link][linkHeaderByte] = value;
      //debug() << " " << value;
    }
    //debug() << endmsg;
  }
  
  if ( !tell1FoundADC ) {
    error() << "Could not retrieve ADC container for TELL1 " 
      << m_tell1Number << endmsg;
    sc = StatusCode::FAILURE;
  }
  
  if ( !tell1FoundHdr ) {
    error() << "Could not retrieve header container for TELL1 " 
      << m_tell1Number << endmsg;
    sc = StatusCode::FAILURE;
  }
  
  return sc;
}

void Velo::Monitoring::ArxTest::runArxTest()
{
  std::vector<int> iArxCard;
  std::vector<int> iBeetle;
  std::vector<int> iLink;
  for(int i=0; i<64; i++){
    iArxCard.push_back( (int)(i/16) );
    iBeetle.push_back( (int)((i-iArxCard[i]*16)/4) );
    iLink.push_back(   (int)((i-iArxCard[i]*16-iBeetle[i]*4)) );
  }

  int yvalInt;
  double xval, yval, weight, sumResidualsLink;

  char name[50], name2[50];
  char title[100], title2[100];
  int nHeaders = m_hdrValuesInOneTELL1[0].size();
  int nChannels = m_adcValuesInOneTELL1[0].size();
  int nChannelsPlusHeaders = m_hdrValuesInOneTELL1[0].size() + m_adcValuesInOneTELL1[0].size();
  int nLinks = m_hdrValuesInOneTELL1.size();
  int iActiveLink = getActiveLink(nLinks);

  // info()<< " Active link: " << iActiveLink <<endmsg;

  // Integrated ADC counts per link

  sprintf(name,"hIntegratedAdcCountsPerLink");
  sprintf(title,"Integrated ADC counts per link");

  for(int i=0; i<nLinks; i++){
    xval = (double) sumAdcCountsLink(i);
    plot(xval,name,title,0.,20000,200,1.);
  }

  // Number of events per active link 

  sprintf(name,"hEventsPerActiveLink");
  sprintf(title,"Events per active link");
  xval = (double) iActiveLink;
  plot(xval,name,title,-0.5,((double) (nLinks-0.5)),nLinks,1.);

  if (iActiveLink != -1) {

    // Fill channel histograms for active link

    sprintf(name,"hAdcVsChannel_Link_%d",iActiveLink);
    sprintf(title,"ADC counts vs channel for card %d, Beetle %d, Link %d",iArxCard[iActiveLink],iBeetle[iActiveLink],iLink[iActiveLink]);

    const std::vector< int >& hdrValuesInOneALink = m_hdrValuesInOneTELL1[iActiveLink];
    const std::vector< int >& adcValuesInOneALink = m_adcValuesInOneTELL1[iActiveLink];

    for(unsigned int i=0;i<hdrValuesInOneALink.size();i++){
      xval = (double) (((int) i)-4);
      weight = (double) (hdrValuesInOneALink[i]);
      //      info() << "xval: " << xval <<", weight: " << weight << endmsg;
      m_hfit->Fill(xval,weight);
      plot2D(xval,weight,name,title,((double) -nHeaders)-0.5,((double) nChannels)-0.5,-0.5,1028.5,nChannelsPlusHeaders,1029);
    }

    for(unsigned int i=0;i<adcValuesInOneALink.size();i++){
      xval = (double) ((int) i);
      weight = (double) (adcValuesInOneALink[i]);
      //      info() << "xval: " << xval <<", weight: " << weight << endmsg;
      m_hfit->Fill(xval,weight);
      plot2D(xval,weight,name,title,((double) -nHeaders)-0.5,((double) nChannels)-0.5,-0.5,1028.5,nChannelsPlusHeaders,1029);
    }

    // fill histogram showing the number of events for which each bit is high

    sprintf(name,"hBitHighOccupancy_Link_%d",iActiveLink);
    for(int i=1;i<m_hfit->GetNbinsX()+1;i++){
      yval = m_hfit->GetBinContent(i);
      yvalInt = (int) yval;
      // info() << "yval: " << yval << ", yvalInt: " << yvalInt << endmsg;
      for(int j=0;j<10;j++){
        if (((yvalInt >> j) & 1) == 1) {
          // info() << "Bit " << j << " high!" << endmsg;
          plot(((double) j),name,title,-0.5,9.5,10,1.);
        }
      }
    }

    // fill debug histograms

    if (m_plotHistosDebug && (iActiveLink == m_numberDebugLink) && m_nEvtsDebug < 10) {
      m_nEvtsDebug++;
      sprintf(name,"hAdcVsChannel_Link_%d_Event_%d",iActiveLink,m_nEvtsDebug);
      for(int i=1;i<m_hfit->GetNbinsX()+1;i++){
        xval = m_hfit->GetBinCenter(i);
        yval = m_hfit->GetBinContent(i);
        info() << "xval: " << xval << ", yval: " << yval << endmsg;
        plot(xval,name,title,m_hfit->GetXaxis()->GetXmin(),m_hfit->GetXaxis()->GetXmax(),m_hfit->GetNbinsX(),yval);
      }
    }

    fitSine();

    sprintf(name,"hSineAmp_Link_%d",iActiveLink);
    sprintf(title,"Sine amplitude for card %d, Beetle %d, Link %d",iArxCard[iActiveLink],iBeetle[iActiveLink],iLink[iActiveLink]);
    xval = m_sineFit->GetParameter(0);
    plot(xval,name,title,400.,600.,800,1.);

    sprintf(name,"hSineFreq_Link_%d",iActiveLink);
    sprintf(title,"Sine frequence for card %d, Beetle %d, Link %d",iArxCard[iActiveLink],iBeetle[iActiveLink],iLink[iActiveLink]);
    xval = (m_sineFit->GetParameter(1))/2/TMath::Pi()/25.e-9/1.e6;
    plot(xval,name,title,0.95,1.05,200,1.);

    sprintf(name,"hSinePhase_Link_%d",iActiveLink);
    sprintf(title,"Sine phase for card %d, Beetle %d, Link %d",iArxCard[iActiveLink],iBeetle[iActiveLink],iLink[iActiveLink]);
    xval = m_sineFit->GetParameter(2);
    plot(xval,name,title,-10.,10.,200,1.);

    sprintf(name,"hSineOffset_Link_%d",iActiveLink);
    sprintf(title,"Sine offset for card %d, Beetle %d, Link %d",iArxCard[iActiveLink],iBeetle[iActiveLink],iLink[iActiveLink]);
    xval = m_sineFit->GetParameter(3);
    plot(xval,name,title,400.,600.,800,1.);

    // calculate residuals w.r.t. the sine function

    sprintf(name,"hResiduals_Link_%d",iActiveLink);
    sprintf(title,"Residuals w.r.t. sine for card %d, Beetle %d, Link %d",iArxCard[iActiveLink],iBeetle[iActiveLink],iLink[iActiveLink]);
    sprintf(name2,"hResidualsVsAdcCounts_Link_%d",iActiveLink);
    sprintf(title2,"Residuals vs ADC counts for card %d, Beetle %d, Link %d",iArxCard[iActiveLink],iBeetle[iActiveLink],iLink[iActiveLink]);
    sumResidualsLink = 0.;
    for(int i=1;i<m_hfit->GetNbinsX()+1;i++){
      //      info() << "bin center: " << m_hfit->GetBinCenter(i) << ", bin content: " << m_hfit->GetBinContent(i) << ", fit value: " << m_sineFit->Eval(m_hfit->GetBinCenter(i)) << endmsg;
      xval = m_sineFit->Eval(m_hfit->GetBinCenter(i));
      yval = m_hfit->GetBinContent(i) - m_sineFit->Eval(m_hfit->GetBinCenter(i));
      plot(yval,name,title,-20.,20.,400,1.);
      plot2D(xval,yval,name2,title2,-0.5,1028.5,-10.,10.,1029,100);
      sumResidualsLink += TMath::Abs(yval); 
    }

    if (sumResidualsLink > 1000.) {

      for(int i=1;i<m_hfit->GetNbinsX()+1;i++){
        info() << "link " << iActiveLink << ", bin " << i << ": center: " << m_hfit->GetBinCenter(i) << ", content: " << m_hfit->GetBinContent(i) << ", fit value: " << m_sineFit->Eval(m_hfit->GetBinCenter(i)) << endmsg;
      }
    }

    m_hfit->Reset();
  }

  // getchar();

}


int Velo::Monitoring::ArxTest::sumAdcCountsLink(int iLink) 
{

  int adcCountSum = 0;

  const std::vector< int >& hdrValuesInOneALink = m_hdrValuesInOneTELL1[iLink];
  const std::vector< int >& adcValuesInOneALink = m_adcValuesInOneTELL1[iLink];

  //debug() << "----> Header ADC counts on link " << iLink << ":" << endmsg;
  for(unsigned int i=0;i<hdrValuesInOneALink.size();i++){
    //debug() << " " << hdrValuesInOneALink[i];
    adcCountSum += abs(hdrValuesInOneALink[i] - m_globalPedestal);
  }
  //debug() << endmsg;
  //debug() << "<----" << endmsg;

  //debug() << "----> Channel ADC counts on link " << iLink << ":" << endmsg;
  for(unsigned int i=0;i<adcValuesInOneALink.size();i++){
    //debug() << " " << adcValuesInOneALink[i];
    adcCountSum += abs(adcValuesInOneALink[i] - m_globalPedestal);
  }
  //debug() << endmsg;
  //debug() << "<----" << endmsg;

  return adcCountSum;

}


int Velo::Monitoring::ArxTest::getActiveLink(int nLinks)
{

  bool lastStillActive = false;
  bool linkMasked;
  int adcCountSum;
  double xval;
  char name[50], title[100];

  if (m_lastActiveLink != -1) {
    if (sumAdcCountsLink(m_lastActiveLink) >= m_thresholdActiveLink) {
      if (sumAdcCountsLink(m_lastActiveLink) <= m_thresholdNoisyLink) {
        lastStillActive = true;
      }
    }
  }

  if (lastStillActive) {
    return m_lastActiveLink;
  } else {
    for(int i=0;i<nLinks;i++){
      linkMasked = false;
      for(unsigned int j=0;j<m_maskedLinks.size();j++){
        if (m_maskedLinks[j] == i) {
          linkMasked = true;
        }
      }
      if (!linkMasked) {
        adcCountSum = sumAdcCountsLink(i);
        if (adcCountSum >= m_thresholdActiveLink) {
          if (adcCountSum <= m_thresholdNoisyLink) {
            m_lastActiveLink = i;
            info() << "Found link " << i << " to be active!" << endmsg;
            return i;
          } else {
            sprintf(name,"hIndicationNoisyLinks");
            sprintf(title,"Indication of noisy links");
            xval = (double) i;
            plot(xval,name,title,-0.5,((double) (nLinks-0.5)),nLinks,1.);
          }
        }
      }
    }
    m_lastActiveLink = -1;
    return -1;
  }
}

void Velo::Monitoring::ArxTest::fitSine() 
{

  const int slidingWindowWidth = 2;
  const int arrayLength = 2*slidingWindowWidth+1;
  int actAdcValues[arrayLength]; 
  bool backwardEqualSign, forwardEqualSign;
  int iBinRoot = -1, iBinExtremum = -1;
  double binContent, average=0., actAmplitude, maxAmplitude=0.;
  double actAdc, period, maximum = 0;
  double angularFrequency, phase;

  // m_sineFit->Reset();

  // estimate initial values ...

  // ... offset and amplitude

  for(int i=0;i<m_hfit->GetNbinsX();i++) {
    binContent = m_hfit->GetBinContent(i);
    average += binContent;
    actAmplitude = TMath::Abs(binContent-((double) m_globalPedestal));
    if (actAmplitude > maxAmplitude) {
      maxAmplitude = actAmplitude;
    }
  }

  average = average/((double) m_hfit->GetNbinsX());

  // ... root

  for(int i=1;i<m_hfit->GetNbinsX()+1;i++) {
    // info() << "bin center: " << m_hfit->GetBinCenter(i) << ", bin content: " << (m_hfit->GetBinContent(i)-m_globalPedestal) << endmsg;
  }

  for(int i=slidingWindowWidth+1;i<(m_hfit->GetNbinsX()+1-slidingWindowWidth);i++) {
    for(int j=0;j<arrayLength;j++){
      actAdcValues[j] = ((int) (m_hfit->GetBinContent(i-slidingWindowWidth+j))) - m_globalPedestal;
      // info() << "ADC value for j=" << j << " :" << actAdcValues[j] << endmsg;
    }
    backwardEqualSign = true;
    forwardEqualSign = true;
    for (int j=1;j<slidingWindowWidth;j++) {
      // info() << "j backward: " << j << endmsg;
      if (actAdcValues[j]*actAdcValues[0] < 0) {
        backwardEqualSign = false;
      }
    }
    for (int j=arrayLength-2;j>(arrayLength-slidingWindowWidth-1);j--) {
      // info() << "j forward: " << j << endmsg;
      if (actAdcValues[j]*actAdcValues[arrayLength-1] < 0) {
        forwardEqualSign = false;
      }
    }
    if (backwardEqualSign && forwardEqualSign && ((actAdcValues[0]*actAdcValues[arrayLength-1]) < 0)) {
      // info() << "root found in bin: " << i << endmsg;
      iBinRoot = i;
      break;
    }
  }

  if (TMath::Abs(actAdcValues[slidingWindowWidth+1]) < TMath::Abs(actAdcValues[slidingWindowWidth])) {
    iBinRoot++;
  }
  // info() << "ibinRoot: " << iBinRoot << endmsg;

  if (iBinRoot != -1) {

    // ... period

    for(int i=iBinRoot;i<m_hfit->GetNbinsX()+1;i++) {
      actAdc = TMath::Abs(m_hfit->GetBinContent(i) - ((double) m_globalPedestal));
      if (actAdc >= maximum) {
        maximum = actAdc;
      } else {
        iBinExtremum = i;
        break;
      }
    }

    // info() << "extremum found in bin: " << iBinExtremum << endmsg;

    // ... calculate angular frequency from period

    period = (double) ((iBinExtremum - iBinRoot) * 4);
    // info() << "period corresponds to " << period << " channels: " << endmsg;
    angularFrequency = 2*TMath::Pi()/period;
    // info() << "angular frequency: " << angularFrequency << endmsg;

    // ... calculate phase from root

    phase = (m_hfit->GetBinCenter(iBinRoot))/period*2*TMath::Pi();
    if (actAdcValues[0] > 0) {
      phase += TMath::Pi();
    }
    phase = 2*TMath::Pi() - phase;
    // info() << "phase: " << phase << endmsg;
  } else {
    angularFrequency = 0.14;
    phase = 1.;
  }

  // fit channel histogram with sine function

  // info() << "x-range: " << xFitMin << " - " << xFitMax << endmsg;
  // info() << "average: " << average << endmsg;
  // info() << "maximum amplitude: " << maxAmplitude << endmsg;
  m_sineFit->SetParameters(maxAmplitude,angularFrequency,phase,average);
  m_sineFit->SetParLimits(0,(maxAmplitude-100.),(maxAmplitude+100.));
  m_sineFit->SetParLimits(1,(angularFrequency-0.25*angularFrequency),(angularFrequency+0.25*angularFrequency));
  if (iBinRoot != -1) {
    m_sineFit->SetParLimits(2,(phase-TMath::Pi()/4.),(phase+TMath::Pi()/4.));
  }
  m_sineFit->SetParLimits(3,(average-100.),(average+100.));
  m_hfit->Fit("sineFit","LQ0"); //originally "LQO"

  // m_sineFit->Print();

}

