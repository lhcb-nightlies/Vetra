// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/AlgFactory.h" 
// ============================================================================

// from Event model
#include "VeloEvent/VeloTELL1Data.h"

//velo
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

// local
#include "CommonFunctions.h"

// AIDA 
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"

// from TELL1 Kernel:
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "Tell1Kernel/VeloDecodeConf.h" 

// Boost 
// ============================================================================
#include "boost/format.hpp"
// ============================================================================

//-----------------------------------------------------------------------------
// Class : Velo::Monitoring::ADCMon
//
// 2007-11-23 Aras Papadelis, arasp@nikhef.nl
// Monitoring of ADC values from the VELO.
//  *Plots are made for all TELL1s by default, unless the user specifies
//   TELL1s={1, 2, 3} in the options files.
//  *Histograms reset and refresh can be set with 
//   RefreshRate=5000 (every 5000 events)  Only applies to profile histograms.
//   Set RefreshRate<0 to NEVER reset histograms.
//  *Profile histograms are always made, memory consuming 2D histograms can be switched 
//   on with the option Plot2DHistos = true;
//  *All TELL1 banks are monitored by default. Use option
//   Containers = {"Raw/Velo/DecodedADCs","Raw/Velo/ReorderedADCs", ...};
//  *For test pulse operation the option SplitInEvenOdd is useful. Even and odd
//   events will be split into different histograms. 
//   
//-----------------------------------------------------------------------------

using namespace LHCb;
using namespace VeloTELL1;
namespace Velo
{
  namespace Monitoring
  {
	class ADCMon : public GaudiHistoAlg {
	  public:
		/// Standard constructor
		ADCMon( const std::string& name, ISvcLocator* pSvcLocator );
		virtual ~ADCMon( ); ///< Destructor
		virtual StatusCode initialize() override;    ///< Algorithm initialization
		virtual StatusCode execute   () override;    ///< Algorithm execution
		virtual StatusCode finalize  () override;    ///< Algorithm finalization
		StatusCode plotADCs();
                StatusCode plotHeadersAndADCs();
	  private  :
		unsigned long int m_evtNumber;
		int m_refresh;
		DeVelo* m_veloDet;
		std::vector<int> m_chosenTell1s;
		bool m_plot2DADCs;
    int  m_maxEvts;
		std::vector<std::string>  m_locations;
		bool m_evenodd;
                bool m_plotHeadersAndADCs;
		bool OddEvent() {if( m_evtNumber%2==1) return  true; else return false; }
		bool EvenEvent() {if( m_evtNumber%2==0) return  true; else return false; }
	//	bool EvenEvent() { return ( m_evtNumber%2==0 ? true : false ); }
	};
  }
}


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::ADCMon::ADCMon( const std::string& name,
	ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
  ,  m_evtNumber(0)
  ,  m_refresh (5000)
  ,  m_veloDet ( 0 )
,  m_locations ()
{
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::ADCs             ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::PedSubADCs       ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::FIRCorrectedADCs ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ) ;
  declareProperty( "Containers" ,   m_locations  ,  "The list of TES locations to be monitored") ;
  declareProperty( "TELL1s",               m_chosenTell1s);
  declareProperty( "RefreshRate",          m_refresh);
  declareProperty( "Plot2DHistos",         m_plot2DADCs=false);
  declareProperty( "PlotHeadersAndADCs",   m_plotHeadersAndADCs = false);
  declareProperty( "SplitInEvenOdd",       m_evenodd=false);
  declareProperty( "MaxEvts",              m_maxEvts = -1);

  setProperty ( "PropertiesPrint", true ).ignore();
}

//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::ADCMon::~ADCMon() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::ADCMon::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( propsPrint() ) printProps();
  debug() << "==> Initialize" << endmsg;
  setHistoTopDir( "Vetra/" );
  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );
  if(m_veloDet == 0) Error("Can't fint detector element. Exiting with Failure",StatusCode::FAILURE);
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::ADCMon::finalize() {
  debug() << "==> Finalize" << endmsg;  
  return GaudiHistoAlg::finalize();  // must be called after all other actions
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::ADCMon::execute() {
  debug() << "==> Execute" << endmsg;  
  if ( ( m_evtNumber < (unsigned) m_maxEvts ) || ( m_maxEvts == -1 ) ) {
          if (m_plotHeadersAndADCs) {
                  plotHeadersAndADCs();  
          } else {
                  plotADCs();  
          }
  }
  ++m_evtNumber;    
  return StatusCode::SUCCESS;
}
//==============================================================================================================
//==============================================================================================================
StatusCode Velo::Monitoring::ADCMon::plotADCs(){
  VeloTELL1Datas* m_tell1DataContainer;
  //Loop over input containers:
  for ( std::vector<std::string>::const_iterator il = m_locations.begin() ;  m_locations.end() != il ; ++il ) {
	if(!exist<VeloTELL1Datas>( *il)){
	  debug()<< " ==> There is no bank at " << *il <<endmsg;
	  continue;
	}else{
	  m_tell1DataContainer = get<VeloTELL1Datas>( *il );
	}
	VeloTELL1Datas::const_iterator sensIt;
	for(sensIt=m_tell1DataContainer->begin();sensIt!=m_tell1DataContainer->end();++sensIt){
	  VeloTELL1Data* dataOneTELL1=(*sensIt);
	  int TELL1_number=dataOneTELL1->key();
	  if(ExcludeTELL1(m_chosenTell1s,TELL1_number)) continue;
	  //Get sensor from detector element:  
	  const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(TELL1_number);
	  if(sensor == 0 ){
		error() << "Can't find a sensor associated with TELL1 " << TELL1_number << ". Aborting..." << endmsg;
		return StatusCode::FAILURE;
	  }         
	  //Set up directory structure and histogram names:    
	  boost::format fmt ( "%s/TELL1_%03d/ADCs_vs_");
	  fmt %  strippedContainerName(*il) % TELL1_number;           
	  boost::format title ( ", TELL1 %d, sensor %d" );
	  title %  TELL1_number % sensor->sensorNumber();
	  //Book histograms explicitly. This gives easy access to the histogram 
	  //pointer so that the histogram can be reset.        
	  AIDA::IProfile1D* histo2chip = bookProfile1D(fmt.str() + "ChipChannel_prof"  , "ADCs vs chip channel" + title.str()  , -0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS);
	  AIDA::IProfile1D* histo2strip = bookProfile1D(fmt.str() +"Strip_prof"  , "ADCs vs strip" + title.str()  , -0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS);

	  AIDA::IProfile1D* histo2chipEven = 0;
	  AIDA::IProfile1D* histo2chipOdd = 0;
	  if(m_evenodd){
		histo2chipEven =  bookProfile1D(fmt.str() + "ChipChannelEven"  , "ADCs vs chip channel (Even)" + title.str()  , -0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS);
		histo2chipOdd = bookProfile1D(fmt.str() + "ChipChannelOdd"  , "ADCs vs chip channel (Odd)" + title.str()  , -0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS);
      }

	  //Reset the profile histograms after m_refresh events.
	  //This is handy for online monitoring.
	  if ( ( 0 != histo2chip && 0!= histo2strip ) && m_evtNumber % m_refresh == 0 && m_refresh>0 ) { 
		histo2chip -> reset() ; 
		histo2strip->reset();
	  } 
	  //Book memory consuming 2D histograms:
	  AIDA::IHistogram2D* histo1chip=0;
	  AIDA::IHistogram2D* histo1strip=0;
	  AIDA::IHistogram2D* h_even=0;
	  AIDA::IHistogram2D* h_odd=0;
	  //If the bank is non-pedestal subtracted the y-axis should have a different range.
	  double ylow=-128.5;
	  double yhigh=127.5;
	  if ( *il == LHCb::VeloTELL1DataLocation::ADCs || *il == LHCb::VeloTELL1DataLocation::Pedestals ) {
		ylow = 512-128.5;
		yhigh = 512 + 127.5 ; 
	  }
	  if(m_plot2DADCs==true && m_evenodd==false){
		histo1chip = book2D(fmt.str() + "ChipChannel",  "ADCs vs chip channel" + title.str(),	
                             -0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS,ylow,yhigh,256);
		histo1strip = book2D(fmt.str() +"Strip"  , "ADCs vs strip" + title.str(), 
                             -0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS, ylow,yhigh,256);
	  }	  
	  if(m_plot2DADCs==true && m_evenodd==true){
		h_even = book2D (fmt.str() + "ChipChannel_EVEN"  , "ADCs vs chip channel, even events" + title.str()  ,
			-0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS, ylow,yhigh,256);
		h_odd = book2D ( fmt.str() + "ChipChannel_ODD"  , "ADCs vs chip channel, odd events" + title.str()  ,
			-0.5,SENSOR_CHANNELS-0.5,  SENSOR_CHANNELS, ylow,yhigh,256);
	  }
	  //Fill all histograms:
	  for(int index=0;index<SENSOR_CHANNELS;++index){
		unsigned int channel=index;
		unsigned int strip = sensor->ChipChannelToStrip(channel);
		signed int value=dataOneTELL1->channelADC(channel);
                if(*il == LHCb::VeloTELL1DataLocation::Pedestals ) 
                        value=dataOneTELL1->pedestalADC(channel);
		if(dataOneTELL1->isReordered()){
		  channel=sensor->StripToChipChannel(static_cast<unsigned int> (index));
		  strip=static_cast<unsigned int> (index);
		  value=dataOneTELL1->stripADC(strip);
		}

		//2d histograms:
                //printf("ch %d val %d\n", channel, value);
		if( 0 != histo1chip && m_plot2DADCs ) { histo1chip->fill ( channel, value) ; }
		if( 0 != histo1strip && m_plot2DADCs ) { histo1strip->fill ( strip, value) ; }
		//Profile histograms:
		if( 0 != histo2chip ) { histo2chip->fill ( channel, value) ; }  
		if( 0 != histo2strip ) { histo2strip->fill ( strip, value) ; } 
		if(m_evenodd){
		  if( 0 != histo2chipEven && m_evtNumber%2==1) { histo2chipEven->fill ( channel, value) ; }  
		  if( 0 != histo2chipOdd && m_evtNumber%2==0 ) { histo2chipOdd->fill ( channel, value) ; }  

        } 
		if(m_plot2DADCs && m_evenodd){
		  if( 0 != h_even && EvenEvent()==true) { h_even->fill(channel,value) ; }
		  if( 0 != h_odd && OddEvent()==true) { h_odd->fill(channel,value) ; }
		}
	  }

	}            

  }

  return StatusCode::SUCCESS;

}

StatusCode Velo::Monitoring::ADCMon::plotHeadersAndADCs()
{
using namespace LHCb;
using namespace VeloTELL1;
        const int allchans = 2304;
        if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers )){
                debug()<< " ==> There is no Header data. QUITTING. " <<endmsg;
                return ( StatusCode::SUCCESS );
        }
        if(!exist<VeloTELL1Datas>( LHCb::VeloTELL1DataLocation::ADCs )){
                debug()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::ADCs <<endmsg;
                return StatusCode::SUCCESS;
        }
        VeloTELL1Datas* m_tell1DataContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::ADCs );
        VeloTELL1Datas* m_headerContainer= get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers );
        //VeloTELL1Datas::const_iterator headerIt = m_headerContainer->begin();
        VeloTELL1Datas::const_iterator sensIt;
        for(sensIt=m_tell1DataContainer->begin();sensIt!=m_tell1DataContainer->end();++sensIt){
                VeloTELL1Data* dataOneTELL1=(*sensIt);
                int TELL1_number=dataOneTELL1->key();
                if(ExcludeTELL1(m_chosenTell1s,TELL1_number)) continue;
                const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(TELL1_number);
                if(sensor == 0 ){
                        error() << "Can't find a sensor associated with TELL1 " << TELL1_number << ". Aborting..." << endmsg;
                        return StatusCode::FAILURE;
                }         
                //if (TELL1_number!=1) continue;
                VeloTELL1Data* headerOneTELL1=m_headerContainer->object(TELL1_number);
                std::vector< signed int > hdata = headerOneTELL1->data();
                //fprintf(stderr, "\ntell1=%d ", TELL1_number);
                int h=0;
                //int fullch = 0;
                //Set up directory structure and histogram names:    
                boost::format fmt ( "RawADCs/TELL1_%03d/ADCs_vs_");
                fmt %  TELL1_number;           
                boost::format title ( ", TELL1 %d, sensor %d" );
                title %  TELL1_number % sensor->sensorNumber();
                //Book histograms explicitly. This gives easy access to the histogram 
                //pointer so that the histogram can be reset.        
                AIDA::IProfile1D* histo2chip = bookProfile1D(fmt.str() + "ChipChannel_prof"  , "ADCs vs chip channel" + title.str()  , -0.5,allchans-0.5,  allchans);

                //Reset the profile histograms after m_refresh events.
                //This is handy for online monitoring.
                //Book memory consuming 2D histograms:
                AIDA::IHistogram2D* histo1chip=0;
                //If the bank is non-pedestal subtracted the y-axis should have a different range.
                double ylow=-128.5;
                double yhigh=127.5;
                        ylow = 512-128.5;
                        yhigh = 512 + 127.5 ; 
                if(m_plot2DADCs==true && m_evenodd==false){
                        histo1chip = book2D(fmt.str() + "ChipChannel",  "ADCs vs chip channel" + title.str(),	
                                        -0.5,allchans-0.5,  allchans,ylow,yhigh,256);
                }	  
                int c=0;
                for(int ch=0;ch<allchans;++ch){
                        int value;

                        if(ch%36<4){
                                value = hdata[h++];
                        } else {
                                value = dataOneTELL1->channelADC(c++);
                        }
                        //2d histograms:
                        //printf("ch %d val %d\n", channel, value);
                        if( 0 != histo1chip && m_plot2DADCs) { histo1chip->fill ( ch, value) ; }
                        //profile histograms:
                        if( 0 != histo2chip ) { histo2chip->fill ( ch, value) ; }  
                }

        }

        return ( StatusCode::SUCCESS );
}

// Declaration of the Algorithm Factory

DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, ADCMon )



