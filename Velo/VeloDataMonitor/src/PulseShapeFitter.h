// $Id: PulseShapeFitter.h,v 1.1 2009-07-15 17:58:45 kakiba Exp $
#ifndef VELORECMONITORS_VELOSAMPLINGMONITOR_H 
#define VELORECMONITORS_VELOSAMPLINGMONITOR_H 1

// Include files
// -------------
#include "VeloDet/DeVelo.h"

// from VeloEvent
#include "VeloEvent/VeloTELL1Data.h"

// from DigiEvent
#include "Event/VeloCluster.h"

// local
#include "VeloMonitorBase.h"

#include <TH1D.h>
#include <TF1.h>

#include "Math/ParamFunctor.h"


/** @class PulseShapeFitter PulseShapeFitter.h
 *  
 *
 *  @author Kazu  
 *  @date   2008-10-22
 */

namespace Velo {


  Double_t fitfsimple(Double_t *x, Double_t *par);

  class PulseShapeFitter : public VeloMonitorBase {
  public: 
    /// Standard constructor
    PulseShapeFitter( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~PulseShapeFitter( );    ///< Destructor
    virtual StatusCode initialize() override;    ///< Algorithm initialization
    virtual StatusCode execute   () override;    ///< Algorithm execution
    virtual StatusCode finalize  () override;    ///< Algorithm finalization
    long          m_nEvents;
    double        m_threshold;
    double m_offset;
    double m_width1;
    double m_width2;
    double m_chisqCut;
    int           m_nSteps;
    int           m_shift;
    int           m_stepSize;
  
    int m_takeNeighbours;
    int m_wrapPoint;
    int  m_wrappedSteps;
    int tell1smatch[5],tell1sorder[88];
    int stripFromChannel[2][2048];
    int channelFromStrip[2][2048];
    //double coarsepulse[88][2048][7];
    bool m_inverted, m_detailedOutput;
    std::vector<int> m_chosenTell1s;
    std::vector<int> m_pulsedChannels;
    //std::vector<std::vector<bool>>; 
    double subtractCommonMode(int channelNumber,  LHCb::VeloTELL1Data *tell1data);
  

    double        m_thresholdForCMS;

  protected:
  private:
    // Retrieve the VeloTell1Data
    LHCb::VeloTELL1Datas* veloTell1Data( std::string samplingLocation );
    // Monitor the VeloTell1Data
    //void MakePulseShape( std::string samplingLocation, LHCb::VeloTELL1Datas* tell1Datas );
    TF1 *func;
    TH1D *pulse;
    

    // Data members
    const DeVelo* m_velo;
    // Job options
    std::vector<std::string> m_samplingLocations;
  };
}

#endif // VELORECMONITORS_VELOSAMPLINGMONITOR_H
