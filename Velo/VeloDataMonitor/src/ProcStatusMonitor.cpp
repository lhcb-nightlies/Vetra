#include <string>

#include "Event/ProcStatus.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "ProcStatusMonitor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Velo::ProcStatusMonitor
//
// 2012-04-24 : David Hutchcroft <David.Hutchcroft@cern.ch>
//-----------------------------------------------------------------------------

namespace Velo {
DECLARE_ALGORITHM_FACTORY( ProcStatusMonitor )
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::ProcStatusMonitor::ProcStatusMonitor(const std::string& name, ISvcLocator* pSvcLocator) :
  Velo::VeloMonitorBase(name, pSvcLocator),
  m_numProcStatus(0)
{
    // where to get the EvtInfos from the TES
  declareProperty("NumberLogWarnings", m_logWarnings = 10);
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::ProcStatusMonitor::initialize() {
    StatusCode sc = Velo::VeloMonitorBase::initialize(); // must be executed first
    if (sc.isFailure()) return sc;  // error printed already by VeloMonitorBase

    if ( m_debugLevel ) debug() << "==> Initialize" << endmsg;

    int nLabel = 10;
    m_numProcStatus = Gaudi::Utils::Aida2ROOT::aida2root(book1D("m_numProcStatus",
		"Number of ProcStatuses of each type found so far",
                 -0.5, nLabel-0.5, nLabel));

    // pre-load with known values
    addLabel("CorruptVeloBuffer");
    addLabel("BadTELL1IDMapping");
    addLabel("HeaderErrorVeloBuffer");
    addLabel("TooManyClusters");
    addLabel("UnsupportedBufferVersion");
    addLabel("BeamSplashFastVelo");
    addLabel("Unknown");

    return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::ProcStatusMonitor::execute() {
  if ( m_debugLevel ) debug() << "==> Execute" << endmsg;
  if( exist<LHCb::ProcStatus>(LHCb::ProcStatusLocation::Default) ){
    LHCb::ProcStatus * pStat = get<LHCb::ProcStatus>(LHCb::ProcStatusLocation::Default);
    const std::vector< std::pair< std::string, int > > & algs = pStat->algs();
    std::vector< std::pair< std::string, int > >::const_iterator iProc = algs.begin();
    for( ; iProc != pStat->algs().end() ; ++iProc ){
      if( iProc->first.find("VELO:") != std::string::npos ){
        // have a VELO ProcStatus to deal with
        bool ok = addProc(iProc->first);
        if(!ok){          
          const std::string mess = iProc->first;
          const int status = iProc->second;
          Warning(format("Unknown VELO ProcStatus %s status %i",
                         mess.c_str(),status),
                  StatusCode::SUCCESS,1).ignore();
        }
      }
    }
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalize
//=============================================================================
StatusCode Velo::ProcStatusMonitor::finalize() {
    if ( m_debugLevel ) debug() << "==> Finalize" << endmsg;
    
    for ( std::vector<std::string>::const_iterator ip = m_procMessages.begin();
          ip != (m_procMessages.end()-1) ; ++ip ){ // skip "Unknown" warnings for that
      info() << "ProcStatus " << (*ip) << " occurred " 
             << m_numProcStatus->GetBinContent(1+ip-m_procMessages.begin())
             << " times " << endmsg;
    }

    return Velo::VeloMonitorBase::finalize();  // must be called after all other actions
}

void Velo::ProcStatusMonitor::addLabel(const std::string & newLabel){
  m_procMessages.push_back(newLabel);
  m_numProcStatus->GetXaxis()->SetBinLabel((int)m_procMessages.size(), newLabel.c_str());
}

bool Velo::ProcStatusMonitor::addProc(const std::string & message){
  // message should be "OK:VELO:TextCode:Alg" need the TextCode bit
  int firstComp = message.find_first_of(":")+1;
  std::string text = message.substr(message.find_first_of(":",firstComp)+1,
                                    message.find_last_of(":")-message.find_first_of(":")-1);
  bool known = false;
  std::vector<std::string>::const_iterator iM = m_procMessages.begin();
  unsigned int pos = m_procMessages.size()-1; // pos of message in list
  while(iM != (m_procMessages.end()-1)){ // last is Unknown so that is the default
    if( text.find(*iM) != std::string::npos ){
      pos = iM-m_procMessages.begin();
      known = true;
      break;
    }
    ++iM;
  }
  m_numProcStatus->Fill((double)pos);
  return known;
}
    
  
