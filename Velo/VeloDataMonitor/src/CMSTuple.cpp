#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/AlgFactory.h" 
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"
#include "CommonFunctions.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "Tell1Kernel/VeloDecodeConf.h" 

#include "Event/ODIN.h"

// ============================================================================
#include "boost/format.hpp"
// ============================================================================
// Class : Velo::Monitoring::CMSTuple
// author: Veerle Heijne
//-----------------------------------------------------------------------------
using namespace LHCb;
using namespace VeloTELL1;
using namespace std;
using namespace Gaudi;
int NLinks = 64;
namespace Velo
{
  namespace Monitoring
  {
	class CMSTuple : public GaudiTupleAlg{
	  public:
		/// Standard constructor
		CMSTuple( const string& name, ISvcLocator* pSvcLocator );
		virtual ~CMSTuple( ); ///< Destructor
		virtual StatusCode initialize() override;    ///< Algorithm initialization
		virtual StatusCode execute   () override;    ///< Algorithm execution
		virtual StatusCode finalize  () override;    ///< Algorithm finalization
		StatusCode makeMCMSTuple();
		StatusCode makeLCMSTuple();
	  private  :
		long int  m_evtNumber;
		int m_refresh;
		int m_plotsimple;
		int m_plotall;
		int m_plotsignals;
		int m_plotlcms;
		DeVelo* m_veloDet;
		vector<int> m_chosenTell1s;
		bool m_plot2DADCs;
		int m_maxEvts;
		vector<string>  m_locations;
		bool m_evenodd;
	};
  }
}


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::CMSTuple::CMSTuple( const string& name,
	ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator )
  ,  m_evtNumber(0)
  ,  m_refresh  (5000)
  ,  m_veloDet  (0)
  ,  m_locations( )

{
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::ADCs             ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::PedSubADCs       ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::ReorderedADCs    ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ) ;
  declareProperty( "Containers" ,   m_locations  ,  "The list of TES locations to be monitored") ;
  declareProperty( "TELL1s",               m_chosenTell1s);
  declareProperty( "RefreshRate",          m_refresh);
  declareProperty( "PlotAll",              m_plotall=1);
  declareProperty( "PlotSignals",          m_plotsignals=1);
  declareProperty( "PlotLCMS",             m_plotlcms=1);
  declareProperty( "PlotSimple",           m_plotsimple =1);
  declareProperty( "Plot2DHistos",         m_plot2DADCs=false);
  declareProperty( "MaxEvts",              m_maxEvts = -1);
  setProperty ( "PropertiesPrint", true ).ignore();
}
//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::CMSTuple::~CMSTuple() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::CMSTuple::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( propsPrint() ) printProps();
  debug() << "==> Initialize" << endmsg;
  setHistoTopDir( "Vetra/" );
  
  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );
  if(m_veloDet == 0) Error("Can't find detector element. Exiting with Failure",StatusCode::FAILURE);

  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::CMSTuple::finalize() {
  debug() << "==> Finalize" << endmsg;  
  return GaudiHistoAlg::finalize();  // must be called after all other actions
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::CMSTuple::execute() {
  debug() << "==> Execute" << endmsg;  
  if((m_evtNumber < m_maxEvts) || (m_maxEvts == -1)){
	makeMCMSTuple();
  }
  m_evtNumber++;    
  return StatusCode::SUCCESS;
}
//==============================================================================================================
//==============================================================================================================
// Store the CMS values per sensor in a tuple
//==============================================================================================================
StatusCode Velo::Monitoring::CMSTuple::makeMCMSTuple(){
  string  pedSubAdcLocation = LHCb::VeloTELL1DataLocation::PedSubADCs;
  string  reorderAdcLocation = LHCb::VeloTELL1DataLocation::ReorderedADCs; 

  if(!exist<VeloTELL1Datas>( pedSubAdcLocation)){
      error()<< " could not get the ped sub data, bailing out of MCMS monitor" << endmsg;
      return StatusCode::SUCCESS;
  }
  if(!exist<VeloTELL1Datas>( reorderAdcLocation )){
      error()<< " could not get the reordered data, bailing out of MCMS monitor" << endmsg;
     return StatusCode::SUCCESS;
  }
  VeloTELL1Datas *pedsubADCs =  get<VeloTELL1Datas>( pedSubAdcLocation );
  VeloTELL1Datas *reorderADCs =  get<VeloTELL1Datas>( reorderAdcLocation );

  VeloTELL1Datas::const_iterator pedsubsensIt,  reordersensIt;
  if(!reorderADCs ) return StatusCode::FAILURE;
  if(!pedsubADCs) return StatusCode::FAILURE;

  Tuple tuple = nTuple("CMSTuple", "CMSTuple");

  //loop over sensors:
  for(pedsubsensIt=pedsubADCs->begin(), reordersensIt = reorderADCs->begin() ;
      pedsubsensIt!=pedsubADCs->end();++pedsubsensIt, ++reordersensIt){
    VeloTELL1Data* pedsubdataOneTELL1=NULL;
    if (*pedsubsensIt)	 pedsubdataOneTELL1=(*pedsubsensIt);
    VeloTELL1Data* reorderdataOneTELL1=NULL;
    if (*reordersensIt)  reorderdataOneTELL1=(*reordersensIt);
    if (!pedsubdataOneTELL1) continue;
    if (!reorderdataOneTELL1) continue;
    int TELL1_number=pedsubdataOneTELL1->key();
    if(ExcludeTELL1(m_chosenTell1s,TELL1_number)) continue;
    if(pedsubdataOneTELL1->key() != reorderdataOneTELL1->key() ) 
      error() << "tell1s not ordered? pedsub " << pedsubdataOneTELL1->key()<< "  cms"  << reorderdataOneTELL1->key() << endmsg;
    
    const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(TELL1_number);
    const DeVeloPhiType* phisensor; 
    const DeVeloRType* rsensor; 
    
    if (sensor->isPhi()) phisensor =  (m_veloDet->phiSensor(TELL1_number));
    else phisensor = NULL;
    if (sensor->isR()) rsensor =  m_veloDet->rSensor(TELL1_number);
    else rsensor = NULL;
    
    if(sensor == 0 ) 	  return StatusCode::FAILURE;
    vector<float> cmsvalues;
    vector<double> rs;
    vector<double> phis;
    vector<double> zs;
    vector<double> strips;
    vector<double> links;

    for(int iLink=0;iLink< 64;++iLink){
      unsigned int channel= iLink*32 +5; // 32 channels per link, 64 links, looking at every channel 5.
      unsigned int strip = sensor->ChipChannelToStrip(channel);
      int pedsubvalue =-1000; // after pedestal subtration
      int reordervalue =-1000; // mcms after reordering
      pedsubvalue=pedsubdataOneTELL1->channelADC(channel);
      reordervalue=reorderdataOneTELL1->stripADC(strip);
      float value = reordervalue - pedsubvalue ;

      cmsvalues.push_back(value);
      strips.push_back(strip);
      links.push_back(iLink);
      if (rsensor != NULL) rs.push_back( rsensor->globalROfStrip(strip));
      else rs.push_back(-100.);
      if (phisensor !=NULL) phis.push_back(phisensor->globalPhiOfStrip(strip));
      else phis.push_back(-100.);
    }

    LHCb::ODIN* odin=0;
    if(exist<LHCb::ODIN>((LHCb::ODINLocation::Default))) {
      odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
    }
    tuple->column( "Event", long( odin->eventNumber ()) );
    tuple->column( "Run", long( odin->runNumber()) );

    tuple->column( "Z", sensor->z() );
    tuple->column( "SensorID", TELL1_number );
    tuple->column( "IsRSensor",sensor->isR() );
    tuple->column( "IsPhiSensor", sensor->isPhi() );

    tuple->farray( "CMSvalue",cmsvalues, "nLinks", 64  );
    tuple->farray( "StripID",strips, "nLinks", 64  );
    tuple->farray( "LinkID",links, "nLinks", 64  );

    tuple->farray( "R",rs, "nLinks", 64  );
    tuple->farray( "Phi",phis, "nLinks", 64  );
    tuple->write();
  }    
    
  return StatusCode::SUCCESS;
}
//====================================================================================
StatusCode Velo::Monitoring::CMSTuple::makeLCMSTuple(){
  return StatusCode::SUCCESS;
}
// Declaration of the Algorithm Factory
DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, CMSTuple )
