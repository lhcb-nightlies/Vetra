#include "GaudiKernel/AlgFactory.h" 
#include "GaudiAlg/GaudiAlgorithm.h"

#include "DetDesc/Condition.h"

#include "Event/RawEvent.h"
#include "Event/RawBank.h"
#include "Event/ODIN.h"

#include "TH1D.h"
#include "TF1.h"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <fstream>
#include <map>
#include <string>

#include <string.h>
#include <stdio.h>

/**
 *  @class : Velo::Monitoring::GainParamCalculator
 *
 *  @author Kurt Rinnert <kurt.rinnert@cern.ch>
 *  @date 02-05-2014
*/

using namespace std;


namespace Velo
{
  namespace Monitoring
  {
    class GainParamCalculator : public GaudiAlgorithm {
      public:

        GainParamCalculator( const string& name, ISvcLocator* pSvcLocator );

        virtual ~GainParamCalculator( );              
        virtual StatusCode initialize() override;    
        virtual StatusCode execute   () override;    
        virtual StatusCode finalize  () override;    

        StatusCode decodeHeaders();
        void  accumulateHeaders();

        void initializeTELL1Map();
        void prepareFitUtils();
        double computeOptimalStep( int sensor, int link );
        void writeXMLFile( vector<double>& step_values, int sensor );

      private:

        int m_minHeader; ///< only header values higher than this will be used
        int m_maxHeader; ///< only header values lower than this will be used

        int m_events_per_step;
        int m_n_steps;

        int m_headers[84][64][2];

        int m_header_mean     [84][64][25];
        int m_n_header_mean   [84][64][25];
        int m_header_lo_mean  [84][64][25];
        int m_n_header_lo_mean[84][64][25];
        int m_header_hi_mean  [84][64][25];
        int m_n_header_hi_mean[84][64][25];

        int m_sensor_has_data[84];
        int m_sensor_had_data_once[84];

        bool m_useTELL1Numbers;
        string m_tell1MapFile;
        map<int,int> m_sensorToTELL1Map;
        string m_rawEventLocation;  

        TH1D*   m_fit_histo;
        double* m_fit_data;
        TF1*    m_fit_function;

        string m_xml_dir;

    };
  }
}


DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring,GainParamCalculator)

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::GainParamCalculator::GainParamCalculator( const string& name,
    ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
  , m_minHeader(400)
  , m_maxHeader(750)
  , m_events_per_step(2000)
  , m_n_steps(23)
{
  declareProperty("RawEventLocation",m_rawEventLocation=LHCb::RawEventLocation::Default);
  declareProperty("TELL1MapFile",m_tell1MapFile="/group/velo/config/livdb_tell1_sensor_map.txt");
  declareProperty("XMLDir",m_xml_dir="./gain_param_xml");
  declareProperty("UseTELL1Numbers",m_useTELL1Numbers=false);
}

//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::GainParamCalculator::~GainParamCalculator() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::GainParamCalculator::initialize() {

  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithmorithm

  debug() << "==> Initialize" << endmsg;
  
  // zero all counters and flags
  memset(m_header_mean     ,0,sizeof(int)*84*64*25);
  memset(m_n_header_mean   ,0,sizeof(int)*84*64*25);
  memset(m_header_lo_mean  ,0,sizeof(int)*84*64*25);
  memset(m_n_header_lo_mean,0,sizeof(int)*84*64*25);
  memset(m_header_hi_mean  ,0,sizeof(int)*84*64*25);
  memset(m_n_header_hi_mean,0,sizeof(int)*84*64*25);
  memset(m_sensor_had_data_once ,0,sizeof(int)*84);

  if ( m_useTELL1Numbers ) {
    initializeTELL1Map();
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Initialize sensor to TELL1 map
//=============================================================================
void Velo::Monitoring::GainParamCalculator::initializeTELL1Map() {

  FILE *mf = fopen(m_tell1MapFile.c_str(),"r");

  if ( 0 == mf ) {
    fatal() << "Could not open TELL1 map file '" << m_tell1MapFile << "' for reading" << endmsg;
    return;
  }

  fscanf(mf,"Tell1 SW #\n");

  int tell1, sensor;

  while ( EOF != fscanf(mf,"TL-%d\t%d\n",&tell1,&sensor) ) {
    m_sensorToTELL1Map[sensor] = tell1;
  }
  

  fclose(mf);

  return;
}

//=============================================================================
// Create ROOT histogram and function object for fitting FHS step plots.
//=============================================================================
void Velo::Monitoring::GainParamCalculator::prepareFitUtils() {

  // one histogram for fitting. this will be re-used for FHS step fits on all links
  m_fit_histo = new TH1D("fit_histo","fit histo",24,-0.5,23.5);
  m_fit_data = m_fit_histo->GetArray();

  // the fit model with range and parameter limits.
  //m_fit_function = new TF1("fit_function", "[2]+[0]*TMath::Exp([1]*x)",1.,21.5);
  m_fit_function = new TF1("fit_function", "[2]+[0]*exp([1]*x)",1.,21.5);
  m_fit_function->SetParLimits(0.0, 60.0, 100.0);
  m_fit_function->SetParLimits(1.0,-0.1,0.0);
  m_fit_function->SetParLimits(2.0,0.0,100.0);

}

//=============================================================================
// Fit FHS vs. step function for one link and return optimal step.
//=============================================================================
double Velo::Monitoring::GainParamCalculator::computeOptimalStep( int sensor, int link ) {

  double fit_params[3];

  // set start parameters for fit
  fit_params[0] = 80.0;
  fit_params[1] = -0.03;
  fit_params[2] = 30.0;
  m_fit_function->SetParameters(fit_params);

  // perform fit
  m_fit_histo->Fit(m_fit_function,"RNQ");
  m_fit_function->GetParameters(fit_params);
  
  // calculate optimal step
  double fhs_norm = 100.0;
  double optimal_step = (1.0/fit_params[1])*log((fhs_norm - fit_params[2])/fit_params[0]);

  if ( optimal_step < 0.0 || optimal_step > 23.0 ) { // out of reasonable range
    warning() << "FHS step fit result out of range! (sensor=" << sensor << ", link=" << link << ", step=" << optimal_step << ")" << endmsg;
    optimal_step = 11.0; // force practical value
  }

  return optimal_step;
}

//=============================================================================
// Write XML Files, one per sensor (or TELL1)
//=============================================================================
void Velo::Monitoring::GainParamCalculator::writeXMLFile( vector<double>& step_values, int sensor ) {

  char format_buffer[64];
  vector<string> param_values(64);

  for ( int fpga_register=0; fpga_register<64; ++fpga_register ) {

    // map link to FPGA register order (revert cable order and change 'endianess')
    int link = 16*(3-(fpga_register/16)) + 8*(1 -(fpga_register%16)/8) + (fpga_register%16)%8;

    // calculate correct integer representation (as it will be written to the FPGA register)
    unsigned int gain_factor = 0xEFC0U + static_cast<unsigned int>(round( ( step_values[link] - 11.0 )*0XF5));

    // convert this to a hex string representation and store
    sprintf(format_buffer,"0x%04x",gain_factor);
    param_values[fpga_register] = string(format_buffer);

  }

  // create a Condition object and write its XML representation to file
  Condition gain_condition;
  gain_condition.addParam("gain",param_values);

  int tell1_number = sensor;
  if ( m_useTELL1Numbers ) { tell1_number = m_sensorToTELL1Map[sensor]; }

  sprintf(format_buffer,"/gain_Tell1%d.xml",tell1_number);
  string xml_file_name = m_xml_dir + format_buffer;

  ofstream xml_file(xml_file_name.c_str());
  if ( ! xml_file.is_open() ) {
    error() << "Could not open file " << xml_file_name << " for writing." << endmsg;
    return;
  }

  sprintf(format_buffer,"VeloTELL1Board%d",tell1_number);
  string cond_name(format_buffer);
  string xml_string = gain_condition.toXml(cond_name,true);


  // convert the non-sensical "std::string" type tag to "string". 
  size_t del_here = xml_string.find("type=\"std::string\"");
  if (del_here < xml_string.size()) {
    xml_string.erase(del_here+6,5);
  }

  xml_file << xml_string;
  xml_file.close();

}

//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::GainParamCalculator::execute() {

  debug() << "==> Execute" << endmsg;  
 
  if ( decodeHeaders() ) { accumulateHeaders(); }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::GainParamCalculator::finalize() {
  debug() << "==> Finalize" << endmsg;  

  vector<double> step_values(64);

  prepareFitUtils();

  // compute all FHS values do the fits and write the gain parameter XML
  for ( int sensor_idx=0; sensor_idx<84; ++sensor_idx ) {

    if ( ! m_sensor_had_data_once[sensor_idx] ) continue;

    int sensor = 64*(sensor_idx/42) + sensor_idx%42;

    for ( int link=0; link<64; ++link ) {
      
      // fully reset histogram for new fit
      m_fit_histo->Reset("M");

      for ( int step=1; step<m_n_steps; ++step ) { // step 0 is not used for the fits
        if ( m_n_header_lo_mean[sensor_idx][link][step] && m_n_header_hi_mean[sensor_idx][link][step] ) {
          double lo_mean = m_header_lo_mean[sensor_idx][link][step]/static_cast<double>(m_n_header_lo_mean[sensor_idx][link][step]);
          double hi_mean = m_header_hi_mean[sensor_idx][link][step]/static_cast<double>(m_n_header_hi_mean[sensor_idx][link][step]);
          m_fit_data[step+1] = hi_mean - lo_mean; // step+1 because we write into a ROOT histogram array
        }
      } // loop over steps

      // perform fit and compute optimal gain parameter
      step_values[link] = computeOptimalStep( sensor, link );

    } // loop over links

    // write condition XML for this sensor
    writeXMLFile( step_values, sensor );

  } // loop over sensors 

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

//==============================================================================================================
// Fast header decoding
//==============================================================================================================
StatusCode Velo::Monitoring::GainParamCalculator::decodeHeaders() {

  /*
   * The NZS raw bank is organized in 4 blocks (one per PPFPGA) and three sections
   * in each block. The sections are parallel streams of 3 32 bit words:
   *
   *  - the first word encodes three links, each link as ten bits starting from the LSB
   *  - the second encodes three links, each link as ten bits starting from the LSB
   *  - the third encodes two links, each link as ten bits starting from the LSB
   *
   * The first 36 words in a section encode even links; the next 36 words encode odd links.
   *
   * Each block ends 8 32 bit words containing the event info.
   *
   * This gives a total block size of 2*3*36 + 8 u_int32.
   *
   * For more details see EDMS 692431 V 2.0.
   *
   */

  // reset per event data presence flags
  memset(m_sensor_has_data ,0,sizeof(int)*84);

  // check for raw event, get it and fetch the raw VELO banks.

  if ( !exist<LHCb::RawEvent>(m_rawEventLocation) ) {
    error() << "No RawEvent at " <<  m_rawEventLocation << "." << endmsg;
    return StatusCode::FAILURE;
  }

  LHCb::RawEvent* raw_event = get<LHCb::RawEvent>(m_rawEventLocation);

  const vector<LHCb::RawBank*>&  raw_banks = raw_event->banks(LHCb::RawBank::VeloFull);

  if ( raw_banks.empty() ) return StatusCode::FAILURE;

  // we have some raw data, decode it.
  for ( vector<LHCb::RawBank*>::const_iterator bi = raw_banks.begin(); bi != raw_banks.end(); ++bi ) {

    const LHCb::RawBank* raw_bank = *bi;

    if ( LHCb::RawBank::MagicPattern != raw_bank->magic() ) { continue; } // this bank is corrupted

    int sensor = raw_bank->sourceID();
    int sensor_idx = 42*(sensor/64) + sensor%64;
    m_sensor_has_data[sensor_idx] = 1;
    m_sensor_had_data_once[sensor_idx] = 1;

    const unsigned int *bank_begin = raw_bank->data();
    int block_size = 2*3*36 + 8; // see EDMS 692431 V 2.0

    for ( int hw_block = 0; hw_block < 4; ++hw_block ) { 
      
      for ( int odd=0; odd<2; ++odd ) { // even numbered links come first

        const unsigned int *bank = bank_begin + hw_block*block_size + odd*3*36;

        for ( int bit=0; bit<2; ++bit ) { 

          int bank_idx = (bit+2)*3; // skip the first two header bits, we only use the 3rd and 4th

          // chip 0 + block*4, link [01], link [23]; chip 1 + block*4, link [01]:

          int link = (3 - hw_block)*16 + odd; // reverse blocks to correct for cable order
          unsigned int word = bank[bank_idx++];
          m_headers[sensor_idx][link][bit] = word & 0x000003FFU;

          link += 2;
          word = word >> 10;
          m_headers[sensor_idx][link][bit] =  word & 0x000003FFU;

          link += 2;
          word = word >> 10;
          m_headers[sensor_idx][link][bit] =  word & 0x000003FFU;


          // chip 1 + block*4, link [23]; chip 2 + block*4, link [01], link[23]:

          link += 2;
          word = bank[bank_idx++];
          m_headers[sensor_idx][link][bit] = word & 0x000003FFU;

          link += 2;
          word = word >> 10;
          m_headers[sensor_idx][link][bit] =  word & 0x000003FFU;

          link += 2;
          word = word >> 10;
          m_headers[sensor_idx][link][bit] =  word & 0x000003FFU;


          // chip 3 + block*4, link [01], link [23]:

          link += 2;
          word = bank[bank_idx++];
          m_headers[sensor_idx][link][bit] = word & 0x000003FFU;

          link += 2;
          word = word >> 10;
          m_headers[sensor_idx][link][bit] =  word & 0x000003FFU;

        } // bit loop
      } // odd/even link loop
    } // PPFPGA block loop
  } // bank loop

  return StatusCode::SUCCESS;
}

//==============================================================================================================
// Fill header sums
//==============================================================================================================
void Velo::Monitoring::GainParamCalculator::accumulateHeaders() {

  LHCb::ODIN* odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
  int step = odin->calibrationStep();

  for( int sensor_idx=0; sensor_idx<84; ++sensor_idx ){

    if ( 0 == m_sensor_has_data[sensor_idx] ) { continue; }; // no data for this sensor in this event

    
    for(int link=0; link<64; ++link){

      // we only use the last two bits; the others are not even decoded!
      int bit3 =  m_headers[sensor_idx][link][0];
      int bit4 =  m_headers[sensor_idx][link][1];

      // only accept headers in a reasonable range
      if ( bit3 < m_minHeader || bit3 > m_maxHeader || bit4 < m_minHeader || bit4 > m_maxHeader ) continue;

      // accumulate the sums
      if ( m_n_header_mean[sensor_idx][link][step] < m_events_per_step ) { // two entries per event; average after m_events_per_step/2
        m_header_mean[sensor_idx][link][step] += bit3; 
        m_header_mean[sensor_idx][link][step] += bit4; 
        m_n_header_mean[sensor_idx][link][step] += 2;
      } else if ( m_n_header_mean[sensor_idx][link][step] > m_events_per_step ) { // the average is valid; accumulate hi/lo
        if ( bit3 <  m_header_mean[sensor_idx][link][step] ) { // bit 3 is lo
          m_header_lo_mean[sensor_idx][link][step] += bit3; 
          ++m_n_header_lo_mean[sensor_idx][link][step]; 
        } else { // bit 3 is hi
          m_header_hi_mean[sensor_idx][link][step] += bit3; 
          ++m_n_header_hi_mean[sensor_idx][link][step]; 
        }
        if ( bit4 <  m_header_mean[sensor_idx][link][step] ) { // bit 4 is lo
          m_header_lo_mean[sensor_idx][link][step] += bit4; 
          ++m_n_header_lo_mean[sensor_idx][link][step]; 
        } else { // bit 4 is hi
          m_header_hi_mean[sensor_idx][link][step] += bit4; 
          ++m_n_header_hi_mean[sensor_idx][link][step]; 
        }
      } else { // exactly halve of the events in this step have been processd, compute average
        m_header_mean[sensor_idx][link][step] /= m_events_per_step;
        m_n_header_mean[sensor_idx][link][step] += 2; // make sure we go into the hi/lo branch next time
      }

    } // link loop
  } // sensor loop

  return;
}


DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, GainParamCalculator )



