#ifndef CABLETEST_H 
#define CABLETEST_H 1

// Include files from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// from Event model
#include "VeloEvent/EvtInfo.h"
#include "VeloEvent/VeloTELL1Data.h"

// from TELL1 Kernel:
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"

//velo
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

//namespaces
using namespace LHCb;
using namespace VeloTELL1;

class DeVelo;
class DeVeloSensor;
class DeVeloRType;
class DeVeloPhiType;

class CableTest : public GaudiTupleAlg {
public:

   /// Standard constructor
  CableTest( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~CableTest( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

  StatusCode plotCorrelation();

  unsigned long int EventNumber(){return m_evtNumber;}

 protected:

  DeVelo* m_veloDet;
  const DeVeloSensor* m_veloSensor;
  DeVeloRType* m_RSensor;
  DeVeloPhiType* m_PhiSensor;
  int m_sensorNumber;
  bool m_sensorIsR;

  unsigned long int m_evtNumber;

  int m_isInit;
  
 // Convergence limit from pedestal subtractor
  unsigned int m_convergenceLimit;   

  std::string m_ADCsLoc;
  std::string m_FIRADCsLoc;
  std::string m_subADCsLoc;
  std::string m_LCMSADCsLoc;
  std::string m_reorderedLoc;

  VeloTELL1Datas* m_ADCs;
  VeloTELL1Datas* m_FIRADCs;
  VeloTELL1Datas* m_subADCs;
  VeloTELL1Datas* m_reordered;
  VeloTELL1Datas* m_LCMSADCs;

  std::vector<int> m_chosenTell1s;

  unsigned int m_startevent;
  unsigned int m_lastevent;
  bool m_printinfo;
  unsigned int m_threshold;
  
};

//Auxilliary class for bit pattern decoding:
class LinkInfo{

 public:
  
  LinkInfo(ALinkPair begEnd, int cut){
    
    bitpattern.reserve(4);
    scdatIt iT; //iterator to vector of unsigned int.

    //Loop over ADC values in link
    for(iT=begEnd.first; iT!=begEnd.second; ++iT){    
      link.push_back(*iT); //Push back ADC value to vector
    }


    //Loop over the 4 segments of the link:
    for(int j=0;j<4;j++){
      bitpattern[j]=0;
      //Loop over the channels in each segment:
      for(int i=0;i<8;i++){  

	if(j==0 && i==0) continue; //Exclude first channel in link

	//Check if ped sub adc value > cut:

	if( abs( link[i+8*j] ) > cut ) {

	  bitpattern[j]+=(int)pow(2.0,i); // Yes? add 2^i
	  

	  
	}
	
      }
    }
  };
  
  int link1() { return (bitpattern[0]/2)-1;};
  int link2(){ return (bitpattern[2]/2)-1;};
  int hybrid1(){ return bitpattern[1];};
  int hybrid2(){ return bitpattern[3];};
  
 private:
  std::vector<int> bitpattern;
  std::vector<int> link;  

};

#endif // CABLETEST_H
