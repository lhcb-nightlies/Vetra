// ============================================================================
// Include files 
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/AlgFactory.h" 
// ============================================================================
// GaudiAlg 
// ============================================================================
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Fill.h"
// ============================================================================
// GaudiUti
// ============================================================================
#include "GaudiUtils/HistoTableFormat.h"
// ============================================================================
// VeloTELL1Event
// ============================================================================
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"
// ============================================================================
// AIDA 
// ============================================================================
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
// ============================================================================
// Boost 
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
// VeloDet
// ============================================================================
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"
// ============================================================================
// local 
// ============================================================================
#include "VetraKernel/INoiseEvaluator.h"
#include "VetraKernel/TELL1Noise.h"
#include "CommonFunctions.h"
// ============================================================================

using namespace VeloTELL1;


namespace Velo 
{
  namespace Monitoring 
  {
    /** @classGainMonTestPulse 
     *  This module should be run along with GainMon to study gain scan test pulse data
     *  This code is essentially a stripped-down version of NoiseMon.cpp 
     *  @Author Grant McGregor
     *  @date   2009-05-26
     */
    class GainMonTestPulse : public GaudiHistoAlg 
    {
      /// friend factory for instantiation
      friend class AlgFactory<Velo::Monitoring::GainMonTestPulse>;
      public:
      // initialize it! 
      virtual StatusCode initialize () override;
      // execute it! 
      virtual StatusCode execute    () override;
      // finalize it! 
      virtual StatusCode finalize   () override;
      void SetTPConstants();
      double NormalizeTP(int sensor, int link);
      

    protected:
      /// standard constructor 
      GainMonTestPulse 
      ( const std::string& name , 
        ISvcLocator*       pSvc ) 
        : GaudiHistoAlg ( name , pSvc ) 
        //
        , m_locations () 
        , m_tell1s    () 
        , m_publish   ( 500 )
        , m_toolType  ( "Velo::Monitoring::NoiseEvaluator" )
        //
        , m_event     ( 0 )
        //
        , m_noises    () 
        //
        , m_isInit    (0)
        //
        , m_veloDet   (0)
        , m_nEvtGainScan(2000)  //Set the number of events in each gain scan step
        //
        //  , m_onlineMonitoring(false)
        
        {
          //
          //m_locations.push_back ( LHCb::VeloTELL1DataLocation::ADCs             ) ;
          //m_locations.push_back ( LHCb::VeloTELL1DataLocation::SubPeds          ) ;
          //m_locations.push_back ( LHCb::VeloTELL1DataLocation::PedSubADCs       ) ;
          //m_locations.push_back ( LHCb::VeloTELL1DataLocation::FIRCorrectedADCs ) ;
          //m_locations.push_back ( LHCb::VeloTELL1DataLocation::ReorderedADCs    ) ;
          m_locations.push_back ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ) ; //Just run on the CMSuppressed ones
          //
          declareProperty 
            ( "Containers" , 
              m_locations  , 
              "The list of TES locations to be monitored") ;
          //
          declareProperty 
            ( "TELL1s"     , 
              m_tell1s     , 
              "The list of TELL1s to be monitored, empty for all") ;
          //

          declareProperty 
            ( "NoiseEvaluatorType" , 
              m_toolType , 
              "The actual type of noise evaluator tool" ) ;
          //
          declareProperty 
            ( "PublishingFrequency" , 
              m_publish             , 
              "The frequency to publish results" ) ;   
          //
          declareProperty( "NormalizeTP", m_NormalizeTP=false);
          declareProperty( "nEvtGainScan", m_nEvtGainScan);
	  declareProperty( "isGainScan", m_isGainScan=false);
          //
          setProperty ( "HistoPrint"  , true     ).ignore() ;
          setProperty ( "HistoTopDir" , "Velo/" ).ignore() ;

        }
      /// virtual and protected destructor 
      virtual ~GainMonTestPulse() {}
      private:
      // no default constructor 
      GainMonTestPulse ()                  ; ///< no default constructor 
      // no copy constructor 
      GainMonTestPulse ( const GainMonTestPulse& ) ; ///< no copy constructor 
      // no assigmenet operator 
      GainMonTestPulse& operator=( const GainMonTestPulse& ) ; ///< no assignement 
      protected:

      // retrieve the tools
      StatusCode retrieveTools   ();
      /// make the plots
      StatusCode makeThePlots () ;

      private:
      // the actual type of container of data locations 
      typedef std::vector<std::string>                  Locations ;
      typedef Velo::Monitoring::INoiseEvaluator::TELL1s TELL1s    ;
      typedef std::map<std::string,Velo::Monitoring::INoiseEvaluator*> Tools ;
      // the input containers of data to be monitored
      Locations     m_locations ; ///< the input containers
      // the list of IDs for TELL1s to be monitored 
      TELL1s        m_tell1s    ; ///< list of ids for TELL1 
      // publishing frequency
      unsigned long m_publish   ; ///< publishing frequency
      // use the public tools? 
      bool          m_public    ; ///< use the public tools 
      // the actual tool type 
      std::string   m_toolType  ; ///< the actual tool type
      // event counter 
      unsigned long m_event     ; ///< internal event counter 
      // the map of tools to be used 
      Tools         m_noises    ; ///< map of tools to be used 
      // Flag for initializing the process info bank.
      int m_isInit;
      // Convergence limit from pedestal subtractor
      unsigned int m_convergenceLimit;      
      // Detector element pointer
      DeVelo* m_veloDet; 

      std::vector< std::map<unsigned int, AIDA::IHistogram1D*> > m_rmsChipChannelHistos; 
      std::vector< std::map<unsigned int, AIDA::IHistogram1D*> > m_rmsStripHistos; 
      int step;
      float TPConstants[106][65];
      bool m_NormalizeTP;
      int m_nEvtGainScan;
      bool m_isGainScan;
    } ;
  } // end of namespace Velo::Monitoring 
} // end of namespace Velo
// ============================================================================

// ============================================================================
//  make the plots 
// ============================================================================
StatusCode Velo::Monitoring::GainMonTestPulse::makeThePlots () 
{
  /// loop over all all tools (information sources) 
  unsigned int noiseCalculator = 0;
  for ( Tools::iterator it = m_noises.begin() ; 
      m_noises.end() != it ; ++it, ++noiseCalculator )
  {
    // for the given tool get all TELL1s:
    Velo::Monitoring::INoiseEvaluator::INoiseMap m = it->second->noise() ;

    //Get the container of TELL1 data banks. It's needed to check for reordering.
    if ( !exist<LHCb::VeloTELL1Data::Container> ( it->first ) ) {
      debug() << "Could not retrieve noise data at " << it->first << endmsg;
      continue;
    } 
    const LHCb::VeloTELL1Data::Container* data = 
      get<LHCb::VeloTELL1Data::Container> ( it->first ); 

    /// loop over TELL1s for the given tool 
    for ( Velo::Monitoring::INoiseEvaluator::INoiseMap::iterator il = 
        m.begin () ; m.end () != il ; ++il ) 
    { 
      // get the counters: 
      typedef Velo::Monitoring::TELL1Noise::Counters Counters ;
      Counters cnts = il->second->counters() ;
      if( m_event%m_nEvtGainScan==0){
        //Reset the noise at the start of each gain scan step
        const_cast<Velo::Monitoring::TELL1Noise*>(il->second)->reset();
      }  
      const unsigned int tell1Number = il->first;
      
      bool isReordered = data->object(tell1Number)->isReordered();

      debug() << it->first << " / " << tell1Number << " is Reordered = " << data->object(tell1Number)->isReordered() << endmsg;

      // construct the proper IDs for the histograms:
      boost::format fmt ( "%s/TELL1_%03d/AllEvents" ) ;
      fmt % strippedContainerName ( it->first ) % tell1Number ;
      //unless it is a gain scan...
      if(m_isGainScan){
	boost::format fmt ( "%s/TELL1_%03d/Step%02d" ) ;
	fmt % strippedContainerName ( it->first ) % tell1Number % step;
      }
      const std::string fmtstr = fmt.str() ;

      //Get sensor from detector element:  
      const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(tell1Number);
      if(sensor == 0 ){
        error() << "Can't find a sensor associated with TELL1 " << tell1Number << ". Skipping..." << endmsg;
        break;
        //return StatusCode::FAILURE;
      }

      boost::format sensorInfo(", sensor %d, %s");
      sensorInfo % sensor->sensorNumber() % (sensor->isR() ? "R" : ( sensor->isPhi() ? "Phi" : "Pile-up")) ;

      //histograms for noise:
      AIDA::IHistogram1D* histoRms_chipch = 0; 
      const GaudiAlg::HistoID rms_chipch = fmtstr + "/RMSNoise_vs_ChipChannel" ;      
      if ( m_rmsStripHistos[noiseCalculator].end() == m_rmsStripHistos[noiseCalculator].find(tell1Number) ) {
        histoRms_chipch = m_rmsChipChannelHistos[noiseCalculator][tell1Number] =  book
                  ( rms_chipch  , "RMS noise vs chipch"+sensorInfo.str() , -0.5 , cnts.size() - 0.5 , cnts.size() ) ;
       
      } else {
        histoRms_chipch = m_rmsChipChannelHistos[noiseCalculator][tell1Number];
      }
      if ( 0 != histoRms_chipch ) { histoRms_chipch -> reset() ; } // ATTENTION: RESET


      // fill the histos :
      for (Counters::iterator ic = cnts.begin() ; cnts.end() != ic ; ++ic ) //CHANGE
      {
        const double index = ic - cnts.begin() ;
        unsigned int chipchannel = static_cast<unsigned int> (index);	
        if(isReordered){
          chipchannel=sensor->StripToChipChannel(static_cast<unsigned int> (index));
        }
        
        if ( 0 != histoRms_chipch ) 
        { double Norm = NormalizeTP( tell1Number, (int)(chipchannel/32) );
        histoRms_chipch -> fill ( chipchannel , (ic -> flagRMS  ())*Norm  ) ;
        
        }
      }
    }        
  }
  
  return StatusCode::SUCCESS ;
}



// ===========================================================================
// Retrieve the tools
// ============================================================================
StatusCode Velo::Monitoring::GainMonTestPulse::retrieveTools   () {
  // loop over all input containers and get the tools:
  for ( Locations::iterator il = m_locations.begin() ; 
      m_locations.end() != il ; ++il ) 
  {
    std::string toolName = strippedContainerName( *il ) ;
    // get the evaluator of the noise:
    Velo::Monitoring::INoiseEvaluator* eval = tool<Velo::Monitoring::INoiseEvaluator>
      ( m_toolType , toolName) ;
    
    if(eval!=0)
      m_noises[ *il ] = eval;
    
  }
  m_rmsChipChannelHistos.resize(m_noises.size()); 
  m_rmsStripHistos.resize(m_noises.size());

  return StatusCode::SUCCESS;
}


// ============================================================================
// initialize the algotithm 
// ============================================================================
StatusCode Velo::Monitoring::GainMonTestPulse::initialize   () {
  StatusCode sc=GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm  
  debug() << "==> Initialize" << endmsg;
  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );  

  retrieveTools();

  //Set TP constants if appropriate:
  if(m_NormalizeTP){
    std::cout<<"Setting TP constants" <<std::endl;
    SetTPConstants(); 
    std::cout<<"TP Constants set! " <<std::endl;   
  }
  return StatusCode::SUCCESS;
}

// ============================================================================
// finalize the algotithm 
// ============================================================================
StatusCode Velo::Monitoring::GainMonTestPulse::finalize   () 
{
  // clear the container of tools
  m_noises.clear() ;
  m_event = 0     ;
  // finalize the base class 
  return GaudiHistoAlg::finalize() ;
}
// ============================================================================
// Execute the algorithm 
// ============================================================================
StatusCode Velo::Monitoring::GainMonTestPulse::execute    () 
{  

  //Get pedestal convergence limit from the emulator process info. This can't be done
  //from initialize() so we use the m_isInit flag to only run it the first event.
  StatusCode sc;
  
  if (  0  ==  m_event  % m_publish )
  { makeThePlots() ; } /// MAKE THE PLOTS
  
  ++m_event;
  step = m_event/m_nEvtGainScan;
  return StatusCode::SUCCESS ;
} 

//=============================================================================
// This function sets normalization constants for equalizing TP
//=============================================================================
void Velo::Monitoring::GainMonTestPulse::SetTPConstants()
{
  FILE* infile;
  //infile = fopen("/afs/cern.ch/user/g/gmcgrego/cmtuser/Vetra_v7r0/Tell1/Vetra/job/TPConstants.txt","r");
  infile = fopen("$VETRAROOT/job/TPConstants.txt","r");
  for(int sensor=0; sensor<106; sensor++){
    for(int link=0; link<64+1;  link++){
      if(link!=0){  
        fscanf(infile, "%f ", &TPConstants[sensor][link-1]) ;
      }
      
      if(link==0){ 
        fscanf(infile, "%f ", &TPConstants[sensor][64]) ;
      } 
    }
  }
}


//=============================================================================
// This function returns a normalization constant for equalizing TP
//=============================================================================
double Velo::Monitoring::GainMonTestPulse::NormalizeTP(int sensor, int link) 
{
  double NormValue(1.);
  if(m_NormalizeTP){
    if(TPConstants[sensor][link]>0){
      NormValue = TPConstants[sensor][link];
    }
    
    if(TPConstants[sensor][link]==-1){
      NormValue=1;
    }
  }
  return NormValue;
}




// ============================================================================
/// Declaration of the Algorithm Factory
DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring,GainMonTestPulse)
  // ============================================================================
  // The END
  // ============================================================================
