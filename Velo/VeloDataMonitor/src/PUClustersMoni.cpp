// $Id: PUClustersMoni.cpp,v 1.3 2010-03-12 20:34:49 serena Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "Kernel/IMCVeloFEType.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiUtils/Aida2ROOT.h"  

// AIDA
#include "AIDA/IHistogram2D.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IProfile1D.h"

// local
#include "PUClustersMoni.h"
#include "Event/RawEvent.h"
#include "Event/RawBank.h"
#include "Event/ODIN.h"
#include "L0Interfaces/IL0DUFromRawTool.h"
#include "L0Interfaces/IL0DUConfigProvider.h"
#include "L0Interfaces/IL0DUEmulatorTool.h"
#include "L0Interfaces/IL0CondDBProvider.h"

// other
#include "TFile.h"
#include "TTree.h"
#include <string>

// kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "VetraKernel/ContPrinter.h"
#include "Tell1Kernel/VeloTell1Core.h"

// boost
#include "boost/lexical_cast.hpp"
#include "boost/format.hpp"

// VeloDet
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PUClustersMoni
//
// 2009-10-23 : Serena Oggero
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( PUClustersMoni )

using namespace LHCb;
using namespace VeloTELL1;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PUClustersMoni::PUClustersMoni( 
                            const std::string& name,ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator ), 
    m_PUanalogclusterContNZS("Raw/Velo/DecodedADC"),
    m_PUanalogheaderContNZS("Raw/Velo/DecodedHeaders"),
    m_triggerType(0),
    m_numOfEvents (0), // all events processed 
    m_numOfGoodEvents(0) // according to rawStatus
    //m_stepNum(0),
    //m_rawEvent(0)
{
  declareProperty("OutputFileName", m_OutputFileName = "PUClusterMoniInfo.root" );
  declareProperty("RawEventLocation", m_rawEventLoc="DAQ/RawEvent");
  declareProperty("PUclusterContZS", m_PUclusterContZS="Raw/Velo/PUClusters");
  declareProperty("PUclusterContNZS", m_PUclusterContNZS="Raw/Velo/PUClustersNZS");
  declareProperty( "MoniBinaryNZS", m_DoBinaryNZS=false );
  declareProperty( "OnLineMonitoring", m_DoOnLine = true );
  declareProperty( "MoniAnalogNZS", m_DoAnalogNZS=false );
  declareProperty( "L0DUFromRawToolType"  , m_fromRawTool = "L0DUFromRawTool" ); //// property for L0DU decoding 

}

//=============================================================================
// Destructor
//=============================================================================
PUClustersMoni::~PUClustersMoni() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode PUClustersMoni::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize PUMoni" << endmsg;
  m_odinDecoder = tool<IEventTimeDecoder>( "OdinTimeDecoder" );
  if ( !m_DoOnLine ) {
    m_OutputFile = new TFile(m_OutputFileName.c_str(), "RECREATE");
    m_OutputFile->cd();
    m_OutputTree = new TTree("OutputTree", "OutputTree");
    bookTree();
    if ( m_DoAnalogNZS ){
      m_OutputTreeAnalog = new TTree("OutputTreeAnalog", "OutputTreeAnalog");
      bookTreeAdcNZS();
    }
  }

  // get the decoding tool for L0DU
  m_fromRaw = tool<IL0DUFromRawTool>( m_fromRawTool , m_fromRawTool  ); 
  m_fromRaw->fillDataMap();

  // initialize histograms per sensor
  char name[80];
  char title[80];
  m_VeloNZSPlotH.resize(4);
  m_VeloNZSPlot.resize(4);
  m_numberHitsPlot.resize(4);

  for ( unsigned int sensId = 0; sensId < m_VeloNZSPlot.size(); sensId++ ){
    if ( m_DoAnalogNZS ){
      sprintf(name, "analogNZS_headers_%d", sensId+128 );
      m_VeloNZSPlotH[sensId] = book2D(name, name,0.,2304.,2304, 420.,620., 200);

      sprintf(name, "analogNZS_adcs_%d", sensId+128 );    
      m_VeloNZSPlot[sensId] = book2D(name, name, 0.,2304., 2304, 420.,620., 200);
    }

    sprintf(name, "Number of hits per channel_%d", sensId+128 );
    sprintf(title, "Number of hits per channel (PU ZS), sensor_%d", sensId+128 );
    m_numberHitsPlot[sensId] = book1D(name, title, -0.5, 2047.5, 512);
  }

  sprintf(name, "# PU clusters (ZS) ");
  sprintf(title,"Number of PU ZS clusters per event");
  m_numberPUClustersZS = book1D(name, title, -0.5, 999.5, 1000);	
  if ( m_DoBinaryNZS ){
    sprintf(name, "# PU clusters (NZS) ");
    sprintf(title, "Number of PU NZS clusters per event");
    m_numberPUClustersNZS = book1D(name, title, -0.5, 999.5, 1000);
  }

  sprintf(name, "# hits per channel vs sensor ");
  sprintf(title,"Number of hits per channel (PU ZS) vs sensor");
  m_numberHitsMap2D = book2D(name, title, 127.5, 131.5, 4, 0.5, 2047.5, 2048);

  sprintf(name, "bxId");
  sprintf(title,"bxId per event");
  m_bxId = book1D(name, title, -0.5, 3599.5, 3600);

  sprintf(name, "triggerType");
  sprintf(title,"trigger type per event");
  m_trgType = book1D(name, title, -0.5, 7.5, 8);

  sprintf(name, "multiplicity");
  sprintf(title,"PU multiplicity per event");
  m_multHisto = book1D(name, title, 0., 512., 512);

  sprintf(name, "L0PUbanksize");
  sprintf(title,"L0PU Raw Bank size per event");
  m_banksizeHisto = book1D(name, title, 0., 100., 100);

  sprintf(name, "PUpeak1");
  sprintf(title,"PU peak1 height");
  m_peak1Histo = book1D(name, title, 0., 100., 100);

  sprintf(name, "PUpeak2");
  sprintf(title,"PU peak2 height");
  m_peak2Histo = book1D(name, title, 0., 100., 100);

  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode PUClustersMoni::execute() {
  debug() << "==> Execute" << endmsg;
  m_numOfEvents++;
  //debug()<< " number of evts: " << m_numOfEvents <<endmsg;

  // initialize bxId type
  m_bxNoBeam=0;
  m_bxBeam1=0;
  m_bxBeam2=0;
  m_bxBeamCrossing=0;
  m_bxUnknown=0;

  // get Odin info
  LHCb::ODIN *odin=0;
  ++m_numOfGoodEvents;
  if(exist<LHCb::ODIN>((LHCb::ODINLocation::Default))) {
    odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default); // only one Odin!
  }else{
    m_odinDecoder->getTime(); // in case one wants to get other kinds of info from odin.
    odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);   
  }
  m_triggerType = odin->triggerType();
  m_trgType->fill(m_triggerType);
  //debug() << "triggerType " << m_triggerType << endmsg;

  m_bxId->fill(odin->bunchId());
   
  //debug() << " *** bxId Type " << odin->bunchCrossingType() << endmsg;
  if (odin->bunchCrossingType() == odin->NoBeam) m_bxNoBeam=1;
  else if (odin->bunchCrossingType() == odin->Beam1) m_bxBeam1=1;
  else if (odin->bunchCrossingType() == odin->Beam2) m_bxBeam2=1;
  else if (odin->bunchCrossingType() == odin->BeamCrossing) m_bxBeamCrossing=1;
  else { 
    m_bxUnknown=1;
    return Info( "report configuration Empty!", StatusCode::SUCCESS ); /// check
  }

  m_bunchId = odin->bunchId();
  m_stepNum = odin->calibrationStep();
  m_evtNum = odin->eventNumber();

  // get L0DU info
  debug() << "GET L0DU" << endmsg;
  StatusCode L0DUdataStatus = getL0DU();

  // check banks (PU and Rich)
//   LHCb::RawEvent* m_rawEvent = get<LHCb::RawEvent>(LHCb::RawEventLocation::Default);
//   LHCb::RawBank::BankType typeNZS = LHCb::RawBank::BankType(LHCb::RawBank::L0PUFull);
//   LHCb::RawBank::BankType typeZS = LHCb::RawBank::BankType(LHCb::RawBank::L0PU);
//   LHCb::RawBank::BankType typeRich = LHCb::RawBank::BankType(LHCb::RawBank::Rich);
//   const std::vector<LHCb::RawBank*> banksNZS = m_rawEvent->banks(typeNZS); //bank name not defined yet!
//   const std::vector<LHCb::RawBank*> banksZS = m_rawEvent->banks(typeZS); //bank name not defined yet!    
//   const std::vector<LHCb::RawBank*> banksRich = m_rawEvent->banks(typeRich); //bank name not defined yet!
//   debug() 	<< "The event contains banks with size: RICH(" << banksRich.size()
// 		<< "), PU(" << 	banksZS.size()
// 		<< "), PUNZS(" << banksNZS.size() << ")" << endmsg;


///
   // get the event and check bank size
   StatusCode eventStatus = getRawEvent();
   if( eventStatus.isFailure() ){
     return Info( "Raw event not found! - skip this event", StatusCode::SUCCESS ); 
   }
///

   // get PU clusters and do the monitoring 
   StatusCode dataStatus = getData();
   StatusCode moniStatus;
   if( dataStatus.isSuccess() ){
     moniStatus = rawPUClusterMonitor();
   }else{
     //return ( dataStatus );
   }
   if( moniStatus.isFailure() ){
     return Error( "PileUp cluster monitoring failed!", moniStatus ); 
   }
  
   // get PU analog clusters and do stuff
   StatusCode analogmoniStatus;
   if( dataStatus.isSuccess() && m_DoAnalogNZS ){
     analogmoniStatus = rawAnalogClusterMonitor();
     if( analogmoniStatus.isFailure() ){
     return Error( "PileUp analog cluster monitoring failed!", analogmoniStatus ); 
     }
   }

  // fill in Ntuple
  if ( !m_DoOnLine ) {
    m_OutputTree->Fill();
    if ( m_DoAnalogNZS ){
      m_OutputTreeAnalog->Fill();
    }
  }
  return ( StatusCode::SUCCESS );
}

//=============================================================================
StatusCode PUClustersMoni::getL0DU()
{
//   debug() << "use decoder of L0DU " << endmsg;
//   debug()<< "L0DU version is " << m_fromRaw->version() << endmsg;
//   debug()<< "L0DU tck is " << m_fromRaw->tck() << endmsg;

  m_fromRaw->decodeBank();

  debug() << "PU multiplicity is " << m_fromRaw->data("PUHits(Mult)") << endmsg;
  m_PUmult =  m_fromRaw->data("PUHits(Mult)");
  m_multHisto->fill(m_PUmult);
  debug() << "PU peak1 cont is " << m_fromRaw->data("PUPeak1(Cont)") << endmsg;
  m_PUpeak1 =  m_fromRaw->data("PUPeak1(Cont)");
  m_peak1Histo->fill(m_PUpeak1);
  debug() << "PU peak2 cont is " << m_fromRaw->data("PUPeak2(Cont)") << endmsg;
  m_PUpeak2 =  m_fromRaw->data("PUPeak2(Cont)");
  m_peak2Histo->fill(m_PUpeak2);

  m_PUstatus1 = m_fromRaw->data("PU1(Status)");
  m_PUstatus2 = m_fromRaw->data("PU2(Status)");
  debug() << "PU1 Status: " << m_fromRaw->data("PU1(Status)") << endmsg;
  debug() << "PU2 Status: " << m_fromRaw->data("PU2(Status)") << endmsg;
//   debug() << "L0DU bcid first: " << m_fromRaw->bcid().first << endmsg;
//   debug() << "L0DU bcid second: " << m_fromRaw->bcid().second << endmsg;

//   debug()<< "get report from " << LHCb::L0DUReportLocation::Default << endmsg;
  if( !exist<LHCb::L0DUReport>( LHCb::L0DUReportLocation::Default )){
    Info("L0DUReport not found at location ").ignore();
    return StatusCode::SUCCESS;
  }

  //report
  LHCb::L0DUReport * report =  get<LHCb::L0DUReport> ( LHCb::L0DUReportLocation::Default );
  debug()<< " ==> getL0DU()  report" <<endmsg;
  if( report->configuration() != NULL ){
    LHCb::L0DUConfig * config   = report->configuration();
//     debug()<< " ==> getL0DU()  config" <<endmsg;
  
    LHCb::L0DUChannel::Map & channels = config->channels(); ////L0DUChannel
//     debug() << "________________ Trigger decision from raw ____________________ L0-yes = " 
//             << report->decision() << endmsg;
    //info() << "Rebuilt decision from summary : " << m_fromRaw->report().decisionFromSummary() << endmsg;
    for(LHCb::L0DUChannel::Map::iterator it = channels.begin();channels.end()!=it;it++){
      std::string name = ((*it).second)->name();
       debug() << "Channel Decision " << name << " : " << report->channelDecisionByName( name ) << endmsg;
      if (name == "CALO"){ m_CaloTrg = report->channelDecisionByName(name);}
      if (name == "HCAL"){ m_HcalTrg = report->channelDecisionByName(name);}
      if (name == "MUON" ){m_MuonTrg = report->channelDecisionByName(name);}
      if (name == "SPD"){ m_SpdTrg = report->channelDecisionByName(name);}
      if (name == "PU"){ m_PUTrg = report->channelDecisionByName(name);}
    }
    LHCb::L0DUElementaryCondition::Map& conds = config->conditions();////L0DUElementaryCondition
    for(LHCb::L0DUElementaryCondition::Map::iterator it = conds.begin();conds.end()!=it;it++){
      std::string name = ((*it).second)->name();
      debug() << "Condition Value " << name << " : " << report->conditionValueByName( name ) << endmsg;
    }
  }
  else {
    debug()<< "report configuration Empty!" << endmsg;
  }

 return ( StatusCode::SUCCESS );
}
//==============================================================================

//=============================================================================
StatusCode PUClustersMoni::getData()
{
  debug()<< " ==> getData() " << endmsg;
  // get binary ZS clusters
  if( !exist<LHCb::VeloClusters>(m_PUclusterContZS) ){
    Info( " ==> There is no PUClusters in ZS TES location" );
  } else {
//     debug() << "I get the ZS PUClusters! from " << m_PUclusterContZS << endmsg;
    m_rawPUClustersZS = get<LHCb::VeloClusters>(m_PUclusterContZS);
  }
  
  // get binary NZS clusters
  if ( m_DoBinaryNZS ){
    if( !exist<LHCb::VeloClusters>(m_PUclusterContNZS) ){
      Info( " ==> There is no PUClusters in NZS TES location" );
      } else {
      m_rawPUClustersNZS = get<LHCb::VeloClusters>(m_PUclusterContNZS);
//       debug() << "I get NZS clusters from " << m_PUclusterContNZS << endmsg;
    }
  }

  // get DeVelo to check analog banks
  if (m_DoAnalogNZS){ 
    m_velo = getDet<DeVelo>( DeVeloLocation::Default );
    if ( !m_velo ){
      return Error("==> There is no DeVelo! Cannot monitor analog banks", StatusCode::SUCCESS) ;
    } 
  }

  //
  debug()	<< " ==> Number of PU ZS clusters found in TES at: "  
  		<< m_PUclusterContZS << " is: " 
          	<< m_rawPUClustersZS->size() <<endmsg;
  if ( m_DoBinaryNZS ){
    debug()	<< " ==> Number of PU NZS clusters found in TES at: "  
  		<< m_PUclusterContNZS << " is: " 
          	<< m_rawPUClustersNZS->size() <<endmsg;
    } 
  m_numberPUClustersZS->fill( m_rawPUClustersZS->size() );	
  if ( m_DoBinaryNZS ){
    m_numberPUClustersNZS->fill( m_rawPUClustersNZS->size() );
  }
   
  return ( StatusCode::SUCCESS );
}

//==============================================================================
StatusCode PUClustersMoni::getRawEvent() {

  if (msgLevel(MSG::DEBUG)) debug() << "==> getRawEvent()" << endmsg;
  
  if ( !exist<LHCb::RawEvent>(m_rawEventLoc) )
  {
//     info() << "==> There is no RawEvent at: " << m_rawEventLoc << endmsg;
    return StatusCode::FAILURE; 
  }
  else
  {
    m_rawEvent = get<LHCb::RawEvent>(m_rawEventLoc);
    debug() << "==> RawEvent read-in from location: "
	    << m_rawEventLoc << endmsg;

    /// check L0PU (ZS) bank length!!
    const std::vector<LHCb::RawBank*> banks = m_rawEvent->banks(LHCb::RawBank::L0PU); 
    std::vector<LHCb::RawBank*>::const_iterator bi;
    for ( bi = banks.begin(); bi != banks.end(); bi++) 
    {
      LHCb::RawBank* aBank = *bi;
      unsigned int wordTot = (aBank->size() / ( 2 * sizeof(unsigned int) ));
      debug() << "L0PU bank size is " << wordTot << endmsg;
      m_bankSize=wordTot;
      m_banksizeHisto->fill(m_bankSize);
    } // loop on PU banks
    ///
  }
  
  return ( StatusCode::SUCCESS ); 
}

//==============================================================================
StatusCode PUClustersMoni::rawPUClusterMonitor()
{
  debug()<< " ==> rawPUClusterMonitor() " <<endmsg;

  // ZS clusters ----------------------------------------
  int contSize = m_rawPUClustersZS->size();
  debug() <<  "ZS clusters size: " << contSize << endmsg;
  
  if(!contSize){
    return Info("Empty ZS cluster container! - Skipping monitor", StatusCode::SUCCESS );
  }

  // loop on clusters
  LHCb::VeloClusters::const_iterator cluIt;
  m_numHitsZS = 0;
  m_numHitsZS_128 = 0;
  m_numHitsZS_130 = 0;
  m_numHitsZS_129 = 0;
  m_numHitsZS_131 = 0;
  m_PUmultFake = 0;
  for( cluIt = m_rawPUClustersZS->begin(); cluIt != m_rawPUClustersZS->end(); cluIt++ )
  {
     LHCb::VeloCluster* cluster = (*cluIt);
 
     // cluster size - number of strips in cluster, should be 1 for PU
     unsigned int clusterSize=cluster->size();
//      debug()<< " ==> Cluster size: " << clusterSize <<endmsg;

     // channelID info (this channel is used to create VeloLightCluster)
     LHCb::VeloChannelID channel = cluster->channelID();
     // put a check on the sensor
     if ( channel.sensor() == 128 || channel.sensor() == 129 || channel.sensor() == 130 || channel.sensor() == 131 ){

     if ( !m_DoOnLine ) sensorZS_ptr[m_numHitsZS] = channel.sensor();
     for(unsigned int i=0; i<clusterSize; i++){ // should be only 1
       if ( !m_DoOnLine ) stripZS_ptr[m_numHitsZS]  = cluster->strip(i);
       if ( clusterSize == 1 ){
         m_numberHitsMap2D->fill(channel.sensor(), cluster->strip(i));
         m_numberHitsPlot[ (channel.sensor()) - 128 ]->fill( cluster->strip(i) );
       }

       if ( !m_DoOnLine ) {
         // corr plot
         if ( channel.sensor() == 128 ){
           stripZS_128_ptr[m_numHitsZS_128]  = cluster->strip(i);
           m_numHitsZS_128++;
         }else if ( channel.sensor() == 130 ){
           stripZS_130_ptr[m_numHitsZS_130]  = cluster->strip(i);
           m_numHitsZS_130++;
         }else if ( channel.sensor() == 129 ){
           stripZS_129_ptr[m_numHitsZS_129]  = cluster->strip(i);
           m_numHitsZS_129++;
         }else if ( channel.sensor() == 131 ){
           stripZS_131_ptr[m_numHitsZS_131]  = cluster->strip(i);
           m_numHitsZS_131++;
         }
       }
     }
     m_numHitsZS++;

     }// end check sensors

   } // loop on ZS clusters
//    debug() << "This event has " << m_numHitsZS_130 << " hits on sensor 130 and " << m_numHitsZS_131 << " on sensor 131 " << endmsg;
   m_PUmultFake = m_numHitsZS_130 + m_numHitsZS_131;
   //} // to solve the crash on events with empty ZS bank

   // just to check
   if ( m_numHitsZS != contSize ){ 
     return Info( "There's something wrong in the ZSclusters collection", StatusCode::SUCCESS );
   }

   // NZS clusters ----------------------------------------
   m_numHitsNZS = 0;
   if ( m_DoBinaryNZS ){
     debug() << "I do binary NZS moni " << endmsg;
     int contSize = m_rawPUClustersNZS->size();
     if(!contSize){
       return Info( "Empty NZS cluster container! - Skipping monitor", StatusCode::SUCCESS );
     }
     LHCb::VeloClusters::const_iterator cluIt;
     for( cluIt = m_rawPUClustersNZS->begin(); cluIt != m_rawPUClustersNZS->end(); cluIt++ )
     {
       LHCb::VeloCluster* cluster = (*cluIt);
       unsigned int clusterSize=cluster->size();
//        debug()<< " ==> Cluster size: " << clusterSize <<endmsg;

       LHCb::VeloChannelID channel = cluster->channelID();
       if ( !m_DoOnLine ){ 
         sensorNZS_ptr[m_numHitsNZS] = channel.sensor();
         for(unsigned int i=0; i<clusterSize; i++){ // should be only 1
           stripNZS_ptr[m_numHitsNZS]  = cluster->strip(i);
         }
       }
       m_numHitsNZS++;
     } // loop on NZS clusters
     //}// to solve the crash on events with empty NZS bank

     // just to check
     if ( m_numHitsNZS != contSize ){ 
       debug() << "There's something wrong in the NZSclusters collection" << endmsg;
     }
   }
   debug() << "END of NZS binary moni " << endmsg;
   

  return ( StatusCode::SUCCESS );
}
//=============================================================================
// ------ NZS VELOFull (analog)  monitoring
StatusCode PUClustersMoni::rawAnalogClusterMonitor()
{
  debug()<< " ==> rawAnalogClusterMonitor() " <<endmsg;
  // get analog NZS clusters  
  m_numHitsVelo = 0;
  m_numHeadVelo = 0;
  m_numHitsHeadVelo = 0;
  
  // ADCs
  // get analog NZS clusters  
  VeloTELL1Datas* m_tell1DataContainer;
  if( !exist<VeloTELL1Datas>(m_PUanalogclusterContNZS) ){
    return Info("NZS VELO ==> There is no VeloFull bank (ADCs) in the TES - Skipping analog monitor", StatusCode::SUCCESS );    
  } else {
    m_tell1DataContainer = get<VeloTELL1Datas>(m_PUanalogclusterContNZS);
    debug() << "NZS VELO ADC: size is " << m_tell1DataContainer->size() <<endmsg;
  }

  // headers
  VeloTELL1Datas* m_decodedHeaders;
  if( !exist<VeloTELL1Datas>(m_PUanalogheaderContNZS) ){
    return Info("NZS VELO ==> There is no VeloFull bank (Headers) in the TES - Skipping analog monitor", StatusCode::SUCCESS );    
  }
  else{
    m_decodedHeaders = get<VeloTELL1Datas>(m_PUanalogheaderContNZS);
    debug() << "NZS VELO HEADER: size is " << m_decodedHeaders->size() <<endmsg;
  }
  VeloTELL1Datas::const_iterator sensIt;
  VeloTELL1Datas::const_iterator headerIt= m_decodedHeaders->begin();
  for( sensIt = m_tell1DataContainer->begin(); sensIt != m_tell1DataContainer->end(); ++sensIt ){
     if(sensIt!=m_tell1DataContainer->begin()) ++headerIt;
     VeloTELL1Data* dataOneTELL1=(*sensIt);
     //VeloTELL1Data* headerOneTELL1 = (*headerIt);
     int TELL1_number=dataOneTELL1->key();
     
     if ( TELL1_number > 127 &&  TELL1_number < 132){ 
       debug() << "Tell1 number is " <<  TELL1_number << endmsg;
     /*
     //another way
     for(int link = 0; link<64; ++link){
       std::vector<int> linkHeaders = getLink( (*headerOneTELL1)[link] );
       std::vector<int>::iterator it;
       int prova = 0;
       for(it=linkHeaders.begin();it!=linkHeaders.end() ; ++it){
         debug()<< "ALTRO MODO: Event " << m_numOfEvents << ", Link " <<  link << ", head["
                 << prova << "] = " << *(it) << endmsg;
         prova++;
       }
     }
     */ 

       //Get sensor from detector element:
       const DeVeloSensor* sensor =  m_velo->sensorByTell1Id( TELL1_number );

       // get Velo strips, beetle channels and ADCs
       for(int index = 0; index < 2048; ++index ){
         unsigned int channel = index;
         unsigned int link = index/32;
         unsigned int strip = sensor->ChipChannelToStrip(channel);
         signed int value = dataOneTELL1->channelADC(channel);
         if ( !m_DoOnLine ){ 
           sensor_Velo[m_numHitsVelo] = TELL1_number;
           channel_Velo[m_numHitsVelo] = channel;
           link_Velo[m_numHitsVelo] = link;
           strip_Velo[m_numHitsVelo] = strip;
           adc_Velo[m_numHitsVelo] = value;
         }
         m_VeloNZSPlot[TELL1_number - 128]->fill(channel + (link+1)*4, value);
         m_numHitsVelo++;
       }

        // get headers for the given TELL1    number
       VeloTELL1Data* adcHeaders = m_decodedHeaders->object( TELL1_number );
       ALinkPair begEndHeader;
       scdatIt ItHeader;
       for( int ALinkCounter=0; ALinkCounter < 64; ++ALinkCounter ){
         begEndHeader=(*adcHeaders)[ALinkCounter];
         ItHeader=begEndHeader.first;
         int cont = 0;
         for( ; ItHeader!=begEndHeader.second; ++ItHeader){
           signed int headvalue = *(ItHeader);
           if ( !m_DoOnLine ) head_Velo[m_numHeadVelo] = headvalue;
           m_VeloNZSPlotH[TELL1_number - 128]->fill(cont + ALinkCounter*36, headvalue);

           cont++;
           m_numHeadVelo++;
         }
       } // link loop
     } // check on Tell1Number
  } // sensors loop
  debug() << "end of rawAnalogClusterMonitor" << endmsg;
  // VELOFULL
  
  return ( StatusCode::SUCCESS ); 
}
//=============================================================================

std::vector<int> PUClustersMoni::getLink (VeloTELL1::ALinkPair begEnd)
{
  std::vector<int>::const_iterator iT; //iterator to vector of unsigned int.
  std::vector<int> linkVector;
  linkVector.clear();

  //Loop over ADC values in link
  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
    linkVector.push_back(*iT); //Push back ADC value to vector
  }
  return linkVector;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode PUClustersMoni::finalize() 
{
  debug() << "==> Finalize" << endmsg;
  if ( !m_DoOnLine ) {
    m_OutputTree->Write();
    if ( m_DoAnalogNZS ){
      m_OutputTreeAnalog->Write();
    }
    m_OutputFile->Close();
  }
  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
//=============================================================================

// prepare TTrees
void PUClustersMoni::bookTree() 
{
 const int numHitMax = 2048; // 4 x 4 x 4 (regions) x 32 (PU strips)
 sensorZS_ptr	= new int[numHitMax];
 stripZS_ptr	= new int[numHitMax];
 sensorNZS_ptr	= new int[numHitMax];
 stripNZS_ptr	= new int[numHitMax];
 m_OutputTree->Branch("numEvt", &m_numOfEvents, "numEvt/I"); 
 m_OutputTree->Branch("triggerType", &m_triggerType, "triggerType/I"); 
 m_OutputTree->Branch("numGoodEvt", &m_numOfGoodEvents, "numGoodEvt/I"); 
 m_OutputTree->Branch("sizenumHitZS", &m_numHitsZS, "sizenumHitZS/I");
 m_OutputTree->Branch("sensorZS", sensorZS_ptr,"sensorZS[sizenumHitZS]/I");
 m_OutputTree->Branch("stripZS", stripZS_ptr,"stripZS[sizenumHitZS]/I");
 //if ( m_triggerType == 5 ){
 if ( m_DoBinaryNZS ){
   m_OutputTree->Branch("sizenumHitNZS", &m_numHitsNZS, "sizenumHitNZS/I");
   m_OutputTree->Branch("sensorNZS", sensorNZS_ptr,"sensorNZS[sizenumHitNZS]/I");
   m_OutputTree->Branch("stripNZS", stripNZS_ptr,"stripNZS[sizenumHitNZS]/I");
 }
 m_OutputTree->Branch("step", &m_stepNum, "step/I");
 m_OutputTree->Branch("L0Id", &m_evtNum, "L0Id/I");
 m_OutputTree->Branch("L0PUbanksize", &m_bankSize, "L0PUbanksize/I");
 m_OutputTree->Branch("bxId", &m_bunchId, "bxId/I");
 m_OutputTree->Branch("bxNoBeam", &m_bxNoBeam, "bxNoBeam/I");
 m_OutputTree->Branch("bxBeam1", &m_bxBeam1, "bxBeam1/I");
 m_OutputTree->Branch("bxBeam2", &m_bxBeam2, "bxBeam2/I");
 m_OutputTree->Branch("bxBeamCrossing", &m_bxBeamCrossing, "bxBeamCrossing/I");
 m_OutputTree->Branch("bxUnknown", &m_bxUnknown, "bxUnknown/I");




 // for correlation plot
 stripZS_128_ptr = new int[numHitMax];
 stripZS_130_ptr = new int[numHitMax];
 m_OutputTree->Branch("sizenumHitZS_128", &m_numHitsZS_128, "sizenumHitZS_128/I");
 m_OutputTree->Branch("sizenumHitZS_130", &m_numHitsZS_130, "sizenumHitZS_130/I");
 m_OutputTree->Branch("stripZS_130", stripZS_130_ptr,"stripZS_130[sizenumHitZS_130]/I");
 m_OutputTree->Branch("stripZS_128", stripZS_128_ptr,"stripZS_128[sizenumHitZS_128]/I");
 stripZS_129_ptr = new int[numHitMax];
 stripZS_131_ptr = new int[numHitMax];
 m_OutputTree->Branch("sizenumHitZS_129", &m_numHitsZS_129, "sizenumHitZS_129/I");
 m_OutputTree->Branch("sizenumHitZS_131", &m_numHitsZS_131, "sizenumHitZS_131/I");
 m_OutputTree->Branch("stripZS_131", stripZS_131_ptr,"stripZS_131[sizenumHitZS_131]/I");
 m_OutputTree->Branch("stripZS_129", stripZS_129_ptr,"stripZS_129[sizenumHitZS_129]/I");

 // L0DU info
 m_OutputTree->Branch("CaloDecision", &m_CaloTrg, "CaloDecision/I");
 m_OutputTree->Branch("HcalDecision", &m_HcalTrg, "HcalDecision/I");
 m_OutputTree->Branch("MuonDecision", &m_MuonTrg, "MuonDecision/I");
 m_OutputTree->Branch("PUDecision", &m_PUTrg, "PUDecision/I");
 m_OutputTree->Branch("SpdDecision", &m_SpdTrg, "SpdDecision/I");
 m_OutputTree->Branch("PUMult", &m_PUmult, "PUMult/I");
 m_OutputTree->Branch("PUpeak1", &m_PUpeak1, "PUpeak1/I");
 m_OutputTree->Branch("PUpeak2", &m_PUpeak2, "PUpeak2/I");
 m_OutputTree->Branch("PUstatus1", &m_PUstatus1, "PUstatus1/I");
 m_OutputTree->Branch("PUstatus2", &m_PUstatus2, "PUstatus2/I");
 m_OutputTree->Branch("PUMultEval", &m_PUmultFake, "PUMultEval/I");
}

void PUClustersMoni::bookTreeAdcNZS()
{
 const int numHitMax = 8192; // 4 (sensors) x 2048 (Velo strips)
 const int numHeadMax =  1024; // 4 (sensors) x 64 (links per sensor) x 4 (header words)
 sensor_Velo    = new int[numHitMax+numHeadMax];
 strip_Velo     = new int[numHitMax];
 channel_Velo   = new int[numHitMax];
 link_Velo      = new int[numHitMax];
 adc_Velo       = new float[numHitMax];
 head_Velo      = new float[numHeadMax];
 m_OutputTreeAnalog->Branch("numEvt", &m_numOfEvents, "numEvt/I");
 m_OutputTreeAnalog->Branch("numHitVelo", &m_numHitsVelo, "numHitVelo/I");
 m_OutputTreeAnalog->Branch("sensorVelo", sensor_Velo,"sensorVelo[9216]/I");
 m_OutputTreeAnalog->Branch("stripVelo", strip_Velo,"stripVelo[numHitVelo]/I");
 m_OutputTreeAnalog->Branch("linkVelo", link_Velo,"linkVelo[numHitVelo]/I");
 m_OutputTreeAnalog->Branch("channelVelo", channel_Velo,"channelVelo[numHitVelo]/I");
 m_OutputTreeAnalog->Branch("adcVelo", adc_Velo,"adcVelo[numHitVelo]/F");
 m_OutputTreeAnalog->Branch("numHeadVelo", &m_numHeadVelo, "numHeadVelo/I");
 m_OutputTreeAnalog->Branch("headVelo", head_Velo,"headVelo[numHeadVelo]/F"); ///// dubbiosa sulla size...
 m_OutputTreeAnalog->Branch("numHitsHeadVelo", &m_numHitsHeadVelo, "numHitsHeadVelo/I");
 m_OutputTreeAnalog->Branch("step", &m_stepNum, "step/I");
 m_OutputTreeAnalog->Branch("L0Id", &m_evtNum, "L0Id/I");
 m_OutputTreeAnalog->Branch("bxId", &m_bunchId, "bxId/I");
 m_OutputTreeAnalog->Branch("triggerTypeVelo", &m_triggerType, "triggerTypeVelo/I");

 // L0DU info
 m_OutputTreeAnalog->Branch("CaloDecision", &m_CaloTrg, "CaloDecision/I");
 m_OutputTreeAnalog->Branch("HcalDecision", &m_HcalTrg, "HcalDecision/I");
 m_OutputTreeAnalog->Branch("MuonDecision", &m_MuonTrg, "MuonDecision/I");
 m_OutputTreeAnalog->Branch("PUDecision", &m_PUTrg, "PUDecision/I");
 m_OutputTreeAnalog->Branch("SpdDecision", &m_SpdTrg, "SpdDecision/I"); 
}

// ==============================================================================

