// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/AlgFactory.h" 
// Event
#include "VeloEvent/EvtInfo.h"
#include "Event/VeloODINBank.h"
#include "VeloEvent/VeloTELL1Data.h"
#include "GaudiKernel/IEventTimeDecoder.h"            // Interface
#include "Event/ODIN.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
// local
#include "CommonFunctions.h"
//velo
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"
// from TELL1 Kernel:
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "Tell1Kernel/VeloDecodeConf.h" 
// Boost 
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
//-----------------------------------------------------------------------------
// Class : L0AcceptScan
//-----------------------------------------------------------------------------
using namespace LHCb;
using namespace VeloTELL1;
//typedef std::vector< std::vector<double> > tell1data; 
namespace Velo
{
  namespace Monitoring
  {
	class  L0AcceptScan : public GaudiHistoAlg {
	  public:
		/// Standard constructor
		L0AcceptScan( const std::string& name, ISvcLocator* pSvcLocator );
		virtual ~L0AcceptScan( ); ///< Destructor
		virtual StatusCode initialize() override;    ///< Algorithm initialization
		virtual StatusCode execute   () override;    ///< Algorithm execution
		virtual StatusCode finalize  () override;    ///< Algorithm finalization
		std::vector<double> SubtractCommonMode( std::vector<int> headers, std::vector<int> data, double cut, int howManyPasses );
		int m_method;
	  private  :
		unsigned long int m_samplingPhaseFirstEvent;
		unsigned long int m_samplingPhaseBurstSize;
		unsigned long int m_samplingPhaseNBursts;
		std::vector<std::string>  m_locations;
		unsigned long int m_runNumber;
		unsigned long long m_gpsTime;
		unsigned int m_newphasedeltaT;
		unsigned long long m_l0EventID;
		unsigned int m_l0EventCounter;
		VeloTELL1Datas* m_tell1DataContainer;
		VeloTELL1Datas* m_headerContainer;
		//static std::map<int, tell1data> PedestalsPerTell1; // map of tell1Id, [link][ch+head] == [64][36] double.
		EvtInfos*       m_evtInfos;
		IEventTimeDecoder* m_odinDecoder; ///< Pointer to tool to decode ODIN bank
		unsigned long int  m_evtNumber;
		unsigned int  m_lastBunchCounter;   
		int  m_lastSample;   
		int m_refresh;
		std::vector<int> m_chosenTell1s;
		int m_plot2d;
		bool m_inverted;
		bool m_plotMeanAndRMS;
		int m_plotcms;
		int m_plotrawprofile;
		int m_plotprofile;
		int m_bunchCounter;
		//      int m_substractPedestals;
		float m_commonModeCut;
		int   m_nEventsToSkip;
		int   m_firsttime;
		int   m_nSteps;
		int   m_stepSize;
		double   m_nStat;
		std::map<int, std::vector<std::vector<float> > >  m_mean;//tell1id, [64][36];
		std::map<int, std::vector<std::vector<float> > >  m_rms;//tell1id, [64][36];
		bool m_evenodd;
		int l0EventCounter() { return   m_l0EventCounter; };
		void l0EventCounter( int id) { m_l0EventCounter = id; };
		bool OddEvent() { return ( m_l0EventCounter%2==1 ? true : false ); }
		bool EvenEvent() { return ( m_l0EventCounter %2==0 ? true : false ); }
		unsigned long long EventNumber() { return m_evtNumber; }
		//Returns a vector with the link ADCs.
		std::vector<int> getLink(VeloTELL1::ALinkPair begEnd) {   
		  std::vector<int>::const_iterator iT; //iterator to vector of unsigned int.
		  std::vector<int> linkVector;
		  linkVector.clear();

		  //Loop over ADC values in link
		  for(iT=begEnd.first; iT!=begEnd.second; ++iT){    
			linkVector.push_back(*iT); //Push back ADC value to vector
		  }
		  return linkVector;
		}
	};
  }
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::L0AcceptScan::L0AcceptScan( const std::string& name,
	ISvcLocator* pSvcLocator)
:  GaudiHistoAlg( name , pSvcLocator )
  , m_l0EventCounter (0)
  , m_evtNumber ( 0 )
  , m_lastBunchCounter(0)
, m_lastSample(0)

{
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::ADCs             ) ;
  //m_locations.push_back ( LHCb::VeloTELL1DataLocation::PedSubADCs       ) ;
  //m_locations.push_back ( LHCb::VeloTELL1DataLocation::FIRCorrectedADCs ) ;
  //m_locations.push_back ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ) ;
  declareProperty( "Containers" ,   m_locations  ,  "The list of TES locations to be monitored") ;
  declareProperty( "TELL1s",                        m_chosenTell1s                       );
  declareProperty( "Plot2D",                        m_plot2d = 0                         );
  declareProperty( "PlotRawProfile",                m_plotrawprofile = 0                 );
  declareProperty( "PlotProfile",                   m_plotprofile = 0                    );
  declareProperty( "EventsToSkip",                  m_nEventsToSkip = 0                    );
  declareProperty( "NSteps",                 m_nSteps = 60                    );
  declareProperty( "StepSize",                     m_stepSize = 100                    );
}

//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::L0AcceptScan::~L0AcceptScan() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::L0AcceptScan::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  debug() << "==> Initialize" << endmsg;
  m_odinDecoder = tool<IEventTimeDecoder>( "OdinTimeDecoder" );
  setHistoTopDir( "Vetra/" );
  m_lastBunchCounter =-1;
  m_firsttime=0;
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::L0AcceptScan::finalize() {
  debug() << "==> Finalize" << endmsg;
  return GaudiHistoAlg::finalize();  // must be called after all other actions
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::L0AcceptScan::execute() {
  int sample = 0;
  m_evtNumber++;
  int currentStep = (int) m_evtNumber/m_stepSize;
  debug() << "==> Execute" << endmsg;
  m_nStat= m_samplingPhaseBurstSize-m_nEventsToSkip;
  if(!exist<EvtInfos>( EvtInfoLocation::Default  )){
	debug()<< " ==> There is no Event info in the data. QUITTING. " <<endmsg;
	return ( StatusCode::SUCCESS );
  }else{
	m_evtInfos  = get<EvtInfos>      ( EvtInfoLocation::Default       );
  }
  if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::ADCs  )){ //put in location here.
	info()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::ADCs  <<endmsg;
	return StatusCode::FAILURE;

  }else{
	m_tell1DataContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::ADCs );
  }

  if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers )){ //put in location here.
	info()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::Headers  <<endmsg;
	return StatusCode::FAILURE;

  }else{
	m_headerContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers );
  }


  EvtInfo* anInfo; 
  EvtInfos::const_iterator infoIt;
  int TELL1_number;
  for(infoIt=m_evtInfos->begin();infoIt!=m_evtInfos->end();++infoIt){
	anInfo=(*infoIt);
	TELL1_number=anInfo->key();
	if(ExcludeTELL1(m_chosenTell1s,TELL1_number)) continue;
	if(m_evtNumber == 1){
	  m_lastBunchCounter = 0;
	  m_lastSample = 0;
	}
	else{
	  if( anInfo->bunchCounter(0) != m_lastBunchCounter+1){
		sample =0;
	  }  
	  else if(anInfo->bunchCounter(0) == m_lastBunchCounter+1){
		sample = m_lastSample+1;
	  }
	}
	VeloTELL1::dataVec iheadersvec;
	VeloTELL1::dataVec ipcnvec;

	if(anInfo!=0 ){
	  //info()<< " bunch counter " << anInfo->bunchCounter(0) << endmsg; // the 0 stands for PPFPGA 0.
	  //info()<< " Detector ID " << anInfo->detectorID(0) << endmsg; //should be 1 for velo
	  //info()<< " L0 Event Number " << anInfo->l0EventID(0) << endmsg;
	  info()<< " FEM PCN " << anInfo->FEMPCN(0) << endmsg;
	  //info()<< " adcFIFOError " << anInfo->adcFIFOError(0) << endmsg;
	  //info()<< " channelError " << anInfo->channelError(0) << endmsg;
	  //info()<< " headerPseudoErrorFlag " << anInfo->headerPseudoErrorFlag(0) << endmsg;
	  //info()<< " PCNError" << anInfo->PCNError(0) << endmsg;
	  l0EventCounter(anInfo->l0EventID(0));
	  m_lastBunchCounter=anInfo->bunchCounter(0);
      m_bunchCounter=anInfo->bunchCounter(0);
      iheadersvec = anInfo->IHeader(0);
      ipcnvec =  anInfo->PCNBeetle(0);
          
	}
	else return StatusCode::SUCCESS;
    VeloTELL1::dataVec::const_iterator iheadersiterator;
    VeloTELL1::dataVec::const_iterator ipcniterator;
	VeloTELL1Datas::const_iterator headerIt= m_headerContainer->begin();
	VeloTELL1Data* headerOneTELL1 = (*headerIt);
    int counter =0;
	for (iheadersiterator = iheadersvec.begin(); iheadersiterator != iheadersvec.end(); iheadersiterator++){
	  info() <<  " iheader value" <<  *iheadersiterator <<  " event number " << m_evtNumber << endmsg;
      //printf(" This is the i header in HEX >>> %x         %d \n", *iheadersiterator, counter );
      counter++;
	}
	for (ipcniterator = ipcnvec.begin(); ipcniterator != ipcnvec.end(); ipcniterator++){
	  info() <<  " ipcnvalue" <<  *ipcniterator <<  " event number " << m_evtNumber << endmsg;
      //printf(" This is the i pcn in HEX >>> %x         %d \n", *ipcniterator, counter );
      counter++;
	}
    for(int ilink = 0; ilink < 64; ilink++){
	  std::vector<int> linkHeaders = getLink( (*headerOneTELL1)[ilink] );
      info() <<  "linkHeaders adcs for link  "<< ilink  << " "  << linkHeaders[0] << " "  << linkHeaders[1]  << " "   << linkHeaders[2] << " "   << linkHeaders[3] << endmsg;
    }
	char bchcntvsstep[200]; sprintf(bchcntvsstep, "FemPCNVsStepT%d", TELL1_number);
	//if(anInfo!=0 ) plot2D(currentStep,anInfo->FEMPCN(0), bchcntvsstep, bchcntvsstep, -0.5,60.5, -0.5, 200, 61, 201);
  }
  m_lastSample=sample;
  for ( std::vector<std::string>::const_iterator il = m_locations.begin() ;  m_locations.end() != il ; ++il ) {
	if(!exist<VeloTELL1Datas>(*il)){
	  debug()<< " ==> There is no bank at " << *il <<endmsg;
	  continue;
	}else{
	  m_tell1DataContainer = get<VeloTELL1Datas>( *il );
	}
    //info() << " trying to get container " << endmsg ;
    // info() << " container  " <<   strippedContainerName(*il)  <<      endmsg ;
	VeloTELL1Datas::const_iterator sensIt;
	VeloTELL1Datas::const_iterator headerIt= m_headerContainer->begin();
	for(sensIt=m_tell1DataContainer->begin();sensIt!=m_tell1DataContainer->end();++sensIt){
	  if(sensIt!=m_tell1DataContainer->begin()) ++headerIt;
	  VeloTELL1Data* dataOneTELL1=(*sensIt);
	  VeloTELL1Data* headerOneTELL1 = (*headerIt);
	  TELL1_number=dataOneTELL1->key();
	  if(ExcludeTELL1(m_chosenTell1s,TELL1_number)) continue;
	  for(int link = 0; link<64; ++link){
		boost::format name ( "%s/TELL1_%03d/hAdcsVsSample_Link_%d");
		boost::format alllinks ( "%s/TELL1_%03d/hADCsVsSample");
		//name % polarity % link;           
		name % strippedContainerName(*il) % TELL1_number %  link;          
		alllinks % strippedContainerName(*il) % TELL1_number;          
 
		boost::format title ( "AdcsVsSample Link %d" );
		boost::format alllinkstitle ( "AdcsVsSample" );
		// title % polarity %link;
		title % link;
		std::vector<int> linkADCs = getLink( (*dataOneTELL1)[link] );
		std::vector<int> linkHeaders = getLink( (*headerOneTELL1)[link] );
		std::vector<int>::iterator it;
		// subtract common mode:
		char adcsVsbunch[200]; sprintf(adcsVsbunch,"AdcsVsBunchCntT1%d_link_%d",TELL1_number,link);
		char adcsVsStep[200]; sprintf(adcsVsStep,"AdcsVsStep_T1_%d_link_%d",TELL1_number, link);
		int count =0;
		for(it=linkADCs.begin();it!=linkADCs.end() ; ++it, count++){
          if(count%100 == 0){    
		   // info() << " Adcs for link " <<  link << " Tell1  " << TELL1_number << " : " << *it <<  endmsg;
		   // info() << " sample Number =  " << sample<< endmsg;
          }
            //info () << "running here as well " << endmsg; 
		    //plot2D(m_bunchCounter,*it, adcsVsbunch , adcsVsbunch, -0.5,400.5, -0.5, 1024, 401, 1024);
		    plot2D( ((double)(currentStep+count*m_nSteps))/m_nSteps,*it, adcsVsStep, adcsVsStep, 0,32-(1.0/32.0/m_nSteps), 512-127.5, 512+128.5, m_nSteps*32, 256);
		    //plot2D((currentStep+count*m_nSteps),*it, adcsVsStep, adcsVsStep, -0.50,m_nSteps*32-0.5, -0.5, 1023.5, m_nSteps*32+1, 1024);
		}
	  }
	}
  }
  return StatusCode::SUCCESS;
}

DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, L0AcceptScan )
