#include "GaudiKernel/AlgFactory.h" 
#include "GaudiAlg/GaudiHistoAlg.h"

#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"

#include "boost/format.hpp"

#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

#include "VetraKernel/INoiseEvaluator.h"
#include "VetraKernel/TELL1Noise.h"

#include "CommonFunctions.h"
#include "Tell1Kernel/PhiStripCategoryMap.h"

#ifndef WIN32
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#endif

#include <stdio.h>
#include <math.h>

#include <numeric>

#include <map>
#include <vector>
#include <string>

#include <fstream>
#include <ostream>



// ============================================================================
// AIDA 
// ============================================================================
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

using namespace VeloTELL1;


namespace Velo 
{
  namespace Monitoring 
  {
    /** @classBadChannelMon 
     *  Algorithm to perform monitoring of bad channels based on
     *  noise per channel. 
     *  @author Kurt Rinnert kurt.rinnert@cern.ch
     *  @date   2008-05-27
     */
    class BadChannelMon : public GaudiHistoAlg 
    {
      /// friend factory for instantiation
      friend class AlgFactory<Velo::Monitoring::BadChannelMon>;

      public:

      virtual StatusCode initialize () override;
      virtual StatusCode execute    () override;
      virtual StatusCode finalize   () override;

      protected:

      BadChannelMon 
        ( const std::string& name , 
          ISvcLocator*       pSvc ) 
        : GaudiHistoAlg ( name , pSvc ) 
        , m_locations () 
        , m_tell1s    () 
        , m_check   ( 10000 )
        , m_toolType  ( "Velo::Monitoring::NoiseEvaluator" )
        , m_event     ( 0 )
        , m_noises    () 
        , m_isInit    (0)
        , m_veloDet   (0)
        , m_phiStripCategoryMap()
        , m_linkNoise( m_phiStripCategoryMap.nCategories(), std::vector<float>(64,0.0f))
        , m_linkNoiseRMS( m_phiStripCategoryMap.nCategories(), std::vector<float>(64,0.0f))
        , m_nChecks(0)
        , m_ignoreFirstChannelOnLink(true)

        {
          // this order matters. DO NOT CHANGE IT.
          m_locations.push_back ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ) ;
          m_locations.push_back ( LHCb::VeloTELL1DataLocation::ADCs             ) ;

          declareProperty 
            ( "TELL1s"     , 
              m_tell1s     , 
              "The list of TEL1s to be monitored, empty for all") ;

          declareProperty 
            ( "NoiseEvaluatorType" , 
              m_toolType = "Velo::Monitoring::NoiseEvaluator" , 
              "The actual type of noise evaluator tool" ) ;

          declareProperty 
            ( "CheckFrequency" , 
              m_check             , 
              "The frequency at which we check for bad channels" ) ;            


          declareProperty("RDeadCut",m_RDeadCut = 0.25);
          declareProperty("PhiDeadCut",m_PhiDeadCut = 0.45);
          declareProperty("RNoisyCut",m_RNoisyCut = 0.25);
          declareProperty("PhiNoisyCut",m_PhiNoisyCut = 3.0);

          declareProperty("RawRDeadThreshold",m_rawRDeadThresh = 0.4);
          declareProperty("RawPhiDeadThreshold",m_rawPhiDeadThresh = 0.4);
          declareProperty("RawRNoisyThreshold",m_rawRNoisyThresh = 3.0);
          declareProperty("RawPhiNoisyThreshold",m_rawPhiNoisyThresh = 3.0);

          declareProperty("RDeadCMCThreshold",m_RDeadCMCThresh = 0.2);
          declareProperty("PhiDeadCMCThreshold",m_PhiDeadCMCThresh = 0.2);
          declareProperty("RNoisyCMCThreshold",m_RNoisyCMCThresh = 0.4);
          declareProperty("PhiNoisyCMCThreshold",m_PhiNoisyCMCThresh = 0.4);

          declareProperty("RDeadSigThreshold",m_RDeadSigThresh = 5.0);
          declareProperty("PhiDeadSigThreshold",m_PhiDeadSigThresh = 5.0);
          declareProperty("RNoisySigThreshold",m_RNoisySigThresh = 5.0);
          declareProperty("PhiNoisySigThreshold",m_PhiNoisySigThresh = 5.0);

          declareProperty("CounterCut", m_counterCut = 0.0);
          declareProperty("OutputDirectory",m_outputDir = "./" );
          declareProperty("IgnoreFirstChannelOnLink",m_ignoreFirstChannelOnLink = true );
          declareProperty("RecordTextFiles",m_recordTextFiles=true);
          declareProperty("OutputFileNamePrefix",m_outputFileNamePrefix="");

          setProperty ( "PropertiesPrint", true ).ignore();
        }

      virtual ~BadChannelMon() {}

      StatusCode retrieveTools();
      StatusCode flagBadChannels();
      void writeFileHeader(FILE* outputFile);
      StatusCode writeResultsToFiles();

      StatusCode makeHistos();

      StatusCode writeSummary();
      void createFileName(std::string& fileName, const DeVeloSensor* sensor, unsigned int tell1Number);
      void truncatedMean(std::vector<float>& values, float& mean, float& rms); ///< truncated mean for average noise on a link


      private:

      BadChannelMon ()                  ; ///< no default constructor 
      BadChannelMon ( const BadChannelMon& ) ; ///< no copy constructor 
      BadChannelMon& operator=( const BadChannelMon& ) ; ///< no assignement 

      private:

      // the actual type of container of data locations 
      typedef std::vector<std::string>                  Locations ;
      typedef Velo::Monitoring::INoiseEvaluator::TELL1s TELL1s    ;
      typedef std::map<std::string,Velo::Monitoring::INoiseEvaluator*> Tools ;
      typedef std::map<unsigned int, std::vector< std::vector<unsigned short> > > TELL1ToFlagCounterMap;
      typedef std::vector< std::vector<unsigned short> > FlagCounters;
      typedef std::map<unsigned int, std::vector<float> > NoiseCache;

      /// generic squares accumulation function
      template <class T, class ACCU>
        struct Squares : public std::binary_function<ACCU,T,ACCU>
        {
          ACCU operator() (const ACCU& accu, const T& value) { return accu + value*value; }  
        };

      Locations     m_locations ; ///< the input containers. DO NOT MAKE THESE CONFIGURABLE. 
      TELL1s        m_tell1s    ; ///< list of ids for TELL1 
      unsigned long m_check    ; ///< update frequency
      bool          m_public    ; ///< use the public tools? 
      std::string   m_toolType  ; ///< the actual tool type
      unsigned long m_event     ; ///< internal event counter 
      Tools         m_noises    ; ///< map of tools to be used

      // cut configuration
      float m_RDeadCut;
      float m_PhiDeadCut;
      float m_RNoisyCut;
      float m_PhiNoisyCut;

      float m_rawRDeadThresh;
      float m_rawPhiDeadThresh;
      float m_rawRNoisyThresh ;
      float m_rawPhiNoisyThresh ;

      float m_RDeadCMCThresh ;
      float m_PhiDeadCMCThresh ;
      float m_RNoisyCMCThresh ;
      float m_PhiNoisyCMCThresh;

      float m_RDeadSigThresh ; 
      float m_PhiDeadSigThresh ;
      float m_RNoisySigThresh;
      float  m_PhiNoisySigThresh;

      float m_counterCut ;

      // Flag for initializing the process info bank.
      int m_isInit;
      // Convergence limit from pedestal subtractor
      unsigned int m_convergenceLimit;      
      // Detector element pointer
      DeVelo* m_veloDet; 

      // automatic version tag from CVS
      static const char* m_rcsId; 

      std::string m_login; ///< the login name under which we run
      std::string m_host;  ///< the host we run on
      std::string m_timeTag;  ///< the time at which we run 
      std::string m_fileTimeStamp;  ///< the time at which we run as it appears in the output file names 
      std::string m_outputDir; ///< where we write the results
      std::string m_outputFileNamePrefix; ///< will be prependet to file names if not empty

      // the counters for bad channel flagging, one per flag and Tell1
      TELL1ToFlagCounterMap m_flagCounters;

      // cache of last seen noise values
      NoiseCache m_noiseByTell1;
      NoiseCache m_rawNoiseByTell1;

      // cache for average noise on a link
      PhiStripCategoryMap m_phiStripCategoryMap; 
      std::vector< std::vector<float> > m_linkNoise;
      std::vector< std::vector<float> > m_linkNoiseRMS;

      // the number of checks performed
      unsigned int m_nChecks; 

      //counters for dead and noisy strips
      int nDead;
      int nNoisy;
      int nDeadPerTell1;
      int nNoisyPerTell1;
      int n_totDead;
      int n_totNoisy;
      int count1;
      bool m_ignoreFirstChannelOnLink; ///< shall we ignore the first channel on a link?
      bool m_recordTextFiles; //record .dat files with information about flagged channels

      std::vector< std::map<unsigned int, AIDA::IHistogram1D*> > m_badStripHistos;

    } ;
  } // end of namespace Velo::Monitoring 
} // end of namespace Velo

const char* Velo::Monitoring::BadChannelMon::m_rcsId = "$Id: BadChannelMon.cpp,v 1.24 2010-04-22 15:56:40 krinnert Exp $";


// ============================================================================
//  update bad channel counters
// ============================================================================
StatusCode Velo::Monitoring::BadChannelMon::flagBadChannels() 
{
  std::vector< std::vector<float> > linkNoiseValues(m_phiStripCategoryMap.nCategories()); // storage for compiting truncated mean

  /// loop over all all tools (information sources) 
  int lookingAtRawNoise = 0;
  for ( Tools::const_iterator it = m_noises.begin() ; 
      m_noises.end() != it ; ++it, ++lookingAtRawNoise ) {
    // for the given tool get all TELL1s:
    Velo::Monitoring::INoiseEvaluator::INoiseMap m = it->second->noise() ;

    //Get the container of TELL1 data banks. It's needed to check for reordering.
    if ( !exist<LHCb::VeloTELL1Data::Container> ( it->first ) ) {
      debug() << "Could not retrieve noise data at " << it->first << endmsg;
      continue;
    } 

    const LHCb::VeloTELL1Data::Container* data = get<LHCb::VeloTELL1Data::Container> ( it->first ); 


    /// loop over TELL1s for the given tool 
    for ( Velo::Monitoring::INoiseEvaluator::INoiseMap::const_iterator il = 
        m.begin () ; m.end () != il ; ++il ) {
      // get the counters: 
      typedef Velo::Monitoring::TELL1Noise::Counters Counters ;
      const Counters& cnts = il->second->counters() ;
      
      bool isReordered = 0;
      
      if (data->object(il->first) != NULL ){
          isReordered = data->object(il->first)->isReordered();
      }
      else{
	warning()<<"TELL1 Number  "<< il->first <<" is not accessible" <<endmsg;
        isReordered =0;
     }

      // debug() << it->first << " / " << il->first << " is Reordered = " << data->object(il->first)->isReordered() << endmsg;

      //Get sensor from detector element:  
      unsigned int tell1Number = il->first;
      const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(tell1Number);
      if(sensor == 0 ){
        error() << "Can't find a sensor associated with TELL1 " << il->first << ". Aborting..." << endmsg;
        return StatusCode::FAILURE;
      }

      // check whether the containers for this TELL1 are initialised.
      // if not, do so.
      FlagCounters& counters = m_flagCounters[tell1Number];
      if (counters.empty()) {
        counters.resize(2048,std::vector<unsigned short>(4,0));
        m_noiseByTell1[tell1Number].resize(2048,0.0f);
        m_rawNoiseByTell1[tell1Number].resize(2048,0.0f);
      }

      // fill the noise cache and take care of the reordering
      std::vector<float>& noise = m_noiseByTell1[tell1Number];
      std::vector<float>& rawNoise = m_rawNoiseByTell1[tell1Number];
      nDead=0;
      nNoisy=0;
      for ( Counters::const_iterator ic = cnts.begin() ; cnts.end() != ic ; ++ic ) {

        const double index = ic - cnts.begin() ;
        unsigned int channel = static_cast<unsigned int> (index);	

        if(isReordered){
          channel=sensor->StripToChipChannel(static_cast<unsigned int> (index));
        }

        // fill noise cache and update flag counters
        if(lookingAtRawNoise) {
          rawNoise[channel] = static_cast<float>(ic->flagRMS());
        } else {
          noise[channel] = static_cast<float>(ic->flagRMS());
        }

      }

      // compute the trucated mean of all channels on a link.
      // we can't do this in the loop above because of the possible reordering.
      for (unsigned int i=0; i<m_phiStripCategoryMap.nCategories(); ++i) {
        linkNoiseValues[i].clear();
      }

      for(unsigned int channel=0; channel<2048; ++channel) {
        unsigned int cat=0;
        if (sensor->isPhi()) {
          cat = m_phiStripCategoryMap.categoryByChannel(channel);
        }

        float noiseValue = 0.0f;
        if(lookingAtRawNoise) {
          noiseValue = rawNoise[channel];

        } else {
          noiseValue = noise[channel];

        }

        linkNoiseValues[cat].push_back(noiseValue);
        if ( 0 == (channel+1) % 32 ) {
          unsigned int nCat = 1; 
          if(sensor->isPhi()) {
            nCat = m_phiStripCategoryMap.nCategories();
          }
          for (unsigned int i=0; i<nCat; ++i) {
            truncatedMean(linkNoiseValues[i],m_linkNoise[i][channel/32],m_linkNoiseRMS[i][channel/32]);
            linkNoiseValues[i].clear();
          }
        }
      }
      boost::format fmt1 ("RawNoise/SensorNo_%03d");
      boost::format fmt2 ("CMCNoise/SensorNo_%03d");
      boost::format fmt3 ("RawNoiseD/SensorNo_%03d");
      boost::format fmt4 ("CMCNoiseD/SensorNo_%03d");
      boost::format fmt5 ("dsig/SensorNo_%03d");

      fmt1  % sensor->sensorNumber() ;
      fmt2  % sensor->sensorNumber() ;
      fmt3  % sensor->sensorNumber() ;
      fmt4  % sensor->sensorNumber() ;
      fmt5  % sensor->sensorNumber() ;


      const std::string fmtstr1 = fmt1.str() ;
      const std::string fmtstr2 = fmt2.str() ;
      const std::string fmtstr3 = fmt3.str() ;
      const std::string fmtstr4 = fmt4.str() ;
      const std::string fmtstr5 = fmt5.str() ;


      const GaudiAlg::HistoID RawNoiseID = fmtstr1;
      const GaudiAlg::HistoID CMCNoiseID = fmtstr2;
      const GaudiAlg::HistoID RawNoiseDID = fmtstr3;
      const GaudiAlg::HistoID CMCNoiseDID = fmtstr4;
      const GaudiAlg::HistoID dsigID = fmtstr5;


      AIDA::IHistogram1D* histoRawNoise = book(RawNoiseID,"RawNoise",-0.5,2047.5,2048);
      AIDA::IHistogram1D* histoCMCNoise = book(CMCNoiseID,"CMCNoise",-0.5,2047.5,2048);
      AIDA::IHistogram1D* histoRawNoiseD = book(RawNoiseDID,"RawNoiseD",-0.5,2047.5,2048);
      AIDA::IHistogram1D* histoCMCNoiseD = book(CMCNoiseDID,"CMCNoiseD",-0.5,2047.5,2048);
      AIDA::IHistogram1D* histodsig = book(dsigID,"dsig",-0.5,2047.5,2048);   

      // look at all channels and check whether they are bad
      for(unsigned int channel=0; channel<2048; ++channel) {

        if (m_ignoreFirstChannelOnLink && (0 == channel % 32)) continue;

        unsigned int cat=0;
        if (sensor->isPhi()) {
          cat = m_phiStripCategoryMap.categoryByChannel(channel);
        }

        float linkNoise    = m_linkNoise[cat][channel/32]; 
        float linkNoiseRMS = m_linkNoiseRMS[cat][channel/32]; 

        float noiseValue = 0.0f;
        float dnoise = 0.0f;

        if(linkNoise){
          if(lookingAtRawNoise) {         
            noiseValue = rawNoise[channel];   

            dnoise = noiseValue - linkNoise;      
            histoRawNoiseD ->fill((channel),(dnoise/linkNoise));
            histoRawNoise  ->fill((channel),noiseValue);

            if(sensor->isPhi()==0){

              if (dnoise/linkNoise > m_RNoisyCut|| noiseValue>m_rawRNoisyThresh ) ++counters[channel][2];                  
              if (-1.0*(dnoise/linkNoise) > m_RDeadCut || noiseValue<m_rawRDeadThresh) ++counters[channel][3];  
            }
            else{	

              if ((dnoise/linkNoise) > m_PhiNoisyCut || noiseValue>m_rawPhiNoisyThresh) ++counters[channel][2];
              if (-1*(dnoise/linkNoise) > m_PhiDeadCut ||(noiseValue<m_rawRDeadThresh) ) ++counters[channel][3];	      	    
            }

          }
          else {
            noiseValue = noise[channel];
            dnoise = noiseValue - linkNoise;
            float dnoisesig = dnoise/linkNoiseRMS;

            histodsig ->fill((channel),dnoisesig);
            histoCMCNoiseD  ->fill((channel),(dnoise/linkNoise));
            histoCMCNoise ->fill((channel),noiseValue);

            if(sensor->isPhi()==0){
              if (dnoisesig > m_RNoisySigThresh && fabs(dnoise) > m_RNoisyCMCThresh) ++counters[channel][0];                 
              if (-1.0*dnoisesig > m_RDeadSigThresh && fabs(dnoise) > m_RDeadCMCThresh) ++counters[channel][1];                 
            }
            else{

              if (dnoisesig > m_PhiNoisySigThresh && fabs(dnoise) > m_PhiNoisyCMCThresh) ++counters[channel][0];                 
              if (-1.0*dnoisesig > m_PhiDeadSigThresh && fabs(dnoise) > m_PhiDeadCMCThresh) ++counters[channel][1];
            }
            //	    float dnoisesig = dnoise/linkNoiseRMS;
          }
        }	  
      }
    }
  }

  return StatusCode::SUCCESS ;
}


// ===========================================================================
// Retrieve the tools
// ============================================================================
StatusCode Velo::Monitoring::BadChannelMon::retrieveTools() 
{
  // loop over all input containers and get the tools:
  for ( Locations::const_iterator il = m_locations.begin() ; 
      m_locations.end() != il ; ++il ) {

    std::string toolName = strippedContainerName( *il ) ;
    // get the evaluator of the noise:
    Velo::Monitoring::INoiseEvaluator* eval = tool<Velo::Monitoring::INoiseEvaluator>
      ( m_toolType , toolName) ;

    // debug() << "Noise evaluator tool name is '" << toolName << "'." << endmsg;

    if(eval!=0)
      m_noises[ *il ] = eval;
  }

  return StatusCode::SUCCESS;
}


// ============================================================================
// initialize the algorithm 
// ============================================================================
StatusCode Velo::Monitoring::BadChannelMon::initialize() 
{
  StatusCode sc=GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm  
  if ( propsPrint() ) printProps();
  debug() << "==> Initialize" << endmsg;
  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );  
  setHistoTopDir( "Vetra/" );
  retrieveTools();

  // user info
#ifndef WIN32
  m_login = std::string(getlogin());
#else
  m_login = std::string("WIN32USER");
#endif
  
  // host info
  const int buflen = 256;
  char buffer[buflen];
  gethostname(buffer,buflen);
  buffer[buflen-1] = '\0'; 
  m_host = std::string(buffer);

#ifndef WIN32
  time_t now = time(0);
  m_timeTag = std::string(ctime(&now));
  char buf[16];
  strftime(buf,16,"%y%m%d_%H%M%S",localtime(&now));
  m_fileTimeStamp = std::string(buf);
#else
  m_fileTimeStamp = "NOTIMEWIN32";
#endif

  n_totDead=0;
  n_totNoisy=0;

  return StatusCode::SUCCESS;
}

// ============================================================================
// finalize the algorithm 
// ============================================================================
StatusCode Velo::Monitoring::BadChannelMon::finalize() 
{
  StatusCode sc;
  if(m_recordTextFiles){
    StatusCode sc = writeResultsToFiles();
    if ( sc.isFailure() ) {
      error() << "Unable to write results to files." << endmsg;
      return sc;  
    }
  }

  sc = writeSummary();

  if( sc.isFailure()){
    error() << "Unable to write summary to files." << endmsg;
    return sc;  
  }

  sc = makeHistos();
  // clear the container of tools
  m_noises.clear() ;
  m_event = 0     ;
  // finalize the base class 
  return GaudiHistoAlg::finalize() ;
}

// ============================================================================
// Execute the algorithm 
// ============================================================================
StatusCode Velo::Monitoring::BadChannelMon::execute() 
{  

  //Get pedestal convergence limit from the emulator process info. This can't be done
  //from initialize() so we use the m_isInit flag to only run it the first event.

  if(m_isInit==0){

    VeloProcessInfos* procInfos;  
    if(!exist<VeloProcessInfos>(VeloProcessInfoLocation::Default)){      
      warning() << " ==> No init info available. Assuming pedestal convergence at 5000 events." << endmsg;
      m_convergenceLimit=5000;
    }else{
      procInfos=get<VeloProcessInfos>(VeloProcessInfoLocation::Default);
      VeloProcessInfos::const_iterator procIt=procInfos->begin();
      if((*procIt)->convLimit()==0){
        debug()<< "Convergence Limit not set!" <<endmsg;
        m_convergenceLimit=(*procIt)->convLimit();
      }else{
        m_convergenceLimit=(*procIt)->convLimit();
      }    
      debug() << "Convergence limit set to " << m_convergenceLimit << endmsg;

    }

    m_isInit=1;

  }

  StatusCode sc;


  if ( m_event>=m_convergenceLimit
      && 0  ==  (m_event-m_convergenceLimit)  % m_check ) { 
    flagBadChannels(); 
    ++m_nChecks;

  } 

  ++m_event;

  return StatusCode::SUCCESS ;
} 


void Velo::Monitoring::BadChannelMon::writeFileHeader(FILE* outputFile)
{
  fprintf(outputFile,"# This file was automatically generated by\n");
  fprintf(outputFile,"# %s\n",m_rcsId);
  fprintf(outputFile,"# running as user %s on %s\n",m_login.c_str(),m_host.c_str());
  fprintf(outputFile,"# %s", m_timeTag.c_str());
  fprintf(outputFile,"#\n");
  fprintf(outputFile,"# Configuration:\n");
  fprintf(outputFile,"#   Raw High noise cut R, Phi           : %+06.2f, %+06.2f\n", m_rawRNoisyThresh,m_rawPhiNoisyThresh);
  fprintf(outputFile,"#   Raw low noise cut R, Phi            : %+06.2f, %+06.2f\n", m_rawRDeadThresh,m_rawPhiDeadThresh);
  fprintf(outputFile,"#   Raw Low noise Diff R, Phi           : %+06.2f,%+06.2f\n", m_RDeadCut, m_PhiDeadCut);
  fprintf(outputFile,"#   Raw High noise Diff R,Phi          : %+06.2f, %+06.2f\n", m_RNoisyCut,m_PhiNoisyCut);
  fprintf(outputFile,"#   CMC Low noise cut R,Phi            : %+06.2f, %+06.2f\n", m_RDeadCMCThresh,m_PhiDeadCMCThresh);
  fprintf(outputFile,"#   CMC High noise cut R, Phi           : %+06.2f, %+06.2f\n", m_RNoisyCMCThresh,m_PhiNoisyCMCThresh);
  fprintf(outputFile,"#   Dead Significance cut   R, Phi      : %+06.2f, %+06.2f\n", m_RDeadSigThresh,m_PhiDeadSigThresh);
  fprintf(outputFile,"#   Noisy Significance cut   R, Phi      : %+06.2f, %+06.2f\n", m_RNoisySigThresh,m_PhiNoisySigThresh);


  fprintf(outputFile,"#   Ingore first channel on link: %s\n", (m_ignoreFirstChannelOnLink ? "TRUE" : "FALSE"));

  fprintf(outputFile,"# Number Dead: %i \n", nDead);
  fprintf(outputFile,"# Number Noisy: %i \n", nNoisy);
  fprintf(outputFile,"#\n");
  fprintf(outputFile,"# Flags:\n");
  fprintf(outputFile,"#    0 : OK\n");
  fprintf(outputFile,"#   -1 : Found to be bad due to high CMC noise\n");
  fprintf(outputFile,"#   -2 : Found to be bad due to very low CMC noise\n");
  fprintf(outputFile,"#   -3 : Found to be bad due to high raw noise\n");
  fprintf(outputFile,"#   -4 : Found to be bad due to very low raw noise\n");
  fprintf(outputFile,"#\n");
  fprintf(outputFile,"# channel chip chipchannel   cmc noise   raw noise   flag\n");
}


void Velo::Monitoring::BadChannelMon::createFileName(std::string& fileName, const DeVeloSensor* sensor, unsigned int tell1Number)
{
  std::string sensorTag("UNKNOWN");
  if (!sensor) {
    error() << "Invalid sensor pointer for TELL1 Id " <<  tell1Number << "." << endmsg;
  } else {
    sensorTag = (sensor->isR() ? "R" : (sensor->isPhi() ? "P" : "PU") );
  }

  boost::format buffer("Noise_Bad_Channels_TELL1_%03d");
  buffer % tell1Number;

#ifndef WIN32
  const char* DirName=m_fileTimeStamp.c_str();
  mkdir(DirName ,0755);
  fileName = m_fileTimeStamp+"/";
#else
  fileName = "";
#endif

  if (!m_outputFileNamePrefix.empty()) {
    fileName = fileName + m_outputFileNamePrefix + "_";
  }
  fileName = fileName + m_fileTimeStamp + "_" + buffer.str() + "_" + sensorTag + ".dat";
}



///////////
// Writing Files
//////////

StatusCode Velo::Monitoring::BadChannelMon::writeResultsToFiles()
{
  TELL1ToFlagCounterMap::const_iterator iFlags;

  for (iFlags = m_flagCounters.begin(); iFlags != m_flagCounters.end(); ++iFlags) {

    unsigned int tell1Number = iFlags->first;
    const FlagCounters& counters = iFlags->second;
    const DeVeloSensor* sensor = m_veloDet->sensorByTell1Id(tell1Number);
    
    if (!sensor) {
      error() << "Can not retrieve sensor pointer for TELL1 Id " <<  tell1Number << "." << endmsg;
      return StatusCode::FAILURE;
    } 

    std::string fileName;
    createFileName(fileName, sensor, tell1Number);

    FILE* outputFile = fopen(fileName.c_str(),"w");

    if (!outputFile) {
      error() << "Could not open file '" << fileName << "' for writing." << endmsg;
      return StatusCode::FAILURE;
    }
    writeFileHeader(outputFile);

    const std::vector<float>& noise = m_noiseByTell1[tell1Number];
    const std::vector<float>& rawNoise = m_rawNoiseByTell1[tell1Number];

    for (unsigned int channel=0; channel<2048; ++channel) {

      unsigned int chip = channel/128;
      unsigned int chipChannel = channel - chip*128;

      // find the flag with the maximum count
      unsigned int iMaxCount = 0;
      const std::vector<unsigned short>& counter = counters[channel];
      for (unsigned int i=1; i<4; ++i) {
        if (counter[i] > counter[iMaxCount]) {
          iMaxCount = i;
        }
      }

      // now see whether it passes the counter cut and set the bad channel flag accordingly
      int badChannelFlag = 0;
      if ( counter[iMaxCount]/static_cast<float>(m_nChecks) > m_counterCut ) {
        badChannelFlag = -static_cast<int>(iMaxCount+1);
      }
      if(iMaxCount==2){
        ++nDead;
        ++nDeadPerTell1;
      }

      if(iMaxCount==1){ 
        ++nNoisy;
        ++nNoisyPerTell1;}
//        unsigned int cat=0;
//        if (sensor->isPhi()) {
//          cat = m_phiStripCategoryMap.categoryByChannel(channel);
//        }

        fprintf(outputFile,"   %04d   %02d     %03d      %06.2f      %06.2f      %2i\n",channel,chip,chipChannel,noise[channel],rawNoise[channel],badChannelFlag);
    }

    fclose(outputFile);

    n_totDead = n_totDead+nDeadPerTell1;
    n_totNoisy = n_totNoisy+nNoisyPerTell1;
  }

  return StatusCode::SUCCESS;
}

StatusCode Velo::Monitoring::BadChannelMon::writeSummary()
{
  std::string SummaryName;
  SummaryName="Summary2.dat";


  FILE* outputSummary = fopen(SummaryName.c_str(),"a");
  if (!outputSummary) {
    error() << "Could not open file '" << SummaryName << "' for writing." << endmsg;
    return StatusCode::FAILURE;
  }

  fprintf(outputSummary,"%i %i",n_totNoisy,n_totDead);
  fclose(outputSummary);

  return StatusCode::SUCCESS;
}

//=================
//make histos
//=================

StatusCode Velo::Monitoring::BadChannelMon::makeHistos()
{
  TELL1ToFlagCounterMap::const_iterator iFlags;

  for (iFlags = m_flagCounters.begin(); iFlags != m_flagCounters.end(); ++iFlags) {
    unsigned int tell1Number = iFlags->first;
    const FlagCounters& counters = iFlags->second;

    const DeVeloSensor* sensor = m_veloDet->sensorByTell1Id(tell1Number);
    if (!sensor) {
      error() << "Can not retrieve sensor pointer for TELL1 Id " <<  tell1Number << "." << endmsg;
      return StatusCode::FAILURE;
    } 

    // construct the proper IDs for the histograms:
    boost::format fmt ( "BadChannels/SensorNo_%03d" ) ;
    boost::format fmt1 ( "NoisyChannels/SensorNo_%03d" ) ;

    fmt  % sensor->sensorNumber() ;
    fmt1 % sensor->sensorNumber() ;

    const std::string fmtstr = fmt.str() ;
    const std::string fmtstr1 = fmt1.str() ;

    const GaudiAlg::HistoID BadStripID = fmtstr;
    const GaudiAlg::HistoID NoisyStripID = fmtstr1;

    AIDA::IHistogram1D* histoBadStrip = book(BadStripID,"Bad strips",-0.5,2047.5,2048);
    AIDA::IHistogram1D* histoNoisyStrip = book(NoisyStripID,"Noisy strips",-0.5,2047.5,2048);

    for (unsigned int channel=0; channel<2048; ++channel) {
      unsigned int iMaxCount = 0;
      //int badChannelFlag = 0;
      const std::vector<unsigned short>& counter = counters[channel];

      for (unsigned int i=0; i<4; ++i) {
        if (counter[i] > counter[iMaxCount]) {
          iMaxCount = i;
        }
      }
      if ( counter[iMaxCount]/static_cast<float>(m_nChecks) > m_counterCut ) {
        //badChannelFlag = -static_cast<int>(iMaxCount+1);
        histoBadStrip -> fill((channel),1);
	if ((iMaxCount == 2) ||(iMaxCount == 0) ){
	  histoNoisyStrip->fill((channel),1);

}
      }

    }
  }
      
  return StatusCode::SUCCESS;
}


void Velo::Monitoring::BadChannelMon::truncatedMean(std::vector<float>& values, float& mean, float& rms) 
{
  std::sort(values.begin(),values.end());

  // we ignore the lowest and highest 10% of the values, so we 
  // first have to determine what 10% corresponds to in number
  // entries.
  size_t offset = values.size()/10;

  // if the offset is 0 and we have more than 4 entries we still
  // omit the highest and lowest
  if (!offset && values.size()>4) {
    offset = 1;
  }

  // compute sum
  std::vector<float>::iterator start = values.begin(); std::advance(start,offset);
  std::vector<float>::iterator end   = values.end(); std::advance(end,-offset);

  float sum   = std::accumulate(start,end,0.0f);   
  float sumSq = std::accumulate(start,end,0.0f,Squares<float,float>());   

  unsigned int n = std::distance(start,end);

  mean = sum/n;
  rms  = sqrt(sumSq/n - mean*mean); 


  return;
}



DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring,BadChannelMon)


