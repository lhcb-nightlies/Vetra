// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/AlgFactory.h" 
// Event
#include "VeloEvent/EvtInfo.h"
#include "Event/VeloODINBank.h"
#include "VeloEvent/VeloTELL1Data.h"
#include "GaudiKernel/IEventTimeDecoder.h"            // Interface for odin
#include "Event/ODIN.h"
//#include "Event/OdinTimeDecoder.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
// local
#include "CommonFunctions.h"
//velo
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"
// from TELL1 Kernel:
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "Tell1Kernel/VeloDecodeConf.h" 
// Boost 
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
//-----------------------------------------------------------------------------
// Class : newADCDelayScan
//
// 2008-02-07 : Aras Papadelis. Rewrite of FullDataMonitorPhaseScan
// 2009-04-17 : Kazu Akiba. Clean-up, re-structure.
//              This code uses a special common mode subtraction due to the
//              nature of the scan: there is no trivial way to beforehand make sure
//              there will be no headers in the middle of the channel data section.
//-----------------------------------------------------------------------------

using namespace LHCb;
using namespace VeloTELL1;
namespace Velo
{
  namespace Monitoring
  {
	class  newADCDelayScan : public GaudiHistoAlg {
	  public:
		/// Standard constructor
		newADCDelayScan( const std::string& name, ISvcLocator* pSvcLocator );
		virtual ~newADCDelayScan( ); ///< Destructor
		virtual StatusCode initialize() override;    ///< Algorithm initialization
		virtual StatusCode execute   () override;    ///< Algorithm execution
		virtual StatusCode finalize  () override;    ///< Algorithm finalization
		int                getADCPhase();
		StatusCode         printOdinInfo( LHCb::ODIN  *odin);
		std::vector<double> SubtractCommonMode( std::vector<int> headers, std::vector<int> data, double cut, int howManyPasses );
		int m_method;
	  private  :
		unsigned long int m_StepSize;
		unsigned long int m_NSteps;
		unsigned long int m_stepId;
		unsigned long int m_runNumber;
		unsigned long long m_gpsTime;
		unsigned int m_newphasedeltaT;
		unsigned long long m_l0EventID;
		//std::map<unsigned long int, unsigned long int> m_l0EventCounter;
		long  m_l0EventCounter;
		VeloTELL1Datas* m_tell1DataContainer;
		VeloTELL1Datas* m_headerContainer;
		EvtInfos*       m_evtInfos;
		IEventTimeDecoder* m_timeDecoder; ///< Pointer to tool to decode ODIN bank
		unsigned long int  m_evtNumber;
		int m_refresh;

		std::vector<int> m_chosenTell1s;
		int    m_plot2d;
		bool   m_inverted;
		bool   m_runWithOdin;
		bool   m_plotMeanAndRMS;
		int    m_plotcms;
		int    m_monitor;
		int    m_plotrawprofile;
		int    m_plotprofile;
		double m_commonModeCut;
		unsigned int    m_nEventsToSkip;
		bool   m_printFullOdinInfo;
		bool   m_printOdinInfo;
		int    m_printFreq;
		double m_nStat;
		bool   m_evenodd;
        //t    m_l0EventNumber;
        // unsigned long int l0EventNumber(){      }
		//int    l0EventCounter(unsigned long int Tell1){ return   m_l0EventCounter; };
		int    l0EventCounter(){ return   m_l0EventCounter; };
		void   l0EventCounter(long id){  m_l0EventCounter = id ;} ;
		bool   OddEvent(){ return ( m_l0EventCounter%2==1 ?   true : false );}
		bool   EvenEvent(){ return ( m_l0EventCounter %2==0 ? true : false );}
		int    Parity(){ return ( m_l0EventCounter %2==0 ? 1 : -1 );}
		unsigned long long EventNumber() { return m_evtNumber; }
		//Returns a vector with the link ADCs.
		std::vector<int> getLink(VeloTELL1::ALinkPair begEnd) {   
		  std::vector<int>::const_iterator iT; //iterator to vector of unsigned int.
		  std::vector<int> linkVector;
		  linkVector.clear();
		  //Loop over ADC values in link
		  for(iT=begEnd.first; iT!=begEnd.second; ++iT){    
			linkVector.push_back(*iT); //Push back ADC value to vector
		  }
		  return linkVector;
		}
	};
  }
}



//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::newADCDelayScan::newADCDelayScan( const std::string& name,
	ISvcLocator* pSvcLocator)
:  GaudiHistoAlg( name , pSvcLocator )
  , m_stepId(0)
  ,  m_l0EventCounter (0)
,  m_evtNumber ( 0 )
{
  declareProperty( "StepSize" ,                 m_StepSize =              100);
  declareProperty( "NSteps" ,                   m_NSteps =                 25);
  declareProperty( "PhaseChangeMethod",         m_method =                  3);
  declareProperty( "NewPhaseDeltaTcut",         m_newphasedeltaT =       1000);
  declareProperty( "TELL1s",                    m_chosenTell1s               );
  declareProperty( "InvertedDelaySequence",     m_inverted = false           );
  declareProperty( "Plot2D",                    m_plot2d = 0                 );
  declareProperty( "PlotRawProfile",            m_plotrawprofile = 0         );
  declareProperty( "PlotProfile",               m_plotprofile = 1            );
  declareProperty( "PlotMeanAndRMS",            m_plotMeanAndRMS = true      );
  declareProperty( "PlotCMS",                   m_plotcms = 1                );
  declareProperty( "Monitor",                   m_monitor = 0                );
  declareProperty( "MonitorPrintFreq",          m_printFreq = 100            );
  declareProperty( "CommonModeCut",             m_commonModeCut =          10);
  declareProperty( "NEventsToSkip",             m_nEventsToSkip =           0);
  declareProperty( "RunWithOdin",             m_runWithOdin =           true);
  declareProperty( "PrintFullOdinInfo",       m_printFullOdinInfo =   false);
  declareProperty( "PrintOdinInfo",           m_printOdinInfo =       false);
}
//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::newADCDelayScan::~newADCDelayScan() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::newADCDelayScan::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  debug() << "==> Initialize" << endmsg;
  m_timeDecoder = tool<IEventTimeDecoder>( "OdinTimeDecoder" );
  setHistoTopDir( "Velo/" );
  m_nStat= m_StepSize-m_nEventsToSkip;
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::newADCDelayScan::finalize() {
  debug() << "==> Finalize" << endmsg;
  return GaudiHistoAlg::finalize();  // must be called after all other actions
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::newADCDelayScan::execute() {
  m_evtNumber++;
  int iADCPhase=-1;
  debug() << "==> Execute" << endmsg;
  m_nStat= m_StepSize-m_nEventsToSkip;
  //getting banks and all the good stuff.
  LHCb::ODIN* odin=0;
  if (exist<LHCb::ODIN>((LHCb::ODINLocation::Default))){
	odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
  }
  else if(m_runWithOdin){
	m_timeDecoder->getTime(); // in case one wants to get other kinds of info from odin.
	odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
	//odin = OdinTimeDecoder::getODIN();
  }
  else if (m_runWithOdin != true) {
 	debug() << "ODIN bank does not exist! changing method to  3" << endmsg;
    m_method = 3;
  }
  if(!exist<EvtInfos>( EvtInfoLocation::Default )){
	debug()<< " ==> There is no Event info in the data. QUITTING. " <<endmsg;
	return ( StatusCode::SUCCESS );
  }else{
	m_evtInfos = get<EvtInfos>(EvtInfoLocation::Default);
	EvtInfo* anInfo; 
	EvtInfos::const_iterator infoIt;
	int TELL1_number= -10;
	for(infoIt=m_evtInfos->begin();infoIt!=m_evtInfos->end();++infoIt){
	  anInfo=(*infoIt);
	  TELL1_number=anInfo->key();
	  if(ExcludeTELL1(m_chosenTell1s,TELL1_number)) continue;
      l0EventCounter(anInfo->l0EventID(0));
      debug() << " l0EventCounter =  " << l0EventCounter() << endmsg;
    }
  }
  if(odin && m_method == 0){
	iADCPhase = odin->calibrationStep() -1; // -1 because the first step we skip...
	if (m_stepId == 0) {
	  plot(iADCPhase,"hADCPhase","ADC phase",-0.5,(m_NSteps-0.5),(m_NSteps+2),1.);
	} else {
	  plot(iADCPhase,"hADCPhase","ADC phase",-1.5,(m_NSteps+1.-0.5),(m_NSteps+2),1.);
	}
  } else {
	debug() << "ODIN bank does not exist! changing method to  3" << endmsg;
    m_method = 3;
	iADCPhase = getADCPhase();
  }
  if(m_printOdinInfo){
    if(odin!=0){
      if((m_evtNumber % m_printFreq) == 1){
        printOdinInfo(odin); 
	    info() << " this is the step: " <<  odin->calibrationStep() << " iADCPhase ==  "<< iADCPhase <<endmsg; 
      }
    }  
  }
  if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::ADCs  )){ //put in location here.
	info()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::ADCs  <<endmsg;
	return StatusCode::FAILURE;

  }else{
	m_tell1DataContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::ADCs );
  }
  VeloTELL1Datas::const_iterator headerIt;
  if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers )){ //put in location here.
	info()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::Headers  <<endmsg;
	return StatusCode::FAILURE;
  }else{
	m_headerContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers );
	headerIt= m_headerContainer->begin();
  }
  // End of TES play.

  if(m_inverted) iADCPhase = m_NSteps - 1 - iADCPhase; // if the delays are taken in a reversed order, invert them back in software.
  if (m_evtNumber %100 == 0) info() << " this is iADCPhase == "<< iADCPhase <<endmsg; 
  //if(m_evtNumber%100 == 0 && odin) info() << " this is the step: (from odin) " 
  //                                        <<  odin->calibrationStep() << "iADCPhase == "<< iADCPhase <<endmsg; 
  //else if(m_evtNumber%100 == 0 && !(odin)) info() << " this is the step: " << "iADCPhase == "<< iADCPhase <<endmsg; 
  // skip events per each step.
  if(m_evtNumber%m_StepSize < m_nEventsToSkip) return StatusCode::SUCCESS;
  // start of plot making
  VeloTELL1Datas::const_iterator sensIt;
  for(sensIt=m_tell1DataContainer->begin();sensIt!=m_tell1DataContainer->end();++sensIt){
	if(sensIt!=m_tell1DataContainer->begin()) ++headerIt;
	VeloTELL1Data* dataOneTELL1=(*sensIt);
	VeloTELL1Data* headerOneTELL1 = (*headerIt);
	int TELL1_number=dataOneTELL1->key();
	if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue; // get rid of unecessary tell1s.
	//EvtInfo* anInfo=m_evtInfos->object(TELL1_number);
	//l0EventCounter(anInfo->l0EventID(0));
	std::string polarity = (EvenEvent()==true ) ? "Even" : "Odd";
	for(int link = 0; link<NumberOfALinks; ++link){
	  //Set up directory structure and histogram names:    
	  boost::format dir ( "TELL1_%03d/");
	  dir % TELL1_number; 
	  boost::format name ( "hADCPhaseScan%s_Link_%d");
	  name % polarity % link;           
	  boost::format subtractedcommonmode ( "TELL1_%03d/subtractedCommonmode%s_%d");
	  subtractedcommonmode % TELL1_number% polarity %link;           
	  boost::format title ( "ADCPhaseScan%s for , Link %d" );
	  title % polarity  % link;
	  int nChannelsPlusHeaders=36;
	  int nxbins  = m_NSteps*nChannelsPlusHeaders;
	  int nybins  = 256; //256
	  std::vector<int> linkADCs = getLink( (*dataOneTELL1)[link] );
	  std::vector<int> linkHeaders = getLink( (*headerOneTELL1)[link] );
	  std::vector<int>::iterator it;
	  std::vector<double>::iterator itf;
	  // subtract common mode for a better determination of the delay:
	  std::vector<double> linkCMSub;
	  std::vector<double> HeadersCMSub;
	  double average = 0;
	  linkCMSub.resize(32);
	  HeadersCMSub.resize(4);
	  int count =0, chanelsinthesum =0;
	  for(it=linkADCs.begin();it!=linkADCs.end() ; ++it, count++){
		if((count >= 3 ) // to make sure to not get the header in the CMS...
		  ){
		  average += (double)(*it);
		  chanelsinthesum++;
		}
	  }
	  average /= (double)(chanelsinthesum);
	  if(m_plotcms && m_monitor) plot1D(average, subtractedcommonmode.str(),subtractedcommonmode.str(), 512-128.5,512+127.5, nybins);
	  int chanWithoutHits = 0;
	  double averageWithoutHits = 0;
	  count = 0;
      // first pass
	  for(it=linkADCs.begin();it!=linkADCs.end() ; it++, count++){
		linkCMSub[count] = (double)(*it) - average;
		if(count < 4) HeadersCMSub[count] = ((double)(linkHeaders[count]) - average);
	  }
      // second pass
	  count = 0; 
	  for(itf=linkCMSub.begin();itf!=linkCMSub.end() ; ++itf, count++){
		if(count >= 3){ 
		  if(fabs(*itf) < m_commonModeCut )  {
			averageWithoutHits += *itf;
			chanWithoutHits++;
		  }
		}
	  }
	  if(chanWithoutHits >0) averageWithoutHits /= (double)(chanWithoutHits);
	  else averageWithoutHits = 0;
	  count = 0;  
	  for(itf=linkCMSub.begin();itf!=linkCMSub.end() ; ++itf, count++){
		linkCMSub[count] = *itf - averageWithoutHits;
		if(count < 4) HeadersCMSub[count] = (HeadersCMSub[count] - averageWithoutHits);
	  }
      // CMS done
	  count = 0;
	  for(itf=HeadersCMSub.begin(), it=linkHeaders.begin();itf!=HeadersCMSub.end() ; ++itf, ++it, count++){
		double jchan = (double)(count);
		double xval = (jchan*m_NSteps + iADCPhase)/(double)(m_NSteps) - 4.5+0.001; // 0.001 to avoid funny bin edge effects.
		profile1D(xval, iADCPhase, dir.str()+"ADCSetting","ADC Sampling phase Vs Bin number",-4.5,31.5,nxbins);
		//if((*it>512-3*128) && (*it<512+3*127)) if(m_plotcms) profile1D(xval,*itf , dir.str()+"cms"+name.str(),title.str()+ " cms",-4.5,31.5,nxbins);
		if(m_plotcms&&m_plotprofile) profile1D(xval,*itf , dir.str()+"cms"+name.str(),title.str()+ " cms",-4.5,31.5,nxbins);
		if(m_plotrawprofile)         profile1D(xval,*it , dir.str()+name.str(),title.str(),-4.5,31.5,nxbins);
		if(m_plot2d)                 plot2D(xval,*it, dir.str()+"raw"+name.str()+"2d" ,"raw"+title.str(),-4.5,31.5,-1,1024,nxbins,2*nybins);
		if(m_plotcms&& m_plot2d)     plot2D(xval,*itf, dir.str()+name.str()+"2d",title.str(),-4.5,31.5,-128.5,+127.5,nxbins,nybins);
	  }   
	  count = 0;
	  for(itf=linkCMSub.begin(), it=linkADCs.begin(); itf!=linkCMSub.end() ; ++itf, it++, count++){
		double jchan = (double)(count);
		double xval = (jchan*m_NSteps + iADCPhase)/(double)(m_NSteps) - 0.5+0.001;
		profile1D(xval, iADCPhase, dir.str()+"ADCSetting","ADC Sampling phase Vs Bin number",-4.5,31.5,nxbins);
		if(m_plotcms&&m_plotprofile) profile1D(xval,*itf , dir.str()+"cms"+name.str(),title.str()+ " cms",-4.5,31.5,nxbins);
		if(m_plotrawprofile)         profile1D(xval,*it, dir.str()+name.str(),title.str(),-4.5,31.5,nxbins);
		if(m_plot2d)                 plot2D(xval,*it, dir.str()+"raw"+name.str()+"2d","raw"+title.str(),-4.5,31.5,-1,1024,nxbins,2*nybins);
		if(m_plotcms&&m_plot2d)      plot2D(xval,*itf, dir.str()+name.str()+"2d",title.str(),-4.5,31.5,-128.5,+127.5,nxbins,nybins);
	  }
	}
  }
  return StatusCode::SUCCESS;
}
int Velo::Monitoring::newADCDelayScan::getADCPhase() 
{
  //this method works for separating phases with the run number, a la assembly lab
  info() <<  " getting smapling phase... method == "<< m_method << endmsg;
  if(m_method==1){
	if (m_stepId == 0) {
	  return ((int) (m_evtNumber/m_StepSize));
	} else {
	  if (m_runNumber == 0) {
		return (-1);
	  } else if (m_runNumber == 1) {
		return (m_NSteps);
	  } else {
		return ((int) (m_runNumber%100000));  
	  } 
	}
  }
  if(m_method==2){
	static int phase=0;
	static unsigned long long timeprevious=0;
	static unsigned long long deltaT=0;
	deltaT = m_gpsTime - timeprevious;
	plot1D(deltaT,"deltaT","deltaT",0.,10.,10);
	debug() << "Time difference: " <<  m_gpsTime << " - " << timeprevious << " = " << deltaT << endmsg;
	timeprevious=m_gpsTime;
	if(deltaT>m_newphasedeltaT){
	  phase++;
	  info() << "Phase change! New phase is " << phase << endmsg;
	}
	return phase; 
  }
  if(m_method==3){
	return ((int) (EventNumber() / m_StepSize)); 	
  }
  return 0;
}

StatusCode Velo::Monitoring::newADCDelayScan::printOdinInfo(LHCb::ODIN *odin){
  if(odin==0){
    info() << " What? no odin decoded? Outrageous..." << endmsg;
    return StatusCode::FAILURE;
  }
  info() << " >>> run number: " <<  odin->runNumber() << endmsg; 
  info() << " >>> event number: " <<  odin->eventNumber() << endmsg; 
  info() << " >>> calibration step: " <<  odin->calibrationStep() << endmsg; 
  info() << " >>> event type: " <<  odin->eventType() << endmsg; 
  info() << " >>> orbit number: " <<  odin->orbitNumber() << endmsg;
  if(m_printFullOdinInfo){  
	info() << " >>> gps time: " <<  odin->gpsTime() << endmsg; 
	info() << " >>> detector status: " <<  odin->detectorStatus() << endmsg; 
	info() << " >>> error bits: " <<  odin->errorBits() << endmsg; 
	info() << " >>> bunch id: " <<  odin->bunchId() << endmsg; 
	info() << " >>> trigger type: " <<  odin->triggerType() << endmsg; 
	info() << " >>> readout type: " <<  odin->readoutType() << endmsg; 
	info() << " >>> force bit: " <<  odin->forceBit() << endmsg; 
	info() << " >>> bunch crossing type: " <<  odin->bunchCrossingType() << endmsg; 
	info() << " >>> bunch current: " <<  odin->bunchCurrent() << endmsg; 
	info() << " >>> version: " <<  odin->version() << endmsg; 
	info() << " >>> trigger configuration key: " <<  odin->triggerConfigurationKey() << endmsg; 
  }
  return StatusCode::SUCCESS;
}



// Declaration of the Algorithm Factory

DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, newADCDelayScan )



