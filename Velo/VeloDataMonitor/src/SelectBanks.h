// $Id: $
#ifndef CHECKBANKS_H 
#define CHECKBANKS_H 1

// Include files
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"


/** @class CheckBanks CheckBanks.h
 *  Algorithm to check if there are both NZS and error banks for at least 1 tell1
 *
 *  @author Roel Aaij
 *  @date   2011-01-07
 */

namespace Velo
{ 
  namespace Monitoring
  {
    class SelectBanks : public GaudiAlgorithm {
    public: 
      /// Standard constructor
      SelectBanks( const std::string& name, ISvcLocator* pSvcLocator );
      
      virtual ~SelectBanks( ); ///< Destructor
      
      virtual StatusCode initialize() override;    ///< Algorithm initialization
      virtual StatusCode execute   () override;    ///< Algorithm execution
      virtual StatusCode finalize  () override;    ///< Algorithm finalization
      
    protected:
      
    private:
      
      // propeties
      std::string m_rawEventLocation;
      
    };
  }
}
#endif // SELECTBANKS_H
