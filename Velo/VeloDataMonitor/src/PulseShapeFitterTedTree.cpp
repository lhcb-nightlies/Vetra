// $Id$
// Include files
// -------------
#include <iostream>
#include <fstream>
// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
// from Tell1Kernel
#include "Tell1Kernel/VeloDecodeConf.h" 
#include "Event/ODIN.h"
#include "Event/VeloODINBank.h"
// local
#include "PulseShapeFitterTedTree.h"
#include "CommonFunctions.h"
// from Boost
#include <boost/assign/list_of.hpp>
#include "boost/lexical_cast.hpp"
// from Gaudi
#include "AIDA/IHistogram2D.h"
// local
// Kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "VeloDet/DeVeloSensor.h"
#include "DetDesc/Condition.h"
#include "Event/RawEvent.h"

using namespace VeloTELL1;
using namespace LHCb;
using namespace std;
using namespace Gaudi;
//-----------------------------------------------------------------------------
// Implementation file for class : PulseShapeFitterTedTree
// Author: Veerle Heijne
// This code should fit pulse shapes to
// Non zero suppressed data and find the peak time.
// It stores an ntuple with the selected data
//-----------------------------------------------------------------------------
// Declaration of the Algorithm Factory
namespace Velo {
  DECLARE_ALGORITHM_FACTORY( PulseShapeFitterTedTree )
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  Velo::PulseShapeFitterTedTree::PulseShapeFitterTedTree( const std::string& name, ISvcLocator* pSvcLocator)
  //: Velo::VeloMonitorBase ( name , pSvcLocator )
  : GaudiTupleAlg ( name , pSvcLocator )
,m_nEvents(0)
{
  std::vector<std::string> tmpLocations = 
	boost::assign::list_of("Prev7")("Prev6")("Prev5")("Prev4")("Prev3")("Prev2")("Prev1")
	("")("Next1")("Next2")("Next3")("Next4")("Next5")("Next6")("Next7");
  declareProperty( "SamplingLocations", m_samplingLocations= tmpLocations );
  //declareProperty( "NSteps", m_nSteps= 1 );
  //declareProperty( "StepSize", m_stepSize= 100 );
  declareProperty( "ChisqCut", m_chisqCut= 10 );
  declareProperty( "CMSCut", m_cmsCut= 10 );
  declareProperty( "MaxAmplitudeCut", m_amplitudeCut= 300);
  declareProperty( "OffsetCut", m_offsetCut= 10 );
  declareProperty( "IntegralCut", m_integralCut= 10 );
  declareProperty( "UseLogLikeFit", m_useLogLikeFit= true );
  declareProperty( "TimeWindow", m_timeWindow= 150 );
  declareProperty( "ExpectedPeakTime", m_expectedPeakTime= 0 );
  declareProperty( "NTimeBins", m_nTimeBins = 201 );
  declareProperty( "FixOffset", m_fixedOffset= false );
  declareProperty( "TryToClusterize", m_tryCluster= false );
  declareProperty( "TELL1s",m_chosenTell1s     );
  declareProperty( "SignalThreshold" , m_threshold=20);
  declareProperty( "TakeNeighbours", m_takeNeighbours=1 );
  declareProperty( "PlotSeparateStrips", m_plotSeparateStrips=false );
  declareProperty( "Offset" ,m_offset=0);
  declareProperty( "WidthBefore", m_width1=13.68);
  declareProperty( "WidthAfter", m_width2=26.76);
  declareProperty( "DetailedOutput" , m_detailedOutput=true);
  declareProperty( "RunWithOdin" , m_runWithOdin = true ); 
  declareProperty( "RunNumbers", m_runNumbers );
  declareProperty( "ChosenBunchCounters", m_chosenBunches );
  declareProperty( "RunWithChosenBunchCounters", m_useBCNT = true );
  declareProperty( "SkipStepZero", m_skipStepZero=true );
  declareProperty( "NEventsPerStep", m_nEventsPerStep= -1  );
  declareProperty( "OnlyStepZero", m_onlyStepZero=false );
  declareProperty( "RemoveHeaders", m_removeHeaders=false );
  declareProperty( "SelectBunchCrossing", m_selectBunchCrossing =false );

  m_runNumbers.push_back(-1);
  m_chosenTell1s.push_back(-1);
  
}
//=============================================================================
// Destructor
//=============================================================================
Velo::PulseShapeFitterTedTree::~PulseShapeFitterTedTree() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::PulseShapeFitterTedTree::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first

  printProps();
  m_timeDecoder = tool<IEventTimeDecoder>( "OdinTimeDecoder" );
  if ( sc.isFailure() ) return sc;
  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );  
  //const int strips=2048;
  int strip_tmp=0,channel_tmp=0;
  std::ifstream ifsR("/home/kakiba/pccChannel-mapping.twocol");
  std::ifstream ifsP("/home/kakiba/rccChannel-mapping.twocol");
  char dummy[100];
  ifsR.getline(dummy,100);
  for(int ichan = 0; ichan<2048; ichan++){
	ifsR>>strip_tmp>>channel_tmp;
	m_stripFromChannel[1][channel_tmp]=strip_tmp;  
	m_channelFromStrip[1][strip_tmp]=channel_tmp;  
	//debug() << " getting reordering data Phi strip " << strip_tmp <<  " -> channel " << channel_tmp<< endmsg;
  }
  ifsP.getline(dummy,100);
  for(int jchan = 0; jchan<2048; jchan++){
	ifsP>>strip_tmp>>channel_tmp;
	m_stripFromChannel[0][channel_tmp]=strip_tmp;  
	m_channelFromStrip[0][strip_tmp]=channel_tmp;                          
	//debug() << " getting reordering data R  strip " << strip_tmp <<  " -> channel " << channel_tmp<< endmsg;
  }
  ifsP.close();
  ifsR.close();

  return StatusCode::SUCCESS;
}

//=============================================================================
/// Main execution
//=============================================================================
StatusCode Velo::PulseShapeFitterTedTree::execute() {

  counter( "# events" ) += 1;
  int finebin = 0;
  std::vector<std::string>::const_iterator itL;
  //check middle sample for the tell1Data:
  const int midSampleIndex = (int)floor(m_samplingLocations.size()/2.);
  const int nSamples = m_samplingLocations.size();
  debug() << "number of samples: " << nSamples << endmsg;

  for(int isample = 0; isample < nSamples ; isample++){
    LHCb::VeloTELL1Datas *SampleData = veloTell1Data(m_samplingLocations[isample]);
    if( SampleData == 0 ) {
      debug() << "Sampledata of sample " << isample << " is empty! RETURN "<< endmsg;
      return StatusCode::SUCCESS;
    }
  }
  LHCb::VeloTELL1Datas *midSampleData = veloTell1Data(m_samplingLocations[midSampleIndex]);
  LHCb::VeloTELL1Datas::const_iterator itD;


#ifndef WIN32
  pulse = new TH1D("pulse","pulse",nSamples*50,-(nSamples/2 +0.1)*25.,(nSamples/2+0.01)*25.); //For fixed bins of 0.5 ns
#endif

  Tuple tuple = nTuple("MyTuple", "MyTuple");

  double (*fittest)(double *, double *) = NULL;
  fittest = &Velo::fitsimple;
  func = new TF1("dtfitfunc",fittest,-200.,200.,5);
  func->SetParameters(0,20,-10,m_width1,m_width2);
  func->SetParLimits(3,m_width1,m_width1);
  func->SetParLimits(4,m_width2,m_width2);
  func->FixParameter(3,m_width1);
  func->FixParameter(4,m_width2);
  func->SetParLimits(2, m_expectedPeakTime - m_timeWindow,m_expectedPeakTime + m_timeWindow);
  func->SetParLimits(0, -10,10);
  func->SetParLimits(1, 0,200);

  if(m_fixedOffset) func->FixParameter(0,0);

  char pulseshape[150];

  LHCb::ODIN* odin=0;
  if (m_runWithOdin){
	if(exist<LHCb::ODIN>((LHCb::ODINLocation::Default))) {
	  odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
	}
    else{
	  m_timeDecoder->getTime(); // in case we wants to get other info from odin.
	  odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
	}	
  }
  else m_runNumbers[0] = -1;
  double delay=0;
  
  m_currentCalStep=odin->calibrationStep();
  double step =  m_currentCalStep;

  int bunchCounter =  odin->bunchId();
  info() << " >>> bunch id: " <<  odin->bunchId() << endmsg;
  std::vector<int>::iterator ibunch;
  int isitright=0;
  if(m_chosenBunches.size() !=0){
	for(ibunch = m_chosenBunches.begin(); ibunch != m_chosenBunches.end(); ibunch++ ){
	  if( bunchCounter == (*ibunch) ) isitright =1;
	}
  }
  else isitright = 1;
  if(isitright==0){ 
	info() << " This is not the  bunch id you were looking for " <<  odin->bunchId() << endmsg;
#ifndef WIN32
	delete pulse;   
#endif
	return StatusCode::SUCCESS;
  }
  // Skip step 0 by default:
  if(m_skipStepZero && m_currentCalStep==0){
    info()<<"Skipping step 0"<<endmsg;
    delete pulse;   
    return StatusCode::SUCCESS;
  }
  // To look only at step 0:
  if(!m_skipStepZero && m_currentCalStep!=0 && m_onlyStepZero){
    info()<<"Skipping step other than 0"<<endmsg;
    delete pulse;   
    return StatusCode::SUCCESS;
  }

  // To take a limited number of events per step:
  bool eventprocessed = false;
  
  if(m_nEventsPerStep > 0 ){
    if(  m_eventPerStepCounter[m_currentCalStep] > m_nEventsPerStep ){
      info()<<"Enough stats! Skipping the rest of step " << m_currentCalStep << endmsg;
      delete pulse;   
      return StatusCode::SUCCESS;
    }
  }
  
  // Retrieve Fillingscheme

  std::vector<int> bunchCrossingSample(nSamples,-1);
  Condition* xx = getDet<Condition>(detSvc(),"/dd/Conditions/Online/LHCb/LHCFillingScheme") ;
  std::string fs1str = xx->param<std::string>("B1FillingScheme");
  std::string fs2str = xx->param<std::string>("B2FillingScheme");
  char* fs1 = (char*)fs1str.c_str();
  char* fs2 = (char*)fs2str.c_str();  
	std::vector<int> fillingScheme(fs2str.size()+1);

  for( unsigned int i=0; i<fs2str.size(); i++){
    if (fs1[i]=='1' && fs2[i]=='1')fillingScheme[i+1]=3;
    if (fs1[i]=='1' && fs2[i]=='0')fillingScheme[i+1]=1;
    if (fs1[i]=='0' && fs2[i]=='1')fillingScheme[i+1]=2;
    if (fs1[i]=='0' && fs2[i]=='0')fillingScheme[i+1]=0; 
  }

  for (int i=-2; i<=2; i++){
    debug()<<"Scheme for "<<i<<" : "<<fillingScheme[(bunchCounter+i)]<<endmsg;  
    bunchCrossingSample[i+2] = fillingScheme[(bunchCounter+i)];
  }
  // select only events which are beam-beam middle sample(2), and have non-beam-beam crossings in  prev1,prev2 and next1
  if(m_selectBunchCrossing){
  if (bunchCrossingSample[2] == 3 &&
      bunchCrossingSample[1] != 3 &&
      bunchCrossingSample[3] != 3 &&
      bunchCrossingSample[0] != 3
      ){
    info()<<"Bunch-crossing ok"<<endmsg;
  }
  else {
    info()<<"Bunch-crossing not ok"<<endmsg;
    delete pulse;
    return StatusCode::SUCCESS;
  }  
  }

#ifndef WIN32
  // loop over sample groups
  for ( itD = midSampleData -> begin(); itD != midSampleData -> end(); ++itD ) {

	LHCb::VeloTELL1Data* data = (*itD);  
	std::vector<LHCb::VeloTELL1Data* > allSamplesTell1Data;
	allSamplesTell1Data.resize(nSamples); 
	unsigned int TELL1_number=data->key();
	if(ExcludeTELL1(m_chosenTell1s,TELL1_number)&&m_chosenTell1s[0]!=-1)  continue;

	const DeVeloSensor* sens=0;
	sens=m_veloDet->sensorByTell1Id(TELL1_number);
	unsigned int sensNumber=0;
	sensNumber=sens->sensorNumber();

	if(sensNumber<64) m_sensorType=0; else m_sensorType=1; // 0 for R, 1 for phi.
	int count =0;
  
  // Get Tell1Data
	for ( itL = m_samplingLocations.begin(); itL != m_samplingLocations.end(); ++itL, count++ ) {
	  LHCb::VeloTELL1Datas* tell1Datas = veloTell1Data( (*itL) );  
	  allSamplesTell1Data[count] = tell1Datas->object(TELL1_number);
	}

	std::vector<bool> signalDetected(2048,false);

  // Loop over samples - only once per samplegroup for each channel
	for(int iSample = 0; iSample<nSamples; iSample++){
    // Loop over links
	  for(int aLink=0; aLink<NumberOfALinks; ++aLink){
      std::vector<int> linkADCs = getLink( (*allSamplesTell1Data[iSample])[aLink]);
      std::vector<double> linkCMS = SubtractCommonMode(linkADCs, m_cmsCut); 
      std::vector<double>::iterator itf;
      int channel =0;
      // Loop over channels
      for(itf=linkCMS.begin();itf!=linkCMS.end() ; ++itf, channel++){
        int channelNumber = channel + 32*aLink;
        if (m_removeHeaders){
          if (channelNumber%32==31) continue;
        }
        
        double cmsadc = *itf;
        char chnumber[100]; sprintf(chnumber, "%04d", channelNumber );
        char t1number[100]; sprintf(t1number, "%03d_sens_%03d", TELL1_number, sensNumber);
        std::string TPshapeTitle;

        // Check if we have not treated this sample group before, and if signal is over threshold
        if(((cmsadc) > m_threshold)&& signalDetected[channelNumber] == false){
          debug() << "  signal detected! channel "  << channelNumber << " on sensor " 
                  << sensNumber << " Let's fit!" << endmsg; 
          debug() << " channel adc = " << cmsadc << endmsg;
          signalDetected[channelNumber] == true;
          debug() << signalDetected[channelNumber] <<endmsg;
          sprintf(pulseshape,"pulse_Tell1%03d_ch%04d_timebin%02d_peak_sens%d",TELL1_number,channelNumber,finebin, sensNumber);
          vector<double> ADCs;
          vector<double> times;

          // Loop over samples to fill the pulse histo
          for(int jSample =0; jSample<nSamples; jSample++){
            double time = 25*(jSample - (int) nSamples/2)+step+delay;
            std::vector<int> templinkADCs = getLink( (*allSamplesTell1Data[jSample])[aLink]);
            std::vector<double> templinkCMS = SubtractCommonMode(templinkADCs, m_cmsCut);
            if(m_tryCluster==true){
              double clustersum = GetClusterADC(channelNumber, allSamplesTell1Data[jSample]);
              pulse->Fill(time,clustersum);
              ADCs.push_back(clustersum);          
              times.push_back(time);
            }
            else if(m_tryCluster==false){
              pulse->Fill(time,templinkCMS[channel]);
              ADCs.push_back(templinkCMS[channel]);          
              times.push_back(time);
            }
          }//end loop samples

          debug()<<"Pulse histogram filled"<<endmsg;
          double amplitude = pulse->GetMaximum();
          double peaktime = pulse->GetXaxis()->GetBinCenter(pulse->GetMaximumBin());
          double integral = (double) pulse->Integral(); 
          //set initial parameters for fit (not fixing them!):
          func->SetParameter(0,0);
          func->SetParameter(1,amplitude);
          func->SetParameter(2,peaktime);
          if(m_useLogLikeFit) pulse->Fit("dtfitfunc","LMQNW");
          else pulse->Fit("dtfitfunc","QNW");
          //retrieve fit parameters:
          double chisqperndof = func->GetChisquare()/((double) func->GetNDF());
          double fitoffset = func->GetParameter(0);
          double fitamplitude = func->GetParameter(1);
          double fitpeaktime = func->GetParameter(2);
          debug()<<"Pulse histogram fitted"<<endmsg;

          // 			if(integral > m_integralCut &&  fabs(peaktime) < m_timeWindow && amplitude > m_threshold){
          // 			  ////if(m_plotSeparateStrips) sprintf(finepulseshape,"ClustersVsTime_%d_ch%04d",sensNumber,channelNumber);
          // 			  ////else sprintf(finepulseshape,"ClustersVsTime_%d",sensNumber);

          // 			  ////std::string ffinepulseshape = finepulseshape;
          // 			  debug()<<"Beginning filling of profiles"<<endmsg;

          //         // Loop over samples to get the fit results from the fitted pulse histo
          // 			  for(int isample =0; isample <nSamples; isample++){
          //           double time = 25*(iSample - (int) nSamples/2)+step+delay;//vh        
          // 			    int binnumber = pulse->FindBin(time);
          // 			    double adcvalue = pulse->GetBinContent(binnumber);
          //           //ADCs.push_back(adcvalue);          
          //           //times.push_back(time);

          //           ////this is the main plot which is used in TimeAlign later on, for fitting the actual landaus and pulseshape:
          //           //plot2D(time,adcvalue,ffinepulseshape,ffinepulseshape,
          //           // 	-25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25, -127.5, 128.5, 50*(nSamples+2), 256);	
			    
          //           //profile1D(time,adcvalue,ffinepulseshape+"prof",ffinepulseshape+"prof",
          //           // 	-25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25,  50*(nSamples+2));
          // 			  }//end loop samples  
          //       }//end if (integral - timewindow - amplitude)
      
          //Fill the Tuple per event
          unsigned int strip = sens->ChipChannelToStrip(channelNumber);
          const DeVeloPhiType* phisensor; 
          const DeVeloRType* rsensor; 
          double radius,phi;
          if (sens->isPhi()) phisensor =  m_veloDet->phiSensor(TELL1_number);
          else phisensor = NULL;
          if (sens->isR()) rsensor =  m_veloDet->rSensor(TELL1_number);
          else rsensor = NULL;
          if (rsensor != NULL) radius = rsensor->globalROfStrip(strip);
          else radius = -100.;
          if (phisensor !=NULL) phi = phisensor->globalPhiOfStrip(strip);
          else phi = -100.;

          // store whether the requirements from the fit are met
          bool condition1=false;
          bool condition2=false;
          //ClustersVsTime condition
          if(integral       > m_integralCut &&  
             fabs(peaktime) < m_timeWindow  && 
             amplitude      > m_threshold   ){
            condition1=true;
          }
          //FittedClustersVsTime condition
          if(fitamplitude      > m_threshold    &&  
             fitamplitude      < m_amplitudeCut && 
             fabs(fitpeaktime) < m_timeWindow   && 
             integral          > m_integralCut  &&
             fabs(fitoffset)   < m_offsetCut    && 
             chisqperndof      < m_chisqCut ) { 
            condition2 = true;
          }

          // Fill Ntuple per channel
          tuple->column( "Condition1", condition1 );
          tuple->column( "Condition2", condition2 );

          tuple->column( "Event", long( odin->eventNumber ()) );
          tuple->column( "Run", long( odin->runNumber()) );
          tuple->column( "EventType", long( odin->eventType()) );
          tuple->column( "BunchID", long( odin->bunchId()) );

          //tuple->column( "BunchCrossingType", long( odin->bunchCrossingType()) );
          tuple->column( "BunchCrossingPrev2",bunchCrossingSample[0]  );
          tuple->column( "BunchCrossingPrev1",bunchCrossingSample[1]  );
          tuple->column( "BunchCrossingMid",bunchCrossingSample[2]  );
          tuple->column( "BunchCrossingNext1",bunchCrossingSample[3]  );
          tuple->column( "BunchCrossingNext2",bunchCrossingSample[4]  );

          tuple->column( "SensorID",sensNumber );
          tuple->column( "StripID",strip );
          tuple->column( "LinkID",aLink  );
          tuple->column( "ChannelID",channelNumber  );
          tuple->column( "R",radius );
          tuple->column( "Phi",phi );
			  
          tuple->column( "Integral",integral);
          tuple->column( "Peak",peaktime  );
          tuple->column( "Amplitude",amplitude  );
          tuple->farray( "ADC_array",ADCs,"nADCs",5);
          tuple->farray( "Time_array",times,"nADCs",5);

          tuple->column( "Fit_ChiPDoF",chisqperndof );
          tuple->column( "Fit_Offset", fitoffset);
          tuple->column( "Fit_Amplitude",fitamplitude );
          tuple->column( "Fit_Peaktime",fitpeaktime );
          tuple->column( "Step",step );			  
          tuple->write();
			   

          debug()<<"ADC counts added to profile"<<endmsg;

          debug()<<"Fit values collected"<<endmsg;
          ADCs.clear();
          times.clear();
          pulse->Reset();

          eventprocessed=true;
          
          debug()<<" m_eventPerStepCounter[m_currentCalStep] for step "<<m_currentCalStep
                 <<" : "<< m_eventPerStepCounter[m_currentCalStep]<<endmsg;
          
        }//endif signal detected - once per samplegroup

		}//end loop linkCMS (channels)
	  }//end loop links
	}//end loop samples (once for each samplegroup per channel)
  }//end loop midSampleData

  debug()<<"**END LOOP** m_eventPerStepCounter[m_currentCalStep] for step "<<m_currentCalStep
         <<" : "<< m_eventPerStepCounter[m_currentCalStep]<<endmsg;
          
  debug()<<"Event done"<<endmsg;
  delete pulse;
#endif
  
  delete func;  
  
  m_nEvents++;
  if (eventprocessed) m_eventPerStepCounter[m_currentCalStep] +=1; 
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::PulseShapeFitterTedTree::finalize() {
  if ( m_debugLevel ) debug() << "==> Finalize" << endmsg;
  return GaudiHistoAlg::finalize();  // must be called after all other actions

}

//=============================================================================
// Retrieve the VeloTell1Data
//=============================================================================
LHCb::VeloTELL1Datas*
Velo::PulseShapeFitterTedTree::veloTell1Data( std::string samplingLocation ) {
  std::string tesPath;
  if(samplingLocation == "") tesPath = samplingLocation + "Raw/Velo/DecodedADC"; 
  else  tesPath = samplingLocation + "/Raw/Velo/DecodedADC";
  size_t pos = tesPath.find( "//" );
  if ( pos != std::string::npos )    tesPath.replace( pos, 2, "/" );
  if ( m_debugLevel ) 	debug() << "Retrieving VeloTell1Data from " << tesPath << endmsg;
  if ( !exist<LHCb::VeloTELL1Datas>( tesPath ) ) {
	debug() << "No VeloTell1Data container found for this event !" << endmsg; 
	if (m_nEvents %100==0  )  info() << "No VeloTell1Data container found for this event !   " << tesPath << endmsg; 
	return 0;
  }
  else {
	LHCb::VeloTELL1Datas* tell1Data = get<LHCb::VeloTELL1Datas>( tesPath );
	if ( m_debugLevel ) debug() << "  -> number of VeloTell1Data found in TES: "
	  << tell1Data->size() <<endmsg;
	return tell1Data;
  }
}
//=============================================================================
// double Velo::fitsimple(double *x, double *par){
//   double fitval;
//   if(x[0]<par[2]) fitval = par[0]+par[1]*(exp(-((x[0]-par[2])/par[3])*((x[0]-par[2])/par[3])));
//   else fitval = par[0]+par[1]*(exp(-((x[0]-par[2])/par[4])*((x[0]-par[2])/par[4])));
//   return fitval;
// }
// simple link common mode subtraction
std::vector<double>  Velo::PulseShapeFitterTedTree::SubtractCommonMode( std::vector<int> data, double cut = 15.0)
{
  int howManyPasses =3;
  std::vector<double> commonModeSubtractedData;
  std::vector<int>::iterator it;
  std::vector<double>::iterator itf;
  double average = 0;
  int count = 0, chanWithoutHits = 0;  double averageWithoutHits = 0;
  for(it=data.begin();it!=data.end() ; ++it){
	average += float(*it);
  }
  average /= float(data.size());
  for(it=data.begin();it!=data.end() ; it++){
	commonModeSubtractedData.push_back((double)(*it) - average);
  }
  int pass=0;
  while(pass < howManyPasses){
	chanWithoutHits=0; averageWithoutHits =0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf){
	  if(fabs(*itf) < cut )  {
		averageWithoutHits += *itf;
		chanWithoutHits++;
	  }
	}
	if(chanWithoutHits > 0) averageWithoutHits /= double(chanWithoutHits);
	else averageWithoutHits = 0;
	count = 0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf, count++){
	  commonModeSubtractedData[count]  = *itf - averageWithoutHits;
	}
	pass++;
  }
  return commonModeSubtractedData;
}
// attempt of clusterizing the data.
double Velo::PulseShapeFitterTedTree::GetClusterADC( int seedChannel, LHCb::VeloTELL1Data *tell1data){
  if(seedChannel < 0 || seedChannel >2047) {
	info() << "channel does not exist! " << endmsg;
  }
  double clusterADC = 0;
  int stripNumber = m_stripFromChannel[m_sensorType][seedChannel];
  for(int ineighbour = -m_takeNeighbours; ineighbour <= m_takeNeighbours; ineighbour++ ){ 
	if(stripNumber+ineighbour >= 0 && stripNumber +ineighbour  <2048){
	  int currentstrip =  stripNumber+ineighbour;
	  int currentchannel = m_channelFromStrip[m_sensorType][currentstrip]; 
	  int channelnumber = (int)(currentchannel%32);
	  int aLink = (int)(currentchannel/32.);
	  std::vector<int> linkADCs = getLink( (*tell1data)[aLink]);
	  std::vector<double> linkCMS = SubtractCommonMode(linkADCs, m_cmsCut); 
	  double channeladc = linkCMS[channelnumber];
	  if(ineighbour ==0 ) debug() << "seedADC == " <<  channeladc <<  endmsg ;  
	  clusterADC += channeladc; 
	} 
  }
  return clusterADC;
}

//simple mean common mode subtraction
double Velo::PulseShapeFitterTedTree::subtractCommonMode(int channelNumber, LHCb::VeloTELL1Data *tell1data){
  ALinkPair begEnd;
  int aLink = (int)(channelNumber/32.);
  begEnd=(*tell1data)[aLink];
  scdatIt iT;
  double commonmode=0;
  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
	commonmode += *iT;  
  }
  commonmode /= 32.0;
  int count = 0;
  double commonmodewithoutsignal= 0.0;
  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
	double cmsadc = *iT - commonmode; 
	if(fabs(cmsadc) < m_threshold){ 
	  commonmodewithoutsignal += *iT;
	  count++;
	}
  }
  commonmodewithoutsignal /= float(count+0.00001);
  double cmsadc = tell1data->channelADC(channelNumber) - commonmodewithoutsignal;
  return cmsadc;
}

