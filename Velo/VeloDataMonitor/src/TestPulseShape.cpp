// $Id: TestPulseShape.cpp,v 1.3 2009-09-20 18:45:18 szumlat Exp $
// Include files
// -------------

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// from Tell1Kernel
#include "Tell1Kernel/VeloDecodeConf.h" 

// local
#include "TestPulseShape.h"
#include "CommonFunctions.h"

// from Boost
#include <boost/assign/list_of.hpp>
#include "boost/lexical_cast.hpp"

// from Gaudi
#include "AIDA/IHistogram2D.h"
// local
// Kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "GaudiKernel/IEventTimeDecoder.h"            // Interface for odin
#include "Event/ODIN.h"

using namespace VeloTELL1;


//-----------------------------------------------------------------------------
// Implementation file for class : TestPulseShape
//
// 2008-08-20 : Kazu Akiba
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
namespace Velo {
  DECLARE_ALGORITHM_FACTORY( TestPulseShape )
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::TestPulseShape::TestPulseShape( const std::string& name, ISvcLocator* pSvcLocator)
  : Velo::VeloMonitorBase ( name , pSvcLocator )
  ,m_nEvents(0)
{
  std::vector<std::string> tmpLocations = 
    boost::assign::list_of("Prev7")("Prev6")("Prev5")("Prev4")("Prev3")("Prev2")("Prev1")
    ("")("Next1")("Next2")("Next3")("Next4")("Next5")("Next6")("Next7");

  declareProperty( "SamplingLocations", m_samplingLocations = tmpLocations );
  declareProperty( "Plot2D", m_plot2d= 0 );
  declareProperty( "CMSCut", m_cmsCut= 10 );
  declareProperty( "DebugPolarity", m_debugPolarity = false );
  declareProperty( "PlotProfile", m_plotProf= 1 );
  declareProperty( "PlotRaw", m_plotRaw= 0 );
  declareProperty( "PlotCMS", m_plotCMS= 1 );
  declareProperty( "PlotOnlyLink", m_plotLink= 0 );
  declareProperty( "SuperimposePolarities", m_dontSplit= 0 );
  declareProperty( "NSteps", m_nSteps= 25 );
  declareProperty( "InvertedSteps", m_inverted= false );
  declareProperty( "StepSize", m_stepSize= 100 );
  declareProperty( "ShiftByNSteps", m_shift= 5 );
  declareProperty( "WrapPoint", m_wrapPoint= 15 );
  declareProperty( "WrappedSteps", m_wrappedSteps= 5 );
  declareProperty( "PulsedChannels", m_pulsedChannels);
  declareProperty( "TELL1s",                        m_chosenTell1s     );
  declareProperty( "RunWithOdin" , m_runWithOdin = true ); // 
  m_pulsedChannels.push_back(4);
  m_pulsedChannels.push_back(23);

}

//=============================================================================
// Destructor
//=============================================================================
Velo::TestPulseShape::~TestPulseShape() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::TestPulseShape::initialize() {
  StatusCode sc = VeloMonitorBase::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;
  m_velo = getDet<DeVelo>( DeVeloLocation::Default );  
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::TestPulseShape::execute() {
  counter( "# events" ) += 1;
  m_nEvents++;
  //info() << "running here"  << endmsg;
  // Loop over the sampling locations and produce the monitoring plots
  // -----------------------------------------------------------------
  std::vector<std::string>::const_iterator itL;
  m_evtInfos = veloTell1EventInfos(); 
  for ( itL = m_samplingLocations.begin(); itL != m_samplingLocations.end(); ++itL ) {
    LHCb::VeloTELL1Datas* tell1Datas = veloTell1Data( (*itL) );
    if( tell1Datas == 0 ) continue;
    MakePulseShape( (*itL), tell1Datas );
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::TestPulseShape::finalize() {
  if ( m_debugLevel ) debug() << "==> Finalize" << endmsg;
  return VeloMonitorBase::finalize(); // must be called after all other actions
}

//=============================================================================
// Retrieve the VeloTell1Data
//=============================================================================
LHCb::VeloTELL1Datas*
Velo::TestPulseShape::veloTell1Data( std::string samplingLocation ) {
  //std::string tesPath = "Raw/Velo/" + samplingLocation + "/ADCCMSuppressed";
  //std::string tesPath = samplingLocation + "Raw/Velo/"+  "/ADCCMSuppressed" ;
  std::string tesPath;
 
   if(samplingLocation == "") tesPath = samplingLocation + "Raw/Velo/DecodedADC"; 
   else  tesPath = samplingLocation + "/Raw/Velo/DecodedADC";
   //info() << "samplingLocation  " << samplingLocation  << endmsg;
  
  size_t pos = tesPath.find( "//" );
  if ( pos != std::string::npos )    tesPath.replace( pos, 2, "/" );
  if (m_nEvents %100==1  )debug() << "tesPath = " << tesPath << endmsg; 
     debug() << "Retrieving VeloTell1Data from " << tesPath << endmsg;
  if ( !exist<LHCb::VeloTELL1Datas>( tesPath ) ) {
     info() << "No VeloTell1Data container found for this event !" << endmsg; 
     if (m_nEvents %100==1  )  info() << "No VeloTell1Data container found for this event !   " << tesPath << endmsg; 
    return 0;
  }
  else {
    LHCb::VeloTELL1Datas* tell1Data = get<LHCb::VeloTELL1Datas>( tesPath );
	debug() << "  -> number of VeloTell1Data found in TES: " << tell1Data->size() <<endmsg;
    return tell1Data;
  }
}
//=============================================================================
EvtInfos* Velo::TestPulseShape::veloTell1EventInfos( ) {
   EvtInfos *evtInfos  =0;
  // get the event info only for the mid sample.
  if(!exist<EvtInfos>( EvtInfoLocation::Default )){
    debug()<< " ==> There is no Event info in the data. QUITTING. " <<endmsg;
  }else{
    evtInfos       = get<EvtInfos>      ( EvtInfoLocation::Default       );
  }
  return evtInfos;
}



//=============================================================================
// Monitor the VeloTell1Data
//=============================================================================
void Velo::TestPulseShape::MakePulseShape( std::string samplingLocation,  LHCb::VeloTELL1Datas* tell1Datas ) 
{ 
  //m_nSteps = 25;
  //unsigned int nxbins = m_samplingLocations.size()*m_nSteps +1;
  unsigned int nxbins = m_samplingLocations.size()*m_nSteps;
  int iphase = (int)(m_nEvents/m_stepSize);
  if(m_inverted==true) iphase = m_nSteps - iphase -1 ;
  if(iphase >= m_wrapPoint && ((iphase - m_wrapPoint) < m_wrappedSteps) ) iphase = iphase - 25;
  int halfTAEWindow = (int)(floor(m_samplingLocations.size()/2.));
  double binsize =  (25./float(m_nSteps));
  std::string polarity;
  // Loop over the VeloTell1Data
  // ==========================+
  LHCb::VeloTELL1Datas::const_iterator itD;
  for ( itD = tell1Datas -> begin(); itD != tell1Datas -> end(); ++itD ) {
    LHCb::VeloTELL1Data* data = (*itD);
	int TELL1_number=data->key();
	if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue;
    EvtInfo* anInfo=m_evtInfos->object(TELL1_number);
    int l0EventCounter= (anInfo->l0EventID(0));
	if (l0EventCounter%2 ==0) polarity = "_Even";
	else polarity = "_Odd";

	// Charge for each channel
	// -----------------------
	ALinkPair begEnd;
	for(int aLink=0; aLink<NumberOfALinks; ++aLink){
	  std::vector<int> linkADCs = getLink( (*data)[aLink]);
	  std::vector<double> linkCMS = SubtractCommonMode(linkADCs, m_cmsCut); 
	  std::vector<double>::iterator itf;
	  // if(m_plotLink >=0 && m_plotLink < 64) if(aLink != m_plotLink ) continue ;
	  double realPhase = float(iphase)*(binsize)+0.000001; 
	  int channel=0;
	  for(itf=linkCMS.begin();itf!=linkCMS.end() ; ++itf, channel++){
		for(std::vector<int>::iterator  ipulsedchannel = m_pulsedChannels.begin(); ipulsedchannel != m_pulsedChannels.end(); ipulsedchannel++ ){ 
		  if(channel == *ipulsedchannel ){
			double  cmsadc = *itf;
			int adc = linkADCs[channel];
			int samplingIndex = 999;
			for( unsigned int i = 0; i < m_samplingLocations.size(); ++i ) 
			  if ( m_samplingLocations[i] == samplingLocation )	samplingIndex = i-halfTAEWindow;
			char chnumber[100]; sprintf(chnumber, "%04d", channel + 32*aLink);
			char t1number[100]; sprintf(t1number, "%03d", TELL1_number);
			std::string TPshapeTitle;
			std::string profilePrefix = "prof";
			TPshapeTitle = "TestPulseShape_Tell1_"; 
			TPshapeTitle+= t1number; //  TPshapeTitle +=  polarity;  
			TPshapeTitle += "_ch_"; TPshapeTitle += chnumber;
			if(m_plot2d && m_plotRaw && m_dontSplit) plot2D(samplingIndex*25. +realPhase, adc, TPshapeTitle,	TPshapeTitle,-0.5*binsize - halfTAEWindow*25, - 0.5*binsize +(halfTAEWindow+1)*25, 512-128.5, 512+127.5, nxbins, 128 );
			if(m_plotProf && m_plotRaw && m_dontSplit ) profile1D(samplingIndex*25. +realPhase, adc, (profilePrefix+TPshapeTitle).c_str(),	(profilePrefix+TPshapeTitle).c_str(),-0.5*binsize - halfTAEWindow*25, - 0.5*binsize +(halfTAEWindow+1)*25, nxbins);
			TPshapeTitle = "TestPulseShape_Tell1_";
			TPshapeTitle+= t1number;  TPshapeTitle +=  polarity;
			TPshapeTitle += "_ch_"; TPshapeTitle += chnumber;
			if(m_plot2d && m_plotRaw) plot2D(samplingIndex*25. +realPhase, adc, TPshapeTitle, TPshapeTitle,-0.5*binsize - halfTAEWindow*25, - 0.5*binsize +(halfTAEWindow+1)*25, 512-128.5, 512+127.5, nxbins, 128 );
			if(m_plotProf && m_plotRaw) profile1D(samplingIndex*25. +realPhase, adc, (profilePrefix+TPshapeTitle).c_str(), (profilePrefix+TPshapeTitle).c_str(),-0.5*binsize - halfTAEWindow*25, - 0.5*binsize +(halfTAEWindow+1)*25, nxbins);
			std::string cmsprefix = "cms";
			if(m_debugPolarity == true )info()<< " tell1"  << TELL1_number <<"  samplingIndex "  << samplingIndex <<  " cmsadc " <<  (cmsadc) << "  chnumber " <<  chnumber << " polarity " << polarity <<  "event number " << m_nEvents <<  endmsg; 
			if(m_plot2d && m_plotCMS) plot2D(samplingIndex*25. +realPhase, (cmsadc), (cmsprefix+TPshapeTitle).c_str(), (cmsprefix+TPshapeTitle).c_str(),-0.5*binsize - halfTAEWindow*25, - 0.5*binsize +(halfTAEWindow+1)*25, -128.5, +127.5, nxbins, 128 );
			if(m_plotProf&& m_plotCMS) profile1D(samplingIndex*25. +realPhase, (cmsadc), (profilePrefix+cmsprefix+TPshapeTitle).c_str(), (cmsprefix+TPshapeTitle).c_str(),-0.5*binsize - halfTAEWindow*25, - 0.5*binsize +(halfTAEWindow+1)*25,  nxbins);
		  }
		}
	  }
	}
  }
}
//=============================================================================
std::vector<double>  Velo::TestPulseShape::SubtractCommonMode( std::vector<int> data, double cut = 15.0)
{
  int howManyPasses =2;
  std::vector<double> commonModeSubtractedData;
  std::vector<int>::iterator it;
  std::vector<double>::iterator itf;
  double average = 0;
  int count = 0, chanWithoutHits = 0;  double averageWithoutHits = 0;
  for(it=data.begin();it!=data.end() ; ++it){
	average += float(*it);
  }
  average /= float(data.size());
  for(it=data.begin();it!=data.end() ; it++){
	commonModeSubtractedData.push_back((double)(*it) - average);
  }
  int pass=0;
  while(pass < howManyPasses){
	chanWithoutHits=0; averageWithoutHits =0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf){
	  if(fabs(*itf) < cut )  {
		averageWithoutHits += *itf;
		chanWithoutHits++;
	  }
	}
	if(chanWithoutHits > 0) averageWithoutHits /= double(chanWithoutHits);
	else averageWithoutHits = 0;
	count = 0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf, count++){
	  commonModeSubtractedData[count]  = *itf - averageWithoutHits;
	}
	pass++;
  }
  return commonModeSubtractedData;
}

