// $Id: $
// Include files 
#include <string>
#include <vector>

// boost
#include <boost/foreach.hpp>
#include <boost/unordered_set.hpp>

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// Event
#include <Event/RawEvent.h>
#include <Event/RawBank.h>
#include <Event/ODIN.h>

// local
#include "SelectBanks.h"

//-----------------------------------------------------------------------------
// Implementation file for class : SelectBanks
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_NAMESPACE_ALGORITHM_FACTORY( Velo::Monitoring,SelectBanks )

using namespace std;
using LHCb::RawBank;
using LHCb::RawEvent;
using boost::unordered_set;

//=============================================================================
Velo::Monitoring::SelectBanks::SelectBanks(const std::string& name,
                        ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
{

   declareProperty( "RawEventLocation", m_rawEventLocation = LHCb::RawEventLocation::Default );

}

//=============================================================================
Velo::Monitoring::SelectBanks::~SelectBanks() {
} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::SelectBanks::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  //m_evtList.open("NZS_EB_events.txt",std::ios_base::out);

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::SelectBanks::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  
  RawEvent* rawEvent = get< RawEvent >( m_rawEventLocation );
  
  // Check for NZS banks
  const vector< RawBank* >& nzsBanks = rawEvent->banks( RawBank::VeloFull );
  if ( nzsBanks.empty() ) {
    setFilterPassed( false );
    return StatusCode::SUCCESS;
    }

  // Check for ZS banks
  /*  const vector< RawBank* >& zsBanks = rawEvent->banks( RawBank::Velo );
  if ( zsBanks.empty() ) {
    setFilterPassed( false );
    return StatusCode::SUCCESS;
    }*/
  
  // Check for error banks
  /*
  const vector< RawBank* >& errorBanks = rawEvent->banks( RawBank::VeloError );
  if ( errorBanks.empty() ) {
     setFilterPassed( false );
     return StatusCode::SUCCESS;
  }
  
  unordered_set< unsigned int > nzsIDs;
  BOOST_FOREACH( const RawBank* bank, nzsBanks ) {
     const unsigned int tell1ID = static_cast< unsigned int >( bank->sourceID() );
     nzsIDs.insert( tell1ID );
  }

  // Check for overlap
  bool good = false;
  BOOST_FOREACH( const RawBank* bank, errorBanks ) {
     const unsigned int tell1ID = static_cast< unsigned int >( bank->sourceID() );
     if ( nzsIDs.count( tell1ID ) ) {
        good = true;
        break;
     }
  }

  const LHCb::ODIN* odin = get< LHCb::ODIN >( LHCb::ODINLocation::Default );
  if ( good ) {
    info() << "Passed event: " << odin->runNumber() << " " << odin->eventNumber() <<endmsg;
  }

  //m_evtList << " L0_ID= " << odin->eventNumber() << " Tell1= " << TELL1_number <<std::endl;	
  */
  setFilterPassed( true );
  
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::SelectBanks::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  //m_evtList.close();

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
