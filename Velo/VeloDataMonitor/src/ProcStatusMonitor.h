#ifndef INCLUDE_PROCSTATUSMONITOR_H
#define INCLUDE_PROCSTATUSMONITOR_H 1

#include <string>
#include <vector>

#include <TH1D.h>

#include "VeloMonitorBase.h"

namespace Velo {

  /** @class ProcStatusMonitor ProcStatusMonitor.h
   * 
   * Check rate of ProcStatus events in decoding and VELO Pattern recognition
   *
   * @author David Hutchcroft <David.Hutchcroft@cern.ch>
   * @date   2012-04-24
   *
   * Make a histogram of the number of times the decoding failed, due to 
   * bad data, too many VELO clusters or the pattern recognition had to stop
   *
   */
  class ProcStatusMonitor : public Velo::VeloMonitorBase {
  public:

    /// Standard Constructor
    ProcStatusMonitor(const std::string& name, ISvcLocator* pSvcLocator);

    virtual ~ProcStatusMonitor() {}; ///< Destructor

    virtual StatusCode initialize() override; ///< Algorithm initialization
    virtual StatusCode    execute() override; ///< Algorithm event execution
    virtual StatusCode   finalize() override; ///< Algorithm finalize

  private:
    /// add a new axis label (up to nBins) to m_numProcStatus
    void addLabel(const std::string & newLabel);

    /// add an entry to the historgram
    bool addProc(const std::string & message);

    /// histogram of ProcStatuses
    TH1D *m_numProcStatus;
    
    /// Vector of ProcStatuses expected (Unknown is last one for new messages)
    std::vector<std::string> m_procMessages;

    /// Number of messages to log file (defaults 10)
    unsigned int m_logWarnings;
    
  };

} // namespace Velo

#endif // INCLUDE_PROCSTATUSMONITOR_H

