// ============================================================================
// Include files 
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/AlgFactory.h" 
// ============================================================================
// GaudiAlg 
// ============================================================================
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Fill.h"
// ============================================================================
// GaudiUti
// ============================================================================
#include "GaudiUtils/HistoTableFormat.h"
// ============================================================================
// VeloTELL1Event
// ============================================================================
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"
// ============================================================================
// AIDA 
// ============================================================================
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
// ============================================================================
// Boost 
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
// VeloDet
// ============================================================================
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"
// ============================================================================
// local 
// ============================================================================
#include "VetraKernel/INoiseEvaluator.h"
#include "VetraKernel/TELL1Noise.h"
#include "Tell1Kernel/IPvssTell1Names.h"

#include "CommonFunctions.h"
// ============================================================================

using namespace VeloTELL1;

namespace Velo 
{
  namespace Monitoring 
  {
    /** @classNoiseMon 
     *  Simple algorithm to perform monitoring of pedestals&noise 
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-10-26
     */
    class NoiseMon : public GaudiHistoAlg 
    {
      /// friend factory for instantiation
      friend class AlgFactory<Velo::Monitoring::NoiseMon>;
      public:
      // initialize it! 
      virtual StatusCode initialize () override;
      // execute it! 
      virtual StatusCode execute    () override;
      // finalize it! 
      virtual StatusCode finalize   () override;
      protected:
      /// standard constructo 
      NoiseMon 
        ( const std::string& name , 
          ISvcLocator*       pSvc ) 
        : GaudiHistoAlg ( name , pSvc ) 
        //
        , m_locations () 
        , m_tell1s    () 
        , m_publish   ( 10000 )
        , m_toolType  ( "Velo::Monitoring::NoiseEvaluator" )
        //
        , m_event     ( 0 )
        //
        , m_noises    () 
        //
        , m_isInit    (0)
        //
        , m_veloDet   (0)
        //
        , m_onlineMonitoring(false)

        {
          //
          m_locations.push_back ( LHCb::VeloTELL1DataLocation::ADCs             ) ;
          //        m_locations.push_back ( LHCb::VeloTELL1DataLocation::SubPeds          ) ;
          m_locations.push_back ( LHCb::VeloTELL1DataLocation::PedSubADCs       ) ;
          m_locations.push_back ( LHCb::VeloTELL1DataLocation::FIRCorrectedADCs ) ;
          m_locations.push_back ( LHCb::VeloTELL1DataLocation::ReorderedADCs    ) ;
          m_locations.push_back ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ) ;
          //
          declareProperty 
            ( "Containers" , 
              m_locations  , 
              "The list of TES locations to be monitored") ;
          //
          declareProperty 
            ( "TELL1s"     , 
              m_tell1s     , 
              "The list of TELL1s to be monitored, empty for all") ;
          //

          declareProperty 
            ( "NoiseEvaluatorType" , 
              m_toolType , 
              "The actual type of noise evaluator tool" ) ;
          //
          declareProperty 
            ( "PublishingFrequency" , 
              m_publish             , 
              "The frequency to publish results" ) ;            
          //
          setProperty ( "HistoPrint"  , false     ).ignore() ;
          setProperty ( "HistoTopDir" , "Vetra/" ).ignore() ;

          declareProperty("OnlineMonitoring"
              , m_onlineMonitoring = false
              , "Set this to 'true' if you want to publish histograms for onli monitoring.");

          setProperty ( "PropertiesPrint", true ).ignore();
        }
      /// virtual and protected destructor 
      virtual ~NoiseMon() {}
      private:
      // no default constructor 
      NoiseMon ()                  ; ///< no default constructor 
      // no copy constructor 
      NoiseMon ( const NoiseMon& ) ; ///< no copy constructor 
      // no assigmenet operator 
      NoiseMon& operator=( const NoiseMon& ) ; ///< no assignement 
      protected:

      /// retrieve the tools
      void retrieveTools   ();
      /// book all histograms
      void bookHistograms   ();
      /// make the plots
      void makeThePlots () ;

      bool isRorPhi( unsigned int sensorNumber );
      int isAorC( unsigned int sensorNumber );
      int getStationNumber( unsigned int sensorNumber);
      bool isDownStream(unsigned int sensorNumber); 
      private:
      // the actual type of container of data locations 
      typedef std::vector<std::string>                  Locations ;
      typedef Velo::Monitoring::INoiseEvaluator::TELL1s TELL1s    ;
      typedef std::map<std::string,Velo::Monitoring::INoiseEvaluator*> Tools ;
      // the input containers of data to be monitored
      Locations     m_locations ; ///< the input containers
      // the list of IDs for TELL1s to be monitored 
      TELL1s        m_tell1s    ; ///< list of ids for TELL1 
      // publishing frequency
      unsigned long m_publish   ; ///< publishing frequency
      // use the public tools? 
      bool          m_public    ; ///< use the public tools 
      // the actual tool type 
      std::string   m_toolType  ; ///< the actual tool type
      // event counter 
      unsigned long m_event     ; ///< internal event counter 
      // the map of tools to be used 
      Tools         m_noises    ; ///< map of tools to be used 
      // pvss name mapping tool
      Velo::IPvssTell1Names* m_pvssNames;
      // Flag for initializing the process info bank.
      int m_isInit;
      // Convergence limit from pedestal subtractor
      unsigned int m_convergenceLimit;      
      // Detector element pointer
      DeVelo* m_veloDet; 

      // are we publishing for online monitoring?
      bool m_onlineMonitoring;

      // cache for histograms
      std::vector< std::map<unsigned int, AIDA::IHistogram1D*> > m_rmsStripHistos; 
      std::vector< std::map<unsigned int, AIDA::IHistogram1D*> > m_rmsChipChannelHistos; 
     
      std::vector<AIDA::IHistogram1D*> m_rmsAverage;    
      std::vector<AIDA::IHistogram1D*> m_rmsAverageR;    
      std::vector<AIDA::IHistogram1D*> m_rmsAveragePhi;    
      std::vector<AIDA::IHistogram1D*> m_rmsNoiseR;    
      std::vector<AIDA::IHistogram1D*> m_rmsNoisePhi;    
      std::vector<AIDA::IHistogram2D*> m_rmsNoiseLink;    
      std::vector<AIDA::IHistogram2D*> m_sensorStationR;    
      std::vector<AIDA::IHistogram2D*> m_sensorStationPhi;    
      std::vector<AIDA::IHistogram1D*> m_headerNoiseAllSensors;    
      std::vector<AIDA::IHistogram1D*> m_noisyHeaderLinksPerSensor; 
    } ;
  } // end of namespace Velo::Monitoring 
} // end of namespace Velo
// ============================================================================

// ============================================================================
//  make the plots 
// ============================================================================
void Velo::Monitoring::NoiseMon::makeThePlots () 
{
  // loop over all all tools (information sources) 
  unsigned int noiseCalculator = 0;
  for ( Tools::const_iterator it = m_noises.begin() ; 
      m_noises.end() != it ; ++it, ++noiseCalculator )
  {
    // for the given tool get all TELL1s:
    Velo::Monitoring::INoiseEvaluator::INoiseMap m = it->second->noise() ;

    // check whether underlying container is reordered
    bool isReordered = it->second->isReordered();

    /// loop over TELL1s for the given tool 
    for ( Velo::Monitoring::INoiseEvaluator::INoiseMap::const_iterator il = 
        m.begin () ; m.end () != il ; ++il ) 
    {
      // get the counters: 
      typedef Velo::Monitoring::TELL1Noise::Counters Counters ;
      const Counters& cnts = il->second->counters() ;
      const unsigned int tell1Number = il->first;

      // check the first channel (counter) to see whether we have enough stats,
      // continue to next TELL1 if not.
      unsigned int stats=cnts.begin()->nEntries();
      if ( 0 == stats || 0 != ( stats %  m_publish) ) {
        continue;
      }      
      
      //Get sensor from detector element:  
      const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(tell1Number);
      if(sensor == 0 ){
        error() << "Can't find a sensor associated with TELL1 " << tell1Number << ". Skipping..." << endmsg;
        continue;
      }

      //histograms for noise:
      AIDA::IHistogram1D* histoRms_chipch = 0; 
      if ( m_rmsStripHistos[noiseCalculator].end() == m_rmsStripHistos[noiseCalculator].find(tell1Number) ) {
        info() << "No booked channel ordered histogram for unexpected sensor number " << tell1Number << ". Skipping." << endmsg;
      } else {
        histoRms_chipch = m_rmsChipChannelHistos[noiseCalculator][tell1Number];
      }
      if ( 0 != histoRms_chipch ) { histoRms_chipch -> reset() ; } // ATTENTION: RESET 

      AIDA::IHistogram1D* histoRms_strip = 0; 
      if ( m_rmsStripHistos[noiseCalculator].end() == m_rmsStripHistos[noiseCalculator].find(tell1Number) ) {
        info() << "No booked channel ordered histogram for unexpected sensor number " << tell1Number << ". Skipping." << endmsg;
      } else {
        histoRms_strip = m_rmsStripHistos[noiseCalculator][tell1Number];
      }
      if ( 0 != histoRms_strip ) { histoRms_strip -> reset() ; } // ATTENTION: RESET 


      // fill the histos :
      for ( Counters::const_iterator ic = cnts.begin() ; cnts.end() != ic ; ++ic ) 
      {
        const double index = ic - cnts.begin() ;

        unsigned int chipchannel = static_cast<unsigned int> (index);	
        unsigned int strip = sensor->ChipChannelToStrip( static_cast<unsigned int> (index));

        if(isReordered){
          chipchannel=sensor->StripToChipChannel(static_cast<unsigned int> (index));
          strip=static_cast<unsigned int> (index);
        }

        if ( 0 != histoRms_chipch ) 
        { histoRms_chipch -> fill ( chipchannel , ic -> flagRMS  () ) ; } 
        
        if ( 0 != histoRms_strip ) 
        { histoRms_strip -> fill ( strip , ic -> flagRMS  () ) ; } 
      }

      // rest all counters for this TELL1
      it->second->reset(tell1Number);
    }        
  }
  return;
}


// ===========================================================================
// Retrieve the tools
// ============================================================================
void Velo::Monitoring::NoiseMon::retrieveTools   () {

  // loop over all input containers and get the tools:
  for ( Locations::const_iterator il = m_locations.begin() ; 
      m_locations.end() != il ; ++il ) 
  {

    std::string toolName = strippedContainerName( *il ) ;
    // get the evaluator of the noise:
    Velo::Monitoring::INoiseEvaluator* eval = tool<Velo::Monitoring::INoiseEvaluator>
      ( m_toolType , toolName) ;

    if(eval!=0)
      m_noises[ *il ] = eval;
    
  }
  m_rmsStripHistos.resize(m_noises.size());
  m_rmsChipChannelHistos.resize(m_noises.size()); 
  m_rmsAverage.resize(m_noises.size());
  m_rmsAverageR.resize(m_noises.size());
  m_rmsAveragePhi.resize(m_noises.size());
  m_rmsNoiseR.resize(m_noises.size());
  m_rmsNoisePhi.resize(m_noises.size());
  m_rmsNoiseLink.resize(m_noises.size());
  m_sensorStationR.resize(m_noises.size());
  m_sensorStationPhi.resize(m_noises.size());
  m_headerNoiseAllSensors.resize(m_noises.size());
  m_noisyHeaderLinksPerSensor.resize(m_noises.size());

  m_pvssNames = tool<Velo::IPvssTell1Names>("Velo::PvssTell1Names","PvssTell1Names");

  return; 
}

// ===========================================================================
// Book Histograms
// ============================================================================
void Velo::Monitoring::NoiseMon::bookHistograms   () {
  // loop over all all tools (information sources) 
  unsigned int noiseCalculator = 0;
  for ( Tools::const_iterator it = m_noises.begin() ; 
      m_noises.end() != it ; ++it, ++noiseCalculator )
  {
    // loop over all VELO sensors
    std::vector<DeVeloSensor*>::const_iterator si;
    for ( si = m_veloDet->sensorsBegin(); si != m_veloDet->sensorsEnd(); ++si ) {
      const DeVeloSensor* sensor = *si;
      unsigned int sensorNumber = sensor->sensorNumber();
      
      // construct the proper IDs for the histograms:
      boost::format fmt ( "%s/TELL1_%03d" ) ;
      fmt % strippedContainerName ( it->first ) % sensorNumber;
      const std::string fmtstr = fmt.str() ;

      boost::format sensorInfo(", sensor %d/%s, %s");
      sensorInfo % sensorNumber % m_pvssNames->pvssName(sensorNumber) % (sensor->isR() ? "R" : ( sensor->isPhi() ? "Phi" : "Pile-up")) ;      

      const GaudiAlg::HistoID rms_chipch = fmtstr + "/RMSNoise_vs_ChipChannel" ;      
      m_rmsChipChannelHistos[noiseCalculator][sensorNumber] =
        book(rms_chipch, "RMS noise vs chipch"+sensorInfo.str(), -0.5, 2048 - 0.5, 2048);
      
      const GaudiAlg::HistoID rms_strip = fmtstr + "/RMSNoise_vs_Strip" ;      
      m_rmsStripHistos[noiseCalculator][sensorNumber] =
        book(rms_strip, "RMS noise vs strip"+sensorInfo.str(), -0.5, 2048 - 0.5, 2048);
    }
    boost::format fmtall ( "%s/Average" ) ;
    fmtall % strippedContainerName ( it->first );
    const std::string fmtallstr = fmtall.str() ;

    const GaudiAlg::HistoID rms_average = fmtallstr + "/RMSNoise_vs_Sensor" ;
    m_rmsAverage[noiseCalculator] =
      book(rms_average, "RMS noise vs sensor", -0.5, 109.5, 110);
  
    const GaudiAlg::HistoID rms_averageR = fmtallstr + "/RMSNoise_vs_Station_R" ;
    m_rmsAverageR[noiseCalculator] =
      book(rms_averageR, "RMS noise vs station", -0.5, 25.5, 104);  

    const GaudiAlg::HistoID rms_averagePhi = fmtallstr + "/RMSNoise_vs_Station_Phi" ;
    m_rmsAveragePhi[noiseCalculator] =
      book(rms_averagePhi, "RMS noise vs station", -0.5, 25.5, 104);  

    const GaudiAlg::HistoID rms_noiseR = fmtallstr + "/RMSNoise_R" ;
    m_rmsNoiseR[noiseCalculator] =
      book(rms_noiseR, "RMS noise (R sensors)", 0.1, 5.1, 50);  

    const GaudiAlg::HistoID rms_noisePhi = fmtallstr + "/RMSNoise_Phi" ;
    m_rmsNoisePhi[noiseCalculator] =
      book(rms_noisePhi, "RMS noise (Phi sensors)", 0.1, 5.1, 50);  

    const GaudiAlg::HistoID rms_noiselink = fmtallstr + "/RMSNoise_vs_Link" ;
    m_rmsNoiseLink[noiseCalculator] =
      book2D(rms_noiselink, "RMS noise vs link", -0.5, 109.5, 110, -0.5, 63.5, 64);  

    const GaudiAlg::HistoID sensor_stationR = fmtallstr + "/Sensor_vs_Station_R" ;
    m_sensorStationR[noiseCalculator] =
      book2D(sensor_stationR, "Sensor vs station", -0.5, 25.5, 416,-5.0,5.0,200);  

    const GaudiAlg::HistoID sensor_stationPhi = fmtallstr + "/Sensor_vs_Station_Phi" ;
    m_sensorStationPhi[noiseCalculator] =
      book2D(sensor_stationPhi, "Sensor vs station", -0.5, 25.5, 416,-5.0,5.0,200);  

    double hnmax=4.; 
    int hnbins=50; 
    double xlow=0.;

    boost::format fmtheader ( "%s/Header" ) ;
    fmtheader % strippedContainerName ( it->first );
    const std::string fmtheaderstr = fmtheader.str() ;

    const GaudiAlg::HistoID rms_headernoiseallsensors = fmtheaderstr + "/HeaderNoise_allSensors" ;
    m_headerNoiseAllSensors[noiseCalculator] =
      book(rms_headernoiseallsensors,     "Normalised Header noise for all sensors", xlow, hnmax+((hnmax-xlow)/((double)(hnbins))), hnbins);

    const GaudiAlg::HistoID rms_noisyheaderlikspersensors = fmtheaderstr + "/NoisyHeaderLinksPerSensor" ;
    m_noisyHeaderLinksPerSensor[noiseCalculator] =
      book(rms_noisyheaderlikspersensors,  "NoisyHeaderLinksPerSensor",-0.5,109.5, 110);
  }

  return;
}

bool Velo::Monitoring::NoiseMon::isRorPhi(unsigned int sensorNumber)
{
  if ( sensorNumber > 63 ) { return 0;}
  else { return 1; }
}

int Velo::Monitoring::NoiseMon::isAorC(unsigned int sensorNumber)
{
  if( sensorNumber%2==0){return 1;}
  else{return 0;}
}

int Velo::Monitoring::NoiseMon::getStationNumber(unsigned int sensorNumber)
{
  int station_number=0;
  if(sensorNumber<42){
    station_number=sensorNumber/2+1;
  }
  else if(sensorNumber>63){
    station_number=(sensorNumber-64)/2+1;
  }
  else{station_number=-1;}

  //skip station 17 and 18
  if(station_number>16){
    station_number=station_number+2;
  }

  //skip station 20 and 21
  if(station_number>19){
    station_number=station_number+2;
  }
  return station_number;
}

bool Velo::Monitoring::NoiseMon::isDownStream(unsigned int sensorNumber){

  if(isAorC(sensorNumber)){
    if(!isRorPhi(sensorNumber)){
      if(sensorNumber%4==0){return 1;}
      else{return 0;}
    }
    else if(isRorPhi(sensorNumber)){
      if(sensorNumber%4==2){return 1;}
      else{return 0;}
    }
    else{return 0;}
  }
  
  else {
    if(!isRorPhi(sensorNumber)){
      if(sensorNumber%4==3){return 1;}
      else{return 0;}
    }
    else if(isRorPhi(sensorNumber)){
      if(sensorNumber%4==1){return 1;}
      else{return 0;}
    }
    else{return 0;}
  }
}

// ============================================================================
// initialize the algorithm 
// ============================================================================
StatusCode Velo::Monitoring::NoiseMon::initialize   () {
  StatusCode sc=GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm  
  if ( propsPrint() ) printProps();
  debug() << "==> Initialize" << endmsg;
  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );  

  retrieveTools();
  bookHistograms();
  
  return StatusCode::SUCCESS;
}

// ============================================================================
// finalize the algotithm 
// ============================================================================
StatusCode Velo::Monitoring::NoiseMon::finalize   () 
{
  
  unsigned int noiseCalculator = 0;

  //auxiliary variables for noise per sensor calculations
  double noise = 0.0;
  double aux = 0.0;
  int channels_cnt = 0;
  //... and per link
  double noise_link = 0.0;
  int link_cnt = 0;
  int link = 0;
  
  
  for ( Tools::const_iterator it = m_noises.begin() ;
      m_noises.end() != it ; ++it, ++noiseCalculator )
  {
    std::vector<DeVeloSensor*>::const_iterator si;
    for ( si = m_veloDet->sensorsBegin(); si != m_veloDet->sensorsEnd(); ++si ) {
      const DeVeloSensor* sensor = *si;
      unsigned int sensorNumber = sensor->sensorNumber();
      //reset values for each sensor
      noise = 0.0;
      channels_cnt = 0;
      noise_link = 0.0;
      link = 0;
      link_cnt = 0;
      for(int i=0; i<2048; i++){
	if(i%32!=0){
	  aux=m_rmsChipChannelHistos[noiseCalculator][sensorNumber]->binHeight(i);
          noise+=aux;
	  channels_cnt++;
          noise_link+=aux;
          link_cnt++;
          if(i/32>link){
            m_rmsNoiseLink[noiseCalculator]->fill(sensorNumber,link,noise_link/link_cnt);       
	    link++;
            noise_link=0.0;
	    link_cnt = 0;
          }
	}
      }
      m_rmsNoiseLink[noiseCalculator]->fill(sensorNumber,link,noise_link/link_cnt); 
      noise/=channels_cnt;
      m_rmsAverage[noiseCalculator]->fill(sensorNumber,noise);
      int station = getStationNumber(sensorNumber);
      double offset = 0.25*(!isDownStream(sensorNumber));
      double text_offset = -0.05+0.35*(!isDownStream(sensorNumber));
      double parity = pow(-1.0,1+isAorC(sensorNumber));
      if(isRorPhi(sensorNumber)){
        m_rmsAverageR[noiseCalculator]->fill(station-offset,parity*noise);
        m_sensorStationR[noiseCalculator]->fill(station-text_offset, parity*(noise+0.65)-0.15,sensorNumber);
        m_rmsNoiseR[noiseCalculator]->fill(noise);
      }else{
        m_rmsAveragePhi[noiseCalculator]->fill(station-offset,parity*noise);
        m_sensorStationPhi[noiseCalculator]->fill(station-text_offset, parity*(noise+0.65)-0.15,sensorNumber);
        m_rmsNoisePhi[noiseCalculator]->fill(noise);
      }

      //Header info
      double hnmax=4.;
      double header_max_cut=2.0;
      int noisyheader=0;
      bool normalized=1; //set=1 to normalize header noise with average noise of link

      for(int link=0;link<64;link++){
        double header_noise=m_rmsChipChannelHistos[noiseCalculator][sensorNumber]->binHeight(32*link);
        if(normalized){
           int first_ch = 1;
           double link_noise=0;
           int first_chip_channel=32*link+first_ch;
           int last_chip_channel=32*link+32;
           for(int ch=first_chip_channel;ch<last_chip_channel;ch++){
              link_noise=link_noise+m_rmsChipChannelHistos[noiseCalculator][sensorNumber]->binHeight(ch);
           }
           link_noise=link_noise/(32-first_ch);
           header_noise=header_noise/link_noise;
        }
        if (header_noise>header_max_cut) noisyheader++;
        double header_fill=header_noise;
        if (header_fill>hnmax) header_fill=hnmax;
        m_headerNoiseAllSensors[noiseCalculator]->fill(header_fill,1.);
      }
      m_noisyHeaderLinksPerSensor[noiseCalculator]->fill(sensorNumber,noisyheader);
    }
  }
  //clear the container of tools
  m_noises.clear() ;
  m_event = 0;
  // finalize the base class 
  return GaudiHistoAlg::finalize() ;
}
// ============================================================================
// Execute the algorithm 
// ============================================================================
StatusCode Velo::Monitoring::NoiseMon::execute    () 
{  
  //Get pedestal convergence limit from the emulator process info. This can't be done
  //from initialize() so we use the m_isInit flag to only run it the first event.

  if(m_isInit==0){
    VeloProcessInfos* procInfos;  
    if(!exist<VeloProcessInfos>(VeloProcessInfoLocation::Default)){      
      debug() << " ==> No init info available in this event. Waiting." << endmsg;
    }else{
      procInfos=get<VeloProcessInfos>(VeloProcessInfoLocation::Default);
      VeloProcessInfos::const_iterator procIt=procInfos->begin();
      if((*procIt)->convLimit()==0){
        debug()<< "Convergence Limit not set!" <<endmsg;
        m_convergenceLimit=(*procIt)->convLimit();
      }else{
        m_convergenceLimit=(*procIt)->convLimit();
      }    
      debug() << "Convergence limit set to " << m_convergenceLimit << endmsg;
      m_isInit=1;
    }
  }


  StatusCode sc;

  if ( m_isInit && m_event>=m_convergenceLimit ) { 
    makeThePlots(); 
  } 

  ++m_event;

  return StatusCode::SUCCESS ;
} 
// ============================================================================
/// Declaration of the Algorithm Factory
DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring,NoiseMon)
  // ============================================================================
  // The END
  // ============================================================================

