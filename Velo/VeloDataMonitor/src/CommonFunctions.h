//Header file with functions that are handy to access for different algorithms in the VELO monitoring.

#include "VeloEvent/VeloProcessInfo.h"


namespace{

 /// replace all symbols in the string 
  inline void replaceAll 
  ( std::string& a , const char c1 , const char c2 = '_') 
  {
    std::string::size_type pos = a.find ( c1 ) ;
    while ( std::string::npos != pos ) { a [ pos ] = c2 ; pos = a.find ( c1 ) ; }
  }
  /// construct the "stripped" name form valid TES location 
  inline std::string strippedContainerName ( std::string location ) 
  {
    std::string::size_type pos = location.find ( "Raw/Velo/" ) ;
    if ( std::string::npos != pos ) 
      { location = std::string ( location , pos + 9 ) ; }
    replaceAll ( location , '.' ) ;
    replaceAll ( location , ':' ) ;
    replaceAll ( location , '/' ) ;
    return location ;

  }

  inline bool ExcludeTELL1(std::vector<int> m_chosenTell1s, int TELL1_number){   

    if(m_chosenTell1s.size()>0 && 
       find(m_chosenTell1s.begin(), m_chosenTell1s.end(), TELL1_number)==m_chosenTell1s.end())
      { return true ; }
    else
      return false;
    
  }          
  
  
  
}
