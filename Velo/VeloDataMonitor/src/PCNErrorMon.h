/**
 * @file   PCNErrorMon.h
 * @author Suvayu Ali <Suvayu.Ali@cern.ch>
 * @date   Tue Apr 24 15:20:12 2012
 *
 * @brief  Definition for the PCNError and PCNErrorMap classes.
 *
 *         As the name indicates, the PCNError class defines a PCN
 *         error in terms of the expected (correct) Pipeline Column
 *         Number and the exclusive OR (XOR) of the faulty PCN value
 *         with the correct PCN.
 *
 *         The PCNErrorMap class defines a map of the faulty bits for
 *         each Beetle chip reporting PCN errors. The maps are
 *         represented as 2-dimensional histograms.
 *
 */

#ifndef __PCNERRORMON_H
#define __PCNERRORMON_H

#include <string>
#include <vector>
#include <bitset>
#include <map>

#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/IMonitorSvc.h"
#include "GaudiKernel/AlgFactory.h"

#include "Event/RawEvent.h"
#include "Event/RawBank.h"
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "VeloEvent/EvtInfo.h"
#include "VeloEvent/VeloErrorBank.h"

#include "SiDAQ/SiClusterWord.h"
#include "SiDAQ/SiADCBankTraits.h"
#include "SiDAQ/SiRawBankDecoder.h"

#include <TH1D.h>
#include <TH2D.h>


namespace Velo {
  namespace Monitoring {
    class PCNErrorMon : public GaudiHistoAlg {

    public:

      PCNErrorMon( const std::string& name, ISvcLocator* pSvcLocator );
      virtual ~PCNErrorMon();

      virtual StatusCode initialize() override;
      virtual StatusCode execute() override;
      virtual StatusCode finalize() override;

    protected:

      /**
       * Read raw event
       *
       * @param rawevent
       */
      void readRawEvent(LHCb::RawEvent* rawevent);

      /**
       * Read error bank.
       *
       * @param banks
       */
      void readErrorBanks(VeloErrorBanks* banks);

      /**
       * Check if FEM PCNs are in sync with each others.
       *
       * @param vFEMPCNbyTELL1 Vector with FEM PCNs by Tell1 ids
       */
      void checkFEMPCNsync(const VeloTELL1::dataVec& vFEMPCNbyTELL1);

      /**
       * Check if Beetle PCNs are in sync with the FEM PCN.
       *
       * @param numberOfTELL1 Tell1 id
       * @param FEMPCN FEM PCN
       * @param FPGAVec Vector with PP-FPGA numbers
       * @param PCNVec Vector with all PCNs per sensor (Tell1)
       */
      void checkBeetlePCNsync(unsigned int numberOfTELL1,
			      unsigned int FEMPCN,
			      VeloTELL1::dataVec& FPGAVec,
			      VeloTELL1::dataVec& PCNVec);

      /**
       * Format summary log
       *
       */
      void formatLog();

      /**
       * Print summary
       *
       */
      void summary();

    private:

      // FIXME: copied from ErrorMon (up to VeloRawBankDecoder)

      // Private s for raw bank decoder instantiation.
      // TODO: To put them here is a temporary solution. They should be
      // exported by another package to avoid code duplication.

      /** Velo raw bank bit pattern
       *
       *  This class is a type parameter for the SiClusterWord
       *  template class that allows to share bit-packing code
       *  with the ST software.
       *  The bit pattern is an implementation of the Velo raw
       *  bank specification in EDMS 637676, Version 2.0 as of
       *  December 1, 2005.
       *
       * @see SiClusterWord
       *
       * @author Kurt Rinnert
       * @date   2006-02-02
       */
      class VeloBitsPolicy {
      public:

	typedef SiDAQ::adc_only_bank_tag adc_bank_type;

	enum bits {
	  positionBits        =  0,
	  channelBits         =  3,
	  sizeBits            = 14,
	  thresBits           = 15,
	  interStripPrecision =  3
	};

	enum mask {
	  positionMask = 0x0007,
	  channelMask  = 0x3ff8,
	  sizeMask     = 0x4000,
	  thresMask    = 0x8000
	};
      };

      typedef SiClusterWord<VeloBitsPolicy> VeloClusterWord;
      typedef SiRawBankDecoder<VeloClusterWord> VeloRawBankDecoder;


      /// PCNError defines PCN errors from PCN and XOR bits
      class PCNError {
      public:

	/// Define different bit types.
	enum _PCN_T {
	  kPCN,			/**< Correct PCN */
	  kXOR,			/**< Exclusive OR of bad PCN with correct PCN */
	  kBAD			/**< Incorrect PCN */
	};

	typedef std::bitset<8> eightbits; /**< Typedef for 8 bit bitset. */

	/**
	 * Constructor to initialise from unsigned integers
	 *
	 * @param pcn Correct PCN
	 * @param pcnxor Exclusive OR with correct PCN
	 */
	PCNError(unsigned int pcn, unsigned int pcnxor);

	/**
	 * Constructor to initialise from bit strings
	 *
	 * @param pcnbits Correct PCN bit string
	 * @param xorbits Exclusive OR bit string
	 */
	PCNError(std::string pcnbits, std::string xorbits);
	~PCNError();

	/**
	 * Get bitset for given bit type
	 *
	 * @param bittype Bit type to return
	 *
	 * @return Returned bitset
	 */
	eightbits getBits(_PCN_T bittype=kPCN) const;

	/**
	 * Set PCN bits from unsigned integers
	 *
	 * @param pcn Correct PCN
	 * @param pcnxor Exclusive OR with correct PCN
	 */
	void      setBits(unsigned int pcn, unsigned int pcnxor);

	/**
	 * Set PCN bits from bit strings
	 *
	 * @param pcnbits Correct PCN bit string
	 * @param xorbits Exclusive OR bit string
	 */
	void      setBits(std::string pcnbits, std::string xorbits);

      private:

	unsigned int _pcn;		/**< Correct PCN */
	unsigned int _xor;		/**< Exclusive OR with correct PCN */
      };


      typedef std::map<unsigned int, TH2D*> TH2DMap; /**< Typedef for a histogram map. */
      typedef std::map<unsigned int, long>  longMap; /**< Typedef for a long map. */

      /**
       * Fill the underlying histograms describing the PCN error map as
       * per TELL1 board ids, Beetle numbers and reported PCN error. The
       * bins are filled in reverse, MSB to LSB. This is done to
       * correspond with how we would write a binary bit on paper.
       *
       * @param tell1id TELL1 board id
       * @param beetle Beetle number
       * @param err PCN error
       */
      void Fill(unsigned int tell1id, unsigned int beetle, PCNError err);

      // configurables
      bool doLog;
      bool excludePileup;
      unsigned int MAX_TELL1ID; // depends on excludePileup
      std::string m_ErrorBankLoc;
      std::string m_RawEventLocation;
      std::vector<unsigned int> m_expectedTell1Ids;
      std::vector<unsigned int> m_tell1Ids;

      // event identifiers
      unsigned int runNo;
      unsigned long long eventID;

      // FEM PCN synchronisation
      std::vector<unsigned int> badfsync_TELL1s;
      std::vector<unsigned int> badfsync_expPCN;
      std::vector<PCNError::eightbits> badfsync_badbits;

      // Beetle PCN synchronisation
      std::vector<unsigned int> badbsync_TELL1s;
      std::vector<unsigned int> badbsync_expPCN;
      std::vector<std::vector<unsigned int> > badnBeetles;
      std::vector<std::vector<PCNError::eightbits> > badPCNbits;

      // for error summary tables
      unsigned long nEvts;
      unsigned long nFEMPCNoffsync;
      unsigned long nBeetlePCNoffsync;
      std::vector<std::string> error_summary1;
      std::vector<std::string> error_summary2;

      // histograms
      TH1D* hFEMPCNsync;                /**< Histogram recording FEM PCN desync frequency */

      // counters
      longMap ErrCounter;		/**< A sparse PCN error map. */

      // PCN error maps
      TH2D*   hBeetleMap;		/**< PCN error map histogram for all Beetle chips. */
      TH2DMap hperBeetleBitMap;		/**< Map of faulty bits for all Beetles with PCN error. */
    };
  }
}

#endif	// __PCNERRORMON_H
