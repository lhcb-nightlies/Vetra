// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/AlgFactory.h" 
// ============================================================================

// from Event model
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/EvtInfo.h"

//velo
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

// local
#include "CommonFunctions.h"
#include "VetraKernel/INoiseEvaluator.h"
#include "VetraKernel/TELL1Noise.h"
// AIDA 
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"

// from TELL1 Kernel:
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"
#include "Tell1Kernel/VeloDecodeConf.h" 

// Boost 
// ============================================================================
#include "boost/format.hpp"
// ============================================================================

//-----------------------------------------------------------------------------
// Class : Velo::Monitoring::GainMon
//
// 2009-04-20 Grant McGregor, grant.duncan.mcgregor@cern.ch
// Formerly ExtractHeaders.cpp. Plots headerr values for Full Header Swing (FHS) and 
// other quantities useful for gain monitoring 
//   
//-----------------------------------------------------------------------------
using namespace LHCb;
using namespace VeloTELL1;
namespace Velo
{
  namespace Monitoring
  {
    class GainMon : public GaudiHistoAlg {
	  public:
      /// Standard constructor
      GainMon( const std::string& name, ISvcLocator* pSvcLocator );
      virtual ~GainMon( ); ///< Destructor
      virtual StatusCode initialize() override;    ///< Algorithm initialization
      virtual StatusCode execute   () override;    ///< Algorithm execution
      virtual StatusCode finalize  () override;    ///< Algorithm finalization
      StatusCode plotHeaders();
      
    private  :
      int  m_evtNumber;
      int  m_NForAvgCalc;
      int m_maxEvts;

      
      int m_refresh;
      DeVelo* m_veloDet;
      std::vector<std::string>  m_locations;
      EvtInfos*       m_evtInfos;
      bool m_plotHeaders;
      bool m_plotFHS;
      bool m_plotHeadersHighLow;
      bool m_isGainScan;
      int m_nEvtGainScan;
      int m_nStepsGainScan;
      int m_maxToPlot;
      int m_minToPlot;
      float HLPlotMin;
      float HHPlotMax;
      bool m_isRR; 
      int step;
      std::vector<int>  m_chosenTell1s;
      std::vector<int>  m_linkHeaders; 
      
      //Declare arrays to average and count the headers:
      float HeaderAvg[106][64];
      float HeaderHighAvg[106][64];
      float HeaderLowAvg[106][64];
      float HeaderHighAvgH3[106][64];
      float HeaderHighAvgH4[106][64];
      float HeaderLowAvgH3[106][64];
      float HeaderLowAvgH4[106][64];
      float HeaderMax[106][64];
      float HeaderMin[106][64];
      int NHeaderHighAvg[106][64];
      int NHeaderLowAvg[106][64];
      int NHeaderHighAvgH3[106][64];
      int NHeaderLowAvgH3[106][64];   
      int NHeaderHighAvgH4[106][64];
      int NHeaderLowAvgH4[106][64];
      int NEventsToAvg[106][64];
      int ADCTooHigh[106][64];
      int ADCTooLow[106][64];
      float GSHeaderAvg[106][64][25];
      float GSHeaderHighAvg[106][64][25];
      float GSHeaderLowAvg[106][64][25];
      int GSNHeaderHighAvg[106][64][25];
      int GSNHeaderLowAvg[106][64][25];
      int GSNEventsToAvg[106][64][25];
      float fpgaHPlotMin[106][4];
      float fpgaLPlotMax[106][4];
      float fpgaHPlotMax[106][4];
      float fpgaLPlotMin[106][4];
   
      std::map<unsigned int, std::vector<IHistogram1D*> > m_histsHeaders;
      std::map<unsigned int, std::vector<IHistogram1D*> > m_histsHeadersLow;
      std::map<unsigned int, std::vector<IHistogram1D*> > m_histsHeadersHigh;
      std::map<unsigned int, std::vector<IHistogram1D*> > m_histsHeadersLowFPGA;
      std::map<unsigned int, std::vector<IHistogram1D*> > m_histsHeadersHighFPGA;


      void bookHistograms();
      void bookHistogramsHighLow(std::vector<IHistogram1D*>& hists
          , const unsigned int sensor
          , const unsigned int link
          , const std::string& hl
          , const unsigned int min
          , const unsigned int max); 

      void bookHistogramsFPGA(std::vector<IHistogram1D*>& hists
          , const unsigned int sensor
          , const unsigned int fpga
          , const std::string& hl
          , const unsigned int min
          , const unsigned int max); 

      //Returns a vector with the link ADCs.
      void getLink(VeloTELL1::ALinkPair begEnd) {   
        std::vector<int>::const_iterator iT; //iterator to vector of unysigned int.
        m_linkHeaders.clear();
        
        //Loop over ADC values in link
        for(iT=begEnd.first; iT!=begEnd.second; ++iT){    
          m_linkHeaders.push_back(*iT); //Push back ADC value to vector
        }
        return;
      }
    };
  }
}


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Velo::Monitoring::GainMon::GainMon( const std::string& name,
	ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
    ,  m_evtNumber(0)
    ,  m_NForAvgCalc(1000)     //This is the number of events the code uses to calculate the average header value.
    ,  m_refresh (5000)
    ,  m_veloDet ( 0 )
    ,  m_locations ()
    ,  m_nEvtGainScan(2000)
    ,  m_nStepsGainScan(23)
    ,  m_maxToPlot (750)      //Allows faulty header values to be excluded. Only headers below this value are plotted.
    ,  m_minToPlot (400)        //Allows faulty header values to be excluded. Only headers above this value are plotted.
{
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::ADCs             ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::PedSubADCs       ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::FIRCorrectedADCs ) ;
  m_locations.push_back ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ) ;
  declareProperty( "Containers" ,   m_locations  ,  "The list of TES locations to be monitored") ;
  declareProperty( "NForAvgCalc",  m_NForAvgCalc);
  declareProperty( "RefreshRate",          m_refresh);
  declareProperty( "PlotHeaders",         m_plotHeaders=true); //Plot headers for each link -- makes lots of plots, turn off if not needed
  declareProperty( "PlotFHS",         m_plotFHS=true);     //Makes 2D FHS summary plots
  declareProperty( "PlotHeadersHighLow",    m_plotHeadersHighLow=true);  //Plots high/low header histograms
  declareProperty( "isGainScan", m_isGainScan=false);  //Option for running on gain scan data
  declareProperty( "nEvtGainScan", m_nEvtGainScan);    //If running on gain scan: number of events per step
  declareProperty( "nStepsGainScan", m_nStepsGainScan); //If running on gain scan: number of steps
  declareProperty( "MaxToPlot" ,  m_maxToPlot);
  declareProperty( "MinToPlot" ,  m_minToPlot);
  declareProperty( "MaxEvts",       m_maxEvts = -1);
  declareProperty( "TELL1s",                        m_chosenTell1s                       );
  declareProperty( "isRR",                        m_isRR=false                       );

  
  setProperty ( "PropertiesPrint", true ).ignore();  
}
//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::GainMon::~GainMon() {

} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::GainMon::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( propsPrint() ) printProps();

  debug() << "==> Initialize" << endmsg;
  setHistoTopDir( "Velo/" );

  bookHistograms();

  //Declare some arrays if using Gain Scan:
  if(m_isGainScan){
    for(int Tell1=0; Tell1<106; Tell1++)  {
      for(int link=0; link<64; link++){//Set GS arrays to zero
        for(int STEP =0; STEP<m_nStepsGainScan; STEP++){
          GSHeaderAvg[Tell1][link][STEP]=0;
          GSHeaderHighAvg[Tell1][link][STEP]=0;
          GSHeaderLowAvg[Tell1][link][STEP]=0;
          GSNHeaderHighAvg[Tell1][link][STEP]=0;
          GSNHeaderLowAvg[Tell1][link][STEP]=0;
          GSNEventsToAvg[Tell1][link][STEP]=0;
        }
      }
      for(int fpga=0; fpga<4; fpga++){
        fpgaHPlotMin[Tell1][fpga]=0;
        fpgaLPlotMax[Tell1][fpga]=0;
        fpgaHPlotMax[Tell1][fpga]=0;
        fpgaLPlotMin[Tell1][fpga]=0;
      }
    }
  }//end m_isGainScan loop


  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );
  if(m_veloDet == 0) Error("Can't find detector element. Exiting with Failure",StatusCode::FAILURE);
  for(int Tell1=0; Tell1<106; Tell1++)
  {
    for(int link=0; link<64; link++){//Set averaging arrays to zero
      HeaderAvg[Tell1][link]=0;
      HeaderHighAvg[Tell1][link]=0;
      HeaderLowAvg[Tell1][link]=0;
      HeaderHighAvgH3[Tell1][link]=0;
      HeaderHighAvgH4[Tell1][link]=0;
      HeaderLowAvgH3[Tell1][link]=0;
      HeaderLowAvgH4[Tell1][link]=0;
      HeaderMax[Tell1][link]=0;
      HeaderMin[Tell1][link]=0;
      NHeaderHighAvg[Tell1][link]=0;
      NHeaderLowAvg[Tell1][link]=0;
      NHeaderHighAvgH3[Tell1][link]=0; 
      NHeaderLowAvgH3[Tell1][link]=0; 
      NHeaderHighAvgH4[Tell1][link]=0; 
      NHeaderLowAvgH4[Tell1][link]=0; 
      NEventsToAvg[Tell1][link]=0;
      ADCTooHigh[Tell1][link]=0;
      ADCTooLow[Tell1][link]=0;

    }
  }


  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::Monitoring::GainMon::finalize() {
  debug() << "==> Finalize" << endmsg;
  if(m_plotFHS){
    for(int Tell1=0; Tell1<106; Tell1++){
      for(int link=0; link<64; link++){//Calculate average high and low headers <HH> and <HL>
        NHeaderHighAvg[Tell1][link]=NHeaderHighAvgH3[Tell1][link]+NHeaderHighAvgH4[Tell1][link];
        NHeaderLowAvg[Tell1][link]=NHeaderLowAvgH3[Tell1][link]+NHeaderLowAvgH4[Tell1][link];
        if(NHeaderHighAvg[Tell1][link]!=0){
          HeaderHighAvg[Tell1][link]=(HeaderHighAvgH3[Tell1][link]+HeaderHighAvgH4[Tell1][link]) /(float)NHeaderHighAvg[Tell1][link];
        }
        if(NHeaderLowAvg[Tell1][link]!=0){  
          HeaderLowAvg[Tell1][link]=(HeaderLowAvgH3[Tell1][link]+HeaderLowAvgH4[Tell1][link])/(float)NHeaderLowAvg[Tell1][link];
        }
      }
      for(int link=0; link<64; link++){//Calculate average high and low headers <HH> and <HL> for header 3
        if(NHeaderHighAvgH3[Tell1][link]!=0){
          HeaderHighAvgH3[Tell1][link]=HeaderHighAvgH3[Tell1][link]/(float)NHeaderHighAvgH3[Tell1][link];
        }
        if(NHeaderLowAvgH3[Tell1][link]!=0){  
          HeaderLowAvgH3[Tell1][link]=HeaderLowAvgH3[Tell1][link]/(float)NHeaderLowAvgH3[Tell1][link];
        }
      }
      for(int link=0; link<64; link++){//Calculate average high and low headers <HH> and <HL> for header 4
        if(NHeaderHighAvgH4[Tell1][link]!=0){
          HeaderHighAvgH4[Tell1][link]=HeaderHighAvgH4[Tell1][link]/(float)NHeaderHighAvgH4[Tell1][link];
        }
        if(NHeaderLowAvgH4[Tell1][link]!=0){  
          HeaderLowAvgH4[Tell1][link]=HeaderLowAvgH4[Tell1][link]/(float)NHeaderLowAvgH4[Tell1][link];
        }
      }

    }

    VeloTELL1Datas* m_headerContainer;
    if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers )){
      info()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::Headers  <<endmsg;
      return StatusCode::FAILURE;
    }else{
      m_headerContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers );
    }

    VeloTELL1Datas::const_iterator headerIt = m_headerContainer->begin();
    for(int Tell1=0; Tell1<106; Tell1++){
      if(Tell1<=41 || Tell1>=64){//Only use real sensors
        int TELL1_number=Tell1;
        if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue;
        boost::format FHStitle ( "FHS/FHS_Sensor%d" );
        FHStitle % TELL1_number;
        AIDA::IProfile1D* FHShisto = bookProfile1D(FHStitle.str()  , FHStitle.str()  , -0.5, 64-0.5,  64);

        for(int link = 0; link<NumberOfALinks; ++link){
          if(NHeaderLowAvg[Tell1][link]!=0){  
            if( (HeaderHighAvg[TELL1_number][link]-HeaderLowAvg[TELL1_number][link])>=0){
              plot2D( TELL1_number,link, "2DSummary_FHS", "Summary of FHS for all sensors", 0, 106, 0, 64, 106, 64, 
                  (HeaderHighAvg[TELL1_number][link]-HeaderLowAvg[TELL1_number][link]));
            }
            if( (HeaderHighAvgH3[TELL1_number][link]-HeaderLowAvgH3[TELL1_number][link])>=0){
              plot2D( TELL1_number,link, "2DSummary_FHS_H3", "Summary of H3 FHS for all sensors", 0, 106, 0, 64, 106, 64, 
                  (HeaderHighAvgH3[TELL1_number][link]-HeaderLowAvgH3[TELL1_number][link]));
            }
            if(  (HeaderHighAvgH4[TELL1_number][link]-HeaderLowAvgH4[TELL1_number][link]) >=0){
              plot2D( TELL1_number,link, "2DSummary_FHS_H4", "Summary of H4 FHS for all sensors", 0, 106, 0, 64, 106, 64, 
                  (HeaderHighAvgH4[TELL1_number][link]-HeaderLowAvgH4[TELL1_number][link]));
            }
            plot2D( TELL1_number,link, "2DSummary_Avg", "Summary of header average for all sensors", 0, 106, 0, 64, 106, 64, 
                0.5*(HeaderHighAvg[TELL1_number][link]+HeaderLowAvg[TELL1_number][link]));       
            plot2D( TELL1_number,link, "2DSummary_HH", "Summary of <HH> for all sensors", 0, 106, 0, 64, 106, 64, 
                HeaderHighAvg[TELL1_number][link]); 
            plot2D( TELL1_number,link, "2DSummary_HL", "Summary of <HL> for all sensors", 0, 106, 0, 64, 106, 64, 
                HeaderLowAvg[TELL1_number][link]);
            plot2D( TELL1_number,link, "ADCTooHigh2D", "Summary of high ADC counts", 0, 106, 0, 64, 106, 64,
                ADCTooHigh[TELL1_number][link]);
            plot2D( TELL1_number,link, "ADCTooLow2D", "Summary of low ADC counts", 0, 106, 0, 64, 106, 64,
                ADCTooLow[TELL1_number][link]);       

            if( 0 != FHShisto ) { FHShisto->fill ( link, (HeaderHighAvg[TELL1_number][link]-HeaderLowAvg[TELL1_number][link] )) ; } 
          }
        }	
      }
    }//End of FHS plot loop


    //Now plot FHS for GS if option set:
    if(m_isGainScan){
      //Calculate average gain scan high and low headers <HH> and <HL>
      for(int Tell1=0; Tell1<106; Tell1++){
        for(int link = 0; link<NumberOfALinks; ++link){
          for(int STEP = 0; STEP<m_nStepsGainScan; STEP++){
            if(GSNHeaderHighAvg[Tell1][link][STEP]!=0){
              GSHeaderHighAvg[Tell1][link][STEP]=(GSHeaderHighAvg[Tell1][link][STEP]) /(float)GSNHeaderHighAvg[Tell1][link][STEP];
            }
            if(GSNHeaderLowAvg[Tell1][link][STEP]!=0){
              GSHeaderLowAvg[Tell1][link][STEP]=(GSHeaderLowAvg[Tell1][link][STEP] )/(float)GSNHeaderLowAvg[Tell1][link][STEP];
            }
          }
        }
      }


      for(headerIt=m_headerContainer->begin();headerIt!=m_headerContainer->end();++headerIt){
        VeloTELL1Data* headerOneTELL1=(*headerIt);
        int TELL1_number=headerOneTELL1->key();
        if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue;
        for(int STEP=0; STEP<m_nStepsGainScan; STEP++){
          boost::format GStitle ( "GS/Step%02d" );
          GStitle % STEP;
          for(int link = 0; link<NumberOfALinks; ++link){
            plot2D( TELL1_number,link, GStitle.str()+"/GS_2DSummary_FHS", "Summary of GS FHS", 0, 106, 0, 64, 106, 64, 
                (GSHeaderHighAvg[TELL1_number][link][STEP]-GSHeaderLowAvg[TELL1_number][link][STEP]));
          }
        }
      }//End of GS FHS plot loop
    }
  }

  debug() << "==> End Finalize" << endmsg;  
  return GaudiHistoAlg::finalize();  // must be called after all other actions
}
//=============================================================================
// Main execution
//=============================================================================S
StatusCode Velo::Monitoring::GainMon::execute() {


  if(!m_isRR)  m_evtNumber++;   //If not RR data, then increment normally... 
  if(!exist<EvtInfos>( EvtInfoLocation::Default  )){
    debug()<< " ==> There is no Event info in the data. QUITTING. " <<endmsg;
    return ( StatusCode::SUCCESS );
  }else{
    m_evtInfos       = get<EvtInfos>      ( EvtInfoLocation::Default       );
  }

  debug() << "==> Execute" << endmsg;  
  if(m_evtNumber < m_maxEvts || m_maxEvts == -1){
    plotHeaders();  
  }

  return StatusCode::SUCCESS;
}
//==============================================================================================================
//==============================================================================================================
void Velo::Monitoring::GainMon::bookHistograms(){

  for ( unsigned int s=0; s<42; ++s ) {
    for ( unsigned int i=0; i<2; ++i ) {
      unsigned int sensor = s+i*64;
      boost::format headerDir("Headers/Sensor_%03d/");
      headerDir % sensor;
      m_histsHeaders[sensor] = std::vector<IHistogram1D*>(64,static_cast<IHistogram1D*>(0));

      m_histsHeadersLow [sensor] = std::vector<IHistogram1D*>(64,static_cast<IHistogram1D*>(0));
      m_histsHeadersHigh[sensor] = std::vector<IHistogram1D*>(64,static_cast<IHistogram1D*>(0));

      m_histsHeadersLowFPGA [sensor] = std::vector<IHistogram1D*>(4,static_cast<IHistogram1D*>(0));
      m_histsHeadersHighFPGA[sensor] = std::vector<IHistogram1D*>(4,static_cast<IHistogram1D*>(0));

      for ( unsigned int link=0; link<64; ++link ) {
        if ( m_plotHeaders ) {
          boost::format HeaderAvgname("Headers_%d_%d");
          HeaderAvgname % sensor % link;
          boost::format HeaderAvgtitle ( "Headers for Sensor %d, link %d" );
          HeaderAvgtitle % sensor %  link;
          m_histsHeaders[sensor][link] = book1D(headerDir.str()+HeaderAvgname.str(), HeaderAvgtitle.str(), 0., 1100., 110);
        }        

      }
    }
  }

  return;
}


void Velo::Monitoring::GainMon::bookHistogramsHighLow(std::vector<IHistogram1D*>& hists
    , const unsigned int sensor
    , const unsigned int link
    , const std::string& hl
    , const unsigned int min
    , const unsigned int max) 
{
  boost::format high_lowdir("Headers_HighLow/Sensor_%03d/");
  high_lowdir % sensor;
  boost::format Headername("Header_%d_%d");
  Headername % sensor  % link;
  boost::format Headertitle ( "Headers for Sensor %d, link %d" );
  Headertitle % sensor %  link;
  hists[link] 
    = book1D(high_lowdir.str()+Headername.str()+"_"+hl, Headertitle.str()+"_"+hl, min, max, 100);

  return;  
}

void Velo::Monitoring::GainMon::bookHistogramsFPGA(std::vector<IHistogram1D*>& hists
    , const unsigned int sensor
    , const unsigned int fpga
    , const std::string& hl
    , const unsigned int min
    , const unsigned int max) 
{
  boost::format fpgadir("Headers_HighLow_ByFPGA/Sensor_%03d/");
  fpgadir % sensor;
  boost::format FPGAname("Header_FPGA_%d_%d");
  FPGAname % sensor  % fpga;
  boost::format FPGAtitle ( "Headers for Sensor %d, FPGA %d" );
  FPGAtitle % sensor %  fpga;
  hists[fpga] 
    = book1D(fpgadir.str()+FPGAname.str()+"_"+hl, FPGAtitle.str()+"_"+hl, min, max, 100);

  return;  
}

//==============================================================================================================
//==============================================================================================================
StatusCode Velo::Monitoring::GainMon::plotHeaders(){

  VeloTELL1Datas* m_headerContainer;

  if(!exist<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers )){
    debug()<< " ==> There is no bank at " << LHCb::VeloTELL1DataLocation::Headers  <<endmsg;
    return StatusCode::SUCCESS;
  }else{
    m_headerContainer = get<VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::Headers );
  }
  VeloTELL1Datas::const_iterator headerIt = m_headerContainer->begin();

  //For RR: only increment the counter when TELL1 number 2 is seen: 
  if(m_isRR){    
    for(headerIt=m_headerContainer->begin();headerIt!=m_headerContainer->end();++headerIt){      
      VeloTELL1Data* headerOneTELL1=(*headerIt);
      int TELL1_number=headerOneTELL1->key();
      if(TELL1_number==2){
        m_evtNumber++;    
      }
    }
  }

  //Loop over headers of first m_NForAvgCalc events to calculate link averages
  if(m_evtNumber<=m_NForAvgCalc){
    for(headerIt=m_headerContainer->begin();headerIt!=m_headerContainer->end();++headerIt){      
      VeloTELL1Data* headerOneTELL1=(*headerIt);
      int TELL1_number=headerOneTELL1->key();

      if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue;
      const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(TELL1_number);
      if(sensor == 0 ){
        error() << "Can't find a sensor associated with TELL1 " << TELL1_number << ". Aborting..." << endmsg;
        return StatusCode::FAILURE;
      }

      //Set up directory structure and histo names:
      for(int link = 0; link<NumberOfALinks; ++link){
        getLink( (*headerOneTELL1)[link]);
        //Flag number of events with particularly high/low ADC counts:
        if(m_linkHeaders[2]>600)ADCTooHigh[TELL1_number][link]++;
        if(m_linkHeaders[2]<400)ADCTooLow[TELL1_number][link]++;
        if(m_linkHeaders[3]>600)ADCTooHigh[TELL1_number][link]++;
        if(m_linkHeaders[3]<400)ADCTooLow[TELL1_number][link]++;

        //Average the headers to find splitting point between high/low headers. Find max/min to set histogram axes
        if(HeaderMin[TELL1_number][link]==0){
          HeaderMin[TELL1_number][link]=m_linkHeaders[2];
        }
        if(m_linkHeaders[2]>HeaderMax[TELL1_number][link] || m_linkHeaders[3]>HeaderMax[TELL1_number][link] ){
          HeaderMax[TELL1_number][link]=(m_linkHeaders[2]>m_linkHeaders[3]) ? m_linkHeaders[2] : m_linkHeaders[3];
        }
        if(m_linkHeaders[2]<HeaderMin[TELL1_number][link] || m_linkHeaders[3]<HeaderMin[TELL1_number][link] ){
          HeaderMin[TELL1_number][link]=(m_linkHeaders[2]<m_linkHeaders[3]) ? m_linkHeaders[2] : m_linkHeaders[3];
        }
        if(m_linkHeaders[2]<m_maxToPlot && m_linkHeaders[2]>m_minToPlot){
          NEventsToAvg[TELL1_number][link]++;
          HeaderAvg[TELL1_number][link]+=m_linkHeaders[2];
        }
        if(m_linkHeaders[3]<m_maxToPlot && m_linkHeaders[3]>m_minToPlot){
          NEventsToAvg[TELL1_number][link]++;
          HeaderAvg[TELL1_number][link]+=m_linkHeaders[3];
        }
        if(m_plotHeaders){
          m_histsHeaders[TELL1_number][link]->fill(m_linkHeaders[2]);
          m_histsHeaders[TELL1_number][link]->fill(m_linkHeaders[3]);
        }

        if(m_linkHeaders[2]> 1100 || m_linkHeaders[2]< 10){  debug() << " ===> Found unusually high/low header" << m_linkHeaders[2] << " " << TELL1_number 
          << "  " << link << endmsg;

        }
        if(m_linkHeaders[3]> 1100 || m_linkHeaders[3]< 10){ debug()<< " ===> Found unusually high/low header" <<  m_linkHeaders[3] << " " << TELL1_number 
          << "  " << link <<endmsg;
        }
      }
    }
  }//End loop over headers

  //Do the same thing for Gain Scan (if applicable):
  if(m_isGainScan){
    if((m_evtNumber%m_nEvtGainScan)<=m_NForAvgCalc){
      step = m_evtNumber/m_nEvtGainScan;
      for(headerIt=m_headerContainer->begin();headerIt!=m_headerContainer->end();++headerIt){
        VeloTELL1Data* headerOneTELL1=(*headerIt);
        int TELL1_number=headerOneTELL1->key();
        if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue;
        for(int link = 0; link<NumberOfALinks; ++link){
          getLink( (*headerOneTELL1)[link]);
          if(m_linkHeaders[2]<m_maxToPlot && m_linkHeaders[2]>m_minToPlot){
            GSNEventsToAvg[TELL1_number][link][step]++;
            GSHeaderAvg[TELL1_number][link][step]+=m_linkHeaders[2];
          }
          if(m_linkHeaders[3]<m_maxToPlot && m_linkHeaders[3]>m_minToPlot){
            GSNEventsToAvg[TELL1_number][link][step]++;
            GSHeaderAvg[TELL1_number][link][step]+=m_linkHeaders[3];
          }
        }
      }
    }
  }

  //Calculate average header value for each link after m_NForAvgCalc events processed:
  if(m_evtNumber==m_NForAvgCalc){
    m_evtNumber++;
    for(int Tell1 =0; Tell1<106; Tell1++){
      for(int link = 0; link<NumberOfALinks; ++link){
        if(HeaderAvg[Tell1][link]!=0){
          HeaderAvg[Tell1][link]/=(float)NEventsToAvg[Tell1][link];
          debug() << "==> Avgs: " << Tell1 <<" " << link <<" " <<  HeaderAvg[Tell1][link] <<  " entries: " << NEventsToAvg[Tell1][link] << endmsg;
        }
      }
    }
  }

  //Same for GS
  if(m_isGainScan){
    if((m_evtNumber%m_nEvtGainScan)==m_NForAvgCalc){
      step = m_evtNumber/m_nEvtGainScan ;
      for(int Tell1 =0; Tell1<106; Tell1++){
        for(int link = 0; link<NumberOfALinks; ++link){
          if(GSHeaderAvg[Tell1][link][step]!=0){
            GSHeaderAvg[Tell1][link][step]/=(float)GSNEventsToAvg[Tell1][link][step];
          }
        }
      }
    }
  }



  //Now loop again over headers, separating low/high header values:
  if(m_evtNumber>m_NForAvgCalc){
    for(headerIt=m_headerContainer->begin();headerIt!=m_headerContainer->end();++headerIt){
      VeloTELL1Data* headerOneTELL1=(*headerIt);
      int TELL1_number=headerOneTELL1->key();
      std::vector<IHistogram1D*>& histsHighLink = m_histsHeadersHigh[TELL1_number];
      std::vector<IHistogram1D*>& histsLowLink  = m_histsHeadersLow [TELL1_number];
      std::vector<IHistogram1D*>& histsHighFPGA = m_histsHeadersHighFPGA[TELL1_number];
      std::vector<IHistogram1D*>& histsLowFPGA  = m_histsHeadersLowFPGA [TELL1_number];
      if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue;
      const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(TELL1_number);
      if(sensor == 0 ){
        error() << "Can't find a sensor associated with TELL1 " << TELL1_number << ". Aborting..." << endmsg;
        return StatusCode::FAILURE;
      }

      for(int link = 0; link<NumberOfALinks; ++link){
        HHPlotMax = (m_maxToPlot < HeaderMax[TELL1_number][link]) ? m_maxToPlot : HeaderMax[TELL1_number][link];
        HLPlotMin = (m_minToPlot > HeaderMin[TELL1_number][link]) ? m_minToPlot : HeaderMin[TELL1_number][link];

        int FPGAno = 3 - int(link/16);

        if(fpgaHPlotMax[TELL1_number][FPGAno]==0)fpgaHPlotMax[TELL1_number][FPGAno]=HeaderMax[TELL1_number][link];
        if(fpgaHPlotMin[TELL1_number][FPGAno]==0)fpgaHPlotMin[TELL1_number][FPGAno]=HeaderAvg[TELL1_number][link];
        if(fpgaLPlotMin[TELL1_number][FPGAno]==0)fpgaLPlotMin[TELL1_number][FPGAno]=HeaderMax[TELL1_number][link];
        if(fpgaLPlotMax[TELL1_number][FPGAno]==0)fpgaLPlotMax[TELL1_number][FPGAno]=HeaderAvg[TELL1_number][link];
        if(HeaderMax[TELL1_number][link]>fpgaHPlotMax[TELL1_number][FPGAno]){
          fpgaHPlotMax[TELL1_number][FPGAno] = (m_maxToPlot < HeaderMax[TELL1_number][link]) ? m_maxToPlot : HeaderMax[TELL1_number][link];
        }
        if(HeaderAvg[TELL1_number][link]<fpgaHPlotMin[TELL1_number][FPGAno]){
          fpgaHPlotMin[TELL1_number][FPGAno] = HeaderAvg[TELL1_number][link];
        }
        if(HeaderMin[TELL1_number][link]<fpgaLPlotMin[TELL1_number][FPGAno]){
          fpgaLPlotMin[TELL1_number][FPGAno] = (m_minToPlot > HeaderMin[TELL1_number][link]) ? m_minToPlot : HeaderMin[TELL1_number][link];
        }
        if(HeaderAvg[TELL1_number][link]>fpgaLPlotMax[TELL1_number][FPGAno]){
          fpgaLPlotMax[TELL1_number][FPGAno] = HeaderAvg[TELL1_number][link];
        }
        getLink( (*headerOneTELL1)[link]);

        //Fill arrays to calculate <HL> and <HH>
        if(m_linkHeaders[2]>m_minToPlot && m_linkHeaders[2]<m_maxToPlot){

          if(m_linkHeaders[2]>HeaderAvg[TELL1_number][link]){
            if(m_plotHeadersHighLow){
              if ( 0 == histsHighLink[link] ) {
                bookHistogramsHighLow(histsHighLink,TELL1_number,link,"High",HeaderAvg[TELL1_number][link]-10,HHPlotMax+10);
              }
              histsHighLink[link]->fill(m_linkHeaders[2]);
            }
            HeaderHighAvgH3[TELL1_number][link]+= m_linkHeaders[2];
            NHeaderHighAvgH3[TELL1_number][link]++;
          }
          else{
            if(m_plotHeadersHighLow){
              if ( 0 == histsLowLink[link] ) {
                bookHistogramsHighLow(histsLowLink,TELL1_number,link,"Low",HLPlotMin-10, HeaderAvg[TELL1_number][link] +10);
              }
              histsLowLink[link]->fill(m_linkHeaders[2]);
            }
            HeaderLowAvgH3[TELL1_number][link]+= m_linkHeaders[2];
            NHeaderLowAvgH3[TELL1_number][link]++;
          }
        }
        if(m_linkHeaders[3]>m_minToPlot && m_linkHeaders[3]<m_maxToPlot){
          if (m_linkHeaders[3]>HeaderAvg[TELL1_number][link]){
            if(m_plotHeadersHighLow){
              if ( 0 == histsHighLink[link] ) {
                bookHistogramsHighLow(histsHighLink,TELL1_number,link,"High",HeaderAvg[TELL1_number][link]-10,HHPlotMax+10);
              }
              histsHighLink[link]->fill(m_linkHeaders[3]);
            }
            HeaderHighAvgH4[TELL1_number][link]+= m_linkHeaders[3];
            NHeaderHighAvgH4[TELL1_number][link]++;
          }
          else{
            if(m_plotHeadersHighLow){
              if ( 0 == histsLowLink[link] ) {
                bookHistogramsHighLow(histsLowLink,TELL1_number,link,"Low",HLPlotMin-10, HeaderAvg[TELL1_number][link]+10);
              }
              histsLowLink[link]->fill(m_linkHeaders[3]);
            }
            HeaderLowAvgH4[TELL1_number][link]+= m_linkHeaders[3];
            NHeaderLowAvgH4[TELL1_number][link]++;
          }
        }
      }
      for(int link = 0; link<NumberOfALinks; ++link){
        int FPGAno = 3 - int(link/16);
        getLink( (*headerOneTELL1)[link]);
        //Fill arrays to calculate <HL> and <HH> split by FPGA
        if(m_linkHeaders[2]>m_minToPlot && m_linkHeaders[2]<m_maxToPlot){
          if(m_linkHeaders[2]>HeaderAvg[TELL1_number][link]){
            if(m_plotHeadersHighLow){
              if ( 0 == histsHighFPGA[FPGAno] ) {
                bookHistogramsHighLow(histsHighFPGA,TELL1_number,FPGAno,"High"
                    ,fpgaHPlotMin[TELL1_number][FPGAno]-10,fpgaHPlotMax[TELL1_number][FPGAno]+10);
              }
              histsHighFPGA[FPGAno]->fill(m_linkHeaders[2]);
            }
          }
          else{
            if(m_plotHeadersHighLow){
              if ( 0 == histsLowFPGA[FPGAno] ) {
                bookHistogramsHighLow(histsLowFPGA,TELL1_number,FPGAno,"Low"
                    ,fpgaLPlotMin[TELL1_number][FPGAno]-10,fpgaLPlotMax[TELL1_number][FPGAno]+10);
              }
              histsLowFPGA[FPGAno]->fill(m_linkHeaders[2]);
            }
          }
        }
        if(m_linkHeaders[3]>m_minToPlot && m_linkHeaders[3]<m_maxToPlot){
          if (m_linkHeaders[3]>HeaderAvg[TELL1_number][link]){
            if(m_plotHeadersHighLow){
              if ( 0 == histsHighFPGA[FPGAno] ) {
                bookHistogramsHighLow(histsHighFPGA,TELL1_number,FPGAno,"High"
                    ,fpgaHPlotMin[TELL1_number][FPGAno]-10,fpgaHPlotMax[TELL1_number][FPGAno]+10);
              }
              histsHighFPGA[FPGAno]->fill(m_linkHeaders[3]);
            }
          }
          else{
            if(m_plotHeadersHighLow){
              if ( 0 == histsLowFPGA[FPGAno] ) {
                bookHistogramsHighLow(histsLowFPGA,TELL1_number,FPGAno,"Low"
                    ,fpgaLPlotMin[TELL1_number][FPGAno]-10,fpgaLPlotMax[TELL1_number][FPGAno]+10);
              }
              histsLowFPGA[FPGAno]->fill(m_linkHeaders[3]);
            }
          }
        }
      }
    }
  }

  //Ditto for GS:
  if(m_isGainScan){
    if((m_evtNumber%m_nEvtGainScan)>m_NForAvgCalc){
      step = m_evtNumber/m_nEvtGainScan;
      for(headerIt=m_headerContainer->begin();headerIt!=m_headerContainer->end();++headerIt){
        VeloTELL1Data* headerOneTELL1=(*headerIt);
        int TELL1_number=headerOneTELL1->key();
        if(ExcludeTELL1(m_chosenTell1s,TELL1_number))  continue;
        for(int link = 0; link<NumberOfALinks; ++link){
          getLink( (*headerOneTELL1)[link]);

          //Fill arrays to calculate <HL> and <HH>
          if(m_linkHeaders[2]>m_minToPlot && m_linkHeaders[2]<m_maxToPlot){

            if(m_linkHeaders[2]>GSHeaderAvg[TELL1_number][link][step]){
              GSHeaderHighAvg[TELL1_number][link][step]+= m_linkHeaders[2];
              GSNHeaderHighAvg[TELL1_number][link][step]++;
            }
            else{
              GSHeaderLowAvg[TELL1_number][link][step]+= m_linkHeaders[2];
              GSNHeaderLowAvg[TELL1_number][link][step]++;
            }
          }
          if(m_linkHeaders[3]>m_minToPlot && m_linkHeaders[3]<m_maxToPlot){
            if (m_linkHeaders[3]>GSHeaderAvg[TELL1_number][link][step]){
              GSHeaderHighAvg[TELL1_number][link][step]+= m_linkHeaders[3];
              GSNHeaderHighAvg[TELL1_number][link][step]++;
            }
            else{
              GSHeaderLowAvg[TELL1_number][link][step]+= m_linkHeaders[3];
              GSNHeaderLowAvg[TELL1_number][link][step]++;
            }
          }
        }
      }
    }
  }
  return StatusCode::SUCCESS;
}
// Declaration of the Algorithm Factory
DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, GainMon )
