/**
 * @file   PCNErrorMon.cpp
 * @author Suvayu Ali <Suvayu.Ali@cern.ch>
 * @date   Tue Apr 24 15:20:12 2012
 *
 * @brief  Implementation file for PCNErrorMon, PCNError and PCNErrorMap
 *
 *
 */

#include <iostream>
#include <algorithm>
#include <sstream>
#include <exception>

#undef NDEBUG
#include <cassert>

#include <boost/format.hpp>
#include <boost/foreach.hpp>

#include <TCanvas.h>
#include <TAxis.h>

#include "Event/ODIN.h"
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"
#include "GaudiUtils/Aida2ROOT.h"

#include "PCNErrorMon.h"

// Declare algorithm
DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring, PCNErrorMon)


namespace Velo{
  namespace Monitoring{

    PCNErrorMon::PCNErrorMon(const std::string& name, ISvcLocator* pSvcLocator)
      : GaudiHistoAlg(name, pSvcLocator)
      , runNo(0)
      , eventID(0)
      , nEvts(0)
      , nFEMPCNoffsync(0)
      , nBeetlePCNoffsync(0)
      , hFEMPCNsync(0)
      , hBeetleMap(0)
    {
      declareProperty("RawEventLocation",
		      m_RawEventLocation=LHCb::RawEventLocation::Default);
      declareProperty("ErrorBankLocation",
		      m_ErrorBankLoc=VeloErrorBankLocation::Default);
      declareProperty("TELL1s", m_expectedTell1Ids,
		      "The list of TELL1s to be monitored.");
      declareProperty("excludePileup", excludePileup=true);
      declareProperty("doLog", doLog=true);
      setProperty ( "PropertiesPrint", true ).ignore();
      setHistoTopDir( "Velo/" );

      if (excludePileup) {
	MAX_TELL1ID = 128;
	info() << "Pile-up modules will be skipped." << endmsg;
      }
      else {
	MAX_TELL1ID = 132;
	info() << "Pile-up modules will be included." << endmsg;
      }
    }


    PCNErrorMon::~PCNErrorMon()
    {
      ErrCounter.clear();
      hperBeetleBitMap.clear();
    }


    StatusCode PCNErrorMon::initialize()
    {
      StatusCode sc = GaudiHistoAlg::initialize();
      if (sc.isFailure()) return sc;
      if (propsPrint()) printProps();

      debug() << "==> Initialize" << endmsg;

      const DeVelo* velo = getDet<DeVelo>(DeVeloLocation::Default);
      // Create the list of TELL1 Ids we will represent in histograms.
      // This is either the configured list, or all Ids as retrieved from
      // the detector element.
      if ( m_expectedTell1Ids.empty() ) { // get Ids from detector element
	std::vector<DeVeloSensor*>::const_iterator sensorItr  =
	  velo->sensorsBegin();
	std::vector<DeVeloSensor*>::const_iterator sensorItrE =
	  velo->sensorsEnd();
	unsigned int tell1Id;	// output

	while ( sensorItr != sensorItrE ) {
	  if ( velo->tell1IdBySensorNumber( (*sensorItr)->sensorNumber(), tell1Id )
	       == false ) {
	    warning() << "Could not map sensor number "
		      << (*sensorItr)->sensorNumber()
		      << " to TELL1 Id." << endmsg;
	  } else {
	    m_tell1Ids.push_back(tell1Id);
	  }
	  ++sensorItr;
	}
      } else { // use Ids from configuration
	m_tell1Ids = m_expectedTell1Ids;
      }

      // sort the list of TELL1 Ids to facilitate finding the minimum
      // and maximum Id for histogram boundaries
      sort( m_tell1Ids.begin(), m_tell1Ids.end() );
      info() << "Total TELL1s: " << m_tell1Ids.size() << endmsg;
      // create counters and book histograms
      if ( m_tell1Ids.empty() ) { // no TELL1 Ids to be monitored
	warning() << "There are no TELL1 Ids to monitor. "
		  << "Something wrong with your CondDB configuration?"
		  << endmsg;
      }

      // extra bins at either end for clearer drawing
      hFEMPCNsync = Gaudi::Utils::Aida2ROOT::
	aida2root(book1D("hFEMPCNsync", "Number of FEM PCN errors",
			 -1.5, MAX_TELL1ID+0.5, MAX_TELL1ID+2));
      hFEMPCNsync->SetXTitle("Sensor #");
      hFEMPCNsync->SetYTitle("Desync frequency");

      hBeetleMap = Gaudi::Utils::Aida2ROOT::
	aida2root(book2D("hBeetleMap", "Beetle PCN error map",
			 -1.5, MAX_TELL1ID+0.5, MAX_TELL1ID+2, -1.5, 16.5, 18));
      hBeetleMap->SetXTitle("Tell1 id");
      hBeetleMap->SetYTitle("Beetle no.");
      hBeetleMap->GetXaxis()->SetTitleSize(0.05);
      hBeetleMap->GetYaxis()->SetTitleSize(0.05);

      // Book per Beetle histograms
      BOOST_FOREACH(unsigned tell1id, m_tell1Ids) {
	for (unsigned beetle = 0; beetle < 16; ++beetle) {
	  // assuming the map elements are initialised to 0
	  unsigned int key(tell1id|(beetle<<8));

	  std::stringstream coords, hnum;
	  coords << "(" << tell1id << "," << beetle << ")";
	  hnum   << tell1id << "_" << beetle;
	  std::string hname("hperBeetleBitMap_" + hnum.str()),
	    htitle("Per Beetle PCN error map " + coords.str());

	  // Create histogram if not present
	  if (hperBeetleBitMap[key] == NULL) {
	    // two empty bins on either side for aesthetic reasons
	    int xbins(10), ybins(4);
	    hperBeetleBitMap[key] = Gaudi::Utils::Aida2ROOT::
	      aida2root(book2D(hname.c_str(), htitle.c_str(),
			       -1.5, 8.5, xbins, -1.5, 2.5, ybins));
	    hperBeetleBitMap[key]->SetXTitle("PCN bits with errors");
	    hperBeetleBitMap[key]->SetYTitle("Correct value for bad PCN bit");

	    // nicer axis title and labels (imp. b/c of inverted bit order)
	    TAxis *xaxis = hperBeetleBitMap[key]->GetXaxis();
	    TAxis *yaxis = hperBeetleBitMap[key]->GetYaxis();

	    std::stringstream lbl;
	    for(int i = 1; i <= xbins; ++i) {
	      if (i == 1 or i == xbins) lbl.str("");
	      else lbl << xbins-1-i;
	      xaxis->SetBinLabel(i, lbl.str().c_str());
	      lbl.str("");
	    }
	    xaxis->SetLabelSize(0.06);
	    xaxis->SetTitleSize(0.05);

	    for(int i = 1; i <= ybins; ++i) {
	      if (i == 1 or i == ybins) lbl.str("");
	      else lbl << i-2;
	      yaxis->SetBinLabel(i, lbl.str().c_str());
	      lbl.str("");
	    }
	    yaxis->SetLabelSize(0.06);
	    yaxis->SetTitleSize(0.05);
	  }
	} // end of Beetle loop
      }	  // end of TELL1 loop

      return StatusCode::SUCCESS;
    }


    StatusCode PCNErrorMon::execute()
    {
      debug() << "==> Execute" << endmsg;
      nEvts++;

      // get event information
      const LHCb::ODIN* odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
      runNo = odin->runNumber();
      eventID = odin->eventNumber();

      // fetch raw bank
      if ( !exist<LHCb::RawEvent>(m_RawEventLocation) ) {
	debug() << "Raw Event not found in " << m_RawEventLocation
		<< endmsg;
      } else {
	LHCb::RawEvent* rawEvent = get<LHCb::RawEvent>(m_RawEventLocation);
	readRawEvent(rawEvent);
      }

      // fetch error bank
      if ( !exist<VeloErrorBanks>(m_ErrorBankLoc) ) {
	debug() << "Error Bank not found in " << m_ErrorBankLoc
		<< endmsg;
      } else {
	VeloErrorBanks* errorbanks=get<VeloErrorBanks>(m_ErrorBankLoc);
	readErrorBanks(errorbanks);
      }

      try {
	if (doLog) formatLog();
      } catch (std::exception& e) {
	info() << e.what() << endmsg;
      }

      // housekeeping
      badfsync_TELL1s .clear();
      badfsync_expPCN .clear();
      badfsync_badbits.clear();
      badbsync_TELL1s .clear();
      badbsync_expPCN .clear();
      badnBeetles     .clear();
      badPCNbits      .clear();
      return StatusCode::SUCCESS;
    }


    StatusCode PCNErrorMon::finalize()
    {
      debug() << "==> Finalize" << endmsg;
      info() << nEvts << " processed!" << endmsg;

      try {			// catch exceptions from boost::format
	summary();
      } catch (std::exception& e) {
	info() << e.what() << endmsg;
      }

      // must be called after all other actions
      return GaudiHistoAlg::finalize();
    }


    void PCNErrorMon::readRawEvent(LHCb::RawEvent* rawevent)
    {
      VeloTELL1::dataVec vFEMPCNbyTELL1(MAX_TELL1ID, 0);
      // Retrieve and iterate over banks
      const std::vector<LHCb::RawBank*>& banks =
	rawevent->banks(LHCb::RawBank::Velo);

      BOOST_FOREACH (const LHCb::RawBank *rawbank, banks) {
	// SiDaQ::buffer_word is a typedef for u_int32_t
	VeloRawBankDecoder RBdata (static_cast<const SiDAQ::buffer_word*>
				   (rawbank->data()));
	// conditionally exclude pile-up stations
	if (excludePileup and rawbank->sourceID() >= (int)MAX_TELL1ID) continue;
	// save FEM PCN by TELL1id
	vFEMPCNbyTELL1[rawbank->sourceID()] = RBdata.header().pcn();
      }

      checkFEMPCNsync(vFEMPCNbyTELL1);
      return;
    }


    void PCNErrorMon::checkFEMPCNsync(const VeloTELL1::dataVec& vFEMPCNbyTELL1)
    {
      // Each element represents all possible PCN. 187 elements
      // initialised to 0
      std::vector<unsigned int> counter(187, 0);

      BOOST_FOREACH(unsigned tell1, m_tell1Ids) {
	// conditionally exclude pile-up stations
	if (excludePileup and tell1 >= MAX_TELL1ID) continue;
	++counter[vFEMPCNbyTELL1[tell1]];
      } // count the frequency of all possible PCNs

      // NB: Returns the _first_ most frequent PCN, doesn't handle the
      // case when there are multiple "most frequent PCN". The most
      // common PCN is also most likely to be the correct PCN.
      std::vector<unsigned int>::const_iterator freqPCNItr =
	max_element(counter.begin(), counter.end());
      unsigned corrPCN = freqPCNItr - counter.begin();

      BOOST_FOREACH(unsigned int tell1, m_tell1Ids) {
	if (excludePileup and tell1 >= MAX_TELL1ID) continue;
	if (vFEMPCNbyTELL1[tell1] != corrPCN) {
	  ++nFEMPCNoffsync;
	  hFEMPCNsync->Fill(tell1);
	  PCNError::eightbits badbits(vFEMPCNbyTELL1[tell1]^corrPCN);
	  if (doLog) {
	    badfsync_TELL1s.push_back(tell1);
	    badfsync_expPCN.push_back(corrPCN);
	    badfsync_badbits.push_back(badbits);
	  } else {
	  }
	}
      }
      return;
    }


    void PCNErrorMon::readErrorBanks(VeloErrorBanks* banks)
    {
      // Iterate over banks. Each bank corresponds to 1 TELL1
      VeloErrorBanks::const_iterator banksItr = banks->begin(),
	banksItrE = banks->end();

      // FIXME: Don't know why BOOST_FOREACH doesn't work
      // BOOST_FOREACH (const VeloErrorBank* errbank, banks) { // loop over all TELL1s

      for (; banksItr != banksItrE; ++banksItr) { // loop over all TELL1s
	// retrieve error bank for PP-FPGAs reporting errors (max 0-3)
	const VeloErrorBank* errbank(*banksItr);
	if (errbank->isEmpty()) continue;

	// get event information
	VeloTELL1::dataVec errsource(errbank->errorSources());

	if (errsource.empty()) continue;
	const EvtInfo* eventinfo(errbank->evtInfo());
	// inherited method from KeyedObject<KEY>
	unsigned int numberOfTELL1(errbank->key());
	assert(eventinfo);

	unsigned int FEMPCN(errbank->PCN());
	VeloTELL1::dataVec allPCNVec;
	// loop over FPGAs in error bank
	BOOST_FOREACH(unsigned int ppfpga, errsource) {
	  // PCN bits for each PPFPGA
	  const VeloTELL1::dataVec PCNBeetleVec(eventinfo->PCNBeetle(ppfpga));

	  const unsigned int MaxBeetles(PCNBeetleVec.size()); // MaxBeetles = 4
	  assert(MaxBeetles == 4);
	  // loop over all Beetles on one FPGA
	  for (unsigned int i = 0; i < MaxBeetles; ++i) {
	    allPCNVec.push_back(PCNBeetleVec[i]);
	  } // end of Beetle loop
	}   // end of PPFPGA loop

	checkBeetlePCNsync(numberOfTELL1, FEMPCN, errsource, allPCNVec);
      }	// end of TELL1  loop
      return;
    }

    void PCNErrorMon::checkBeetlePCNsync(unsigned int numberOfTELL1,
					 unsigned int FEMPCN,
					 VeloTELL1::dataVec& FPGAVec,
					 VeloTELL1::dataVec& PCNVec)
    {
      std::vector<unsigned int> tmpBeetles;
      std::vector<PCNError::eightbits> tmpPCNbits;
      assert (4*FPGAVec.size() == PCNVec.size());

      for (unsigned int j = 0; j < FPGAVec.size(); ++j) {
	for (unsigned int i = 0; i < 4; ++i) {
	  if (FEMPCN != PCNVec[j*4+i]) {
	    ++nBeetlePCNoffsync;

	    PCNError err(FEMPCN, FEMPCN^PCNVec[j*4+i]);
	    Fill(numberOfTELL1, (3-FPGAVec[j])*4+i, err);

	    if (doLog) {
	      PCNError::eightbits badbits(FEMPCN^PCNVec[j*4+i]);
	      tmpBeetles.push_back((3-FPGAVec[j])*4+i);
	      tmpPCNbits.push_back(badbits);
	    }
	  }
	} // loop over 4 Beetles
      }   // loop over FPGAs with errors

      if (tmpBeetles.empty() == false) {
	badbsync_TELL1s.push_back(numberOfTELL1);
	badbsync_expPCN.push_back(FEMPCN);
	badnBeetles.push_back(tmpBeetles);
	badPCNbits.push_back(tmpPCNbits);
      }
      return;
    }


    void PCNErrorMon::formatLog()
    {
      // run, eventID, TELL1, expected PCN, FEM PCN state, PPFPGA, bad bits
      boost::format line1("| %6u | %10u | %5u | %7u | %8s | %8s |");
      for (unsigned int idx = 0; idx < badfsync_TELL1s.size(); ++idx) {
	if (idx == 0) line1 % runNo % eventID;
	else line1 % "" % "";
	PCNError::eightbits goodbits(badfsync_expPCN[idx]);
	line1 % badfsync_TELL1s[idx] % badfsync_expPCN[idx]
	  % goodbits.to_string<char>()
	  % badfsync_badbits[idx].to_string<char>();
	error_summary1.push_back(line1.str());
	debug() << "FEMPCN sync: " << line1 << endmsg;
	line1.clear();
      }

      boost::format line2("| %6u | %10u | %5u | %7s | %6u | %8s | %8s |");
      if (badnBeetles.empty() == false) {
	for (unsigned idx1 = 0; idx1 < badbsync_TELL1s.size(); ++idx1) {
	  for (unsigned idx2 = 0; idx2 < badnBeetles[idx1].size(); ++idx2) {
	    if (idx2 == 0) line2 % runNo % eventID % badbsync_TELL1s[idx1]
			     % badbsync_expPCN[idx1];
	    else line2 % "" % "" % "" % "";
	    PCNError::eightbits goodbits(badbsync_expPCN[idx1]);
	    line2 % badnBeetles[idx1][idx2] % goodbits.to_string<char>()
	      % badPCNbits[idx1][idx2].to_string<char>();
	    error_summary2.push_back(line2.str());
	    debug() << "Beetle PCN : " << line2 << endmsg;
	    line2.clear();
	  }
	}
      }
      return;
    }


    void PCNErrorMon::summary()
    {
      info() << "Found " << nFEMPCNoffsync
	     << " FEM PCNs out of sync!" << endmsg;
      info() << "Found " << nBeetlePCNoffsync
	     << " Beetle PCNs out of sync!" << endmsg;

      if (doLog) {
	// FEM PCN desync log
	info() << endmsg;
	info() << boost::format("| %6s | %10s | %5s | %7s | %8s | %8s |")
	  % "Run #" % "Event ID" % "TELL1" % "Exp PCN" % "Exp bits" % "bad bits"
	       << endmsg;
	info() << "|--------+------------+-------+---------+----------+----------|"
	       << endmsg;
	BOOST_FOREACH(std::string line, error_summary1) {
	  info() << line << endmsg;
	}
	info() << endmsg;

	// Beetle PCN desync log
	info() << endmsg;
	info() << boost::format("| %6s | %10s | %5s | %7s | %6s | %8s | %8s |")
	  % "Run #" % "Event ID" % "TELL1" % "Exp PCN" % "Beetle" % "Exp bits" % "bad bits"
	       << endmsg;
	info() << "|--------+------------+-------+---------+--------+----------+----------|"
	       << endmsg;
	BOOST_FOREACH(std::string line, error_summary2) {
	  info() << line << endmsg;
	}
	info() << endmsg;
      }

      return;
    }


    //////////////////////////////
    // PCNError implementations //
    //////////////////////////////

    PCNErrorMon::PCNError::PCNError(unsigned int pcn, unsigned int pcnxor) :
      _pcn(pcn), _xor(pcnxor) {}


    PCNErrorMon::PCNError::PCNError(std::string pcnbits, std::string xorbits) :
      _pcn(PCNError::eightbits(pcnbits).to_ulong()),
      _xor(PCNError::eightbits(xorbits).to_ulong()) {}


    PCNErrorMon::PCNError::~PCNError() {}


    PCNErrorMon::PCNError::eightbits PCNErrorMon::PCNError::getBits
    (_PCN_T bittype) const
    {
      unsigned int value(-1);
      if (bittype == kPCN) value = _pcn;
      else if (bittype == kXOR) value = _xor;
      else if (bittype == kBAD) value = _pcn^_xor;
      else value = _pcn;
      return eightbits (value);
    }


    void PCNErrorMon::PCNError::setBits(unsigned int pcn, unsigned int pcnxor)
    {
      _pcn = pcn;
      _xor = pcnxor;
      return;
    }


    void PCNErrorMon::PCNError::setBits(std::string pcnbits,
					std::string xorbits)
    {
      _pcn = eightbits(pcnbits).to_ulong();
      _xor = eightbits(xorbits).to_ulong();
      return;
    }


    void PCNErrorMon::Fill(unsigned int tell1id, unsigned int beetle,
			   PCNError err)
    {
      PCNError::eightbits pcnbits(err.getBits(PCNError::kPCN)),
	xorbits(err.getBits(PCNError::kXOR));

      if (xorbits.any()) {
	// assuming the map elements are initialised to 0
	unsigned int key(tell1id|(beetle<<8));
	ErrCounter[key]++;

	// Fill map
	hBeetleMap->Fill(tell1id, beetle);
	// Fill per Beetle bitwise map
	// store bits in the reverse order (MSB -> LSB)
	for (unsigned int i = 0; i < 8; ++i) { // 8 bits
	  if (xorbits.test(7-i)) hperBeetleBitMap[key]->Fill(i,pcnbits[7-i]);
	}
      }

      return;
    }
  }
}
