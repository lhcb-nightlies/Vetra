
// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
// Include files from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
// TELL1 Event:
#include "Event/VeloODINBank.h"
#include "Event/ODIN.h"


//-----------------------------------------------------------------------------
// PrintODINBank
//
// 2007-10-30 Aras Papadelis, arasp@nikhef.nl
// Simple class for printing the contents of the ODIN bank.
// Options:
//            PrintBasicInfo: Run number, L0 event Id, bunch ID, orbit ID  
//            PrintExtendedInfo: The rest
//            PrintFrequency
//-----------------------------------------------------------------------------


class PrintODINBank : public GaudiHistoAlg {
public: 
  /// Standard constructor
  PrintODINBank( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~PrintODINBank( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization


  bool m_printbasic;
  bool m_printextended;
  int m_printfrequency;
  bool m_useVeloOdin;

private:


};

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrintODINBank::PrintODINBank( const std::string& name,
                                              ISvcLocator* pSvcLocator)
  :  GaudiHistoAlg( name , pSvcLocator )
{


  declareProperty("PrintBasicInfo",m_printbasic=true);
  declareProperty("PrintExtendedInfo",m_printextended=false);
  declareProperty("PrintFrequency",m_printfrequency=100);
  declareProperty("UseVeloOdin",m_useVeloOdin=true);
  
}

//=============================================================================
// Destructor
//=============================================================================
PrintODINBank::~PrintODINBank() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrintODINBank::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode PrintODINBank::finalize() {
  debug() << "==> Finalize" << endmsg;
  return GaudiHistoAlg::finalize();  // must be called after all other actions
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode PrintODINBank::execute() {

  debug() << "==> Execute" << endmsg;

  static unsigned int eventcounter=0;
  
  if(eventcounter%m_printfrequency==0){
    if ( m_useVeloOdin ) {
      if(exist<VeloODINBanks>(VeloODINBankLocation::Default)){
        VeloODINBanks* m_odin=get<VeloODINBanks>(VeloODINBankLocation::Default);
        VeloODINBanks::const_iterator odinIt=m_odin->begin();
        VeloODINBank* odin=(*odinIt);
     
        if(m_printbasic || m_printextended){
  	  info()<< "*********ODIN BANK CONTENT***************"<<endmsg;   
  	  info()<< "Run number        : " << odin->runNumber() << endmsg;
  	  info()<< "L0 event ID       : " << odin->L0EventID() << endmsg;    
  	  info()<< "Orbit ID          : " << odin->orbitNumber() << endmsg;
  	  info()<< "Bunch ID          : " << odin->bunchID() <<endmsg;
        }
        if(m_printextended){
  	  info()<< "GPS time          : " << odin->GPSTime() << endmsg;
	  info()<< "Event type        : " << odin->eventType() << endmsg;
	  info()<< "Odin version      : " << odin->odinVersion() <<endmsg;
	  info()<< "Detector Status   : " << odin->detectorStatus() << endmsg;
	  info()<< "Bunch current     : " << odin->bunchCurrent() << endmsg;
	  info()<< "Error bits        : " << odin->errorBits() <<endmsg;
	  info()<< "Read-out type     : " << odin->readoutType() << endmsg;
	  info()<< "Trigger type      : " << odin->triggerType() << endmsg;
        }
        if(m_printbasic || m_printextended){
  	  info()<< "*****************************************"<<endmsg;
        }
    
      } else {
        info() << "Can't find VeloODIN bank in data!" << endmsg;
      }
    }
    else { // use ODIN instead of VeloODINBank
      if(exist<LHCb::ODIN>(LHCb::ODINLocation::Default)){
        LHCb::ODIN* odin=get<LHCb::ODIN>(LHCb::ODINLocation::Default);
     
        if(m_printbasic || m_printextended){
  	  info()<< "*********ODIN BANK CONTENT***************"<<endmsg;   
  	  info()<< "Run number        : " << odin->runNumber() << endmsg;
  	  info()<< "Event number        : " << odin->eventNumber() << endmsg;
  	  info()<< "Orbit ID          : " << odin->orbitNumber() << endmsg;
  	  info()<< "Bunch ID          : " << odin->bunchId() <<endmsg;
        }
        if(m_printextended){
  	  info()<< "GPS time          : " << odin->gpsTime() << endmsg;
	  info()<< "Event type        : " << odin->eventType() << endmsg;
	  info()<< "Odin version      : " << odin->version() <<endmsg;
	  info()<< "Detector Status   : " << odin->detectorStatus() << endmsg;
	  info()<< "Bunch current     : " << odin->bunchCurrent() << endmsg;
	  info()<< "Error bits        : " << odin->errorBits() <<endmsg;
	  info()<< "Read-out type     : " << odin->readoutType() << endmsg;
	  info()<< "Trigger type      : " << odin->triggerType() << endmsg;
        }
        if(m_printbasic || m_printextended){
  	  info()<< "*****************************************"<<endmsg;
        }
    
      } else {
        info() << "Can't find ODIN bank in data!" << endmsg;
      }
    }
  }
  eventcounter++;     
  
  return StatusCode::SUCCESS;
}



// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( PrintODINBank )
