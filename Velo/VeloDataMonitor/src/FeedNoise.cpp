// ============================================================================
// Include files 
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/AlgFactory.h" 
// ============================================================================
// GaudiAlg 
// ============================================================================
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
// VeloTELL1Event
// ============================================================================
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"
// ============================================================================
// Boost 
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
// local 
// ============================================================================
#include "VetraKernel/INoiseEvaluator.h"
#include "VetraKernel/TELL1Noise.h"
#include "CommonFunctions.h"
// ============================================================================
namespace Velo 
{
  namespace Monitoring 
  {
    /** @class FeedNoise 
     *  Simple algorithm to perform monitoring of pedestals&noise 
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-10-26
     *  Split into two algorithms by Aras Papadelis, 2007-11-26
     *  This algorithm feeds the noise tool.
     */
    class FeedNoise : public GaudiHistoAlg 
    {
      /// friend factory for instantiation
      friend class AlgFactory<Velo::Monitoring::FeedNoise>;
      public:
      // initialize it! 
      //virtual StatusCode initialize () ;
      // execute it! 
      virtual StatusCode execute    () override;
      // finalize it! 
      virtual StatusCode finalize   () override;
      protected:
      /// standard constructor 
      FeedNoise 
        ( const std::string& name , 
          ISvcLocator*       pSvc ) 
        : GaudiHistoAlg ( name , pSvc ) 
        , m_locations () 
        , m_tell1s    () 
        , m_public    ( true )
        , m_toolType  ( "Velo::Monitoring::NoiseEvaluator" )
        , m_sigma ( 6.0 )
        , m_event     ( 0 )
        , m_noises    () 
        , m_isInit    (0)
      {
        //
        m_locations.push_back ( LHCb::VeloTELL1DataLocation::ADCs             ) ;
        m_locations.push_back ( LHCb::VeloTELL1DataLocation::PedSubADCs       ) ;
        m_locations.push_back ( LHCb::VeloTELL1DataLocation::FIRCorrectedADCs ) ;
        m_locations.push_back ( LHCb::VeloTELL1DataLocation::ReorderedADCs    ) ;
        m_locations.push_back ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ) ;
        declareProperty 
          ( "Containers" , 
            m_locations  , 
            "The list of TES locations to be monitored") ;
        declareProperty 
          ( "TELL1s"     , 
            m_tell1s     , 
            "The list of TELL1s to be monitored, empty for all") ;
        declareProperty 
          ( "UsePublicTools" , 
            m_public , 
            "Make the tools for noise evaluation public" ) ;
        declareProperty 
          ( "NoiseEvaluatorType" , 
            m_toolType , 
            "The actual type of noise evaluator tool" ) ;
        declareProperty 
          ( "PublishingFrequency" , 
            m_publish             , 
            "The frequency to publish results" ) ;            
        declareProperty 
          ( "SignalSigma" , 
            m_sigma=6.0  , 
            "Threshold for large signals (in sigmas)" ) ;
        declareProperty 
          ( "UseHitThresholds" , 
            m_useHitThresholds=true  , 
            "Filter signal based on hit thresholds. Only affects CMSuppressedADCs container." ) ;
        setProperty ( "HistoPrint"  , true     ).ignore() ;
        setProperty ( "HistoTopDir" , "Vetra/" ).ignore() ;
      }
      /// virtual and protected destructor 
      virtual ~FeedNoise() {}
      private:
      // no default constructor 
      FeedNoise ()                  ; ///< no default constructor 
      // no copy constructor 
      FeedNoise ( const FeedNoise& ) ; ///< no copy constructor 
      // no assigmenet operator 
      FeedNoise& operator=( const FeedNoise& ) ; ///< no assignement 
      protected:
      /// fill the tools with the data 
      StatusCode feedTheTools () ;

      private:
      // the actual type of container of data locations 
      typedef std::vector<std::string>                  Locations ;
      typedef Velo::Monitoring::INoiseEvaluator::TELL1s TELL1s    ;
      typedef std::map<std::string,Velo::Monitoring::INoiseEvaluator*> Tools ;
      // the input containers of data to be monitored
      Locations     m_locations ; ///< the input containers
      // the list of IDs for TELL1s to be monitored 
      TELL1s        m_tell1s    ; ///< list of ids for TELL1 
      // publishing frequency
      unsigned long m_publish   ; ///< publishing frequency
      // use the public tools? 
      bool          m_public    ; ///< use the public tools 
      // the actual tool type 
      std::string   m_toolType  ; ///< the actual tool type
      // threshold to skip the large signals (in sigmas)
      double       m_sigma ; ///< threshold to skip the large signals (in sigmas)
      bool         m_useHitThresholds; ///< use hit thresholds to filter signal on CM supp. ADCs
      // event counter 
      unsigned long m_event     ; ///< internal event counter 
      // the map of tools to be used 
      Tools         m_noises    ; ///< map of tools to be used 
      // Flag for initializing the process info bank.
      int m_isInit;
      // Convergence limit from pedestal subtractor
      unsigned int m_convergenceLimit;      



    } ;
  } // end of namespace Velo::Monitoring 
} // end of namespace Velo
// ============================================================================

// ============================================================================
// Feed the tool with data 
// ============================================================================
StatusCode Velo::Monitoring::FeedNoise::feedTheTools() 
{
  // load all headers
  if ( !exist<LHCb::VeloTELL1Data::Container> ( LHCb::VeloTELL1DataLocation::Headers ) ) {
    debug() << "Could not retrieve headers from " << LHCb::VeloTELL1DataLocation::Headers << endmsg;
    return StatusCode::SUCCESS;
  }
  const LHCb::VeloTELL1Data::Container* headers = 
    get<LHCb::VeloTELL1Data::Container> ( LHCb::VeloTELL1DataLocation::Headers ) ;

  // loop over all input containers 
  for ( Locations::const_iterator il = m_locations.begin() ; 
      m_locations.end() != il ; ++il ) 
  {
    // check the existence of the data in TES 
    if ( !exist<LHCb::VeloTELL1Data::Container> ( *il ) ) 
    {
      Warning ( "Non-existing location: " +(*il) + "'" ) ;
      continue ;                                                   // CONTINUE 
    }
    // get the evaluator of the noise:
    Velo::Monitoring::INoiseEvaluator* eval = m_noises [ *il ] ;
    // locate the tool if not yet done 
    if ( 0 == eval )  
    {
      // construct the actual tool name 
      std::string toolName = strippedContainerName( *il ) ;
      if ( m_public ) { toolName += ":PUBLIC" ; }
      eval = tool<Velo::Monitoring::INoiseEvaluator> 
        ( m_toolType , toolName , this ) ;
      always() << " tool instance name is " << eval->name() << endmsg ;
      GaudiTool* noiseEval = dynamic_cast<GaudiTool*>(eval);
      if ( 0 == noiseEval ) {
        warning() << "Can not configure tool instance because of wrong type." << endmsg;
      } else {
        noiseEval->setProperty ( "nSigma", m_sigma ).ignore() ;
        if ( m_useHitThresholds && LHCb::VeloTELL1DataLocation::CMSuppressedADCs == *il ) {
           noiseEval->setProperty ( "UseHitThresholds", m_useHitThresholds ).ignore() ;
        }
      }
      m_noises [ *il ] = eval ;
    }
    // get the data from TES 
    const LHCb::VeloTELL1Data::Container* data = 
      get<LHCb::VeloTELL1Data::Container> ( *il )  ;    

    // use the tool :
    StatusCode sc = m_tell1s.empty() 
      ? eval->process ( data ,            headers ) 
      : eval->process ( data , m_tell1s , headers ) ;
    if ( sc.isFailure() ) 
    { Error ( "Error from INoiseEvaluator '" + eval->name() + "'", sc ) ; }
  }
  //
  return StatusCode::SUCCESS ;
}

// ============================================================================
// finalize the algotithm 
// ============================================================================
StatusCode Velo::Monitoring::FeedNoise::finalize   () 
{
  // clear the container of tools
  m_noises.clear() ;
  m_event = 0     ;
  // finalize the base class 
  return GaudiHistoAlg::finalize() ;
}
// ============================================================================
// Execute the algorithm 
// ============================================================================
StatusCode Velo::Monitoring::FeedNoise::execute    () 
{  


  //Get pedestal convergence limit from the emulator process info. This can't be done
  //from initialize() so we use the m_isInit flag to only run it the first event.

  if(m_isInit==0){
    VeloProcessInfos* procInfos;  
    if(!exist<VeloProcessInfos>(VeloProcessInfoLocation::Default)){      
      debug() << " ==> No init info available in this event. Waiting." << endmsg;
    }else{
      procInfos=get<VeloProcessInfos>(VeloProcessInfoLocation::Default);
      VeloProcessInfos::const_iterator procIt=procInfos->begin();
      if((*procIt)->convLimit()==0){
        debug()<< "Convergence Limit not set!" <<endmsg;
        m_convergenceLimit=(*procIt)->convLimit();
      }else{
        m_convergenceLimit=(*procIt)->convLimit();
      }    
      debug() << "Convergence limit set to " << m_convergenceLimit << endmsg;
      m_isInit=1;
    }
  }


  StatusCode sc;

  // feed the tool with the data if we have reached the first event number.  
  if( m_isInit && m_event>=m_convergenceLimit)
    sc = feedTheTools() ; ///< feed the tools with the data

  ++m_event;

  return sc ;
} 
// ============================================================================
/// Declaration of the Algorithm Factory
DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring,FeedNoise)
  // ============================================================================
  // The END
  // ============================================================================
