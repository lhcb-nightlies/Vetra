// $Id: TestPulseShape.h,v 1.2 2009-09-09 13:03:44 kakiba Exp $
#ifndef VELODATAMONITOR_TESTPULSESHAPE_H
#define VELODATAMONITOR_TESTPULSESHAPE_H 1

// Include files
// -------------
#include "VeloDet/DeVelo.h"

// from VeloEvent
#include "VeloEvent/VeloTELL1Data.h"

// from DigiEvent
#include "Event/VeloCluster.h"
#include "VeloEvent/EvtInfo.h"

// local
#include "VeloMonitorBase.h"

/** @class TestPulseShape TestPulseShape.h
 *  
 *
 *  @author Kazu  
 *  @date   2008-10-22
 */

namespace Velo
{
  class TestPulseShape : public VeloMonitorBase {
  public: 
    /// Standard constructor
    TestPulseShape( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~TestPulseShape( );    ///< Destructor
    virtual StatusCode initialize() override;    ///< Algorithm initialization
    virtual StatusCode execute   () override;    ///< Algorithm execution
    virtual StatusCode finalize  () override;    ///< Algorithm finalization
    long          m_nEvents;
    int           m_nSteps;
    int           m_shift;
    int           m_stepSize;
	int  m_plot2d;
	bool  m_debugPolarity;
	int  m_plotProf;
	int  m_plotRaw;
	int  m_plotLink;
	int  m_plotCMS;
	int  m_dontSplit;
	bool  m_runWithOdin;
	EvtInfos *m_evtInfos;

    double m_cmsCut;

	int m_wrapPoint;
	int  m_wrappedSteps;
    bool m_inverted;
	std::vector<int> m_chosenTell1s;
	std::vector<int> m_pulsedChannels;
	std::vector<double> SubtractCommonMode( std::vector<int> data, double cut );
  protected:
  private:
    // Retrieve the VeloTell1Data
    LHCb::VeloTELL1Datas* veloTell1Data( std::string samplingLocation );
	EvtInfos* veloTell1EventInfos();
    // Monitor the VeloTell1Data
    void MakePulseShape( std::string samplingLocation, LHCb::VeloTELL1Datas* tell1Datas );
    // Data members
    const DeVelo* m_velo;
    // Job options
    std::vector<std::string> m_samplingLocations;
	std::vector<int> getLink(VeloTELL1::ALinkPair begEnd) {
	  std::vector<int>::const_iterator iT; //iterator to vector of unsigned int.
	  std::vector<int> linkVector;
	  linkVector.clear();
	  //Loop over ADC values in link
	  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
		linkVector.push_back(*iT); //Push back ADC value to vector
	  }
	  return linkVector;
	}

  };
}

#endif  //  VELODATAMONITOR_TESTPULSESHAPE_H
