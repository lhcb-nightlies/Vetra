// $Id: ErrorMon.cpp,v 1.12 2009/10/09 07:18:15 erodrigu Exp $
#include <algorithm>

#include "boost/format.hpp"

#include "GaudiKernel/AlgFactory.h"
#include "GaudiAlg/Fill.h"

#include "ErrorMon.h"

#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"

#include "TMath.h"

using namespace VeloTELL1;

//-----------------------------------------------------------------------------
// Implementation file for class : ErrorMon
//
// 2008-06-30 : Kurt Rinnert <kurt.rinnert@cern.ch>
//
// 2010-03-29 : Chiara Farinelli <chiara.farinelli@cern.ch>
//
// Usage : run with options Vetra-Default-EB_Data.py
//
//-----------------------------------------------------------------------------

DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring,ErrorMon)

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  Velo::Monitoring::ErrorMon::ErrorMon(const std::string& name, ISvcLocator* pSvcLocator)
  : GaudiHistoAlg(name, pSvcLocator)
  , m_velo(0)
  , m_nEvts(0)
  , m_evtNumber(0)
  , m_sensors(0)
  , m_evtsPerTell1(0)
  , m_errsPerTell1(0)
  , m_overviewpseudo(0)
  , m_overviewpcn(0)
  , m_overviewadcfifo(0)
  , m_overviewchannel(0)
  , m_overviewiheader(0)
  , m_errNum(0)
  , m_errFreq(0)
  , m_errorType(0)

{
  declareProperty("RawEventLocation",m_rawEventLocation=LHCb::RawEventLocation::Default);
  declareProperty("ErrorBankLocation", m_errorBankLoc=VeloErrorBankLocation::Default);
  declareProperty("EventsPerTell1PubRate", m_evtsPerTell1PubRate=10000000);
  declareProperty("ErrorsPerTell1PubRate", m_errsPerTell1PubRate=10000000);
  declareProperty("DebugMode", m_debugmode=false);
  declareProperty 
    ( "TELL1s"
      , m_expectedTell1Ids
      , "The list of TELL1s to be monitored, empty for all from detector element.") ;


  setProperty ( "PropertiesPrint", true ).ignore();

  setHistoTopDir( "Velo/" );
}

//=============================================================================
// Destructor
//=============================================================================
Velo::Monitoring::ErrorMon::~ErrorMon() {;}

//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::Monitoring::ErrorMon::initialize()
{
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if (sc.isFailure()) return sc;  // error printed already by GaudiHistoAlg

  if ( propsPrint() ) printProps();

  //sc = service("ErrorMonitorSvc", m_ErrorMonitorSvc, true);
  //declareInfo(name1, m_sensors, name2, m_ErrorMonitorSvc);

  std::string name1= "Velo_ErrorMon_Sensors_with_Errors";
  std::string name2= "Number of Sensors with Errors";

  declareInfo(name1, m_sensors, name2);
  debug() << "==> Initialize" << endmsg;

  m_velo = getDet<DeVelo>( DeVeloLocation::Default );  

  // Create the list of TELL1 Ids we will represent in histograms.
  // This is either the configured list, or all Ids as retrieved from
  // the detector element.
  if ( m_expectedTell1Ids.empty() ) { // get Ids from detector element
    for ( std::vector<DeVeloSensor*>::const_iterator si = m_velo->sensorsBegin();
        si != m_velo->sensorsEnd();
        ++si ) {
      unsigned int tell1Id;
      if ( ! m_velo->tell1IdBySensorNumber((*si)->sensorNumber(),tell1Id) ) {
        warning() << "Could not map sensor number " 
          << (*si)->sensorNumber() 
          << " to TELL1 Id."
          << endmsg;
      } else {
        m_tell1Ids.push_back(tell1Id);
      }
    }
  } else { // use Ids from configuration
    m_tell1Ids = m_expectedTell1Ids;
  }
  // sort the list of TELL1 Ids to facilitate finding the minimum
  // and maximum Id for histogram boundaries
  std::sort(m_tell1Ids.begin(),m_tell1Ids.end());

  // create counters and book histograms
  if ( ! m_tell1Ids.empty() ) {
    for ( unsigned int i=0; i<m_tell1Ids.size(); ++i ) {
      m_nErrorsByTell1[m_tell1Ids[i]] = 0;
      m_nEventsByTell1[m_tell1Ids[i]] = 0;
    }

    unsigned int minTell1 = m_tell1Ids.front();
    unsigned int maxTell1 = m_tell1Ids.back();

    unsigned int minLink=0;
    unsigned int maxLink=63;

    // construct histogram titles and book the histogram for missing events
    boost::format fmtEvt ( "TELL1's with missing events in last %d" ) ;
    fmtEvt % m_evtsPerTell1PubRate ;
    const std::string titleEvt = fmtEvt.str() ;
    m_evtsPerTell1 = book( "TELL1 Missing Events",titleEvt, minTell1-1.5, maxTell1+1.5, maxTell1-minTell1+3);

    // construct histogram titles and book the histogram for error flags
    boost::format fmtErr ( "TELL1's with errors in last %d" ) ;
    fmtErr % m_errsPerTell1PubRate ;
    const std::string titleErr = fmtErr.str() ;
    m_errsPerTell1 = book( "TELL1 Error Flags",titleErr, minTell1-1.5, maxTell1+1.5, maxTell1-minTell1+3);

    // construct histogram titles and book the histogram for Pseudoheader errors per sensor per link
    const std::string titlePseudo ( "Pseudoheader errors per sensor per link" ) ;
    m_overviewpseudo = book2D( "Overview pseudoheader errors",titlePseudo, minTell1-0.5, maxTell1+0.5,  maxTell1-minTell1+1, minLink-0.5, maxLink+0.5, maxLink-minLink+1);

    // construct histogram titles and book the histogram for PCN errors per sensor per link
    const std::string titlePCN( "PCN errors per sensor per link" ) ;
    m_overviewpcn = book2D( "Overview PCN errors",titlePCN, minTell1-0.5, maxTell1+0.5, maxTell1-minTell1+1, minLink-0.5, maxLink+0.5, maxLink-minLink+1);

    // construct histogram titles and book the histogram for adcFIFO errors per sensor per link
    const std::string titleADCFIFO( "adc_FIFO errors per sensor per link" ) ;
    m_overviewadcfifo = book2D( "Overview adc_FIFO errors",titleADCFIFO, minTell1-0.5, maxTell1+0.5, maxTell1-minTell1+1, minLink-0.5, maxLink+0.5, maxLink-minLink+1);

    // construct histogram titles and book the histogram for channel errors per sensor per link
    //const std::string titleChannel ( "Channel errors per sensor per link" ) ;
    //m_overviewchannel = book2D( "Overview channel errors",titleChannel, minTell1-0.5, maxTell1+0.5, maxTell1-minTell1+1, minLink-0.5, maxLink+0.5, maxLink-minLink+1);

    // construct histogram titles and book the histogram for iheader errors per sensor per link
    const std::string titleIHeader ( "IHeader errors per sensor per Beetle" ) ;
    m_overviewiheader = book2D( "Overview IHeader errors",titleIHeader, minTell1-0.5, maxTell1+0.5, maxTell1-minTell1+1, minLink-0.5, maxLink+0.5, maxLink-minLink+1);

    // construct histogram titles and book the histogram for the frequency of error banks
    const std::string titleCount( "Number of Errors per sensor" ) ;
    m_errNum = book( "Error Counter",titleCount, minTell1-0.5, maxTell1+0.5, maxTell1-minTell1+1);

    // construct histogram titles and book the histogram for the frequency of error banks
    const std::string titleFreq( "Frequency of Error Banks per sensor" ) ;
    m_errFreq = book2D( "Error Frequency",titleFreq, minTell1-0.5, maxTell1+0.5, maxTell1-minTell1+1, 0, 1.1, 1100);

    // construct histogram titles and book the histogram for types of errors present
    const std::string titleType( "0=pseudoheader, 1=PCN, 2=adc_FIFO, 3=channel" ) ;
    m_errorType = book2D( "Types of Errors",titleType, minTell1-0.5, maxTell1+0.5, maxTell1-minTell1+1, minLink-0.5, minLink+3.5, minLink+4);

    // construct histogram titles and book the histogram for sensors with 2 iheader or PCN values different from others within one FPGA 
    //const std::string titleDouble( "Sensors with 2 different values for: 1=PCN, 2=IHeader" ) ;
    //m_errorDouble = book2D( "Double Errors",titleDouble, minTell1-0.5, maxTell1+0.5, maxTell1-minTell1+1, minLink-0.5, minLink+1.5, minLink+2);

    const std::string titleType2( "Asynchronous tell1s") ;
    m_asyncTells = book2D( "asynchronousTell1s",titleType2, minTell1-0.5, maxTell1+0.5, maxTell1-minTell1+1, -0.5,189.5, 190);

  } else { // no TELL1 Ids to be monitored, this should not happen
    warning() << "There are no TELL1 Ids to monitor.  Something wrong with your CondDB configuration?"
      << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode Velo::Monitoring::ErrorMon::execute()
{
  //return StatusCode::SUCCESS;

  debug() << "==> Execute" << endmsg;
  m_evtNumber++;
  //info() << "event num= " << m_evtNumber << endmsg;

  // fetch raw bank in any case
  if ( !exist<LHCb::RawEvent>(m_rawEventLocation) ) {
    debug() << "Raw Event not found in " << m_rawEventLocation
      << endmsg;
  } else { 
    LHCb::RawEvent* rawEvent = get<LHCb::RawEvent>(m_rawEventLocation);

    const std::vector<LHCb::RawBank*>& banks = rawEvent->banks(LHCb::RawBank::Velo);
    countErrorsAndMissingEvents(banks);
  }

  if ( !exist<VeloErrorBanks>(m_errorBankLoc) ) {
    debug() << "Error Bank not found in " << m_errorBankLoc
      << endmsg;
  } 
  else {
    VeloErrorBanks* errorBanks=get<VeloErrorBanks>(m_errorBankLoc);
    monitorErrorBanks(errorBanks);
  }

  m_nEvts++;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalize
//=============================================================================
StatusCode Velo::Monitoring::ErrorMon::finalize()
{
  debug() << "==> Finalize" << endmsg;

  if (m_debugmode) {
    printSummary();
  }

  return GaudiHistoAlg::finalize();  // must be called after all other actions
}

//=============================================================================
// Private Helpers
//=============================================================================
void Velo::Monitoring::ErrorMon::countErrorsAndMissingEvents(const std::vector<LHCb::RawBank*>& banks) 
{
  // monitor number of errors and missing events from ZS bank
  if ( 0 == m_nEvts % m_evtsPerTell1PubRate ) { 
    m_evtsPerTell1->reset(); 
  }

  if ( 0 == m_nEvts % m_errsPerTell1PubRate ) {
    m_errsPerTell1->reset();
  }

  std::map<int, std::vector<int> > pcn_tell1s;

  for (std::vector<LHCb::RawBank*>::const_iterator bi = banks.begin(); 
      bi != banks.end();
      ++bi) {

    const LHCb::RawBank* rb = *bi;
    const unsigned int tell1Number = static_cast<unsigned int>(rb->sourceID());

    // skip bank if TELL1 Id is not in list of expected Ids 
    if ( m_nErrorsByTell1.end() == m_nErrorsByTell1.find(tell1Number) ) {
      warning() << "Unexpected TELL1 Id " << tell1Number << ", skipping." << endmsg;
      continue;
    }     

    unsigned long& events = m_nEventsByTell1[tell1Number];
    events++;

    const SiDAQ::buffer_word* rawBank = static_cast<const SiDAQ::buffer_word*>(rb->data());

    // construct new raw bank decoder, implicitely decodes header
    VeloRawBankDecoder decoder(rawBank);

    unsigned long& errors = m_nErrorsByTell1[tell1Number];
    if ( decoder.hasError() ) {
      //++errors;
      m_nErrorsByTell1[tell1Number]++;
      errors++;
      //double err_ratio = errors / double(m_nEvts);

      //error() << "tell1= " << tell1Number << " #err= " << errors << endmsg; 

    }

    int mypcn = decoder.header().pcn();
    m_pcn[tell1Number] = mypcn;
    (pcn_tell1s[mypcn]).push_back(tell1Number);

    if ( 0 != m_nEvts && 0 == m_nEvts % m_errsPerTell1PubRate ) {
      if ( errors ) { 
        m_errsPerTell1->fill(tell1Number, errors);
      }
      errors=0;
    }
  }

  std::map<int, std::vector<int> >::iterator ipcn;
  std::map<int, int> pcnfrequencies;
  if ( pcn_tell1s.size() >1 || m_debugmode){
    //info() << " size of the map of pcns... " << pcn_tell1s.size() << endmsg;
    for(ipcn  =  pcn_tell1s.begin(); ipcn!= pcn_tell1s.end(); ipcn++){ 
      int pcn = ipcn->first;
      int frequency = (ipcn->second).size();
      pcnfrequencies[frequency] = pcn ;
    }
    std::map<int, int>::iterator  mostfrequentpcns;
    int rightpcn = (pcnfrequencies.rbegin())->second;
    //info() << " most frequent pcn is << " << rightpcn << endmsg;
    //if((pcnfrequencies.rbegin()->first )!= m_tell1Ids.size()  ) {
    for(ipcn  =  pcn_tell1s.begin(); ipcn!= pcn_tell1s.end(); ipcn++){
      if (ipcn->first != rightpcn || m_debugmode){   
        //info() << "wrong pcn! << " << ipcn->first << " for tells " ;
        for(std::vector<int>::iterator itell = (ipcn->second).begin(); itell != (ipcn->second).end(); itell++){
          //info() << *itell << ", " ;
          m_asyncTells->fill(ipcn->first,*itell);
        }
        //info() << endmsg;
      }
    } 
    //}
  }

  for ( unsigned int i=0; i<m_tell1Ids.size(); ++i ) {
    unsigned int tell1Number = m_tell1Ids[i];
    unsigned long& events = m_nEventsByTell1[tell1Number];

    if ( 0 != m_nEvts && 0 == m_nEvts % m_evtsPerTell1PubRate ) { 
      if ( events < m_evtsPerTell1PubRate ) {
        m_evtsPerTell1->fill(tell1Number,m_evtsPerTell1PubRate-events);
      }
      events = 0;
    }
  }
}   

void Velo::Monitoring::ErrorMon::monitorErrorBanks(const VeloErrorBanks* banks)
{
  // monitor the decoded error banks for specific errors
  VeloErrorBanks::const_iterator eBIt;
  for(eBIt=banks->begin(); eBIt!=banks->end(); eBIt++){

    const VeloErrorBank* errorBank =(*eBIt);

    if ( errorBank->isEmpty() ) { // nothing to see here, please move along.
      continue;
    }

    const EvtInfo* infoPtr=errorBank->evtInfo();

    unsigned int tell1Number = errorBank->key();
    //info() << "tell1= " << tell1Number << endmsg;


    //unsigned int num_pseudoerr = 0;
    //unsigned int num_pcnerr = 0;
    unsigned int num_iheaderr = 0;
    //unsigned int num_fifoerr = 0;

    for ( unsigned int pp=0; pp<4; ++pp) {

      if ( 0 == errorBank->errorBankLength(pp) ) {
        continue;

      }

      unsigned int pseudo_error  = infoPtr->headerPseudoErrorFlag(pp);
      //unsigned int pcn_error     = infoPtr->PCNError(pp);
      unsigned int FIFO_error    = infoPtr->adcFIFOError(pp);
      //unsigned int channel_error = infoPtr->channelError(pp);
      dataVec IHeader_error = infoPtr->IHeader(pp);
      dataVec::const_iterator IHeaderIT;
      dataVec PCN_error = infoPtr->PCNBeetle(pp);
      dataVec::const_iterator PCNIT;

      //unsigned int ppbeetle;
      //unsigned int Beetle;
      //unsigned int ppbeetle2;
      //unsigned int Beetle2;
      unsigned int Link; 
      unsigned int link=0;
      unsigned int ppBtl = 0;
      unsigned int ppBtl2 = 0;
      unsigned int IheaderBeetle[4][4] ={{0}};
      unsigned int PCNBeetle[4][4] ={{0}};

      int h_linkmask0 = 17;    //= 2*4 +2*0;
      int h_linkmask1 = 34;    //= 2*5 +2*1;
      int h_linkmask2 = 68;    //= 2*6 +2*2;
      int h_linkmask3 = 136;   //= 2*7 +2*3;

      int p_linkmask0 = 3;     //= 2*1 +2*0;
      int p_linkmask1 = 12;    //= 2*3 +2*2;
      int p_linkmask2 = 48;    //= 2*5 +2*4;
      int p_linkmask3 = 192;   //= 2*7 +2*6;


      //info() << " pp= " << pp << " iheader= " << IHeader_error << endmsg;
      //info() << " pp= " << pp << " pcn= " << PCN_error << endmsg;
      //info() << " pp= " << pp << " pseudo= " << pseudo_error << endmsg;
      //info() << " pp= " << pp << " fifo= " << FIFO_error << endmsg;

      bool noIHEADerror=false;
      for(IHeaderIT=IHeader_error.begin() ; IHeaderIT!=IHeader_error.end(); ++IHeaderIT, ++ppBtl){
        IheaderBeetle[pp][ppBtl] = (*IHeaderIT);

        //error() << " event= " << m_evtNumber << " pp= " << pp << " beetle=  " << ppBtl << " Iheader= " << IheaderBeetle[pp][ppBtl]<< endmsg;
      }
      if(IheaderBeetle[pp][0] != IheaderBeetle[pp][1] &&
          IheaderBeetle[pp][0] != IheaderBeetle[pp][2] &&
          IheaderBeetle[pp][0] != IheaderBeetle[pp][3]){

        if((IheaderBeetle[pp][0] & h_linkmask0) != (IheaderBeetle[pp][3] & h_linkmask0)){
          link = 0;
        }
        if((IheaderBeetle[pp][0] & h_linkmask1) != (IheaderBeetle[pp][3] & h_linkmask1)){
          link = 1;
        }
        if((IheaderBeetle[pp][0] & h_linkmask2) != (IheaderBeetle[pp][3] & h_linkmask2)){
          link = 2;
        }
        if((IheaderBeetle[pp][0] & h_linkmask3) != (IheaderBeetle[pp][3] & h_linkmask3)){
          link = 3;
        }

        //ppbeetle = 0;
      }

      if(IheaderBeetle[pp][1] != IheaderBeetle[pp][0] &&
          IheaderBeetle[pp][1] != IheaderBeetle[pp][2] &&
          IheaderBeetle[pp][1] != IheaderBeetle[pp][3]){

        if((IheaderBeetle[pp][1] & h_linkmask0) != (IheaderBeetle[pp][0] & h_linkmask0)){
          link = 4;
        }
        if((IheaderBeetle[pp][1] & h_linkmask1) != (IheaderBeetle[pp][0] & h_linkmask1)){
          link = 5;
        }
        if((IheaderBeetle[pp][1] & h_linkmask2) != (IheaderBeetle[pp][0] & h_linkmask2)){ 
          link = 6;
        }
        if((IheaderBeetle[pp][1] & h_linkmask3) != (IheaderBeetle[pp][0] & h_linkmask3)){ 
          link = 7;
        }

        //ppbeetle = 1;
      }

      if(IheaderBeetle[pp][2] != IheaderBeetle[pp][0] &&
          IheaderBeetle[pp][2] != IheaderBeetle[pp][1] &&
          IheaderBeetle[pp][2] != IheaderBeetle[pp][3]){

        if((IheaderBeetle[pp][2] & h_linkmask0) != (IheaderBeetle[pp][1] & h_linkmask0)){ 
          link = 8;
        }
        if((IheaderBeetle[pp][2] & h_linkmask1) != (IheaderBeetle[pp][1] & h_linkmask1)){ 
          link = 9;
        }
        if((IheaderBeetle[pp][2] & h_linkmask2) != (IheaderBeetle[pp][1] & h_linkmask2)){ 
          link = 10;
        }
        if((IheaderBeetle[pp][2] & h_linkmask3) != (IheaderBeetle[pp][1] & h_linkmask3)){
          link = 11;
        }

        //ppbeetle = 2;
      }

      if(IheaderBeetle[pp][3] != IheaderBeetle[pp][0] &&
          IheaderBeetle[pp][3] != IheaderBeetle[pp][1] &&
          IheaderBeetle[pp][3] != IheaderBeetle[pp][2]){

        if((IheaderBeetle[pp][3] & h_linkmask0) != (IheaderBeetle[pp][2] & h_linkmask0)){
          link = 12;
        }
        if((IheaderBeetle[pp][3] & h_linkmask1) != (IheaderBeetle[pp][2] & h_linkmask1)){
          link = 13;
        }
        if((IheaderBeetle[pp][3] & h_linkmask2) != (IheaderBeetle[pp][2] & h_linkmask2)){ 
          link = 14;
        }
        if((IheaderBeetle[pp][3] & h_linkmask3) != (IheaderBeetle[pp][2] & h_linkmask3)){
          link = 15;
        }

        //ppbeetle = 3;
      }

      if(IheaderBeetle[pp][0] == IheaderBeetle[pp][1] &&
          IheaderBeetle[pp][1] == IheaderBeetle[pp][2] &&
          IheaderBeetle[pp][2] == IheaderBeetle[pp][3]){
        //error() << " WOOHOO... no error!!! " << endmsg;

        noIHEADerror=true;

      }

      //================== I WANT TO CHECK IF TWO IHEADER VALUES ARE DIFFERENT ==============================

      //if(((IheaderBeetle[pp][0] != IheaderBeetle[pp][1]) & (IheaderBeetle[pp][0] != IheaderBeetle[pp][2]) & (IheaderBeetle[pp][0] == IheaderBeetle[pp][3])) | 
      // ((IheaderBeetle[pp][0] != IheaderBeetle[pp][2]) & (IheaderBeetle[pp][0] != IheaderBeetle[pp][3]) & (IheaderBeetle[pp][0] == IheaderBeetle[pp][1])) | 
      // ((IheaderBeetle[pp][0] != IheaderBeetle[pp][3]) & (IheaderBeetle[pp][0] != IheaderBeetle[pp][1]) & (IheaderBeetle[pp][0] == IheaderBeetle[pp][2])) | 

      // ((IheaderBeetle[pp][1] != IheaderBeetle[pp][0]) & (IheaderBeetle[pp][1] != IheaderBeetle[pp][2]) & (IheaderBeetle[pp][1] == IheaderBeetle[pp][3])) | 
      // ((IheaderBeetle[pp][1] != IheaderBeetle[pp][2]) & (IheaderBeetle[pp][1] != IheaderBeetle[pp][3]) & (IheaderBeetle[pp][1] == IheaderBeetle[pp][0])) | 
      // ((IheaderBeetle[pp][1] != IheaderBeetle[pp][3]) & (IheaderBeetle[pp][1] != IheaderBeetle[pp][0]) & (IheaderBeetle[pp][1] == IheaderBeetle[pp][2])) | 

      // ((IheaderBeetle[pp][2] != IheaderBeetle[pp][0]) & (IheaderBeetle[pp][2] != IheaderBeetle[pp][1]) & (IheaderBeetle[pp][2] == IheaderBeetle[pp][3])) | 
      // ((IheaderBeetle[pp][2] != IheaderBeetle[pp][1]) & (IheaderBeetle[pp][2] != IheaderBeetle[pp][3]) & (IheaderBeetle[pp][2] == IheaderBeetle[pp][0])) | 
      // ((IheaderBeetle[pp][2] != IheaderBeetle[pp][3]) & (IheaderBeetle[pp][2] != IheaderBeetle[pp][0]) & (IheaderBeetle[pp][2] == IheaderBeetle[pp][1])) | 

      // ((IheaderBeetle[pp][3] != IheaderBeetle[pp][0]) & (IheaderBeetle[pp][3] != IheaderBeetle[pp][1]) & (IheaderBeetle[pp][3] == IheaderBeetle[pp][2])) | 
      // ((IheaderBeetle[pp][3] != IheaderBeetle[pp][1]) & (IheaderBeetle[pp][3] != IheaderBeetle[pp][2]) & (IheaderBeetle[pp][3] == IheaderBeetle[pp][0])) | 
      // ((IheaderBeetle[pp][3] != IheaderBeetle[pp][2]) & (IheaderBeetle[pp][3] != IheaderBeetle[pp][0]) & (IheaderBeetle[pp][3] == IheaderBeetle[pp][1]))){

      //error() << " double iheader error!!! " << endmsg;

      //m_errorDouble->fill(tell1Number, IHEAD);

      //}


      if(!noIHEADerror){
        num_iheaderr++;

        //Beetle = pp*4 + ppbeetle;
        Link = pp*16 + link;

        //error() << " local_beetle= " << ppbeetle << " Beetle= " << Beetle << " Link= " << Link <<endmsg;

        m_overviewiheader->fill(tell1Number, Link);
        m_errorType->fill(tell1Number,IHEADERR);
        m_errNum->fill(tell1Number);
      }

      //error() << " L0= " << h_linkmask0 << " L1= " << h_linkmask1 << " L2= " << h_linkmask2 << " L3 = " << h_linkmask3 <<endmsg;
      bool noPCNerror=false;

      for(PCNIT=PCN_error.begin() ; PCNIT!=PCN_error.end(); ++PCNIT, ++ppBtl2){
        PCNBeetle[pp][ppBtl2] = (*PCNIT);

        //error() << " event= " << m_evtNumber << " pp= " << pp << " beetle=  " << ppBtl2 << " PCN_err= " << PCNBeetle[pp][ppBtl2] << endmsg;
      }

      if(PCNBeetle[pp][0] != PCNBeetle[pp][1] &&
          PCNBeetle[pp][0] != PCNBeetle[pp][2] &&
          PCNBeetle[pp][0] != PCNBeetle[pp][3]){

        if((PCNBeetle[pp][0] & p_linkmask0) != (PCNBeetle[pp][3] & p_linkmask0)){
          link = 0;
        }
        if((PCNBeetle[pp][0] & p_linkmask1) != (PCNBeetle[pp][3] & p_linkmask1)){
          link = 1;
        }
        if((PCNBeetle[pp][0] & p_linkmask2) != (PCNBeetle[pp][3] & p_linkmask2)){
          link = 2;
        }
        if((PCNBeetle[pp][0] & p_linkmask3) != (PCNBeetle[pp][3] & p_linkmask3)){
          link = 3;
        }

        //ppbeetle2 = 0;
      }

      if(PCNBeetle[pp][1] != PCNBeetle[pp][0] &&
          PCNBeetle[pp][1] != PCNBeetle[pp][2] &&
          PCNBeetle[pp][1] != PCNBeetle[pp][3]){

        if((PCNBeetle[pp][1] & p_linkmask0) != (PCNBeetle[pp][0] & p_linkmask0)){
          link = 4;
        }
        if((PCNBeetle[pp][1] & p_linkmask1) != (PCNBeetle[pp][0] & p_linkmask1)){
          link = 5;
        }
        if((PCNBeetle[pp][1] & p_linkmask2) != (PCNBeetle[pp][0] & p_linkmask2)){
          link = 6;
        }
        if((PCNBeetle[pp][1] & p_linkmask3) != (PCNBeetle[pp][0] & p_linkmask3)){
          link = 7;
        }

        //ppbeetle2 = 1;
      }

      if(PCNBeetle[pp][2] != PCNBeetle[pp][0] &&
          PCNBeetle[pp][2] != PCNBeetle[pp][1] &&
          PCNBeetle[pp][2] != PCNBeetle[pp][3]){

        if((PCNBeetle[pp][2] & p_linkmask0) != (PCNBeetle[pp][1] & p_linkmask0)){
          link = 8;
        }
        if((PCNBeetle[pp][2] & p_linkmask1) != (PCNBeetle[pp][1] & p_linkmask1)){
          link = 9;
        }
        if((PCNBeetle[pp][2] & p_linkmask2) != (PCNBeetle[pp][1] & p_linkmask2)){
          link = 10;
        }
        if((PCNBeetle[pp][2] & p_linkmask3) != (PCNBeetle[pp][1] & p_linkmask3)){
          link = 11;
        }

        //ppbeetle2 = 2;
      }

      if(PCNBeetle[pp][3] != PCNBeetle[pp][0] &&
          PCNBeetle[pp][3] != PCNBeetle[pp][1] &&
          PCNBeetle[pp][3] != PCNBeetle[pp][2]){

        if((PCNBeetle[pp][3] & p_linkmask0) != (PCNBeetle[pp][2] & p_linkmask0)){
          link = 12;
        }
        if((PCNBeetle[pp][3] & p_linkmask1) != (PCNBeetle[pp][2] & p_linkmask1)){
          link = 13;
        }
        if((PCNBeetle[pp][3] & p_linkmask2) != (PCNBeetle[pp][2] & p_linkmask2)){
          link = 14;
        }
        if((PCNBeetle[pp][3] & p_linkmask3) != (PCNBeetle[pp][2] & p_linkmask3)){
          link = 15;
        }

        //ppbeetle2 = 3;
      }

      if(PCNBeetle[pp][0] == PCNBeetle[pp][1] &&
          PCNBeetle[pp][1] == PCNBeetle[pp][2] &&
          PCNBeetle[pp][2] == PCNBeetle[pp][3]){

        //error() << " WOOHOO... no error!!! " << endmsg;
        noPCNerror=true;

      }
      else{
        //error() << PCNBeetle[pp][0] << "  " <<PCNBeetle[pp][1] << "  " <<PCNBeetle[pp][2] << "  " <<PCNBeetle[pp][3] <<endmsg;
      }

    //  if(((PCNBeetle[pp][0] != PCNBeetle[pp][1]) & (PCNBeetle[pp][0] != PCNBeetle[pp][2]) & (PCNBeetle[pp][0] = PCNBeetle[pp][3])) || 
    //      ((PCNBeetle[pp][0] != PCNBeetle[pp][2]) & (PCNBeetle[pp][0] != PCNBeetle[pp][3]) & (PCNBeetle[pp][0] = PCNBeetle[pp][1])) || 
    //      ((PCNBeetle[pp][0] != PCNBeetle[pp][3]) & (PCNBeetle[pp][0] != PCNBeetle[pp][1]) & (PCNBeetle[pp][0] = PCNBeetle[pp][2])) || 
    //
    //      ((PCNBeetle[pp][1] != PCNBeetle[pp][0]) & (PCNBeetle[pp][1] != PCNBeetle[pp][2]) & (PCNBeetle[pp][1] = PCNBeetle[pp][3])) || 
    //      ((PCNBeetle[pp][1] != PCNBeetle[pp][2]) & (PCNBeetle[pp][1] != PCNBeetle[pp][3]) & (PCNBeetle[pp][1] = PCNBeetle[pp][0])) || 
    //      ((PCNBeetle[pp][1] != PCNBeetle[pp][3]) & (PCNBeetle[pp][1] != PCNBeetle[pp][0]) & (PCNBeetle[pp][1] = PCNBeetle[pp][2])) || 
    //
    //      ((PCNBeetle[pp][2] != PCNBeetle[pp][0]) & (PCNBeetle[pp][2] != PCNBeetle[pp][1]) & (PCNBeetle[pp][2] = PCNBeetle[pp][3])) || 
    //      ((PCNBeetle[pp][2] != PCNBeetle[pp][1]) & (PCNBeetle[pp][2] != PCNBeetle[pp][3]) & (PCNBeetle[pp][2] = PCNBeetle[pp][0])) || 
    //      ((PCNBeetle[pp][2] != PCNBeetle[pp][3]) & (PCNBeetle[pp][2] != PCNBeetle[pp][0]) & (PCNBeetle[pp][2] = PCNBeetle[pp][1])) || 
    //
    //      ((PCNBeetle[pp][3] != PCNBeetle[pp][0]) & (PCNBeetle[pp][3] != PCNBeetle[pp][1]) & (PCNBeetle[pp][3] = PCNBeetle[pp][2])) || 
    //      ((PCNBeetle[pp][3] != PCNBeetle[pp][1]) & (PCNBeetle[pp][3] != PCNBeetle[pp][2]) & (PCNBeetle[pp][3] = PCNBeetle[pp][0])) || 
    //      ((PCNBeetle[pp][3] != PCNBeetle[pp][2]) & (PCNBeetle[pp][3] != PCNBeetle[pp][0]) & (PCNBeetle[pp][3] = PCNBeetle[pp][1]))){
    //
    //    //error() << " double pcn error!!! " << endmsg;
    //
    //    //m_errorDouble->fill(tell1Number, PCN);
    //
    //  } ///////////// removed as I think the last entry should be a comparison (not setting PCNBeetle! DEH 



      if(!noPCNerror){

        //num_pcnerr++;

        //Beetle2 = pp*4 + ppbeetle2;

        Link = pp*16 + link;

        //error() << " local_beetle= " << ppbeetle2 << " Beetle= " << Beetle2 << " Link= " << Link <<endmsg;

        m_overviewpcn->fill(tell1Number, Link);
        m_errorType->fill(tell1Number,PCNERR);
        //m_errorType->fill(tell1Number,CHANERR);
        m_errNum->fill(tell1Number);
      }

      for (unsigned int ppLink=0; ppLink<16; ++ppLink) {
        unsigned int ppLinkMask = 1 << ppLink;
        unsigned int link = pp*16 + ppLink;

        if ( 0 != (pseudo_error & ppLinkMask ) ) {
          m_overviewpseudo->fill(tell1Number, link);
          //error() << " event= " << m_evtNumber << " pseudo error= "<< pseudo_error << " Link= " << link << endmsg;

        }

        if ( 0 != (FIFO_error & ppLinkMask) ) {
          m_overviewadcfifo->fill(tell1Number, link);
        }

        //if ( 0 != (channel_error & ppLinkMask) ) {
        //m_overviewchannel->fill(tell1Number, link);
        //}

      }

      if(pseudo_error !=0) {

        //num_pseudoerr++;

        m_errorType->fill(tell1Number,PSEUDOERR);
        m_errNum->fill(tell1Number);


      } 

      if(FIFO_error !=0){
        //num_fifoerr++;

        m_errorType->fill(tell1Number,FIFOERR);
        m_errNum->fill(tell1Number);
      }

      if(pseudo_error !=0) {

        //num_pseudoerr++;

        //m_errorType->fill(tell1Number,CHANERR);
      }
    }


  }
} 

void Velo::Monitoring::ErrorMon::printSummary() 
{
  info() << "================ VELO ZS Bank Error Report ================" << endmsg;  
  for (std::map<unsigned int, unsigned long>::const_iterator iT1 = m_nErrorsByTell1.begin();
      iT1 != m_nErrorsByTell1.end();
      iT1++) {
    double err_ratio = double(iT1->second) / double(m_nEvts);//double(m_nEventsByTell1[iT1->first]);
    info() << "TELL1: " << iT1->first << "\t# EVENTS: " << m_nEvts << "\t# ERRORS: " << iT1->second << "\tERROR FREQUENCY= " << err_ratio << endmsg;   
    if(err_ratio != 0 && err_ratio != 1){
      m_errFreq->fill(iT1->first,err_ratio);
    }
  }
  info() << "===========================================================" << endmsg;
  info() << "Number of error banks= " << m_bankCounter << endmsg;  

  m_sensors = 0;

  for ( unsigned int i=0; i<m_tell1Ids.size(); ++i ) {
    unsigned int tell1Number = m_tell1Ids[i];
    unsigned long& errors = m_nErrorsByTell1[tell1Number];
    if(errors != 0){
      m_sensors++;
    }
  }
  info() << "===========================================================" << endmsg;
  info() << "# of Sensors with Errors= " << m_sensors << endmsg;  
  info() << "===========================================================" << endmsg;

}
