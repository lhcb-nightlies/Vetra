//Header file with functions that are handy to access for different algorithms in the VELO monitoring.

#include <map>
#include <string>

#include "GaudiAlg/GaudiHistoAlg.h"
//#include "GaudiKernel/AlgFactory.h" 

#include "Event/RawEvent.h"
#include "Event/RawBank.h"

#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloErrorBank.h"

#include "VeloEvent/VeloProcessInfo.h"
#include "VeloEvent/VeloErrorBank.h"

#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"

#include <fstream>
#include "TFile.h"
#include "TH2F.h"
#include "TGraph.h"
#include "TTree.h"
#include "TString.h"


namespace Velo
{
  namespace Monitoring
  {
    class ADCTuple : public GaudiHistoAlg {
    public:

       
      /// Standard constructor
      ADCTuple( const std::string& name, ISvcLocator* pSvcLocator );
      
      virtual ~ADCTuple( );               ///< Destructor
      virtual StatusCode initialize() override;    ///< Algorithm initialization
      virtual StatusCode execute   () override;    ///< Algorithm execution
      virtual StatusCode finalize  () override;    ///< Algorithm finalization
     
      StatusCode  plotADCs();
 
           
    private  :
      

      std::string m_errorBankLoc;
      std::string m_rawEventLocation;  
      std::string m_NtupleLocation;  
        
      unsigned long int  m_evtNumber;
      int m_refresh;
      
      const DeVelo* m_veloDet;

      TFile* EB_Histograms;
      std::ofstream m_evtList;

      TH2F* Links;
      TH1F* Channel_adc;

      TGraph* graph;

      TFile* f;
      TTree* t;

      int m_tell1;
      int link;
      int channel;

      TString num;

      std::vector<float> header_vector;
      std::vector<float> channel_vector;
      std::vector<float> Tell1_vector;
      std::vector<float> L0ID_vector;
 
      std::vector<int> m_chosenTell1s;
      std::vector<int> m_chosenLinks;
      std::vector<std::string>  m_locations;

      bool m_plot2DADCs;
      bool m_evenodd;
      bool m_LinkPlot;
      bool OddEvent() {if( m_evtNumber%2==1) return  true; else return false; }
      bool EvenEvent() {if( m_evtNumber%2==0) return  true; else return false; }

      AIDA::IProfile1D* m_headers; 
      AIDA::IProfile1D* m_channels; 

      //    bool EvenEvent() { return ( m_evtNumber%2==0 ? true : false ); }
      
      //----------------------------
      //Returns a vector with the link ADCs.
      std::vector<int> getLink(VeloTELL1::ALinkPair begEnd) {   
	std::vector<int>::const_iterator iT; //iterator to vector of unysigned int.
	std::vector<int> linkVector;
	linkVector.clear();
	//Loop over ADC values in link
	for(iT=begEnd.first; iT!=begEnd.second; ++iT){    
	  linkVector.push_back(*iT); //Push back ADC value to vector
	}
	return linkVector;
      }
    
    /// replace all symbols in the string 
    inline void replaceAll 
      ( std::string& a , const char c1 , const char c2 = '_') 
      {
	std::string::size_type pos = a.find ( c1 ) ;
	while ( std::string::npos != pos ) { a [ pos ] = c2 ; pos = a.find ( c1 ) ; }
      }
    /// construct the "stripped" name form valid TES location 
    inline std::string strippedContainerName ( std::string location ) 
      {
	std::string::size_type pos = location.find ( "Raw/Velo/" ) ;
	if ( std::string::npos != pos ) 
	  { location = std::string ( location , pos + 9 ) ; }
	replaceAll ( location , '.' ) ;
	replaceAll ( location , ':' ) ;
	replaceAll ( location , '/' ) ;
	return location ;
	
      }
    
    inline bool ExcludeTELL1(std::vector<int> m_chosenTell1s, int TELL1_number){   
      
      if(m_chosenTell1s.size()>0 && 
	 find(m_chosenTell1s.begin(), m_chosenTell1s.end(), TELL1_number)==m_chosenTell1s.end())
	{ return true ; }
      else
	return false;
      
    }          
    inline bool ExcludeLink(std::vector<int> m_chosenLinks, int link){   
      
      if(m_chosenLinks.size()>0 && 
	 find(m_chosenLinks.begin(), m_chosenLinks.end(), link)==m_chosenLinks.end())
	{ return true ; }
      else
	return false;
      
    }          
    
       
    };
  }
}
