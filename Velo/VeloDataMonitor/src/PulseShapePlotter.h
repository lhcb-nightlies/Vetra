// $Id: PulseShapePlotter.h,v 1.1 2009-07-15 17:58:45 kakiba Exp $
#ifndef VELODATAMONITOR_PULSESHAPEFITTERTED 
#define VELODATAMONITOR_PULSESHAPEFITTERTED 1

// Include files
// -------------
#include "VeloDet/DeVelo.h"

// from VeloEvent
#include "VeloEvent/VeloTELL1Data.h"

// from DigiEvent
#include "Event/VeloCluster.h"

// local
#include "VeloMonitorBase.h"


#include "Math/ParamFunctor.h"
#include "GaudiKernel/IEventTimeDecoder.h"            // Interface for odin

#include "Event/ODIN.h"

/** @class PulseShapePlotter PulseShapePlotter.h
 *  
 *
 *  @author Kazu  
 *  @date   2008-10-22
 */

namespace Velo {



  class PulseShapePlotter : public VeloMonitorBase {
  public: 
    /// Standard constructor
    PulseShapePlotter( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~PulseShapePlotter( );    ///< Destructor
    virtual StatusCode initialize() override;    ///< Algorithm initialization
    virtual StatusCode execute   () override;    ///< Algorithm execution
    virtual StatusCode finalize  () override;    ///< Algorithm finalization
    long   m_nEvents;
    double m_threshold;
    double m_offset;
    double m_width1;
    double m_width2;
    double m_chisqCut;
    double m_cmsCut;

    double m_integralCut;
    double m_timeWindow;
    double m_expectedPeakTime;
    double m_nTimeBins;
    bool   m_runWithOdin; // 
    bool   m_fixedOffset;
    bool   m_tryCluster;
    bool   m_plotSeparateStrips;
    bool   m_includePileUp;
    bool   m_skipStepZero;
    bool   m_selectBunchCrossing;
    bool   m_rejectHighADCOuterSamples;
    double m_maxADCOuterSamples;
    double m_thresholdForCMS;
    double m_amplitudeCut;
    double m_offsetCut;
    bool   m_useLogLikeFit;
    //int    m_nSteps;
    int    m_shift;
    //int    m_stepSize;
    unsigned int    m_prevRunNumber;
    //int    m_eventsPerFile;
    int    m_eventsThisFile;
    int    m_takeNeighbours;
    int    m_wrapPoint;
    int    m_wrappedSteps;
    int    m_currentCalStep;
    int    tell1smatch[5],tell1sorder[88];
    int    m_stripFromChannel[2][2048];
    int    m_channelFromStrip[2][2048];
    int    m_sensorType;
    bool   m_inverted, m_detailedOutput;
    std::vector<int> m_chosenTell1s;
    std::vector<int> m_pulsedChannels;
    std::vector<int> m_runNumbers;
    std::vector<double> m_runNumbersDelay;
    std::vector<int> m_chosenBunches;
    bool   m_removeHeaders;
    //bool   m_useBCNT;
    std::map<int, double> m_runToDelay;
    std::map<int, int> m_eventPerStepCounter;

    double subtractCommonMode(int channelNumber,  LHCb::VeloTELL1Data *tell1data);
    std::vector<int> getLink(VeloTELL1::ALinkPair begEnd);
    //std::vector<int> getLink();
    std::vector<double> SubtractCommonMode( std::vector<int> data, double cut );
    int    m_nEventsPerStep;
    int    m_numberOfCMSIterations;



  protected:
  private:
    IEventTimeDecoder* m_timeDecoder; ///< Pointer to tool to decode ODIN bank
    // Retrieve the VeloTell1Data
    LHCb::VeloTELL1Datas* veloTell1Data( std::string samplingLocation );
    // Monitor the VeloTell1Data
    //void MakePulseShape( std::string samplingLocation, LHCb::VeloTELL1Datas* tell1Datas );

    // Data members
    const DeVelo* m_velo;
    // Job options
    std::vector<std::string> m_samplingLocations;
    std::string m_dataLocation;
  };
}
#endif // VELODATAMONITOR_PULSESHAPEFITTERTED
