// $Id: PulseShapeFitterTed.cpp,v 1.2 2009-09-20 18:45:17 szumlat Exp $
// Include files
// -------------
#include <iostream>
#include <fstream>
// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
// from Tell1Kernel
#include "Tell1Kernel/VeloDecodeConf.h" 
#include "Event/ODIN.h"
#include "Event/VeloODINBank.h"
// local
#include "PulseShapeFitterTed.h"
#include "CommonFunctions.h"
// from Boost
#include <boost/assign/list_of.hpp>
#include "boost/lexical_cast.hpp"
// from Gaudi
#include "AIDA/IHistogram2D.h"
// local
// Kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"

#include "Event/ODIN.h"
#include "DetDesc/Condition.h"

using namespace VeloTELL1;

//-----------------------------------------------------------------------------
// Implementation file for class : PulseShapeFitterTed
// Author: Kazu and Ivan Mous.
// This code should fit pulse shapes to
// Non zero suppressed data and find the peak time.
// the rising and falling times have to be externally  measured...
// the code can also cope with a relative delay input and plot the 
// "clusters" found against time.
//-----------------------------------------------------------------------------
// Declaration of the Algorithm Factory
namespace Velo {
  DECLARE_ALGORITHM_FACTORY( PulseShapeFitterTed )
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  Velo::PulseShapeFitterTed::PulseShapeFitterTed( const std::string& name, ISvcLocator* pSvcLocator)
  : Velo::VeloMonitorBase ( name , pSvcLocator )
,m_nEvents(0)
{
  std::vector<std::string> tmpLocations = 
	boost::assign::list_of("Prev7")("Prev6")("Prev5")("Prev4")("Prev3")("Prev2")("Prev1")
	("")("Next1")("Next2")("Next3")("Next4")("Next5")("Next6")("Next7");
  declareProperty( "SamplingLocations", m_samplingLocations = tmpLocations);
  //declareProperty( "NSteps", m_nSteps= 1 );
  declareProperty( "ChisqCut", m_chisqCut= 10 );
  declareProperty( "CMSCut", m_cmsCut= 10 );
  declareProperty( "MaxAmplitudeCut", m_amplitudeCut= 300);
  declareProperty( "OffsetCut", m_offsetCut= 10 );
  declareProperty( "IntegralCut", m_integralCut= 10 );
  declareProperty( "UseLogLikeFit", m_useLogLikeFit= true );
  declareProperty( "TimeWindow", m_timeWindow= 150 );
  declareProperty( "ExpectedPeakTime", m_expectedPeakTime= 0 );
  declareProperty( "NTimeBins", m_nTimeBins = 201 );
  declareProperty( "FixOffset", m_fixedOffset= false );
  declareProperty( "TryToClusterize", m_tryCluster= false );
  declareProperty( "TELL1s",m_chosenTell1s     );
  declareProperty( "SignalThreshold" , m_threshold=20);
  declareProperty( "TakeNeighbours", m_takeNeighbours=1 );
  declareProperty( "PlotSeparateStrips", m_plotSeparateStrips=false );
  declareProperty( "Offset" ,m_offset=0);
  declareProperty( "WidthBefore", m_width1=13.68);
  declareProperty( "WidthAfter", m_width2=26.76);
  //declareProperty( "DetailedOutput" , m_detailedOutput= true);
  declareProperty( "RunWithOdin" , m_runWithOdin = true ); // 
  declareProperty( "RunNumbers", m_runNumbers );
  declareProperty( "RunNumbersDelay", m_runNumbersDelay );
  declareProperty( "ChosenBunchCounters", m_chosenBunches );
  declareProperty( "RunWithChosenBunchCounters", m_useBCNT = true );
  declareProperty( "SkipStepZero", m_skipStepZero= true );
  declareProperty( "NEventsPerStep", m_nEventsPerStep= -1  );
  declareProperty( "OnlyStepZero", m_onlyStepZero= false );
  declareProperty( "RemoveHeaders", m_removeHeaders=false );
  declareProperty( "SelectBunchCrossing", m_selectBunchCrossing =false );

  m_runNumbers.push_back(-1);
  m_runNumbersDelay.push_back(0);
  m_chosenTell1s.push_back(-1);
  
}
//=============================================================================
// Destructor
//=============================================================================
Velo::PulseShapeFitterTed::~PulseShapeFitterTed() {} 
//=============================================================================
// Initialization
//=============================================================================
StatusCode Velo::PulseShapeFitterTed::initialize() {
  StatusCode sc = VeloMonitorBase::initialize(); // must be executed first
  printProps();
  //if(m_nSteps==1) m_stepSize=1000000; 
  m_timeDecoder = tool<IEventTimeDecoder>( "OdinTimeDecoder" );
  if ( sc.isFailure() ) return sc;
  m_velo = getDet<DeVelo>( DeVeloLocation::Default );  
  //const int strips=2048;
  int strip_tmp=0,channel_tmp=0;
  std::ifstream ifsR("/home/kakiba/pccChannel-mapping.twocol");
  std::ifstream ifsP("/home/kakiba/rccChannel-mapping.twocol");
  char dummy[100];
  ifsR.getline(dummy,100);
  for(int ichan = 0; ichan<2048; ichan++){
	ifsR>>strip_tmp>>channel_tmp;
	m_stripFromChannel[1][channel_tmp]=strip_tmp;  
	m_channelFromStrip[1][strip_tmp]=channel_tmp;  
	debug() << " getting reordering data Phi strip " << strip_tmp <<  " -> channel " << channel_tmp<< endmsg;
  }
  ifsP.getline(dummy,100);
  for(int jchan = 0; jchan<2048; jchan++){
	ifsP>>strip_tmp>>channel_tmp;
	m_stripFromChannel[0][channel_tmp]=strip_tmp;  
	m_channelFromStrip[0][strip_tmp]=channel_tmp;                          
	debug() << " getting reordering data R  strip " << strip_tmp <<  " -> channel " << channel_tmp<< endmsg;
  }
  ifsP.close();
  ifsR.close();

  if(m_runNumbers.size() == m_runNumbersDelay.size() &&  m_runNumbersDelay.size() >0  ){
	std::vector<int>::const_iterator irunNumber; int i =0;
	for( irunNumber =m_runNumbers.begin(); irunNumber!=m_runNumbers.end(); irunNumber++, i++){
	  m_runToDelay[*irunNumber] = m_runNumbersDelay[i]; 
	  info() << " run number = " <<  *irunNumber << " corresponds to delay " << m_runToDelay[*irunNumber] << endmsg;
	}
  }
  else info() << " check out option file for the delays and runs cause something seems wrong" << endmsg;
  return StatusCode::SUCCESS;
}

//=============================================================================
/// Main execution
//=============================================================================
StatusCode Velo::PulseShapeFitterTed::execute() {
  counter( "# events" ) += 1;
  int finebin = 0;
  int currentRunNumber;
  std::vector<std::string>::const_iterator itL;
  //check middle sample for the tell1Data:
  const int midSampleIndex = (int)floor(m_samplingLocations.size()/2.);
  const int nSamples = m_samplingLocations.size();
  for(int isample = 0; isample < nSamples ; isample++){
    LHCb::VeloTELL1Datas *SampleData = veloTell1Data(m_samplingLocations[isample]);
    if( SampleData == 0 ){  
      debug() << "Sampledata of sample " << isample << " is empty! RETURN "<< endmsg;
      return StatusCode::SUCCESS;
    }
  }
  
  LHCb::VeloTELL1Datas *midSampleData = veloTell1Data(m_samplingLocations[midSampleIndex]);
  LHCb::VeloTELL1Datas::const_iterator itD;

#ifndef WIN32
  pulse = new TH1D("pulse","pulse",nSamples*50,-(nSamples/2 +0.1)*25.,(nSamples/2+0.01)*25.); //For fixed bins of 0.5 ns
#endif
  
  double (*fittest)(double *, double *) = NULL;
  fittest = &Velo::fitsimple;
  func = new TF1("dtfitfunc",fittest,-200.,200.,5);
  func->SetParameters(0,20,-10,m_width1,m_width2);
  func->SetParLimits(3,m_width1,m_width1);
  func->SetParLimits(4,m_width2,m_width2);
  func->FixParameter(3,m_width1);
  func->FixParameter(4,m_width2);
  func->SetParLimits(2, m_expectedPeakTime - m_timeWindow,m_expectedPeakTime + m_timeWindow);
  func->SetParLimits(0, -10,10);
  func->SetParLimits(1, 0,200);

  if(m_fixedOffset) func->FixParameter(0,0);

  char fitvalues_offset[150]; 
  char fitvalues_ampltiude[150]; 
  char fitvalues_peak[150];
  char finepulseshape[150];
  char pulseshape[150];
  double delay=0;

  LHCb::ODIN* odin=0;
  if (m_runWithOdin){
	if(exist<LHCb::ODIN>((LHCb::ODINLocation::Default))) {
	  odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
	}
    else{
	  m_timeDecoder->getTime(); // in case we want to get other info from odin.
	  odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
	}	
  }
  else m_runNumbers[0] = -1;
  if(m_runNumbers[0] > 0){
	currentRunNumber = odin->runNumber();
	delay = m_runToDelay[currentRunNumber]; 
	info() << " delay = " << delay  << " currentRunNumber " << currentRunNumber << endmsg;
  }

  int bunchCounter =  odin->bunchId();
  info() << " >>> bunch id: " <<  odin->bunchId() << endmsg;
  std::vector<int>::iterator ibunch;
  int isitright=0;
  if(m_chosenBunches.size() !=0){
    for(ibunch = m_chosenBunches.begin(); ibunch != m_chosenBunches.end(); ibunch++ ){
      if( bunchCounter == (*ibunch) ) isitright =1;
    }
  }
  else isitright = 1;
  if(isitright==0){ 
    info() << " This is not the  bunch id you were looking for " <<  odin->bunchId() << endmsg;
#ifndef WIN32
    delete pulse;   
#endif
    return StatusCode::SUCCESS;
  }
  m_currentCalStep=odin->calibrationStep();
  // Skip step0
  if(m_skipStepZero && m_currentCalStep==0){
    info()<<"Skipping step 0"<<endmsg;
    delete pulse;   
    return StatusCode::SUCCESS;
  }
  // Only step0
  if(!m_skipStepZero && m_currentCalStep!=0 && m_onlyStepZero){
    info()<<"Skipping step other than 0"<<endmsg;
    delete pulse;   
    return StatusCode::SUCCESS;
  }
  // Limited number of events per step
  m_eventPerStepCounter[m_currentCalStep] +=1; 
  if(m_nEventsPerStep >0 ){
    if(  m_eventPerStepCounter[m_currentCalStep] > m_nEventsPerStep ){
      info()<<"Enough stats! Skipping the rest of step " << m_currentCalStep << endmsg;
      delete pulse;   
      return StatusCode::SUCCESS;
    }
  }
  
  // Retrieve Fillingscheme to select suitable bunchcrossings
  if(m_selectBunchCrossing){
    std::vector<int> bunchCrossingSample(nSamples,-1);
    Condition* xx = getDet<Condition>(detSvc(),"/dd/Conditions/Online/LHCb/LHCFillingScheme") ;
    std::string fs1str = xx->param<std::string>("B1FillingScheme");
    std::string fs2str = xx->param<std::string>("B2FillingScheme");
    char* fs1 = (char*)fs1str.c_str();
    char* fs2 = (char*)fs2str.c_str();  
    std::vector<int> fillingScheme(fs2str.size()+1);
    
    for( unsigned int i=0; i<fs2str.size(); i++){
      if (fs1[i]=='1' && fs2[i]=='1')fillingScheme[i+1]=3;
      if (fs1[i]=='1' && fs2[i]=='0')fillingScheme[i+1]=1;
      if (fs1[i]=='0' && fs2[i]=='1')fillingScheme[i+1]=2;
      if (fs1[i]=='0' && fs2[i]=='0')fillingScheme[i+1]=0; 
    }
    // info()<<fillingScheme<<endmsg;
    
    for (int i=-2; i<=2; i++){
      //if (fillingScheme[(bunchCounter+i)%3564]=="bb")
      debug()<<"Scheme for "<<i<<" : "<<fillingScheme[(bunchCounter+i)]<<endmsg;  
      bunchCrossingSample[i+2] = fillingScheme[(bunchCounter+i)];
    }
    // select only events which are beam-beam, and have non-beam-beam crossings in  prev1, prev2 and next1
    if (bunchCrossingSample[2] == 3 &&
        bunchCrossingSample[1] !=3 &&
        bunchCrossingSample[3] !=3 &&
        bunchCrossingSample[0] !=3 
        ){
      info()<<"Bunch-crossing ok"<<endmsg;
    }
    else {
      debug()<<"Bunch-crossing not ok"<<endmsg;
      delete pulse;
      return StatusCode::SUCCESS;
    }
  }//end selectBunchCrossing
#ifndef WIN32

  double step =odin->calibrationStep();

  // Loop over samples
  for ( itD = midSampleData -> begin(); itD != midSampleData -> end(); ++itD ) {
    LHCb::VeloTELL1Data* data = (*itD);  
    std::vector<LHCb::VeloTELL1Data* > allSamplesTell1Data;
    allSamplesTell1Data.resize(nSamples); 
    int TELL1_number=data->key();
    if(ExcludeTELL1(m_chosenTell1s,TELL1_number)&&m_chosenTell1s[0]!=-1)  continue;
    const DeVeloSensor* sens=0;
    sens=m_veloDet->sensorByTell1Id(TELL1_number);
    unsigned int sensNumber=0;
    sensNumber=sens->sensorNumber();
    if(sensNumber<64) m_sensorType=0; else m_sensorType=1; // 0 for R, 1 for phi.
    int count =0;
    for ( itL = m_samplingLocations.begin(); itL != m_samplingLocations.end(); ++itL, count++ ) {
      LHCb::VeloTELL1Datas* tell1Datas = veloTell1Data( (*itL) );  
      allSamplesTell1Data[count] = tell1Datas->object(TELL1_number);
    }
    std::vector<bool> signalDetected(2048,false);
    
    // Loop over samples
    for(int iSample = 0; iSample<nSamples; iSample++){
      for(int aLink=0; aLink<NumberOfALinks; ++aLink){
	std::vector<int> linkADCs = getLink( (*allSamplesTell1Data[iSample])[aLink]);
	std::vector<double> linkCMS = SubtractCommonMode(linkADCs, m_cmsCut); 
	std::vector<double>::iterator itf;
	int channel =0; 
	
	// Loop over channels
	for(itf=linkCMS.begin();itf!=linkCMS.end() ; ++itf, channel++){
	  int channelNumber = channel + 32*aLink;
	  
	  //Exclude channels with header bits:
	  //if (channelNumber%64==63) continue;
	  if (m_removeHeaders){
	    if (channelNumber%32==31) continue;
	  }
	  
	  double cmsadc = *itf;
	  char chnumber[100]; sprintf(chnumber, "%04d", channelNumber );
	  char t1number[100]; sprintf(t1number, "%03d_sens_%03d", TELL1_number, sensNumber);
	  std::string TPshapeTitle;
	  if(((cmsadc) > m_threshold)&& signalDetected[channelNumber] == false){
	    debug()<<"Now at step: "<< step <<endmsg;
	    debug() << "  signal detected! channel "  << channelNumber << " on sensor " << sensNumber << " Let's fit!" << endmsg; 
	    debug() << " channel adc = " << cmsadc << endmsg;
	    
	    //double clusteradcs = GetClusterADC(channelNumber, allSamplesTell1Data[iSample]);
	    //info() << " cluster adc? " << clusteradcs<< endmsg;
	    signalDetected[channelNumber] == true;
            debug()<< signalDetected[channelNumber] <<endmsg;
	    sprintf(pulseshape,"pulse_Tell1%03d_ch%04d_timebin%02d_peak_sens%d",TELL1_number,channelNumber,finebin, sensNumber);
	    
	    // Loop over samples to store ADC values for the fit
	    for(int jSample =0; jSample<nSamples; jSample++){
	      double time = 25*(jSample - (int) nSamples/2)+delay+step;
	      
	      std::vector<int> templinkADCs = getLink( (*allSamplesTell1Data[jSample])[aLink]);
	      std::vector<double> templinkCMS = SubtractCommonMode(templinkADCs, m_cmsCut);
	      if(m_tryCluster==true){
		double clustersum = GetClusterADC(channelNumber, allSamplesTell1Data[jSample]);
		pulse->Fill(time,clustersum);
		debug()<<"Filled values ADC "<<clustersum <<" time "<<time <<endmsg;
	      }
	      else if(m_tryCluster==false){
		pulse->Fill(time,templinkCMS[channel]);
		debug()<<"Filled values ADC "<<templinkCMS[channel]<<" time "<<time <<endmsg;
	      }
	    }// end loop over samples
	    
	    debug()<<"Pulse histogram filled"<<endmsg;
	    double amplitude = pulse->GetMaximum();
	    double peaktime = pulse->GetXaxis()->GetBinCenter(pulse->GetMaximumBin());
	    double integral = (double) pulse->Integral(); 
	    
	    //Initialize (not fix) fit parameters
	    func->SetParameter(0,0);
	    func->SetParameter(1,amplitude);
	    func->SetParameter(2,peaktime);
	    
	    //Fit a pulseshape per channel
	    if(m_useLogLikeFit) pulse->Fit("dtfitfunc","LMQNW");
	    else pulse->Fit("dtfitfunc","QNW");
	    double chisqperndof = func->GetChisquare()/((double) func->GetNDF());
	    double fitoffset = func->GetParameter(0);
	    double fitamplitude = func->GetParameter(1);
	    double fitpeaktime = func->GetParameter(2);
	    debug()<<"Pulse histogram fitted"<<endmsg;
	    
	    // Select only channels with sufficient total ADC in all samples,
	    // and reasonable amplitude and peaktime from the fit
	    // Fill "ClustersVsTime" histos
	    if(integral       > m_integralCut &&  
	       fabs(peaktime) < m_timeWindow  && 
	       amplitude      > m_threshold   ){
	      if(m_plotSeparateStrips) sprintf(finepulseshape,"ClustersVsTime_%d_ch%04d",sensNumber,channelNumber);
	      else sprintf(finepulseshape,"ClustersVsTime_%d",sensNumber);
	      std::string ffinepulseshape = finepulseshape;
	      debug()<<"Beginning filling of profiles"<<endmsg;
	      
	      // Loop over samples to store ADC values in histo
	      for(int kSample =0; kSample <nSamples; kSample++){
		double time = 25*(kSample - (int) nSamples/2)+delay+step;	
		int binnumber = pulse->FindBin(time);
		double adcvalue = pulse->GetBinContent(binnumber);
		debug()<<"Retrieve fitted values ADC "<<adcvalue<<" time "<<time <<endmsg;
		
		//info()<<time<<" "<<adcvalue<<" "<<ffinepulseshape<<" "<<ffinepulseshape<<" "<<
		//  -25*((nSamples+2)/2)-0.25<<" "<<25*((nSamples+2)/2+1)-0.25<<" "<< -127.5<<
		//  " "<< 128.5<<" "<< 50*(nSamples+2)<<" "<< 256<<endmsg;
		plot2D(time,adcvalue,ffinepulseshape,ffinepulseshape,
		       -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25, -127.5, 128.5, 50*(nSamples+2), 256);
		profile1D(time,adcvalue,ffinepulseshape+"prof",ffinepulseshape+"prof",
			  -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25,  50*(nSamples+2));			
		//info ()  <<  " bin "  << binnumber << " wrong? " << pulse->GetBinContent(kSample+1) 
		//         << " adc val  " << adcvalue << " delay "  << delay << endmsg;
	      }
	    }   
	    debug()<<"ADC counts added to profile"<<endmsg;
	    
	    // Additional histos, filled when all conditions are met
	    if(fitamplitude      > m_threshold    &&  
	       fitamplitude      < m_amplitudeCut && 
	       fabs(fitpeaktime) < m_timeWindow   &&
	       integral          > m_integralCut  &&
	       fabs(fitoffset)   < m_offsetCut    && 
	       chisqperndof      < m_chisqCut      ) {
	      debug()<<"Channel: "<<channelNumber<<" ("<<channelNumber%32<<")  Chi^2/ndf: "<< chisqperndof
		     <<" Offset: "<< fitoffset <<" +- "<<func->GetParError(0) << " Amplitude: "<< fitamplitude <<" +- "<<func->GetParError(1)
		     <<"  Peak: "<< fitpeaktime <<" +- "<<func->GetParError(2) << endmsg;
	      debug() << " pulse integral = "  << integral << endmsg;
	      
	      //char chi2plotname[200];
	      // fill the plots
	      sprintf(fitvalues_peak,  "fit_peak_sens%d",  sensNumber);
	      plot1D(fitpeaktime, fitvalues_peak, fitvalues_peak,-200.5,200.5,m_nTimeBins);	
	      sprintf(fitvalues_peak,"Weightedfit_peak_sens%d",   sensNumber);
	      double weight = 1.0/func->GetParError(2)/func->GetParError(2); // per event weight is equal to 1/error^2
	      plot1D(fitpeaktime, fitvalues_peak, fitvalues_peak,-200.5,200.5,m_nTimeBins, weight);
	      sprintf(fitvalues_peak,"fit_peak_sens%d_delay%d",  sensNumber, (int) delay); //same plot but for each delay in the options.
	      plot1D(fitpeaktime, fitvalues_peak, fitvalues_peak,-200.5,200.5,m_nTimeBins);	
	      sprintf(fitvalues_peak,"Weightedfit_peak_sens%d_delay%d",   sensNumber, (int) delay);
	      plot1D(fitpeaktime, fitvalues_peak, fitvalues_peak,-200.5,200.5,m_nTimeBins, weight);
	      sprintf(fitvalues_offset,"fit_offset_sens%d", sensNumber); 
	      plot1D(fitoffset, fitvalues_offset, fitvalues_offset,-15.5,15.5,31);// monitor the offset	    
	      sprintf(fitvalues_ampltiude,"fit_amplitude_sens%d", sensNumber);
	      plot1D(fitamplitude, fitvalues_ampltiude, fitvalues_ampltiude,0,300,m_nTimeBins);// monitor of the amplitude.
	      sprintf(finepulseshape,"FittedClustersVsTime_%d",sensNumber); // clusters that passed the fit condition.
	      std::string ffinepulseshape = finepulseshape;
	      
	      // Fill "FittedClustersVsTime" histos
	      for(int kSample =0; kSample <nSamples; kSample++){
		double time = 25*(kSample - (int) nSamples/2)+delay+step;
		int binnumber = pulse->FindBin(time);
		double adcvalue = pulse->GetBinContent(binnumber);
		plot2D(time,adcvalue,ffinepulseshape,ffinepulseshape,
		       -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25, -127.5, 128.5, 50*(nSamples+2), 256);			
		profile1D(time,adcvalue,ffinepulseshape+"prof",ffinepulseshape+"prof",
			  -25*((nSamples+2)/2)-0.25,25*((nSamples+2)/2+1)-0.25,  50*(nSamples+2));			
	      }
	    }//end if integral etc.
	    
	    debug()<<"Fit values collected"<<endmsg;
	  }//end if cmscut
	  pulse->Reset();
	}// end loop over channels
      }//end loop over links
    }// end loop over samples
  }// end loop over samples
  debug()<<"Event done"<<endmsg;
  delete pulse;
#endif
  
  delete func;  
  
  m_nEvents++;
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode Velo::PulseShapeFitterTed::finalize() {
  if ( m_debugLevel ) debug() << "==> Finalize" << endmsg;
  return VeloMonitorBase::finalize(); // must be called after all other actions
}

//=============================================================================
// Retrieve the VeloTell1Data
//=============================================================================
LHCb::VeloTELL1Datas*
Velo::PulseShapeFitterTed::veloTell1Data( std::string samplingLocation ) {
  std::string tesPath;
  if(samplingLocation == "") tesPath = samplingLocation + "Raw/Velo/DecodedADC"; 
  else  tesPath = samplingLocation + "/Raw/Velo/DecodedADC";
  size_t pos = tesPath.find( "//" );
  if ( pos != std::string::npos )    tesPath.replace( pos, 2, "/" );
  if ( m_debugLevel ) 	debug() << "Retrieving VeloTell1Data from " << tesPath << endmsg;
  if ( !exist<LHCb::VeloTELL1Datas>( tesPath ) ) {
	debug() << "No VeloTell1Data container found for this event !" << endmsg; 
	if (m_nEvents %100==0  )  info() << "No VeloTell1Data container found for this event !   " << tesPath << endmsg; 
	return 0;
  }
  else {
	LHCb::VeloTELL1Datas* tell1Data = get<LHCb::VeloTELL1Datas>( tesPath );
	if ( m_debugLevel ) debug() << "  -> number of VeloTell1Data found in TES: "
	  << tell1Data->size() <<endmsg;
	return tell1Data;
  }
}
//=============================================================================
double Velo::fitsimple(double *x, double *par){
  double fitval;
  if(x[0]<par[2]) fitval = par[0]+par[1]*(exp(-((x[0]-par[2])/par[3])*((x[0]-par[2])/par[3])));
  else fitval = par[0]+par[1]*(exp(-((x[0]-par[2])/par[4])*((x[0]-par[2])/par[4])));
  return fitval;
}
// simple link common mode subtraction
std::vector<double>  Velo::PulseShapeFitterTed::SubtractCommonMode( std::vector<int> data, double cut = 15.0)
{
  int howManyPasses =3;
  std::vector<double> commonModeSubtractedData;
  std::vector<int>::iterator it;
  std::vector<double>::iterator itf;
  double average = 0;
  int count = 0, chanWithoutHits = 0;  double averageWithoutHits = 0;
  for(it=data.begin();it!=data.end() ; ++it){
	average += float(*it);
  }
  average /= float(data.size());
  for(it=data.begin();it!=data.end() ; it++){
	commonModeSubtractedData.push_back((double)(*it) - average);
  }
  int pass=0;
  while(pass < howManyPasses){
	chanWithoutHits=0; averageWithoutHits =0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf){
	  if(fabs(*itf) < cut )  {
		averageWithoutHits += *itf;
		chanWithoutHits++;
	  }
	}
	if(chanWithoutHits > 0) averageWithoutHits /= double(chanWithoutHits);
	else averageWithoutHits = 0;
	count = 0;
	for(itf=commonModeSubtractedData.begin();itf!=commonModeSubtractedData.end() ; ++itf, count++){
	  commonModeSubtractedData[count]  = *itf - averageWithoutHits;
	}
	pass++;
  }
  return commonModeSubtractedData;
}
// attempt of clusterizing the data.
double Velo::PulseShapeFitterTed::GetClusterADC( int seedChannel, LHCb::VeloTELL1Data *tell1data){
  if(seedChannel < 0 || seedChannel >2047) {
	info() << "channel does not exist! " << endmsg;
  }
  double clusterADC = 0;
  int stripNumber = m_stripFromChannel[m_sensorType][seedChannel];
  for(int ineighbour = -m_takeNeighbours; ineighbour <= m_takeNeighbours; ineighbour++ ){ 
	if(stripNumber+ineighbour >= 0 && stripNumber +ineighbour  <2048){
	  int currentstrip =  stripNumber+ineighbour;
	  int currentchannel = m_channelFromStrip[m_sensorType][currentstrip]; 
	  int channelnumber = (int)(currentchannel%32);
	  int aLink = (int)(currentchannel/32.);
	  std::vector<int> linkADCs = getLink( (*tell1data)[aLink]);
	  std::vector<double> linkCMS = SubtractCommonMode(linkADCs, m_cmsCut); 
	  double channeladc = linkCMS[channelnumber];
	  if(ineighbour ==0 ) debug() << "seedADC == " <<  channeladc <<  endmsg ;  
	  clusterADC += channeladc; 
	} 
  }
  return clusterADC;
}

//simple mean common mode subtraction
double Velo::PulseShapeFitterTed::subtractCommonMode(int channelNumber, LHCb::VeloTELL1Data *tell1data){
  ALinkPair begEnd;
  int aLink = (int)(channelNumber/32.);
  begEnd=(*tell1data)[aLink];
  scdatIt iT;
  double commonmode=0;
  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
	commonmode += *iT;  
  }
  commonmode /= 32.0;
  int count = 0;
  double commonmodewithoutsignal= 0.0;
  for(iT=begEnd.first; iT!=begEnd.second; ++iT){
	double cmsadc = *iT - commonmode; 
	if(fabs(cmsadc) < m_threshold){ 
	  commonmodewithoutsignal += *iT;
	  count++;
	}
  }
  commonmodewithoutsignal /= float(count+0.00001);
  double cmsadc = tell1data->channelADC(channelNumber) - commonmodewithoutsignal;
  return cmsadc;
}

