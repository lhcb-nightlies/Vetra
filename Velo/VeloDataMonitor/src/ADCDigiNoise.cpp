// ============================================================================
// Include files 
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/AlgFactory.h" 
// ============================================================================
// GaudiAlg 
// ============================================================================
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Fill.h"
// ============================================================================
// GaudiUti
// ============================================================================
#include "GaudiUtils/HistoTableFormat.h"
// ============================================================================
// VeloTELL1Event
// ============================================================================
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"
// ============================================================================
// AIDA 
// ============================================================================
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
// ============================================================================
// Boost 
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
// VeloDet
// ============================================================================
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"
// ============================================================================
// local 
// ============================================================================
#include "VetraKernel/INoiseEvaluator.h"
#include "VetraKernel/TELL1Noise.h"
#include "CommonFunctions.h"
// ============================================================================

using namespace VeloTELL1;


namespace Velo 
{
  namespace Monitoring 
  {
    /** @classADCDigiNoise 
     *  Simple algorithm to perform monitoring of pedestals&noise 
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-10-26
     */
    class ADCDigiNoise : public GaudiHistoAlg 
    {
      /// friend factory for instantiation
      friend class AlgFactory<Velo::Monitoring::ADCDigiNoise>;
      public:
      // initialize it! 
      virtual StatusCode initialize () override;
      // execute it! 
      virtual StatusCode execute    () override;
      // finalize it! 
      virtual StatusCode finalize   () override;
      protected:
      /// standard constructor 
      ADCDigiNoise 
        ( const std::string& name , 
          ISvcLocator*       pSvc ) 
        : GaudiHistoAlg ( name , pSvc ) 
        , m_locations () 
        , m_tell1s    () 
        , m_publish   ( 10000 )
        , m_toolType  ( "Velo::Monitoring::NoiseEvaluator" )
        , m_event     ( 0 )
        , m_noises    () 
        , m_veloDet   (0)
        , m_stepSize(0)
        , m_nSteps(0)

        {
          //
          m_locations.push_back ( LHCb::VeloTELL1DataLocation::ADCs             ) ;
          //m_locations.push_back ( LHCb::VeloTELL1DataLocation::SubPeds          ) ;
          //m_locations.push_back ( LHCb::VeloTELL1DataLocation::PedSubADCs       ) ;
          //m_locations.push_back ( LHCb::VeloTELL1DataLocation::FIRCorrectedADCs ) ;
          //m_locations.push_back ( LHCb::VeloTELL1DataLocation::ReorderedADCs    ) ;
          m_locations.push_back ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ) ;
          //
          declareProperty ( "Containers" , m_locations  , "The list of TES locations to be monitored") ;
          declareProperty ( "TELL1s"     , m_tell1s     , "The list of TELL1s to be monitored, empty for all") ;
          declareProperty ( "NoiseEvaluatorType" , m_toolType , "The actual type of noise evaluator tool" ) ;
          declareProperty ( "PublishingFrequency" , m_publish , "The frequency to publish results" ) ;            
          declareProperty ( "StepSize", m_stepSize=100 ); 
          declareProperty ( "NSteps", m_nSteps=16 );
          setProperty ( "HistoPrint"  , true     ).ignore() ;
          setProperty ( "HistoTopDir" , "Vetra/" ).ignore() ;

        }
      /// virtual and protected destructor 
      virtual ~ADCDigiNoise() {}
      private:
      // no default constructor 
      ADCDigiNoise ()                  ; ///< no default constructor 
      // no copy constructor 
      ADCDigiNoise ( const ADCDigiNoise& ) ; ///< no copy constructor 
      // no assigmenet operator 
      ADCDigiNoise& operator=( const ADCDigiNoise& ) ; ///< no assignement 
      protected:

      // retrieve the tools
      StatusCode retrieveTools   ();
      /// make the plots
      StatusCode makeThePlots () ;

      private:
      // the actual type of container of data locations 
      typedef std::vector<std::string>                  Locations ;
      typedef Velo::Monitoring::INoiseEvaluator::TELL1s TELL1s    ;
      typedef std::map<std::string,Velo::Monitoring::INoiseEvaluator*> Tools ;
      // the input containers of data to be monitored
      Locations     m_locations ; ///< the input containers
      // the list of IDs for TELL1s to be monitored 
      TELL1s        m_tell1s    ; ///< list of ids for TELL1 
      // publishing frequency
      unsigned long m_publish   ; ///< publishing frequency
      // use the public tools? 
      bool          m_public    ; ///< use the public tools 
      // the actual tool type 
      std::string   m_toolType  ; ///< the actual tool type
      // event counter 
      unsigned long m_event     ; ///< internal event counter 
      // the map of tools to be used 
      Tools         m_noises    ; ///< map of tools to be used 
      // Flag for initializing the process info bank.
      int m_isInit;
      // Detector element pointer
      DeVelo* m_veloDet; 

      unsigned int m_stepSize;
      unsigned int m_nSteps;

    } ;
  } // end of namespace Velo::Monitoring 
} // end of namespace Velo
// ============================================================================

// ============================================================================
//  make the plots 
// ============================================================================
StatusCode Velo::Monitoring::ADCDigiNoise::makeThePlots () 
{

  std::string polarity;
  if ( 0 == m_event % 2 ) {
    polarity = "even";
  } else {
    polarity = "odd";
  }
  polarity = "";
  // info() << "got here!  step:" << endmsg ;

  /// loop over all all tools (information sources) 
  unsigned int noiseCalculator = 0;
  for ( Tools::iterator it = m_noises.begin();   m_noises.end() != it ; ++it, ++noiseCalculator )
  {
    // for the given tool get all TELL1s:
    Velo::Monitoring::INoiseEvaluator::INoiseMap noisemap = it->second->noise() ;
      info() << "got HERE  " << 1 << endmsg;

    //Get the container of TELL1 data banks. It's needed to check for reordering.
    if ( !exist<LHCb::VeloTELL1Data::Container> ( it->first ) ) {
      debug() << "Could not retrieve noise data at " << it->first << endmsg;
      continue;
    } 
    const LHCb::VeloTELL1Data::Container* data = get<LHCb::VeloTELL1Data::Container> ( it->first ); 
      info() << "got HERE  " << 2 << endmsg;

    /// loop over TELL1s for the given tool 
    for ( Velo::Monitoring::INoiseEvaluator::INoiseMap::iterator il =noisemap.begin () ; noisemap.end () != il ; ++il )    {
      // get the counters: 
      info() << "got HERE  " << 3 << endmsg;
      typedef Velo::Monitoring::TELL1Noise::Counters Counters ;
      const Counters& cnts = il->second->counters() ;
      const unsigned int tell1Number = il->first;
      
	  info() << "got here!  step: <<<<<<<<<<<<< " <<  endmsg;
      //Check 
      bool isReordered = data->object(tell1Number)->isReordered();

      debug() << it->first << " / " << tell1Number << " is Reordered = " << data->object(tell1Number)->isReordered() << endmsg;

      // construct the proper IDs for the histograms:
      boost::format fmt ( "ADCDigi/%s/TELL1_%03d/" ) ;
      fmt % strippedContainerName ( it->first ) % tell1Number ;
      const std::string fmtstr = fmt.str() ;

      //Get sensor from detector element:  
      const DeVeloSensor* sensor =  m_veloDet->sensorByTell1Id(tell1Number);
      if(sensor == 0 ){
        error() << "Can't find a sensor associated with TELL1 " << tell1Number << ". Skipping..." << endmsg;
        break;
        //return StatusCode::FAILURE;
      }

      boost::format sensorInfo(", sensor %d, %s");
      sensorInfo % sensor->sensorNumber() % (sensor->isR() ? "R" : ( sensor->isPhi() ? "Phi" : "Pile-up")) ;      

      // fill the histos :
      for ( Counters::const_iterator ic = cnts.begin() ; cnts.end() != ic ; ++ic ) 
      {
      info() << "got HERE  " << 6 << endmsg;
        int step = m_event/m_stepSize;
        info() << "got here!  step: " <<  step << endmsg;
        const double index = ic - cnts.begin() ;
        unsigned int chipchannel = static_cast<unsigned int> (index);	
        if(isReordered){
          chipchannel=sensor->StripToChipChannel(static_cast<unsigned int> (index));
        }
        int link = chipchannel/32;
        int linkchannel = chipchannel % 32;
        char  hName[1024];
      info() << "got HERE  " << 4 << endmsg;
        sprintf(hName,"%shMean_%s_link_%02d",fmtstr.c_str(),polarity.c_str(),link);
        plot1D((linkchannel)+float(step)/float(m_nSteps)-0.5,hName,hName, -4.5, 31.5, 36*m_nSteps, ic->flagMean());
      info() << "got HERE  " << 5 << endmsg;
        sprintf(hName,"%shRMS_%s_link_%02d",fmtstr.c_str(), polarity.c_str(),link);
        plot1D((linkchannel)+float(step)/float(m_nSteps)-0.5,hName,hName, -4.5, 31.5, 36*m_nSteps, ic->flagRMS());
      }

      /// reset the counters at the end of each step
      const_cast<Velo::Monitoring::TELL1Noise*>(il->second)->reset();
      
    }        
  }
  return StatusCode::SUCCESS ;
}


// ===========================================================================
// Retrieve the tools
// ============================================================================
StatusCode Velo::Monitoring::ADCDigiNoise::retrieveTools   () {
  // loop over all input containers and get the tools:
  for ( Locations::const_iterator il = m_locations.begin() ; 
      m_locations.end() != il ; ++il ) 
  {

    std::string toolName = strippedContainerName( *il );// + "_Odd";
    // get the evaluator of the noise:
    Velo::Monitoring::INoiseEvaluator* eval = tool<Velo::Monitoring::INoiseEvaluator> ( m_toolType , toolName) ;

    if(eval!=0)
      m_noises[ *il ] = eval;

    /*
    toolName = strippedContainerName( *il ) + "_Even";
    // get the evaluator of the noise:
    eval = tool<Velo::Monitoring::INoiseEvaluator> ( m_toolType , toolName) ;

    if(eval!=0)
      m_noises[ *il ] = eval;
   */

  }

  return StatusCode::SUCCESS;
}


// ============================================================================
// initialize the algotithm 
// ============================================================================
StatusCode Velo::Monitoring::ADCDigiNoise::initialize   () {
  StatusCode sc=GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm  
  debug() << "==> Initialize" << endmsg;
  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );  

  retrieveTools();

  return StatusCode::SUCCESS;
}

// ============================================================================
// finalize the algotithm 
// ============================================================================
StatusCode Velo::Monitoring::ADCDigiNoise::finalize   () 
{
  // clear the container of tools
  m_noises.clear() ;
  m_event = 0     ;
  // finalize the base class 
  return GaudiHistoAlg::finalize() ;
}
// ============================================================================
// Execute the algorithm 
// ============================================================================
StatusCode Velo::Monitoring::ADCDigiNoise::execute    () 
{  

  StatusCode sc;
  info() << "event number " << m_event << " (m_event+1)  % m_stepSize  " <<  ((m_event+1)  % m_stepSize) << endmsg;
  if ( 0 != m_event && 0  ==  (m_event+1)  % m_stepSize ){
   
    makeThePlots(); 
  } 
  //int step = m_event/m_stepSize;
  //info() << "got here!  step: " <<  step << endmsg;

  ++m_event;

  return StatusCode::SUCCESS ;
} 
// ============================================================================
/// Declaration of the Algorithm Factory
DECLARE_NAMESPACE_ALGORITHM_FACTORY(Velo::Monitoring,ADCDigiNoise)
  // ============================================================================
  // The END
  // ============================================================================
