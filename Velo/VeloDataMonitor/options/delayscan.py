"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of non-zero-suppressed data.         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
##############################################################################
"""

from Gaudi.Configuration     import EventSelector
from GaudiConf.Configuration import *
from Configurables import LHCbApp
from Configurables import Vetra
from Configurables import PrepareVeloFullRawBuffer
from Configurables import DecodeVeloFullRawBuffer
from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters
vetra = Vetra()
vetra.DataBanks      = 'NZS'
vetra.RunEmulation   = True
vetra.EmuMode        = 'Dynamic'
vetra.BitPerfectness = False
vetra.OutputType     = 'NONE'
vetra.EvtMax = -1
# --> generated data are taken wihtout the ODIN bank
PrepareVeloFullRawBuffer().RunWithODIN = False
# set the appropriate DB tags
LHCbApp().DDDBtag   = 'head-20080905'
LHCbApp().CondDBtag = 'head-20080905'
#LHCbApp().CondDBtag = ''
DecodeVeloFullRawBuffer().SectorCorrection = True
DecodeVeloFullRawBuffer().CableOrder = [ 3, 2, 1, 0 ]
#VeloTELL1Reordering().SectorCorrection = True
##############################################################################


##############################################################################


###############################################################################

from Configurables import GaudiSequencer
from Configurables import Velo__Monitoring__ADCDelayScan  as PhaseScan
moni_phasescan = GaudiSequencer( 'Moni_PhaseScan' )
phaseScan = PhaseScan( 'PhaseScan' )
moni_phasescan.Members = [phaseScan]
phaseScan.BurstSize = 100
phaseScan.NBursts  = 16
phaseScan.BurstId = 0
EventSelector().FirstEvent = 101
EventSelector().PrintFreq = 100
phaseScan.PhaseChangeMethod=1
phaseScan.OutputLevel = 3
phaseScan.TELL1s = [36]
phaseScan.Plot2D = 0
phaseScan.PlotCMS = 1
phaseScan.NEventsToSkip =0
phaseScan.CommonModeCut = 200
phaseScan.PlotRaw = 1
phaseScan.PlotProfile = 1
phaseScan.InvertedDelaySequence = False 
def mystuff():
  GaudiSequencer( 'MoniVELOSeq' ).Members = [ moni_phasescan ]
appendPostConfigAction( mystuff )


#GaudiSequencer( 'MoniVELOSeq' ).Members.Remove( GaudiSequencer( 'Moni_NZS' ) )
##############################################################################
EventSelector().Input = [
#     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/50758/050758_0000000001.raw' SVC='LHCb::MDFSelector'"
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/55625/055625_0000000001.raw' SVC='LHCb::MDFSelector'"
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000001.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000002.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000003.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000004.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000005.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000006.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000007.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000008.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000009.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000010.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000011.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000012.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000013.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000014.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000015.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000016.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000017.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000018.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000019.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000021.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000022.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000023.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000024.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000025.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000026.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000027.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000028.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000029.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000030.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000031.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/57431/057431_0000000032.raw' SVC='LHCb::MDFSelector'"
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/58494/058494_0000000001.raw' SVC='LHCb::MDFSelector'"
#     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/58522/058522_0000000001.raw' SVC='LHCb::MDFSelector'"
     "DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/ADCDELAYSCAN/60160/060160_0000000001.raw' SVC='LHCb::MDFSelector'"
]
###############################################################################
vetra.HistogramFile  = 'DelayScan60160.root'
