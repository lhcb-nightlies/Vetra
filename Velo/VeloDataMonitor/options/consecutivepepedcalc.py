"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of non-zero-suppressed data.         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
##############################################################################
"""

from Gaudi.Configuration     import EventSelector
from GaudiConf.Configuration import *
from Configurables import LHCbApp
from Configurables import Vetra
vetra = Vetra()
vetra.DataBanks      = 'NONE'
vetra.RunEmulation   = False
#Will include my  own version of the emulator.
vetra.EmuMode        = 'Dynamic'
vetra.BitPerfectness = False
vetra.OutputType     = 'NONE'
# --> generated data are taken wihtout the ODIN bank
# set the appropriate DB tags
LHCbApp().DDDBtag   = 'head-20080905'
LHCbApp().CondDBtag = 'head-20080905'
#LHCbApp().CondDBtag = ''
from GaudiKernel.ProcessJobOptions import importOptions
importOptions("$VETRAROOT/options/Velo/TAEDecodingNZS.py")
#importOptions("$VETRAROOT/options/Velo/TAEEmulatorNZS.py")
#VeloTELL1Reordering().SectorCorrection = True
##############################################################################


##############################################################################


###############################################################################

from Configurables import GaudiSequencer
from Configurables import Velo__ConsecutivePedestalCalculator as PedCalc 
moni_pedcalc = GaudiSequencer( 'Moni_PedCalc' )
pedCalc = PedCalc( 'PedCalc' )
moni_pedcalc.Members = [pedCalc]
pedCalc.StepSize = 100000000
pedCalc.NSteps  = 50
EventSelector().FirstEvent = 1
EventSelector().PrintFreq = 1
vetra.EvtMax = 10000000000000
#pedCalc.SamplingLocations = ["Prev3", "Prev2", "Prev1","","Next1","Next2","Next3"] 
pedCalc.SamplingLocations = [ "Prev2", "Prev1","","Next1","Next2"]
pedCalc.CMSCut = 20
pedCalc.ChisqCut = 200
pedCalc.SignalThreshold = 15
pedCalc.MaxAmplitudeCut= 150
pedCalc.OffsetCut= 10 
pedCalc.IntegralCut= 40
pedCalc.TimeWindow= 200
pedCalc.FixOffset= True
pedCalc.TakeNeighbours=0
pedCalc.NTimeBins=50
pedCalc.UseLogLikeFit=False  
pedCalc.TryToClusterize = False
pedCalc.PlotSeparateStrips=False
pedCalc.OutputLevel = 4
#pedCalc.TELL1s=[13]
#pedCalc.WidthBefore=13.7
#pedCalc.WidthAfter=26.8 
#pedCalc.WidthBefore=15.7 from tp
#pedCalc.WidthAfter=23.3  from tp
#out from june ted: 
#pedCalc.WidthBefore=17.0
#pedCalc.WidthAfter=23.1
# @ -30
pedCalc.WidthBefore=13.4
pedCalc.WidthAfter=19.9
pedCalc.DetailedOutput=False
pedCalc.RunWithChosenBunchCounters=False
#pedCalc.ChosenBunchCounters = {};
pedCalc.ChosenBunchCounters = [2685,2686,2687,2688]
#pedCalc.RunNumbers = [ 58821, 58822, 58823,58824] 
#pedCalc.RunNumbers = [ 58979, 58980, 58981, 58983, 58985,  58990, 58823,58824] 
#pedCalc.RunNumbersDelay = [ 0, 0, 0, 0, 0, 12.5, 19.0,  6.5]
pedCalc.RunNumbers = [ 58992, 58994, 58995,59000,59001, 59004, 59007, 59008, 59009]
pedCalc.RunNumbersDelay = [ 12.5, 0,  6.5, -6, -6, -12.5, -12.5, -18.5, -18.5]
#pedCalc.RunNumbers = [ 50425, 50426, 50427, 50428, 50430]
#pedCalc.RunNumbersDelay = [0,  0, 12.5, 19, 6.5]
#pedCalc.RunNumbers = [ 58525]
#pedCalc.RunNumbersDelay = [0]

def mystuff():
  GaudiSequencer( 'MoniVELOSeq' ).Members = [ moni_pedcalc]
appendPostConfigAction( mystuff )

##############################################################################

##############################################################################
EventSelector().Input = [
#     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55033/055033_0000000001.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55033/055033_0000000002.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55033/055033_0000000003.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55033/055033_0000000004.raw' SVC='LHCb::MDFSelector'"
#  "DATA='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/TED/50425/050425_0000000001.raw'  SVC='LHCb::MDFSelector'",
#  "DATA='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/TED/50426/050426_0000000001.raw'  SVC='LHCb::MDFSelector'",
#  "DATA='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/TED/50427/050427_0000000001.raw'  SVC='LHCb::MDFSelector'",
#  "DATA='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/TED/50428/050428_0000000001.raw'  SVC='LHCb::MDFSelector'",
#  "DATA='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/TED/50430/050430_0000000001.raw'  SVC='LHCb::MDFSelector'"
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/TEDNZS/58821/058821_0000000001.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/TEDNZS/58822/058822_0000000001.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/TEDNZS/58823/058823_0000000001.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/58979/058979_0000000001.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/58980/058980_0000000001.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/58981/058981_0000000001.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/58983/058983_0000000001.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/58985/058985_0000000001.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/58990/058990_0000000001.raw' SVC='LHCb::MDFSelector'",
"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/58992/058992_0000000001.raw' SVC='LHCb::MDFSelector'",
"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/58994/058994_0000000001.raw' SVC='LHCb::MDFSelector'",
"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/58995/058995_0000000001.raw' SVC='LHCb::MDFSelector'",
"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/59000/059000_0000000001.raw' SVC='LHCb::MDFSelector'",
"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/59001/059001_0000000001.raw' SVC='LHCb::MDFSelector'",
"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/59004/059004_0000000001.raw' SVC='LHCb::MDFSelector'",
"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/59007/059007_0000000001.raw' SVC='LHCb::MDFSelector'",
"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/59008/059008_0000000001.raw' SVC='LHCb::MDFSelector'",
"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/59009/059009_0000000001.raw' SVC='LHCb::MDFSelector'"
]
###############################################################################
vetra.HistogramFile  = 'tedpedestals.root'


