#!/usr/bin/env python

import sys
import os
import time
import shutil

import TELL1Map

def runGaudiJob():
  global data_files
  global xml_dir
  global use_tell1_numbers
  global tell1_map_file

  from Gaudi.Configuration  import EventSelector
  from GaudiPython.Bindings import AppMgr
  from Configurables import Velo__Monitoring__GainParamCalculator as GainParamCalculator
  from Configurables        import createODIN
  from Configurables        import LHCbApp

  lhcb=LHCbApp()
  EventSelector().PrintFreq = 1000
  
  EventSelector().Input = data_files
  
  gainParamCalculator = GainParamCalculator('GainParamCalculator')
  gainParamCalculator.TELL1MapFile = tell1_map_file
  gainParamCalculator.UseTELL1Numbers = use_tell1_numbers
  gainParamCalculator.XMLDir = xml_dir
  #gainParamCalculator.OutputLevel = 1
  
  appMgr      = AppMgr()
  appMgr.addAlgorithm('createODIN')
  appMgr.addAlgorithm('Velo__Monitoring__GainParamCalculator/GainParamCalculator')
  
  appMgr.run(-1)
  # trigger stop, finalize and termination
  appMgr.__del__()


def prepareDataFiles():
  global data_dir
  global run_number
  global data_files

  run_dir = data_dir + '/' + str(run_number)
  file_list = os.listdir(run_dir)

  for f in file_list:
    data_files.append("DATAFILE='%s' SVC='LHCb::MDFSelector'" % (run_dir+'/'+f))

#def makeXMLCurrent():
#  global xml_dir
#  global install_dir
#
#
#  if not os.access(xml_dir,os.R_OK):
#    print "error: can not read directory %s." % xml_directory
#    sys.exit(1)
#
#  if not os.access(install_dir,os.W_OK):
#    print "error: can not write to directory %s." % install_dir
#    sys.exit(1)
#
#  base_xml_dir = os.path.basename(os.path.normpath(xml_dir))
#  shutil.copytree(os.path.normpath(xml_dir),os.path.normpath(install_dir)+'/'+base_xml_dir)
#
#  old_cwd = os.getcwd()
#  os.chdir(install_dir)
#
#  if os.access('current',os.F_OK):
#    current = os.readlink('current')
#    if os.access('last_current',os.F_OK):
#      os.remove('last_current')
#      os.symlink(current,'last_current')
#    os.remove('current')
#    os.symlink(base_xml_dir,'current')
#
#  os.chdir(old_cwd)

    

########################################################################
# main execution
########################################################################

# globals, might be changed by options
now = time.localtime()
data_dir = '/daqarea/lhcb/data/%d/RAW/FULL/VELO/ADCGAINSCAN' % (now.tm_year)
data_files = []
run_number = None
use_tell1_numbers = False
xml_dir = None
make_xml_current = False
tell1_map = None
install_dir = '/group/velo/config/arx_dac_gain'

# command line options
from optparse import OptionParser
parser = OptionParser()
parser.add_option("-r","--run", dest="run_number", help="run Gaudi calculator on ADC gain scan run N", metavar="N", action="store", type="int", default=None)
parser.add_option("-d","--data-dir", dest="data_dir", help="look for ADC gain scan runs in DATADIR", metavar="DATADIR", action="store", type="string", default=data_dir)
parser.add_option("-x","--xml-dir", dest="xml_dir", help="write/read XML files to/from XMLDIR", metavar="XMLDIR", action="store", type="string", default=None)
parser.add_option("-m","--tell1-map-file", dest="tell1_map_file", help="read sensor to TELL1 mapping from FILE", metavar="FILE", action="store", type="string", default=None)
parser.add_option("-t","--use-tell1-numbers", dest="use_tell1_numbers", help="use TELL1 numbers instead of sensor numbers when creating XML", action="store_true")
#parser.add_option("-c","--make-xml-current", dest="make_xml_current", help="make the XML current for recipe uploading", action="store_true")
parser.add_option("-i","--install-dir", dest="install_dir", help="install XML for recipe uploading in INSTALLDIR", metavar="INSTALLDIR", action="store", type="string", default=install_dir)
(options, args) = parser.parse_args()

data_dir = options.data_dir
run_number = options.run_number
use_tell1_numbers = options.use_tell1_numbers
#make_xml_current = options.make_xml_current
install_dir = options.install_dir

# the user must supply an XML directory path if the Gaudi calculator is not run
if options.xml_dir:
  xml_dir = options.xml_dir
else:
  if run_number:
    xml_dir =  './gain_param_xml_%d_%d%02d%02d' % (run_number,now.tm_year,now.tm_mon,now.tm_mday)
  else:
    print "error: you must specify an XML directory for reading when not running the Gaudi calculator."
    sys.exit(1)

# do not use TELL1Map module if the user supplies a map file
if options.tell1_map_file:
  tell1_map_file = options.tell1_map_file
else:
  tell1_map_file = '/group/velo/config/livdb_tell1_sensor_map.txt'
  TELL1Map.TELL1Map(tell1_map_file)

# make sure the ouput directory for the Gaudi job exists (if the Gaudi calculator is run)
if run_number and not os.access(xml_dir,os.F_OK):
  os.mkdir(xml_dir)


if run_number:
  print "Will run Gaudi gain parameter calculator on run %d." % run_number
  prepareDataFiles()
  runGaudiJob()

#if make_xml_current:
#  print "Will install XML files from %s and make them current." % xml_dir 
#  makeXMLCurrent()



