#!/usr/bin/env python

import sys
import os
import time
import shutil


def runGaudiJob():
  global n_events
  global data_files
  global histogram_file
  global thresholds_file
  global write_thresholds_file

  from Gaudi.Configuration  import EventSelector
  from GaudiPython.Bindings import AppMgr
  from Configurables import Velo__Monitoring__HeaderMon as HeaderMon
  from Configurables        import createODIN
  from Configurables        import LHCbApp
  from Configurables        import HistogramPersistencySvc
  from Configurables import RootHistCnv__PersSvc


  lhcb=LHCbApp()
  EventSelector().PrintFreq = 100
  
  EventSelector().Input = data_files
  
  headerMon = HeaderMon('HeaderMon')
  headerMon.ThresholdsFile = thresholds_file
  headerMon.WriteThresholdsFile = write_thresholds_file;
  #headerMon.OutputLevel = 1
  
  RootHistCnv__PersSvc( 'RootHistCnv' ).ForceAlphaIds = True
  HistogramPersistencySvc().OutputFile=histogram_file

  appMgr      = AppMgr()
  appMgr.HistogramPersistency='ROOT'
  appMgr.addAlgorithm('createODIN')
  appMgr.addAlgorithm('Velo__Monitoring__HeaderMon/HeaderMon')
  
  appMgr.run(n_events)
  # trigger stop, finalize and termination
  appMgr.__del__()


def prepareDataFiles():
  global data_dir
  global run_number
  global data_files

  run_dir = data_dir + '/' + str(run_number)
  file_list = os.listdir(run_dir)

  for f in file_list:
    data_files.append("DATAFILE='%s' SVC='LHCb::MDFSelector'" % (run_dir+'/'+f))

#def makeThresholdsCurrrent():
#  global xml_dir
#  global install_dir
#
#
#  if not os.access(xml_dir,os.R_OK):
#    print "error: can not read directory %s." % xml_directory
#    sys.exit(1)
#
#  if not os.access(install_dir,os.W_OK):
#    print "error: can not write to directory %s." % install_dir
#    sys.exit(1)
#
#  base_xml_dir = os.path.basename(os.path.normpath(xml_dir))
#  shutil.copytree(os.path.normpath(xml_dir),os.path.normpath(install_dir)+'/'+base_xml_dir)
#
#  old_cwd = os.getcwd()
#  os.chdir(install_dir)
#
#  if os.access('current',os.F_OK):
#    current = os.readlink('current')
#    if os.access('last_current',os.F_OK):
#      os.remove('last_current')
#      os.symlink(current,'last_current')
#    os.remove('current')
#    os.symlink(base_xml_dir,current)
#
#  os.chdir(old_cwd)

def showFHSPlot():
  global histogram_file
  global run_number

  gStyle.SetOptStat(0)

  f_fhs = TFile(histogram_file,'READ')
  h_fhs = f_fhs.Get('Velo/HeaderMon/FHS')
  h_fhs.SetMinimum(80)

  title = 'Full Header Swing'
  if run_number:
    title = title + ' Run %d' % run_number
  c_fhs = TCanvas('c_fhs',title)
  c_fhs.cd()
  c_fhs.Draw()
  h_fhs.Draw('colz')

  raw_input('Press ENTER to continue...')

  f_fhs.Close()

 


########################################################################
# main execution
########################################################################

# globals, might be changed by options
now = time.localtime()
data_dir = None
data_files = []
run_number = None
n_events = 4000
thresholds_file = None
histogram_file = None
write_thresholds_file = False
make_thresholds_current = False
show_fhs_plot = False
install_dir = '/group/velo/config'
activity = 'EOF_CALIB'

# command line options
from optparse import OptionParser
parser = OptionParser()
parser.add_option("-r","--run", dest="run_number", help="run Gaudi header monitor on NZS run RUN", metavar="RUN", action="store", type="int", default=None)
parser.add_option("-n","--events", dest="n_events", help="run Gaudi header monitor on N events", metavar="N", action="store", type="int", default=4000)
parser.add_option("-d","--data-dir", dest="data_dir", help="look for NZS runs in DATADIR", metavar="DATADIR", action="store", type="string", default=None)
parser.add_option("-f","--thresholds-file", dest="thresholds_file", help="write thresholds to  FILE", metavar="FILE", action="store", type="string", default=None)
parser.add_option("-H","--histogram-file", dest="histogram_file", help="write monitoring histograms to  FILE", metavar="FILE", action="store", type="string", default=None)
parser.add_option("-t","--write-thresholds-file", dest="write_thresholds_file", help="write thresholds file to disk.", action="store_true",default=False)
#parser.add_option("-c","--make-thresholds-current", dest="make_thresholds_current", help="make the thresholds current for recipe uploading", action="store_true",default=False)
parser.add_option("-p","--show-fhs-plot", dest="show_fhs_plot", help="show the FHS plot when the job finishes. requires X display.", action="store_true",default=False)
parser.add_option("-i","--install-dir", dest="install_dir", help="install thresholds for recipe uploading in INSTALLDIR", metavar="INSTALLDIR", action="store", type="string", default=install_dir)
parser.add_option("-a","--activity"
    , dest="activity", help="run on data taken with ACTIVITY. automatically sets the corresponding data directory."
    , metavar="ACTIVITY"
    , action="store"
    , type="choice"
    , choices=["NZS","EOF_CALIB"]
    , default=activity)
(options, args) = parser.parse_args()

data_dir = options.data_dir
run_number = options.run_number
n_events = options.n_events
thresholds_file = options.thresholds_file
histogram_file = options.histogram_file
write_thresholds_file = options.write_thresholds_file
#make_thresholds_current = options.make_thresholds_current
show_fhs_plot = options.show_fhs_plot
install_dir = options.install_dir
activity = options.activity

# set activity to UNKNOWN if the user specified a data directory
if data_dir:
  activity = 'UNKNOWN'

# set data directory based on activity
if run_number and not data_dir:
  if 'NZS' == activity:
    data_dir = '/daqarea/lhcb/data/%d/RAW/FULL/VELO/NZS' % (now.tm_year)
  if 'EOF_CALIB' == activity:
    data_dir = '/daqarea/lhcb/data/%d/RAW/CALIB/LHCb/EOF_CALIB%d' % (now.tm_year,now.tm_year%100)

# automatic histogram file name if none is given
if run_number and not histogram_file:
  histogram_file = './header_histos_%s_%d_%d%02d%02d.root' % (activity,run_number,now.tm_year,now.tm_mon,now.tm_mday)

# automatic threshold file name if none is given
if run_number and not thresholds_file:
  thresholds_file = 'PseudoHeaderThresholds_%s_%d_%d%02d%02d.txt' % (activity,run_number,now.tm_year,now.tm_mon,now.tm_mday)

# this script can also be used to only make an existing set of thresholds current 
# for recipe uploading. in this case the use *must* supply a threshold file name.
if not run_number:
  #if not make_thresholds_current and not show_fhs_plot:
  if not show_fhs_plot:
    print "Error: No run number specified and neither threshold update nor FHS drawing requested."
    sys.exit(1)
  if show_fhs_plot and not histogram_file:
    print "Error: No run number specified and FHS drawing requested w/o specifying a histogram file."
    sys.exit(1)
  #if make_thresholds_current and not thresholds_file:
    #print "Error: No run number specified and thresholds update requested w/o specifying a thresholds file."
    #sys.exit(1)
#else:
  #if make_thresholds_current and not write_thresholds_file:
  #  write_thresholds_file = True


from ROOT import TFile, TCanvas, TH2D, gStyle

if run_number:
  print
  print "runnning Gaudi header monitor on %d events from run %d..." % (n_events,run_number)
  print 
  prepareDataFiles()
  runGaudiJob()


if show_fhs_plot:
  print
  print "showing FHS plot from histogram file %s..." % histogram_file
  print
  showFHSPlot()



