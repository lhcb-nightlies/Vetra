from Gaudi.Configuration import *
from Configurables import ( LHCbApp,
                            LHCb__RawDataCnvSvc,
                            createODIN,
                            GaudiSequencer,
                            #CheckBanks,
                            bankKiller,
                            LHCb__MDFWriter )
#from DetCond.Configuration import *
from Configurables import Velo__Monitoring__SelectBanks as SelectBanks
#from CheckBanks.CheckBanksConf import CheckBanks

app = LHCbApp()
app.DDDBtag = 'default'
app.CondDBtag = 'default'
app.DataType = '2011'
app.EvtMax = -1 

EventPersistencySvc().CnvServices.append( LHCb__RawDataCnvSvc('RawDataCnvSvc') )

EventSelector().PrintFreq = 100

ApplicationMgr().HistogramPersistency = "ROOT"
HistogramPersistencySvc().OutputFile = "rate_histos.root"

# The stuff to run
co = createODIN()
check = SelectBanks()
killer = bankKiller()
killer.BankTypes = [ 'VeloError' ]
writer = LHCb__MDFWriter( 'Writer',
                          Compress = 0,
                          ChecksumType = 1,
                          GenerateMD5 = True,
                          Connection = 'file:/tmp/NZSdata_events.raw'
                          )

# Top level sequence
topSeq = GaudiSequencer( "TopSequence" )
topSeq.Members = [ co, check, killer, writer ]

ApplicationMgr().TopAlg = [ topSeq ]
ApplicationMgr().ExtSvc += [ "UpdateManagerSvc" ]

#EventSelector().Input = [

#"DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2011/RAW/FULL/LHCb/COLLISION11/89350/089350_0000000002.raw' SVC='LHCb::MDFSelector'",

#]


