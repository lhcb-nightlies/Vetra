"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of non-zero-suppressed data.         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
##############################################################################
"""

from Gaudi.Configuration     import EventSelector
from GaudiConf.Configuration import *
from Configurables import LHCbApp
from Configurables import Vetra
vetra = Vetra()
vetra.DataBanks      = 'NONE'
vetra.RunEmulation   = False
#Will include my  own version of the emulator.
vetra.EmuMode        = 'Dynamic'
vetra.BitPerfectness = False
vetra.OutputType     = 'NONE'
# --> generated data are taken wihtout the ODIN bank
# set the appropriate DB tags
LHCbApp().DDDBtag   = 'head-20080905'
LHCbApp().CondDBtag = 'head-20080905'
#LHCbApp().CondDBtag = ''
from GaudiKernel.ProcessJobOptions import importOptions
importOptions("$VETRAROOT/options/Velo/TAEDecodingNZS.py")
#importOptions("$VETRAROOT/options/Velo/TAEEmulatorNZS.py")
#VeloTELL1Reordering().SectorCorrection = True
##############################################################################


##############################################################################


###############################################################################

from Configurables import GaudiSequencer
from Configurables import Velo__PulseShapeFitterPedSub as PulseShape
moni_pulseshape = GaudiSequencer( 'Moni_PulseShape' )
pulseShape = PulseShape( 'PulseShape' )
moni_pulseshape.Members = [pulseShape]
pulseShape.StepSize = 100000000
pulseShape.NSteps  = 50
EventSelector().FirstEvent = 1
EventSelector().PrintFreq = 1
vetra.EvtMax = 10000000000000
#pulseShape.SamplingLocations = ["Prev3", "Prev2", "Prev1","","Next1","Next2","Next3"] 
pulseShape.SamplingLocations = [ "Prev2", "Prev1","","Next1","Next2"]
pulseShape.CMSCut = 10
pulseShape.ExpectedPeakTime = -4
pulseShape.TimeWindow= 20
pulseShape.SignalThreshold = 20
pulseShape.MaxAmplitudeCut= 150
pulseShape.OffsetCut= 10 
pulseShape.IntegralCut= 30
pulseShape.FixOffset= True
pulseShape.TakeNeighbours=0
pulseShape.NTimeBins=50
pulseShape.UseLogLikeFit=False  
pulseShape.TryToClusterize = False
pulseShape.PlotSeparateStrips=False
pulseShape.OutputLevel = 4
pulseShape.InputPedFileName = "tedpedestals.root"
pulseShape.PedDirectory = "Velo/PedCalc"
#pulseShape.TELL1s=[13]
#pulseShape.WidthBefore=13.7
#pulseShape.WidthAfter=26.8 
#pulseShape.WidthBefore=15.7 from tp
#pulseShape.WidthAfter=23.3  from tp
#out from june ted: 
#pulseShape.WidthBefore=17.0
#pulseShape.WidthAfter=23.1
# @ -30
pulseShape.WidthBefore=13.4
pulseShape.WidthAfter=19.9
pulseShape.DetailedOutput=False
pulseShape.RunWithChosenBunchCounters=False
#pulseShape.ChosenBunchCounters = {};
#pulseShape.ChosenBunchCounters = [2685,2686,2687,2688,2689]
pulseShape.ChosenBunchCounters = [2685,2686,2687,2688,2689,2690,2691,2692,2693,2694,2695,2696,2697]
pulseShape.RunNumbers = [ 59019, 59021, 59017,59018] 
pulseShape.RunNumbersDelay = [ 0,  0, 0, 0]
#pulseShape.RunNumbers = [ 50425, 50426, 50427, 50428, 50430]
#pulseShape.RunNumbersDelay = [0,  0, 12.5, 19, 6.5]
#pulseShape.RunNumbers = [ 58525]
#pulseShape.RunNumbersDelay = [0]

def mystuff():
  GaudiSequencer( 'MoniVELOSeq' ).Members = [ moni_pulseshape]
appendPostConfigAction( mystuff )

##############################################################################

##############################################################################
EventSelector().Input = [
#     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55033/055033_0000000001.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55033/055033_0000000002.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55033/055033_0000000003.raw' SVC='LHCb::MDFSelector'",
#     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55033/055033_0000000004.raw' SVC='LHCb::MDFSelector'"
#  "DATA='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/TED/50425/050425_0000000001.raw'  SVC='LHCb::MDFSelector'",
#  "DATA='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/TED/50426/050426_0000000001.raw'  SVC='LHCb::MDFSelector'",
#  "DATA='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/TED/50427/050427_0000000001.raw'  SVC='LHCb::MDFSelector'",
#  "DATA='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/TED/50428/050428_0000000001.raw'  SVC='LHCb::MDFSelector'",
#  "DATA='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/TED/50430/050430_0000000001.raw'  SVC='LHCb::MDFSelector'"
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/TEDNZS/58821/058821_0000000001.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/TEDNZS/58822/058822_0000000001.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/VELO/TEDNZS/58823/058823_0000000001.raw' SVC='LHCb::MDFSelector'",
"DATAFILE='file:///castorfs/cern.ch/grid//lhcb/data/2009/RAW/FULL/LHCb/TED/59017/059017_0000000001.raw' SVC='LHCb::MDFSelector'",
"DATAFILE='file:///castorfs/cern.ch/grid//lhcb/data/2009/RAW/FULL/LHCb/TED/59018/059018_0000000001.raw' SVC='LHCb::MDFSelector'",
"DATAFILE='file:///castorfs/cern.ch/grid//lhcb/data/2009/RAW/FULL/LHCb/TED/59019/059019_0000000001.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2009/RAW/FULL/LHCb/TED/59038/059038_0000000001.raw' SVC='LHCb::MDFSelector'"
"DATAFILE='file:///castorfs/cern.ch/grid//lhcb/data/2009/RAW/FULL/LHCb/TED/59021/059021_0000000001.raw' SVC='LHCb::MDFSelector'"
]
###############################################################################
vetra.HistogramFile  = '~/forIvan/TedCheckpedsub_cleaner.root'


