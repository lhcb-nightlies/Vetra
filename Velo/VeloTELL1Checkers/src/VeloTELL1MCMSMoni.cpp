// $Id: VeloTELL1MCMSMoni.cpp,v 1.3 2009-12-15 16:17:44 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
#include "AIDA/IHistogram2D.h"
#include "AIDA/IHistogram1D.h"

// local
#include "VeloTELL1MCMSMoni.h"

// kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "VetraKernel/ContPrinter.h"
#include "Tell1Kernel/VeloTell1Core.h"

// boost
#include "boost/lexical_cast.hpp"
#include "boost/format.hpp"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTELL1MCMSMoni
//
// 2008-04-02 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloTELL1MCMSMoni )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTELL1MCMSMoni::VeloTELL1MCMSMoni( const std::string& name,
                                      ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator ),
    m_procInfos ( 0 ),
    m_procInfosLoc ( VeloProcessInfoLocation::Default ),
    m_mcmsADCs ( 0 ),
    m_mcmsLoc ( LHCb::VeloTELL1DataLocation::MCMSCorrectedADCs ),
    m_evtNumber ( 0 ),
    m_isInitialized ( false ),
    m_convergenceLimit ( 0 )
{
  declareProperty("PlotMCMSADCs", m_plotMCMSADCs=true);
}
//=============================================================================
// Destructor
//=============================================================================
VeloTELL1MCMSMoni::~VeloTELL1MCMSMoni() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloTELL1MCMSMoni::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  setHistoTopDir("Vetra/");
  // quality checker - one global histo
  boost::format h_name_q("Proj_All_Data_MCMS");
  boost::format h_title_q("All_Data_Proj_MCMS");
  m_projAllData=book1D(h_name_q.str(), h_title_q.str(), -140, 140, 280);

  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloTELL1MCMSMoni::execute() {

  m_isDebug=msgLevel(MSG::DEBUG);
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  //
  ++m_evtNumber;
  if(!m_isInitialized){
    if(!exist<VeloProcessInfos>(m_procInfosLoc)){
      if (m_isDebug) debug()<< " ==> No init info available! " <<endmsg;
      return ( StatusCode::SUCCESS );
    }else{
      m_procInfos=get<VeloProcessInfos>(m_procInfosLoc);
    }
    m_isInitialized=true;
    VeloProcessInfos::const_iterator procIt=m_procInfos->begin();
    if((*procIt)->convLimit()==0){
      if(m_isDebug) debug()<< " --> You are running with conv limit set to 0"
                           <<endmsg;
      //Error("Convergence Limit not set!", StatusCode::FAILURE);
    }else{
      m_convergenceLimit=(*procIt)->convLimit();
    }
  }
  StatusCode dataStatus, histoStatus;
  debug()<< " --> Conv limit: " << m_convergenceLimit <<endmsg;
  if(m_evtNumber>m_convergenceLimit){
    dataStatus=getData();
    if(dataStatus.isSuccess()){
      histoStatus=plotMCMSCorrectedData();
    }else{
      return ( StatusCode::SUCCESS );
    }
  }
  //
  return ( histoStatus );
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloTELL1MCMSMoni::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode VeloTELL1MCMSMoni::getData()
{
  if (m_isDebug) debug()<< " ==> getData()" <<endmsg;
  // get ADC samples after pedestal subtraction
  // it does not matter if one deals with real or
  // simulated data 
  if(!exist<LHCb::VeloTELL1Datas>(m_mcmsLoc)){
    debug()<< "There is no MCMS corrected ADC samples in TES!" 
           << "at location: " << m_mcmsLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{
    m_mcmsADCs=get<LHCb::VeloTELL1Datas>(m_mcmsLoc);
  }
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
void VeloTELL1MCMSMoni::bookMCMSCorrectedDataHistos(const int tell1)
{
  if(m_mcmsDataHistos.find(tell1)==m_mcmsDataHistos.end()){
    m_mcmsDataHistos[tell1]=
      book2D(tell1,
             "MCMS corrected data for TELL1 number "
             +boost::lexical_cast<std::string>(tell1),
             0., 2048., 2048, -130., 130., 260);
  }
}
//=============================================================================
StatusCode VeloTELL1MCMSMoni::plotMCMSCorrectedData()
{
  if (m_isDebug) debug()<< " ==> plotMCMSCorrectedData() " <<endmsg;
  //
  LHCb::VeloTELL1Datas::const_iterator sensIt;
  if(m_plotMCMSADCs){
    for(sensIt=m_mcmsADCs->begin(); sensIt!=m_mcmsADCs->end(); ++sensIt){
      VeloTELL1::ALinkPair begEnd;
      VeloTELL1::scdatIt iT;
      unsigned int channel=0;
      int TELL1=(*sensIt)->key();
      std::map<int, AIDA::IHistogram2D*>::iterator iH=
        m_mcmsDataHistos.find(TELL1);
      if(iH==m_mcmsDataHistos.end()){
        bookMCMSCorrectedDataHistos(TELL1);
        iH=m_mcmsDataHistos.find(TELL1);
      }
      LHCb::VeloTELL1Data* adcData=(*sensIt);
      VeloTELL1::sdataVec data=adcData->data();
      //if(m_numberOfEvt>m_convergenceLimit) contPrinter(data, 15);
      for(int aLink=0; aLink<NumberOfALinks; ++aLink){
        begEnd=(*adcData)[aLink];
        for(iT=begEnd.first; iT!=begEnd.second; ++iT, ++channel){
          iH->second->fill(channel, (*iT));
          m_projAllData->fill(*iT);
        }
      }
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//--
