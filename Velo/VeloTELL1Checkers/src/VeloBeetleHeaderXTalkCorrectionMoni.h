// $Id: VeloBeetleHeaderXTalkCorrectionMoni.h,v 1.4 2009-08-05 14:45:42 szumlat Exp $
#ifndef VELOBEETLEHEADERXTALKCORRECTIONMONI_H 
#define VELOBEETLEHEADERXTALKCORRECTIONMONI_H 1

// Include files
// std includes
#include <vector>
#include <map>

// ROOT
#include "TH1.h"
#include "TH2.h"

// local
#include "VeloNZSMoniBase.h"

// from event
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"


class DeVelo;

namespace AIDA {
  class IHistogram1D;
}

/** @class VeloBeetleHeaderXTalkCorrectionMoni VeloBeetleHeaderXTalkCorrectionMoni.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2009-05-09
 */

class VeloBeetleHeaderXTalkCorrectionMoni : public VeloNZSMoniBase{

  enum channels{
    FIRST_CHANNEL = 0,
    SECOND_CHANNEL = 1,
    REF_CHANNEL = 4
  };

  typedef std::map<int, std::map<int, float> >::iterator RMS_IT;
  typedef std::map<int, float>::iterator RMS_IT_VAL;
  typedef std::map<int, std::map<int, int> >::iterator CNT_IT;
  typedef std::map<int, int>::iterator CNT_IT_VAL;
  typedef std::map<int, AIDA::IHistogram1D*>::const_iterator H1D_IT;
  
public: 
  /// Standard constructor
  VeloBeetleHeaderXTalkCorrectionMoni( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloBeetleHeaderXTalkCorrectionMoni( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getData();
  void accumulateChannelsNoise();     /// collect the noise to check the BHXT correction

private:

  int m_BHXTCorrectionActivated;
  unsigned int m_referenceChannel;
  VeloProcessInfos* m_procInfos;
  std::string m_procInfosLoc;
  LHCb::VeloTELL1Datas* m_subADCs;  /// ADC samples after peds subtraction
  std::string m_subADCsLoc;         /// the container name
  bool m_isInitialized;
  unsigned int m_convLimit;         /// convergence limit
  bool m_isDebug;
  DeVelo* m_velo;
  std::map<int, std::map<int, float> > m_firstChanNoise;
  std::map<int, std::map<int, float> > m_secondChanNoise;
  std::map<int, std::map<int, float> > m_referenceChanNoise;
  std::map<int, std::map<int, int> > m_globalCnt;
  AIDA::IHistogram1D* m_usedSensors;

};
#endif // VELOBEETLEHEADERXTALKCORRECTIONMONI_H
