// $Id: VeloPedestalSubtractorMoni.h,v 1.16 2009-12-15 16:17:44 szumlat Exp $
#ifndef VELOPEDESTALSUBTRACTORMONI_H 
#define VELOPEDESTALSUBTRACTORMONI_H 1

// std includes
#include <vector>
#include <map>

// ROOT
#include "TH2.h"
#include "TH1.h"
#include "TProfile.h"

// local
#include "VeloNZSMoniBase.h"

// from event
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"

namespace AIDA {
  class IHistogram2D;
  class IHistogram1D;
  class IProfile1D;
}

/** @class VeloPedestalSubtractorMoni VeloPedestalSubtractorMoni.h
 *  
 * implementation of the basic monitor for VeloPedestalSubtractor
 * algorithm, one can produce a set of histograms with 
 * ADC samples and sebtracted pedestals
 *
 *  @author Tomasz Szumlak
 *  @date   2006-06-16
 */

class DeVelo;

class VeloPedestalSubtractorMoni : public VeloNZSMoniBase{
public:

  enum data{
    CTRL_1=6,
    CTRL_2=12,
    CTRL_3=18,
    CTRL_4=24
  };
    
  /// Standard constructor
  VeloPedestalSubtractorMoni(
  const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloPedestalSubtractorMoni( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getData();
  StatusCode plotSubtractedData();
  StatusCode plotPedestals();
  void bookSubtractedDataHistos(int tell1);
  void bookPedestalHistos(int tell1);
  void setPedFlag();
  void unsetPedFlag();
  bool pedFlag();
  void correlations();
  void plotCorrelations();
  void plot1DPedSubSummary();
  void convergenceHistory(const int pos);
  
private:

  VeloProcessInfos* m_procInfos;
  std::string m_procInfosLoc;
  LHCb::VeloTELL1Datas* m_subADCs;  /// ADC samples after peds subtraction
  std::string m_subADCsLoc;         /// the container name
  LHCb::VeloTELL1Datas* m_peds;     /// container with subtracted pedestals
  std::string m_pedsLoc;            /// the container name
  unsigned long m_numberOfEvt;      /// number of processed events
  bool m_plotSubADCs;               /// peds subtracted data will be plot
  bool m_plotPeds;                  /// if set pedestals will be plot
  bool m_isPed;                     /// check if there are pedestals at TES
  bool m_isInitialized;
  bool m_storeSumBank;
  unsigned int m_convLimit;         /// convergence limit
  bool m_isDebug;
  std::map<int, AIDA::IHistogram2D*> m_subtractedDataHistos;
  std::map<int, AIDA::IProfile1D*> m_pedestalHistos;
  std::map<int, AIDA::IProfile1D*> m_pedestalSum;
  std::map<int, AIDA::IProfile1D*> m_subDataP;
  AIDA::IHistogram1D* m_qualityHisto;
  AIDA::IHistogram1D* m_usedSensors_hist;
  std::vector<int> m_usedSensors;
  std::map<int, std::vector<AIDA::IHistogram2D*> > m_subtractedDataPerLinkHistos;
  AIDA::IHistogram2D* m_correlations;
  unsigned int m_t1;
  unsigned int m_t2;
  unsigned int m_startPedestal;
  unsigned int m_trackingStep;
  unsigned int m_pedStored;
  DeVelo* m_velo;
  int m_hitCut;
  bool m_plotCorrelations;
  bool m_plotALinks;
  bool m_removeHits;
  std::map<int, std::map<int, std::vector<float> > > m_controlPoints;
  std::map<int, std::map<int, std::vector<float> > > m_allPoints;
  std::map<int, std::map<int, std::vector<int> > > m_allPointsCnts;
  std::map<int, unsigned int> m_pedCacheState;
  std::map<int, std::vector<float> > m_pedConvergence;
  std::map<int, std::map<int, std::vector<int> > > m_controlPointsCnts;
  std::map<int, std::pair<TH1F*, TH1F*> > m_controls;
  std::map<int, TH1F*> m_pedConvHistos;
  std::map<int, TH1F*> m_pedBankHistos;
  std::map<int, TH1F*> m_controlsProj;
  std::map<int, std::vector<int> > m_pedBank;
  std::pair<TH1F*, TH1F*> m_pedSubSummary;
  std::pair<TH2F*, TH2F*> m_pedSubSummaryAll;
  AIDA::IHistogram1D* m_projAllData;
  unsigned int m_initPed;
  int m_checkConvHistory;
  int m_runOffline;
  float m_deltaPed;
  unsigned int m_convHistorySteps;
  int m_pedSubQualityCut;
  bool m_pedBanksCached;
  bool m_rr;

};
#endif // VELOPEDESTALSUBTRACTORMONI_H
