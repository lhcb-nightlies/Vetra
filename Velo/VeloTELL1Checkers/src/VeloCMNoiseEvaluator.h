// $Id: VeloCMNoiseEvaluator.h,v 1.1 2009-05-15 13:18:55 szumlat Exp $
#ifndef VELOCMNOISEEVALUATOR_H 
#define VELOCMNOISEEVALUATOR_H 1

// Include files

#include <map>

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// from event
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"

/** @class VeloCMNoiseEvaluator VeloCMNoiseEvaluator.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2009-05-04
 */
class VeloCMNoiseEvaluator : public GaudiTupleAlg{

  enum strips{
    SENSOR_STRIPS=2048
  };

public: 
  /// Standard constructor
  VeloCMNoiseEvaluator( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloCMNoiseEvaluator( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getData();
  StatusCode plotCMNoise();
  void bookCMNoiseHistos(const int tell1);

private:

  VeloProcessInfos* m_procInfos;
  std::string m_procInfosLoc;
  LHCb::VeloTELL1Datas* m_reordered;  /// ADC samples after reordering
  std::string m_reorderedLoc;          /// the container name
  LHCb::VeloTELL1Datas* m_lcms;        /// ADC samples after LCMS
  std::string m_lcmsLoc;               /// the container name
  unsigned long m_evtNumber;           /// number of processed events
  unsigned int m_convergenceLimit;
  bool m_isDebug;
  bool m_isInitialized;
  bool m_plotCMNoise;
  std::map<int, AIDA::IHistogram2D*> m_cmNoiseHistos;

};
#endif // VELOCMNOISEEVALUATOR_H
