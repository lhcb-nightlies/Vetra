// $Id: $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// local
#include "VeloNZSMoniBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloNZSMoniBase
//
// 2010-07-16 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloNZSMoniBase )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloNZSMoniBase::VeloNZSMoniBase( const std::string& name,
                                  ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator ),
    m_evtNumber ( 0 ),
    m_nzsSvc ( 0 ),
    m_nzsSvcLoc ( "Raw/Velo/NZSInfo" )
{

}
//=============================================================================
// Destructor
//=============================================================================
VeloNZSMoniBase::~VeloNZSMoniBase() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloNZSMoniBase::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloNZSMoniBase::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloNZSMoniBase::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}

//=============================================================================
StatusCode VeloNZSMoniBase::getNZSSvc()
{
  if ( msgLevel(MSG::DEBUG) ) debug()<< " ==> getNZSSvc() " <<endmsg;
  //
  if(!exist<VeloNZSInfos>(m_nzsSvcLoc)){
     if(msgLevel(MSG::DEBUG)) debug()<< " ==> Could not retrieve the NZS Svc at: "
           << m_nzsSvcLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{  
    // get data banks from default TES location
    m_nzsSvc=get<VeloNZSInfos>(m_nzsSvcLoc);
     if(msgLevel(MSG::DEBUG)) debug()<< " ==> The data have been read-in from location: "
           << m_nzsSvcLoc <<endmsg;    
  }
  //
  return ( StatusCode::SUCCESS );  
}
//=============================================================================
VeloNZSInfo* VeloNZSMoniBase::nzsSvc() const
{
  if ( msgLevel(MSG::DEBUG) ) debug()<< " ==> nzsSvc() " <<endmsg;
  //
  VeloNZSInfo* infoSvc=*(m_nzsSvc->begin());
  //
  return ( infoSvc );
}
//@-
