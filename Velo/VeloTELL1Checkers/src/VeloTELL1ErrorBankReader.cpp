// $Id: VeloTELL1ErrorBankReader.cpp,v 1.8 2010-03-01 21:17:11 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 

// local
#include "VeloTELL1ErrorBankReader.h"
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"

using namespace VeloTELL1;

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTELL1ErrorBankReader
//
// 2006-05-26 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloTELL1ErrorBankReader )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTELL1ErrorBankReader::VeloTELL1ErrorBankReader( const std::string& name,
                                                    ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator ),
    m_evtNumber ( 0 ),
    m_errorBankLoc ( VeloErrorBankLocation::Default )
{
  declareProperty("DumpErrorBank", m_dumpErrorBank=0);
}
//=============================================================================
// Destructor
//=============================================================================
VeloTELL1ErrorBankReader::~VeloTELL1ErrorBankReader() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloTELL1ErrorBankReader::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;
  setHistoTopDir( "Vetra/" );
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloTELL1ErrorBankReader::execute() {
  m_isDebug=msgLevel(MSG::DEBUG);
  if(m_isDebug) debug() << "==> Execute" << endmsg;
  //
  m_evtNumber++;
  StatusCode dataStatus=getData();
  StatusCode moniStatus;
  if(dataStatus.isSuccess()){
    moniStatus=getAllSensors();
  }
  //
  return ( moniStatus );
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloTELL1ErrorBankReader::finalize() {

  if(m_isDebug) debug() << "==> Finalize" << endmsg;
  //
  plotSummary();
  //
  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode VeloTELL1ErrorBankReader::getData()
{
  if(m_isDebug) debug()<< " ==> getData() " <<endmsg;
  //
  if(!exist<VeloErrorBanks>(m_errorBankLoc)){
      return ( Error(" --> There is no Error Bank at default location",
               StatusCode::FAILURE) );
  }else{  
    // get data banks from default TES location
    m_errorBanks=get<VeloErrorBanks>(m_errorBankLoc);
    if(m_isDebug) debug()<< " ==> The ErrorBanks have been read-in from location: "
           << m_errorBankLoc
           << ", size of error bank container (number of read-out TELL1s): "
           << m_errorBanks->size() <<endmsg;
  }
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1ErrorBankReader::getAllSensors()
{
  if(m_isDebug) debug()<< " ==> getAllSensores() " <<endmsg;
  //
  if(m_errorBanks->size()!=0){
    if(m_isDebug) debug()<< " --> retrieved banks vector from TES " <<endmsg;
    VeloErrorBanks::const_iterator eBIt;
    //
    for(eBIt=m_errorBanks->begin(); eBIt!=m_errorBanks->end(); eBIt++){
      VeloErrorBank* aBank=(*eBIt);
      if(m_isDebug) debug()<< " src id: " << aBank->key() <<endmsg;
      m_bankCounter[aBank->key()]++;
      if(m_isDebug) debug()<< " --> Iterating over the banks " <<endmsg;
      if(!aBank->isEmpty()){
        if(m_isDebug) debug()<< " --> Inside the bank " <<endmsg;
        // check Error Bank content
        dataVec errorSources=aBank->errorSources();
        if(m_isDebug) debug()<< " Number of PP-FPGAs that triggered error is: "
               << (errorSources.size()) <<endmsg;

        if(errorSources.size()>FPGA_MAX){
          info()<< " FATAL! " <<endmsg;
          return ( StatusCode::FAILURE );
        }

        if(errorSources.empty())
          info()<< " --> Error sources vector is empty "
                << " no errors were sent during data processing " <<endmsg;

        bool introduceBank=false;
        if(!introduceBank&&m_dumpErrorBank){
          info()<< " ----------------------------------------- " <<endmsg;
          info()<< " ----------------------------------------- " <<endmsg;
          info()<< " --> General information about error bank: " <<endmsg; 
          info()<< " ----------------------------------------- " <<endmsg;
          info()<< " ----------------------------------------- " <<endmsg;
          info()<< " --> Bank Length (whole): " << aBank->bankLength() <<endmsg;
          info()<< " --> Bank type: " << aBank->bankType() <<endmsg;
          info()<< " --> Bank version: " << aBank->bankVersion() <<endmsg;
          info()<< " --> Magic Pattern " << std::hex
                << aBank->magicPattern() << std::dec <<endmsg;
          info()<< " TELL1 number: " << aBank->key() <<endmsg;
          introduceBank=true;
        }

        if(m_dumpErrorBank){
          for(unsigned int src=0; src<errorSources.size(); ++src){
            info()<< " -------------------------------- " <<endmsg;
            info()<< " ==> Decoded Error Bank contents: " <<endmsg;
            info()<< " -------------------------------- " <<endmsg;
            info()<< " --> Fixed content for PP-FPGA number: " 
                  << (errorSources[src]) <<endmsg;
            info()<< " --> NZS(Ped) Bank Length: " << std::hex
                  << aBank->nzsBankLength(errorSources[src]) << std::dec <<endmsg;
            info()<< " detector ID: " 
                  << aBank->detectorID(errorSources[src]) <<endmsg;
            info()<< " l0 from error bank: " 
                  << aBank->l0EventID(errorSources[src]) <<endmsg;
            info()<< " clusterDataSectionLength: "
                  << aBank->clusterDataSectionLength(errorSources[src])
                  <<endmsg;
            info()<< " adcDataSectionLength: "
                  << aBank->adcDataSectionLength(errorSources[src]) <<endmsg;
            info()<< " numberOfCluster: "
                  << aBank->numberOfCluster(errorSources[src]) <<endmsg;
            info()<< " errorBankLength: " << std::hex
                  << aBank->errorBankLength(errorSources[src]) << std::dec <<endmsg;            
            info()<< " Bunch counter from error bank object: "
                  << aBank->bunchCounter(errorSources[src]) <<endmsg;
          }

          // take a look into evt info block
          const EvtInfo* infoPtr=aBank->evtInfo();
          info()<< " -------------------------------- " <<endmsg;
          info()<< " ==> Error Event Inflo Block      " <<endmsg;
          info()<< " -------------------------------- " <<endmsg;
          info()<< " --> Errors have been detected for "
                << (errorSources.size()) << " PP-FPGA processors " <<endmsg;
          VeloTELL1::dataVec::const_iterator errIT=errorSources.begin();
          int errBlock=0;

          for( ; errIT!=errorSources.end(); ++errIT, ++errBlock){
            info()<< " --> Error Block sent out for PP-FPGA number: "
                  << (*errIT) <<endmsg;
            info()<< " --> evt info sector: " << errBlock <<endmsg;
            info()<< " --> Pseudo flag: " 
                  << infoPtr->headerPseudoErrorFlag(errBlock) <<endmsg;
            info()<< " --> FEM PCN: " << infoPtr->FEMPCNG(errBlock) <<endmsg;
            info()<< " --> PCN Error: " << infoPtr->PCNError(errBlock) <<endmsg;
            info()<< " --> I Header Bit(s): " <<endmsg;
            VeloTELL1::dataVec bits=infoPtr->IHeader(errBlock);
            VeloTELL1::dataVec::const_iterator bitIT=bits.begin();
            unsigned int chip=0;
            for( ; bitIT!=bits.end(); ++bitIT, ++chip){
              info()<< " -> I bit for chip " << chip << ": "<< std::hex
                    << (*bitIT) << std::dec <<endmsg;
            }

          }

        }

      }else{
        if(m_isDebug) debug()<< " --> No error detected skipping to the next event " <<endmsg;
        return ( StatusCode::SUCCESS ); 
      }

    }

  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1ErrorBankReader::dumpEvtInfo(EvtInfo*& anInfo)
{
  if(m_isDebug) debug()<< " ==> dumpEvtInfo() " <<endmsg;
  //
  if(anInfo==0){
    info()<< " ==> Event Info block is empty! " <<endmsg;
    return ( StatusCode::FAILURE );
  }
  //
  info()<< " ==> Event Info for TELL1 no: " << anInfo->key() <<endmsg;
  info()<< "detectorID: " << anInfo->detectorID() <<endmsg;
  info()<< " bunchCounter: " << anInfo->bunchCounter() <<endmsg;
  info()<< " l0EventID " << anInfo->l0EventID() <<endmsg;
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloTELL1ErrorBankReader::plotSummary() const
{
  if(m_isDebug) debug()<< " --> plotSummary() " <<endmsg;
  //
  if(!m_bankCounter.empty()){
    std::map<unsigned int, int>::const_iterator cntIT=m_bankCounter.begin();
    float errCnt=0.;
    for( ; cntIT!=m_bankCounter.end(); ++cntIT){
      errCnt=static_cast<float>(cntIT->second);
      errCnt/=static_cast<float>(m_evtNumber);
      plot2D(cntIT->first, errCnt,
             10, " Error bank frequency distribution ",
             -0.5, 411.5, 0., 1.5, 412, 100);
    }
  }
}
//--
