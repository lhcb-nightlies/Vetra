// $Id: VeloBeetleHeaderXTalkCorrectionMoni.cpp,v 1.7 2009-11-17 19:16:43 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
#include "GaudiUtils/Aida2ROOT.h"

// local
#include "VeloBeetleHeaderXTalkCorrectionMoni.h"

// kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "VetraKernel/ContPrinter.h"
#include "Tell1Kernel/VeloTell1Core.h"

// boost
#include "boost/lexical_cast.hpp"
#include "boost/format.hpp"

// VeloDet
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

// AIDA
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloBeetleHeaderXTalkCorrectionMoni
//
// 2009-05-09 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloBeetleHeaderXTalkCorrectionMoni )

using namespace Gaudi::Utils;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloBeetleHeaderXTalkCorrectionMoni::VeloBeetleHeaderXTalkCorrectionMoni(
                        const std::string& name, ISvcLocator* pSvcLocator)
  : VeloNZSMoniBase ( name , pSvcLocator ),
    m_referenceChannel ( 0 ),
    m_procInfos ( 0 ),
    m_procInfosLoc ( VeloProcessInfoLocation::Default ),
    m_subADCs ( 0 ),
    m_subADCsLoc ( LHCb::VeloTELL1DataLocation::PedSubADCs ),
    m_isInitialized ( false ),
    m_convLimit ( 0 ),
    m_isDebug ( msgLevel( MSG::DEBUG ) ),
    m_velo ( 0 )
{
  declareProperty("BHXTCorrectionActivated", m_BHXTCorrectionActivated=0);
  declareProperty("ReferenceChannel", m_referenceChannel=0);
}
//=============================================================================
// Destructor
//=============================================================================
VeloBeetleHeaderXTalkCorrectionMoni::~VeloBeetleHeaderXTalkCorrectionMoni() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloBeetleHeaderXTalkCorrectionMoni::initialize(){
  StatusCode sc = VeloNZSMoniBase::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if(m_isDebug) debug() << "==> Initialize" << endmsg;
  setHistoTopDir("Vetra/");
  m_velo=getDet<DeVelo>(DeVeloLocation::Default);
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloBeetleHeaderXTalkCorrectionMoni::execute(){
  // use this trick to save time
  
  if(m_isDebug) debug() << "==> Execute" << endmsg;
  
  // event counter
  ++m_evtNumber;
  
  // initialize the algorithm
  if(!m_isInitialized){
  
    if(!exist<VeloProcessInfos>(m_procInfosLoc)){
      
      if(m_isDebug) debug()<< " ==> No init info available! " <<endmsg;
      return ( StatusCode::SUCCESS );
      
    }else{
      
      m_procInfos=get<VeloProcessInfos>(m_procInfosLoc);
    
    }
    
    m_isInitialized=true;
    VeloProcessInfos::const_iterator procIt=m_procInfos->begin();
    
    if((*procIt)->convLimit()==0){
      
      if(m_isDebug) debug()<< " --> You are running with conv limit set to 0"
                           <<endmsg;
    }else{
      
      m_convLimit=(*procIt)->convLimit();
      info()<< " --> Conv Limit: " << m_convLimit <<endmsg;
    
    }
  
  }
  
  // get the data from the TES
  StatusCode dataStatus=getData();

  if(dataStatus.isSuccess()){
    StatusCode nzsSvcStatus=this->getNZSSvc();
    if(!nzsSvcStatus.isSuccess()) return ( nzsSvcStatus );  
    accumulateChannelsNoise();
  }
  
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloBeetleHeaderXTalkCorrectionMoni::finalize(){

  if(m_isDebug) debug() << "==> Finalize" << endmsg;
  
  info()<< " --> Creating and writing the control plots for"
        << " the BHXT correction" <<endmsg;

  float RMS=0.;
  int CNT=0;
  std::string flag="";
  0==m_BHXTCorrectionActivated ? flag="Before_Correction" : flag="Corrected";
  
  //std::map<int, TH1F*> affectedChannelsNoise;
  std::map<int, AIDA::IHistogram1D*> affectedChannelsNoise;
  std::map<int, AIDA::IHistogram1D*> secondChannelsNoise;
  std::map<int, AIDA::IHistogram1D*> referenceChannelsNoise;
  
  // plot the first channel again, this time after the correction
  //assert(m_firstChanNoise.size()!=0);
  RMS_IT ch0IT=m_firstChanNoise.begin();

  boost::format h_name("TELL1_%03d/BHXT_Chann0_%s_Sensor_%03d");
  boost::format h_title("BHXT_Chan0_%s_Sensor_%03d");

  for( ; ch0IT!=m_firstChanNoise.end(); ++ch0IT){
  
    const DeVeloSensor* sensor=m_velo->sensorByTell1Id(ch0IT->first);

    if(sensor==0){
      info()<< " Cannot map Tell1 number into Sensor Number! " <<endmsg;
      return ( Error("Problem with COND DB", StatusCode::FAILURE) );
    }

    h_name % (ch0IT->first) % flag % (sensor->sensorNumber());
    h_title % flag % (sensor->sensorNumber());
    affectedChannelsNoise[ch0IT->first]=book1D(h_name.str(), h_title.str(),
                                               -0.5, 63.5, 64
    );
    TH1D* handleHist=Aida2ROOT::aida2root(affectedChannelsNoise[ch0IT->first]);
  
    RMS_IT_VAL ch0IT_V=ch0IT->second.begin();      
    for( ; ch0IT_V!=ch0IT->second.end(); ++ch0IT_V){
      CNT=m_globalCnt[ch0IT->first][ch0IT_V->first];
      RMS=sqrt(ch0IT_V->second/CNT);
      handleHist->SetBinContent(ch0IT_V->first+1, RMS);
    }
        
  }
    
  // the same for the next to first channel
  //assert(m_secondChanNoise.size()!=0);
  RMS_IT ch1IT=m_secondChanNoise.begin();

  boost::format h_name_1("TELL1_%03d/BHXT_Chann1_%s_Sensor_%03d");
  boost::format h_title_1("BHXT_Chan1_%s_Sensor_%03d");

  for( ; ch1IT!=m_secondChanNoise.end(); ++ch1IT){

    const DeVeloSensor* sensor=m_velo->sensorByTell1Id(ch1IT->first);

    if(sensor==0){
      info()<< " Cannot map Tell1 number into Sensor Number! " <<endmsg;
      return ( Error("Problem with COND DB", StatusCode::FAILURE) );
    }

    h_name_1 % (ch1IT->first) % flag % (sensor->sensorNumber());
    h_title_1 % flag % (sensor->sensorNumber());  
    secondChannelsNoise[ch1IT->first]=book1D(h_name_1.str(), h_title_1.str(),
                                               -0.5, 63.5, 64
    );
    TH1D* handleHist=Aida2ROOT::aida2root(secondChannelsNoise[ch1IT->first]);
      
    RMS_IT_VAL ch1IT_V=ch1IT->second.begin();      
    for( ; ch1IT_V!=ch1IT->second.end(); ++ch1IT_V){
      CNT=m_globalCnt[ch1IT->first][ch1IT_V->first];
      RMS=sqrt(ch1IT_V->second/CNT);
      handleHist->SetBinContent(ch1IT_V->first+1, RMS);
    }
        
  }

  //---------------------------------------------------------------------
  // it is essential to compare RMS values measured in the affected
  // channels after correction with these measured in reference channels
  //---------------------------------------------------------------------
  //assert(m_referenceChanNoise.size()!=0);
  RMS_IT chRefIT=m_referenceChanNoise.begin();

  boost::format h_name_r("TELL1_%03d/BHXT_RefChann_%s_Sensor_%03d");
  boost::format h_title_r("BHXT_RefChan_%s_Sensor_%03d");

  for( ; chRefIT!=m_referenceChanNoise.end(); ++chRefIT){
  
    const DeVeloSensor* sensor=m_velo->sensorByTell1Id(chRefIT->first);

    if(sensor==0){
      info()<< " Cannot map Tell1 number into Sensor Number! " <<endmsg;
      return ( Error("Problem with COND DB", StatusCode::FAILURE) );
    }


    h_name_r % (chRefIT->first) % flag % (sensor->sensorNumber());
    h_title_r % flag % (sensor->sensorNumber());  
    referenceChannelsNoise[chRefIT->first]=book1D(h_name_r.str(), h_title_r.str(),
                                                  -0.5, 63.5, 64
    );
    TH1D* handleHist=Aida2ROOT::aida2root(referenceChannelsNoise[chRefIT->first]);
      
    RMS_IT_VAL chRefIT_V=chRefIT->second.begin();
      
    for( ; chRefIT_V!=chRefIT->second.end(); ++chRefIT_V){
      CNT=m_globalCnt[chRefIT->first][chRefIT_V->first];
      RMS=sqrt(chRefIT_V->second/CNT);
      handleHist->SetBinContent(chRefIT_V->first+1, RMS);
    }
        
  }

  //---------------------------------------------------------------------
  // create and write 2d summary plot
  // this will store the difference between the corrected RMS and the ref
  // one, if the correction is done appropriately this value should be 
  // close to zero ADC counts
  //----------------------------------------------------------------------
  H1D_IT ch0H_IT=affectedChannelsNoise.begin();
  H1D_IT chRefH_IT=referenceChannelsNoise.begin();

  // 2d summary - whole VELO
  boost::format sum_name_2d("BHXT_SummaryPlot_");
  boost::format sum_title_2d("BHXT_SummaryPlot_ALink_vs_Sensor_%s_");
  sum_title_2d % flag;
  AIDA::IHistogram2D* summary=book2D(sum_name_2d.str()+"2D",
                                     sum_title_2d.str()+"2D",
                                     -0.5, 132.5, 133, -0.5, 63.5, 64
  );
  TH2D* summary_handle=Aida2ROOT::aida2root(summary);
    
  const unsigned int BINS_X=summary_handle->GetNbinsX();
  const unsigned int BINS_Y=summary_handle->GetNbinsY();
    
  for(unsigned int binx=1; binx<=BINS_X; ++binx){
      
    for(unsigned int biny=1; biny<=BINS_Y; ++biny){
        
      summary_handle->SetBinContent(binx, biny, -99.);
      
    }
    
  }

  // 1d summary whole VELO
  boost::format sum_name_1d("BHXT_SummaryPlot_");
  boost::format sum_title_1d("BHXT_SummaryPlot_%s_");
  sum_title_1d % flag;
  AIDA::IHistogram1D* summary_1d=book1D(sum_name_1d.str()+"1D",
                                        sum_title_1d.str()+"1D",
                                        0., 6., 64
  );

  boost::format diff_name_1d("BHXT_DiffPlot_");
  boost::format diff_title_1d("BHXT_DiffPlot_%s_");
  diff_title_1d % flag;
  AIDA::IHistogram1D* diff_1d=book1D(diff_name_1d.str()+"1D",
                                     diff_title_1d.str()+"1D",
                                     -2., 4., 100
  );
    
  unsigned int BINS=0;
  float RMS_CORR=0., RMS_REF=0.;
  assert(affectedChannelsNoise.size()==referenceChannelsNoise.size());
  float RMS_DIFF=0.;
  for( ; ch0H_IT!=affectedChannelsNoise.end(); ++ch0H_IT, ++chRefH_IT){
    
    assert(ch0H_IT->first==chRefH_IT->first);
    TH1D* handle_chan_0=Aida2ROOT::aida2root(ch0H_IT->second);
    TH1D* handle_ref=Aida2ROOT::aida2root(chRefH_IT->second);    
    BINS=handle_chan_0->GetNbinsX();

    for(unsigned int bin=1; bin<=BINS; ++bin){
      
      RMS_CORR=handle_chan_0->GetBinContent(bin);
      RMS_REF=handle_ref->GetBinContent(bin);
      RMS_DIFF=RMS_CORR-RMS_REF;
      summary_1d->fill(RMS_CORR);
      diff_1d->fill(RMS_DIFF);
      summary_handle->SetBinContent(ch0H_IT->first+1, bin, fabs(RMS_DIFF));
      
    }
      
  }
    
  return VeloNZSMoniBase::finalize();  // must be called after all other actions
}

//=============================================================================
StatusCode VeloBeetleHeaderXTalkCorrectionMoni::getData()
{
  if (m_isDebug) debug()<< " ==> getData()" <<endmsg;
  // get ADC samples after pedestal subtraction
  // it does not matter if one deals with real or
  // simulated data 
  if(!exist<LHCb::VeloTELL1Datas>(m_subADCsLoc)){
    if(m_isDebug) debug()<< "There is no subtracted ADC samples in TES!" 
                         << "at location: " << m_subADCsLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{
    m_subADCs=get<LHCb::VeloTELL1Datas>(m_subADCsLoc);
  }
  //
  return StatusCode::SUCCESS;
}

//=============================================================================
void VeloBeetleHeaderXTalkCorrectionMoni::accumulateChannelsNoise() {

  if(m_isDebug) debug() << "==>  accumulateChannelsNoise() " << endmsg;
 
  LHCb::VeloTELL1Datas::const_iterator sensIt=m_subADCs->begin();

    for( ; sensIt!=m_subADCs->end(); ++sensIt){
      VeloTELL1::ALinkPair begEnd;
      VeloTELL1::scdatIt iT;
      const unsigned int tell1=static_cast<unsigned int>((*sensIt)->key());
      if(this->nzsSvc()->counter(tell1)<=m_convLimit) continue;      
      const DeVeloSensor* sens=m_velo->sensorByTell1Id(tell1);
      LHCb::VeloTELL1Data* adcData=(*sensIt);
         
      // prepare and initialize memory
      RMS_IT chan0IT, chan1IT, chanRefIT;
      CNT_IT cnt0IT, cnt1IT, cntRefIT;
  
      // affected channel
      chan0IT=m_firstChanNoise.find(tell1);
  
      // reserve memory for RMS values - first analogue link channel
      if(chan0IT==m_firstChanNoise.end()){

        for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){
	
          m_firstChanNoise[tell1][aLink]=0.;
	        m_globalCnt[tell1][aLink]=0;
        
	      }

      }
      
      // second channel
      chan1IT=m_secondChanNoise.find(tell1);
  
      // reserve memory for RMS values - second analogue link channel
      if(chan1IT==m_secondChanNoise.end()){

        for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){
	
          m_secondChanNoise[tell1][aLink]=0.;

        
	      }

      }
      
      // reference channel
      chanRefIT=m_referenceChanNoise.find(tell1);
  
      // reserve memory for RMS values - reference channel
      if(chanRefIT==m_referenceChanNoise.end()){

        for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){
	
          m_referenceChanNoise[tell1][aLink]=0.;
     
        }

      }
      
      unsigned ref_channel=0;
      float ch0Val=0., ch1Val=0., chRefVal=0.;
      unsigned int ch0Strip=0, chRefStrip=0;
      unsigned int absChan0=0, absRefChan=0;
      0!=m_referenceChannel ? ref_channel=m_referenceChannel : ref_channel=REF_CHANNEL;
      
      // accumulate the noise
      for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){
      
        begEnd=(*adcData)[aLink];
        iT=begEnd.first;
	      ch0Val=*(iT+FIRST_CHANNEL);
	      //if(m_BHXTCorrectionActivated){
        ch1Val=*(iT+SECOND_CHANNEL);
        if(sens->isR()) chRefVal=*(iT+ref_channel);
	
        // we need to do some checkings for the Phi sensor
        if(sens->isPhi()){
	     
	        // determine chip channel...
	        unsigned int startChan=aLink*VeloTELL1::CHANNELS_PER_ALINK;
	        absChan0=startChan+FIRST_CHANNEL;
	        absRefChan=startChan+ref_channel;
	    
	        // and strip number
	        const DeVeloPhiType* phiSens=dynamic_cast<const DeVeloPhiType*>(sens);
	        ch0Strip=phiSens->ChipChannelToStrip(absChan0);
	        chRefStrip=phiSens->ChipChannelToStrip(absRefChan);
	        const unsigned int distance_to_end=VeloTELL1::CHANNELS_PER_ALINK-ref_channel; 
	        unsigned int chRefZone=0;
	        unsigned int ch0Zone=phiSens->zoneOfStrip(ch0Strip);
	        unsigned int chan=0;
	        bool match=false;
	        for( ; chan<=distance_to_end; ++chan){
	    
	          if(chan==distance_to_end) info()<< " Problem! " <<endmsg;
	          chRefStrip=phiSens->ChipChannelToStrip(absRefChan+chan);
	          chRefZone=phiSens->zoneOfStrip(chRefStrip);
	          (ch0Zone==chRefZone) ? match=true : match=false;
	          if(match){
	      
	            break;
	        
	          }else{
              continue;
      
            }
          
          }        
	    
	      assert((iT+ref_channel+chan)!=begEnd.second);
	      chRefVal=*(iT+ref_channel+chan);
        
        }
        
	      // store the actual values
	      m_firstChanNoise[tell1][aLink]+=static_cast<float>(ch0Val*ch0Val);
        m_referenceChanNoise[tell1][aLink]+=static_cast<float>(chRefVal*chRefVal);
	      //if(m_BHXTCorrectionActivated){
        m_secondChanNoise[tell1][aLink]+=static_cast<float>(ch1Val*ch1Val);
        //}
	      m_globalCnt[tell1][aLink]++;
      }

    }

  return;
}
//-
