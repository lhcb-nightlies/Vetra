// $Id: VeloTELL1PedestalsReader.h,v 1.3 2008-03-26 16:19:34 krinnert Exp $
#ifndef VELOTELL1PEDESTALSREADER_H 
#define VELOTELL1PEDESTALSREADER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "VeloEvent/VeloTELL1Data.h"

namespace AIDA {
  class IHistogram2D;
}

/** @class VeloTELL1PedestalsReader VeloTELL1PedestalsReader.h
 *  
 *  Basic monitoring algorithm to plot decoded pedestal values
 *  for each sensor (2048 channels) or for each ALink (32 channels)
 *  The most important options are PlotAllPed and plot AllALinks
 *  by default both are set to false. To dump all pedestal values
 *  (only debugging!) set flag PrintInfo
 *
 *  @author Tomasz Szumlak
 *  @date   2006-05-25
 */

using namespace LHCb;

class VeloTELL1PedestalsReader : public GaudiTupleAlg {
public: 
  /// Standard constructor
  VeloTELL1PedestalsReader( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloTELL1PedestalsReader( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getData();
  StatusCode getAllSensors();
  StatusCode plotAllPed(VeloTELL1Data*& inData);
  StatusCode plotALinks(VeloTELL1Data*& inData);
  bool pedBankFlag();
  void setPedBankFlag();
  void unsetPedBankFlag();

  void bookPedHistos(int tell1);
  void bookALinkHistos(int tell1, int aLink);

private:

  std::string m_decodedPedLoc;
  VeloTELL1Datas* m_decodedPeds;
  bool m_isPedBank;
  bool m_printInfo;
  bool m_plotAllPed;
  bool m_plotALinks;
  bool m_isDebug;

  std::map< int, AIDA::IHistogram2D* > m_pedHistos;
  std::map< int, AIDA::IHistogram2D* > m_aLinkHistos;

};
#endif // VELOTELL1PEDESTALSREADER_H
