// $Id: VeloTELL1ErrorBankReader.h,v 1.6 2009-03-11 17:42:00 szumlat Exp $
#ifndef VELOTELL1ERRORBANKREADER_H 
#define VELOTELL1ERRORBANKREADER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "VeloEvent/EvtInfo.h"
#include "VeloEvent/VeloErrorBank.h"

/** @class VeloTELL1ErrorBankReader VeloTELL1ErrorBankReader.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2006-05-26
 */

class VeloTELL1ErrorBankReader : public GaudiTupleAlg {
public: 

  enum numb{
    FPGA_MAX=4
  };

  /// Standard constructor
  VeloTELL1ErrorBankReader( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloTELL1ErrorBankReader( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getData();
  StatusCode getAllSensors();
  StatusCode dumpEvtInfo(EvtInfo*& anInfo);
  void plotSummary() const;
  
private:

  bool m_isDebug;
  int m_dumpErrorBank;
  int m_evtNumber;
  std::string m_errorBankLoc;
  VeloErrorBanks* m_errorBanks;
  std::map<unsigned int, int> m_bankCounter;
  
};
#endif // VELOTELL1ERRORBANKREARED_H
