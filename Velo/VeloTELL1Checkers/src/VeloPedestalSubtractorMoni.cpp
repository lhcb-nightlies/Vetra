// $Id: VeloPedestalSubtractorMoni.cpp,v 1.22 2010-04-05 15:17:24 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "GaudiUtils/Aida2ROOT.h"

// AIDA
#include "AIDA/IHistogram2D.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IProfile1D.h"

// local
#include "VeloPedestalSubtractorMoni.h"

// kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "VetraKernel/ContPrinter.h"
#include "Tell1Kernel/VeloTell1Core.h"

// boost
#include "boost/lexical_cast.hpp"
#include "boost/format.hpp"

// VeloDet
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloPedestalSubtractorMoni
//
// 2006-06-16 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloPedestalSubtractorMoni )

using namespace LHCb;
using namespace VeloTELL1;
using namespace Gaudi::Utils;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloPedestalSubtractorMoni::VeloPedestalSubtractorMoni( 
                            const std::string& name,ISvcLocator* pSvcLocator)
  : VeloNZSMoniBase ( name , pSvcLocator ),
    m_procInfos ( 0 ),
    m_procInfosLoc ( VeloProcessInfoLocation::Default ),
    m_subADCs ( 0 ),
    m_subADCsLoc ( LHCb::VeloTELL1DataLocation::PedSubADCs ),
    m_peds ( 0 ),
    m_pedsLoc ( LHCb::VeloTELL1DataLocation::SubPeds ),
    m_numberOfEvt ( 0 ),
    m_isPed ( false ),
    m_isInitialized ( false ),
    m_convLimit ( 0 ),
    m_isDebug ( msgLevel( MSG::DEBUG ) ),
    m_correlations ( ),
    m_pedStored ( 0 ),
    m_velo ( ),
    m_pedCacheState ( ),
    m_pedConvergence ( ),
    m_convHistorySteps ( 0 ),
    m_pedBanksCached ( false )
{
  declareProperty("PlotSubADCs", m_plotSubADCs=true);
  declareProperty("PlotPeds", m_plotPeds=true);
  declareProperty("StoreSumBank", m_storeSumBank=false);
  declareProperty("CorrelationsTell1_1", m_t1=5);
  declareProperty("CorrelationsTell1_2", m_t2=8);
  declareProperty("PlotCorrelations", m_plotCorrelations=false);
  declareProperty("PlotALinks", m_plotALinks=false);
  declareProperty("HitCut", m_hitCut=10);
  declareProperty("RemoveHits", m_removeHits=true);
  declareProperty("StartPedestal", m_startPedestal=512);
  declareProperty("TrackingStep", m_trackingStep=32);
  declareProperty("InitPed", m_initPed=512);
  declareProperty("CheckConvHistory", m_checkConvHistory=0);
  declareProperty("RunOffline", m_runOffline=true);
  declareProperty("DeltaPed", m_deltaPed=24.5);
  declareProperty("PedSubQualityCut", m_pedSubQualityCut=1);
  declareProperty("RoundRobin", m_rr=true);
}
//=============================================================================
// Destructor
//=============================================================================
VeloPedestalSubtractorMoni::~VeloPedestalSubtractorMoni() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloPedestalSubtractorMoni::initialize() {
  StatusCode sc = VeloNZSMoniBase::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if(m_isDebug) debug() << "==> Initialize" << endmsg;
  setHistoTopDir("Vetra/");
  m_velo = getDet<DeVelo>(DeVeloLocation::Default);

  // quality checker - one global histo
  boost::format h_name_q("Quality_Check");
  boost::format h_title_q("Pedestal_Subtracted_ADCs_Tail");
  m_qualityHisto=book1D(h_name_q.str(), h_title_q.str(), -20.5, 20.5, 21);

  // used sensors - global histo, needed by the VELOCOND DB updater
  boost::format h_name_u("Used_Sensors");
  boost::format h_title_u("Used_Sensor_Map");
  m_usedSensors_hist=book1D(h_name_u.str(), h_title_u.str(), -0.5, 499.5, 500);  

  // quality checker - one global histo
  boost::format h_name("Proj_All_Data_Ped");
  boost::format h_title("All_Data_Proj_Ped");
  m_projAllData=book1D(h_name.str(), h_title.str(), -140, 140, 280);
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloPedestalSubtractorMoni::execute() {
  
  if(m_isDebug) debug() << "==> Execute" << endmsg;
  //
  ++m_numberOfEvt;
  // get the information on convergence limit
  if(!m_isInitialized){
    if(!exist<VeloProcessInfos>(m_procInfosLoc)){
      if (m_isDebug) debug()<< " --> No NZS data available! " <<endmsg;
      return ( StatusCode::SUCCESS );
    }else{
      m_procInfos=get<VeloProcessInfos>(m_procInfosLoc);
    }
    m_isInitialized=true;
    VeloProcessInfos::const_iterator procIt=m_procInfos->begin();
    if((*procIt)->convLimit()==0){
      if(m_isDebug) debug()<< " --> You are running with conv limit set to 0"
                           <<endmsg;
    }else{
      m_convLimit=(*procIt)->convLimit();
    }
    m_convHistorySteps=static_cast<int>(m_convLimit/m_trackingStep);
  }
  // convergence history -> accumulate data points
  StatusCode dataStatus=getData();

  if(!m_rr){
    
    if(m_numberOfEvt<m_convLimit&&m_checkConvHistory&&m_runOffline){
      if(m_numberOfEvt%m_trackingStep==0&&dataStatus.isSuccess()){
        unsigned int step=static_cast<int>(m_numberOfEvt/m_trackingStep);
        convergenceHistory(step);
      }
    }
  }
  
  //
  if(dataStatus.isSuccess()){
    StatusCode nzsSvcStatus=this->getNZSSvc();
    if(!nzsSvcStatus.isSuccess()) return ( nzsSvcStatus );
    if(pedFlag()){
      if(m_plotCorrelations) correlations();
      StatusCode plotStatus=plotSubtractedData();
      if(plotStatus.isFailure()) return ( plotStatus );
      if(m_runOffline){
        plotStatus=plotPedestals();
        if(plotStatus.isFailure()) return ( plotStatus );
      }
      unsetPedFlag();
    }
  }
  //
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloPedestalSubtractorMoni::finalize() {

  if(m_isDebug) debug() << "==> Finalize" << endmsg;

  // --> cache the list of used sensors - critical for the dynamic
  //     VELOCOND DB updater!
  std::map<int, AIDA::IProfile1D*>::const_iterator iT;
  iT=m_pedestalHistos.begin();
  for( ; iT!=m_pedestalHistos.end(); ++iT){
    m_usedSensors_hist->fill(iT->first);
    m_usedSensors.push_back(iT->first);
  }
 
  plot1DPedSubSummary();

  return VeloNZSMoniBase::finalize();  //must be called after all other actions
}
//=============================================================================
StatusCode VeloPedestalSubtractorMoni::getData()
{
  if (m_isDebug) debug()<< " ==> getData()" <<endmsg;
  // get ADC samples after pedestal subtraction
  // it does not matter if one deals with real or
  // simulated data 
  if(!exist<LHCb::VeloTELL1Datas>(m_subADCsLoc)){
    if (m_isDebug) debug()<< "There is no subtracted ADC samples in TES!" 
                          << "at location: " << m_subADCsLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{
    m_subADCs=get<LHCb::VeloTELL1Datas>(m_subADCsLoc);
    setPedFlag();
  }
  // get subtracted pedestals (determined during pedestal
  // subtraction stage), simulated and real data are treated
  // in the same way
  if(!exist<VeloTELL1Datas>(m_pedsLoc)){
    if (m_isDebug) debug()<< "There is no pedestals in TES!" 
          << "at location: " << m_pedsLoc <<endmsg;
  }else{
    m_peds=get<VeloTELL1Datas>(m_pedsLoc);
  }
  //
  return StatusCode::SUCCESS;
}

//=============================================================================
void VeloPedestalSubtractorMoni::bookSubtractedDataHistos(int tell1)
{
  if (m_isDebug) debug()<< " ==> bookSubtractedDataHistos() " <<endmsg;

  const DeVeloSensor* sensor=m_velo->sensorByTell1Id(tell1);
  
  if(sensor==0){
    info()<< " Cannot map Tell1 number into Sensor Number! " <<endmsg;
    return;
  }

  // book profile histograms for ped subtracted data
  // and create the directory structure
  boost::format h_name("TELL1_%03d/Ped_Sub_ADCs_");
  h_name % tell1;
  boost::format h_title("Ped_Sub_ADCs_vs_Chip_Channel_sensor_%d");
  h_title % (sensor->sensorNumber());

  // 2D ped sub data
  if ( m_subtractedDataHistos.find(tell1) == m_subtractedDataHistos.end() ) {

    m_subtractedDataHistos[tell1]=book2D(h_name.str()+"2D", h_title.str(),
                                         -0.5, VeloTELL1::SENSOR_CHANNELS-0.5, 
                                         VeloTELL1::SENSOR_CHANNELS,
                                         -m_deltaPed, m_deltaPed,
                                         static_cast<int>(2*m_deltaPed)
                                  );

  }

  // the same as above but split into analogue links
  if(m_plotALinks&&m_subtractedDataPerLinkHistos.end()==
     m_subtractedDataPerLinkHistos.find(tell1)){

     char name[80];
     m_subtractedDataPerLinkHistos[tell1].resize(NumberOfALinks);

     for(int aLink=0; aLink<NumberOfALinks; ++aLink){

       sprintf(name, "Ped_Sub_Sensor%d_ALink%d", tell1, aLink);
       m_subtractedDataPerLinkHistos[tell1][aLink] =
       book2D(name,
              "Subtracted data for Sensor/ALink number "+
              boost::lexical_cast<std::string>(tell1)+
              boost::lexical_cast<std::string>("/")+
              boost::lexical_cast<std::string>(aLink),
              0., 32., 32,  -24.5, 25.5, 50
	      );
    }

  }


  if(m_subDataP.find(tell1) == m_subDataP.end()){

    m_subDataP[tell1]=bookProfile1D(h_name.str()+"Profile",
                                    h_title.str()+"Profile",
                                    -0.5, VeloTELL1::SENSOR_CHANNELS-0.5,
                                    VeloTELL1::SENSOR_CHANNELS
                      );

  }
  
}

//=============================================================================
void VeloPedestalSubtractorMoni::bookPedestalHistos(int tell1)
{
  if (m_isDebug) debug()<< " ==> bookPedestalHistos() " <<endmsg;

  const DeVeloSensor* sensor=m_velo->sensorByTell1Id(tell1);
  
  if(sensor==0){
    info()<< " Cannot map Tell1 number into Sensor Number! " <<endmsg;
    return;
  }

  // book profile histograms for ped subtracted data
  // and create the directory structure
  boost::format h_name("TELL1_%03d/Pedestal_Bank");
  h_name % tell1;
  boost::format h_title("Pedestal_Bank_vs_Chip_Channel_sensor_%d");
  h_title % (sensor->sensorNumber());

  if(m_pedestalHistos.find(tell1)==m_pedestalHistos.end()){

    m_pedestalHistos[tell1]=bookProfile1D(h_name.str(), h_title.str(),
                                          -0.5, VeloTELL1::SENSOR_CHANNELS-0.5,
                                          VeloTELL1::SENSOR_CHANNELS
    );

  }
  if(m_storeSumBank){
    // in addition store pedestal sum - should be OK 1d plots
    // note - thera are differences between c++ and python store sum here
    boost::format name("TELL1_%03d/Pedestal_Sum");
    name % tell1;
    boost::format title("Pedestal_Sum_vs_Chip_Channel_sensor_%d");
    title % (sensor->sensorNumber());

    if(m_pedestalSum.find(tell1)==m_pedestalSum.end()){

      m_pedestalSum[tell1]=bookProfile1D(name.str(), title.str(),
                                         -0.5, VeloTELL1::SENSOR_CHANNELS-0.5,
                                         VeloTELL1::SENSOR_CHANNELS
      );

    }

  }

}

//=============================================================================
StatusCode VeloPedestalSubtractorMoni::plotSubtractedData()
{
  if (m_isDebug) debug()<< " ==> plotSubtractedData() " <<endmsg;
  //
  VeloTELL1Datas::const_iterator sensIt;

  if(m_plotSubADCs){

    for(sensIt=m_subADCs->begin(); sensIt!=m_subADCs->end(); ++sensIt){
      ALinkPair begEnd;
      scdatIt iT;
      unsigned int channel=0, aLinkChannel;
      int TELL1=(*sensIt)->key();

      if(this->nzsSvc()->counter(TELL1)<m_convLimit) continue;

      std::map<int, AIDA::IHistogram2D*>::iterator iH;
      std::map<int, std::vector<AIDA::IHistogram2D*> >:: iterator iH1;
      std::map<int, AIDA::IProfile1D*>::iterator pIT;
      
      if(m_runOffline){
        iH=m_subtractedDataHistos.find(TELL1);

        if (iH == m_subtractedDataHistos.end() ) {
          bookSubtractedDataHistos(TELL1);
          iH = m_subtractedDataHistos.find(TELL1);
        }

        pIT=m_subDataP.find(TELL1);
        if(pIT==m_subDataP.end()){
          return Error("Problem with histo booking!", StatusCode::FAILURE);
        }

        if(m_plotALinks){
          iH1=m_subtractedDataPerLinkHistos.find(TELL1);
          if(iH1==m_subtractedDataPerLinkHistos.end()){
            return Error("Problem with histo booking!", StatusCode::FAILURE);
          }

        }

      }

      VeloTELL1Data* adcData=(*sensIt);
      sdataVec data=adcData->data();
      int val=0;
      int ctrl1=0, ctrl2=0, ctrl3=0, ctrl4=0;

      //if(m_numberOfEvt>m_convergenceLimit) contPrinter(data, 15);
      
      for(int aLink=0; aLink<NumberOfALinks; ++aLink){
        aLinkChannel=0;
        begEnd=(*adcData)[aLink];
        iT=begEnd.first;

        if(m_runOffline){
          
          for( ; iT!=begEnd.second; ++iT, ++channel, ++aLinkChannel){
            (m_removeHits)?((*iT)>=m_hitCut?val=0:val=(*iT)):val=(*iT);
            if(std::abs(*iT)>m_pedSubQualityCut) m_qualityHisto->fill(*iT);
            iH->second->fill(channel, val);
            pIT->second->fill(channel, val);
            m_projAllData->fill(val);
            if(m_plotALinks) iH1->second[aLink]->fill(aLinkChannel, val);
          }
          
          if(m_plotCorrelations){
            
            
            // now calculate mean values of the control points
            std::map<int, std::map<int, std::vector<float> > >::iterator meanIT;
            meanIT=m_controlPoints.find(TELL1);
            // reserve memory for means
            if(meanIT==m_controlPoints.end()){

              for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){
                m_controlPoints[TELL1][aLink].resize(4);
              }

            }

            // some of the values may be hits - we need counters
            std::map<int, std::map<int, std::vector<int> > >::iterator cntIT;
            cntIT=m_controlPointsCnts.find(TELL1);
            // reserve memory for counters
            if(cntIT==m_controlPointsCnts.end()){

              for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){
                m_controlPointsCnts[TELL1][aLink].resize(4);
              }

            }

            // retrieve the control values for each aLink
            std::abs(*(begEnd.first+CTRL_1))>=m_hitCut?
              ctrl1=0:ctrl1=*(begEnd.first+CTRL_1);
            std::abs(*(begEnd.first+CTRL_2))>=m_hitCut?
              ctrl2=0:ctrl2=*(begEnd.first+CTRL_2);
            std::abs(*(begEnd.first+CTRL_3))>=m_hitCut?
              ctrl3=0:ctrl3=*(begEnd.first+CTRL_3);
            std::abs(*(begEnd.first+CTRL_4))>=m_hitCut?
              ctrl4=0:ctrl4=*(begEnd.first+CTRL_4);

            if(ctrl1){
              m_controlPoints[TELL1][aLink][0]+=static_cast<float>(ctrl1);
              m_controlPointsCnts[TELL1][aLink][0]++;
            }

            if(ctrl2){
              m_controlPoints[TELL1][aLink][1]+=static_cast<float>(ctrl2);
              m_controlPointsCnts[TELL1][aLink][1]++;
            }

            if(ctrl3){
              m_controlPoints[TELL1][aLink][2]+=static_cast<float>(ctrl3);
              m_controlPointsCnts[TELL1][aLink][2]++;
            }

            if(ctrl4){
              m_controlPoints[TELL1][aLink][3]+=static_cast<float>(ctrl4);
              m_controlPointsCnts[TELL1][aLink][3]++;
            }
        
            // now calculate mean values for all points
            std::map<int, std::map<int, std::vector<float> > >::iterator mAllIT;
            mAllIT=m_allPoints.find(TELL1);
            // reserve memory for means
            if(mAllIT==m_allPoints.end()){

              for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){
                m_allPoints[TELL1][aLink].resize(32);
              }

            }    
        
            // some of the values may be hits - we need counters
            std::map<int, std::map<int, std::vector<int> > >::iterator cntAllIT;
            cntAllIT=m_allPointsCnts.find(TELL1);
            // reserve memory for counters
            if(cntAllIT==m_allPointsCnts.end()){

              for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){
                m_allPointsCnts[TELL1][aLink].resize(32);
              }

            }
        
            scdatIt aIT=begEnd.first;
            unsigned int chan=0;
            int ctrl=0;
            for( ; aIT!=begEnd.second; ++aIT, ++chan){
              std::abs(*aIT)>=m_hitCut?ctrl=0:ctrl=(*aIT);

              if(ctrl){
                m_allPoints[TELL1][aLink][chan]+=static_cast<float>(ctrl);
                m_allPointsCnts[TELL1][aLink][chan]++;
              
              }

            }

          }

        }

      }

    }

  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloPedestalSubtractorMoni::plotPedestals()
{
  if (m_isDebug) debug()<< " ==> plotPedestals() " <<endmsg;
  //
  VeloTELL1Datas::const_iterator sensIt;
  if(m_plotPeds){
    for(sensIt=m_peds->begin(); sensIt!=m_peds->end(); ++sensIt){
      VeloTELL1::ALinkPair begEnd;
      scdatIt iT;
      unsigned int channel=0;
      int TELL1=(*sensIt)->key();
      if(this->nzsSvc()->counter(TELL1)<m_convLimit) continue;
      if(this->nzsSvc()->counter(TELL1)==m_convLimit){
        
        m_pedBank[TELL1]=(*sensIt)->data();

      }

      std::map<int, AIDA::IProfile1D*>::iterator iH=m_pedestalHistos.find(TELL1);
      if (iH == m_pedestalHistos.end() ) {

        bookPedestalHistos(TELL1);
        iH = m_pedestalHistos.find(TELL1);

      }

      std::map<int, AIDA::IProfile1D*>::iterator sH=m_pedestalSum.find(TELL1);
      if(m_storeSumBank){

        if (sH == m_pedestalSum.end() ) {

          bookPedestalHistos(TELL1);
          sH = m_pedestalSum.find(TELL1);

        }

      }

      VeloTELL1Data* adcData=(*sensIt);
      for(int aLink=0; aLink<NumberOfALinks; ++aLink){

        begEnd=(*adcData)[aLink];
        for(iT=begEnd.first; iT!=begEnd.second; ++iT, ++channel){

          iH->second->fill(channel, (*iT));
          if(m_storeSumBank) sH->second->fill(channel, ((*iT)<<10));

        }

      }

    }

  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloPedestalSubtractorMoni::plot1DPedSubSummary()
{
  if ( !(m_plotSubADCs && m_runOffline) ) return;
  if ( m_usedSensors.empty() ) return;

  const unsigned int nbins = 100;
  auto *summary_1d_h = Aida2ROOT::aida2root(book1D("Ped_Sub_ADCs_Summary_1d", "Pedestal Subtracted ADC Summary", -10.0, 10.0, nbins));

  // retrieve pedestal subtracted ADCs from all *used* sensors
  for (auto si = m_usedSensors.begin(); si != m_usedSensors.end(); ++si) {
    auto pi = m_subDataP.find(*si);
    if ( m_subDataP.end() == pi ) continue;    
    auto *pedsubadc_p = Aida2ROOT::aida2root(pi->second);

    // get all ADC values from this sensor and fill summary histogram
    int profile_bins = pedsubadc_p->GetNbinsX();
    for (int bin=1; bin <= profile_bins; ++bin) {
      summary_1d_h->Fill(pedsubadc_p->GetBinContent(bin));
    }
  }

  // add underflow (overflow) to first (last) bin
  summary_1d_h->SetBinContent(1, summary_1d_h->GetBinContent(1)+summary_1d_h->GetBinContent(0));
  summary_1d_h->SetBinContent(nbins, summary_1d_h->GetBinContent(nbins)+summary_1d_h->GetBinContent(nbins+1));

  // scale summary plot by number of channels/100
  double scale = 20.48 * m_usedSensors.size();
  summary_1d_h->Scale(1.0/scale);

  // after the above, we can abuse the underflow and overflow bin to store the percentages
  // of pedestal subtracted ADCs that are outside the OK window.
  // this avoids having to integrate in the display code, making the display faster.
  double ok_cut = 1.5;
  double low_adc_percent = summary_1d_h->Integral(summary_1d_h->FindFixBin(-10.0,0.0,0.0), 
      summary_1d_h->FindFixBin(-ok_cut,0.0,0.0));
  double high_adc_percent = summary_1d_h->Integral(summary_1d_h->FindFixBin(ok_cut,0.0,0.0), 
      summary_1d_h->FindFixBin(10.0,0.0,0.0));
  summary_1d_h->SetBinContent(0,low_adc_percent);
  summary_1d_h->SetBinContent(nbins+1,high_adc_percent);

  // some beautification (does this get used by the GUI at all?)
  summary_1d_h->GetXaxis()->SetTitle("<ADC_{Ped Sub}>");
  summary_1d_h->GetYaxis()->SetTitle("(#Channels(|ADC|>1) [%]");
  summary_1d_h->SetMaximum(25.0);

  return;
}
//=============================================================================
void VeloPedestalSubtractorMoni::setPedFlag()
{
  m_isPed=true;
}
//=============================================================================
void VeloPedestalSubtractorMoni::unsetPedFlag()
{
  m_isPed=false;
}
//=============================================================================
bool VeloPedestalSubtractorMoni::pedFlag()
{
  return ( m_isPed );
}
//=============================================================================
void VeloPedestalSubtractorMoni::correlations()
{
  if (m_isDebug) debug()<< " ==> correlations() " <<endmsg;
  // get data for correlation plot
  Tuple corr=nTuple(101, "Correlations", CLID_ColumnWiseTuple);
  LHCb::VeloTELL1Data* t1Data=m_subADCs->object(m_t1);
  LHCb::VeloTELL1Data* t2Data=m_subADCs->object(m_t2);
  //
  VeloTELL1::ALinkPair t1Pair, t2Pair;
  std::vector<double> t1DataVec, t2DataVec;
  VeloTELL1::scdatIt it1, it2;
  int chan=0;
  // make hard cut on the hit value 10 adc default
  for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){
    t1Pair=(*t1Data)[aLink];
    t2Pair=(*t2Data)[aLink];
    it1=t1Pair.first;
    it2=t2Pair.first;
    for( ; it1!=t1Pair.second; ++it1, ++it2, ++chan){
      if(*it1>=10){
        t1DataVec.push_back(static_cast<float>(chan));
      }
      if(*it2>=10){
        t2DataVec.push_back(static_cast<float>(chan));
      }
    }
  }
  // fill the NTuple
  corr->farray("sens1", t1DataVec.begin(), t1DataVec.end(),
               "Correlations", 100000);
  corr->farray("sens2", t2DataVec.begin(), t2DataVec.end(),
               "Correlations", 100000);
  //  
  corr->write();
  return;
}
//=============================================================================
void VeloPedestalSubtractorMoni::convergenceHistory(const int step)
{
  if(m_isDebug) debug()<< " --> convergencyHistory() " <<endmsg;
  //
  VeloTELL1Datas::const_iterator sensIt;
  float initPed=static_cast<float>(m_initPed);
  for(sensIt=m_peds->begin(); sensIt!=m_peds->end(); ++sensIt){
    VeloTELL1::ALinkPair begEnd;
    VeloTELL1::scdatIt iT;
    int tell1=(*sensIt)->key();
    std::map<int, std::vector<float> >::iterator hiIT;
    hiIT=m_pedConvergence.find(tell1);
    // reserve memory for history points
    if(hiIT==m_pedConvergence.end()){
      m_pedConvergence[tell1].resize(m_convHistorySteps);
    }
    // for start choose one point per sensor
    LHCb::VeloTELL1Data* peds=(*sensIt);
    begEnd=(*peds)[24];
    float testPoint=static_cast<float>(*(begEnd.first+15));
    if(m_convHistorySteps){
      m_pedConvergence[tell1][step]=(initPed-testPoint);
    }
  }
  return;
}
//
