// $Id: $
#ifndef VELONZSMONIBASE_H 
#define VELONZSMONIBASE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// form TELL1 Kernel
#include "Tell1Kernel/VeloTell1Core.h"

// from TELL1 event
#include "VetraKernel/VeloNZSInfo.h"

/** @class VeloNZSMoniBase VeloNZSMoniBase.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2010-07-16
 */

namespace VeloTELL1{
  typedef std::map<unsigned int, unsigned int> Tell1CountersMap;
}

class VeloNZSMoniBase : public GaudiTupleAlg {
public: 
  /// Standard constructor
  VeloNZSMoniBase( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloNZSMoniBase( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization
  virtual StatusCode getNZSSvc();
  virtual VeloNZSInfo* nzsSvc() const;

protected:

  unsigned int m_evtNumber;

private:

  VeloNZSInfos* m_nzsSvc;
  std::string m_nzsSvcLoc;

};
#endif // VELONZSMONIBASE_H
