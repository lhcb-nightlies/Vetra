// $Id: VeloChannelReordererMoni.cpp,v 1.10 2009-10-10 15:39:03 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "AIDA/IHistogram2D.h"

// local
#include "VeloChannelReordererMoni.h"

// kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "VetraKernel/ContPrinter.h"
#include "Tell1Kernel/VeloTell1Core.h"

// boost
#include "boost/lexical_cast.hpp"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloChannelReordererMoni
//
// 2006-10-23 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloChannelReordererMoni )

using namespace LHCb;
using namespace VeloTELL1;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloChannelReordererMoni::VeloChannelReordererMoni( const std::string& name,
                                                    ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator ),
    m_procInfos ( 0 ),
    m_procInfosLoc ( VeloProcessInfoLocation::Default ),
    m_reorderedLoc ( LHCb::VeloTELL1DataLocation::ReorderedADCs ),
    m_numberOfEvents ( 0 ),
    m_convergenceLimit ( 0 ),
    m_isInitialized ( false )
{
  declareProperty("PlotReorderedData", m_plotReordered=true);
  declareProperty("PlotAll", m_plotAll=false);
  declareProperty("SubtractorType", m_subType=1);
}
//=============================================================================
// Destructor
//=============================================================================
VeloChannelReordererMoni::~VeloChannelReordererMoni() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloChannelReordererMoni::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;
  setHistoTopDir("Vetra/");
  // 
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloChannelReordererMoni::execute() {

  m_isDebug   = msgLevel( MSG::DEBUG );

  if (m_isDebug) debug() << "==> Execute" << endmsg;
  //
  m_numberOfEvents++;
  if(!m_isInitialized){
    if(!exist<VeloProcessInfos>(m_procInfosLoc)){
      if (m_isDebug) debug()<< " ==> No init info available! " <<endmsg;
    }else{
      m_procInfos=get<VeloProcessInfos>(m_procInfosLoc);
    }
    m_isInitialized=true;
    VeloProcessInfos::const_iterator procIt=m_procInfos->begin();
    if((*procIt)->convLimit()==0){
      if (m_isDebug) debug()<<" --> You run with conv limit set to 0! "
                            << endmsg;
      //Error("Convergence Limit not set!", StatusCode::FAILURE);
    }else{
      m_convergenceLimit=(*procIt)->convLimit();
    }
  }
  if(m_numberOfEvents>m_convergenceLimit){
    StatusCode sc=getData();
    if(sc) plotReorderedData();
  }
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloChannelReordererMoni::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode VeloChannelReordererMoni::getData()
{
  if (m_isDebug) debug()<< " ==> getData()" <<endmsg;
  // get ADC samples after channel reordering
  // it does not matter if one deals with real or
  // simulated data 
  if(!exist<LHCb::VeloTELL1Datas>(m_reorderedLoc)){
    error()<< "There is no reordered ADC samples in TES!" 
           << "at location: " << m_reorderedLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{
    m_reordered=get<LHCb::VeloTELL1Datas>(m_reorderedLoc);
  }
  //
  return ( StatusCode::SUCCESS );
}

//=============================================================================
void VeloChannelReordererMoni::bookHistograms(int tell1)
{
  if ( m_reorderedDataHistos.find(tell1) == m_reorderedDataHistos.end() ) {
    m_reorderedDataHistos[tell1] =
      book2D("reordered_adc_sensor_"+boost::lexical_cast<std::string>(tell1),
             "Reordered ADCs data for TELL1 number "+
             boost::lexical_cast<std::string>(tell1),
             -0.5, 2047.5, 2048, -129.5, 130.5, 260
      );
  }
}
//=============================================================================
void VeloChannelReordererMoni::bookFullHistograms(int tell1)
{
  if ( m_reorderedDataHistos.find(tell1) == m_reorderedDataHistos.end() ) {
    m_reorderedDataHistos[tell1] =
      book2D("reordered_all_adc_sensor_"+boost::lexical_cast<std::string>(tell1),
             "Reordered ADCs data for TELL1 number "+
             boost::lexical_cast<std::string>(tell1),
             -0.5, 2303.5, 2304, -129.5, 130.5, 260
      );
  }
}
//=============================================================================
StatusCode VeloChannelReordererMoni::plotReorderedData()
{
  if (m_isDebug) debug()<< " plotReorderedData() " <<endmsg;
  //
  if(m_plotReordered){
    
    VeloTELL1Datas::const_iterator sensIt=m_reordered->begin();
    for( ; sensIt!=m_reordered->end(); ++sensIt){
      
      ALinkPair begEnd;
      scdatIt iT;
      int TELL1=(*sensIt)->key();
      std::map< int, AIDA::IHistogram2D* >::iterator iH;
      iH=m_reorderedDataHistos.find(TELL1);

      if (iH == m_reorderedDataHistos.end() ){
        
        m_plotAll ? bookFullHistograms(TELL1) : bookHistograms(TELL1);
        iH = m_reorderedDataHistos.find(TELL1);

      }

      VeloTELL1Data* adcData=(*sensIt);
      int stripADC=0;
      assert(adcData->isReordered());

      if(m_plotAll){

        sdataVec data=adcData->data();
        sdataVec::iterator iT=data.begin();
        unsigned int pos=0;

        for( ; iT!=data.end(); ++iT, ++pos){

          iH->second->fill(pos, (*iT));

        }

      }else{
        
        for(int strip=0; strip<STRIPS; ++strip){
          stripADC=adcData->stripADC(strip);
          iH->second->fill(strip, stripADC);

        }

      }      
      //for(int aLink=0; aLink<NumberOfALinks; ++aLink){
      //  begEnd=(*adcData)[aLink];
      //  for(iT=begEnd.first; iT!=begEnd.second; ++iT, ++channel){
      //    iH->second->fill(channel, (*iT));
      //  }
      //}
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//
