// $Id: VeloCMNoiseEvaluator.cpp,v 1.2 2009-09-20 19:04:27 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
#include "AIDA/IHistogram2D.h"

// local
#include "VeloCMNoiseEvaluator.h"

// kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "VetraKernel/ContPrinter.h"
#include "Tell1Kernel/VeloTell1Core.h"

// boost
#include "boost/lexical_cast.hpp"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloCMNoiseEvaluator
//
// 2009-05-04 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloCMNoiseEvaluator )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloCMNoiseEvaluator::VeloCMNoiseEvaluator( const std::string& name,
                                            ISvcLocator* pSvcLocator)
  : GaudiTupleAlg( name , pSvcLocator ),
    m_procInfos ( 0 ),
    m_procInfosLoc ( VeloProcessInfoLocation::Default ),
    m_reordered ( 0 ),
    m_reorderedLoc ( LHCb::VeloTELL1DataLocation::ReorderedADCs ),
    m_lcms ( 0 ),
    m_lcmsLoc ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ),
    m_evtNumber ( 0 ),
    m_isInitialized ( false )
{
  declareProperty("PlotCMNoise", m_plotCMNoise=true);
}
//=============================================================================
// Destructor
//=============================================================================
VeloCMNoiseEvaluator::~VeloCMNoiseEvaluator() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloCMNoiseEvaluator::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloCMNoiseEvaluator::execute() {

  m_isDebug=msgLevel(MSG::DEBUG);
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute()" << endmsg;
  //
  ++m_evtNumber;
  if(!m_isInitialized){
    if(!exist<VeloProcessInfos>(m_procInfosLoc)){
      if (m_isDebug) debug()<< " ==> No init info available! " <<endmsg;
    }else{
      m_procInfos=get<VeloProcessInfos>(m_procInfosLoc);
    }
    m_isInitialized=true;
    VeloProcessInfos::const_iterator procIt=m_procInfos->begin();
    if((*procIt)->convLimit()==0){
      if(m_isDebug) debug()<< " --> You are running with conv limit set to 0"
                           <<endmsg;
    }else{
      m_convergenceLimit=(*procIt)->convLimit();
    }
  }
  StatusCode dataStatus, histoStatus;
  if(m_evtNumber>m_convergenceLimit){
    dataStatus=getData();
    if(dataStatus.isSuccess()){
      histoStatus=plotCMNoise();
    }else{
      return ( dataStatus );
    }
  }
  //
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloCMNoiseEvaluator::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}

//=============================================================================
StatusCode VeloCMNoiseEvaluator::getData()
{
  if (m_isDebug) debug()<< " ==> getData()" <<endmsg;

  if(!exist<LHCb::VeloTELL1Datas>(m_reorderedLoc)){
    error()<< "There is no MCMS corrected ADC samples in TES!" 
           << "at location: " << m_reorderedLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{
    m_reordered=get<LHCb::VeloTELL1Datas>(m_reorderedLoc);
  }
  //
  if(!exist<LHCb::VeloTELL1Datas>(m_lcmsLoc)){
    error()<< "There is no MCMS corrected ADC samples in TES!" 
           << "at location: " << m_lcmsLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{
    m_lcms=get<LHCb::VeloTELL1Datas>(m_lcmsLoc);
  }
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
void VeloCMNoiseEvaluator::bookCMNoiseHistos(const int tell1)
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> bookCMNoiseHistos()" << endmsg;
  if(m_cmNoiseHistos.find(tell1)==m_cmNoiseHistos.end()){
    m_cmNoiseHistos[tell1]=
      book2D(tell1, " CM_noise_for_sensor_"
             +boost::lexical_cast<std::string>(tell1),
             0., 2047., 2048, -30., 30., 60);
  }
}
//=============================================================================
StatusCode VeloCMNoiseEvaluator::plotCMNoise()
{
  if (m_isDebug) debug()<< " ==> plotCMNoise() " <<endmsg;
  //
  LHCb::VeloTELL1Datas::const_iterator rIT, lIT;
  float cmNoise=0.;
  if(m_plotCMNoise){
    rIT=m_reordered->begin();
    lIT=m_lcms->begin();
    for( ; rIT!=m_reordered->end(); ++rIT, ++lIT){
      int tell1R=(*rIT)->key();
      //      int tell1L=(*lIT)->key();
      //      assert(tell1R==tell1L); # warning about unused variable tell1L
      std::map<int, AIDA::IHistogram2D*>::iterator iH=
        m_cmNoiseHistos.find(tell1R);
      if(iH==m_cmNoiseHistos.end()){
        bookCMNoiseHistos(tell1R);
        iH=m_cmNoiseHistos.find(tell1R);
      }
      LHCb::VeloTELL1Data* rData=(*rIT);
      LHCb::VeloTELL1Data* lData=(*lIT);
      if(rData->isReordered()&&lData->isReordered()){
        for(int str=0; str<SENSOR_STRIPS; ++str){
          cmNoise=static_cast<float>(rData->stripADC(str)-lData->stripADC(str));
          iH->second->fill(str, cmNoise);
        }
      }
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//--
