// $Id: VeloTELL1ADCReader.h,v 1.4 2008-03-26 16:19:33 krinnert Exp $
#ifndef VELOTELL1ADCREADER_H 
#define VELOTELL1ADCREADER_H 1

// Include files
#include <vector>
#include <map>


// from Gaudi
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/EvtInfo.h"
#include "Event/VeloODINBank.h"
#include "VeloEvent/VeloProcessInfo.h"

// local
#include "VeloNZSMoniBase.h"

/** @class VeloTELL1ADCReader VeloTELL1ADCReader.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2006-05-25
 */
namespace AIDA {
  class IHistogram2D;
}

class DeVelo;

class VeloTELL1ADCReader : public VeloNZSMoniBase{
public: 
  /// Standard constructor
  VeloTELL1ADCReader( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloTELL1ADCReader( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getData();
  StatusCode getAllSensors();
  StatusCode plotAll(LHCb::VeloTELL1Data*& inData, AIDA::IHistogram2D* histo);
  StatusCode plotRealAndDummy(LHCb::VeloTELL1Data*& inData, AIDA::IHistogram2D* histo);
  StatusCode plotALinks(LHCb::VeloTELL1Data*& inData, const std::vector< AIDA::IHistogram2D* >& histos);
  StatusCode dumpEvtInfo(EvtInfo*& anInfo);
  void odinInfo();
  void setADCBankFlag();
  void unsetADCBankFlag();
  void setEvtInfoFlag();
  void unsetEvtInfoFlag();
  bool adcBankFlag();
  bool evtInfoFlag();
  void finalProcess();
  void bookHistograms(int TELL1);

private:

  DeVelo* m_velo;
  VeloProcessInfos* m_procInfos;
  std::string m_procInfosLoc;
  std::string m_decodedADCLoc;       /// location of decoded ADC in TES
  std::string m_decodedHeaderLoc;    /// location of decoded headers in TES
  std::string m_evtInfoLoc;          /// location of decoded Event Info
  std::string m_odinLoc;             /// location of the decoded ODIN bank
  LHCb::VeloTELL1Datas* m_decodedADCs;     /// container for ADCs
  LHCb::VeloTELL1Datas* m_decodedHeaders;  /// container for ADC headers
  EvtInfos* m_evtInfos;              /// container for Event Info blocks
  VeloODINBanks* m_odin;             /// container with ODIN bank
  bool m_isADCBank;                  /// set the flag if ADC data is present
  bool m_isEvtInfo;                  /// flag for the EvtInfo
  bool m_printInfo;                  /// for data dump
  bool m_plotAllData;                /// plot all ADCs vs. all channels
  bool m_plotALinks;                 /// plot ADCs vs. channels for each ALink
  bool m_odinDump;                   /// dump the ODIN bank if the flag is set
  unsigned int m_eventNumber;
  unsigned int m_subType;            /// by default fast subtractor is used (1)
  unsigned int m_convLimit;
  bool m_isInitialized;
  bool m_isDebug;
  int m_sensor;         /// because of mem issue plot data from just one sensor

  //= Histograms
  std::map< int, AIDA::IHistogram2D* >                m_decodedDataHistos;
  std::map< int, AIDA::IHistogram2D* >                m_decodedAndDummyDataHistos;
  std::map< int, std::vector< AIDA::IHistogram2D* > > m_decodedDataPerLinkHistos;

};
#endif // VELOTELL1ADCREADER_H
