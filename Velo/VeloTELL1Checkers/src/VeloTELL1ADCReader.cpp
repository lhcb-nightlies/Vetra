// $Id: VeloTELL1ADCReader.cpp,v 1.9 2009-10-10 15:39:03 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "AIDA/IHistogram2D.h"

// local
#include "VeloTELL1ADCReader.h"

// Kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "VetraKernel/ContPrinter.h"
#include "Tell1Kernel/VeloTell1Core.h"

// boost
#include "boost/lexical_cast.hpp"
#include "boost/format.hpp"

// VeloDet
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"

using namespace VeloTELL1;
using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTELL1ADCReader
//
// 2006-05-25 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloTELL1ADCReader )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTELL1ADCReader::VeloTELL1ADCReader( const std::string& name,
                                        ISvcLocator* pSvcLocator)
  : VeloNZSMoniBase ( name , pSvcLocator ),
    m_velo ( ),
    m_procInfos ( 0 ),
    m_procInfosLoc ( VeloProcessInfoLocation::Default ),
    m_decodedADCLoc ( VeloTELL1DataLocation::ADCs ),
    m_decodedHeaderLoc ( VeloTELL1DataLocation::Headers ),
    m_evtInfoLoc ( EvtInfoLocation::Default ),
    m_odinLoc ( VeloODINBankLocation::Default ),
    m_isADCBank ( false ),
    m_isEvtInfo ( false ),
    m_eventNumber ( 0 ),
    m_convLimit ( 0 ),
    m_isInitialized ( false )
{ 
  declareProperty("PrintInfo", m_printInfo=false);
  declareProperty("PlotAllData", m_plotAllData=false);
  declareProperty("PlotALinks", m_plotALinks=false);
  declareProperty("DumpODIN", m_odinDump=false);
  declareProperty("SubtractorType", m_subType=1);
  declareProperty("Sensor", m_sensor=-99);
  declareProperty("InputADC", m_decodedADCLoc="Raw/Velo/PreparedADCs");
}
//=============================================================================
// Destructor
//=============================================================================
VeloTELL1ADCReader::~VeloTELL1ADCReader() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloTELL1ADCReader::initialize() {

  StatusCode sc=VeloNZSMoniBase::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  m_velo = getDet<DeVelo>(DeVeloLocation::Default);
  debug() << "==> Initialize" << endmsg;
  setHistoTopDir( "Vetra/" );
  //
  return StatusCode::SUCCESS;

}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloTELL1ADCReader::execute() {

  m_isDebug = msgLevel( MSG::DEBUG );

  if (m_isDebug) debug() << "==> Execute" << endmsg;
  //
  m_eventNumber++;

  if(!m_isInitialized){

    if(!exist<VeloProcessInfos>(m_procInfosLoc)){

      if (m_isDebug) debug()<< " ==> No init info available! " <<endmsg;
      return ( StatusCode::SUCCESS );

    }else{

      m_procInfos=get<VeloProcessInfos>(m_procInfosLoc);

    }

    VeloProcessInfos::const_iterator procIt=m_procInfos->begin();

    if((*procIt)->convLimit()==0){

      if(m_isDebug) debug()<< " --> You are running with conv limit set to 0"
                           <<endmsg;

    }else{

      m_convLimit=(*procIt)->convLimit();

    }

    m_isInitialized=true;

  }

  //if(m_eventNumber>m_convLimit){

  StatusCode dataStatus=getData();

  if(dataStatus.isSuccess())
  {

    StatusCode nzsSvcStatus=this->getNZSSvc();
    if(!nzsSvcStatus.isSuccess()) return ( nzsSvcStatus );

    if(adcBankFlag()){

      getAllSensors();

    }

    if(m_odinDump) odinInfo();
    finalProcess();

  }
  
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloTELL1ADCReader::finalize() {

  debug() << "==> Finalize" << endmsg;
  // must be called after all other actions
  return ( VeloNZSMoniBase::finalize() );

}
//=============================================================================
StatusCode VeloTELL1ADCReader::getData()
{
  if (m_isDebug) debug()<< " ==> getData() " <<endmsg;
  // check and get the decoded ADCs
  if(!exist<VeloTELL1Datas>(m_decodedADCLoc)){
    if (m_isDebug) debug()<< " ==> There is no decoded ADCs at: "
                          << m_decodedADCLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{  
    // get data banks from default TES location
    m_decodedADCs=get<VeloTELL1Datas>(m_decodedADCLoc);
    if (m_isDebug) debug()<< " ==> The data banks have been read-in from location: "
           << m_decodedADCLoc
           << ", size of data container (number of read-out TELL1s): "
           << m_decodedADCs->size() <<endmsg;
    setADCBankFlag();                 // there is data so set the flag!
  }
  // check and get decoded ADC headers
  if(!exist<VeloTELL1Datas>(m_decodedHeaderLoc)){
    if (m_isDebug) debug()<< " ==> There is no decoded ADC headers at: "
                          << m_decodedHeaderLoc <<endmsg;
  }else{  
    // get data banks from default TES location
    m_decodedHeaders=get<VeloTELL1Datas>(m_decodedHeaderLoc);
    if (m_isDebug) debug()<< " ==> The decoded ADC headers have been read-in from location: "
           << m_decodedHeaderLoc
           << ", size of ADCH container (number of read-out TELL1s): "
           << m_decodedHeaders->size() <<endmsg;
  }
  //
  if(!exist<EvtInfos>(m_evtInfoLoc)){
    if (m_isDebug) debug() << " ==> There is no event info at: "
           << m_evtInfoLoc <<endmsg;
  }else{  
    // get data banks from default TES location
    m_evtInfos=get<EvtInfos>(m_evtInfoLoc);
    if (m_isDebug) debug()<< " ==> The EvtInfos have been read-in from location: "
           << m_evtInfoLoc
           << ", size of data container (number of read-out TELL1s): "
           << m_evtInfos->size() <<endmsg;
    setEvtInfoFlag();                 // there is data so set the flag!
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1ADCReader::getAllSensors()
{
  if (m_isDebug) debug() << " ==> getAllSensors " <<endmsg;
  //
  // declare iterator for data object with ADC samples 
  // for each sensor
  VeloTELL1Datas::const_iterator sensIt;
  // vector for 32 channels SIGNED values!
  ALinkPair begEndADC, begEndHeader;
  // declare iterator for navigate through 
  // vector with channels cdatIt -> signed_const_data_iterator
  scdatIt ItADC;
  scdatIt ItHeader;
  // loop over sensor data
  if(adcBankFlag()){
    for(sensIt=m_decodedADCs->begin(); sensIt!=m_decodedADCs->end(); ++sensIt){
      // temporary object for ADCs 
      VeloTELL1Data* adcData=(*sensIt);
      const int TELL1=adcData->key();

      if(this->nzsSvc()->counter(TELL1)<m_convLimit) continue;

      // make some not very sophisticated plots...
      if (m_plotAllData) {
        std::map< int, AIDA::IHistogram2D* >::iterator iHisto = m_decodedDataHistos.find(TELL1);
        if(m_sensor>0){
          if(TELL1!=m_sensor) continue;
        }
        if (iHisto == m_decodedDataHistos.end()) {
          bookHistograms(TELL1);
          iHisto = m_decodedDataHistos.find(TELL1);
        }
        plotAll(adcData,iHisto->second);
        //plotRealAndDummy(adcData,m_decodedAndDummyDataHistos[TELL1]);
      }
      if (m_plotALinks) {
        std::map< int, std::vector< AIDA::IHistogram2D* > >::iterator iHistos = m_decodedDataPerLinkHistos.find(TELL1);
        if (iHistos == m_decodedDataPerLinkHistos.end()) {
          bookHistograms(TELL1);
          iHistos = m_decodedDataPerLinkHistos.find(TELL1);
        }
        plotALinks(adcData,iHistos->second);
      }

      // get headers for the given TELL1 number
      VeloTELL1Data* adcHeaders=m_decodedHeaders->object(TELL1);
      // check Event info block for the data
      EvtInfo* anInfo=m_evtInfos->object(TELL1);
      if(m_printInfo){
        if (evtInfoFlag()) dumpEvtInfo(anInfo);
        // get ALinks
        info()<< " ==> Decoded data for TELL1 no: " << TELL1 <<endmsg;
        for(int ALinkCounter=0; ALinkCounter<NumberOfALinks; ++ALinkCounter){
          // returns vector with 32 channels (one ALink)
          info()<< " ==> Decoded data from ALink [" << ALinkCounter
            << "] " <<endmsg;
          std::cout<< " > ADC samples " <<std::endl;
          //
          begEndADC=(*adcData)[ALinkCounter];
          begEndHeader=(*adcHeaders)[ALinkCounter];
          ItADC=begEndADC.first;
          ItHeader=begEndHeader.first;
          int channelCounter=0;
          // check output
          for( ; ItADC!=begEndADC.second; ItADC+=4){
            for(int chanCounter=0; chanCounter<4; ++chanCounter){
              std::cout<< " chan[" << channelCounter << "]: ";
              if(channelCounter<10) std::cout<< " ";
              std::cout<< *(ItADC+chanCounter) << "\t";
              channelCounter++;
            }
            std::cout<<std::endl;
          }
          //
          std::cout<< " > Decoded ADC Headers: " <<std::endl;
          int header=0;
          for( ; ItHeader!=begEndHeader.second; ++ItHeader){
            std::cout<< " adcHead[" << header << "]: "
              << *(ItHeader);
            ++header;
          }
          std::cout<<std::endl;
        }
      }
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1ADCReader::dumpEvtInfo(EvtInfo*& anInfo)
{
  if (m_isDebug) debug()<< " ==> dumpEvtInfo() " <<endmsg;
  //
  if(anInfo==0){
    info()<< " ==> Event Info block is empty! " <<endmsg;
    return ( StatusCode::FAILURE );
  }
  //
  info()<< " ==> Event Info for TELL1 no: " << anInfo->key() <<endmsg;
  info()<< "detectorID: " << anInfo->detectorID() <<endmsg;
  info()<< " bunchCounter: " << anInfo->bunchCounter() <<endmsg;
  info()<< " l0EventID " << anInfo->l0EventID() <<endmsg;
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloTELL1ADCReader::bookHistograms(int tell1)
{
  if (m_isDebug) debug()<< " ==> bookHistograms() " <<endmsg;
 
  const DeVeloSensor* sensor=m_velo->sensorByTell1Id(tell1);
  
  if(sensor==0){
    info()<< " Cannot map Tell1 number into Sensor Number! " <<endmsg;
    return;
  }

  // book profile histograms for ped subtracted data
  // and create the directory structure
  boost::format h_name("TELL1_%03d/Raw_ADC_");
  h_name % tell1;
  boost::format h_title("Raw_ADCs_vs_Chip_Channel_sensor_%d");
  h_title % (sensor->sensorNumber());

  // book only required histograms and don't book any twice 
  if(m_plotAllData&&m_decodedDataHistos.end()==m_decodedDataHistos.find(tell1)){
    m_decodedDataHistos[tell1]=book2D(h_name.str()+"2D", h_title.str(),
                                      -0.5, 2047.5, 2048, 420., 620., 200);
  }
  //
  if(m_plotALinks&&m_decodedDataPerLinkHistos.end()==m_decodedDataPerLinkHistos.find(tell1)) {
    m_decodedDataPerLinkHistos[tell1].resize(NumberOfALinks);
    for(int aLink=0; aLink<NumberOfALinks; ++aLink){
      m_decodedDataPerLinkHistos[tell1][aLink] =
        book2D(aLink+1000*tell1,
               "Decoded data for Sensor/ALink number "+
                boost::lexical_cast<std::string>(tell1)+
                boost::lexical_cast<std::string>("/")+
                boost::lexical_cast<std::string>(aLink), 
                0., 32., 32,  350., 650., 300);
    }
  }
  return;
}
//=============================================================================
StatusCode VeloTELL1ADCReader::plotAll(VeloTELL1Data*& inData, AIDA::IHistogram2D* histo)
{
  if (m_isDebug) debug()<< " ==> plotAll() " <<endmsg;
  //
  ALinkPair begEnd;
  scdatIt iT;
  unsigned int channel=0;
  sdataVec data=inData->data();
  //if(m_eventNumber>m_convLimit) contPrinter(data, 1000);
  for(int aLink=0; aLink<NumberOfALinks; ++aLink){
    begEnd=(*inData)[aLink];
    for(iT=begEnd.first; iT!=begEnd.second; ++iT, ++channel){
      histo->fill(channel, (*iT));
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1ADCReader::plotRealAndDummy(VeloTELL1Data*& inData, AIDA::IHistogram2D* histo)
{
  if (m_isDebug) debug()<< " ==> plotRealAndDummy() " <<endmsg;
  //
  scdatIt iT;
  unsigned int channel=0;
  sdataVec data=inData->data();
  //if(m_eventNumber>m_convLimit) contPrinter(data, 535);
  for(iT=data.begin(); iT!=data.end(); ++iT, ++channel){
    histo->fill(channel, (*iT));
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1ADCReader::plotALinks(VeloTELL1Data*& inData, const std::vector< AIDA::IHistogram2D* >& histos )
{
  if (m_isDebug) debug()<< " ==> plotALinks() " <<endmsg;
  //
  ALinkPair begEnd;
  scdatIt iT;
  for(int aLink=0; aLink<NumberOfALinks; ++aLink){
    AIDA::IHistogram2D* h = histos[aLink];
    unsigned int channel=0;
    begEnd=(*inData)[aLink];
    for(iT=begEnd.first; iT!=begEnd.second; ++iT, ++channel){
      h->fill(channel, (*iT));
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloTELL1ADCReader::setADCBankFlag()
{
  if (m_isDebug) debug()<< " ==> setADCBankFlag() " <<endmsg;
  //
  m_isADCBank=true;
}
//=============================================================================
void VeloTELL1ADCReader::unsetADCBankFlag()
{
  if (m_isDebug) debug()<< " ==> unsetADCBankFlag() " <<endmsg;
  //
  m_isADCBank=false;
}
//=============================================================================
void VeloTELL1ADCReader::setEvtInfoFlag()
{
  if (m_isDebug) debug()<< " ==> setEvtInfoFlag() " <<endmsg;
  //
  m_isEvtInfo=true;
}
//=============================================================================
void VeloTELL1ADCReader::unsetEvtInfoFlag()
{
  if (m_isDebug) debug()<< " ==> unsetEvtInfoFlag() " <<endmsg;
  //
  m_isEvtInfo=false;
}
//=============================================================================
bool VeloTELL1ADCReader::adcBankFlag()
{
  return ( m_isADCBank );
}
//=============================================================================
bool VeloTELL1ADCReader::evtInfoFlag()
{
  return ( m_isEvtInfo );
}
//=============================================================================
void VeloTELL1ADCReader::finalProcess()
{
  if (m_isDebug) debug()<< " ==> finalProcess() " <<endmsg;
  //
  unsetADCBankFlag();
  unsetEvtInfoFlag();
}
//=============================================================================
void VeloTELL1ADCReader::odinInfo()
{
  if (m_isDebug) debug()<< " ==> odinInfo() " <<endmsg;
  //
  VeloODINBanks::const_iterator odinIt=m_odin->begin();
  VeloODINBank* odin=(*odinIt);
  info()<< " Size of the ODIN bank: " << odin->odinSize() <<endmsg;
  info()<< " ODIN source ID: " << odin->odinSourceID() <<endmsg;
  info()<< " ODIN version: " << odin->odinVersion() <<endmsg;
  info()<< " ODIN type: " << odin->odinType() <<endmsg;
  info()<< " Run number: " << odin->runNumber() <<endmsg;
  info()<< " Event type: " << odin->eventType() <<endmsg;
}
//--
