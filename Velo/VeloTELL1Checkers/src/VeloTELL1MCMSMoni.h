// $Id: VeloTELL1MCMSMoni.h,v 1.2 2009-12-15 16:17:44 szumlat Exp $
#ifndef VELOTELL1MCMSMONI_H 
#define VELOTELL1MCMSMONI_H 1

// Include files
#include <map>

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// from event
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"

/** @class VeloTELL1MCMSMoni VeloTELL1MCMSMoni.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2008-04-02
 */

namespace AIDA {
  class IHistogram1D;
}

class VeloTELL1MCMSMoni : public GaudiTupleAlg{

public: 
  /// Standard constructor
  VeloTELL1MCMSMoni( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloTELL1MCMSMoni( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getData();
  StatusCode plotMCMSCorrectedData();
  void bookMCMSCorrectedDataHistos(int tell1);

private:

  VeloProcessInfos* m_procInfos;
  std::string m_procInfosLoc;
  LHCb::VeloTELL1Datas* m_mcmsADCs;  /// ADC samples after peds subtraction
  std::string m_mcmsLoc;             /// the container name
  unsigned long m_evtNumber;         /// number of processed events
  bool m_plotMCMSADCs;                /// peds subtracted data will be plot
  bool m_isInitialized;
  unsigned int m_convergenceLimit;
  bool m_isDebug;
  std::map< int, AIDA::IHistogram2D* > m_mcmsDataHistos;
  AIDA::IHistogram1D* m_projAllData;

};
#endif // VELOTELL1MCMSMONI_H
