// $Id: VeloTELL1PedestalsReader.cpp,v 1.4 2008-03-26 16:19:34 krinnert Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "AIDA/IHistogram2D.h"

// local
#include "VeloTELL1PedestalsReader.h"
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"

// boost
#include "boost/lexical_cast.hpp"

using namespace VeloTELL1;

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTELL1PedestalsReader
//
// 2006-05-25 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloTELL1PedestalsReader )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTELL1PedestalsReader::VeloTELL1PedestalsReader( const std::string& name,
                                                    ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator ),
    m_decodedPedLoc ( VeloTELL1DataLocation::Pedestals ),
    m_isPedBank ( false )
{ 
  declareProperty("PrintInfo", m_printInfo=false);
  declareProperty("PlotAllPed", m_plotAllPed=false);
  declareProperty("PlotALinks", m_plotALinks=false);
}
//=============================================================================
// Destructor
//=============================================================================
VeloTELL1PedestalsReader::~VeloTELL1PedestalsReader() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloTELL1PedestalsReader::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;
  setHistoTopDir( "Vetra/" );
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloTELL1PedestalsReader::execute() {

  m_isDebug = msgLevel( MSG::DEBUG );

  if (m_isDebug) debug() << "==> Execute" << endmsg;
  //
  getData();
  if(m_isPedBank){
    getAllSensors();
  }
  if(pedBankFlag()) unsetPedBankFlag();
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloTELL1PedestalsReader::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode VeloTELL1PedestalsReader::getData()
{
  if (m_isDebug) debug()<< " ==> getData() " <<endmsg;
  //
  if(!exist<VeloTELL1Datas>(m_decodedPedLoc)){
    info()<< " ==> There is no decoded Pedestals at: "
           << m_decodedPedLoc <<endmsg;
  }else{  
    // get data banks from default TES location
    m_decodedPeds=get<VeloTELL1Datas>(m_decodedPedLoc);
    if (m_isDebug) info()<< " ==> The data banks have been read-in from location: "
           << m_decodedPedLoc
           << ", size of data container (number of read-out TELL1s): "
           << m_decodedPeds->size() <<endmsg;
    setPedBankFlag();
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1PedestalsReader::getAllSensors()
{
  if (m_isDebug) debug()<< " ==> getAllSensors() " <<endmsg;
  //
  // get decoded pedestals
  if(pedBankFlag()){
    // iterator for pedestal bank's container
    VeloTELL1Datas::const_iterator sensIt;
    // vector for 32 pedestals (one ALink)
    VeloTELL1::ALinkPair begEnd;
    scdatIt datIt;
    //
    for(sensIt=m_decodedPeds->begin(); sensIt!=m_decodedPeds->end(); ++sensIt){
      // one pedestal bank
      VeloTELL1Data* pedData=(*sensIt);
      if(m_plotAllPed) plotAllPed(pedData);
      if(m_plotALinks) plotALinks(pedData);
      const int TELL1No=pedData->key();
      info()<< " TELL1 number: " << TELL1No <<endmsg;
      if(m_printInfo){
        for(int ALinkCounter=0; ALinkCounter<NumberOfALinks; ++ALinkCounter){
          info()<< " ==> Decoded data from ALink [" << ALinkCounter
            << "] " <<endmsg;
          std::cout<< " > Pedestals " <<std::endl;
          //
          begEnd=(*pedData)[ALinkCounter];
          int channelCounter=0;
          for(datIt=begEnd.first; datIt!=begEnd.second; datIt+=4){
            for(int chanCounter=0; chanCounter<4; ++chanCounter){
              std::cout<< " chan[" << channelCounter << "]: ";
              if(channelCounter<10) std::cout<< " ";
              std::cout<< *(datIt+chanCounter) << "\t";
              channelCounter++;
            }
            std::cout<<std::endl;
          }
        }
      }
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloTELL1PedestalsReader::bookPedHistos(int tell1)
{
  if ( m_pedHistos.find(tell1) == m_pedHistos.end() ) {
    m_pedHistos[tell1] =
      book2D(tell1,"Decoded pedestals for TELL1 number "+boost::lexical_cast<std::string>(tell1), 0., 2048., 2048, 300., 650., 350);
  }
}

//=============================================================================
void VeloTELL1PedestalsReader::bookALinkHistos(int tell1, int aLink)
{
  if ( m_aLinkHistos.find(tell1) == m_aLinkHistos.end() ) {
    m_aLinkHistos[aLink+1000*tell1] =
      book2D( aLink+1000*tell1, "Decoded data for Sensor/ALink number "
            + boost::lexical_cast<std::string>(tell1)
            + boost::lexical_cast<std::string>("/")
            + boost::lexical_cast<std::string>(aLink),
             0., 32., 32, 350., 650., 350);
  }
}

//=============================================================================
StatusCode VeloTELL1PedestalsReader::plotAllPed(VeloTELL1Data*& inData)
{
  if (m_isDebug) debug()<< " ==> plotAll() " <<endmsg;
  //
  VeloTELL1::ALinkPair begEnd;
  scdatIt iT;
  unsigned int channel=0;
  int TELL1=inData->key();
  std::map< int, AIDA::IHistogram2D* >::iterator iH = m_pedHistos.find(TELL1);
  if (iH == m_pedHistos.end() ) {
    bookPedHistos(TELL1);
    iH = m_pedHistos.find(TELL1);
  }
  for(int aLink=0; aLink<NumberOfALinks; ++aLink){
    begEnd=(*inData)[aLink];
    for(iT=begEnd.first; iT!=begEnd.second; ++iT, ++channel){
      iH->second->fill(channel, (*iT));
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1PedestalsReader::plotALinks(VeloTELL1Data*& inData)
{
  if (m_isDebug) debug()<< " ==> plotALinks() " <<endmsg;
  //
  VeloTELL1::ALinkPair begEnd;
  scdatIt iT;
  int TELL1=inData->key();
  for(int aLink=0; aLink<NumberOfALinks; ++aLink){
    std::map< int, AIDA::IHistogram2D* >::iterator iH = m_aLinkHistos.find(aLink+1000*TELL1);
    if (iH == m_aLinkHistos.end() ) {
      bookALinkHistos(TELL1,aLink);
      iH = m_aLinkHistos.find(aLink+1000*TELL1);
    }
    unsigned int channel=0;
    begEnd=(*inData)[aLink];
    for(iT=begEnd.first; iT!=begEnd.second; ++iT, ++channel){
      iH->second->fill(channel, (*iT));
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloTELL1PedestalsReader::setPedBankFlag()
{
  m_isPedBank=true;
}
//=============================================================================
void VeloTELL1PedestalsReader::unsetPedBankFlag()
{
  m_isPedBank=false;
}
//=============================================================================
bool VeloTELL1PedestalsReader::pedBankFlag()
{
  return ( m_isPedBank );
}
//
