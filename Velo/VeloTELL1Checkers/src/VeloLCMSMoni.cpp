// $Id: VeloLCMSMoni.cpp,v 1.8 2009-12-15 16:17:44 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "AIDA/IHistogram2D.h"
#include "AIDA/IHistogram1D.h"

// local
#include "VeloLCMSMoni.h"

// kernel
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "VetraKernel/ContPrinter.h"
#include "Tell1Kernel/VeloTell1Core.h"

// boost
#include "boost/lexical_cast.hpp"
#include "boost/format.hpp"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloLCMSMoni
//
// 2007-02-09 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( VeloLCMSMoni )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloLCMSMoni::VeloLCMSMoni( const std::string& name,
                            ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator ),
    m_procInfos ( 0 ),
    m_procInfosLoc ( VeloProcessInfoLocation::Default ),
    m_LCMSADCs ( 0 ),
    m_LCMSADCsLoc ( LHCb::VeloTELL1DataLocation::CMSuppressedADCs ),
    m_eventNumber ( 0 ),
    m_isInitialized ( false ),
    m_convergenceLimit ( 0 )
{
  declareProperty("PlotLCMS", m_plotLCMS=true);
}
//=============================================================================
// Destructor
//=============================================================================
VeloLCMSMoni::~VeloLCMSMoni() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloLCMSMoni::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;
  //
  setHistoTopDir("Vetra/");
  //
  boost::format h_name_q("Proj_All_Data_LCMS");
  boost::format h_title_q("All_Data_Proj_LCMS");
  m_projAllData=book1D(h_name_q.str(), h_title_q.str(), -140, 140, 280);
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloLCMSMoni::execute() {

  m_isDebug = msgLevel( MSG::DEBUG );
  
  if (m_isDebug) debug() << "==> Execute" << endmsg;
  //
  ++m_eventNumber;
  if(!m_isInitialized){
    if(!exist<VeloProcessInfos>(m_procInfosLoc)){
      debug()<< " ==> No init info available! " <<endmsg;
      return ( StatusCode::SUCCESS );
    }else{
      m_procInfos=get<VeloProcessInfos>(m_procInfosLoc);
    }
    m_isInitialized=true;
    VeloProcessInfos::const_iterator procIt=m_procInfos->begin();
    if((*procIt)->convLimit()==0){
      if(m_isDebug) debug()<< " --> You are running with conv limit set to 0"
                           <<endmsg;
      //Error("Convergence Limit not set!", StatusCode::FAILURE);
    }else{
      m_convergenceLimit=(*procIt)->convLimit();
    }
  }
  StatusCode dataStatus, histoStatus;
  if(m_eventNumber>m_convergenceLimit){
    dataStatus=getData();
    if(dataStatus.isSuccess()){
      histoStatus=plotLCMSData();
    }else{
      return ( dataStatus );
    }
  }
  //
  return ( histoStatus );
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloLCMSMoni::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode VeloLCMSMoni::getData()
{
  if (m_isDebug) debug()<< " ==> getData()" <<endmsg;
  // get ADC samples after pedestal subtraction
  // it does not matter if one deals with real or
  // simulated data 
  if(!exist<LHCb::VeloTELL1Datas>(m_LCMSADCsLoc)){
    error()<< "There is no subtracted ADC samples in TES!" 
           << "at location: " << m_LCMSADCsLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{
    m_LCMSADCs=get<LHCb::VeloTELL1Datas>(m_LCMSADCsLoc);
  }
  //
  return StatusCode::SUCCESS;
}
//=============================================================================
void VeloLCMSMoni::bookHistograms(int tell1)
{
  if ( m_LCMSHistos.find(tell1) == m_LCMSHistos.end() ) {
    m_LCMSHistos[tell1] =
      book2D(tell1,"LCMS ADCs for TELL1 number "+boost::lexical_cast<std::string>(tell1), 0., 2304., 2304, -130., 130., 260);
  }
}
//=============================================================================
StatusCode VeloLCMSMoni::plotLCMSData()
{
  if (m_isDebug) debug()<< " plotLCMSData() " <<endmsg;
  //
  LHCb::VeloTELL1Datas::const_iterator sensIt;
  if(m_plotLCMS){
    for(sensIt=m_LCMSADCs->begin(); sensIt!=m_LCMSADCs->end(); ++sensIt){
      VeloTELL1::ALinkPair begEnd;
      VeloTELL1::scdatIt iT;
      unsigned int channel=0;
      int TELL1=(*sensIt)->key();
      std::map<int, AIDA::IHistogram2D*>::iterator iH=m_LCMSHistos.find(TELL1);
      if(iH==m_LCMSHistos.end()){
        bookHistograms(TELL1);
        iH=m_LCMSHistos.find(TELL1);
      }
      LHCb::VeloTELL1Data* adcData=(*sensIt);
      VeloTELL1::sdataVec data=adcData->data();
      if(adcData->isReordered()){  /// reorderd data -> plot strips
        iT=data.begin();
        for( ; iT!=data.end(); ++iT, ++channel){
          iH->second->fill(channel, (*iT));
          m_projAllData->fill(*iT);
        }
      }else{  /// data is not reordered plot adc samples
        //if(m_numberOfEvt>m_convergenceLimit) contPrinter(data, 15);
        for(int aLink=0; aLink<VeloTELL1::NumberOfALinks; ++aLink){
          begEnd=(*adcData)[aLink];
          for(iT=begEnd.first; iT!=begEnd.second; ++iT, ++channel){
            iH->second->fill(channel, (*iT));
            m_projAllData->fill(*iT);
          }
        }
      }
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//
