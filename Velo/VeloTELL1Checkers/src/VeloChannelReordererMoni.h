// $Id: VeloChannelReordererMoni.h,v 1.6 2009-10-10 15:39:03 szumlat Exp $
#ifndef VELOCHANNELREORDERERMONI_H 
#define VELOCHANNELREORDERERMONI_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// from Event
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"

namespace AIDA {
  class IHistogram2D;
}

/** @class VeloChannelReordererMoni VeloChannelReordererMoni.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2006-10-23
 */

class VeloChannelReordererMoni : public GaudiTupleAlg{
public: 

  enum str{
    STRIPS=2048
  };

  /// Standard constructor
  VeloChannelReordererMoni(const std::string& name, ISvcLocator* pSvcLocator);

  virtual ~VeloChannelReordererMoni( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:
  
  void bookHistograms(int tell1);
  void bookFullHistograms(int tell1);
  StatusCode getData();
  StatusCode plotReorderedData();

private:

  VeloProcessInfos* m_procInfos;
  std::string m_procInfosLoc;
  LHCb::VeloTELL1Datas* m_reordered; /// ADC samples after saturation
  std::string m_reorderedLoc;        /// the container name
  bool m_plotReordered;              /// saturated data will be plot
  unsigned int m_numberOfEvents;
  unsigned int m_convergenceLimit;
  unsigned int m_subType;            /// by default fast subtractor is used
  bool m_isInitialized;
  bool m_isDebug;
  bool m_plotAll;
  std::map< int, AIDA::IHistogram2D* > m_reorderedDataHistos;
  
  
};
#endif // VELOCHANNELREORDERERMONI_H
