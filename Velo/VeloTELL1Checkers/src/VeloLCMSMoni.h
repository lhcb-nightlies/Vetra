// $Id: VeloLCMSMoni.h,v 1.5 2009-12-15 16:17:44 szumlat Exp $
#ifndef VELOLCMSMONI_H 
#define VELOLCMSMONI_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// from TELL1 event
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"

namespace AIDA {
  class IHistogram2D;
  class IHistogram1D;
}

/** @class VeloLCMSMoni VeloLCMSMoni.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2007-02-09
 */

class VeloLCMSMoni : public GaudiTupleAlg{

public: 
  /// Standard constructor
  VeloLCMSMoni( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloLCMSMoni( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  void bookHistograms(int tell1);
  StatusCode getData();
  StatusCode plotLCMSData();

private:

  VeloProcessInfos* m_procInfos;
  std::string m_procInfosLoc;
  LHCb::VeloTELL1Datas* m_LCMSADCs;  /// ADC samples after reordering
  std::string m_LCMSADCsLoc;         /// the container name
  unsigned long m_eventNumber;      /// number of processed events
  bool m_plotLCMS;                  /// plot LCM suppressed data
  bool m_isInitialized;
  unsigned int m_convergenceLimit;
  bool m_isDebug;
  std::map< int, AIDA::IHistogram2D* > m_LCMSHistos;
  AIDA::IHistogram1D* m_projAllData;
};
#endif // VELOLCMSMONI_H
