// $Id: VeloODINBank.h,v 1.9 2007-12-10 02:46:23 szumlat Exp $
#ifndef EVENT_VELOODINBANK_H 
#define EVENT_VELOODINBANK_H 1

// Include files
#include "GaudiKernel/ContainedObject.h"
#include "GaudiKernel/ObjectVector.h"
#include "Tell1Kernel/VeloDecodeCore.h"

/** @class VeloODINBank VeloODINBank.h Event/VeloODINBank.h
 *  
 *  The VeloOdinBank class implements object to store the content
 *  of the ODIN raw bank. The bank is added during the event building.
 *  The bank contains a number of general information about the
 *  event that is used by DAQ system. One such a bank is written out
 *  along with the rest of the data per event. The class inherits
 *  from ContainedObject and has no key (one instance per event).
 *  
 *  @author Tomasz Szumlak
 *  @date   2006-07-22
 */

using namespace VeloTELL1;

namespace VeloODINBankLocation{
  static const std::string& Default="Raw/Velo/ODINBank";
}

class VeloODINBank:  public ContainedObject{

public: 

  enum masks{
    #ifndef WIN32
     longlongMask32=0x00000000ffffffffLLU,
     longlongMask64=0xffffffff00000000LLU
    #else
     longlongMask32=0x00000000ffffffffULL,
     longlongMask64=0xffffffff00000000ULL
    #endif
  };
  /// Standard constructor
  VeloODINBank( ): m_odinBody (),
                   m_odinSize ( 0 ),
                   m_odinSourceID ( 0 ),
                   m_odinVersion ( 0 ),
                   m_odinType ( 0 ) { }

  virtual ~VeloODINBank( ){ } ///< Destructor

  void setODINBody(dataVec inData);
  void setODINSize(unsigned int inValue);
  unsigned int odinSize();
  void setODINSourceID(unsigned int inValue);
  unsigned int odinSourceID();
  void setODINVersion(unsigned int inValue);
  unsigned int odinVersion();
  void setODINType(unsigned int);
  unsigned int odinType();
  unsigned int runNumber();
  unsigned int eventType();
  unsigned int orbitNumber();
  unsigned long long L0EventID();
  unsigned long long GPSTime();
  unsigned short int bunchCurrent();
  unsigned short int errorBits();
  unsigned short int detectorStatus();
  unsigned short int bunchID();
  unsigned short int BXType();
  unsigned short int forceBit();
  unsigned short int readoutType();
  unsigned short int triggerType();
  
protected:

private:

  dataVec m_odinBody;           /// ODIN bank data body
  unsigned int m_odinSize;      /// size = size of data + size of header
  unsigned int m_odinSourceID;  /// number of ODIN board (set by ECS)
  unsigned int m_odinVersion;   /// version number
  unsigned int m_odinType;      /// used by offline to recognize bank type

};
//
inline void VeloODINBank::setODINBody(dataVec inData)
{
  m_odinBody=inData;
}
//
inline void VeloODINBank::setODINSize(unsigned int inValue)
{
  m_odinSize=inValue;
}
//
inline unsigned int VeloODINBank::odinSize()
{
  return ( m_odinSize );
}
//
inline void VeloODINBank::setODINSourceID(unsigned int inValue)
{
  m_odinSourceID=inValue;
}
//
inline unsigned int VeloODINBank::odinSourceID()
{
  return ( m_odinSourceID );
}
//
inline void VeloODINBank::setODINVersion(unsigned int inValue)
{
  m_odinVersion=inValue;
}
//
inline unsigned int VeloODINBank::odinVersion()
{
  return ( m_odinVersion );
}
//
inline void VeloODINBank::setODINType(unsigned int inValue)
{
  m_odinType=inValue;
}
//
inline unsigned int VeloODINBank::odinType()
{
  return ( m_odinType );
}
//
inline unsigned int VeloODINBank::runNumber()
{
  return ( m_odinBody[0] );
}
//
inline unsigned int VeloODINBank::eventType()
{
  return ( m_odinBody[1] );
}
//
inline unsigned int VeloODINBank::orbitNumber()
{
  return ( m_odinBody[2] );
}
//
inline unsigned long long VeloODINBank::L0EventID()
{
  unsigned long long temp1=m_odinBody[3], temp2=0;
  //
  temp1=(temp1<<bitShift32)&longlongMask64;
  temp2=(~temp2)&m_odinBody[4];
  return ( temp1|temp2 );
}
//
inline unsigned long long VeloODINBank::GPSTime()
{
  unsigned long long temp1=m_odinBody[5], temp2=0;
  //
  temp1=(temp1<<bitShift32)&longlongMask64;
  temp2=(~temp2)&m_odinBody[6];
  return ( temp1|temp2 );
}
//
inline unsigned short int VeloODINBank::detectorStatus()
{
  return ( m_odinBody[7]&bitMask24 );
}
//
inline unsigned short int VeloODINBank::errorBits()
{
  return ( (m_odinBody[7]>>bitShift24)&bitMask8 );
}
//
inline unsigned short int VeloODINBank::bunchID()
{
  return ( m_odinBody[8]&bitMask12 );
}
//
inline unsigned short int VeloODINBank::triggerType()
{
  return ( (m_odinBody[8]>>bitShift16)&bitMask3 );
}
//
inline unsigned short int VeloODINBank::readoutType()
{
  return ( (m_odinBody[8]>>bitShift19)&bitMask2 );
}
//
inline unsigned short int VeloODINBank::forceBit()
{
  return ( (m_odinBody[8]>>bitShift21)&bitMask1 );
}
//
inline unsigned short int VeloODINBank::BXType()
{
  return ( (m_odinBody[8]>>bitShift22)&bitMask2 );
}
//
inline unsigned short int VeloODINBank::bunchCurrent()
{
  return ( (m_odinBody[8]>>bitShift24)&bitMask8 );
}
//
typedef ObjectVector<VeloODINBank> VeloODINBanks;
#endif // EVENT_VELOODINBANK_H
