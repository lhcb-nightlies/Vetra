#!/usr/bin/env python

###############################################################################
'''
  noiseDiff.py
  ============
  
  Python script for displaying the difference between reference data and 
  recently taken data.

  Description:
    Simply takes the difference of two noise histograms and plots
    Can be used for Raw or CMS noise
  
  Requirements:
    Needs the NoiseMon to run; located in VeloDataMonitors

  Example usage:
    noiseDiff.py foo.root refplot.root [cms]
      or in python/ROOT
    noiseDiff("foo.root", "refplot.root", 'cms')

  @author Karol hennessy
  @date   2009-03-04
'''
###############################################################################

from ROOT import *
import sys
import string

# =============================================================================
def noiseDiff(testfile, ref_file, sensor, opt):
        f = TFile(testfile)
        reff = TFile(ref_file)
        hpath = 'Vetra/Noise/SubtractedPedADCs/TELL1_'+sensor+'/RMSNoise_vs_ChipChannel'

        if string.lower(opt) == 'cms' :
                hpath = 'Vetra/Noise/ADCCMSuppressed/TELL1_'+sensor+'/RMSNoise_vs_ChipChannel'
        h = f.Get(hpath)
        href = reff.Get(hpath)
        h.Draw()
        hdiff = h.Clone()
        hdiff.SetLineColor(kBlack)
        hdiff.Add(href, -1)
        hdiff.SetMaximum(3)
        hdiff.SetMinimum(-3)
        hdiff.Draw('hist')
        raw_input()

# =============================================================================
def noiseBoth(testfile, ref_file, sensor, opt):
        f = TFile(testfile)
        reff = TFile(ref_file)
        hpath = 'Vetra/Noise/SubtractedPedADCs/TELL1_'+sensor+'/RMSNoise_vs_ChipChannel'

        if string.lower(opt) == 'cms' :
                hpath = 'Vetra/Noise/ADCCMSuppressed/TELL1_'+sensor+'/RMSNoise_vs_ChipChannel'
        h = f.Get(hpath)
        h.SetMaximum(6)
        h.Draw('hist')
        href = reff.Get(hpath)
        h.SetLineColor(kRed)
        href.SetLineColor(kBlue)
        href.Draw('hist same')
        raw_input()

# =============================================================================
def usage():
        print """
  Example usage:
    noiseDiff.py <foo.root> <refplot.root> <sensor_number> [cms]
        """

# =============================================================================
if __name__ == "__main__":
        if len( sys.argv ) == 2 and ( '--help' == sys.argv[1] or '-h' == sys.argv[1] ):
                usage()
                exit()
        
        if len(sys.argv) < 4 : 
                print "ERROR: Not enough arguments."
                usage()
                exit()

        testfile = sys.argv[1]
        ref_file = sys.argv[2]
        sensor = sys.argv[3]
        opt = ''
        if len(sys.argv) >= 5 :
                opt = sys.argv[4]

        noiseBoth(testfile, ref_file, sensor, opt)
        noiseDiff(testfile, ref_file, sensor, opt)

# =============================================================================
