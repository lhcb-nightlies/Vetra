###############################################################################
"""

 noiseVoltage.py
 ===============
 Python script for monitoring of noise vs voltage.

 Requirements:
   Needs input root file produced from output of NoiseMon.cpp.

 Example usage:
   - in python interactive mode:
     noiseVoltage('ASide')

 where ASide can be substituted with CSide

 @author Abdi Noor
 @date   2009-04-17

"""
###############################################################################

from ROOT import *
import os
import commands
import sys

gROOT.SetStyle("Plain")
gROOT.SetBatch()
gStyle.SetPalette(2)
gStyle.SetTitleOffset(1.3,"y")
gStyle.SetTitleX(0.11)
gStyle.SetTitleY(0.95)


hists=dict()
canvs=dict()
h_RMSNoiseVsSensor=dict()
c_RMSNoiseVsSensor=dict()

def ASide():
        RootFilesA=[
                "/home/anoor/Noise_V_Voltage/ASide/0V/VELODQM_40939_2008-12-17_16.28.12.root",
                "/home/anoor/Noise_V_Voltage/ASide/10V/VELODQM_40999_2008-12-17_21.15.12.root",
                "/home/anoor/Noise_V_Voltage/ASide/20V/VELODQM_41000_2008-12-17_15.23.12.root",
                "/home/anoor/Noise_V_Voltage/ASide/30V/VELODQM_41019_2008-12-17_22.32.12.root",
                "/home/anoor/Noise_V_Voltage/ASide/40V/VELODQM_41020_2008-12-17_23.11.12.root",
                "/home/anoor/Noise_V_Voltage/ASide/50V/VELODQM_41022_2008-12-18_00.29.12.root",
                "/home/anoor/Noise_V_Voltage/ASide/60V/VELODQM_41023_2008-12-18_01.08.12.root",
                "/home/anoor/Noise_V_Voltage/ASide/70V/VELODQM_41024_2008-12-18_01.47.12.root",
                "/home/anoor/Noise_V_Voltage/ASide/80V/VELODQM_41025_2008-12-18_02.25.12.root",
                "/home/anoor/Noise_V_Voltage/ASide/90V/VELODQM_41026_2008-12-18_03.04.12.root",
                "/home/anoor/Noise_V_Voltage/ASide/100V/VELODQM_41027_2008-12-18_03.43.12.root" 
                ]

        return RootFilesA

def CSide():
        RootFilesC=[
                "/home/anoor/Noise_V_Voltage/CSide/0V/VELODQM_40443_0V.root",
                "/home/anoor/Noise_V_Voltage/CSide/10V/VELODQM_40446_10V.root",
                "/home/anoor/Noise_V_Voltage/CSide/20V/VELODQM_40447_20V.root",
                "/home/anoor/Noise_V_Voltage/CSide/30V/VELODQM_40448_30V.root",
                "/home/anoor/Noise_V_Voltage/CSide/40V/VELODQM_40449_40V.root",
                "/home/anoor/Noise_V_Voltage/CSide/50V/VELODQM_40451_50V.root",
                "/home/anoor/Noise_V_Voltage/CSide/60V/VELODQM_40453_60V.root",
                "/home/anoor/Noise_V_Voltage/CSide/70V/VELODQM_40457_70V.root",
                "/home/anoor/Noise_V_Voltage/CSide/80V/VELODQM_40458_80V.root",
                "/home/anoor/Noise_V_Voltage/CSide/90V/VELODQM_40459_90V.root",
                "/home/anoor/Noise_V_Voltage/CSide/100V/VELODQM_40460_100V.root"
                ]
        
        return RootFilesC

def noiseVoltage(Side,RootFN="Nothing"):
        global c_RMSNoiseVsSensor
        global h_RMSNoiseVsSensor
        global hists
        global canvs
       
        now=commands.getoutput('date +"%m-%d-%Y-%T"')
        sen = VeloSensor()  

        CMS=1 #set CMS=0 for non-CMS data
        exclude_first_channel=1 #//set =1 to exclude first channel

        if not RootFN == 'Nothing':
                RefFile= TFile("%s.root"%(RootFN),"UPDATE")


        canvas_ASideR= TCanvas("ASide R canvas","1/Noise vs Voltage for R",1)
        canvas_ASidePhi= TCanvas("ASide phi canvas","1/Noise vs Voltage for Phi",1)
        leg_ASideR=  TLegend(0.67,0.67,0.99,0.99)
        leg_ASidePhi=  TLegend(0.67,0.67,0.99,0.99)
        
        canvas_CSideR= TCanvas("CSide R canvas","1/Noise vs Voltage for R",1)
        canvas_CSidePhi= TCanvas("CSide phi canvas","1/Noise vs Voltage for Phi",1)
        leg_CSideR=  TLegend(0.67,0.67,0.99,0.99)
        leg_CSidePhi=  TLegend(0.67,0.67,0.99,0.99)
        
        if Side == 'ASide':
                #print '/n works'
                os.mkdir("ASide-%s"%(now))
                RootFiles=ASide()
        
        if Side == 'CSide':
                #print 'wrong'
                os.mkdir("CSide-%s"%(now))
                RootFiles=CSide()
                
        for File in RootFiles:
                
                print File
                f= TFile(File)

                fileName = File.split('/')[-1].strip('.root')
               

                if not File in h_RMSNoiseVsSensor:
 
                        canvas="RootFile_%s"%(fileName)
                        histo= "histo_%s"%(fileName)
                
                        c_RMSNoiseVsSensor[File]= TCanvas(canvas,"RMS Noise vs Sensor",1)
                        c_RMSNoiseVsSensor[File].cd()
                        h_RMSNoiseVsSensor[File]= TH1F(histo,"Average RMS Noise per Sensor",110,-0.5,109.5)


                for tell in range(0,110):
                         

                        if sen.initializeSensor(f,tell):
                                i= RootFiles.index(File)
                                bin = i+1 
                                length=len(RootFiles)
                                final=RootFiles[-1]
                                #print "Num files", length
                                #print "File I am in",RootFiles
                                #print "Last file", final

                                if not tell in hists  :
                                        
                                         
                                        canvasName="TELL1_%03d"%(tell)
                                        histoName="Sensor_%03d"%(tell)


                                        if Side == 'CSide':
                                                canvs[tell]=TCanvas(canvasName,"1/Noise as Function of Bias Voltage",1)
                                                canvs[tell].cd()
                                                canvs[tell].SetTopMargin(0.2)
                                                hists[tell]=TH1F(histoName,"#frac{1}{Noise} vs Bias Voltage",11,-5,105)
                                                hists[tell].GetYaxis().SetRangeUser(0,1)
                                                        
                                                hists[tell].SetMarkerStyle(tell%20)
                                                hists[tell].SetMarkerColor(tell%30)
                                                hists[tell].SetLineColor(tell%30)
                                                               
                                                hists[tell].GetXaxis().SetTitle("Voltage (V)")
                                                hists[tell].GetYaxis().SetTitle("#frac{1}{Noise} #left(#frac{1}{ADC Counts}#right)")
                                        
                                        if Side == 'ASide':
                                                canvs[tell]=TCanvas(canvasName,"1/Noise as Function of Bais Voltage",1)
                                                canvs[tell].cd()
                                                canvs[tell].SetTopMargin(0.2)
                                                hists[tell]=TH1F(histoName,"#frac{1}{Noise} vs Bias Voltage",11,-5,105)
                                                hists[tell].GetYaxis().SetRangeUser(0,1)
                                                hists[tell].SetMarkerStyle((tell+1)%20)
                                                hists[tell].SetMarkerColor((tell+1)%30)
                                                hists[tell].SetLineColor((tell+1)%30)
                                                hists[tell].GetXaxis().SetTitle("Voltage (V)")
                                                hists[tell].GetYaxis().SetTitle("#frac{1}{Noise} #left(#frac{1}{ADC Counts}#right)")
                                       
                               
                                
                                sensor_noise=sen.getRMSNoiseSensor(CMS, exclude_first_channel)
                                h_RMSNoiseVsSensor[File].Fill(tell,sensor_noise)
                                                
                                sensor_signaltonoise=1.0/sensor_noise

                                hists[tell].SetBinContent(bin,sensor_signaltonoise)
                                canvs[tell].cd()
                                hists[tell].Draw("PL")
                                print "Dir I am in first is....",gDirectory.GetName()
                                #if yes == 'yes':
                                        #print "YES!!!!!!!!!!"
                                if File == final and not RootFN == "Nothing":
                                        RefFile.cd()
                                        gDirectory.pwd()
                                        hists[tell].Write()
                                        print "Dir I am in now is....",gDirectory.GetName()

                                if Side == 'ASide':
                                        canvs[tell].Print("ASide-%s/TELL_%03d.ps"%(now,tell))
                                        canvs[tell].Print("ASide-%s/TELL_%03d.png"%(now,tell))
                                        #canvs[tell].Write()
                                
                                if Side == 'CSide':
                                        canvs[tell].Print("CSide-%s/TELL_%03d.ps"%(now,tell))
                                        canvs[tell].Print("CSide-%s/TELL_%03d.png"%(now,tell))
                                
                                if Side == 'ASide': 
                                        
                                        if tell in range(0,45):
                                                canvas_ASideR.cd()
                                                
                                                canvas_ASideR.SetTopMargin(0.2)
                                                #c_R.SetLeftMargin(0.2)

                                                hists[tell].Draw("PL,same")
                                                hists[tell].SetStats(kFALSE)
                                                
                                                if i ==0:
                                                        leg_ASideR.AddEntry(hists[tell],histoName)
                                                        leg_ASideR.SetHeader("A Side R Sensors ")
                                                        leg_ASideR.SetNColumns(2)
                                                        leg_ASideR.SetFillColor(10)
                                                        leg_ASideR.Draw()

                                                        
                                                
                                                
                                        if tell in range(60,110):
                                        ##else: 
                                                canvas_ASidePhi.cd()
                                                canvas_ASidePhi.SetTopMargin(0.2)
                                                                
                                                hists[tell].Draw("PL,same")
                                                hists[tell].SetStats(kFALSE)
                                                
                                                if i ==0:
                                                        leg_ASidePhi.AddEntry(hists[tell],histoName)
                                                        leg_ASidePhi.SetHeader("A Side Phi Sensors")
                                                        leg_ASidePhi.SetNColumns(2)
                                                        leg_ASidePhi.SetFillColor(10)
                                                        leg_ASidePhi.Draw()

                                        canvas_ASideR.Update()
                                        canvas_ASideR.Print("ASide-%s/ASideR.ps"%(now))
                                        canvas_ASideR.Print("ASide-%s/ASideR.png"%(now))
                                        canvas_ASideR.Print("ASide-%s/ASideR.C"%(now))

                                        
                                        canvas_ASidePhi.Update()
                                        canvas_ASidePhi.Print("ASide-%s/ASidePhi.ps"%(now))
                                        canvas_ASidePhi.Print("ASide-%s/ASidePhi.C"%(now))
                          
                                if Side == 'CSide': 
                                        
                                        if tell in range(0,45):
                                                canvas_CSideR.cd()
                                                
                                                canvas_CSideR.SetTopMargin(0.2)
                                                #c_R.SetLeftMargin(0.2)

                                                hists[tell].Draw("PL,same")
                                                hists[tell].SetStats(kFALSE)
                                                
                                                if i ==0:
                                                        leg_CSideR.AddEntry(hists[tell],histoName)
                                                        leg_CSideR.SetHeader("C Side R Sensors ")
                                                        leg_CSideR.SetNColumns(2)
                                                        leg_CSideR.SetFillColor(10)
                                                        leg_CSideR.Draw()

                                                        
                                                
                                                
                                        if tell in range(60,110):
                                        ##else: 
                                                canvas_CSidePhi.cd()
                                                canvas_CSidePhi.SetTopMargin(0.2)
                                                                
                                                hists[tell].Draw("PL,same")
                                                hists[tell].SetStats(kFALSE)
                                                
                                                if i ==0:
                                                        leg_CSidePhi.AddEntry(hists[tell],histoName)
                                                        leg_CSidePhi.SetHeader("C Side Phi Sensors")
                                                        leg_CSidePhi.SetNColumns(2)
                                                        leg_CSidePhi.SetFillColor(10)
                                                        leg_CSidePhi.Draw()
                                
                                        canvas_CSideR.Update()
                                        canvas_CSideR.Print("CSide-%s/CSideR.ps"%(now))
                                        canvas_CSideR.Print("CSide-%s/CSideR.C"%(now))

                                        
                                        canvas_CSidePhi.Update()
                                        canvas_CSidePhi.Print("CSide-%s/CSidePhi.ps"%(now))
                                        canvas_CSidePhi.Print("CSide-%s/CSidePhi.C"%(now))
                               

                            
                c_RMSNoiseVsSensor[File].cd()
                h_RMSNoiseVsSensor[File].GetXaxis().SetRangeUser(0,110)
                h_RMSNoiseVsSensor[File].GetXaxis().SetTitle("Sensor Number")
                h_RMSNoiseVsSensor[File].GetYaxis().SetTitle("Noise (ADC Counts)")
                c_RMSNoiseVsSensor[File].Update() 
                h_RMSNoiseVsSensor[File].Draw()
                if Side=='ASide':
                        c_RMSNoiseVsSensor[File].Print("ASide-%s/RootFile_%s.ps"%(now,fileName))
                
                if Side=='CSide':
                        c_RMSNoiseVsSensor[File].Print("CSide-%s/RootFile_%s.ps"%(now,fileName))

                #raw_input("return to quit")
                                 

                
                
def both(RootFileName):

        noiseVoltage('ASide',RootFileName)
        noiseVoltage('CSide',RootFileName)



def noiseRatio(TestFile, RefFile,sensor):
        f = TFile(TestFile)
        reff = TFile(RefFile)
        
        

        c1= TCanvas("c1","Noise/Noise_Reff vs Voltage",1)
        
        c1.Divide(1,2)  
        c1.cd(1).Divide(2,1) 
        
        h = f.Get(sensor)
        href = reff.Get(sensor)
        
        c1.cd(1).cd(1)
        href.SetTitle("#frac{1}{Noise} vs Bias Voltage REFERENCE")
        href.Draw('PL')
        
        c1.cd(1).cd(2)
        h.SetTitle("#frac{1}{Noise} vs Bias Voltage TEST")
        h.Draw('PL')
       
        hdiff = h.Clone()
        hdiff.SetLineColor(kRed)
        hdiff.Divide(href)
        hdiff.SetMaximum(2)
        hdiff.SetTitle("#frac{Noise}{Noise_{Reff}} vs Bias Voltage for %s"%(sensor))
        hdiff.GetXaxis().SetTitle("Voltage (V)")
        hdiff.GetYaxis().SetTitle("#frac{Noise}{Noise_{Reff}}")
        c1.cd(2)
        hdiff.Draw('PL')

        psTitle = "noiseRatio_%s"%(sensor)
        savefile = raw_input("Save histograms to file %s.ps? (y/n): "%psTitle )

                   # print psTitle
        if savefile in ('y', 'Y'):
               c1.Print("%s.ps"%(psTitle))

       #Anything else quits:
        else:
                       print 'Histograms NOT saved'
                                               
