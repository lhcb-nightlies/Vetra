
# ==============================================================================
# addIPMoni.py.
# Author: Michael Alexander.
# A script to be appended to the default Vetra monitoring options to add
# IP resolutions monitoring.
# Should be run at the pit with:
# vetraOffline -u $VETRASCRIPTSROOT/python/addIPMoni.py
# ==============================================================================

from Gaudi.Configuration import *
from Configurables import Velo__VeloIPResolutionMonitor, GaudiSequencer,\
     VetraRecoConf, RecSysConf, TrackSys

VetraRecoConf().Sequence = RecSysConf().getProp('DefaultTrackingSubdets')
VetraRecoConf().TrackPatRecAlgorithms = TrackSys().getProp('DefaultPatRecAlgorithms')


def addIPMoni() :
    ipMoni = Velo__VeloIPResolutionMonitor("VeloIPResolutionMonitor")
    ipMoni.NBins = 10
    ipMoni.NBinsPhi = 16
    ipMoni.NBinsEta = 15
    GaudiSequencer( 'Moni_ZS' ).Members += [ ipMoni ]
    # Remove VELO decoding as this is done earlier in Vetra.
    GaudiSequencer( 'RecoDecodingSeq' ).Members.pop(0)
    
appendPostConfigAction(addIPMoni)
    
