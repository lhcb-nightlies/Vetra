from ROOT import *
import math
gROOT.SetStyle("Plain")
gStyle.SetStatW(0.2)
gStyle.SetStatH(0.2)
gStyle.SetOptTitle(1)
gStyle.SetOptStat(1111)
gStyle.SetOptFit(1111)

class noisy_channel:
    def _init_(self,channel,sensor,adc):
        self.channel=channel
        self.sensor=sensor
        self.adc=adc
    def setNoise(self,channel,sensor,adc):
        self.channel=channel
        self.sensor=sensor
        self.adc=adc
    def getADC(self):
        return self.adc
    def getChannel(self):
        return self.channel
    def getSensor(self):
        return self.sensor
#filename = "/calib/velo/dqm/70192/VELODQM_70192_2010-04-16_09.38.04_NZS.root"
can = TCanvas()
h1 = TH1D("h1","Noise per link",1000,0.,20.)
test = TH1D("h2","Noisy Sensors",120,0.,120.)
test1 = TH1D("h3","Noisy Sensors",120,0.,120.)
test2 = TH1D("h4","Noisy Sensors",120,0.,120.)
test3 = TH1D("h5","Noisy Sensors",120,0.,120.)
noise_plot = TH1D("noise_plot","Noise plots",300,0,127)
ref_noise_plot = TH1D("noise_plot","Noise plots",300,0,127)
phi_strip = TH1D("phi_strip","Nosiy Phi Strip",2048,-0.5,2047.5)
r_strip = TH1D("r_strip","Nosiy R Strip",2048,-0.5,2047.5)

test_diff = TH1D("test_diff","Noisy Sensors",120,0.,120.)
#test_diff = TH1D("test_diff","Noisy Sensors",120,0.,120.)
noise_diff = TH1D("noise_diff","ABS diff in noise",200,0.,40.)
dir = "/calib/velo/dqm/"
filename = [dir+"70347/VELODQM_70347_2010-04-18_13.51.04_NZS.root"
            ,dir+"70192/VELODQM_70192_2010-04-16_09.38.04_NZS.root"
            ,dir+"70288/VELODQM_70288_2010-04-17_17.34.04_NZS.root"
            ,dir+"70278/VELODQM_70278_2010-04-18_14.31.04_NZS.root"
            ,dir+"70280/VELODQM_70280_2010-04-17_13.52.04_NZS.root"]
ref_filename = "/calib/velo/dqm/REF_PLOTS/Reference_Noise_NZS_70260_20100416.root"
diff_counter = 0
counter = 0

counter1 = 0
counter2 = 0
counter3 = 0
ref_file = TFile.Open(ref_filename)
for file in range(filename.__len__()):
    NoiseFile = TFile.Open(filename[file])
    print filename
    ##NoiseFile = TFile.Open("Vetra_histos.TED.09.root")
    noise = []
    tell1 = []
    info  = []
    info2 = []
    link_noise_ch = []
    #noise lists with [tell_no][strip(tuple numbering)]
    three_flag = 0
    link_ch_mean = 0.
    for tell in range(110):
#        name = "Vetra/NoiseMon/ADCCMSuppressed/TELL1_%03d/RMSNoise_vs_Strip"%(tell)
        name = "Vetra/NoiseMon/ADCCMSuppressed/TELL1_%03d/RMSNoise_vs_ChipChannel"%(tell)
        #    name = "Vetra/NoiseMon/SubtractedPedADCs/TELL1_%03d/RMSNoise_vs_Strip"%(tell)
        #    name = "Vetra/NoiseMon/FIRCorrected/TELL1_%03d/RMSNoise_vs_Strip"%(tell)
        #    name = "Vetra/NoiseMon/ADCReordered/TELL1_%03d/RMSNoise_vs_Strip"%(tell)
        #    name = "Vetra/NoiseMon/DecodedADC/TELL1_%03d/RMSNoise_vs_Strip"%(tell)
        n1 = NoiseFile.Get(name)
        n2 = ref_file.Get(name)
        if n1:
            three_flag = 0
            sen_counter = 0
            sen_counter1 = 0
            sen_counter2 = 0
            sen_counter3 = 0
            link_counter = 0
            for n in range(1,2048):
                if n is 1:
                    print "Sensor: ",tell
                noise.append(n1.GetBinContent(n))
                limit = 10
                noise_plot.Fill(n1.GetBinContent(n))
                ref_noise_plot.Fill(n2.GetBinContent(n))
                
##                 if tell ==8 and n >400 and n<500:
##                     print "adc value: ", n1.GetBinContent(n)
##                     print "n0: ",noise[n-1], "n1: ",noise[n-2],"n2: ",noise[n-3]
                if abs(n1.GetBinContent(n)-n2.GetBinContent(n))>10:
                    test_diff.AddBinContent(tell)
                    diff_counter= diff_counter+1
                    if tell>60:
                        phi_strip.AddBinContent(n)
                    else:
                        r_strip.AddBinContent(n)
                if n > 3:

                    if n1.GetBinContent(n)>limit and noise[n-2] >limit and noise[n-3]>limit:
                        three_flag = 1
                    if n1.GetBinContent(n)>10:
                        # print "    channel: ", n-1, " RMS Noise: ",n1.GetBinContent(n)
                        noisy_channel_info  = noisy_channel()
                        noisy_channel_info.setNoise(n-1,tell,n1.GetBinContent(n))
                        # print noisy_channel_info
                        info.append(noisy_channel_info)
                        counter = counter+1
                        test.AddBinContent(tell)
                        sen_counter = sen_counter+1
                        link_counter = link_counter+1
                        # check for noisy link
                    link_ch_mean = link_ch_mean + n1.GetBinContent(n)
                    if three_flag == 1:
                        print "Sensor test 3: ",tell,"hot link: ",n/32, "Channel: ",n
                        three_flag = 0
                        counter3 = counter3+1
                        test3.AddBinContent(tell)
                        sen_counter2 = sen_counter2+1
##                         n1.Draw("hist")
##                         can.Update()
                        
                    if not (n%32):
                        link_ch_mean = link_ch_mean/32
#                        print link_ch_mean
                        h1.Fill(link_ch_mean)
                        link_noise_ch.append(link_ch_mean)
                        if (link_ch_mean)> 6:
                            print "Sensor: ",tell,"hot link: ",n/32, "Channel: ",n
##                             n1.Draw("hist")
##                             can.Update()
                            counter1 = counter1+1
                            sen_counter1 = sen_counter1+1
                            test1.AddBinContent(tell)
                        if link_counter >4:
                            print "Sensor test 2: ",tell,"hot link: ",n/32, "Channel: ",n
                            counter2 = counter2+1
                            sen_counter2 = sen_counter2+1
                            test2.AddBinContent(tell)

##                             n1.Draw("hist")
##                             can.Update()


                        link_ch_mean = 0.
                        link_counter = 0
            
#            raw_input()
            tell1.append(noise)
            noise.__init__()
        else:
            noise = [0.0]
            tell1.append(noise)
    
print "diff channels over 10: ", diff_counter
print "total channels over 10: ",counter
print "total channels noisey links over 10: ",counter1
print "total channels noisy links 5 over 10: ",counter2
print "total channels 3 con over 15: ",counter3
#NoiseFile.Close()        
