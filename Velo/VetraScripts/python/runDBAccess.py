# =============================================================================
"""
 runDBAccess.py

 python module to access run database

 @author Marco Gersabeck
 @date   09/03/2012
"""
# =============================================================================

import urllib2

# =============================================================================
def getFillDict( fill ):
  _root_url = 'http://lbrundb.cern.ch/api/fill/'
  url = _root_url+str(fill)
  try:
    res = urllib2.urlopen(url).read()
  except urllib2.HTTPError:
    return {}
  ress = eval(res)
  return ress

# =============================================================================
def getFillInfoValue( fill, infoItem ):
  rdict = getRunDict( fill )
  if rdict.has_key( infoItem ):
    return rdict[ infoItem ]
  else:
    return 'N/A'

# =============================================================================
def getTotalLumi( fill ):
  return getFillInfoValue( fill, 'lumi_total' )

# =============================================================================
def getVeloInLumi( fill ):
  return getFillInfoValue( fill, 'lumi_veloin' )

# =============================================================================
def getPrevFillID( fill ):
  return getFillInfoValue( fill, 'prev_fill_id' )

# =============================================================================
def getRunDict( run ):
  _root_url = 'http://lbrundb.cern.ch/api/run/'
  url = _root_url+str(run)
  try:
    res = urllib2.urlopen(url).read()
  except urllib2.HTTPError:
    return {}
  ress = eval(res)
  return ress

# =============================================================================
def getRunInfoValue( run, infoItem ):
  rdict = getRunDict( run )
  if rdict.has_key( infoItem ):
    return rdict[ infoItem ]
  else:
    return 'N/A'

# =============================================================================
def getMagnetState( run ):
  return getRunInfoValue( run, 'magnetState' )

# =============================================================================
def getMagnetStateFromFname( fname_full ):
  print fname_full
  fname = fname_full.split('/')[-1]
  run = int(fname.split('_')[1])
  mag = getRunInfoValue( run, 'magnetState' )
  retval = 0
  if 'UP' == mag: retval = 1
  elif 'DOWN' == mag: retval = -1
  return retval

# =============================================================================
if __name__ == "__main__":
  pass
