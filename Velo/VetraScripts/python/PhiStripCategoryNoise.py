#!/usr/bin/env python
"""Produce summary noise plots for each strip Category.

This Exports:

  - PhiStripCategoryNoise  Produces summary plots 
    The input is a ROOT file containing noise plots as
    produced by the standard Vetra NZS analysis.

Author: Kurt Rinnert <kurt.rinnert@cern.ch>
Date:   2009-09-02
Revision: $Id: PhiStripCategoryNoise.py,v 1.5 2009-09-03 13:43:19 krinnert Exp $

"""
from ROOT import *
from PhiStripMap import PhiStripMap

class PhiStripCategoryNoise:

  def __init__( self, rootFileName, cmsNoiseDir='/Vetra/NoiseMon/ADCCMSuppressed' ):
    """Initialize summary plot facility.

    Open ROOT file and look for noise plots in given directory.
    Get an instance of strip category map.

    """
    gROOT.SetStyle("Plain")
    self.stripMap = PhiStripMap()
    self.rootFile = TFile(rootFileName, "READ")
    self.rootFile.cd(cmsNoiseDir)


    self.hists = [ TH1F("hCat0","Noise, Phi Cat. 0",100,1,3)
        , TH1F("hCat1","Noise, Phi Cat. 1",100,1,3)
        , TH1F("hCat2","Noise, Phi Cat. 2",100,1,3) ]

    for i in range(3):
      self.hists[i].SetLineWidth(2)
      self.hists[i].SetLineColor(kBlue)
      self.hists[i].SetFillColor(kBlue)
      self.hists[i].SetFillStyle(3001)

    self.canvases = []

  def fillHistograms( self ):
    """Fill summary histograms.

    """
    histName = "RMSNoise_vs_ChipChannel"

    for sensor in range(64,106):
      subDir = "TELL1_%03d" % sensor
      h = gDirectory.Get(subDir + "/" + histName)
     
      if h:
        for channel in range(0,2048):
          noise = h.GetBinContent(channel+1)
          cat = self.stripMap.stripCategory(channel)
          self.hists[cat].Fill(noise)
      else:
        print "Noise histograms for sensor %d not available." % sensor

  def drawHistograms( self, eps=False, png=False ):
    """Draw summary histograms.

    """
    for cat in range(3):
      canvasName = "cat%d" % cat
      canvasTitle = "Phi Category %d" % cat
      self.canvases.append(TCanvas(canvasName,canvasTitle,600,600))
      self.canvases[cat].cd()
      self.hists[cat].Draw()

      if eps:
        self.canvases[cat].SaveAs("phi_cat_%d.eps" % cat)
      if png:
        self.canvases[cat].SaveAs("phi_cat_%d.png" % cat)

if __name__ ==  "__main__":
  from optparse import OptionParser
  parser = OptionParser()
  parser.add_option("-f", "--infile", dest="infilename", 
      help="read histograms from FILE", metavar="FILE")
  parser.add_option("-d", "--histdir", dest="histdir", 
      help="read noise histograms in DIR", metavar="DIR", 
      default="/Vetra/NoiseMon/ADCCMSuppressed")
  parser.add_option("-e", "--eps", action="store_true", dest="writeeps", help="write eps files", default=False)
  parser.add_option("-p", "--png", action="store_true", dest="writepng", help="write png files", default=False)
  (options, args) = parser.parse_args()


  plotter = PhiStripCategoryNoise(options.infilename, options.histdir)
  plotter.fillHistograms()
  plotter.drawHistograms(options.writeeps, options.writepng)

