import json
import urllib
import time

# String constants describing magnet polarity
UP = 'up'
DOWN = 'down'
OFF = 'off'
# Enumerate all possible polarity states
POLARITIES = (UP, DOWN, OFF)

# Template URL for retrieving response from LHCb RunDB JSON API
# Template parameter {0}: Search string, e.g. partitionname=LHCb
RUNDB_API_RUN_URL = 'http://lbrundb.cern.ch/api/run/{0}'
RUNDB_API_SEARCH_URL = 'http://lbrundb.cern.ch/api/search/?{0}'


class RunDB(object):
    """Query the LHCb Run Database, RunDB, for run information.

    Uses the RunDB JSON API to retrieve information.  For information on the
    RunDB JSON API and the format of the response, see
    https://lbdokuwiki.cern.ch/rundb:rundb_web_interface_api.

    Example usage:
    from rundb import RunDB
    query = RunDB()
    output = query.query_search(167666)
    runs = query.filter_runs(output['runs'],1800)
    """
    def __init__(self, url_run=RUNDB_API_RUN_URL, url_search=RUNDB_API_SEARCH_URL):
        """Initialise the object with template URL.

        url_run and url_search arguments must have a single template argument,
        which is the search string, e.g.

            >>> RunDB(url_run='http://example.com/search/?{0}')

        If any error occurs during any query, such as the RunDB being
        uncontactable or an invalid search string is passed,
        None is returned.
        """
        self._url_run = url_run
        self._url_search = url_search

    def query_run(self, run):
        """Return dictionary of run information.

        If there is any failure, such as the RunDB being uncontactable, or the
        search string is invalid, None is returned.
        """
        request = urllib.urlopen(self._url_run.format(run))
        # Make sure the request was OK, not e.g. 404 not found
        if request.getcode() != 200:
            return None
        try:
            d = json.load(request)
        except ValueError:
            # Couldn't decode the response
            return None
        return d

    def query_search(self, run):
        """Return dictionary of run information.

        If there is any failure, such as the RunDB being uncontactable, or the
        search string is invalid, None is returned.
        """
        searchbase = 'partition=LHCb&rows=-1'
        searchstart = '&starttime='
        searchend = '&endtime='
        searchstate = '&state=IN%20BKK|ENDED|DEFERRED'
        searchdestination = '&destination=CASTOR|OFFLINE'

        response = self.query_run(run)
        if not response:
            return None
        try:
            starttime = response['endtime']
            starttime = time.strftime('%Y-%m-%dT%H:%M:%S',time.localtime(time.mktime(time.strptime(response['endtime'],'%Y-%m-%dT%H:%M:%S'))+900)) # add 15 minutes to endtime
        except KeyError:
            # Polarity not included in JSON response
            return None
        while starttime is None:
            run += 1
            response = self.query_run(run)
            starttime = time.strftime('%Y-%m-%dT%H:%M:%S',time.localtime(time.mktime(time.strptime(response['endtime'],'%Y-%m-%dT%H:%M:%S'))+900)) # add 15 minutes to endtime
        
        endtime = time.strftime('%Y-%m-%dT%H:%M:%S')

        search = searchbase+searchstart+starttime+searchend+endtime+searchstate+searchdestination

        request = urllib.urlopen(self._url_search.format(search))
        # Make sure the request was OK, not e.g. 404 not found
        if request.getcode() != 200:
            return None
        try:
            d = json.load(request)
        except ValueError:
            # Couldn't decode the response
            return None
        return d

    def filter_runs(self, results, threshold):
        """ Check destination, run duration
        """
        pattern = '%Y-%m-%dT%H:%M:%S'
        runs = []
        for run in results:
            stime = long(time.mktime(time.strptime(run['starttime'], pattern)))
            etime = long(time.mktime(time.strptime(run['endtime'], pattern)))
            if etime - stime < threshold: continue
            runs.append(run['runid'])
        return runs

    def polarity(self, run):
        """Return the polarity of the run, one of UP, DOWN, or OFF constants.

        Example polarity check:

            >>> from veloview.utils import rundb
            >>> polarity = rundb.RunDB().run_polarity(149715)
            >>> if polartiy == rundb.UP:
            ...     # Do something
            ... else polarity == rundb.DOWN:
            ...     # Do something else

        If the RunDB query fails, None is returned.
        """
        response = self.query_run(run)
        # Check for null responses
        if not response:
            return None
        try:
            polarity = response['magnetState']
        except KeyError:
            # Polarity not included in JSON response
            return None
        # API returns UP for magnet up, DOWN for magnet down, OFF for off
        # Map these to our string constants and return the appropriate one
        return {
            'UP': UP,
            'DOWN': DOWN,
            'OFF': OFF
        }[polarity]
