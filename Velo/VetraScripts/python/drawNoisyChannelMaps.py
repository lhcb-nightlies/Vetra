#! /usr/bin/env python
###############################################################################
"""
  drawNoisyChannelMaps
  ===================
   Scans over Noise data for all TELL1s and produces summary output
   of bad strips in each TELL1

   Produces plots of
   1. number of bad channels per sensor
   2. 2D plot of bad channels per sensor and analogue link

   Requirements:
   - needs input root file
     from output of Velo/VeloDataMonitor/BadChannelMon.cpp
     this produces a set of histgrams with bad strips flagged
     
   Example usage:
   - in python interactive
     python -i drawNoisyChannelMaps.py
     drawNoisyChannelMaps(filename.root)

   - to draw plots for a specific sensor
     python -i drawNoisyChannelMaps.py
     drawNoisyChannelsPerSensor(filename.root,x)
     where x is the sensor number
       
   - in VeloMoniGUI
     use BadChannels tab

 @author James Keaveney
 @date  2009-11-20    
 @author Sara Traynor
 @date   2009-03-17

"""

from ROOT import *
import math
from string import *
import os
import glob
from sys import argv


global badChanHisto
global badLinkHisto
global noisyChanHisto
global noisyLinkHisto
global deadChanHisto



global deadLinkHisto
global c1
global c2
global badChannelPerSensorHisto
global RawNoisePerSensorHisto
global noisyChannelPerSensorHisto
global RawNoisePerSensorHisto
global cmNoisePerSensorHisto


badChannelPerSensorHisto=TH1F("badChannelPerSensorHisto","bad channels in sensor",2048,0,2048)
noisyChannelPerSensorHisto=TH1F("noisyChannelPerSensorHisto","Noisy channels in sensor",2048,0,2048)
deadChannelPerSensorHisto=TH1F("deadChannelPerSensorHisto","Dead channels in sensor",2048,0,2048)
RawNoisePerSensorHisto=TH1F("RawNoisePerSensorHisto","Raw Noise in sensor",2048,0,2048)
cmNoisePerSensorHisto=TH1F("cmNoisePerSensorHisto","CMC Noise in sensor",2048,0,2048)


badChanHisto=TH1F("badChanHisto","Number of Bad Channels",105,0,105)
badLinkHisto=TH2F("badLinkHisto","2D Bad Link Map",106,0,106,64,0,64)

noisyChanHisto=TH1F("noisyChanHisto","Number of Noisy Channels",105,0,105)
noisyLinkHisto=TH2F("noisyLinkHisto","2D Noisy Link Map",106,0,106,64,0,64)

deadChanHisto=TH1F("deadChanHisto","Number of Dead Channels",105,0,105)
deadLinkHisto=TH2F("deadLinkHisto","2D Dead Link Map",106,0,106,64,0,64)

deadChanPerSensorHistoList = []

# file where to look for histograms
MONI_FILENAME = 'Vetra_histos.root'
HVOFF_FILENAME =  'Vetra_histos.root'

# directory where to look for histograms
#MONI_DIR = '/Vetra/BadChannel/BadStrips'
MONI_DIR = '/Vetra/BadChannelMon/BadChannels'

###############################################################################
def drawNoisyChannelMaps(FileName):
    mainDrawNoisyChannelMaps(FileName)
    # draw plots
    c1=drawPlotsMakeCanvas()  
    return (c1)

def mainDrawNoisyChannelMaps(FileName, HVOffFileName):

# open root file
   file=TFile(FileName)
   if file.IsOpen() == 0:
       print "Error could not open file " + FileName
       return

# open HV off root file
   fileHVOff=TFile(HVOffFileName)
   if fileHVOff.IsOpen() == 0:
       print "Error could not open file " + HVOffFileName
       return


   # Auto detect the total number of sensor histograms from the directory structure:
   dir = file.Get(MONI_DIR)
   if dir:
       nentries = (dir.GetListOfKeys()).GetEntries()
       print "got dir"
   else:
       print "No Bad channels histograms present in this root file."
       return

   dirHVOff = fileHVOff.Get(MONI_DIR)
   if dirHVOff:
       nentriesHVOff = (dirHVOff.GetListOfKeys()).GetEntries()
       print "got dir in HV off file"
   else:
       print "No Bad channels histograms present in the HV off root file."
       return
      
    #loop over histograms
   for nHisto in range (0, nentries):
        
        # get histograms
        nSensor,histoStrips,histoNoisyStrips,histoRawNoiseD,histoCMCNoiseD,histoRawNoise,histoCMCNoise = getHistogram(file, nHisto)
        nSensorH,histoStripsH,histoNoisyStripsH,histoRawNoiseDH,histoCMCNoiseDH,histoRawNoiseH,histoCMCNoiseH = getHistogram(fileHVOff, nHisto)

        for bin in range (0, 2048):
            if((bin-1) % 32) ==0:
                continue
            else:
                rawNoiseOn = histoRawNoise.GetBinContent(bin)
                rawNoiseOff = histoRawNoiseH.GetBinContent(bin)
                rawNoiseDiff = rawNoiseOff - rawNoiseOn
                linkdiv = bin/32
                linkNo = int(bin/32)
                if abs (rawNoiseDiff) < .07:
                    print linkNo
                    deadChanHisto.Fill(nSensor, 1)
                    deadLinkHisto.Fill(nSensor, linkdiv, 1)
                           
        # calculate number bad Strips per Sensor
        nBadStrips=histoStrips.GetEntries()
        nNoisyStrips = histoNoisyStrips.GetEntries()
        
        # fill 1D plot (add 1 for root ideosyncracy)
        badChanHisto.SetBinContent(nSensor+1,nBadStrips)
        noisyChanHisto.SetBinContent(nSensor+1,nNoisyStrips)   

        # loop over analogue links
        for iLink in range (0,63):
            # calculate number bad strips per analogue link
            nBadStripsLink=calculateBadStripsOnLink(iLink,histoStrips)
            nNoisyStripsLink=calculateBadStripsOnLink(iLink,histoNoisyStrips)
            nDeadStripsLink=calculateBadStripsOnLink(iLink,deadChanHisto)
 
            # fill 2D plot
            badLinkHisto.SetBinContent(nSensor+1,iLink+1,nBadStripsLink)
            noisyLinkHisto.SetBinContent(nSensor+1,iLink+1,nNoisyStripsLink)


   return

def drawPlotsMakeCanvas():

    c1=TCanvas("c1","Number of Bad strips by sensor Number",0,0,800,600)
  
    c1.Divide(1,2)
    #1D histo
    badChanHisto.GetXaxis().SetLabelSize(0.045)
    badChanHisto.GetXaxis().SetTitle("Sensor Number")
    badChanHisto.GetYaxis().SetLabelSize(0.045)
    badChanHisto.GetYaxis().SetTitle("number of bad channels")
    c1.cd(1)
    badChanHisto.Draw()

    #2D histo
    c1.cd(2)
    badLinkHisto.GetXaxis().SetTitle("Sensor  Number")
    badLinkHisto.GetYaxis().SetTitle("Analogue  Link")
    badLinkHisto.SetMinimum(0.);
    badLinkHisto.SetMaximum(32.);
    badLinkHisto.Draw('colz')

#    c1.Update()
    return (c1)


def drawPlots(on_canvas, op):
    c1=on_canvas
    c1.Divide(1,2)

    if op == 0:
        c1.cd(1)
        deadChanHisto.Draw()
        deadChanHisto.GetXaxis().SetLabelSize(0.08)
        deadChanHisto.GetXaxis().SetLabelSize(0.08)
        deadChanHisto.GetYaxis().SetTitle("Number of dead channels")
        deadChanHisto.GetXaxis().SetTitle("Sensor Number")
        print "# deads"
        print  deadChanHisto.Integral()
        c1.cd(2)
        deadLinkHisto.Draw('colz')       
    
    if op ==1:
        c1.cd(1)
        noisyChanHisto.Draw()
        print "# noisies"
        print  noisyChanHisto.Integral()
        c1.cd(2)
        noisyLinkHisto.Draw('colz')
    

    if op ==2 :
       
        #1D histo
        badChanHisto.GetXaxis().SetLabelSize(0.07)
        badChanHisto.GetXaxis().SetTitle("Sensor Number")
        badChanHisto.GetXaxis().SetTitleSize(0.06)
        badChanHisto.GetXaxis().SetTitleOffset(.8)
        badChanHisto.GetYaxis().SetLabelSize(0.07)
        badChanHisto.GetYaxis().SetTitle("Number of bad channels")
        badChanHisto.GetYaxis().SetTitleSize(0.07)
        badChanHisto.GetYaxis().SetTitleOffset(.4)

        c1.cd(1)
        badChanHisto.Draw()
 
        #2D histo
        c1.cd(2)
        badLinkHisto.GetXaxis().SetLabelSize(0.07)
        badLinkHisto.GetXaxis().SetTitle("Sensor Number")
        badLinkHisto.GetXaxis().SetTitleSize(0.06)
        badLinkHisto.GetXaxis().SetTitleOffset(.8)

        badLinkHisto.GetYaxis().SetLabelSize(0.07)
        badLinkHisto.GetYaxis().SetTitle("Analogue Link")
        badLinkHisto.GetYaxis().SetTitleSize(0.07)
        badLinkHisto.GetYaxis().SetTitleOffset(.4)

    
        badLinkHisto.SetMinimum(0.);
        badLinkHisto.SetMaximum(32.);
        badLinkHisto.Draw('colz')
  
        # c1.Update()
    return (c1)

def define_style() :
    gROOT.SetStyle( 'Plain' )
    gStyle.SetPalette(1)
    gStyle.SetOptStat(0)
    gROOT.ForceStyle()
    
    
def calculateBadStripsOnLink(iLink,histo): 
# calculates how many channels are bad on this link
   numInLink=32
   firstInLink=iLink*numInLink
   lastInLink=((iLink+1)*numInLink)-1

   # loop over channels and find bad ones
   BadTotal=0
   for iChan in range (firstInLink,lastInLink):
       BadTotal = BadTotal + histo.GetBinContent(iChan+1)
   return BadTotal


def getHistogram(f, nHisto):
# takes filename and histogram number nHisto
# returns histograms

  dir = f.Get("/Vetra/BadChannelMon/BadChannels")
  if (dir.GetListOfKeys()).GetEntries() != 1:
      name = ((dir.GetListOfKeys()).At(nHisto)).GetName()
      
      if name[:6] == "Sensor" :
          SensorNo = int(name[-3:])
                    
          histoStrips=f.Get("/Vetra/BadChannelMon/BadChannels/SensorNo_" + str(SensorNo).zfill(3))
          histoNoisyStrips=f.Get("/Vetra/BadChannelMon/NoisyChannels/SensorNo_" + str(SensorNo).zfill(3))
          histoRawNoiseD=f.Get("/Vetra/BadChannelMon/RawNoiseD/SensorNo_" + str(SensorNo).zfill(3))
          histoCMCNoiseD=f.Get("/Vetra/BadChannelMon/CMCNoiseD/SensorNo_" + str(SensorNo).zfill(3))
          histoRawNoise=f.Get("/Vetra/BadChannelMon/RawNoise/SensorNo_" + str(SensorNo).zfill(3))
          histoCMCNoise=f.Get("/Vetra/BadChannelMon/CMCNoise/SensorNo_" + str(SensorNo).zfill(3))
          
          
      return  SensorNo,histoStrips,histoNoisyStrips,histoRawNoiseD,histoCMCNoiseD,histoRawNoise,histoCMCNoise
  
def getHistogramFromSenNo(f, inSensorNo):
# takes filename and sensor number inSensorNo
# returns histograms for that sensor

    global badChannelPerSensorHisto
    global RawNoisePerSensorHisto
    global cmNoisePerSensorHisto

    dir = f.Get("/Vetra/BadChannelMon/BadChannels")
    
    SensorNo = int(inSensorNo)
    

    histoStrips=f.Get("/Vetra/BadChannelMon/BadChannels/SensorNo_" + str(SensorNo).zfill(3))
    histoNoisyStrips=f.Get("/Vetra/BadChannelMon/NoisyChannels/SensorNo_" + str(SensorNo).zfill(3))
    histoRawNoiseD=f.Get("/Vetra/BadChannelMon/RawNoiseD/SensorNo_" + str(SensorNo).zfill(3))
    histoCMCNoiseD=f.Get("/Vetra/BadChannelMon/CMCNoiseD/SensorNo_" + str(SensorNo).zfill(3))
    histoRawNoise=f.Get("/Vetra/BadChannelMon/RawNoise/SensorNo_" + str(SensorNo).zfill(3))
    histoCMCNoise=f.Get("/Vetra/BadChannelMon/CMCNoise/SensorNo_" + str(SensorNo).zfill(3))
    
    if not histoStrips:
        print "can't find Sensor %d in file"%inSensorNo
        return
    else:
        print "found Sensor %d in file"%inSensorNo
        
    return  SensorNo,histoStrips, histoNoisyStrips,histoRawNoiseD,histoCMCNoiseD,histoRawNoise,histoCMCNoise

    
def drawNoisyChannelsPerSensor(FileName,SensorNo):
    mainDrawNoisyChannelsPerSensor(FileName,SensorNo)
    c1=drawPlotsMakeCanvasPerSensor()
   
    return(c1)
        

def mainDrawNoisyChannelsPerSensor(FileName,HVOffFileName,SensorNo):

   #Takes in root file FileName and Sensor Number Sensor No
   #open root file    
    file=TFile(FileName)
    if file.IsOpen() == 0:
        print "Error could not open file " + FileName
        return


    # open HV off root file
    fileHVOff=TFile(HVOffFileName)
    if fileHVOff.IsOpen() == 0:
       print "Error could not open file " + HVOffFileName
       return
    deadChannelPerSensorHisto.Reset()
    #Get the Histograms using filename and sensor number
   
    nSensor,histoStrips,noisyStrips,histoRawNoiseD,histoCMCNoiseD,histoRawNoise,histoCMCNoise = getHistogramFromSenNo(file, SensorNo)
    nSensorHVOff,histoStripsHVOff,noisyStripsHVOff,histoRawNoiseDHVOff,histoCMCNoiseDHVOff,histoRawNoiseHVOff,histoCMCNoiseHVOff = getHistogramFromSenNo(fileHVOff, SensorNo)

    #make histograms for scope issues 
    for iChan in range(0,2048):
        rawNoiseOn = histoRawNoise.GetBinContent(iChan+1)
        rawNoiseOff = histoRawNoiseHVOff.GetBinContent(iChan+1)
        rawNoiseDiff = rawNoiseOff - rawNoiseOn
        if iChan % 32 != 0:
            if rawNoiseDiff < .25 :
                deadChannelPerSensorHisto.SetBinContent(iChan+1,1)
        noisyChannelPerSensorHisto.SetBinContent(iChan+1,noisyStrips.GetBinContent(iChan+1))
        badChannelPerSensorHisto.SetBinContent(iChan+1,histoStrips.GetBinContent(iChan+1))
        RawNoisePerSensorHisto.SetBinContent(iChan+1,histoRawNoise.GetBinContent(iChan+1))
        cmNoisePerSensorHisto.SetBinContent(iChan+1,histoCMCNoise.GetBinContent(iChan+1))

 

    return 

def drawPlotsMakeCanvasPerSensor():

    global badChannelPerSensorHisto
    global RawNoisePerSensorHisto
  
    
    c1=TCanvas("c2","Bad Channels and Noise for Sensor",0,0,800,600)
  
    c1.Divide(1,2)
    
    badChannelPerSensorHisto.GetXaxis().SetTitle("Channel")
    c1.cd(1)
    badChannelPerSensorHisto.Draw()

    
    c1.cd(2)
    RawNoisePerSensorHisto.GetXaxis().SetTitle("Channel")
    RawNoisePerSensorHisto.GetYaxis().SetTitle("RawNoise")
    RawNoisePerSensorHisto.Draw()

    c1.Modified()
    c1.Update()

    return (c1)


def drawPlotsPerSensor(on_canvas, op):
    global badChannelPerSensorHisto
    global RawNoisePerSensorHisto
    global noisyChannelPerSensorHisto
    global cmNoisePerSensorHisto
    global deadChannelPerSensorHisto

    c2=on_canvas
  
    c2.Divide(1,2)

    if op==0:
         c2.cd(1)
         deadChannelPerSensorHisto.Draw()
         c2.cd(2)
         RawNoisePerSensorHisto.Draw()
         c2.Update()
    
    if op==1:
         c2.cd(1)
         noisyChannelPerSensorHisto.Draw()
         c2.cd(2)
         cmNoisePerSensorHisto.Draw()
         c2.Update()

    if op==2:
        c2.cd(1)
        badChannelPerSensorHisto.GetXaxis().SetTitle("Channel")
        badChannelPerSensorHisto.Draw()   
        c2.cd(2)
        RawNoisePerSensorHisto.GetXaxis().SetTitle("Channel")
        RawNoisePerSensorHisto.GetYaxis().SetTitle("RawNoise")
        RawNoisePerSensorHisto.Draw()

        badChannelPerSensorHisto.GetXaxis().SetLabelSize(0.07)
        badChannelPerSensorHisto.GetXaxis().SetTitleSize(0.06)
        badChannelPerSensorHisto.GetXaxis().SetTitleOffset(.8)
        badChannelPerSensorHisto.GetYaxis().SetLabelSize(0.07)
        badChannelPerSensorHisto.GetYaxis().SetTitleSize(0.07)
        badChannelPerSensorHisto.GetYaxis().SetTitleOffset(.4)

        RawNoisePerSensorHisto.GetXaxis().SetLabelSize(0.07)
        RawNoisePerSensorHisto.GetXaxis().SetTitleSize(0.06)
        RawNoisePerSensorHisto.GetXaxis().SetTitleOffset(.8)
        RawNoisePerSensorHisto.GetYaxis().SetLabelSize(0.07)
        RawNoisePerSensorHisto.GetYaxis().SetTitleSize(0.07)
        RawNoisePerSensorHisto.GetYaxis().SetTitleOffset(.4)
    
    return (c2)



###############################################################################

if __name__ == "__main__":
    if len( argv ) == 2 and ( '--help' == argv[1] or '-h' == argv[1] ):
        usage()
        exit()
    
    if len( argv ) > 3:
        print "Too many parameters, some will be ignored"
        usage()
    
    # set histograms file name, overwriting it if passed as an argument
    if len( argv ) > 1:
        MONI_FILENAME = argv[1]
    
    # set histograms directory, overwriting it if passed as an argument
    if len( argv ) > 2:
        MONI_DIR = argv[2]

    define_style()

   
class BadChannelsMoniHistos:
   def __init__( self, filename=MONI_FILENAME, subdir=MONI_DIR ) :
      import os
      """Constructor"""
      self.filename = filename
      self.subdir   = subdir
      HVOffFile = os.readlink("/home/keaveney/cmtuser/Vetra_v8r1p1/Velo/VetraScripts/macros/HVOffFile")
      self.HVOffFile = HVOffFile
      print HVOffFile
      print "constructing BadChannelsMoniHistos ..."

   def draw_badchannels_gui_page(self, mop):
       mainDrawNoisyChannelMaps(self.filename , self.HVOffFile )
       drawPlots(canvasBadChannels, mop)       
       # return 
class BadChannelsMoniHistosSensor:
    def __init__( self, filename=MONI_FILENAME, subdir=MONI_DIR ) :
        import os
        """Constructor"""
        self.filename = filename
        self.subdir   = subdir
        HVOffFile = os.readlink("/home/keaveney/cmtuser/Vetra_v8r1p1/Velo/VetraScripts/macros/HVOffFile")
        self.HVOffFile = HVOffFile
        print HVOffFile
        print "constructing BadChannelsMoniHistos ..."
    def draw_badchannelspersensor_gui_page(self,sensor,opSS  ):
        mainDrawNoisyChannelsPerSensor(self.filename,self.HVOffFile,sensor)
        drawPlotsPerSensor(canvasBadChannels,opSS) 
