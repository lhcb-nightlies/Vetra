""" Serves as a central place for configuring constants
used in python VeloMoniGUI code. 

For global definitions it provides a parser that creates
a dictionary from all #define directives in 
$VETRASCRIPTSROOT/macros/GUIConfig.h

@author Kurt Rinnert, <kurt.rinnert@cern.ch>
@date 27.06.2011
"""
import re
import os

class CPPDefineParser:
	"""Parses a file and creates a dictionary from all object-like 
	C pre processor #define directives. 
	"""

	def __init__(self):
		self.defines = { }
		self.reg_exp = re.compile("^\s*#define.*")

	def parse(self, file_name):
		self.defines = { }
		file = open(file_name);

		for line in file:
			match = self.reg_exp.search(line)
			if match:
				tokens = line.rstrip().split(None,2)
				self.defines[tokens[1]] = tokens[2]


def GUIConfig():
	"""Read the GUI configuration from 
	$VETRASCRIPTSROOT/macros/GUIConfig.h
	"""
	config_path = os.environ[ 'VETRASCRIPTSROOT' ]
	if os.path.exists( config_path ):
		config_path = config_path + os.sep + 'macros' + os.sep + 'GUIConfig.h'
	else:
		return None

	parser = CPPDefineParser()
	parser.parse(config_path)

	return parser.defines
	



