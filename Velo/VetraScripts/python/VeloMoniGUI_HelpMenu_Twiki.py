# Helper (temporary) script used by VeloMoniGUI
# Eduardo Rodrigues
# 2009-03-03

import webbrowser

class WebBrowser:
    def open( self, url ) :
        webbrowser.open( url )
