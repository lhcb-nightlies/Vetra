#! /usr/bin/env python
###############################################################################
"""

 drawClustersMPVFWHMMoni.py
 ===================

 Python script to display most probable value (MPV) and full width half maximum (FWHM) of ADC distributions. A full Landau fit is time consuming and hence only a search for the maximum bin is performed, and the points at half height.

 Description:
   This script displays MPV and FWHM histograms based on histograms from the VeloExpertClusterMonitor algorithm in the Velo/VeloRecMonitors package.

 Requirements:
   The input file should be the histogram output of the algorithm
     VeloExpertClusterMonitor
   located in the package
     Velo/VeloRecMonitors

   The following options need to be turned on.....(James to fill in)

 Examples usage:
   1)
   python -i drawClustersMPVFWHMMoni.py [ inputFile.root [ My/RootFile/Directory ] ]
   drawClustersMPVFWHMMoni()
   2)
   python -i drawClustersMPVFWHMMoni.py
   drawClustersMPVFWHMMoni( filename='/Path/To/HistosFile.root', subdir='Velo/Bla',corrected= int )
   3) to make reference plots
   RefMPVFWHMMoni("big.root",EXPERT_MONI_DIR)
 @author James & Chris
 @date   2009-06-01

"""
###############################################################################

from os   import path
from sys  import argv
from ROOT import *
from ROOT import gROOT, gStyle, gDirectory, kBlue, kRed
from ROOT import TFile, TCanvas, TH1F,TH1D, TGraph, TLine
from ROOT import VeloSensor, helperTools, ClusterFitter

import os
# Histograms for calculated (fast) MPV/FWHM
global MPVHistogram,FWHMHistogram
MPVHistogram=TH1F("MPV","MPV Vs Sensor",120,-0.5,119.5)
FWHMHistogram=TH1F("FWHM","FWHM Vs Sensor",120,-0.5,119.5)
global MPVHistogramRef,FWHMHistogramRef
MPVHistogramRef=TH1F("MPV_Ref","MPV Vs Sensor",120,-0.5,119.5)
FWHMHistogramRef=TH1F("FWHM_Ref","FWHM Vs Sensor",120,-0.5,119.5)

global SNMPVHist,SNFWHMHist,SNMPVHist_Ref,SNFWHMHist_Ref

SNMPVHist=TH1F("SNMPVHist","SN MPV",120,-0.5,119.5)
SNFWHMHist=TH1F("SNFWHMHist","SN FWHM",120,-0.5,119.5)
SNMPVHist_Ref=TH1F("SNMPVHist_Ref","SN MPV Ref",120,-0.5,119.5)
SNFWHMHist_Ref=TH1F("SNFWHMHist_Ref","SN FWHM Ref",120,-0.5,119.5)

# Histograms for fitted MPV/FWHM
global MPVHistogram_ontrack,FWHMHistogram_ontrack
global MPVHistogramRef_ontrack,FWHMHistogramRef_ontrack
MPVHistogram_ontrack=TH1F("MPV_ontrack","Ontrack MPV Vs Sensor",120,-0.5,119.5)
FWHMHistogram_ontrack=TH1F("FWHM_ontrack","Ontrack FWHM Vs Sensor",120,-0.5,119.5)
MPVHistogramRef_ontrack=TH1F("MPV_ref_ontrack","Ontrack MPV Vs Sensor reference",120,-0.5,119.5)
FWHMHistogramRef_ontrack=TH1F("FWHM_ref_ontrack","Ontrack FWHM Vs Sensor reference",120,-0.5,119.5)

global MPVHistogram_corrected,FWHMHistogram_corrected
global MPVHistogramRef_corrected,FWHMHistogramRef_corrected
MPVHistogram_corrected=TH1F("MPV_corrected","Corrected MPV Vs Sensor",120,-0.5,119.5)
FWHMHistogram_corrected=TH1F("FWHM_corrected","Corrected FWHM Vs Sensor",120,-0.5,119.5)
MPVHistogramRef_corrected=TH1F("MPV_ref_corrected","Corrected MPV Vs Sensor reference",120,-0.5,119.5)
FWHMHistogramRef_corrected=TH1F("FWHM_ref_corrected","Corrected FWHM Vs Sensor reference",120,-0.5,119.5)

global MPVHistogram_raw,FWHMHistogram_raw
global MPVHistogramRef_raw,FWHMHistogramRef_raw
MPVHistogram_raw=TH1F("MPV_raw","Raw MPV Vs Sensor",120,-0.5,119.5)
FWHMHistogram_raw=TH1F("FWHM_raw","Raw FWHM Vs Sensor",120,-0.5,119.5)
MPVHistogramRef_raw=TH1F("MPV_ref_raw","Raw MPV Vs Sensor reference",120,-0.5,119.5)
FWHMHistogramRef_raw=TH1F("FWHM_ref_raw","Raw FWHM Vs Sensor reference",120,-0.5,119.5)

# file where to look for histograms
EXPERT_MONI_FILENAME = 'Vetra_histos.root'

# directory where to look for histograms
EXPERT_MONI_DIR = 'Velo/VeloExpertClusterMonitor/'
CORRECTED_ADC = 'ontrack/corr/adc/'
RAW_ADC = 'raw/non_corr/adc/'
NON_CORRECTED_ADC = 'ontrack/non_corr/adc/'
EXPERT_MONI_REF_DIR = '/Velo/VeloExpertClusterMonitor/Reference'
NOISE_DIR = '/Vetra/NoiseMon'

raw_fit = {}
corr_fit = {}
ontr_fit = {}
# =============================================================================
def MPVFWHMusage():
  """
  prints the usage instructions
  """
  print "Please use as follows:"
  print "python -i drawClustersMPVFWHMMoni.py [ inputFile.root [ My/RootFile/Directory ] ]"
  print " - default input file: \"Vetra_histos.root\", in the working directory"
  print " - default plot directory: \"Velo/VeloExpertClusterMonitor\"\n"

# =============================================================================
# defines a class to store MPV FWHM and sensor_number
class LandauParam:
        def _init_(self,mpv,fwhm,sensnum,err):
                self.mpv=mpv
                self.fwhm=fwhm
                self.sensnum=sensnum

        def setLandau(self,mpv,fwhm):
                self.mpv=mpv
                self.fwhm=fwhm
        def setLandauParam(self,mpv,fwhm,sensnum):
                self.mpv=mpv
                self.fwhm=fwhm
                self.sensnum=sensnum
        def setLandauParamErr(self,mpv,fwhm,sensnum,err):
                self.mpv=mpv
                self.fwhm=fwhm
                self.sensnum=sensnum
                self.err = err
        def setmpv(self,mpv):
                self.mpv=mpv

        def setfwhm(self,fwhm):
                self.fwhm=fwhm

        def setsensnum(self,sensnum):
                self.sensnum=sensnum

        def mpv(self):
                return self.mpv

        def fwhm(self):
                return self.fwhm

        def sensnum(self):
                return self.sensnum
        def err(self):
                return self.err


# =============================================================================
def times(h1,factor):
  bins = h1.GetNbinsX()
  entries = h1.GetEntries()
  if h1.GetXaxis():
    max = h1.GetXaxis().GetXmax()*factor
    min = h1.GetXaxis().GetXmin()*factor
  h2 = TH1D("h2","h2",bins,min,max)
  for i in range( bins):
    for j in range(int(h1.GetBinContent(i))):
      h2.AddBinContent(i)
  h2.SetEntries(entries)
  #print h2.GetEntries()

  return h2
# =============================================================================
def _drawClustersMPVFWHMMoni( filename, subdir,corrected,Plots ) :
  """Internal function. Not to be called directly by the user"""
  histos1 = ClusterMPVFWHMMoniHistos( filename, subdir )
  c1 = TCanvas( 'MPV and FWHM of Landau' )
  c1.Divide( 1, 2 )
  histos1.draw_MPVFWHM_page( c1,corrected,Plots )
  return ( c1)
# =============================================================================
def RefMPVFWHMMoni( filename, subdir,Plots=1 ) :
  """Internal function. Not to be called directly by the user"""
  histos1 = ClusterMPVFWHMMoniHistos( filename, subdir )
  c1 = TCanvas( 'MPV and FWHM of Landau' )
  c1.Divide( 1, 2 )
  for corrected in range(3):
    print corrected
    histos1.draw_MPVFWHM_page( c1,corrected,Plots )
  histos1.saveReferencePlots()
  return ( c1)

# =============================================================================
def define_MPVFWHM_style() :
    gROOT.SetStyle( 'Plain' )
    gStyle.SetPalette(1)
    gStyle.SetOptStat(0100)
    gStyle.SetStatW(0.09)
    gStyle.SetStatH(0.09)
    #gStyle.SetTitleFontSize(0.12);
    #gStyle.SetLabelSize(0.08);
    #gROOT.ForceStyle()
# =============================================================================
def define_Landau_style() :
    gROOT.SetStyle( 'Plain' )
    gStyle.SetPalette(1)
    gStyle.SetOptStat(0001)
    gStyle.SetOptFit(1)
    gStyle.SetStatW(0.14)
    gStyle.SetStatH(0.2)
    #gStyle.SetTitleFontSize(0.09);
    #gStyle.SetLabelSize(0.08);
    #gROOT.ForceStyle()
# =============================================================================
# This function finds the maximum point on a single sensor ADC sum plot and the FWHM of the plot.
# This does not do a fit, it is a quick routine which just finds the highest bin and numerically claculates the width.

#Inputs: h1 - the cluster ADC sum histogram
#        sensor - sensor number
#Output: returns a sensor class containing MPV, FWHM, and sensor number
# Set min bin to 15 cut is aimed to exclude noise
def calculateMPVFWHM(h1,sensor):
  #     h1.Rebin() ##// for low stats can be useful to rebin the historam combining entries
  if h1.GetNbinsX()==1000:
    h1.Rebin(10)
  xLowerFinal = None
  xUpperFinal = None
  if h1.GetEntries() > 0. :
    if h1.GetBinCenter(h1.GetMaximumBin())<15:
      h1.SetAxisRange(15.,100.)
      #          if h1.GetBinCenter(h1.GetMaximumBin())<20:
      #            h1.SetAxisRange(0.,100.)
      #            h1.Rebin()
      #            h1.SetAxisRange(17.,100.)
      MPV = h1.GetBinCenter(h1.GetMaximumBin())
      MPVEntries = h1.GetBinContent(h1.GetMaximumBin())
    else:
      # find the highest bin in the plot - this is the MPV
      MPV = h1.GetBinCenter(h1.GetMaximumBin())
      # find the FWHM in the plot
      MPVEntries =  h1.GetBinContent(h1.GetMaximumBin())
      # finds the lower value of X for the FWHM point
      #        h1.SetAxisRange(0.,100.)
      #        h1.Rebin()
    MPVEntries =  h1.GetBinContent(h1.GetMaximumBin())
    lowersearch = range(1,h1.GetMaximumBin())
    lowersearch.reverse()
    for i in lowersearch:
      if (h1.GetBinContent(i)<(MPVEntries/2.)):
        y0 = h1.GetBinContent(i)- h1.GetBinContent((i+1))
        x0 = h1.GetBinCenter(i)-h1.GetBinCenter(i+1)
        xLowerFinal = h1.GetBinCenter(i) + (x0/y0)*(MPVEntries/2.0 - h1.GetBinContent(i))
        break
      # finds the upper value of X for the FWHM point
      else:
        xLowerFinal = None
    for i in range(h1.GetMaximumBin(),h1.GetNbinsX()):
      if (h1.GetBinContent(i)<(MPVEntries/2.)):
        y1 = h1.GetBinContent(i-1)- h1.GetBinContent(i)
        x1 = h1.GetBinCenter(i-1)-h1.GetBinCenter(i)
        xUpperFinal = h1.GetBinCenter(i) - (x1/y1)*(MPVEntries/2.0 - h1.GetBinContent(i))
        # +( ((MPVEntries/2)*(x1/y1)))
        break
      else:
        xUpperFinal = None
        # calculate FWHM from upper and lower x values
    if (xUpperFinal):
      if xLowerFinal:
        FWHM = xUpperFinal-xLowerFinal
      else:
        FWHM = 0.
    else:
      FWHM = 0.
      # create a class instance to store these parameters
  else:
    MPV = 0.
    FWHM = 0.
  Landau = LandauParam()
  Landau.setLandauParam(MPV, FWHM,sensor)
  return Landau
# =============================================================================
# mean of histogram
def HistMean(h1,minBin,maxBin):
  sum = 0.0
  noBins = maxBin-minBin
  for bin in range(minBin,maxBin):
    sum = h1.GetBinContent(bin)+sum
    if h1.GetBinContent(bin) == 0.0:
      noBins = noBins-1
  if noBins == 0.0:
    return 0.0
  else:
    return sum/noBins
# =============================================================================
# fit for mean of  histogram
def fitHistMean(h1,minRange,maxRange,canvasDiv = 1):
  canvasMPVFWHM.cd(canvasDiv)
  if h1.GetEntries()> 0 and h1.GetMean()>0. :
    func = TF1("mypol0","pol0",minRange,maxRange)
    h1.Fit(func,"NOQ","",minRange,maxRange)
    #func  = h1.GetFunction("pol0")
    func.Draw("same")
    if func:
      return func.GetParameter(0),func.GetParError(0)
    else:
      return 0,0
  else:
    return 0,0
# find outliers
# =============================================================================
def outliers(h1,factor = 2.5):
  mean_r,err_r = fitHistMean(h1,0,41)
  for i in range(41):
    cont = h1.GetBinContent(i)
    if abs(cont-mean_r)>(err_r*factor):
      print i
  mean_phi,err_phi = fitHistMean(h1,64,105)
  for i in range(64,105):
    cont = h1.GetBinContent(i)
    if abs(cont-mean_phi)>(err_phi*factor):
      print i
  return mean_r,mean_phi
# =============================================================================
# fit to get the MPV and FWHM
def fitMPVFWHMForGUI(h1,sensor,fits=None):
  global cluster
  FWHM = Double()
  MPV = Double()
  FH = Double()
  FL = Double()
  fit = TF1()
  global lower_range
  lower_range = 0.66
  global higher_range
  higher_range = 1.4
  cluster = ClusterFitter()
  theFitFn = cluster.langausData(h1,MPV,FWHM,lower_range,higher_range)
  mpv = theFitFn.GetParameter(1)
  fwhm = FWHM
  if not fits is None:
    fits[sensor] = theFitFn
    fits[sensor].SetName("%s_%s" % (fits[sensor].GetName(), "save"))

  Landau = LandauParam()
  Landau.setLandauParam(mpv,fwhm,sensor)
  return Landau
# =============================================================================
# fit to get the MPV and FWHM
def fitMPVFWHM(h1,sensor,corr):
  global cluster
  FWHM = Double()
  MPV = Double()
  FH = Double()
  FL = Double()
  fit = TF1()
  global lower_range
  lower_range = 0.66
  global higher_range
  higher_range = 1.4
  cluster = ClusterFitter()
  if corr is 0:
    ontr_fit[sensor] = cluster.langausData(h1,MPV,FWHM,lower_range,higher_range)
    mpv = ontr_fit[sensor].GetParameter(1)
    #    err = ontr_fit[sensor].GetParError(1)
    #    fwhm = ontr_fit[sensor].GetParameter(0)
    fwhm = FWHM
  elif corr is 1:
    corr_fit[sensor] = cluster.langausData(h1,MPV,FWHM,lower_range,higher_range)
    mpv = corr_fit[sensor].GetParameter(1)
#    err = corr_fit[sensor].GetParError(1)
    #fwhm = corr_fit[sensor].GetParameter(0)
    fwhm = FWHM
  elif corr is 2:
    raw_fit[sensor] = cluster.langausData(h1,MPV,FWHM,lower_range,higher_range)
    mpv = raw_fit[sensor].GetParameter(1)
#    err = raw_fit[sensor].GetParError(1)
    #fwhm = raw_fit[sensor].GetParameter(0)
    fwhm = FWHM
      
  Landau = LandauParam()
  Landau.setLandauParam(mpv,fwhm,sensor)
  return Landau
# =============================================================================
def get_histo_min_max( histograms, ExtraSpace = 0.0, OnlyRPhi=False ):
    """Helper function to get the max and min y of a list of histos"""
    # set to values that will be replaced in the following 
    ymin = histograms[0].GetMaximum()
    ymax = histograms[0].GetMinimum()

    # find max/min excluding sensors that don't exist
    MaxSensor = 110
    for h in histograms:
        for sens in xrange(0,MaxSensor):
            if helperTools.isSensorRType(sens)\
                or helperTools.isSensorPhiType(sens)\
                or not OnlyRPhi:
                ymin = min( ymin, h.GetBinContent(sens+1) )
                ymax = max( ymax, h.GetBinContent(sens+1) )
    return (ymin - ExtraSpace, ymax + ExtraSpace)
# ============================================================================

class ClusterMPVFWHMMoniHistos :
# =============================================================================

    def __init__( self, filename=EXPERT_MONI_FILENAME, subdir=EXPERT_MONI_DIR,debug=0 ) :

        """Constructor"""
        self.filename = filename
        self.subdir   = subdir
        self.SenPath = ""
        self.debug = debug
        self.ADC_plot = TH1D()
        self.canvasMPVFWHM = None
        self.canvasSensorLandau = None
##         self.SN_plot = TH1D()
##         print "sn plot"

        if(self.debug):self.print_details()

        # =============================================================================
        # loops over all sensors, finding FWHM and MPV for each
        # fills a histogram with these values
        # returns MPV, FWHM histograms

        # Inputs: RootFN- root file Name
        #         Corrected - whether to use raw adc values (1) or corrected ADC values (0)

        # Outputs: returns histograms of MPV and FWHM versus sensor number
        # =============================================================================

    def setDebug( self, yesNo ):
      self.debug = yesNo

    def get_landau( self,titles ):
      """
      Retrieves a list of histos given as argument from file/directory optionally
      specified as parameters to __main__.
      @param  filename the input file name
      @param  subdir the input file directory
      @param  titles the list of histogram titles to retrieve
      @return histos the list of histograms
      """
      # open root file
      if os.path.isfile( self.filename ):
        landau_file = TFile( self.filename )
      else:
        if (self.debug): print 'File "%s" not found! Nothing done.' % self.filename
        #usage()
        return None
        #exit()
      global histos
      if landau_file.Get(titles):
      ## get histograms
        tmp = gDirectory.Get( titles ).Clone()
        tmp.SetDirectory( 0 )
        histos=tmp

##       except ( TypeError, AttributeError ):
##         pass
        return histos
    def get_reflandau( self,RefFileName,titles ):
      """
      Retrieves a list of histos given as argument from file/directory optionally
      specified as parameters to __main__.
      @param  filename the input file name
      @param  subdir the input file directory
      @param  titles the list of histogram titles to retrieve
      @return histos the list of histograms
      """
      # open root file
      if os.path.isfile( RefFileName ):
        landau_file = TFile( RefFileName )
      else:
        if (self.debug): print 'File "%s" not found! Nothing done.' % RefFileName
        #usage()
        return None
        #exit()
      global histos_ref
      if landau_file.Get(titles):
      ## get histograms
        tmp = gDirectory.Get( titles ).Clone()
        tmp.SetDirectory( 0 )
        histos_ref=tmp

##       except ( TypeError, AttributeError ):
##         pass
        return histos_ref

    def get_refplots( self,RefFileName,titles ):
      """
      Retrieves a list of histos given as argument from file/directory optionally
      specified as parameters to __main__.
      @param  filename the input file name
      @param  subdir the input file directory
      @param  titles the list of histogram titles to retrieve
      @return histos the list of histograms
      """
      if os.path.isfile( RefFileName ):
        ref_file = TFile( RefFileName )
      else:
        print 'File "%s" not found! Nothing done.' % RefFileName
        return None        #exit()
      global ref_histos
      dir = ref_file.GetDirectory(EXPERT_MONI_REF_DIR)
      if dir:
        tmp = dir.Get(titles).Clone()
        tmp.SetDirectory( 0 )
        ref_histos=tmp
        return ref_histos
      else:
          return None
      
    def drawHistos(self,canvasMPVFWHM,HistogramMPV, HistogramFWHM):
      canvasMPVFWHM.Clear()
      canvasMPVFWHM.Divide(1,2)
      canvasMPVFWHM.cd()
      if HistogramMPV :
        canvasMPVFWHM.cd(1)

        HistogramMPV.SetXTitle("Sensor Number")
        HistogramMPV.SetYTitle("MPV (ADC counts)")
        HistogramMPV.SetMarkerColor(kRed)
        HistogramMPV.SetMarkerStyle(20)
        if HistogramMPV.GetXaxis():
          HistogramMPV.GetXaxis().SetLabelSize(0.08)
          HistogramMPV.GetXaxis().SetTitleSize(0.1)
          HistogramMPV.GetXaxis().SetTitleOffset(.4)
          HistogramMPV.GetYaxis().SetLabelSize(0.08)
          HistogramMPV.GetYaxis().SetTitleSize(0.1)
          HistogramMPV.GetYaxis().SetTitleOffset(.4)
        HistogramMPV.Draw("P")
        canvasMPVFWHM.Update()

      if HistogramFWHM :
        canvasMPVFWHM.cd(2)

        HistogramFWHM.SetXTitle("Sensor Number")
        HistogramFWHM.SetYTitle("FWHM (ADC counts)")
        HistogramFWHM.SetMarkerColor(kBlue)
        HistogramFWHM.SetMarkerStyle(20)
        if HistogramFWHM.GetXaxis():
          HistogramFWHM.GetXaxis().SetLabelSize(0.08)
          HistogramFWHM.GetXaxis().SetTitleSize(.1)
          HistogramFWHM.GetXaxis().SetTitleOffset(.4)
          HistogramFWHM.GetYaxis().SetLabelSize(0.08)
          HistogramFWHM.GetYaxis().SetTitleSize(.1)
          HistogramFWHM.GetYaxis().SetTitleOffset(.4)
        HistogramFWHM.Draw("P")

        canvasMPVFWHM.Update()


    def drawDiffGraphs(self,canvasMPVFWHM,GraphMPV, GraphFWHM):
      canvasMPVFWHM.Clear()
      canvasMPVFWHM.Divide(1,2)
      canvasMPVFWHM.cd()
      if GraphMPV :
        canvasMPVFWHM.cd(1)

        GraphMPV.GetXaxis().SetTitle("Sensor Number")
        GraphMPV.GetYaxis().SetTitle("MPV (ADC counts)")
        GraphMPV.SetMarkerColor(kRed)
        GraphMPV.SetMarkerStyle(20)
        if GraphMPV.GetXaxis():
          GraphMPV.GetXaxis().SetLabelSize(0.08)
          GraphMPV.GetXaxis().SetTitleSize(0.1)
          GraphMPV.GetXaxis().SetTitleOffset(.4)
          GraphMPV.GetYaxis().SetLabelSize(0.08)
          GraphMPV.GetYaxis().SetTitleSize(0.1)
          GraphMPV.GetYaxis().SetTitleOffset(.4)
        GraphMPV.Draw("PA")
        canvasMPVFWHM.Update()

      if GraphFWHM :
        canvasMPVFWHM.cd(2)

        GraphFWHM.GetXaxis().SetTitle("Sensor Number")
        GraphFWHM.GetYaxis().SetTitle("FWHM (ADC counts)")
        GraphFWHM.SetMarkerColor(kBlue)
        GraphFWHM.SetMarkerStyle(20)
        if GraphFWHM.GetXaxis():
          GraphFWHM.GetXaxis().SetLabelSize(0.08)
          GraphFWHM.GetXaxis().SetTitleSize(.1)
          GraphFWHM.GetXaxis().SetTitleOffset(.4)
          GraphFWHM.GetYaxis().SetLabelSize(0.08)
          GraphFWHM.GetYaxis().SetTitleSize(.1)
          GraphFWHM.GetYaxis().SetTitleOffset(.4)
        GraphFWHM.Draw("PA")

        canvasMPVFWHM.Update()


    def PlotSN(self,canvas,MPVHis,FWHMHis,CMS=True):
      canvas.Clear()
      canvas.Divide(1,2)
      canvas.cd()
      define_MPVFWHM_style()

      global SNFWHMHist,SNMPVHist
      SNFWHMHist.Reset()
      SNMPVHist.Reset()
      for sensor in range(110):
        sn = self.SignalNoise(sensor,CMS)
        SNFWHMHist.Fill(sensor,FWHMHis.GetBinContent(sensor+1)/sn)
        SNMPVHist.Fill(sensor,MPVHis.GetBinContent(sensor+1)/sn)
      canvas.cd(1)
      SNMPVHist.SetTitleSize(0.04)
      SNMPVHist.SetXTitle("Sensor Number")
      SNMPVHist.SetYTitle("MPV (ADC counts)")
      SNMPVHist.SetMarkerColor(kRed)
      SNMPVHist.SetMarkerStyle(20)
      if SNMPVHist.GetXaxis():
        SNMPVHist.GetXaxis().SetLabelSize(0.08)
        SNMPVHist.GetXaxis().SetTitleSize(.1)
        SNMPVHist.GetXaxis().SetTitleOffset(.4)
        SNMPVHist.GetYaxis().SetLabelSize(0.08)
        SNMPVHist.GetYaxis().SetTitleSize(.1)
        SNMPVHist.GetYaxis().SetTitleOffset(.4)
        SNMPVHist.GetXaxis().SetLabelSize(0.08)
        SNMPVHist.GetYaxis().SetLabelSize(0.08)
      SNMPVHist.Draw("P")
      canvas.cd(2)
      SNFWHMHist.SetTitleSize(.04)
      SNFWHMHist.SetXTitle("Sensor Number")
      SNFWHMHist.SetYTitle("FWHM (ADC counts)")
      SNFWHMHist.SetMarkerColor(kBlue)
      SNFWHMHist.SetMarkerStyle(20)
      if SNFWHMHist.GetXaxis():
        SNFWHMHist.GetXaxis().SetLabelSize(0.08)
        SNFWHMHist.GetXaxis().SetTitleSize(.1)
        SNFWHMHist.GetXaxis().SetTitleOffset(.4)
        SNFWHMHist.GetYaxis().SetLabelSize(0.08)
        SNFWHMHist.GetYaxis().SetTitleSize(.1)
        SNFWHMHist.GetYaxis().SetTitleOffset(.4)
        SNFWHMHist.GetXaxis().SetLabelSize(0.08)
        SNFWHMHist.GetYaxis().SetLabelSize(0.08)
      SNFWHMHist.Draw("P")
      canvas.Update()

    def PlotSNRef(self,canvas,MPVHis,FWHMHis,CMS=True):
      define_MPVFWHM_style()

      global SNFWHMHist_Ref,SNMPVHist_Ref
      SNFWHMHist_Ref.Reset()
      SNMPVHist_Ref.Reset()
      for sensor in range(110):
        sn = self.SignalNoise(sensor,CMS)
        SNFWHMHist_Ref.Fill(sensor,FWHMHis.GetBinContent(sensor+1)/sn)
        SNMPVHist_Ref.Fill(sensor,MPVHis.GetBinContent(sensor+1)/sn)
      canvas.cd(1)
      SNMPVHist_Ref.SetMarkerColor(kRed-7)
      SNMPVHist_Ref.SetMarkerStyle(20)
      if SNMPVHist_Ref.GetXaxis():
        SNMPVHist_Ref.GetXaxis().SetLabelSize(0.08)
        SNMPVHist_Ref.GetXaxis().SetTitleSize(.1)
        SNMPVHist_Ref.GetXaxis().SetTitleOffset(.4)
        SNMPVHist_Ref.GetYaxis().SetLabelSize(0.08)
        SNMPVHist_Ref.GetYaxis().SetTitleSize(.1)
        SNMPVHist_Ref.GetYaxis().SetTitleOffset(.4)
      SNMPVHist_Ref.Draw("P same")
      canvas.cd(2)
      SNFWHMHist_Ref.SetMarkerColor(kBlue-7)
      SNFWHMHist_Ref.SetMarkerStyle(20)
      if SNFWHMHist_Ref.GetXaxis():
        SNFWHMHist_Ref.GetXaxis().SetLabelSize(0.08)
        SNFWHMHist_Ref.GetXaxis().SetTitleSize(.1)
        SNFWHMHist_Ref.GetXaxis().SetTitleOffset(.4)
        SNFWHMHist_Ref.GetYaxis().SetLabelSize(0.08)
        SNFWHMHist_Ref.GetYaxis().SetTitleSize(.1)
        SNFWHMHist_Ref.GetYaxis().SetTitleOffset(.4)
      SNFWHMHist_Ref.Draw("P same")
      canvas.Update()

#============================================================================
    def PlotMPVFWHM(self, RootFN, Corrected, Plot, mpv_hist, fwhm_hist, fits=None):
        """ Alternative function which takes the histograms as parameters
        """
        if(self.debug): print "performing the PlotMPVFWHM function"
        # loop over sensors
        MaxSensor=110
        
        if Plot is 1 and fits:
          fits.clear()
        
        mpv_hist.Reset()
        fwhm_hist.Reset()
        
        for Sen in range(MaxSensor):
          if Corrected is 0:
            SenPath=EXPERT_MONI_DIR + CORRECTED_ADC +"adc_cluster_sen_%03d"%(Sen)
          elif Corrected is 1:
            SenPath=EXPERT_MONI_DIR + NON_CORRECTED_ADC + "adc_cluster_sen_%03d"%(Sen)
          elif Corrected is 2:
            SenPath=EXPERT_MONI_DIR + RAW_ADC + "adc_cluster_sen_%03d"%(Sen)
          else:
            if(self.debug): print "You need to set the type of data to run over"
            if(self.debug): print "   0 = non angle corrected ontrack data"
            if(self.debug): print "   1 = angle corrected ontrack data"
            if(self.debug): print "   2 = raw clusters data"
            return
          # check if correction wanted or not
          # gets histogram and checks exists
          his = self.get_reflandau(RootFN, SenPath)
          
          if his :
##          if self.LandauFile.Get(SenPath):
##            his = self.LandauFile.Get(SenPath)
##            print Corrected
            if Plot is 0:
              ##calculate MPV, FWHM
              result = calculateMPVFWHM(his,Sen)
              sensor_num = result.sensnum
              FWHM = result.fwhm
              MPV = result.mpv
              fwhm_hist.Fill(sensor_num,result.fwhm)
              mpv_hist.Fill(sensor_num,MPV)
            elif Plot is 1 :
              ##fit MPV,FWHM
              histSuffix = ["_ontrack", "_corr", "_raw"]
              
              if Corrected in range(0,3):
                name = his.GetName()+histSuffix[Corrected]
                his.SetName(name)
                result = fitMPVFWHMForGUI(his,Sen,fits)
                sensor_num = result.sensnum
                fwhm_hist.Fill(sensor_num,result.fwhm)
                mpv_hist.Fill(sensor_num,result.mpv)
##                MPVHistogram_ontrack.SetBinError(sensor_num[Sen],result.err)
              
#============================================================================
    def PlotMPVFWHM_Ref(self,RootFN,Corrected,Plot=0):
        MPV = []
        FWHM = []
        sensor_num = []
        if(self.debug): print "performing the PlotMPVFWHM function"
        # loop over sensors

        MaxSensor=110

        for Sen in range(MaxSensor):
          if Sen is 0 and Plot is 1:
            if Corrected is 0:
              self.reset_Ontrack()
            if Corrected is 1:
              self.reset_Corrected()
            if Corrected is 2:
              self.reset_Raw()

          if Corrected is 0:
            SenPath=EXPERT_MONI_DIR + CORRECTED_ADC +"adc_cluster_sen_%03d"%(Sen)
          elif Corrected is 1:
            SenPath=EXPERT_MONI_DIR + NON_CORRECTED_ADC + "adc_cluster_sen_%03d"%(Sen)
          elif Corrected is 2:
            SenPath=EXPERT_MONI_DIR + RAW_ADC + "adc_cluster_sen_%03d"%(Sen)
          else:
            if(self.debug): print "You need to set the type of data to run over"
            if(self.debug): print "   0 = non angle corrected ontrack data"
            if(self.debug): print "   1 = angle corrected ontrack data"
            if(self.debug): print "   2 = raw clusters data"
            return
          # check if correction wanted or not
          # gets histogram and checks exists
          his = self.get_reflandau(RootFN,SenPath)

          if his :
            ##calculate MPV, FWHM
            result = calculateMPVFWHM(his,Sen)
            sensor_num.append(result.sensnum)
            FWHM.append(result.fwhm)
            MPV.append(result.mpv)
            FWHMHistogramRef.Fill(sensor_num[Sen],FWHM[Sen])
            MPVHistogramRef.Fill(sensor_num[Sen],MPV[Sen])
          else:
            #   if histogram doesn't exist put 0 entry
            result=LandauParam()
            result.setLandauParam(0.,0.,Sen)
            MPV.append(result.mpv)
            sensor_num.append(result.sensnum)
            FWHM.append(result.fwhm)

# ============================================================================
    def draw_MPVFWHM_page( self, on_canvas,Corrected,Plots=1  ) :
      """Draws one canvas with MPV and FWHM histograms"""
      define_MPVFWHM_style()
      MPVHistogram.Reset()
      FWHMHistogram.Reset()
      if Plots == 1:

        if Corrected ==0:

          if MPVHistogram_ontrack.GetEntries() < 1:
            self.PlotMPVFWHM(self.filename,Corrected,Plots,MPVHistogram_ontrack,FWHMHistogram_ontrack)
            title = "Ontrack MPV vs Sensor"
            MPVHistogram_ontrack.SetTitle(title)
            MPVHistogram_ontrack.SetName("MPV_ontrack")
            title2 = "Ontrack FWHM vs Sensor"
            FWHMHistogram_ontrack.SetTitle(title2)
            FWHMHistogram_ontrack.SetName("FWHM_ontrack")
            self.drawHistos(on_canvas,MPVHistogram_ontrack,FWHMHistogram_ontrack)
          self.drawHistos(on_canvas,MPVHistogram_ontrack,FWHMHistogram_ontrack)
        elif Corrected == 1:
          if MPVHistogram_corrected.GetEntries() < 1:
            self.PlotMPVFWHM(self.filename,Corrected,Plots,MPVHistogram_corrected,FWHMHistogram_corrected)
            title = "Corrected MPV vs Sensor"
            MPVHistogram_corrected.SetTitle(title)
            MPVHistogram_corrected.SetName("MPV_corrected")
            title2 = "Corrected FWHM vs Sensor"
            FWHMHistogram_corrected.SetTitle(title2)
            FWHMHistogram_corrected.SetName("FWHM_corrected")
            self.drawHistos(on_canvas,MPVHistogram_corrected,FWHMHistogram_corrected)
          self.drawHistos(on_canvas,MPVHistogram_corrected,FWHMHistogram_corrected)
        else:
          if MPVHistogram_raw.GetEntries() < 1:
            self.PlotMPVFWHM(self.filename,Corrected,Plots,MPVHistogram_raw,FWHMHistogram_raw)
            title = "Raw MPV vs Sensor"
            MPVHistogram_raw.SetTitle(title)
            MPVHistogram_raw.SetName("MPV_raw")
            title2 = "Raw FWHM vs Sensor"
            FWHMHistogram_raw.SetTitle(title2)
            FWHMHistogram_raw.SetName("FWHM_raw")
            self.drawHistos(on_canvas,MPVHistogram_raw,FWHMHistogram_raw)
          self.drawHistos(on_canvas,MPVHistogram_raw,FWHMHistogram_raw)
      if Plots == 0:
        self.PlotMPVFWHM(self.filename,Corrected,Plots,MPVHistogram,FWHMHistogram)

        if MPVHistogram :
          on_canvas.cd(1)
          MPVHistogram.SetXTitle("Sensor Number")
          MPVHistogram.SetYTitle("MPV (ADC counts)")
          MPVHistogram.SetMarkerColor(kRed)
          MPVHistogram.SetMarkerStyle(20)
          MPVHistogram.Draw("P")
        else:
          if(self.debug): print 'No histogram found !'
        if FWHMHistogram :
          on_canvas.cd(2)
          FWHMHistogram.SetXTitle("Sensor Number")
          FWHMHistogram.SetYTitle("FWHM (ADC counts)")
          FWHMHistogram.SetMarkerColor(kBlue)
          FWHMHistogram.SetMarkerStyle(20)
          FWHMHistogram.Draw("P")
        else:
          if(self.debug): print 'No histogram found !'
        on_canvas.Update()
## ============================================================================
    def getHistMean(self,Corrected,Plots):
      all = [0,100]
      r = [0,55]
      phi = [55,110]
      global All_mpv
      All_mpv = Double()
      All_mpv = 22.#if Plots is 0:
      no = All_mpv
      print All_mpv
      return All_mpv#,R_mpv,Phi_mpv
## ============================================================================
    def draw_Landau_page( self,on_canvas,Sensor,Corrected ):
      define_Landau_style()
      if(self.debug): print "in draw_landau_page"
      if Corrected is 0:
        self.SenPath=EXPERT_MONI_DIR + CORRECTED_ADC +"adc_cluster_sen_%03d"%(Sensor)
      elif Corrected is 1:
        self.SenPath=EXPERT_MONI_DIR + NON_CORRECTED_ADC + "adc_cluster_sen_%03d"%(Sensor)
      elif Corrected is 2:
        self.SenPath=EXPERT_MONI_DIR + RAW_ADC + "adc_cluster_sen_%03d"%(Sensor)
      else:
        if(self.debug): print "You need to set the type of data to run over"
        if(self.debug): print "   0 = non angle corrected ontrack data"
        if(self.debug): print "   1 = angle corrected ontrack data"
        if(self.debug): print "   2 = raw clusters data"
        return
      on_canvas.cd()
      file = TFile( '%s' %self.filename)
      if(self.debug): print file
      if (file):
        self.ADC_plot = file.Get(self.SenPath)
        if(self.ADC_plot):
          title = "Cluster ADCs for Sensor %d" %Sensor
          self.ADC_plot.SetTitle(title)
          self.ADC_plot.Draw()
          on_canvas.Update()
          raw_input("return to quit")
        else:
          if(self.debug): print "Histogram could not be found"
      else:
        if(self.debug): print "File was not found"
# ============================================================================
    def print_details( self ):
      print 'Running \"drawClustersMPVFWHMMoni.py":'
      print '  - filename: %s' % self.filename
      print '  - sub-dir : %s' % self.subdir
# ============================================================================
    def FitLandau(self,Sensor,Corrected):
      define_Landau_style()
      if (self.filename):
        if(self.debug): print "FitLandau: file name is good"
        canvasSensorLandau.cd()
        if Corrected is 0:
          self.SenPath=EXPERT_MONI_DIR + CORRECTED_ADC +"adc_cluster_sen_%03d"%(Sensor)
        elif Corrected is 1:
          self.SenPath=EXPERT_MONI_DIR + NON_CORRECTED_ADC + "adc_cluster_sen_%03d"%(Sensor)
        elif Corrected is 2:
          self.SenPath=EXPERT_MONI_DIR + RAW_ADC + "adc_cluster_sen_%03d"%(Sensor)
        else:
          if(self.debug): print "You need to set the type of data to run over"
          if(self.debug): print "   0 = non angle corrected ontrack data"
          if(self.debug): print "   1 = angle corrected ontrack data"
          if(self.debug): print "   2 = raw clusters data"
          return


        if (self.LandauFile):

          self.ADC_plot = (self.LandauFile.Get(self.SenPath))
          if (self.ADC_plot):
            title = "Cluster ADCs for Sensor %d" %Sensor
            name = "Sens_%d" %Sensor
            if SNoise:
              self.SN_plot.SetName(name)
              self.SN_plot.SetTitle(title)
              self.SN_plot.SetTitleSize(5.2)
              if self.SN_plot.GetXaxis():
                self.SN_plot.GetXaxis().SetTitle("Cluster SN Counts")
                self.SN_plot.GetYaxis().SetTitle("Entries")
              # self.SN_plot.SetLineColor()
              self.SN_plot.Draw()
              self.SN_plot.SetMarkerStyle(25)
              self.SN_plot.SetMarkerSize(0.3)
              self.SN_plot.SetMarkerColor(kBlue)
              self.SN_plot.Draw("Same")
              canvasSensorLandau.Update()

            self.ADC_plot.SetTitle(title)
            self.ADC_plot.SetTitleSize(5.2)
            if self.ADC_plot.GetXaxis():
              self.ADC_plot.GetXaxis().SetTitle("Cluster ADC Counts")
              self.ADC_plot.GetYaxis().SetTitle("Entries")
            # self.ADC_plot.SetLineColor()
            self.ADC_plot.Draw()
            self.ADC_plot.SetMarkerStyle(25)
            self.ADC_plot.SetMarkerSize(0.3)
            self.ADC_plot.SetMarkerColor(kBlue)
            self.ADC_plot.Draw("Same")
            canvasSensorLandau.Update()
          else:
            if(self.debug): print "Histogram could not be found"
        else:
          if(self.debug): print "File was not found"
      else:
        if(self.debug): print "The File has no name"
# ============================================================================
    def draw_Landau_gui_page( self ,Sensor,Corrected,RefFile=None):
      SNoise=False
      if (self.debug):
        print "draw landau histogram"
      define_Landau_style()
      if (self.filename):
        if(self.debug): print "file name is good"
        self.canvasSensorLandau.cd()
        if Corrected is 0:
          self.SenPath=EXPERT_MONI_DIR + CORRECTED_ADC +"adc_cluster_sen_%03d"%(Sensor)
        elif Corrected is 1:
          self.SenPath=EXPERT_MONI_DIR + NON_CORRECTED_ADC + "adc_cluster_sen_%03d"%(Sensor)
        elif Corrected is 2:
          self.SenPath=EXPERT_MONI_DIR + RAW_ADC + "adc_cluster_sen_%03d"%(Sensor)
        else:
          if(self.debug): print "You need to set the type of data to run over"
          if(self.debug): print "   0 = non angle corrected ontrack data"
          if(self.debug): print "   1 = angle corrected ontrack data"
          if(self.debug): print "   2 = raw clusters data"
          return
        plot = self.get_landau(self.SenPath)
        if RefFile and RefFile != "":
            refplot = self.get_reflandau(RefFile, self.SenPath)
        else:
            refplot = None
        if plot:
          self.ADC_plot = plot#(self.LandauFile.Get(self.SenPath))
          self.ADC_ref_plot = refplot
          if (self.ADC_plot):
            title = "Cluster ADCs for Sensor %d" %Sensor
            name = "Sens_%d" %Sensor
            if SNoise:
              noise = 1/2.5#1/self.SignalNoise(Sensor)
              self.SN_plot = times(self.ADC_plot,noise)
              define_Landau_style()
              self.SN_plot.SetName(name)
              self.SN_plot.SetTitle(title)
              self.SN_plot.SetTitleSize(5.2)
              if SN_plot.GetXaxis():
                self.SN_plot.GetXaxis().SetTitle("Cluster SN Counts")
                self.SN_plot.GetYaxis().SetTitle("Entries")
              # self.SN_plot.SetLineColor()
              self.SN_plot.Draw()
              self.SN_plot.SetMarkerStyle(25)
              self.SN_plot.SetMarkerSize(0.3)
              self.SN_plot.SetMarkerColor(kBlue)
              self.SN_plot.Draw("Same")
              self.canvasSensorLandau.Update()
            else:
              self.ADC_plot.SetTitle(title)
              self.ADC_plot.SetTitleSize(5.2)
              if self.ADC_plot.GetXaxis():
                self.ADC_plot.GetXaxis().SetLabelSize(0.08)
                self.ADC_plot.GetYaxis().SetLabelSize(0.08)
                self.ADC_plot.GetXaxis().SetTitle("Cluster ADC Counts")
                self.ADC_plot.GetYaxis().SetTitle("Entries")
              # self.ADC_plot.SetLineColor()
              self.ADC_plot.SetMarkerStyle(25)
              self.ADC_plot.SetMarkerSize(0.3)
              self.ADC_plot.SetMarkerColor(kBlue)
              self.ADC_plot.Draw()
              if self.ADC_ref_plot:
                  self.ADC_ref_plot.SetMarkerStyle(3)
                  self.ADC_ref_plot.SetMarkerSize(0.3)
                  self.ADC_ref_plot.SetLineColor(kGreen-7)
                  self.ADC_ref_plot.SetMarkerColor(kGreen-7)
                  fscale = self.ADC_plot.Integral() / self.ADC_ref_plot.Integral()
                  self.ADC_ref_plot.Scale( fscale )
                  self.ADC_ref_plot.Draw("same")
              if Corrected is 0:
                if ontr_fit.has_key(Sensor) and ontr_fit[Sensor]:
                  self.ADC_plot.GetListOfFunctions().Add(ontr_fit[Sensor].Clone())
                  ontr_fit[Sensor].Draw("sames")
              elif Corrected is 1:
                if corr_fit.has_key(Sensor) and corr_fit[Sensor]:
                  self.ADC_plot.GetListOfFunctions().Add(corr_fit[Sensor].Clone())
                  corr_fit[Sensor].Draw("SAMES")
              elif Corrected is 2:
                if raw_fit.has_key(Sensor) and raw_fit[Sensor]:
                  self.ADC_plot.GetListOfFunctions().Add(raw_fit[Sensor].Clone())
                  raw_fit[Sensor].Draw("SAMES")
              self.canvasSensorLandau.Update()
          else:
            if(self.debug): print "Histogram could not be found"
        else:
          if(self.debug): print "File was not found"
      else:
        if(self.debug): print "The File has no name"
## =============================================================================
    def SignalNoise(self,Sensor,CMS=True):
      file = TFile( self.filename )
      sens = VeloSensor(file , Sensor)
      if sens.initializeSensor(file,Sensor):
        rms = sens.getRMSNoiseSensor(CMS,0)
        file.Close()
        if rms == 0.0:
          return 1.0
        else:
          return rms
      else:
        file.Close()
        return 1.0
# ============================================================================
    def Reset_All(self):
      #reset_Landau()\
      if (self.debug): print "Reset All"
      self.reset_Raw()
      self.reset_Ontrack()
      self.reset_Corrected()
    def reset_Raw(self):
      if (self.debug):print "Reset Raw MPV+FWHM hist"
      MPVHistogram_raw.Reset()
      FWHMHistogram_raw.Reset()
      MPVHistogramRef_raw.Reset()
      FWHMHistogramRef_raw.Reset()
      raw_fit.clear()
    def reset_Ontrack(self):
      if (self.debug):print "Reset Ontrack MPV+FWHM hist"
      MPVHistogram_ontrack.Reset()
      FWHMHistogram_ontrack.Reset()
      MPVHistogramRef_ontrack.Reset()
      FWHMHistogramRef_ontrack.Reset()
      ontr_fit.clear()
    def reset_Corrected(self):
      if (self.debug): print "Reset Corrected MPV+FWHM hist"
      MPVHistogram_corrected.Reset()
      FWHMHistogram_corrected.Reset()
      MPVHistogramRef_corrected.Reset()
      FWHMHistogramRef_corrected.Reset()
      corr_fit.clear()
# ============================================================================
    def draw_Ref_Hists(self,canvas,Hist1,Hist2):
        canvas.cd(1)
        Hist1.SetMarkerColor(kRed-7)
        Hist1.SetMarkerStyle(20)
        Hist1.Draw("P same")
        canvas.cd(2)
        Hist2.SetMarkerColor(kBlue-7)
        Hist2.SetMarkerStyle(20)
        Hist2.Draw("P same")

# ============================================================================
    def get_MPVFWHM_histograms(self, Corrected, Plots):
        mpv = None
        fwhm = None
        if Plots == 1: # Plots=1: Fit landau
          if Corrected ==0:
            if MPVHistogram_ontrack.GetEntries() < 1:
              self.PlotMPVFWHM(self.filename,Corrected,Plots, MPVHistogram_ontrack, FWHMHistogram_ontrack, ontr_fit)
            mpv = MPVHistogram_ontrack
            fwhm = FWHMHistogram_ontrack
          elif Corrected == 1:
            if MPVHistogram_corrected.GetEntries() < 1:
              self.PlotMPVFWHM(self.filename, Corrected, Plots, MPVHistogram_corrected, FWHMHistogram_corrected, corr_fit)
            mpv = MPVHistogram_corrected
            fwhm = FWHMHistogram_corrected
          else:
            if MPVHistogram_raw.GetEntries() < 1:
              self.PlotMPVFWHM(self.filename,Corrected,Plots, MPVHistogram_raw, FWHMHistogram_raw, raw_fit)
            mpv = MPVHistogram_raw
            fwhm = FWHMHistogram_raw              
        elif Plots==0: # Plots=0: Fast MPV/FWHM
          self.PlotMPVFWHM(self.filename, Corrected, Plots, MPVHistogram, FWHMHistogram)
          mpv = MPVHistogram
          fwhm = FWHMHistogram
        return mpv, fwhm      
# ============================================================================
    def get_RefMPVFWHM_histograms( self , RefFileName,Corrected, Plots) : 
        MPV = None
        FWHM = None
         # Prepare reference fits if necessary
        if Plots ==1:
          if Corrected == 0:
            MPV = self.get_refplots(RefFileName,"MPV_ontrack")
            FWHM = self.get_refplots(RefFileName,"FWHM_ontrack")
            if not MPV or MPV_ontrack.GetEntries() < 1:
                MPV = MPVHistogramRef_ontrack
                FWHM = FWHMHistogramRef_ontrack
                if not MPV or MPV.GetEntries() < 1:
                    self.PlotMPVFWHM(RefFileName,Corrected,Plots, MPV, FWHM)
              
          elif Corrected ==1:
            MPV = self.get_refplots(RefFileName,"MPV_corrected")
            FWHM = self.get_refplots(RefFileName,"FWHM_corrected")
            if not MPV or MPV.GetEntries() < 1:
                MPV = MPVHistogramRef_corrected
                FWHM = FWHMHistogramRef_corrected
                if not MPV or MPV.GetEntries() < 1:
                    self.PlotMPVFWHM(RefFileName,Corrected,Plots, MPV, FWHM)
              
          elif Corrected ==2:
            MPV = self.get_refplots(RefFileName,"MPV_raw")
            FWHM = self.get_refplots(RefFileName,"FWHM_raw")
            if not MPV or MPV.GetEntries() < 1:
                MPV = MPVHistogramRef_ontrack
                FWHM = FWHMHistogramRef_ontrack
                if not MPV or MPV.GetEntries() < 1:
                    self.PlotMPVFWHM(RefFileName,Corrected,Plots, MPV, FWHM)

        elif Plots==0:
          MPV = MPVHistogramRef
          FWHM = FWHMHistogramRef
          self.PlotMPVFWHM(RefFileName,Corrected,Plots,MPV,FWHM)
        return MPV, FWHM
# ============================================================================
    def histo_projection( self , hist, rphi, pmin, pmax, CompressOverflow=False ): 
        """ Get a histogram of the bin contents of hist, use 'R' or 'Phi' """
        name = hist.GetName() + "_projy_" + rphi
        title = hist.GetTitle()
        # Create bins with integer values
        xmin = int( pmin ) # round down
        xmax = int( pmax + 1 ) # round up
        nbins = xmax - xmin
        proj = TH1D( name, title, nbins, xmin - 0.5, xmax - 0.5 )

        MaxSensor = 110

        for sens in xrange(0,MaxSensor):
            if rphi == "R" and helperTools.isSensorRType(sens):
                val = hist.GetBinContent(sens+1)
                if CompressOverflow and val < xmin:
                    proj.Fill(xmin)
                if CompressOverflow and val > xmax - 1:
                    proj.Fill(xmax)
                else:
                    proj.Fill(val)
            elif rphi == "Phi" and helperTools.isSensorPhiType(sens):
                val = hist.GetBinContent(sens+1)
                if CompressOverflow and val < xmin:
                    proj.Fill(xmin)
                if CompressOverflow and val > xmax - 1:
                    proj.Fill(xmax - 1)
                else:
                    proj.Fill(val)

        return proj
# ============================================================================
    def draw_RefMPVFWHM_gui_page( self , RefFileName,Corrected, Plots=0,SN = False) : 
        define_MPVFWHM_style()
        # Prepare reference fits if necessary
        mpv, fwhm = self.get_MPVFWHM_histograms(Corrected, Plots)
        mpvref, fwhmref = self.get_RefMPVFWHM_histograms(RefFileName, Corrected, Plots)
         
        self.canvasMPVFWHM.cd()
        
        # Plot the reference
        if SN :
            self.PlotSN(self.canvasMPVFWHM, mpvref, fwhmref)
            self.PlotSNRef(self.canvasMPVFWHM, mpv, fwhm)
        else:
            self.drawHistos(self.canvasMPVFWHM, mpv, fwhm)
            self.draw_Ref_Hists(self.canvasMPVFWHM, mpvref, fwhmref)

        self.canvasMPVFWHM.Update()

# ============================================================================
    def draw_MPVFWHM_gui_page( self , Corrected, Plots=0, SN=False) :
        """Draws one canvas with MPV and FWHM histograms.
        Note: only to be used in the GUI, or from C++ !"""
        define_MPVFWHM_style()
        if(self.debug): print "file name is good"
        
        mpv, fwhm = self.get_MPVFWHM_histograms(Corrected, Plots)
        
        if SN:
          self.PlotSN(self.canvasMPVFWHM, mpv, fwhm)
        else:
          self.drawHistos(self.canvasMPVFWHM, mpv, fwhm)
        
        self.canvasMPVFWHM.Update()
        return
# ============================================================================
    def draw_DiffMPVFWHM_gui_page( self , RefFileName, Corrected, Plots=0 ) :
        """Draws one canvas with the difference between main and 
        reference histograms. Displays the mean diff as a horizontal line
        """
        define_MPVFWHM_style()
        
        # Maximum sensor number to process for diff
        MaxSens = 110
        
        # Fill histograms for reference and main root file
        mpv, fwhm = self.get_MPVFWHM_histograms(Corrected, Plots)
        mpv_ref, fwhm_ref = self.get_RefMPVFWHM_histograms(RefFileName, Corrected, Plots)
        
        global MPVGraphDiff,FWHMGraphDiff
        MPVGraphDiff = TGraph(120)
        FWHMGraphDiff = TGraph(120)
        
        corr_title = ["Ontrack",
                      "Corrected",
                      "Raw"]
        MPVGraphDiff.SetTitle("%s MPV Vs Sensor, diff (this-reference)" % (corr_title[Corrected]))
        FWHMGraphDiff.SetTitle("%s FWHM Vs Sensor, diff (this-reference)" % (corr_title[Corrected]))
        
        for sens in xrange(0,MaxSens):
            if helperTools.isSensorRType(sens) or helperTools.isSensorPhiType(sens):
                dmpv = mpv.GetBinContent(sens+1) - mpv_ref.GetBinContent(sens+1)
                dfwhm = fwhm.GetBinContent(sens+1) - fwhm_ref.GetBinContent(sens+1)
                MPVGraphDiff.SetPoint(sens, sens, dmpv)
                FWHMGraphDiff.SetPoint(sens, sens, dfwhm)
        
        self.drawDiffGraphs(self.canvasMPVFWHM, MPVGraphDiff, FWHMGraphDiff )
        self.canvasMPVFWHM.Update()
        return
# ============================================================================
    def draw_projection_gui_page( self, Corrected, RefOpt, RefFileName ) :
        """Draws a projection of the ADC count for MPV & FWHM """
        if self.debug:
            print "draw_projection_gui_page() called"

        self.canvasMPVFWHM.Clear()
        define_MPVFWHM_style()
        
        # Fixed axis range for each histogram
        # MPV limits
        mmin, mmax = 30, 50
        # FWHM limits 
        fmin, fmax = 5, 29
        # Difference MPV limits
        dmmin, dmmax = -10, 10
        dfmin, dfmax = -10, 10


        # Keep the objects in a global so they are not destructed when we return
        global ProjMPVR, ProjMPVPhi, ProjFWHMR, ProjFWHMPhi
        ProjMPVR = ProjMPVPhi = ProjFWHMR = ProjFWHMPhi = None
        
        # lists for projected histograms: can't use THStack when displaying 
        # stats (?)
        ProjStackMPV = []
        ProjStackFWHM = []

        global mpvlegend, fwhmlegend
        mpvlegend = TLegend(0.75,0.80,0.95,0.95)
        mpvlegend.SetFillColor(kWhite)
        fwhmlegend = TLegend(0.75,0.80,0.95,0.95)
        fwhmlegend.SetFillColor(kWhite)

        # Generate plots
        mpv, fwhm = self.get_MPVFWHM_histograms(Corrected, 0)

        if RefOpt == 0: # no reference
            if self.debug:
                print " - Plotting projection with no reference"
            # Add R and phi
            # ** MPV **
            ProjMPVR = self.histo_projection(mpv, "R", mmin, mmax, True)
            ProjMPVR.SetLineColor(kMagenta)
            ProjMPVR.SetMarkerColor(kMagenta)
            ProjMPVR.SetLineWidth(4)
            ProjStackMPV.append(ProjMPVR)
            mpvlegend.AddEntry(ProjMPVR, "R")
            ProjMPVPhi = self.histo_projection(mpv, "Phi", mmin, mmax, True)
            ProjMPVPhi.SetLineColor(kGreen)
            ProjMPVPhi.SetMarkerColor(kGreen)
            ProjMPVPhi.SetLineWidth(4)
            ProjStackMPV.append(ProjMPVPhi)
            mpvlegend.AddEntry(ProjMPVPhi, "#Phi")
            # ** FWHM **
            ProjFWHMR = self.histo_projection(fwhm, "R", fmin, fmax, True)
            ProjFWHMR.SetLineColor(kMagenta)
            ProjFWHMR.SetMarkerColor(kMagenta)
            ProjFWHMR.SetLineWidth(4)
            ProjStackFWHM.append(ProjFWHMR)
            fwhmlegend.AddEntry(ProjFWHMR, "R")
            ProjFWHMPhi = self.histo_projection(fwhm, "Phi", fmin, fmax, True)
            ProjFWHMPhi.SetLineColor(kGreen)
            ProjFWHMPhi.SetMarkerColor(kGreen)
            ProjFWHMPhi.SetLineWidth(4)
            ProjStackFWHM.append(ProjFWHMPhi)
            fwhmlegend.AddEntry(ProjFWHMPhi, "#Phi")
        elif RefOpt == 1 or RefOpt == 2: # use reference
            mpv_ref, fwhm_ref =\
                self.get_RefMPVFWHM_histograms(RefFileName, Corrected, 0)

            if RefOpt == 1: # display main file and reference
                if self.debug:
                    print " - Plotting projection with main file and reference"
                # Make the globals arrays, to store main and reference histogram
                ProjMPVR = [None, None]
                ProjMPVPhi = [None, None]

                ProjFWHMR = [None, None]
                ProjFWHMPhi = [None, None]
                mpvlegend.SetY1(0.5)
                fwhmlegend.SetY1(0.5)

                # ** MPV and FWHM main file  R and Phi + ref R and Phi**
                for histr, histphi, stack, graph, graph_ref, legend, limits in \
                        zip([ProjMPVR, ProjFWHMR], 
                            [ProjMPVPhi, ProjFWHMPhi],
                            [ProjStackMPV, ProjStackFWHM], 
                            [mpv, fwhm],
                            [mpv_ref, fwhm_ref],
                            [mpvlegend, fwhmlegend],
                            [(mmin,mmax), (fmin,fmax)] ):
                    smin,smax = limits
                    histr[0] = self.histo_projection(graph, "R", smin,smax, True)
                    histr[0].SetLineWidth(4)
                    histr[0].SetLineColor(kMagenta)
                    histr[0].SetMarkerColor(kMagenta)
                    stack.append(histr[0])
                    legend.AddEntry(histr[0], "R")
                    histphi[0] = self.histo_projection(graph, "Phi", smin,smax, True)
                    histphi[0].SetLineWidth(4)
                    histphi[0].SetLineColor(kGreen)
                    histphi[0].SetMarkerColor(kGreen)
                    stack.append(histphi[0])
                    legend.AddEntry(histphi[0], "#Phi")


                    histr[1] = self.histo_projection(graph_ref, "R", smin, smax, True)
                    histr[1].SetLineWidth(2)
                    histr[1].SetLineStyle(kDashed)
                    histr[1].SetLineColor(kMagenta-3)
                    histr[1].SetMarkerColor(kMagenta-3)
                    rint = max(histr[0].Integral(), 1)
                    histr[1].Scale(histr[1].Integral()/rint)
                    stack.append(histr[1])
                    legend.AddEntry(histr[1], "ref R")
                    histphi[1] = self.histo_projection(graph_ref, "Phi", smin, smax, True)
                    histphi[1].SetLineWidth(2)
                    histphi[1].SetLineStyle(kDashed)
                    histphi[1].SetLineColor(kGreen-3)
                    histphi[1].SetMarkerColor(kGreen-3)
                    phiint = max(histphi[0].Integral(), 1.0)
                    histphi[1].Scale(histphi[1].Integral()/phiint)
                    stack.append(histphi[1])
                    legend.AddEntry(histphi[1], "ref #Phi")


            else: # display difference
                if self.debug:
                    print " - Plotting difference between main file and reference"
                # ** MPV ** 
                mpv_diff = mpv.Clone("mpv_diff")
                mpv_diff.Add( mpv_ref, -1 )
                ProjMPVR = self.histo_projection(mpv_diff, "R", dmmin, dmmax, True)
                ProjMPVR.SetLineColor(kMagenta)
                ProjMPVR.SetMarkerColor(kMagenta)
                ProjMPVR.SetLineWidth(4)
                ProjStackMPV.append(ProjMPVR)
                mpvlegend.AddEntry(ProjMPVR, "R")
                ProjMPVPhi = self.histo_projection(mpv_diff, "Phi", dmmin, dmmax, True)
                ProjMPVPhi.SetLineColor(kGreen)
                ProjMPVPhi.SetMarkerColor(kGreen)
                ProjMPVPhi.SetLineWidth(4)
                ProjStackMPV.append(ProjMPVPhi)
                mpvlegend.AddEntry(ProjMPVPhi, "#Phi")
                # ** FWHM **
                fwhm_diff = fwhm.Clone("fwhm_diff")
                fwhm_diff.Add( fwhm_ref, -1 )
                ProjFWHMR = self.histo_projection(fwhm_diff, "R", dfmin, dfmax, True)
                ProjFWHMR.SetLineColor(kMagenta)
                ProjFWHMR.SetMarkerColor(kMagenta)
                ProjFWHMR.SetLineWidth(4)
                ProjStackFWHM.append(ProjFWHMR)
                fwhmlegend.AddEntry(ProjFWHMR, "R")
                ProjFWHMPhi = self.histo_projection(fwhm_diff, "Phi", dfmin, dfmax, True)
                ProjFWHMPhi.SetLineColor(kGreen)
                ProjFWHMPhi.SetMarkerColor(kGreen)
                ProjFWHMPhi.SetLineWidth(4)
                ProjStackFWHM.append(ProjFWHMPhi)
                fwhmlegend.AddEntry(ProjFWHMPhi, "#Phi")


        # Draw
        self.canvasMPVFWHM.Divide(2,1)
        self.canvasMPVFWHM.cd(1)
        gStyle.SetOptStat(1100)
        ymin, ymax = get_histo_min_max( ProjStackMPV, 4, False )
        ProjStackMPV[0].SetTitle("MPV")
        ProjStackMPV[0].Draw()
        ProjStackMPV[0].GetYaxis().SetRangeUser(0.0,ymax)
        ProjStackMPV[0].GetXaxis().SetTitle("MPV [ADC counts]")
        ProjStackMPV[1].Draw("sames")
        # 0:R 1:Phi 2:ref-R 3:Ref-Phi -- only stats for R & Phi
        for h in ProjStackMPV[2:4]:
            h.Draw("same")
        mpvlegend.Draw()
        gPad.Update()

        self.canvasMPVFWHM.cd(2)
        ymin, ymax = get_histo_min_max( ProjStackFWHM, 4, False )
        ProjStackFWHM[0].SetTitle("FWHM")
        ProjStackFWHM[0].Draw()
        ProjStackFWHM[0].GetYaxis().SetRangeUser(0.0,ymax)
        ProjStackFWHM[0].GetXaxis().SetTitle("FWHM [ADC counts]")
        ProjStackFWHM[1].Draw("sames")
        for h in ProjStackFWHM[2:4]:
            h.Draw("same")

        fwhmlegend.Draw()
        gPad.Update()

        # Display stats boxes for mean. note: non-ref hists are at index 0, 1
        pad = [1,2]
        hists = [ProjStackMPV, ProjStackFWHM]
        for npad, hist in zip( pad, hists ):
            posy = [0.7, 0.5]
            typ = ["R", "#Phi"]
            self.canvasMPVFWHM.cd(npad)
            for i, theposy, styp in zip([0,1], posy, typ):
                stats = hist[i].FindObject("stats")
                #stats.SetLabel(styp)
                stats.SetX1NDC(0.11)
                stats.SetX2NDC(0.25)
                stats.SetY1NDC(theposy)
                stats.SetY2NDC(theposy+0.15)
                stats.SetLineColor(hist[i].GetLineColor())
                stats.Draw()
        
        self.canvasMPVFWHM.Update()


# ============================================================================

    
     
    def drawSummaryPlots(self):
        global sumfile
        sumfile = TFile(self.filename)
        phi = sumfile.Get("Velo/VeloExpertClusterMonitor/ontrack/adc_cluster_phi")
        r = sumfile.Get("Velo/VeloExpertClusterMonitor/ontrack/adc_cluster_R")
        r_clust = ClusterFitter()
        phi_clust = ClusterFitter()
        global canvasMPVFWHM
        canvasMPVFWHM = TCanvas("summary", "Summary plots")
        canvasMPVFWHM.cd()
        canvasMPVFWHM.Clear()
        canvasMPVFWHM.Divide(2,1)
        r_FWHM = Double()
        r_MPV = Double()
        phi_FWHM = Double()
        phi_MPV = Double()
        canvasMPVFWHM.cd(1)
        r_fit = r_clust.langausData(r,r_MPV,r_FWHM,0.2)
        mpv_r = r_fit.GetParameter(1)
        fwhm_r = r_fit.GetParameter(0)
        print mpv_r, "   ",fwhm_r
        canvasMPVFWHM.cd(2)
        phi_fit = phi_clust.langausData(phi,phi_MPV,phi_FWHM,0.2)
        mpv_phi = phi_fit.GetParameter(1)
        fwhm_phi = phi_fit.GetParameter(0)
        print mpv_phi, "    ", fwhm_phi
        canvasMPVFWHM.Update()
    def saveReferencePlots(self) :
        #RefMPVFWHMMoni("big.root",EXPERT_MONI_DIR)
        RefFileName = "Ref"+self.filename
        docopy = "cp " + self.filename +" " +RefFileName
        os.system(docopy)
        RefFile = TFile(RefFileName,"update")
        dir = RefFile.GetDirectory("/Velo/VeloExpertClusterMonitor")
        dir.cd()
        dir.mkdir("Reference")
        RefDir = RefFile.GetDirectory("Velo/VeloExpertClusterMonitor/Reference")
        RefDir.cd()
        MPVHistogram_ontrack.Write()
        FWHMHistogram_ontrack.Write()
        MPVHistogram_corrected.Write()
        FWHMHistogram_corrected.Write()
        MPVHistogram_raw.Write()
        FWHMHistogram_raw.Write()
        RefFile.Close()
# =============================================================================
if __name__ == "__main__":
    if len( argv ) == 2 and ( '--help' == argv[1] or '-h' == argv[1] ):
        MPVFWHMusage()
        exit()

    if len( argv ) > 3:
        print "Too many parameters, some will be ignored"
        MPVFWHMusage()

    # set histograms file name, overwriting it if passed as an argument
    if len( argv ) > 1:
        EXPERT_MONI_FILENAME = argv[1]

    # set histograms directory, overwriting it if passed as an argument
    if len( argv ) > 2:
        EXPERT_MONI_DIR = argv[2]

    define_MPVFWHM_style()

    def drawClustersMPVFWHMMoni( filename=EXPERT_MONI_FILENAME, subdir=EXPERT_MONI_DIR ,corrected=0,Plots=0) :
      """Main function to draw 2 pages of cluster-related monitoring
         when running this script in standalone.
      """
      return _drawClustersMPVFWHMMoni( filename, subdir,corrected ,Plots)

# =============================================================================
