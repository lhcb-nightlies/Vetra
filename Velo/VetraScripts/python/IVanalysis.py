#!/usr/bin/env python
import os, sys, string, subprocess
pyrootpath = os.environ[ 'ROOTSYS' ]

if os.path.exists( pyrootpath):
    sys.path.append( pyrootpath + os.sep + 'bin')
    sys.path.append( pyrootpath + os.sep + 'lib')

from ROOT import *
import ROOT
import ConfigParser
from array import array
from math import *
import math
import runDBAccess
import datetime
import operator

import itertools

def map_sensor(ma, ch):
    sensormap = 'VL00XX'
    number=-1.0
    if (ma==0):
        if (ch==0):
             sensormap='PU01AB'
             number=0
        elif (ch==1):
             sensormap='PU02AT'
             number=1
        elif (ch==2):
             sensormap='VL01AT'
             number=2
        elif (ch==3):
             sensormap='VL01AB'
             number=3
        elif (ch==4):
             sensormap='VL02AT'
             number=4
        elif (ch==5):
             sensormap='VL02AB'
             number=5
        elif (ch==6):
             sensormap='VL03AT'
             number=6
        elif (ch==7):
            number=7
            sensormap='VL03AB'
    elif (ma==1):
        if (ch==0):
            number=8
            sensormap='VL04AT'
        elif (ch==1):
            number=9
            sensormap='VL04AB'
        elif (ch==2):
            number=10
            sensormap='VL05AT'
        elif (ch==3):
            number=11
            sensormap='VL05AB'
        elif (ch==4):
            number=12
            sensormap='VL06AT'
        elif (ch==5):
            number=13
            sensormap='VL06AB'
        elif (ch==6):
            number=14
            sensormap='VL07AT'
        elif (ch==7):
            number=15
            sensormap='VL07AB'
    elif (ma==2):
        if (ch==0):
            number=16
            sensormap='VL08AT'
        elif (ch==1):
            number=17
            sensormap='VL08AB'
        elif (ch==2):
            number=18
            sensormap='VL09AT'
        elif (ch==3):
            number=19
            sensormap='VL09AB'
        elif (ch==4):
            number=20
            sensormap='VL10AT'
        elif (ch==5):
            number=21
            sensormap='VL10AB'
        elif (ch==6):
            number=22
            sensormap='VL11AT'
        elif (ch==7):
            number=23
            sensormap='VL11AB'
    elif (ma==3):
        if (ch==0):
            number=24
            sensormap='VL12AT'
        elif (ch==1):
            number=25
            sensormap='VL12AB'
        elif (ch==2):
            number=26
            sensormap='VL13AT'
        elif (ch==3):
            number=27
            sensormap='VL13AB'
        elif (ch==4):
            number=28
            sensormap='VL14AT'
        elif (ch==5):
            number=29
            sensormap='VL14AB'
        elif (ch==6):
            number=30
            sensormap='VL15AT'
        elif (ch==7):
            number=31
            sensormap='VL15AB'
    elif (ma==4):
        if (ch==0):
            number=32
            sensormap='VL16AT'
        elif (ch==1):
            number=33
            sensormap='VL16AB'
        elif (ch==2):
            number=34
            sensormap='VL19AT'
        elif (ch==3):
            number=35
            sensormap='VL19AB'
        elif (ch==4):
            number=36
            sensormap='VL22AT'
        elif (ch==5):
            number=37
            sensormap='VL22AB'
        elif (ch==6):
            number=38
            sensormap='VL23AT'
        elif (ch==7):
            number=39
            sensormap='VL23AB'
    elif (ma==5):
        if (ch==0):
            number=40
            sensormap='VL24AT'
        elif (ch==1):
            number=41
            sensormap='VL24AB'
        elif (ch==2):
            number=42
            sensormap='VL25AT'
        elif (ch==3):
            number=43
            sensormap='VL25AB'
    elif (ma==6):
        if (ch==0):
            number=44
            sensormap='PU01CT'
        elif (ch==1):
            number=45
            sensormap='PU02CB'
        elif (ch==2):
            number=46
            sensormap='VL01CT'
        elif (ch==3):
            number=47
            sensormap='VL01CB'
        elif (ch==4):
            number=48
            sensormap='VL02CT'
        elif (ch==5):
            number=49
            sensormap='VL02CB'
        elif (ch==6):
            number=50
            sensormap='VL03CT'
        elif (ch==7):
            number=51
            sensormap='VL03CB'
    elif (ma==7):
        if (ch==0):
            number=52
            sensormap='VL04CT'
        elif (ch==1):
            number=53
            sensormap='VL04CB'
        elif (ch==2):
            number=54
            sensormap='VL05CT'
        elif (ch==3):
            number=55
            sensormap='VL05CB'
        elif (ch==4):
            number=56
            sensormap='VL06CT'
        elif (ch==5):
            number=57
            sensormap='VL06CB'
        elif (ch==6):
            number=58
            sensormap='VL07CT'
        elif (ch==7):
            number=59
            sensormap='VL07CB'
    elif (ma==8):
        if (ch==0):
            number=60
            sensormap='VL08CT'
        elif (ch==1):
            number=61
            sensormap='VL08CB'
        elif (ch==2):
            number=62
            sensormap='VL09CT'
        elif (ch==3):
            number=63
            sensormap='VL09CB'
        elif (ch==4):
            number=64
            sensormap='VL10CT'
        elif (ch==5):
            number=65
            sensormap='VL10CB'
        elif (ch==6):
            number=66
            sensormap='VL11CT'
        elif (ch==7):
            number=67
            sensormap='VL11CB'
    elif (ma==9):
        if (ch==0):
            number=68
            sensormap='VL12CT'
        elif (ch==1):
            number=69
            sensormap='VL12CB'
        elif (ch==2):
            number=70
            sensormap='VL13CT'
        elif (ch==3):
            number=71
            sensormap='VL13CB'
        elif (ch==4):
            number=72
            sensormap='VL14CT'
        elif (ch==5):
            number=73
            sensormap='VL14CB'
        elif (ch==6):
            number=74
            sensormap='VL15CT'
        elif (ch==7):
            number=75
            sensormap='VL15CB'
    elif (ma==10):
        if (ch==0):
            number=76
            sensormap='VL16CT'
        elif (ch==1):
            number=77
            sensormap='VL16CB'
        elif (ch==2):
            number=78
            sensormap='VL19CT'
        elif (ch==3):
            number=79
            sensormap='VL19CB'
        elif (ch==4):
            number=80
            sensormap='VL22CT'
        elif (ch==5):
            number=81
            sensormap='VL22CB'
        elif (ch==6):
            number=82
            sensormap='VL23CT'
        elif (ch==7):
            number=83
            sensormap='VL23CB'
    elif (ma==11):
        if (ch==0):
            number=84
            sensormap='VL24CT'
        elif (ch==1):
            number=85
            sensormap='VL24CB'
        elif (ch==2):
            number=86
            sensormap='VL25CT'
        elif (ch==3):
            number=87
            sensormap='VL25CB'
    return number, sensormap

#fills list of currents for each sensor at each voltage
def parse_file(year, month, day, hour, minutes, lumi, ivfile, voltagelist, valuelist, ntc1list, ntc2list, prevdate,intlumi, alumilist, fillno):
    
    vetoed=0
    hfile = open(ivfile)
    fname = hfile.name
    nametokens = fname.split('/')
    dateandtime = (nametokens[5].split('_'))
    year[0] = float("20"+dateandtime[0][0:2])
    month[0] = float(dateandtime[0][2:4])
    day[0] = float(dateandtime[0][4:6])
    hour[0] = float(dateandtime[1][0:2])
    minutes[0] = float(dateandtime[1][2:4])

    ## All arguments for datetime must be integers
    date = datetime.datetime(int(year[0]),int(month[0]),int(day[0]),int(hour[0]),int(minutes[0]),0)

    #check list of lumis from file for the date
    intlumi, fillno = find_lumi(prevdate,date,intlumi,alumilist, fillno)
    lumi[0] = intlumi
    
    #save the lumi to file, skip calculation if value is also in the file
    #lumifile.write(str(date)+" "+str(intlumi)+"\n")
    
    for voltage in voltagelist:
        arrayname = "valuearray"+str(voltage)
        arrayname = array("d",[-10.0]*90)
        temp1 = "ntc1"+str(voltage)
        temp1 = array("d",[-10.0]*90)
        temp2 = "ntc2"+str(voltage)
        temp2 = array("d",[-10.0]*90)
        j=0
        hfile = open(ivfile)
        for line in hfile:
            tokens = line.split()
            if (tokens[0]=='channel'):
                continue
            if (float(tokens[1])==float(voltage)):
                ma = int(tokens[0].split('/')[3][2:])
                ch = int(tokens[0].split('/')[4][2:])
                if (j>80 and ma==0 and ch==0):
                    continue
                if (ma==0 and ch==0):
                    vetoed+=1
                elif (ma==0 and ch==1):
                    vetoed+=1
                elif (ma==6 and ch==0):
                    vetoed+=1
                elif (ma==6 and ch==1):
                    vetoed+=1
                else: #needs some work
                    current = float(tokens[3])
                    sensnumber, sensorname = map_sensor(ma, ch)
                    #arrayname[sensnumber] = current
                    try:
                        i = float(tokens[4])
                    except ValueError, TypeError:
                        ntc1 = -90.0
                    else:
                        ntc1 = float(tokens[4])
                        temp1[sensnumber] = ntc1

                    try:
                        i = float(tokens[5])
                    except ValueError, TypeError:
                        ntc2 = -90.0
                    else:
                        ntc2 = float(tokens[5])
                        temp2[sensnumber] = ntc2
                    #before storing current, correct for the ntc temps
                    part1 = ((273.15-15.5)/(273.15+ntc1))*((273.15-15.5)/(273.15+ntc1))
                    part2 = exp((-1.21/(2*0.00008617))*((1/(273.15-15.5))-(1/(273.15+ntc1))))
                    newcurrent = current*part1*part2
                    #print current
                    #print "newcurrent", newcurrent
                    #arrayname[sensnumber] = newcurrent
                    arrayname[sensnumber] = current
                    j+=1
                    
        valuelist.append(arrayname)
        ntc1list.append(temp1)
        ntc2list.append(temp2)
        hfile.close()
    return intlumi, date, fillno




def fit_slope(valuelist,slopelist1,slopelist2,slopelist3):

    array_v = array("d", [0.0]*len(valuelist))
    array_i = array("d", [0.0]*len(valuelist))
    
    for iter in range(len(valuelist)):
        new_v = iter*10
        array_v[iter]=new_v
    
    i=0
    while i<88:

        f1=ROOT.TF1("fitfunction",'pol1',50,100)
        f2=ROOT.TF1("fitfunction",'pol1',101,150)
        f3=ROOT.TF1("fitfunction",'pol1',151,200)
            
        for index,currentarray in enumerate(valuelist):
            array_i[index]=currentarray[i]

        iv_graph = ROOT.TGraph(len(array_v),array_v,array_i)

        if len(valuelist)>=10:
            iv_graph.Fit(f1,"RQN")
            fitresult1 = f1.GetParameters()
            slope1=fitresult1[1]
        else:
            slope1 = 0
        slopelist1.append(slope1)
            
        if len(valuelist)>=15:
            iv_graph.Fit(f2,"RQN")
            fitresult2 = f2.GetParameters()
            slope2=fitresult2[1]
        else:
            slope2=0    
        slopelist2.append(fitresult2[1])
            
        if len(valuelist)>17:
            iv_graph.Fit(f3,"RQN")
            fitresult3 = f3.GetParameters()
            slope3=fitresult3[1]
        else:
            slope3=0
        slopelist3.append(slope3)
        
        f1.Delete()
        f2.Delete()
        f3.Delete()
        
        i+=1
            
    return
    
#needs some work
def find_lumi(prevdate,scandate,intlumi,alumilist, fillno):
    newlumi = 0.0
    a=0
    while a<len(alumilist):
        newdate = datetime.datetime.strptime( alumilist[a]['timestamp'] , "%Y-%m-%dT%H:%M:%Sz" )
        if newdate>prevdate:
            if newdate > scandate:
                break
            else:
                fillno = alumilist[a]['fill_id']
                lumi = alumilist[a]['lumi_total']
                newlumi+=lumi  
        a+=1
    intlumi+=newlumi
    return intlumi, fillno
    
#### New work for predictions

def temp_compensate(current,t_curr,t_ref): #function to correct current temp to reference temp
    boltzmann = 1.38065*pow(10,-23)
    bandgap = 1.21 #eV
    exponent = ((-bandgap*1.6*pow(10,-19))/(2*boltzmann))*((1/t_ref)-(1/t_curr))
    outside = (t_ref/t_curr)*(t_ref/t_curr)
    kfactor = outside*math.exp(exponent)
    I_corr = kfactor*current
    return I_corr

def calculateAnnealing(time): #function to calculate the total annealing based on accumulation of annealing factors to date
    alpha_I = 1.23*pow(10,-17)
    alpha_O = 7.07*pow(10,-17)
    beta = 3.29*pow(10,-18)
    t_I = 1.4*pow(10,4) #minutes
    t_O = 1 #minutes

    if time<t_O:
        alpha=0
    else:
        alpha = alpha_I * (exp(-float(time)/t_I)) + alpha_O - (beta*(log(float(time)/t_O)))

    return alpha

def calculate_prediction(lumi, sensor, scan_number):
    #alpha values for sensor fluence at 7TeV
    alphas_7TeV = [2.62276e+13,2.75205e+13,2.93666e+13,3.14825e+13,3.32501e+13,3.47333e+13,3.49275e+13,3.38911e+13,3.20538e+13,3.03178e+13,2.86692e+13,2.70825e+13,2.59761e+13,2.48101e+13,2.40841e+13,2.34858e+13,2.0306e+13,1.91836e+13,1.90577e+13,1.87153e+13,1.85667e+13,2.69236e+13,2.82856e+13,3.01896e+13,3.2174e+13,3.34219e+13,3.45289e+13,3.43859e+13,3.25388e+13,3.10163e+13,2.90164e+13,2.7273e+13,2.58683e+13,2.47045e+13,2.37789e+13,2.28627e+13,2.22064e+13,1.93299e+13,1.80021e+13,1.77524e+13,1.75119e+13,1.7182e+13]

    #beta values for sensor fluence at 7TeV
    betas_7TeV = [1.7152,1.72604,1.76384,1.8048,1.84233,1.87915,1.87515,1.87222,1.83371,1.79545,1.76274,1.72944,1.71177,1.68906,1.67632,1.67479,1.65582,1.6482,1.64012,1.63182,1.63586,1.672,1.75574,1.79454,1.83743,1.85691,1.87469,1.86682,1.82598,1.8117,1.76856,1.7305,1.70505,1.68006,1.66461,1.64796,1.64081,1.61955,1.61361,1.60115,1.59971,1.59234]

    annealing = [0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1, 8.6, 2.1, 0.5, 6.9, 7.8, 18.0, 14.3, 27.5, 26.2, 0.1, 17.4, 10.5, 0.1, 9.4, 6.1, 57600.1,0.350740918565, 14.7851221212, 0.0, 0.0, 0.0, 0.0, 0.0, 1.89602209214, 2.83989177565, 0.0, 665.457744981, 0.0, 2.26689165344, 8.269119705, 2.82947545366, 0.644072165976, 3.15737613371, 420.969565062, 0.0, 3.55559544626, 0.0, 5.15880494493, 0.0, 0.0, 0.0, 0.0, 0.851374625349, 3.83631591387, 2197.67109077, 0.0, 6.69980191113, 0.0, 0.0, 4.78884809085, 0.253177515029, 1.61831954621, 0.0, 0.0, 0.0, 0.404309264293, 5.48415693915, 0.0, 0.0, 0.0, 2.03072442045, 1.27333523393, 0.0, 2.13049244983, 2.48107245771, 0.0, 0.531307791646, 0.0, 0.0, 3.60618071496, 0.0, 0.0, 0.171257155401, 2.67600433675, 0.0, 2.63525951817, 7.65133710203, 28.5776102969, 0.0, 1.58190347381, 2.46075683688, 1.79497494092, 0.0, 0.0, 0.0, 0.0, 1.44683018166, 0.0, 9.27369401645, 1.72819422687, 2.32460965093, 5.4447150686, 0.0, 0.0, 3.37411099422, 0.0, 1.92554507972, 27.2459312322, 8.16510006378, 132.731824522, 795.646242301, 9.7731843505, 0.0, 0.0, 0.0, 0.0, 0.0] #These values need changing as more temp info available (0.0, 0.0 just temporary)

    lumiEnd7TeV = 1260522068.75/1000000000 # total luminosity delivered at 7TeV end (scan 71)
    currentEnd7TeV = lumiEnd7TeV*1000000*((alphas_7TeV[sensor]*3.1415*0.03)/(2-betas_7TeV[sensor]))*((pow(4.3,2-betas_7TeV[sensor]))-(pow(0.8,2-betas_7TeV[sensor]))) # predicted current at end of 7TeV run

    if num<71:
        current_prediction = (lumi/1000000000)*1000000*((alphas_7TeV[sensor]*3.1415*0.03)/(2-betas_7TeV[sensor]))*((pow(4.3,2-betas_7TeV[sensor]))-(pow(0.8,2-betas_7TeV[sensor]))) #current change fb-1, hence lumi/1e9
    elif num>70 and num<130:
        current_prediction = ((lumi/1000000000)-lumiEnd7TeV)*1000000*((1.15*alphas_7TeV[sensor]*3.1415*0.03)/(2-betas_7TeV[sensor]))*((pow(4.3,2-betas_7TeV[sensor]))-(pow(0.8,2-betas_7TeV[sensor])))+currentEnd7TeV #change to 8TeV alpha/beta when available
    elif num>129 and num<150:
        current_prediction = (lumi/1000000000)*1000000*((1.85*alphas_7TeV[sensor]*3.1415*0.03)/(2-betas_7TeV[sensor]))*((pow(4.3,2-betas_7TeV[sensor]))-(pow(0.8,2-betas_7TeV[sensor]))) #change to 13TeV alpha/beta when available
    else:
        current_prediction = (lumi/1000000000)*1000000*((2.00*alphas_7TeV[sensor]*3.1415*0.03)/(2-betas_7TeV[sensor]))*((pow(4.3,2-betas_7TeV[sensor]))-(pow(0.8,2-betas_7TeV[sensor]))) #change to 14TeV alpha/beta when available
    annealingNow=0 #reset annealingNow to 0 to calculate it for each scan
    for i in range(scan_number):
        annealingNow+=annealing[i] #calculate the sum of the annealing factors to this point        
    annealingNow = annealingNow-(annealing[scan_number]/2) #subtract half of the most recent annealing factor, as we approximate that lumi delivered in middle of scan period

    return current_prediction,annealingNow

#### End new work

#This here be my main functor

if __name__ == "__main__":
    print "Generating root files please be patient"
    #RootFile = "/tmp/IVtrending.root"
    RootFile = "/calib/velo/dqm/IVScan/IVtrending.root"
    MaxScanVoltage = 200
    
    #if len(sys.argv) > 1:
    #    RootFile = sys.argv[1]

    voltagelist = []
    valuelist = []
    ntc1list = []
    ntc2list = []
    currentvalues = std.vector('double')()
    currentpredictions = std.vector('double')()  ## new
    currentpredictionsSigPlus = std.vector('double')() ## new
    currentpredictionsSigMinus = std.vector('double')() ## new
    ntc1values = std.vector('double')()
    ntc2values = std.vector('double')()
    slopelist1 = []
    slopelist2 = []
    slopelist3 = []
    slopevalues1 = std.vector('double')()
    slopevalues2 = std.vector('double')()
    slopevalues3 = std.vector('double')()
    alumilist = []
   
    i=0
    while i<=MaxScanVoltage:
        voltagelist.append(i)
        i+=10

    itexists = os.path.exists('/calib/velo/dqm/IVScan/IVtrending.root')

    if itexists:
        afile = TFile(RootFile,"READ")#read old scans
        #check date of last entry in ntuple
        IVtree = gDirectory.Get("IVtree")
        num = IVtree.GetEntriesFast()
        last = IVtree.GetEntry(num-1)
        print IVtree.year,IVtree.month,IVtree.day

        ## All arguments for datetime must be integers
        prevdate = datetime.datetime(int(IVtree.year),int(IVtree.month),int(IVtree.day),int(IVtree.hour),int(IVtree.minutes),0)
        a = IVtree.lastfill + 1
        intlumi = IVtree.lumi
        afile.Close()
        ofile = TFile(RootFile, "UPDATE")#add new data
        
        IVtree = gDirectory.Get("IVtree")
       
        hour=array("d", [0.0])
        minutes=array("d", [0.0])
        year=array("d", [0.0])
        month=array("d", [0.0])
        day=array("d",[0.0])
        totaldays=array("d",[0.0])
        lumi=array("d",[0.0])
        lastfill=array("d",[0.0])
        IVtree.SetBranchAddress("hour", hour)
        IVtree.SetBranchAddress("minutes", minutes)
        IVtree.SetBranchAddress("year", year)
        IVtree.SetBranchAddress("month", month)
        IVtree.SetBranchAddress("day", day)
        IVtree.SetBranchAddress("totaldays",totaldays)
        IVtree.SetBranchAddress("lumi", lumi)
        IVtree.SetBranchAddress("lastfill", lastfill)
        IVtree.SetBranchAddress("ntc1", ntc1values)
        IVtree.SetBranchAddress("ntc2", ntc2values)
        IVtree.SetBranchAddress("IVvalues", currentvalues)
        IVtree.SetBranchAddress("IVpredictions", currentpredictions)  ## new
        IVtree.SetBranchAddress("IVpredictionsSigmaPlus", currentpredictionsSigPlus) ## new
        IVtree.SetBranchAddress("IVpredictionsSigmaMinus", currentpredictionsSigMinus) ## new
        IVtree.SetBranchAddress("IVslopes1", slopevalues1)
        IVtree.SetBranchAddress("IVslopes2", slopevalues2)
        IVtree.SetBranchAddress("IVslopes3", slopevalues3)
        
    #if not already done - define ttree
    else:
        ofile = TFile(RootFile, "RECREATE")#create file
        prevdate = datetime.datetime(2008,1,1,0,0,0)
        intlumi = 0
        a = 1089 #the first fill with data
        num = 0 ## new

        #Need use the ROOT. import when calling with ExecScript, PyROOT is strange
        IVtree = ROOT.TTree("IVtree", "IV Tree")
        hour=array("d", [0.0])        
        minutes=array("d", [0.0])
        year=array("d", [0.0])
        month=array("d", [0.0])
        day=array("d",[0.0])
        totaldays=array("d",[0.0])
        lumi=array("d",[0.0])
        lastfill=array("d",[0.0])
        IVtree.Branch("hour", hour, "hour/D", 64000)
        IVtree.Branch("minutes", minutes, "minutes/D", 64000)
        IVtree.Branch("year", year, "year/D", 64000)
        IVtree.Branch("month", month, "month/D", 64000)
        IVtree.Branch("day", day, "day/D", 64000)
        ## Want to store total days since first IV scan
        IVtree.Branch("totaldays", totaldays, "totaldays/D", 64000)
        IVtree.Branch("lumi", lumi, "lumi/D", 64000)
        IVtree.Branch("lastfill", lastfill, "lastfill/D", 64000)
        IVtree.Branch("ntc1", ntc1values)
        IVtree.Branch("ntc2", ntc2values)
        IVtree.Branch("IVvalues", currentvalues)
        IVtree.Branch("IVpredictions", currentpredictions)  ## new
        IVtree.Branch("IVpredictionsSigmaPlus", currentpredictionsSigPlus) ## new
        IVtree.Branch("IVpredictionsSigmaMinus", currentpredictionsSigMinus) ## new
        IVtree.Branch("IVslopes1", slopevalues1)
        IVtree.Branch("IVslopes2", slopevalues2)
        IVtree.Branch("IVslopes3", slopevalues3)

    fillno = a
    test = True

    IVpath = "/calib/velo/dqm/IVScan/"
    IVfiles = os.listdir(IVpath)
    IVfiles = [(IVpath+filename) for filename in IVfiles if (("/IVScan/1" in (IVpath+filename)) and (".iv" in filename) and ("-30C" in filename) and ("LV_on" in filename) and (os.path.getsize(IVpath+filename)>100000))]
    IVfiles.sort(key=lambda x: os.path.getmtime(x))
    countx=1
    scan_number = 0  ## new

    for file in IVfiles:
    #for file in itertools.islice(IVfiles,100):
        #only run for new files - check date first
        hfile = open(file)
        fname = hfile.name
        nametokens = fname.split('/')
        dateandtime = (nametokens[5].split('_'))
        year[0] = float("20"+dateandtime[0][0:2])
        month[0] = float(dateandtime[0][2:4])
        day[0] = float(dateandtime[0][4:6])
        hour[0] = float(dateandtime[1][0:2])
        minutes[0] = float(dateandtime[1][2:4])
        #hfile.Close()

        ## Date and time of first scan
        firstScan = datetime.datetime(2010,2,8,16,39,0)
        ## All arguments for datetime.datetime must be integers
        scandate = datetime.datetime(int(year[0]),int(month[0]),int(day[0]),int(hour[0]),int(minutes[0]),0)
        ## Calculate number of days since first scan
        difference = scandate-firstScan
        totDays = difference.days
        totaldays[0] = float(totDays)
        
        scan_number+=1 ## new
        update = False
        if scandate>prevdate:

            update = True
            while test:
                if runDBAccess.getFillDict(a):
                    alumilist.append(runDBAccess.getFillDict(a))
                a+=1
                if a == 4000:#this should change to an automatic value when possible
                    test = False
        
            alumi, adate, afill = parse_file(year, month, day, hour, minutes,lumi, file, voltagelist, valuelist, ntc1list, ntc2list,prevdate,intlumi,alumilist,fillno)

            ##### New work to calculate predictions

            delta_lumi = alumi - intlumi

            n=0
            annealingValue = 0
            while n<42: # 0<n<21 is A side, 21<n<42 is C side
                pred,annealNow = calculate_prediction(alumi,n,num)
                annealingValue = annealNow
                currentWarm = pred*calculateAnnealing(annealingValue) #calculate the current at this point at +21c
                current = temp_compensate(currentWarm,273.+21,273.-8.3) #correct current to operating temperature
                currentSigPlus =temp_compensate(currentWarm,273.+21,273.-8.3+1.6)
                currentSigMinus = temp_compensate(currentWarm,273.+21,273.-8.3-1.6)
                print scan_number, n, current, currentSigPlus, currentSigMinus
                currentpredictions.push_back(current)
                currentpredictionsSigPlus.push_back(currentSigPlus)
                currentpredictionsSigMinus.push_back(currentSigMinus)
                n+=1

            ##### End prediction calculation

            intlumi = alumi

            prevdate = adate

            lastfill = afill
        
            print countx, year ,month, day, totaldays
        
            fit_slope(valuelist,slopelist1,slopelist2,slopelist3)

            countx+=1
        
            i=0
            
            while i<88:
                slopevalues1.push_back(slopelist1[i])
                slopevalues2.push_back(slopelist2[i])
                slopevalues3.push_back(slopelist3[i])
                i+=1
          
            for index, voltage in enumerate(voltagelist):
                i=0
                while i<88:
                    currentvalues.push_back(valuelist[index][i])
                    ntc1values.push_back(ntc1list[index][i])
                    ntc2values.push_back(ntc2list[index][i])
                    i+=1
            IVtree.Fill()
            del valuelist[:]
            del ntc1list[:]
            del ntc2list[:]
            currentvalues.clear()
            currentpredictions.clear() ## new
            currentpredictionsSigPlus.clear() ## new 
            currentpredictionsSigMinus.clear() ## new
            ntc1values.clear()
            ntc2values.clear()
            del slopelist1[:]
            del slopelist2[:]
            del slopelist3[:]
            slopevalues1.clear()
            slopevalues2.clear()
            slopevalues3.clear()
        num+=1

    if update:                    
        IVtree.Write()#make sure is only saved if there is an update 
    ofile.Close()

#end

  



