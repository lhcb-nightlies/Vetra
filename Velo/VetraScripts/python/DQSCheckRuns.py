#!/usr/bin/env python -O
import os, sys, string, fnmatch
from ROOT import TTree, TFile, AddressOf, gROOT, gDirectory, TBranch
pyrootpath = os.environ[ 'ROOTSYS' ]

if os.path.exists( pyrootpath ):
   sys.path.append( pyrootpath + os.sep + 'bin' )
   sys.path.append( pyrootpath + os.sep + 'lib' )

import ConfigParser
from array import array
from math import *
from time import localtime, strftime

def get_comments( fname ):
    f=open(fname,'r')
    commentsMark=0
    comments = []
    for line in f.readlines():
       if line[1:9]=="COMMENTS":
          commentsMark=1
          continue
       if len(line)>40 and line[36:39]=="END":
          commentsMark=0
          continue
       if commentsMark==1 and not line=="\n":   
          comments.append(line)
    f.close()
    return ''.join(comments)

def read_dqs_file( fname ):
  fp = open(fname)
  # strip [COMMENTS] section to avoid parse errors
  fl = fp.readlines()
  import io  
  fps = io.BytesIO(''.join([l for l in fl[0:fl.index('[COMMENTS]\n')]]))
  try:
    config.readfp( fps )
  except ConfigParser.ParsingError:
    print 'Parser error, file',fname
  else:
    pass
  
  #Initialize the variables
  run = None
  trendFlag = 1
  time = None
  DQSVersion = None

  try:
    #CHECK FOR EMPTY DQS:
    if config.get('NOISE', 'Avg noise (Phi)')=="-.--":
       print "DQSCheckRuns: Found empty DQS in file ",fname,"... skipping"
       trendFlag=0
       #pass
    else:   
       time = config.get('INFO','Timestamp')
       run = config.get('INFO','Run number')
       DQSVersionString = config.get('INFO', 'DQS version')
       version_split = DQSVersionString[1:].split('.')
       DQSVersion = 100*int(version_split[0])+int(version_split[1])
       if DQSVersion>101 and trendFlag!=0:
         trendFlag = int( config.get('INFO', 'Trending flag'))
  except ConfigParser.NoOptionError:
    print 'Missing option error, file',fname
  else: 
    pass 
  return time, run, trendFlag, DQSVersion

###################################################################
#                           MAIN                                  #
###################################################################

if __name__ == "__main__":
  ##if the next line is initially set to 0, then the script makes a blank root file first...
  DQSpath = "/calib/velo/dqm/DQS/summaries/"
  OutFile = "/tmp/DQSOverview.txt"
  if len(sys.argv) > 1:
    OutFile = sys.argv[1]    
  writeFile = open(OutFile, 'w')
  writeFile.write("Below is the DQS status of runs in the DQM directory\n\n")
  writeFile.write("########################################################################################################\n")
  writeFile.write('%-6s : %-14s : %-14s : %-8s : %-10s : %s\n' %
		  ('Run', 'Starting time', 'DQFlag', 'DataType', 'DQSVersion', 'Comments' ))
  writeFile.write("########################################################################################################\n")
  recentOnly = False
  if len(sys.argv) > 2:
     recentOnly = True
     print
     print 'Considering recent runs only (all runs still in /calib/velo/dqm directly)'
  else:
     print
     print 'Considering all runs (including archived runs in subdirectories)'
  foundDir=0
  if os.path.isdir(DQSpath):
      foundDir=1
  else: 
      print "DQSCheckRuns: Cannot find DQS directory, making blank ROOT file"
  print "DQSCheckRuns: Making DQ overview table"
  ## Make a tree
  #f = TFile('DQSOverview.root','RECREATE')
  #t = TTree('DQS','DQS Overview')

  ## Create a struct
  #gROOT.ProcessLine(\
  #      	  "struct MyStruct{\
  #      	  Char_t RunNumber[50];\
  #      	  Char_t DQFlag[50];\
  #      	  Char_t Version[50];\
  #      	  Char_t DQComments[1000];\
  #      	  };")
  #from ROOT import MyStruct
  #s = MyStruct()
  #t.Branch('RunNumber',AddressOf(s, 'RunNumber'), 'RunNumber/C');
  #t.Branch('DQFlag', AddressOf(s,'DQFlag'),'DQFlag/C');
  #t.Branch('Version', AddressOf(s,'Version'),"Version/C");
  #t.Branch('DQComments', AddressOf(s, 'DQComments'), 'DQComments/C');

  if foundDir:
     DQSfiles=os.listdir(DQSpath)
     DQSfiles=(''.join([DQSpath,filename]) for filename in DQSfiles if ( "dqs" in filename) ) #Select ALL
     
     config = ConfigParser.SafeConfigParser()
     
     runArrayRootFiles =[] 
     runsSeen = {}
     runVec = {}
     timeVec = {}
     trendVec = {}
     commentsVec = {}
     versionVec = {}
     typeVec = {}
     for fname in DQSfiles:
       comments = get_comments(fname)
       time, run, trendFlag, DQSVersion = read_dqs_file(fname)
       try:
           run = int(run)
       except:
          print "DQSCheckRuns: Found run number ", str(run), " in file ", fname, ", skipping..."
          continue
       if run < 0:
          print "DQSCheckRuns: Found run number ", str(run), " in file ", fname, ", skipping..."
          continue
       #Count each run just once. Append 00, 01, 02 to the run to give a different index for ZS, NZS, NZS+ZS
       if fname[fname.rfind("_")+1:fname.rfind("dqs")-1]=="ZS":
           runIndex = 100*run+0
       elif fname[fname.rfind("_")+1:fname.rfind("dqs")-1]=="NZS":
           runIndex = 100*run+1
       elif fname[fname.rfind("_")+1:fname.rfind("dqs")-1]=="NZS+ZS":
           runIndex = 100*run+2
       else: continue       
       if runIndex in runVec:
          continue
       runVec[runIndex] = run
       timeVec[runIndex] = time
       trendVec[runIndex] = trendFlag
       if 10 > (DQSVersion%100):
         versionVec[runIndex] = str(DQSVersion/100)+'.'+str(DQSVersion%100)+' '
       else:  
         versionVec[runIndex] = str(DQSVersion/100)+'.'+str(DQSVersion%100)
       typeVec[runIndex] = fname[fname.rfind("_")+1:fname.rfind("dqs")-1]
       commentsVec[runIndex] = comments
     #Find the run range
     FIRSTRUN = min(runVec.values())
     LASTRUN = max(runVec.values())
     print "DQSCheckRuns: There were ", len(runVec), " analysed runs. The run range found is: ", FIRSTRUN,"--", LASTRUN

     print "DQSCheckRuns: Looking for root files... " 
     dList = []
     for d in os.listdir("/calib/velo/dqm"):
        if( os.path.isdir(os.path.join("/calib/velo/dqm",d)) ):
           try:
              if( recentOnly ):
                 dummy = int(d) # check dir is a number only
              else:
                 dummy = int(d[0]) # only check first letter in a digit
              dList.append(os.path.join("/calib/velo/dqm",d))
           except:
              pass # not a number
     for d in dList:
        for path, dirs, files in os.walk(d):
          files = fnmatch.filter(files, "VELODQM_*ZS*root")
          for filename in files:
             #print filename[filename.rfind("_")+1:filename.rfind("root")-1]
             runNumber=filename[8:filename.find("_", 8)]
             #Count each run just once
             try:
                 runsSeen[int(runNumber)] = 1
             except:
                 pass
     print 'DQSCheckRuns: Sorting run list'
     runArrayRootFiles = runsSeen.keys()
     runArrayRootFiles.sort()
     runArrayRootFiles.reverse()
     print "DQSCheckRuns: Now writing file"
     for checkRun in runArrayRootFiles: 
        runIndex = 100 * checkRun
        dqflag = '?'
        runType = 'N/A'
        version = 'N/A'
        comments = 'N/A'
        time = 'Unchecked'
        if runVec.has_key(runIndex + 0):
            #Look for ZS first
            runType = 'ZS'
        elif runVec.has_key(runIndex + 1):
            #Look for NZS...
            runType = 'NZS'
            runIndex += 1
        elif runVec.has_key(runIndex + 2):
            #Look for NZS+ZS...
            runType = 'NZS+ZS'
            runIndex += 2
        else:
            # not available
            pass
      
        if runType != 'N/A':
            if trendVec[runIndex] == 0:
               dqflag = 'Bad'
            elif trendVec[runIndex] == 1:
               dqflag = 'Good'
            else:
	       dqflag = 'Minor problems'
            time = strftime("%d/%m/%y %H:%M", localtime(float(timeVec[runIndex])*1e-6))
            version = versionVec[runIndex]
            comments = commentsVec[runIndex].replace("\n", " ")

        writeFile.write('%6u : %-14s : %-14s : %-8s : %-10s : %s\n' %
			(checkRun,
			 time,
			 dqflag,
			 runType,
			 version,
			 comments))
     writeFile.close()
  else:
     writeFile.close()

print "DQSCheckRuns: Completed"
