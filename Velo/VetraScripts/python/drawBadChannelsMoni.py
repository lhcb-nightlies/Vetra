#! /usr/bin/env python
###############################################################################
"""
  drawBadChannelsMaps
  ===================
   Scans over Noise data for all TELL1s and produces histograms
   of the dead and noisy channels in the GUI.

   Produces plots of
   1. number of dead channels per sensor
   2. 2D plot of dead channels per sensor and analogue link

   3. number of noisy channels per sensor
   4. 2D plot of noisy channels per sensor and analogue link

   Requirements:
   - the root file loaded in the GUI must contain the output
     histograms from  Velo/VeloDataMonitor/BadChannelMon.cpp

   Example usage:
   - in VeloMoniGUI (BadChannels tab)

 @author James Keaveney
 @date  2009-11-20    
 @author Sara Traynor
 @date   2009-03-17
 @author Wenchao Zhang
 @date  2011-05-30
"""

from ROOT import *
import math
from string import zfill
import os
import glob
from sys import argv
import datetime
import re

global badChanHisto
global badLinkHisto
global noisyChanHisto
global noisyLinkHisto
global deadChanHisto
global deadLinkHisto

global c1
global c2


global badChannelPerSensorHisto
global RawNoisePerSensorHisto
global noisyChannelPerSensorHisto
global RawNoisePerSensorHisto
global cmNoisePerSensorHisto


badChannelPerSensorHisto=TH1F("badChannelPerSensorHisto","bad channels in sensor",2048,0,2048)
noisyChannelPerSensorHisto=TH1F("noisyChannelPerSensorHisto","Noisy channels in sensor",2048,0,2048)
deadChannelPerSensorHisto=TH1F("deadChannelPerSensorHisto","Dead channels in sensor",2048,0,2048)
RawNoisePerSensorHisto=TH1F("RawNoisePerSensorHisto","Raw Noise in sensor",2048,0,2048)
cmNoisePerSensorHisto=TH1F("cmNoisePerSensorHisto","CMC Noise in sensor",2048,0,2048)


badChanHisto=TH1F("badChanHisto","Number of Bad Channels",106,0,106)
badLinkHisto=TH2F("badLinkHisto","2D Bad Link Map",106,0,106,64,0,64)

noisyChanHisto=TH1F("noisyChanHisto","Number of Noisy Channels",106,0,106)
noisyLinkHisto=TH2F("noisyLinkHisto","2D Noisy Link Map",106,0,106,64,0,64)

deadChanHisto=TH1F("deadChanHisto","Number of Dead Channels",106,0,106)
deadLinkHisto=TH2F("deadLinkHisto","2D Dead Link Map",106,0,106,64,0,64)


# directory where to look for histograms
BADCHANNELS_MONI_DIR = '/Vetra/BadChannelMon/BadChannels'

###############################################################################
def mainDrawNoisyChannelMaps(FileName, HVOffFileName, lists):


# open root file
   file=TFile(FileName)
   print 'FileName = ' + str(FileName)
   if file.IsOpen() == 0:
       print "Error could not open file " + FileName
       return

# open HV off root file
   fileHVOff=TFile(HVOffFileName)
   print 'HVOffFileName = ' + str(HVOffFileName)

   if fileHVOff.IsOpen() == 0:
      print "Error could not open file " + HVOffFileName
      return

   # Auto detect the total number of sensor histograms from the directory structure:
   dir = file.Get(BADCHANNELS_MONI_DIR)
   if dir:
       nentries = (dir.GetListOfKeys()).GetEntries()
   else:
       print "No Bad channels histograms present in this root file."
       return

   dirHVOff = fileHVOff.Get(BADCHANNELS_MONI_DIR)
   
   if dirHVOff:
       nentriesHVOff = (dirHVOff.GetListOfKeys()).GetEntries()
   else:
       print "No Bad channels histograms present in the HV off root file."
       return
       
   deadChanHisto.Reset()
   deadLinkHisto.Reset()
   badChanHisto.Reset()
   badLinkHisto.Reset()
   noisyChanHisto.Reset()
   noisyLinkHisto.Reset()
   deads =[]
   noisies = []
   hots = []
   channeldead =[]
   channelnoisy =[]
   s = re.search("_([\d]{5,})_", HVOffFileName)
   if s:
      runnumber = str(s.group(1))
   else:
      print "Error: run number not found for file", HVOffFileName

   #runnumber=str(HVOffFileName[-37:-31])
   #print runnumber
    #loop over histograms
   for nHisto in range (0, nentries):
        deadChannelPerSensorHisto.Reset()
        # get histograms
        nSensor,histoStrips,histoNoisyStrips,histoRawNoiseD,histoCMCNoiseD,histoRawNoise,histoCMCNoise = getHistogram(file, nHisto)
        nSensorH,histoStripsH,histoNoisyStripsH,histoRawNoiseDH,histoCMCNoiseDH,histoRawNoiseH,histoCMCNoiseH = getHistogram(fileHVOff, nHisto)

        for bin in range (1, 2049):
            rawNoiseOn = histoRawNoise.GetBinContent(bin)
            noisyStrip = histoNoisyStrips.GetBinContent(bin)
         #   print "channel " +  str(bin) + "  noise == " +  str(rawNoiseOn)
            if((bin-1) % 32) ==0:
                continue
            else:
                rawNoiseOn = histoRawNoise.GetBinContent(bin)
                rawNoiseOff = histoRawNoiseH.GetBinContent(bin)
                rawNoiseDiff = rawNoiseOff - rawNoiseOn
                linkdiv = (bin-1)/32
                linkNo = int((bin-1)/32)
                #deadChanHisto.Reset()
                if rawNoiseOff < 2:
                   
                   deadChanHisto.Fill(nSensor, 1)
                   deadChannelPerSensorHisto.SetBinContent(bin,1)
                   deadLinkHisto.Fill(nSensor, linkdiv, 1)
                   if lists:
                      channeldead = [str(nSensor),str(bin-1) ]
                      deads.append(channeldead)
                     
                if int(noisyStrip) == 1:
                   #noisyChanHisto.Fill(nSensor,1)
                   #noisyLinkHisto.Fill(nSensor, linkdiv,1)
                   if lists:
                      channelnoisy = [str(nSensor),str(bin-1) ]
                      noisies.append(channelnoisy)
                           
        # calculate number bad Strips per Sensor
        #nNoisyStrips = histoNoisyStrips.GetEntries()
        nNoisyStrips = histoNoisyStrips.Integral()
        #nNoisyStrips = noisyChanHisto.GetEntries()
        #nDeadStrips = 0.0
        #nDeadStrips = deadChanHisto.GetEntries()
        nDeadStrips = deadChanHisto.GetBinContent(nSensor+1)
       
        nBadStrips = nNoisyStrips + nDeadStrips
        #print nNoisyStrips
        #print nDeadStrips
        #print nBadStrips        
        # fill 1D plot (add 1 for root ideosyncracy)
        
       # deadChanHisto.SetBinContent(nSensor+1,nDeadStrips)
        noisyChanHisto.SetBinContent(nSensor+1,nNoisyStrips)   
        badChanHisto.SetBinContent(nSensor+1,nBadStrips)


        # loop over analogue links
        for iLink in range (0,64):
            # calculate number bad strips per analogue link
            #nBadStripsLink=calculateBadStripsOnLink(iLink,histoStrips)
            nNoisyStripsLink=calculateBadStripsOnLink(iLink,histoNoisyStrips)
            nDeadStripsLink=calculateBadStripsOnLink(iLink,deadChannelPerSensorHisto)
            nBadStripsLink = nNoisyStripsLink + nDeadStripsLink
            #print nDeadStripsLink
            # fill 2D coloured plot

            #deadLinkHisto.SetBinContent(nSensor+1,iLink+1,nDeadStripsLink)
            noisyLinkHisto.SetBinContent(nSensor+1,iLink+1,nNoisyStripsLink)
            badLinkHisto.SetBinContent(nSensor+1,iLink+1,nBadStripsLink)
            # nDeadStrips = 0.0
   #print lists         
   if lists==1:
      printListHeader(deads, noisies, hots,runnumber)
   else:
      print 'The list of bad channels has NOT been written to the disk '
   return


def drawPlots(on_canvas, op):
    c1=on_canvas
    c1.Divide(1,2)

    if op == 0:
        c1.cd(1)
        deadChanHisto.Draw()
        deadChanHisto.GetXaxis().SetLabelSize(0.08)
        deadChanHisto.GetXaxis().SetLabelSize(0.08)
        deadChanHisto.GetYaxis().SetTitle("Number of dead channels")
        deadChanHisto.GetXaxis().SetTitle("Sensor Number")
        print "# dead channels"
        print  deadChanHisto.Integral()
        c1.cd(2)
        deadLinkHisto.GetXaxis().SetLabelSize(0.07)
        deadLinkHisto.GetXaxis().SetTitle("Sensor Number")
        deadLinkHisto.GetXaxis().SetTitleSize(0.06)
        deadLinkHisto.GetXaxis().SetTitleOffset(.8)
        deadLinkHisto.GetYaxis().SetLabelSize(0.07)
        deadLinkHisto.GetYaxis().SetTitle("Analogue Link")
        deadLinkHisto.GetYaxis().SetTitleSize(0.07)
        deadLinkHisto.GetYaxis().SetTitleOffset(.4)
        deadLinkHisto.SetMinimum(0.);
        deadLinkHisto.SetMaximum(32.);
                                      
        deadLinkHisto.Draw('colz')       
    
    if op ==1:
        c1.cd(1)
        noisyChanHisto.Draw()
        noisyChanHisto.GetXaxis().SetLabelSize(0.07)
        noisyChanHisto.GetXaxis().SetTitle("Sensor Number")
        noisyChanHisto.GetXaxis().SetTitleSize(0.06)
        noisyChanHisto.GetXaxis().SetTitleOffset(.8)
        noisyChanHisto.GetYaxis().SetLabelSize(0.07)
        noisyChanHisto.GetYaxis().SetTitle("Number of bad channels")
        noisyChanHisto.GetYaxis().SetTitleSize(0.07)
        noisyChanHisto.GetYaxis().SetTitleOffset(.4)
        print "# noisy channels"
        print  noisyChanHisto.Integral()
        c1.cd(2)
        noisyLinkHisto.GetXaxis().SetLabelSize(0.07)
        noisyLinkHisto.GetXaxis().SetTitle("Sensor Number")
        noisyLinkHisto.GetXaxis().SetTitleSize(0.06)
        noisyLinkHisto.GetXaxis().SetTitleOffset(.8)
        noisyLinkHisto.GetYaxis().SetLabelSize(0.07)
        noisyLinkHisto.GetYaxis().SetTitle("Analogue Link")
        noisyLinkHisto.GetYaxis().SetTitleSize(0.07)
        noisyLinkHisto.GetYaxis().SetTitleOffset(.4)    
        noisyLinkHisto.SetMinimum(0.);
        noisyLinkHisto.SetMaximum(32.);
        noisyLinkHisto.Draw('colz')
    

    if op ==2 :
       
        #1D histo
        c1.cd(1)
        badChanHisto.GetXaxis().SetLabelSize(0.07)
        badChanHisto.GetXaxis().SetTitle("Sensor Number")
        badChanHisto.GetXaxis().SetTitleSize(0.06)
        badChanHisto.GetXaxis().SetTitleOffset(.8)
        badChanHisto.GetYaxis().SetLabelSize(0.07)
        badChanHisto.GetYaxis().SetTitle("Number of bad channels")
        badChanHisto.GetYaxis().SetTitleSize(0.07)
        badChanHisto.GetYaxis().SetTitleOffset(.4)

        
        badChanHisto.Draw()
 
        #2D histo
        c1.cd(2)
        badLinkHisto.GetXaxis().SetLabelSize(0.07)
        badLinkHisto.GetXaxis().SetTitle("Sensor Number")
        badLinkHisto.GetXaxis().SetTitleSize(0.06)
        badLinkHisto.GetXaxis().SetTitleOffset(.8)
        badLinkHisto.GetYaxis().SetLabelSize(0.07)
        badLinkHisto.GetYaxis().SetTitle("Analogue Link")
        badLinkHisto.GetYaxis().SetTitleSize(0.07)
        badLinkHisto.GetYaxis().SetTitleOffset(.4)

    
        badLinkHisto.SetMinimum(0.);
        badLinkHisto.SetMaximum(32.);
        badLinkHisto.Draw('colz')
        print "Total # bad channels"
        print noisyChanHisto.Integral() +  deadChanHisto.Integral()
       
    return (c1)

def define_style() :
    gROOT.SetStyle( 'Plain' )
    gStyle.SetPalette(1)
    gStyle.SetOptStat(0)
    gStyle.SetTitleX(0.5)
    gStyle.SetTitleAlign(23)
    gStyle.SetTextFont(50)
    gROOT.ForceStyle()
    
    
def calculateBadStripsOnLink(iLink,histo): 
# calculates how many channels are bad on this link
   numInLink=32
   firstInLink=iLink*numInLink
   lastInLink=((iLink+1)*numInLink)

   # loop over channels and find bad ones
   BadTotal=0
   for iChan in range (firstInLink,lastInLink):
       BadTotal = BadTotal + histo.GetBinContent(iChan+1)
   return BadTotal


def getHistogram(f, nHisto):
# takes filename and histogram number nHisto
# returns histograms

  dir = f.Get("/Vetra/BadChannelMon/BadChannels")
  if (dir.GetListOfKeys()).GetEntries() != 1:
      name = ((dir.GetListOfKeys()).At(nHisto)).GetName()
      
      if name[:6] == "Sensor" :
          SensorNo = int(name[-3:])
                    
          histoStrips=f.Get("/Vetra/BadChannelMon/BadChannels/SensorNo_" + str(SensorNo).zfill(3))
          histoNoisyStrips=f.Get("/Vetra/BadChannelMon/NoisyChannels/SensorNo_" + str(SensorNo).zfill(3))
          histoRawNoiseD=f.Get("/Vetra/BadChannelMon/RawNoiseD/SensorNo_" + str(SensorNo).zfill(3))
          histoCMCNoiseD=f.Get("/Vetra/BadChannelMon/CMCNoiseD/SensorNo_" + str(SensorNo).zfill(3))
          histoRawNoise=f.Get("/Vetra/BadChannelMon/RawNoise/SensorNo_" + str(SensorNo).zfill(3))
          histoCMCNoise=f.Get("/Vetra/BadChannelMon/CMCNoise/SensorNo_" + str(SensorNo).zfill(3))
          
          
      return  SensorNo,histoStrips,histoNoisyStrips,histoRawNoiseD,histoCMCNoiseD,histoRawNoise,histoCMCNoise
  
def getHistogramFromSenNo(f, inSensorNo):
# takes filename and sensor number inSensorNo
# returns histograms for that sensor

    global badChannelPerSensorHisto
    global RawNoisePerSensorHisto
    global cmNoisePerSensorHisto

    dir = f.Get("/Vetra/BadChannelMon/BadChannels")
    
    SensorNo = int(inSensorNo)
    
    histoStrips=f.Get("/Vetra/BadChannelMon/BadChannels/SensorNo_" + str(SensorNo).zfill(3))
    histoNoisyStrips=f.Get("/Vetra/BadChannelMon/NoisyChannels/SensorNo_" + str(SensorNo).zfill(3))
    histoRawNoiseD=f.Get("/Vetra/BadChannelMon/RawNoiseD/SensorNo_" + str(SensorNo).zfill(3))
    histoCMCNoiseD=f.Get("/Vetra/BadChannelMon/CMCNoiseD/SensorNo_" + str(SensorNo).zfill(3))
    histoRawNoise=f.Get("/Vetra/BadChannelMon/RawNoise/SensorNo_" + str(SensorNo).zfill(3))
    histoCMCNoise=f.Get("/Vetra/BadChannelMon/CMCNoise/SensorNo_" + str(SensorNo).zfill(3))
    
    if not histoStrips:
        print "can't find Sensor %d in file"%inSensorNo
        return
    else:
        print "found Sensor %d in file"%inSensorNo
        
    return  SensorNo,histoStrips, histoNoisyStrips,histoRawNoiseD,histoCMCNoiseD,histoRawNoise,histoCMCNoise

        

def mainDrawNoisyChannelsPerSensor(FileName,HVOffFileName,SensorNo):

   #Takes in root file FileName and Sensor Number Sensor No
   #open root file    
    file=TFile(FileName)
    if file.IsOpen() == 0:
        print "Error could not open file " + FileName
        return

    # open HV off root file
    fileHVOff=TFile(HVOffFileName)
    if fileHVOff.IsOpen() == 0:
       print "Error could not open file " + HVOffFileName
       return
    deadChannelPerSensorHisto.Reset()
    noisyChannelPerSensorHisto.Reset()
    cmNoisePerSensorHisto.Reset()
    badChannelPerSensorHisto.Reset()

    #Get the Histograms using filename and sensor number
   
    nSensor,histoStrips,noisyStrips,histoRawNoiseD,histoCMCNoiseD,histoRawNoise,histoCMCNoise = getHistogramFromSenNo(file, SensorNo)

    nSensorHVOff,histoStripsHVOff,noisyStripsHVOff,histoRawNoiseDHVOff,histoCMCNoiseDHVOff,histoRawNoiseHVOff,histoCMCNoiseHVOff = getHistogramFromSenNo(fileHVOff, SensorNo)

    #make histograms for scope issues 
    for iChan in range(1,2049):
        rawNoiseOn = histoRawNoise.GetBinContent(iChan)
        rawNoiseOff = histoRawNoiseHVOff.GetBinContent(iChan)
        rawNoiseDiff = rawNoiseOff - rawNoiseOn
        noisyStrip = int(noisyStrips.GetBinContent(iChan))
        if (iChan-1) % 32 != 0:
               RawNoisePerSensorHisto.SetBinContent(iChan,histoRawNoise.GetBinContent(iChan))
               cmNoisePerSensorHisto.SetBinContent(iChan,histoCMCNoise.GetBinContent(iChan))
               #RawNoisePerSensorHisto.SetTitle("Raw Noise in sensor"+'_'+ str(SensorNo))
               #cmNoisePerSensorHisto.SetTitle("CMC Noise in sensor"+'_'+ str(SensorNo))
               if rawNoiseOff < 2 :
                  deadChannelPerSensorHisto.SetBinContent(iChan,1)
                  badChannelPerSensorHisto.SetBinContent(iChan,histoStrips.GetBinContent(iChan))
                  #deadChannelPerSensorHisto.SetTitle("Dead channels in senso"+'_'+ str(SensorNo))
                  #badChannelPerSensorHisto.SetTitle("bad channels in sensor"+'_'+ str(SensorNo))
               if noisyStrip == 1:
                  noisyChannelPerSensorHisto.SetBinContent(iChan,noisyStrips.GetBinContent(iChan))
                  badChannelPerSensorHisto.SetBinContent(iChan,histoStrips.GetBinContent(iChan))
                  #noisyChannelPerSensorHisto.SetTitle("Noisy channels in sensor"+'_'+ str(SensorNo))
                  #badChannelPerSensorHisto.SetTitle("bad channels in sensor"+'_'+ str(SensorNo))
    deadChannelPerSensorHisto.SetTitle("Dead channels in senso"+'_'+ str(SensorNo))
    badChannelPerSensorHisto.SetTitle("bad channels in sensor"+'_'+ str(SensorNo))
    noisyChannelPerSensorHisto.SetTitle("Noisy channels in sensor"+'_'+ str(SensorNo))
    RawNoisePerSensorHisto.SetTitle("Raw Noise in sensor"+'_'+ str(SensorNo))
    cmNoisePerSensorHisto.SetTitle("CMC Noise in sensor"+'_'+ str(SensorNo))
    return 

def drawPlotsPerSensor(on_canvas, op):
    global badChannelPerSensorHisto
    global RawNoisePerSensorHisto
    global noisyChannelPerSensorHisto
    global cmNoisePerSensorHisto
    global deadChannelPerSensorHisto

    c2=on_canvas
  
    c2.Divide(1,2)

    if op==0:
         c2.cd(1)
         deadChannelPerSensorHisto.Draw()
         deadChannelPerSensorHisto.GetXaxis().SetLabelSize(0.07)
         deadChannelPerSensorHisto.GetXaxis().SetTitleSize(0.06)
         deadChannelPerSensorHisto.GetXaxis().SetTitleOffset(.8)
         deadChannelPerSensorHisto.GetYaxis().SetLabelSize(0.07)
         deadChannelPerSensorHisto.GetYaxis().SetTitleSize(0.07)
         deadChannelPerSensorHisto.GetYaxis().SetTitleOffset(.4)
         deadChannelPerSensorHisto.GetXaxis().SetTitle("Channel")

         c2.cd(2)
         RawNoisePerSensorHisto.Draw()
         RawNoisePerSensorHisto.GetXaxis().SetLabelSize(0.07)
         RawNoisePerSensorHisto.GetXaxis().SetTitleSize(0.06)
         RawNoisePerSensorHisto.GetXaxis().SetTitleOffset(.8)
         RawNoisePerSensorHisto.GetYaxis().SetLabelSize(0.07)
         RawNoisePerSensorHisto.GetYaxis().SetTitleSize(0.07)
         RawNoisePerSensorHisto.GetYaxis().SetTitleOffset(.4)
         RawNoisePerSensorHisto.GetYaxis().SetTitle("Raw noise")

         c2.Update()
    
    if op==1:
         c2.cd(1)
         noisyChannelPerSensorHisto.Draw()
         noisyChannelPerSensorHisto.GetXaxis().SetLabelSize(0.07)
         noisyChannelPerSensorHisto.GetXaxis().SetTitleSize(0.06)
         noisyChannelPerSensorHisto.GetXaxis().SetTitleOffset(.8)
         noisyChannelPerSensorHisto.GetYaxis().SetLabelSize(0.07)
         noisyChannelPerSensorHisto.GetYaxis().SetTitleSize(0.07)
         noisyChannelPerSensorHisto.GetYaxis().SetTitleOffset(.4)
         noisyChannelPerSensorHisto.GetXaxis().SetTitle("Channel")


         c2.cd(2)
         cmNoisePerSensorHisto.Draw()
         cmNoisePerSensorHisto.GetXaxis().SetLabelSize(0.07)
         cmNoisePerSensorHisto.GetXaxis().SetTitleSize(0.06)
         cmNoisePerSensorHisto.GetXaxis().SetTitleOffset(.8)
         cmNoisePerSensorHisto.GetYaxis().SetLabelSize(0.07)
         cmNoisePerSensorHisto.GetYaxis().SetTitleSize(0.07)
         cmNoisePerSensorHisto.GetYaxis().SetTitleOffset(.4)
         cmNoisePerSensorHisto.GetXaxis().SetTitle("Channel")
         cmNoisePerSensorHisto.GetYaxis().SetTitle("CMC noise")

         c2.Update()

    if op==2:
        c2.cd(1)
        badChannelPerSensorHisto.GetXaxis().SetTitle("Channel")
        badChannelPerSensorHisto.Draw()
        
        badChannelPerSensorHisto.GetXaxis().SetLabelSize(0.07)
        badChannelPerSensorHisto.GetXaxis().SetTitleSize(0.06)
        badChannelPerSensorHisto.GetXaxis().SetTitleOffset(.8)
        badChannelPerSensorHisto.GetYaxis().SetLabelSize(0.07)
        badChannelPerSensorHisto.GetYaxis().SetTitleSize(0.07)
        badChannelPerSensorHisto.GetYaxis().SetTitleOffset(.4)
        c2.cd(2)
        RawNoisePerSensorHisto.Draw()
        RawNoisePerSensorHisto.GetXaxis().SetTitle("Channel")
        RawNoisePerSensorHisto.GetYaxis().SetTitle("RawNoise")
        RawNoisePerSensorHisto.GetXaxis().SetLabelSize(0.07)
        RawNoisePerSensorHisto.GetXaxis().SetTitleSize(0.06)
        RawNoisePerSensorHisto.GetXaxis().SetTitleOffset(.8)
        RawNoisePerSensorHisto.GetYaxis().SetLabelSize(0.07)
        RawNoisePerSensorHisto.GetYaxis().SetTitleSize(0.07)
        RawNoisePerSensorHisto.GetYaxis().SetTitleOffset(.4)
    
    return (c2)

def printListHeader(Deads, Noisies, Hots,RunNumber):
   dt = datetime.datetime
   ListName = '/calib/velo/dqm/BCLists/' + str(RunNumber) +'_' + str(dt.today().date())+'_'+ str(dt.today().time().hour)+'.'+str(dt.today().time().minute)+'.'+str(dt.today().time().second)+'_BCList.bcl'
   #ListName = '/calib/velo/dqm/BCLists/' +  str(dt.today().date())+'_'+ str(dt.today().time().hour)+':'+str(dt.today().time().minute)+':'+str(dt.today().time().second)+'_BCList.bcl'
   f = open(ListName, 'w')
   strDeads = 'deads = ' + str(Deads)
   strNoisies = 'noisies = ' + str(Noisies)
   strHots = 'hots = ' + str(Hots)
   strRunNumber=str(RunNumber)

   f.write('[INFO]\n')
   #f.write('This file contains three python format lists of Dead, Noisy and Hot VELO channels\n')
   f.write('Timestamp:' + dt.today().date().strftime("%Y%m%d")+str(dt.today().time().hour)+str(dt.today().time().minute)+str(dt.today().time().second)+'\n')
   f.write('Run number :'+str(RunNumber)+'\n')
   f.write('[BAD CHANNELS]\n')
   f.write('Total # dead :'+ str(len(Deads))+'\n')
   f.write('Total # noisy:'+ str(len(Noisies))+'\n')
   #f.write('Max # dead:'+ str(len(Deads)+len(Noisies))+'\n')
   #f.write('Max # noisy')
   #f.write('This bad channel search identified : ')
   #f.write('\n')
   #f.write(  str(len(Deads)) + ' dead strips (Raw noise HV OFF < 2) '  )
   #f.write('\n')
   #f.write(  str(len(Noisies)) + ' noisy strips (flagged as noisy by VeloDataMonitor/BadChannelMon.cpp) ')
   #f.write('\n')
   f.write(  str(len(Hots)) + ' hot strips (% occupancy  > 1%) **NOT IMPLEMENTED YET**')
   f.write('\n')
   f.write(  str(0) + ' cold strips (% occupancy  <  TBD%) **NOT IMPLEMENTED YET**')
   f.write('\n')
   f.write(strDeads)
   f.write('\n')
   f.write(strNoisies)
   f.write('\n')
   f.write(strHots)
   print 'The list of bad channels has been written to the disk '
   return

   
class BadChannelsMoniHistos:
   def __init__( self, filename = "", HVOffFile = "", subdir=BADCHANNELS_MONI_DIR  ) :
      import os
      """Constructor"""
      self.filename = filename
      self.subdir   = subdir
      self.HVOffFile = HVOffFile
      self.canvasBadChannels = None
      print HVOffFile
      print "constructing BadChannelsMoniHistos ..."

   def draw_badchannels_gui_page(self, mop, write):
       mainDrawNoisyChannelMaps(self.filename , self.HVOffFile, write )
       drawPlots(self.canvasBadChannels, mop)
       
class BadChannelsMoniHistosSensor:
    def __init__( self, filename = "", HVOffFile = "", subdir=BADCHANNELS_MONI_DIR ) :
        import os
        """Constructor"""
        self.filename = filename
        self.subdir   = subdir
        self.HVOffFile = HVOffFile
        self.canvasBadChannels = None
        print HVOffFile
        print "constructing BadChannelsMoniHistos ..."

    def draw_badchannelspersensor_gui_page(self,sensor,opSS  ):
        mainDrawNoisyChannelsPerSensor(self.filename,self.HVOffFile, sensor)
        drawPlotsPerSensor(self.canvasBadChannels,opSS) 
