##---------------------------------------------------------------------------//#
##
##  CheckVer
##  ========================                                                 //
##                                                                            //
##   Script to compare current bad strips with Verified Bad Strips. 
##                                                                            //
##   Requirements:                                                            //
##   - root file produced from BadChannelMon.cpp
##                                                                            //
##   Example usage:                                                           //
##   - in python interactive, input root file
##     ScanChannels("rootfile.root") will produce 3 plots for each sensor
##     Top plot is a graph where
##             1 = Channel is bad in current root file but not on verified list
##             2 = Channel is verified as bad but not in current root file
#              3 = Channel is bad in current root file and verified as bad
##     Second plot is percentage difference in raw noise between the channel and the link noise
##     Third plot is percentage difference in cmc noise between the channel and the link noise
##   @author Sara Traynor
##   @date   2009-03-17                                                       //
##                                                                            //
## ---------------------------------------------------------------------------//


from ROOT import *
import math
from string import *
import os
import glob
import TELL1Map


gROOT.SetStyle("Plain")

print "input: ScanChannel(RootFN.root) \n Plot: 1- bad  in current root file, but not in verified \n 2- bad in verified, not in current root file \n 3- bad in both"

def ScanChannels(RootFN):
    SensorNo=0
    #Check filename. Add .root if necessary:
    if RootFN[-5:] != ".root":
        RootFN = RootFN + ".root"


    f=TFile(RootFN)
    if f.IsOpen() == 0:
        print "Could not open " + RootFN
        return


     #Auto detect the TELL1 Number from the directory structure:
    detected = False
    dir = f.Get("/Vetra/BadChannel/BadStrips")
    if dir:
       nentries = (dir.GetListOfKeys()).GetEntries()
       for n in range(0, nentries):	  	 	
           if (dir.GetListOfKeys()).GetEntries() != 1:
               name = ((dir.GetListOfKeys()).At(n)).GetName()
              
           
               if name[:6] == "Sensor" :
                   SensorNo = int(name[-3:])
                   detected = True           
               if detected == True:
                   print "Sensor %d was found in the file."%SensorNo
                    
           h1=TH1F("h1","Ver Bad",2048,-0.5,2047.5)
           h2=f.Get("/Vetra/BadChannel/BadStrips/SensorNo_" + str(SensorNo).zfill(3))
           h3=f.Get("/Vetra/BadChannel/RawNoiseD/SensorNo_" + str(SensorNo).zfill(3))
           h4=f.Get("/Vetra/BadChannel/CMCNoiseD/SensorNo_" + str(SensorNo).zfill(3))
           h5=f.Get("/Vetra/BadChannel/RawNoise/SensorNo_" + str(SensorNo).zfill(3))
           h6=f.Get("/Vetra/BadChannel/CMCNoise/SensorNo_" + str(SensorNo).zfill(3))

           if not h3:
               print "Can't open histogram, QUITTING.  Are you sure that the filename is correct?"
               return


           SensorList,BadStrips = ScanVerifiedList()
           
           for i in range(0,len(SensorList)):
               if int(SensorList[i])==SensorNo:
                   for j in range(0,2048):
                       if j == int(BadStrips[i]):
                           h1.SetBinContent((j+1),2)
                       
           for i in range(0,105):
               if i==SensorNo:
               
                   for j in range(0,2048):
                       BadCurrent = h2.GetBinContent(j+1)
                       filled = h1.GetBinContent(j+1)
                       print "current : " ,j,filled,BadCurrent

                       
                       if BadCurrent>0:
                       
                          
                           if filled==2:
                          
                               h1.SetBinContent(j+1,3)
                           else:
                               if filled<1:
                                   h1.SetBinContent(j+1,1)
                       
                                                    

           h2.SetTitle("Raw Noise difference %d"%(SensorNo))
           h2.GetXaxis().SetTitle("Strip")




           c1= TCanvas("c1","Bad Strip Dist %d"%(SensorNo),0,0,800,700)
               
           pad11=TPad("p11","p1",0,0.97,1.0,1.0)
           pad12= TPad("p12","p2",0,0.66,1.0,0.97)
           pad13=TPad("p13","p3",0,0.33,1.0,0.65)
           pad14=TPad("p14","p4",0,0,1.0,0.32)
           pad11.Draw()
           pad12.Draw()
           pad13.Draw()
           pad14.Draw()
           pad11.cd()
           pt1 = TPaveText(0.2,0.2,0.9,0.9,"NDC")
           ttext = pt1.AddText("Bad Strip Dist %d"%(SensorNo))
           pt1.SetBorderSize(0)
           pt1.SetFillColor(0)
           pt1.Draw()
               
           pad13.cd()
           h1.SetLineColor(2)
           
           h3.SetLineColor(4)
           h4.SetLineColor(2)
           
           h3.Draw("histo")
           h4.Draw("histo same")

           pad12.cd()
           h1.Draw("histo")

           pad14.cd()
           h5.SetLineColor(4)
           h6.SetLineColor(2)

           h5.Draw("histo")
           h6.Draw("histo same")
           pad12.Update()
           c1.Update()

           psTitle = RootFN[RootFN.rfind('/')+1:-5]+"_TELL1_%d" % SensorNo

                  
           savefile = raw_input("Save histograms to file %s.ps? (y/n): "%psTitle )
          
           print psTitle
           if savefile in ('y', 'Y'):  
               c1.Print("%s.ps("%(psTitle))
               c2.Print("%s.ps)"%(psTitle))        
           
       
           else:  
               print 'Histograms NOT saved '

           del h1
           del h2
           del h3
           del h4
    return

def ScanVerifiedList() :


    Module = []
    tell1 = []
    sensor = []
    nBadStripsList = []
    SensorList = []
    Mod1 = []
    BadChannel = []
    BadChannelList = []


    fin1= open (os.environ['VETRASCRIPTSROOT']+"newVerified.txt")
    fin2= open (os.environ['VETRASCRIPTSROOT']+"python/ModuletoTell1.txt")
   

    
    m = TELL1Map.TELL1Map()
  #  x,y=SensorToTell1()

    lines1 = fin1.readlines()
    lines2 = fin2.readlines()

    for line in lines2:
        
        words = split(line)
        length = len(words)
   
        for i in range(0,105):
            try:
                m.getTell1(i)
            except KeyError:
                Tell1 = -10
            else:
                Tell1 = m.getTell1(i)
            if Tell1 != -10:
                
                if int(Tell1)==int(words[1]):
                    
                    Module.append(words[0])
                    tell1.append(words[1])
                    sensor.append(i)
                

    j = 0
    sensor1 = 0
    for line in lines1:
        words = split(line)
       
        
        Mod1.append(words[0]+words[2])
        BadChannel.append(words[5])
    nBadStrips=0
    flag=0
    
    for j in range(0,len(Module)):
     
    
        for i in range(0,len(Mod1)):
               
                if Mod1[i]==Module[j]:
                    sensor1 = sensor[j]
                    
                    SensorList.append(int(sensor1))
                    BadChannelList.append(BadChannel[i])
                    
    return SensorList,BadChannelList

