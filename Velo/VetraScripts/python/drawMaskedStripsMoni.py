from ROOT import *

global c1
global maskedstripshisto

import os

MONI_DIR = 'Vetra/VeloPedestalSubtractorMoni/'

maskedstripshisto=TH1F("Masked_Bank","",2048,0,2048)

def getMaskedStrips():
    
    from VeloCondAccess import VeloCondAccess

    # we'll need to convert from channels to strips
    import StripChannelMap
    channel_map = StripChannelMap.StripChannelMap()

    # parameters we are interested in. In this case only one.
    parameters = ['link_mask']
    conddb_access = VeloCondAccess(parameters)

    # create a zero-suppressed cache of masked strips
    masked_strips = {}
    sensors = range(42) + range(64,106)
    for sensor in sensors:
      masked_strips[sensor] = []
      masks = conddb_access.getParameterValues(sensor, 'link_mask')
      for tell1_channel in range(2048):
        # we don't store unmasked channels
        if 0 == masks[tell1_channel]:
          continue
  
        # the data base reflects the swapped cable order of the hardware,
        # we need to convert to chip channels
        channel = channel_map.swapCableOrder(tell1_channel)
        # and finally to strips
        strip = channel_map.channelToStrip(sensor,channel)
        
        # put strip and channel number into the zer-suppressed cache
        masked_strips[sensor].append((strip,channel))
    
    return masked_strips

def mainDrawMaskedStripsMaps(FileName):

    import VeloCondAccess

    masked_strips = getMaskedStrips()

    file=TFile(FileName,"RECREATE")
    print 'FileName = ' + str(FileName)
    if file.IsOpen() == 0:
        print "Error could not open file " + FileName
        return
   
    dir = file.mkdir('Vetra')
    dir2 = dir.mkdir('VeloPedestalSubtractorMoni')
    listofnumbers = []
    for i in range(42):
        listofnumbers.append(i)
    for i in range(64,106):
        listofnumbers.append(i)
    for sensor in listofnumbers:
        for (strip,channel) in masked_strips[sensor]:
            maskedstripshisto.Fill(channel,1)

        maskedstripshisto.Draw()
        maskedstripshisto.SetTitle("Masked Strips for Sensor " + str(sensor))

        if(sensor<10):
            sub_folder= "TELL1_00" + str(sensor)

        elif(sensor<100):
            sub_folder= "TELL1_0"+ str(sensor)

        else:
            sub_folder= "TELL1_"+ str(sensor)

        fullpath= MONI_DIR+sub_folder
        dir3= dir2.mkdir(sub_folder)
        dir3.cd()
        maskedstripshisto.Write()
        maskedstripshisto.Reset()
        dir2.cd()

class MaskedStripsMoniHistos:
   def __init__( self, filename = "Vetra_histos.root",  subdir=MONI_DIR  ) :
      import os
      """Constructor"""
      self.filename = filename
      self.subdir   = subdir
      print "Constructing MaskedStripsMoniHistos ..."
   def runmaskedforgui(self):
       mainDrawMaskedStripsMaps(self.filename)
       print str(self.filename) + ' created'
