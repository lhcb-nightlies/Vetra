####################################################
#@date: 15-06-2011                                 #
#@author:Wenchao Zhang                             #
#!/usr/bin/env python                              #
####################################################
import os, sys, string
pyrootpath = os.environ[ 'ROOTSYS' ]

if os.path.exists( pyrootpath ):
   sys.path.append( pyrootpath + os.sep + 'bin' )
   sys.path.append( pyrootpath + os.sep + 'lib' )

from ROOT import TBranch, gDirectory, TFile, TH1F, TH2F, TCanvas, TMath, gROOT, TPad, TGraph, TGraphErrors, TF1, gStyle, TLegend, TVectorD
import ConfigParser
from array import array
from math import *

def init_root():
  gROOT.SetStyle('Plain')
  gStyle.SetOptStat(0)
  gStyle.SetOptFit(1)
  gStyle.SetPalette(1)

def book_histo2D( hname, htitle, nbinx, minx, maxx, nbiny, miny, maxy, xtitle, ytitle ):
  histo = TH2F( hname, htitle, nbinx, minx-0.5, maxx+0.5, nbiny, miny, maxy )
  histo.GetXaxis().SetTitle(xtitle)
  histo.GetYaxis().SetTitle(ytitle)
  histo.SetDrawOption('colz')
  return histo

def book_histo1D( hname, htitle, nbin, min, max, xtitle, ytitle ):
  histo = TH1F( hname, htitle, nbin, min-0.5, max+0.5 )
  histo.SetMarkerStyle(20)
  histo.GetXaxis().SetTitle(xtitle)
  histo.GetYaxis().SetTitle(ytitle)
  return histo

def make_graph( gname, gtitle, n, xvec, yvec):
   graph = TGraph(n, xvec , yvec)
   graph.SetName(gname)
   graph.SetTitle(gtitle)
   return graph

def findRange( anArray):
   minVal=0.0  
   for i in range(0, len(anArray)):
      if anArray[i]!=-99 and anArray[i]<minVal:
         minVal=anArray[i]
   maxVal=max(anArray)   
   theRange = abs( max( abs(minVal), maxVal))
   if theRange==0.0:
     theRange=10   
   return theRange
         
    
def read_dqs_file( fname ):
  try:
    config.readfp( open( fname ) )
  except ConfigParser.ParsingError:
    pass
  else:
    pass

  #Initialize all the variables:
  time = -99.
  run = -99.
  deadTotal = -99.
  noisyTotal = -99.
  badTotal = -99.
  
  try:
    #INFO
    time = config.get('INFO','Timestamp')
    run = config.get('INFO','Run number')
    
    #BAD CHANNELS
    if config.get('BAD CHANNELS', 'Total # dead')!="N/A":
       deadTotal = config.get('BAD CHANNELS', 'Total # dead')
    if config.get('BAD CHANNELS', 'Total # noisy')!="N/A":
       noisyTotal = config.get('BAD CHANNELS', 'Total # noisy')
    if (config.get('BAD CHANNELS', 'Total # dead')!="N/A") & (config.get('BAD CHANNELS', 'Total # noisy')!="N/A"):
       badTotal = str(int(deadTotal) + int(noisyTotal))
    
  except ConfigParser.NoOptionError:
    pass 
  else: 
    pass
  return time, run, deadTotal, noisyTotal, badTotal
  

###################################################################
#                           MAIN                                  #
###################################################################

if __name__ == "__main__":

  DQSpath = "/calib/velo/dqm/BCLists/"
  DQSfiles=os.listdir(DQSpath)  
  DQSfiles=[(DQSpath+filename) for filename in DQSfiles if ( "bcl" in filename)] 
  #print "Will analyse the following DQS files:"
  #for name in DQSfiles:
  #  print name
  
  init_root()
  config = ConfigParser.ConfigParser()
  
  #Declare the vectors to store all the numbers
  valuesArray=[ [], [], [],[], []]
  runArray =[] 
  timeVec = {}
  runVec ={} 
  deadTotalVec={}
  noisyTotalVec={}
  badTotalVec={}
  

  for fname in DQSfiles:
    time, run, deadTotal, noisyTotal, badTotal = read_dqs_file(fname)
    #Sometimes two files (usually one NZS, one ZS) have the same time stamp. So we need to be careful we don't overwrite the right value.
    if time in runVec:
        #print time
	
        if runVec[time]==-99: runVec[time] = float(run)
        
        if deadTotalVec[time]==-99:  deadTotalVec[time] =deadTotal
        if noisyTotalVec[time]==-99:  noisyTotalVec[time] =noisyTotal
        if badTotalVec[time]==-99:  badTotalVec[time] =badTotal
        
    else:
      #print time
      runVec[time] = run
      
      deadTotalVec[time] =deadTotal
      noisyTotalVec[time] =noisyTotal
      badTotalVec[time] =badTotal
     
  keys = runVec.keys()
  keys.sort()
  #print keys
  #Find the run range
  FIRSTRUN = int(runVec[str(keys[0])])
  LASTRUN = int(runVec[str(keys[len(keys)-1])])
  NBINSRUN = LASTRUN - FIRSTRUN +1
  print "There were ", len(keys), " BCL files found. The run range found is: ", FIRSTRUN,"--", LASTRUN
  for key in keys:
      runArray.append( float(runVec[key]))
      
      valuesArray[0].append( float( deadTotalVec[key]))
      valuesArray[1].append( float( noisyTotalVec[key]))
      valuesArray[2].append( float( badTotalVec[key]))
      valuesArray[3].append( float(key)/1.0e6)

      
  graphList=[] 
  graphList.append(make_graph( "graph_deadTotal_vs_run", "Bad Channels: Total number dead", len(keys), array('f', runArray) , array('f', valuesArray[0])))
  graphList.append(make_graph( "graph_noisyTotal_vs_run", "Bad Channels: Total number noisy", len(keys), array('f', runArray) , array('f', valuesArray[1])))
  graphList.append(make_graph( "graph_badTotal_vs_run", "Bad Channels: Total number bad", len(keys), array('f', runArray) , array('f', valuesArray[2])))
  graphList.append(make_graph( "graph_timeStamp_vs_run", "Timestamp for analysis", len(keys), array('d', runArray) , array('d', valuesArray[3])))
  
  RootFile = "/tmp/BCLHistos.root"
  if len(sys.argv) > 1:
    RootFile = sys.argv[1]
  try:
    checkFile = open( RootFile )
  except IOError:
    pass
  else:
    os.remove( RootFile )
  ###Save the histograms to a ROOT file
  print "Writing trending histograms, please wait..." 
  fr = TFile( RootFile , "recreate")  
  fr.mkdir("Trends_vs_run")  
  fr.cd("Trends_vs_run")
  for i in range(0, len(graphList)):  
    graphList[i].Write()
  fr.Close()
  print "Finished writing trending histograms"
  for i in range(0, len(graphList)):  
    graphList[i].Delete()
  del  valuesArray, runArray
