#! /usr/bin/env python
###############################################################################
'''

 drawIVCurve.py
 ==============

 Python script to draw an IV curve from a data file.

 Requirements:
   The input file should be the output of an IV scan obtained 
   from PVSS with the following format:
    <1st column>   <2nd column>  <3rd column> <4th column> <5th column>
       channel         HV(V)         I(uA)       ntc1(C)      ntc2(C)
   located in the package
     G:\velo\modulepowerup\IVscan (online area in IP8)
   which needs two options:
    <IV curve data file> and  <sensor slot number>or<A side>or<C side>or<Velo>
   to produce the IV curve plot for one sensor/the A side/the C side/the Velo.

 Description:
   This script takes the measured current, its bias voltage and the
   ntc1 temperature for each voltage set.
   It then plots current versus voltage for the chosen sensor(s). 
   An eps file of the graph is produced for every selected sensor
   and saved in the same directory where the script is run.
   Every output eps file has the following format:
   IVCurve_<sensor slot number>_<date>_<time>_<ntc1 temperature>.eps
   The date and time corresponds to the IV curve data taken time. 
   

 Example usage:
   python drawIVCurve.py <datafile name> <datafile reference> <sensor slot number> or <Aside> or <Cside> or <Velo>
   
 @author Barinjaka Rakotomiaramanana
 @date   2009-02-13

'''
###############################################################################
from sys  import argv
from ROOT import TCanvas, TGraph, TPad, TLegend
from ROOT import gROOT, gStyle
from array import array
import math

#-------------------------------------------------------------
# Sensor slot number - HV channel mapping
mapping = {
  'PU01_AB' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch00_PU01_AB'
, 'PU02_AT' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch01_PU02_AT'
, 'VL01_AT' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch02_VL01_AT'
, 'VL01_AB' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch03_VL01_AB'
, 'VL02_AT' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch04_VL02_AT'
, 'VL02_AB' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch05_VL02_AB'
, 'VL03_AT' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch06_VL03_AT'
, 'VL03_AB' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch07_VL03_AB'
, 'VL04_AT' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch00_VL04_AT'
, 'VL04_AB' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch01_VL04_AB'
, 'VL05_AT' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch02_VL05_AT'
, 'VL05_AB' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch03_VL05_AB'
, 'VL06_AT' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch04_VL06_AT'
, 'VL06_AB' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch05_VL06_AB'
, 'VL07_AT' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch06_VL07_AT'
, 'VL07_AB' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch07_VL07_AB'
, 'VL08_AT' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch00_VL08_AT'
, 'VL08_AB' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch01_VL08_AB'
, 'VL09_AT' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch02_VL09_AT'
, 'VL09_AB' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch03_VL09_AB'
, 'VL10_AT' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch04_VL10_AT'
, 'VL10_AB' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch05_VL10_AB'
, 'VL11_AT' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch06_VL11_AT'
, 'VL11_AB' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch07_VL11_AB'
, 'VL12_AT' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch00_VL12_AT'
, 'VL12_AB' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch01_VL12_AB'
, 'VL13_AT' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch02_VL13_AT'
, 'VL13_AB' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch03_VL13_AB'
, 'VL14_AT' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch04_VL14_AT'
, 'VL14_AB' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch05_VL14_AB'
, 'VL15_AT' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch06_VL15_AT'
, 'VL15_AB' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch07_VL15_AB'
, 'VL16_AT' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch00_VL16_AT'
, 'VL16_AB' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch01_VL16_AB'
, 'VL19_AT' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch02_VL19_AT'
, 'VL19_AB' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch03_VL19_AB'
, 'VL22_AT' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch04_VL22_AT'
, 'VL22_AB' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch05_VL22_AB'
, 'VL23_AT' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch06_VL23_AT'
, 'VL23_AB' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch07_VL23_AB'
, 'VL24_AT' : 'VEDCSHV:Iseg/can0/crate00/ma05/ch00_VL24_AT'
, 'VL24_AB' : 'VEDCSHV:Iseg/can0/crate00/ma05/ch01_VL24_AB'
, 'VL25_AT' : 'VEDCSHV:Iseg/can0/crate00/ma05/ch02_VL25_AT'
, 'VL25_AB' : 'VEDCSHV:Iseg/can0/crate00/ma05/ch03_VL25_AB'
, 'PU01_CB' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch00_PU01_CB'
, 'PU02_CT' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch01_PU02_CT'
, 'VL01_CT' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch02_VL01_CT'
, 'VL01_CB' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch03_VL01_CB'
, 'VL02_CT' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch04_VL02_CT'
, 'VL02_CB' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch05_VL02_CB'
, 'VL03_CT' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch06_VL03_CT'
, 'VL03_CB' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch07_VL03_CB'
, 'VL04_CT' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch00_VL04_CT'
, 'VL04_CB' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch01_VL04_CB'
, 'VL05_CT' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch02_VL05_CT'
, 'VL05_CB' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch03_VL05_CB'
, 'VL06_CT' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch04_VL06_CT'
, 'VL06_CB' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch05_VL06_CB'
, 'VL07_CT' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch06_VL07_CT'
, 'VL07_CB' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch07_VL07_CB'
, 'VL08_CT' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch00_VL08_CT'
, 'VL08_CB' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch01_VL08_CB'
, 'VL09_CT' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch02_VL09_CT'
, 'VL09_CB' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch03_VL09_CB'
, 'VL10_CT' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch04_VL10_CT'
, 'VL10_CB' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch05_VL10_CB'
, 'VL11_CT' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch06_VL11_CT'
, 'VL11_CB' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch07_VL11_CB'
, 'VL12_CT' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch00_VL12_CT'
, 'VL12_CB' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch01_VL12_CB'
, 'VL13_CT' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch02_VL13_CT'
, 'VL13_CB' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch03_VL13_CB'
, 'VL14_CT' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch04_VL14_CT'
, 'VL14_CB' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch05_VL14_CB'
, 'VL15_CT' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch06_VL15_CT'
, 'VL15_CB' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch07_VL15_CB'
, 'VL16_CT' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch00_VL16_CT'
, 'VL16_CB' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch01_VL16_CB'
, 'VL19_CT' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch02_VL19_CT'
, 'VL19_CB' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch03_VL19_CB'
, 'VL22_CT' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch04_VL22_CT'
, 'VL22_CB' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch05_VL22_CB'
, 'VL23_CT' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch06_VL23_CT'
, 'VL23_CB' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch07_VL23_CB'
, 'VL24_CT' : 'VEDCSHV:Iseg/can0/crate00/ma11/ch00_VL24_CT'
, 'VL24_CB' : 'VEDCSHV:Iseg/can0/crate00/ma11/ch01_VL24_CB'
, 'VL25_CT' : 'VEDCSHV:Iseg/can0/crate00/ma11/ch02_VL25_CT'
, 'VL25_CB' : 'VEDCSHV:Iseg/can0/crate00/ma11/ch03_VL25_CB'
}
mapping_Aside = {
  'PU01_AB' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch00_PU01_AB'
, 'PU02_AT' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch01_PU02_AT'
, 'VL01_AT' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch02_VL01_AT'
, 'VL01_AB' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch03_VL01_AB'
, 'VL02_AT' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch04_VL02_AT'
, 'VL02_AB' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch05_VL02_AB'
, 'VL03_AT' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch06_VL03_AT'
, 'VL03_AB' : 'VEDCSHV:Iseg/can0/crate00/ma00/ch07_VL03_AB'
, 'VL04_AT' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch00_VL04_AT'
, 'VL04_AB' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch01_VL04_AB'
, 'VL05_AT' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch02_VL05_AT'
, 'VL05_AB' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch03_VL05_AB'
, 'VL06_AT' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch04_VL06_AT'
, 'VL06_AB' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch05_VL06_AB'
, 'VL07_AT' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch06_VL07_AT'
, 'VL07_AB' : 'VEDCSHV:Iseg/can0/crate00/ma01/ch07_VL07_AB'
, 'VL08_AT' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch00_VL08_AT'
, 'VL08_AB' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch01_VL08_AB'
, 'VL09_AT' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch02_VL09_AT'
, 'VL09_AB' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch03_VL09_AB'
, 'VL10_AT' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch04_VL10_AT'
, 'VL10_AB' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch05_VL10_AB'
, 'VL11_AT' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch06_VL11_AT'
, 'VL11_AB' : 'VEDCSHV:Iseg/can0/crate00/ma02/ch07_VL11_AB'
, 'VL12_AT' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch00_VL12_AT'
, 'VL12_AB' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch01_VL12_AB'
, 'VL13_AT' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch02_VL13_AT'
, 'VL13_AB' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch03_VL13_AB'
, 'VL14_AT' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch04_VL14_AT'
, 'VL14_AB' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch05_VL14_AB'
, 'VL15_AT' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch06_VL15_AT'
, 'VL15_AB' : 'VEDCSHV:Iseg/can0/crate00/ma03/ch07_VL15_AB'
, 'VL16_AT' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch00_VL16_AT'
, 'VL16_AB' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch01_VL16_AB'
, 'VL19_AT' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch02_VL19_AT'
, 'VL19_AB' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch03_VL19_AB'
, 'VL22_AT' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch04_VL22_AT'
, 'VL22_AB' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch05_VL22_AB'
, 'VL23_AT' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch06_VL23_AT'
, 'VL23_AB' : 'VEDCSHV:Iseg/can0/crate00/ma04/ch07_VL23_AB'
, 'VL24_AT' : 'VEDCSHV:Iseg/can0/crate00/ma05/ch00_VL24_AT'
, 'VL24_AB' : 'VEDCSHV:Iseg/can0/crate00/ma05/ch01_VL24_AB'
, 'VL25_AT' : 'VEDCSHV:Iseg/can0/crate00/ma05/ch02_VL25_AT'
, 'VL25_AB' : 'VEDCSHV:Iseg/can0/crate00/ma05/ch03_VL25_AB'  
} 
mapping_Cside = {
  'PU01_CB' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch00_PU01_CB'
, 'PU02_CT' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch01_PU02_CT'
, 'VL01_CT' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch02_VL01_CT'
, 'VL01_CB' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch03_VL01_CB'
, 'VL02_CT' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch04_VL02_CT'
, 'VL02_CB' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch05_VL02_CB'
, 'VL03_CT' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch06_VL03_CT'
, 'VL03_CB' : 'VEDCSHV:Iseg/can0/crate00/ma06/ch07_VL03_CB'
, 'VL04_CT' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch00_VL04_CT'
, 'VL04_CB' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch01_VL04_CB'
, 'VL05_CT' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch02_VL05_CT'
, 'VL05_CB' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch03_VL05_CB'
, 'VL06_CT' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch04_VL06_CT'
, 'VL06_CB' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch05_VL06_CB'
, 'VL07_CT' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch06_VL07_CT'
, 'VL07_CB' : 'VEDCSHV:Iseg/can0/crate00/ma07/ch07_VL07_CB'
, 'VL08_CT' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch00_VL08_CT'
, 'VL08_CB' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch01_VL08_CB'
, 'VL09_CT' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch02_VL09_CT'
, 'VL09_CB' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch03_VL09_CB'
, 'VL10_CT' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch04_VL10_CT'
, 'VL10_CB' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch05_VL10_CB'
, 'VL11_CT' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch06_VL11_CT'
, 'VL11_CB' : 'VEDCSHV:Iseg/can0/crate00/ma08/ch07_VL11_CB'
, 'VL12_CT' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch00_VL12_CT'
, 'VL12_CB' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch01_VL12_CB'
, 'VL13_CT' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch02_VL13_CT'
, 'VL13_CB' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch03_VL13_CB'
, 'VL14_CT' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch04_VL14_CT'
, 'VL14_CB' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch05_VL14_CB'
, 'VL15_CT' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch06_VL15_CT'
, 'VL15_CB' : 'VEDCSHV:Iseg/can0/crate00/ma09/ch07_VL15_CB'
, 'VL16_CT' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch00_VL16_CT'
, 'VL16_CB' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch01_VL16_CB'
, 'VL19_CT' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch02_VL19_CT'
, 'VL19_CB' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch03_VL19_CB'
, 'VL22_CT' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch04_VL22_CT'
, 'VL22_CB' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch05_VL22_CB'
, 'VL23_CT' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch06_VL23_CT'
, 'VL23_CB' : 'VEDCSHV:Iseg/can0/crate00/ma10/ch07_VL23_CB'
, 'VL24_CT' : 'VEDCSHV:Iseg/can0/crate00/ma11/ch00_VL24_CT'
, 'VL24_CB' : 'VEDCSHV:Iseg/can0/crate00/ma11/ch01_VL24_CB'
, 'VL25_CT' : 'VEDCSHV:Iseg/can0/crate00/ma11/ch02_VL25_CT'
, 'VL25_CB' : 'VEDCSHV:Iseg/can0/crate00/ma11/ch03_VL25_CB'
}

#-------------------------------------------------------------
# Functions

def rootsettings():
  gROOT.Reset()
  gStyle.SetStatW(0.20)
  gStyle.SetStatH(0.20)
  gStyle.SetStatColor(0)
  gStyle.SetStatX(0.88)
  gStyle.SetStatY(0.88)
  gStyle.SetStatFont(42)
  gStyle.SetPalette(45)
  gStyle.SetCanvasColor(10)
  gStyle.SetStatBorderSize(1)

def draw(gr,title,linecolor,linestyle,option,max):
  gr.SetLineColor( linecolor )
  gr.SetLineWidth( 2 )
  gr.SetLineStyle( linestyle )
  gr.SetTitle( title )
  gr.SetMarkerColor( linecolor )
  gr.SetMarkerStyle( 21 )
  gr.GetXaxis().SetTitle( 'Voltage(V)' )
  gr.GetYaxis().SetRangeUser(0,1.2*max)
  gr.GetYaxis().SetTitle( 'Current(#muA)' )
  gr.Draw(option)
  

def IVsensor_drawing(datafile,datafileref,sensor_full):
    # get sensor name
    sensor_nbr = ( sensor_full.split('_') )[0]
       
    # declare classes
    voltageClass = IV( datafile,sensor_nbr,2)
    currentClass = IV( datafile,sensor_nbr,3)
    ntc1Class = IV( datafile,sensor_nbr,4)

    voltageClassref = IV( datafileref,sensor_nbr,2)
    currentClassref = IV( datafileref,sensor_nbr,3)
    ntc1Classref = IV( datafileref,sensor_nbr,4)

    voltage_up = voltageClass.get_parameter_up()
    current_up = currentClass.get_parameter_up()
    ntc1_up = ntc1Class.get_parameter_up()	

    voltage_down = voltageClass.get_parameter_down()
    current_down = currentClass.get_parameter_down()
    ntc1_down = ntc1Class.get_parameter_down()

    voltageref_up = voltageClassref.get_parameter_up()
    currentref_up = currentClassref.get_parameter_up()
    ntc1ref_up = ntc1Classref.get_parameter_up()

    voltageref_down = voltageClassref.get_parameter_down()
    currentref_down = currentClassref.get_parameter_down()
    ntc1ref_down = ntc1Classref.get_parameter_down()
    
    # get name
    date_split = datafile.split('_')
    date = '%s_%s' % ( date_split[0], date_split[1] )

    # sensor print name
    sensor_split = sensor_full.split('_')
    sensor_name  = '%s_%s_%s_%.1fC' % ( sensor_split[1], sensor_split[2], date, ntc1_up[0] )

    # drawing
    c1 = TCanvas( 'c1',sensor_name , 200, 10, 700, 500 )
    c1.SetGrid()
    nb_up = len(voltage_up)
    nb_down = len(voltage_down)
    nbref_up = len(voltageref_up)
    nbref_down = len(voltageref_down)
    gr_up = TGraph( nb_up, voltage_up, current_up )
    gr_down = TGraph( nb_down, voltage_down, current_down )
    grref_up = TGraph( nbref_up, voltageref_up, currentref_up )
    grref_down = TGraph( nbref_down, voltageref_down, currentref_down )

    maxi = max(current_up+currentref_up+current_down+currentref_down)
    print maxi    
 
    draw(gr_up,sensor_name,2,1,'ALP',maxi)
    draw(gr_down,sensor_name,2,4,'same',maxi)
    draw(grref_up,sensor_name,4,1,'same',maxi)
    draw(grref_down,sensor_name,4,4,'same',maxi)
    
    legref_up = 'ref. up %sC'% ( ntc1ref_up[0] )
    legref_down = 'ref. down %sC'% ( ntc1ref_down[0] )    
 
    legend = TLegend(0.14,0.77,0.32,0.89,"","brNDC")
    legend.SetFillColor(0)
    legend.AddEntry(grref_up,legref_up,"l")
    legend.AddEntry(grref_down,legref_down,"l")
    legend.AddEntry(gr_up,'data up',"l")
    legend.AddEntry(gr_down,'data down',"l")
    legend.Draw()

    graph_name = 'IVCurve_%s.eps' % ( sensor_name )
    c1.SaveAs(graph_name)

def usage():
  print """
  Usage:
     python drawIVCurve.py <datafile> <datafile reference> <sensor slot number> or <Aside> or <Cside> or <Velo>

     example
        python drawIVCurve.py  090220_1212_IVscan_VELOC_-10C.dat 090216_1723_IVscan_VELOC_8C.dat  VL06_CT 
  
  """
  exit()

#-------------------------------------------------------------
# Classes

class IV:

  def __init__(self,file,sensor,parameter):

    self.file = file
    self.sensor = sensor
    self.parameter = parameter

  def get_parameter_up(self):
   
    x = array( 'd' )
    f = open( self.file, 'rU' ) 
    lines = f.readlines()
    lines.remove(lines[0])
    n = len(lines)
    y = [ l.split() for l in lines ]
    for i in range( n ):
      if i<(n/2):
        if y[i][0]==self.sensor :
          x.append( float( y[i][self.parameter] ) )
    return x
  
  def get_parameter_down(self):
    x = array( 'd' )
    f = open( self.file, 'rU' ) 
    lines = f.readlines()
    lines.remove(lines[0])
    n = len(lines)
    y = [ l.split() for l in lines ]
    for i in range( n ):
      if i>(n/2):
        if y[i][0]==self.sensor :
          x.append( float( y[i][self.parameter] ) )
       
    return x       

#-------------------------------------------------------------
# main program

if __name__=='__main__':

  if len ( argv ) < 4 :
    usage()
  
  else :
    rootsettings()
    input_datafile,ref_datafile,option = argv[1:4]

    if option == 'Velo':
      for k in mapping.keys():
        IVsensor_drawing( input_datafile,ref_datafile,mapping[k] )

    if option == 'Aside':
      for k in mapping_Aside.keys():
        IVsensor_drawing( input_datafile,ref_datafile,mapping_Aside[k] )
    if option == 'Cside':
      for k in mapping_Cside.keys():
        IVsensor_drawing( input_datafile,ref_datafile,mapping_Cside[k] )
    if option[0]+option[1]=='VL':
        IVsensor_drawing( input_datafile,ref_datafile,mapping[option] )     
      
    if ((option!='Velo')&(option!='Cside')&(option!='Aside')&(option[0]+option[1]!='VL')):
      usage()
    
    

    
  
   
    
    
    
    
