"""Map software stip numbers to chip channels and vice versa.

This Exports:
  
  - StripChannnelMap  The StripChannnelMap class provides methods to convert
	strip numbers to chip channels etc..

Author: Kurt Rinnert <kurt.rinnert@cern.ch>
Date:   2011-06-09
Revision: $Id: $

"""

class StripChannelMap:
	phi_pattern = [ 683, 684, 0, 1, 685, 687, 686, 2, 688, 689, 3, 690 ]
	phi_strip_to_channel = []
	phi_channel_to_strip = []
	r_strip_to_channel = []
	r_channel_to_strip = []
	is_initialized = False

	def __init__( self ):
		if not StripChannelMap.is_initialized:
			self._initPhiStripMaps()
			self._initRStripMaps()
			StripChannelMap.is_initialized = True

	def _initPhiStripMaps( self ):
		StripChannelMap.phi_strip_to_channel = [ 0 for i in range(2048) ]
		StripChannelMap.phi_channel_to_strip = [ 0 for i in range(2048) ]
		for channel in range(2048):
			offset = ((2047-channel)/12)*4
			base = StripChannelMap.phi_pattern[(2047-channel)%12]
			if base > 682:
				offset = offset * 2
			strip = base + offset
			StripChannelMap.phi_strip_to_channel[strip] = channel
			StripChannelMap.phi_channel_to_strip[channel] = strip

	def _initRStripMaps( self ):
		StripChannelMap.r_strip_to_channel = [ 0 for i in range(2048) ]
		StripChannelMap.r_channel_to_strip = [ 0 for i in range(2048) ]
		for channel in range(2048):
			strip = -999
			chip = channel/128
			chip_channel = channel%128
			sector = 3 - chip/4
			if 0 == sector%2:
				if 0 == chip%4:
					strip = 512*sector + chip_channel
				else:
					strip = 512*(sector+1) - (((chip%4)-1)*128 + chip_channel +1)
			else:
				if 3 == chip%4:
					strip = 512*sector+127 - chip_channel
				else:
					strip = 512*sector + ((chip%4)+1)*128 + chip_channel
			StripChannelMap.r_strip_to_channel[strip] = channel
			StripChannelMap.r_channel_to_strip[channel] = strip

	def phiStripToChannel( self, strip ):
		return StripChannelMap.phi_strip_to_channel[strip]

	def phiChannelToStrip( self, channel ):
		return StripChannelMap.phi_channel_to_strip[channel]

	def rStripToChannel( self, strip ):
		return StripChannelMap.r_strip_to_channel[strip]

	def rChannelToStrip( self, channel ):
		return StripChannelMap.r_channel_to_strip[channel]

	def channelToStrip( self, sensor, channel ):
		if sensor < 64 or sensor > 127:
			return self.rChannelToStrip(channel)
		else:
			return self.phiChannelToStrip(channel)

	def stripToChannel( self, sensor, strip ):
		if sensor < 64 or sensor > 127:
			return self.rStripToChannel(strip)
		else:
			return self.phiStripToChannel(strip)

	def swapCableOrder( self, channel ):
		return (3 - channel/512)*512 + channel%512



