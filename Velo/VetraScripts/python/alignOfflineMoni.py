#! /usr/bin/env python
###############################################################################
"""

 alignOfflineMoni.py
 ===================

 Python script for offline alignment monitoring

 Description:
   This script fits sinusoidals to residual vs. phi distributions.
   The amplitudes of the sine and cosine are proportional to the size of 
   translational misalignments. The value and significance (value/error) of the
   fit parameters is used for the monitoring.
   If the combination of value and significance are above a warning threshold
   a warning count is increased; if they are above an alarm threshold a text
   output lists the names of the modules affected and an alarm count is
   increased as well.
   Two plots show the distribution of fit parameter significances and the 
   distribution of fit parameters if the significance is above alarm threshold.

 Requirements:
   The input file should be the histogram output of the algorithm 
     VeloTrackMonitor
   located in the package
     Velo/VeloRecMonitors
   which needs the option
     AlignMoniBasic = True
   to produce the alignment monitoring plots.

 Example usage:
   python -i alignMoniBasic.py [ inputFile.root [ My/RootFile/Directory ] ]
   
 Options:
   Warning and alarm thresholds can be configured at the beginning of the code.

 @author Marco Gersabeck
 @date   2009-02-13

"""
###############################################################################

### Definition of parameters
# significance for misalignment to raise warning
misal_limit_warn_sig = 3.
# absolute threshold for misalignment to raise warning
misal_limit_warn = 1.
# significance for misalignment to raise alarm
misal_limit_alarm_sig = 5.
# absolute threshold for misalignment to raise alarm
misal_limit_alarm = 3.
# number of sensors to look for histograms
num_sensors = 84 
# minimum limit of entries per bin in profile for cleaning
bin_limit = 20
# file where to look for histograms
moni_file_name = 'BrunelMoni.root'
# directory where to look for histograms
moni_dir = 'Velo/VeloTrackMonitor/'

from ROOT import TFile, TProfile, TF1, TMath, TH1F, TCanvas
from ROOT import gDirectory
from math import fabs 
from os   import path
import sys

# =============================================================================
def usage():
  """
  prints the usage instructions
  """
  print "Please use as follows:"
  print "python -i alignMoniBasic.py [ inputFile.root [ My/RootFile/Directory ] ]"
  print "The default input file is BrunelMoni.root in the working directory"
  print "The default plot directory is Velo/VeloTrackMonitor\n"
# ===END USAGE()===============================================================

# =============================================================================
def read_histos( histos ):
  """
  Reads histos into list given as argument from file/directory optionally 
  specified as parameters to __main__ 
  @param moni_histos the list of histograms.
  """
  # open root file
  if path.isfile( moni_file_name ):
    moni_file = TFile( moni_file_name )
  else:
    print 'File %s not found! Nothing done.' % moni_file_name
    usage()
    exit()
  
  # get histograms
  if not gDirectory.cd( moni_dir ):
    print 'Could not find "%s" in "%s" ' % ( moni_dir, moni_file_name )
    usage()
    exit()
  else: 
    for n in range( num_sensors ):
      hname = 'm_prof_res_' + str( n )
      try:
        tmp = gDirectory.Get( hname ).Clone()
        tmp.SetDirectory( 0 )
        histos.append( tmp )
      except ( TypeError, AttributeError ):
        pass

# ===END READ_HISTOS()=========================================================

# =============================================================================
def clean_histos( moni_histos ):
  """
  cleans histograms of bins with very low statistics
  @param moni_histos the list of histograms.
  """
  # histo cleaning
  for histo in moni_histos:
    for bin in range( histo.GetXaxis().GetNbins() ):
      if histo.GetBinEntries( bin ) < bin_limit:
        histo.SetBinContent( bin, 0 )
# ===END CLEAN_HISTOS()========================================================

# =============================================================================
def analyse_histos( moni_histos, c1 ):
  """
  analyses histograms for misalignments and outputs messages accordingly
  plots overview histograms of mislignments and their significances
  @param moni_histos the list of histograms.
  """
  # fit residual distributions and fill overview plots
  fit_fun = TF1( 'fit_fun', '[0] * sin( TMath::Pi() * x / 180. ) + [1] * cos( TMath::Pi() * x / 180. ) + [2]', -210, 210 )
  c1.Divide( 2, 1 )
  c1.cd(1)
  histo_fits_sig = TH1F( 'histo_fits_sig', 'Overview of misalignment significances', 40, -20, 20 )
  histo_fits = TH1F( 'histo_fits', 'Overview of misalignments above warning threshold', 40, -100, 100 )
  pars = [ 0., 0., 0. ]
  parerrs = [ 0., 0., 0. ]
  n_warning = 0
  n_alarm = 0
  print 'Histograms above alarm threshold:'
  for histo in moni_histos:
    histo.Fit( fit_fun, 'Q' )
    pars = fit_fun.GetParameters()
    parerrs = fit_fun.GetParErrors()
    warn_fired = False
    alarm_fired = False
    for n in range( 2 ):
      if parerrs[ n ] != 0.:
        histo_fits_sig.Fill( pars[ n ] / parerrs[ n ] )
        if fabs( pars[ n ] / parerrs[ n ] ) > misal_limit_warn_sig:
          histo_fits.Fill( pars[ n ] )
        if fabs( pars[ n ] / parerrs[ n ] ) > misal_limit_warn_sig and fabs( pars[ n ] ) > misal_limit_warn:
          if not warn_fired:
            n_warning += 1
            warn_fired = True
        if fabs( pars[ n ] / parerrs[ n ] ) > misal_limit_alarm_sig and fabs( pars[ n ] ) > misal_limit_alarm:
          print histo.GetName(), n, pars[ n ], parerrs[ n ]
          if not alarm_fired:
            n_alarm += 1
            alarm_fired = True
  
  print 'Number of histos above warning threshold: ' + str( n_warning )
  print 'Number of histos above alarm threshold:   ' + str( n_alarm )
  
  c1.cd(1)
  histo_fits.Draw('hist')
  c1.cd(2)
  histo_fits_sig.Draw('hist')
  print 'Fit of misalignment significances:'
  histo_fits_sig.Fit( 'gaus' )
  c1.Update()
# ===END ANALYSE_HISTIOS()=====================================================

# =============================================================================
if __name__ == "__main__":
  if len( sys.argv ) == 2 and ( '--help' == sys.argv[1] or '-h' == sys.argv[1] ):
    usage()
    exit()
  
  if len( sys.argv ) > 3:
    print "Too many parameters, some will be ignored"
    usage()
  
  # set histograms file name, overwriting it if passed as an argument
  if len( sys.argv ) > 1:
    moni_file_name = sys.argv[1]
  
  # set histograms directory, overwriting it if passed as an argument
  if len( sys.argv ) > 2:
    moni_dir = sys.argv[2]
  
  moni_histos = []
  read_histos( moni_histos )
  clean_histos( moni_histos )
  c1 = TCanvas( 'c1', '', 10, 10, 800, 300 )
  analyse_histos( moni_histos, c1 )
