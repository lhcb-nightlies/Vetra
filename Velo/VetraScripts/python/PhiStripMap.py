#!/usr/bin/env python
"""Provide mapping from chip channel to strip category

This Exports:
  
  - PhiStripMap  Provides mapping from channel to category
    The map is a simple hard wired list.

Author: Kurt Rinnert <kurt.rinnert@cern.ch>
Date:   2009-09-02
Revision: $Id: PhiStripMap.py,v 1.2 2009-09-02 15:05:21 krinnert Exp $

"""

class PhiStripMap:
  
  def __init__(self):
    """Construct map.

    """
    self.map = [0,1,2,2,0,0,1,2,1,0,2,1]

  def stripCategory( self, chipChannel ):
    """Return category for given chip channel.

    """
    return self.map[ chipChannel%12 ]


