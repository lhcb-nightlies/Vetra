#! /usr/bin/env python
###############################################################################
"""

 drawClustersMoni.py
 ===================

 Python script to display cluster-related monitoring histograms.

 Description:
   This script displays the cluster-related monitoring histograms produced
   by the VeloClusterMonitor algorithm in the Velo/VeloRecMonitors package.

 Requirements:
   The input file should be the histogram output of the algorithm 
     VeloClusterMonitor
   located in the package
     Velo/VeloRecMonitors

 Examples usage:
   1) default input file and histograms subdirectory
   python -i drawClustersMoni.py [ inputFile.root [ My/RootFile/Directory ] ]
   drawClustersMoni()
   2) specifying the input file and histograms subdirectory
   python -i drawClustersMoni.py
   drawClustersMoni( filename='/Path/To/HistosFile.root', subdir='Velo/Bla' )
   3) specifying a reference file as well
   drawClustersMoni( 'data.root', 'ref.root' )
   
 @author Eduardo Rodrigues
 @date   2009-05-30

"""
###############################################################################

from os   import path
from sys  import argv
from ROOT import gROOT, gStyle, gDirectory, kBlue, kWhite
from ROOT import TFile, TCanvas, TLegend, TPad

# file where to look for histograms
MONI_FILENAME = 'Vetra_histos.root'

# directory where to look for histograms
MONI_DIR = 'Velo/VeloClusterMonitor/'

# reference file (by default no reference file is provided)
MONI_REF_FILENAME = ''

# =============================================================================
def usage():
  """
  prints the usage instructions
  """
  print "Please use one of the following ways:"
  print """\
  1) default input file and histograms subdirectory
  python -i drawClustersMoni.py [ inputFile.root [ My/RootFile/Directory ] ]
  drawClustersMoni()
  2) specifying the input file and histograms subdirectory
  python -i drawClustersMoni.py
  drawClustersMoni( filename='/Path/To/HistosFile.root', subdir='Velo/Bla' )
  3) specifying a reference file as well
  drawClustersMoni( 'data.root', 'ref.root' )

  - default input file: \"Vetra_histos.root\", in the working directory
  - default plot directory: \"Velo/VeloClusterMonitor\" """

# =============================================================================
def _drawClustersMoni( filename, reffilename, subdir, debug ) :
  """Internal function. Not to be called directly by the user.
  It really is needed. Do not remove it replacing it by drawClustersMoni(...)!
  """
  histos = ClusterMoniHistos( filename, reffilename, subdir, debug )
  c1 = TCanvas( 'Strip distributions' )
  c1.Divide( 2, 2 )
  histos.draw_clusters_page( c1 )
  c2 = TCanvas( 'ADC distributions' )
  c2.Divide( 2, 2 )
  histos.draw_adc_page( c2 )
  return ( c1, c2 )

# =============================================================================
def define_style() :
  gROOT.SetStyle( 'Plain' )
  gStyle.SetPalette(1)
  gStyle.SetOptStat( 1110 )
  gROOT.ForceStyle()

# =============================================================================
class ClusterMoniHistos :
    def __init__( self, filename=MONI_FILENAME,
                  reffilename=MONI_REF_FILENAME,
                  subdir=MONI_DIR,
                  debug=False ) :
        """Constructor"""
        self.filename    = filename
        self.reffilename = reffilename
        self.subdir      = subdir
        self.debug       = debug
        # use tuples with ('Title',''draw opt','x-axis title','y-axis title')
        self.titles_cl   = [ '# VELO clusters',
                             'Active chip links vs sensor',
                             'Cluster size',
                             'Cluster size vs sensor'                           
                             ]
        self.titles_cl_inset = [ '# VELO clusters (zoom)' ]
        self.titles_hmcl = [ 'n_clusters_highmult',
                             'Active chip links vs sensor',
                             'Cluster size',
                             'Cluster size vs sensor'
                             ]
        self.titles_hmcl_inset = [ '# VELO clusters (zoom)' ]
        self.titles_adc   = [ 'Cluster ADC value',
                              'Cluster ADC values vs sensor',
                              'Cluster ADC value (R)',
                              'Cluster ADC value (Phi)'
                              ]
        
        # use tuples with ('draw opt','x-axis title','y-axis title')
        self.histParam_cl = [( 'hist', 'Number of Clusters', 'Entries'      ),
                             ( 'colz', 'Sensor Number',      'A links'      ),
                             ( 'hist', 'Cluster size',       'Entries'      ),
                             ( 'colz', 'Sensor Number',      'Cluster size' )
                             ]
        self.histParam_cl_inset = [ ( 'hist', 'Number of Clusters', 'Entries'      ) ]
        
        # use tuples with ('draw opt','x-axis title','y-axis title')
        self.histParam_adc = [('hist','ADC of cluster','Entries'),
                              ('colz','Sensor Number', 'ADC values'),
                              ('hist','ADC of clusters in R sensors','Entries'),
                              ('hist','ADC of clusters in Phi sensors', 'Entries')
                              ]
        self.canvasClusters = None
        self.canvasADCs = None
    def print_details( self, method=None ):
      print 'Running \"drawClustersMoni.py"',
      if method :
        print ', method "%s":' % method
      else :
        print
      print '  - filename: %s' % self.filename
      if self.reffilename != None :
        print '  - ref filename: %s' % self.reffilename
      print '  - sub-dir : %s' % self.subdir
    
    def get_histos( self, filename, subdir, titles ):
      """
      Retrieves a list of histos given as argument from file/directory optionally 
      specified as parameters to __main__.
      @param  filename the input file name
      @param  subdir the input file directory
      @param  titles the list of histogram titles to retrieve
      @return histos the list of histograms
      """
      # open root file
      if path.isfile( filename ):
        moni_file = TFile( filename )
      else:
        print 'File "%s" not found! Nothing drawn.' % filename
        usage()
        return [ None, None, None, None ]
      
      # get histograms
      if self.debug :
        print '  ... getting histos from directory "%s"' % subdir
      thehistos = []
      if not gDirectory.cd( subdir ) :
        print 'Could not find "%s" in "%s" !' % ( subdir, filename )
        #usage()
        return [ None, None, None, None ]
      else:
        for title in titles :
          try :
            tmp = gDirectory.Get( title ).Clone()
            tmp.SetDirectory( 0 )
            thehistos.append( tmp )
            #if self.debug : print '  ... getting histo "s"' % title
          except ( TypeError, AttributeError, ReferenceError ) :
            thehistos.append( None )
        return thehistos
    
    def draw_clusters_page( self, on_canvas  ) :
      """Draws one canvas with cluster-related histograms"""
      if self.debug : self.print_details( 'draw_clusters_page' )
      global histos
      histos = self.get_histos( self.filename, self.subdir, self.titles_cl )
      # references only for the "# VELO clusters" and the "Cluster size" histograms, i.e. self.titles_cl[0/2]
      global histos_ref
      if self.reffilename != '' :
        histos_ref = self.get_histos( self.reffilename, self.subdir, [ self.titles_cl[0], self.titles_cl[2] ] )
        if histos_ref :
          histos_ref = [ histos_ref[0], None, histos_ref[1] , None ]
      else :
        histos_ref = [ None, None, None, None ]
      if histos :
        for i, h in enumerate( histos ) :
          on_canvas.cd( i+1 )
          if h :
            h.SetXTitle( self.histParam_cl[i][1] )
            h.SetYTitle( self.histParam_cl[i][2] )
            h.SetName( '' )
            h.SetFillColor( kBlue-10 )
            h.SetLineColor( kBlue )
            if not histos_ref[i] :
              h.Draw( self.histParam_cl[i][0]  )
            else :
              h.SetYTitle( self.histParam_cl[i][2].replace('Entries','Normalised') )
              h.DrawNormalized( self.histParam_cl[i][0]  )
              histos_ref[i].SetMarkerStyle( 21 )
              histos_ref[i].SetMarkerSize( 0.4 )
              histos_ref[i].DrawNormalized( 'samepl' )
              global legend
              legend = TLegend( 0.1, 0.8, 0.35, 0.9 )
              legend.SetTextSize( 0.05 )
              legend.SetFillColor( kWhite )
              legend.AddEntry( histos_ref[i], 'reference', 'pl' )
              legend.Draw()
          on_canvas.Update()
      else:
        print 'No histograms found !'
        print 'Expected the following histograms:'
        for t in self.titles_cl :
          print '\t-', t
        
      raw_input( "press return to continue" )
    
    def draw_adc_page( self, on_canvas  ) :
      """Draws one canvas with histograms of ADC distributions"""
      if self.debug : self.print_details( 'draw_adc_page' )
      global histos2
      histos2 = self.get_histos( self.filename, self.subdir, self.titles_adc )
      # references for all but the 2D histo, i.e. self.titles_adc[1]
      titles2_ref = self.titles_adc[:]
      titles2_ref.remove( 'Cluster ADC values vs sensor' )
      global histos2_ref
      if self.reffilename != '' :
        histos2_ref = self.get_histos( self.reffilename, self.subdir, titles2_ref )
        histos2_ref.insert( 1, None )
      else :
        histos2_ref = [ None, None, None, None ]
      if histos2 :
        legDone = False
        for i, h2 in enumerate( histos2 ) :
          on_canvas.cd( i+1 )
          if h2 :
            h2.GetXaxis().SetTitle( self.histParam_adc[i][1] )
            h2.GetYaxis().SetTitle( self.histParam_adc[i][2] )
            h2.SetName( '' )
            h2.SetFillColor( kBlue-10 )
            h2.SetLineColor( kBlue )
            if not histos2_ref[i] :
              h2.Draw( self.histParam_adc[i][0] )
            else :
              h2.DrawNormalized( self.histParam_adc[i][0] )
              histos2_ref[i].SetMarkerStyle( 21 )
              histos2_ref[i].SetMarkerSize( 0.4 )
              histos2_ref[i].DrawNormalized( 'samepl' )
              if not legDone :
                global legend2
                legend2 = TLegend( 0.1, 0.8, 0.35, 0.9 )
                legend2.SetTextSize( 0.05 )
                legend2.SetFillColor( kWhite )
                legend2.AddEntry( histos2_ref[i], 'reference', 'pl' )
                legend2.Draw()
                legDone = True
          on_canvas.Update()
      else:
        print 'No histograms found !'
        print 'Expected the following histograms:'
        for t in self.titles_adc :
          print '\t-', t
    
    def draw_clusters_gui_page( self, extended_clusters ) :
      """Draws one canvas with cluster-related histograms.
      Note: only to be used in the GUI, or from C++ !"""
      if extended_clusters == 0: 
        titles = self.titles_cl
      else:
        titles = self.titles_hmcl

      if self.debug :
        self.print_details( 'draw_clusters_gui_page' )
      define_style()
      global histos, histos_inset
      histos = self.get_histos( self.filename, self.subdir, titles )
      histos_inset = self.get_histos( self.filename, self.subdir, self.titles_cl_inset )
      if histos and histos[0] and histos[0].GetName() == titles[0]:
        self._set_clusters_overflow( histos[0], self.filename )
        # references only for the "# VELO clusters" and the "Cluster size" histograms, i.e. titles[0/2]
        global histos_ref, histos_ref_inset
        if self.reffilename != MONI_REF_FILENAME :
          histos_ref = self.get_histos( self.reffilename, self.subdir, [ titles[0], titles[2] ] )
          histos_ref_inset = self.get_histos( self.reffilename, self.subdir, self.titles_cl_inset )
          if histos_ref and len(histos_ref) == 2:
            histos_ref = [ histos_ref[0], None, histos_ref[1] , None ]
            self._set_clusters_overflow( histos_ref[0], self.reffilename )
          else:
            histos_ref = [ None, None, None, None ]
            print "Reference plots not available"
        else:
          histos_ref = [ None, None, None, None ]
          histos_ref_inset = [ None ]
        
        for i, h in enumerate( histos ) :
          #log_y = (i == 0 and extended_clusters != 0)
          log_y = i == 0
          tmp = self._draw_clusters_gui_pad( h, histos_ref[i], i+1, log_y )
        return tmp
      else:
        print 'No histograms found !'
      self.canvasClusters.Update()

    def draw_adc_gui_page( self ) :
      """Draws one canvas with histograms of ADC distributions.
      Note: only to be used in the GUI, or from C++ !"""
      if self.debug :
        self.print_details( 'draw_adc_gui_page' )
      define_style()
      global histos2
      histos2 = self.get_histos( self.filename, self.subdir, self.titles_adc )
      # references for all but the 2D histo, i.e. self.titles_adc[1]
      titles2_ref = self.titles_adc[:]
      titles2_ref.remove( 'Cluster ADC values vs sensor' )
      global histos2_ref
      if self.reffilename != MONI_REF_FILENAME :
        histos2_ref = self.get_histos( self.reffilename, self.subdir, titles2_ref )
        histos2_ref.insert( 1, None )
      else :
        histos2_ref = [ None, None, None, None ]
      if histos2 :
        for i, h2 in enumerate( histos2 ) :
          range = 127 if '(R)' in h2.GetTitle() or '(Phi)' in h2.GetTitle() else -1
          tmp2 = self._draw_adc_gui_pad( h2, histos2_ref[i], i+1, range )
        return tmp2
      else:
        print 'No histograms found !'
      self.canvasADCs.Update()
        
    def _draw_clusters_gui_pad( self, histo, histo_ref, pad_number, log_y = False ) :
        pad = self.canvasClusters.cd( pad_number )
        if log_y:
          pad.SetLogy()
        if histo:
          histo.SetXTitle( self.histParam_cl[pad_number-1][1] )
          histo.SetYTitle( self.histParam_cl[pad_number-1][2] )
          histo.SetName( '' )
          histo.SetFillColor( kBlue-10 )
          histo.SetLineColor( kBlue )
          if not histo_ref :
            histo.Draw( self.histParam_cl[pad_number-1][0] )
          else :
            histo.SetYTitle( self.histParam_cl[pad_number-1][2].replace('Entries','Normalised') )
            histo.DrawNormalized( self.histParam_cl[pad_number-1][0] )
            histo_ref.SetMarkerStyle( 21 )
            histo_ref.SetMarkerSize( 0.4 )
            histo_ref.DrawNormalized( 'samepl' )
            global legend
            legend = TLegend( 0.1, 0.8, 0.35, 0.9 )
            legend.SetTextSize( 0.05 )
            legend.SetFillColor( kWhite )
            legend.AddEntry( histo_ref, 'reference', 'pl' )
            legend.Draw()
          self.canvasClusters.Update()
          if pad_number == 1 :
            self._draw_inset( histos_inset[ pad_number-1 ], histos_ref_inset[ pad_number-1 ], pad_number )
            self.canvasClusters.cd()
        return ''

    def _draw_inset( self, histo, histo_ref, pad_number ) :
      if not histo : return ''
      pad = TPad( '', '', 0.51, 0.43, 0.9, 0.87 )
      pad.Draw()
      pad.cd()
      pad_number = 1 # only valid for the clusters distribution!
      histo.SetTitle( '' )
      histo.SetXTitle( '' )
      histo.SetYTitle( '' )
      histo.GetXaxis().SetLabelSize( 0.08 )
      histo.GetYaxis().SetLabelSize( 0.08 )
      histo.SetAxisRange( 0, 50 )
      if not histo_ref :
        histo.Draw( self.histParam_cl_inset[pad_number-1][0] )
      else :
        histo.DrawNormalized( self.histParam_cl_inset[pad_number-1][0] )
        histo_ref.SetMarkerStyle( 21 )
        histo_ref.SetMarkerSize( 0.3 )
        histo_ref.DrawNormalized( 'samepl' )
      pad.Update()
      return ''
    
    def _draw_adc_gui_pad( self, histo2, histo2_ref, pad_number2, max_range=-1 ) :
        self.canvasADCs.cd ( pad_number2 )
        global legDone
        legDone = False
        if max_range > 0 : histo2.SetAxisRange( 0, max_range )
        if histo2 :
          histo2.SetXTitle(self.histParam_adc[pad_number2-1][1])
          histo2.SetYTitle(self.histParam_adc[pad_number2-1][2])
          histo2.SetName( '' )
          histo2.SetFillColor( kBlue-10 )
          histo2.SetLineColor( kBlue )
          if not histo2_ref :
            histo2.Draw( self.histParam_adc[pad_number2-1][0] )
          else :
            histo2.DrawNormalized( self.histParam_adc[pad_number2-1][0] )
            histo2_ref.SetMarkerStyle( 21 )
            histo2_ref.SetMarkerSize( 0.4 )
            histo2_ref.DrawNormalized( 'samepl' )
            global legend2
            if not legDone :
              legend2 = TLegend( 0.1, 0.8, 0.35, 0.9 )
              legend2.SetTextSize( 0.05 )
              legend2.SetFillColor( kWhite )
              legend2.AddEntry( histo2_ref, 'reference', 'pl' )
              legend2.Draw()
              legDone = True
        self.canvasADCs.Update()
        return ''

    def _set_clusters_overflow( self, clusters_hist, filename ):
        """ Set the last bin of the cluster multiplicity histogram equal to 
            the overflow """
        if clusters_hist:
          # use high mult histogram if available, even if displaying the default
          hm_hist = self.get_histos( filename, self.subdir, self.titles_hmcl[0:1] )
          if hm_hist and hm_hist[0]:
            overflow_hist = hm_hist[0]
          else:
            overflow_hist = clusters_hist
          
          nb = clusters_hist.GetNbinsX()
          high = clusters_hist.GetBinLowEdge(nb) + clusters_hist.GetBinWidth(nb)

          # compute bins for integral
          over_nb = overflow_hist.GetNbinsX()
          over_width = overflow_hist.GetBinWidth(1)
          over_start = high / over_width
          overflow = overflow_hist.Integral(int(over_start) + 1, over_nb + 1)

          prev = clusters_hist.GetBinContent(nb)
          clusters_hist.SetBinContent(nb, prev + overflow)
        
        



# =============================================================================
if __name__ == "__main__":
    if len( argv ) == 2 and ( '--help' == argv[1] or '-h' == argv[1] ):
        usage()
        exit()
    if len( argv ) > 3:
        print "Too many parameters, some will be ignored"
        usage()
    
    # set histograms file name, overwriting it if passed as an argument
    if len( argv ) > 1 :
        MONI_FILENAME = argv[1]
    
    # set histograms reference file name, overwriting it if passed as an argument
    if len( argv ) > 2 :
        MONI_REF_FILENAME = argv[2]
    
    # set histograms directory, overwriting it if passed as an argument
    if len( argv ) > 3 :
        MONI_DIR = argv[3]
    
    define_style()

    def drawClustersMoni( filename=MONI_FILENAME,
                          reffilename=MONI_REF_FILENAME,
                          subdir=MONI_DIR,
                          debug=False ) :
      """Main function to draw 2 pages of cluster-related monitoring
      when running this script in standalone.
      """
      return _drawClustersMoni( filename, reffilename, subdir, debug )

# =============================================================================
