#!/usr/bin/env python
import os, sys, string
pyrootpath = os.environ[ 'ROOTSYS' ]

if os.path.exists( pyrootpath ):
   sys.path.append( pyrootpath + os.sep + 'bin' )
   sys.path.append( pyrootpath + os.sep + 'lib' )

vetrascripts_path = os.environ[ 'VETRASCRIPTSROOT' ]
if os.path.exists( vetrascripts_path ):
  sys.path.append(vetrascripts_path + os.sep + 'python')

import GUIConfig

from ROOT import TBranch, gDirectory, TFile, TH1F, TH2F, TCanvas, TMath, gROOT, TPad, TGraph, TGraphErrors, TF1, gStyle, TLegend, TVectorD
import ConfigParser
from array import array
from math import *

def init_root():
  gROOT.SetStyle('Plain')
  gStyle.SetOptStat(0)
  gStyle.SetOptFit(1)
  gStyle.SetPalette(1)

def book_histo2D( hname, htitle, nbinx, minx, maxx, nbiny, miny, maxy, xtitle, ytitle ):
  histo = TH2F( hname, htitle, nbinx, minx-0.5, maxx+0.5, nbiny, miny, maxy )
  histo.GetXaxis().SetTitle(xtitle)
  histo.GetYaxis().SetTitle(ytitle)
  histo.SetDrawOption('colz')
  return histo

def book_histo1D( hname, htitle, nbin, min, max, xtitle, ytitle ):
  histo = TH1F( hname, htitle, nbin, min-0.5, max+0.5 )
  histo.SetMarkerStyle(20)
  histo.GetXaxis().SetTitle(xtitle)
  histo.GetYaxis().SetTitle(ytitle)
  return histo

def make_graph( gname, gtitle, n, xvec, yvec):
   graph = TGraph(n, xvec , yvec)
   graph.SetName(gname)
   graph.SetTitle(gtitle)
   return graph

def findRange( anArray):
   minVal=0.0  
   for i in range(0, len(anArray)):
      if anArray[i]!=-99 and anArray[i]<minVal:
         minVal=anArray[i]
   maxVal=max(anArray)   
   theRange = abs( max( abs(minVal), maxVal))
   if theRange==0.0:
     theRange=10   
   return theRange
         
    
def read_dqs_file( fname ):

  parserError = False
  try:
    config.readfp( open( fname ) )
  except ConfigParser.ParsingError:
    pass
  else:
    pass

  # get global gui config dictionary like cuts etc.
  gui_configuration = GUIConfig.GUIConfig()

  #Initialize all the variables:
  time = -99.
  run = -99.
  avgNoiseR = -99.
  avgNoisePhi= -99.
  nEvents = -99.
  noisyXT = -99.
  residuals = -99.
  deadTotal = -99.
  noisyTotal = -99.
  deadMax = -99.
  noisyMax = -99.
  errors = -99.
  clustAvg = -99.
  clust4Strip = -99.
  clustMPVR = -99.
  clustFWHMR = -99.
  clustMPVPhi = -99.
  clustFWHMPhi = -99.
  clustLargeEvts = -99.
  occ = -99.
  highOcc = -99.
  lowADCM1 = -99.
  lowADCM2 = -99.
  clustersPerTrack = -99.
  moduleMismatch = -99.
  highMismatch = -99.
  pseudoEff = -99.
  lowPseudoEff = -99.
  avgResPull = -99.
  pvX = -99.
  pvY = -99.
  pvZ = -99.
  pvLR = -99.
  pvLRy = -99.
  pvLRz = -99.
  beamCentreX = -99.
  beamCentreY = -99.
  avLowMult = -99.
  evWZeroClust = -99.
  trendFlag=1.
  DQSVersion = 0
  
  try:
    #CHECK FOR EMPTY DQS:
    if config.get('NOISE', 'Avg noise (Phi)')=="-.--":
       print "Found empty DQS... skipping"
       trendFlag=0
    #INFO
    time = config.get('INFO','Timestamp')
    if config.get('INFO', 'Run number')=="N/A":
       trendFlag=0
    else:  run = config.get('INFO','Run number')
    nEvents = config.get('INFO','Events analysed')
    DQSVersionString = config.get('INFO', 'DQS version')
    version_split = DQSVersionString[1:].split('.')
    DQSVersion = 100*int(version_split[0])+int(version_split[1])
    if DQSVersion>101 and trendFlag!=0:
      trendFlag = float( config.get('INFO', 'Trending flag'))
    #NOISE
    if config.get('NOISE', 'Avg noise (R)')!="N/A":
       avgNoiseR = config.get('NOISE', 'Avg noise (R)')
    if config.get('NOISE', 'Avg noise (Phi)')!="N/A":
       avgNoisePhi = config.get('NOISE', 'Avg noise (Phi)')
    #CROSS TALK
    if config.get('CROSS TALK', 'Number of noisy header links')!="N/A":
       noisyXT = config.get('CROSS TALK', 'Number of noisy header links')
    #PEDESTALS
    if DQSVersion <106:
       if config.get('PEDESTALS', 'Number of large residuals')!="N/A":
          residuals = config.get('PEDESTALS', 'Number of large residuals')
    else:
       if config.get('PEDESTALS', 'Number of badly tuned pedestals')!="N/A":
          residuals = config.get('PEDESTALS', 'Number of badly tuned pedestals')
    #BAD CHANNELS
    if config.get('BAD CHANNELS', 'Total # dead')!="N/A":
       deadTotal = config.get('BAD CHANNELS', 'Total # dead')
    if config.get('BAD CHANNELS', 'Total # noisy')!="N/A":
       noisyTotal = config.get('BAD CHANNELS', 'Total # noisy')
    if config.get('BAD CHANNELS', 'Max # dead')!="N/A":
       deadMax = config.get('BAD CHANNELS', 'Max # dead')
    if config.get('BAD CHANNELS', 'Max # noisy')!="N/A":
       noisyMax = config.get('BAD CHANNELS', 'Max # noisy')
    #ERRORS
    if config.get('ERRORS', 'Number of TELL1s with >50 errors in last 1000 events')!="N/A":
       errors = config.get('ERRORS', 'Number of TELL1s with >50 errors in last 1000 events')
    #CLUSTERS
    if config.get('CLUSTERS','Avg number of strips per cluster')!="N/A":
       clustAvg = config.get('CLUSTERS','Avg number of strips per cluster')
    if config.get('CLUSTERS','% of 4-strip clusters')!="N/A":
       clust4Strip = config.get('CLUSTERS','% of 4-strip clusters')
    if config.get('CLUSTERS','Landau MPV (R)')!="N/A":
       clustMPVR = config.get('CLUSTERS','Landau MPV (R)')
    if config.get('CLUSTERS','Landau FWHM (R)')!="N/A":
       clustFWHMR = config.get('CLUSTERS','Landau FWHM (R)')
    if config.get('CLUSTERS','Landau MPV (Phi)')!="N/A":
       clustMPVPhi = config.get('CLUSTERS','Landau MPV (Phi)')
    if config.get('CLUSTERS','Landau FWHM (Phi)')!="N/A":
       clustFWHMPhi = config.get('CLUSTERS','Landau FWHM (Phi)')
    if DQSVersion >= 108 and config.get('CLUSTERS','Large events per 10000')!="N/A":
          clustLargeEvts = config.get('CLUSTERS','Large events per 10000')
    if DQSVersion >= 111 and config.get('CLUSTERS','Av. low mult.')!="N/A":
          avLowMult = config.get('CLUSTERS','Av. low mult.')
    if DQSVersion >= 111 and config.get('CLUSTERS','Events with 0 clusters')!="N/A":
          evWZeroClust = config.get('CLUSTERS','Events with 0 clusters')
    #OCCUPANCY
    if config.get('OCCUPANCY','Avg strip occupancy %')!="N/A":
       occ = config.get('OCCUPANCY','Avg strip occupancy %')
    
    occ_cuts = gui_configuration['USED_HIGH_OCC_CUTS'].split(',')
    for cut in occ_cuts:
      cut_percent = int(cut)
      option_name = 'Number of strips with occupancy >%i%%' % cut_percent
      if config.has_option('OCCUPANCY', option_name):
        if config.get('OCCUPANCY',option_name)!="N/A":
          highOcc = config.get('OCCUPANCY',option_name)
        break
  
  
    #TRACKS
    if config.get('TRACKS', 'Avg number of clusters per track')!="N/A":
       clustersPerTrack = config.get('TRACKS', 'Avg number of clusters per track')
    if config.get('TRACKS', 'Avg module mismatch %')!="N/A":
       moduleMismatch = config.get('TRACKS', 'Avg module mismatch %')
    if config.get('TRACKS', 'Sensors with mismatch >20%')!="N/A":
      highMismatch = config.get('TRACKS', 'Sensors with mismatch >20%')
    if config.get('TRACKS', 'Avg pseudo-efficiency %')!="N/A":
       pseudoEff = config.get('TRACKS', 'Avg pseudo-efficiency %')
    if config.get('TRACKS', 'Number of sensors with pseudo-efficiency <90%')!="N/A":
       lowPseudoEff = config.get('TRACKS', 'Number of sensors with pseudo-efficiency <90%')
    if config.get('TRACKS', 'Avg residual pull')!="N/A":
       avgResPull = config.get('TRACKS', 'Avg residual pull')
    if DQSVersion>=110:
       if config.get('LOW ADC', 'Ratio close to M1 %')!="N/A":
          lowADCM1 = config.get('LOW ADC', 'Ratio close to M1 %')
       if config.get('LOW ADC', 'Ratio close to M2 %')!="N/A":
          lowADCM2 = config.get('LOW ADC', 'Ratio close to M2 %')
    #PRIMARY VERTICES
    if config.get('PRIMARY VERTICES', 'Avg position (X)')!="N/A":
       pvX = config.get('PRIMARY VERTICES', 'Avg position (X)')
    if config.get('PRIMARY VERTICES', 'Avg position (Y)')!="N/A":
       pvY = config.get('PRIMARY VERTICES', 'Avg position (Y)')
    if config.get('PRIMARY VERTICES', 'Avg position (Z)')!="N/A":
       pvZ = config.get('PRIMARY VERTICES', 'Avg position (Z)')
    if config.get('PRIMARY VERTICES', 'Avg L-R X position')!="N/A":
       pvLR = config.get('PRIMARY VERTICES', 'Avg L-R X position')
    if DQSVersion>104:
       if config.get('PRIMARY VERTICES', 'Avg L-R Y position')!="N/A":
          pvLRy = config.get('PRIMARY VERTICES', 'Avg L-R Y position')
       if config.get('PRIMARY VERTICES', 'Avg L-R Z position')!="N/A":
          pvLRz = config.get('PRIMARY VERTICES', 'Avg L-R Z position')
    if config.get('PRIMARY VERTICES', 'Beam-centre local (X)')!="N/A":
       beamCentreX = config.get('PRIMARY VERTICES', 'Beam-centre local (X)')
    if config.get('PRIMARY VERTICES', 'Beam-centre local (Y)')!="N/A":
       beamCentreY = config.get('PRIMARY VERTICES', 'Beam-centre local (Y)')
  except ConfigParser.NoOptionError:
    pass 
  else: 
    pass
  return time, run, nEvents, avgNoiseR, avgNoisePhi, noisyXT, residuals, deadTotal, noisyTotal, deadMax, noisyMax, errors,clustAvg, clust4Strip, clustMPVR, clustFWHMR, clustMPVPhi, clustFWHMPhi, clustLargeEvts, occ, highOcc, lowADCM1, lowADCM2, clustersPerTrack, moduleMismatch, highMismatch, pseudoEff, lowPseudoEff, avgResPull, pvX, pvY, pvZ, pvLR, pvLRy, pvLRz,  beamCentreX, beamCentreY, DQSVersion, trendFlag, avLowMult, evWZeroClust 


###################################################################
#                           MAIN                                  #
###################################################################

if __name__ == "__main__":

  DQSpath = "/calib/velo/dqm/DQS/summaries/"
  DQSfiles=os.listdir(DQSpath)
  #DQSfiles=[(DQSpath+filename) for filename in DQSfiles if ( "NZS.dqs" in filename) ]   #Select NZS only
  #DQSfiles=[(DQSpath+filename) for filename in DQSfiles if ( "_ZS.dqs" in filename) ]  #Select ZS only
  DQSfiles=[(DQSpath+filename) for filename in DQSfiles if ( "ZS.dqs" in filename) ] #Select ALL
  #print "Will analyse the following DQS files:"
  #for name in DQSfiles:
  #  print name
  
  init_root()
  config = ConfigParser.ConfigParser()
  
  #Declare the vectors to store all the numbers
  valuesArray= [ [] for x in xrange(0,37) ]
  runArray =[] 
  timeVec = {}
  runVec ={} 
  avgNoiseRVec = {}
  avgNoisePhiVec = {}
  nEventsVec={}
  noisyXTVec={}
  residualsVec={}
  deadTotalVec={}
  noisyTotalVec={}
  deadMaxVec={}
  noisyMaxVec={}
  errorsVec={}
  clustAvgVec={}
  clust4StripVec={}
  clustMPVRVec={}
  clustFWHMRVec={}
  clustMPVPhiVec={}
  clustFWHMPhiVec={}
  clustLargeEvtsVec={}
  occVec={}
  highOccVec={}
  lowADCM1Vec={}
  lowADCM2Vec={}
  clustersPerTrackVec={}
  moduleMismatchVec={}
  highMismatchVec={}
  pseudoEffVec={}
  lowPseudoEffVec={}
  avgResPullVec={}
  pvXVec={}
  pvYVec={}
  pvZVec={}
  pvLRVec={}
  pvLRyVec={}
  pvLRzVec={}
  beamCentreXVec={}
  beamCentreYVec={}
  avLowMultVec={}
  evWZeroClustVec={}

  for fname in DQSfiles:
    time, run, nEvents, avgNoiseR, avgNoisePhi, noisyXT, residuals, deadTotal, noisyTotal, deadMax, noisyMax, errors,clustAvg, clust4Strip, clustMPVR, clustFWHMR, clustMPVPhi, clustFWHMPhi, clustLargeEvts, occ, highOcc, lowADCM1, lowADCM2, clustersPerTrack, moduleMismatch, highMismatch, pseudoEff, lowPseudoEff, avgResPull, pvX, pvY, pvZ, pvLR, pvLRy, pvLRz,  beamCentreX, beamCentreY, DQSVersion, trendFlag, avLowMult, evWZeroClust   = read_dqs_file(fname)
    if trendFlag<=0 or run<0:
       continue 
    time = long(time)
    #Sometimes two files (usually one NZS, one ZS) have the same time stamp. So we need to be careful we don't overwrite the right value.
    if time in occVec:
        #print time
        if occVec[time]==-99: occVec[time]=occ
        if runVec[time]==-99: runVec[time] = float(run)
        if avgNoiseRVec[time]==-99: avgNoiseRVec[time] = avgNoiseR
        if avgNoisePhiVec[time]==-99: avgNoisePhiVec[time] = avgNoisePhi
        if nEventsVec[time]==-99: nEventsVec[time] =nEvents
        if noisyXTVec[time]==-99: noisyXTVec[time] =noisyXT
        if residualsVec[time]==-99:  residualsVec[time] =residuals
        if deadTotalVec[time]==-99:  deadTotalVec[time] =deadTotal
        if noisyTotalVec[time]==-99:  noisyTotalVec[time] =noisyTotal
        if deadMaxVec[time]==-99:  deadMaxVec[time] =deadMax
        if noisyMaxVec[time]==-99:  noisyMaxVec[time] =noisyMax
        if errorsVec[time]==-99:  errorsVec[time] =errors
        if clustAvgVec[time]==-99:  clustAvgVec[time] =clustAvg
        if clust4StripVec[time]==-99: clust4StripVec[time] =clust4Strip
        if clustMPVRVec[time]==-99:  clustMPVRVec[time] =clustMPVR
        if clustFWHMRVec[time]==-99:  clustFWHMRVec[time] =clustFWHMR
        if clustMPVPhiVec[time]==-99: clustMPVPhiVec[time] =clustMPVPhi
        if clustFWHMPhiVec[time] ==-99: clustFWHMPhiVec[time] =clustFWHMPhi
        if clustLargeEvtsVec[time] ==-99: clustLargeEvtsVec[time] =clustLargeEvts
        if highOccVec[time] ==-99:   highOccVec[time]=highOcc
        if lowADCM1Vec[time] ==-99: lowADCM1Vec[time]=lowADCM1
        if lowADCM2Vec[time] ==-99: lowADCM2Vec[time]=lowADCM2
        if clustersPerTrackVec[time]  ==-99: clustersPerTrackVec[time]=clustersPerTrack
        if moduleMismatchVec[time]  ==-99:  moduleMismatchVec[time]=moduleMismatch
        if highMismatchVec[time] ==-99: highMismatchVec[time]=highMismatch
        if pseudoEffVec[time] ==-99: pseudoEffVec[time]=pseudoEff
        if lowPseudoEffVec[time] ==-99: lowPseudoEffVec[time]=lowPseudoEff
        if avgResPullVec[time] ==-99: avgResPullVec[time]=avgResPull
        if pvXVec[time]==-99:  pvXVec[time]=pvX
        if pvYVec[time]==-99:  pvYVec[time]=pvY
        if pvZVec[time]==-99:   pvZVec[time]=pvZ
        if pvLRVec[time]==-99:   pvLRVec[time]=pvLR
        if pvLRyVec[time]==-99:   pvLRyVec[time]=pvLRy
        if pvLRzVec[time]==-99:   pvLRzVec[time]=pvLRz
        if beamCentreXVec[time]==-99:  beamCentreXVec[time]=beamCentreX
        if beamCentreYVec[time]  ==-99: beamCentreYVec[time]=beamCentreY
	if avLowMultVec[time] ==-99: avLowMultVec[time]=avLowMult
	if evWZeroClustVec[time] ==-99: evWZeroClustVec[time]=evWZeroClust
    else:
      #print time
      runVec[time] = run
      avgNoiseRVec[time] = avgNoiseR
      avgNoisePhiVec[time] = avgNoisePhi
      nEventsVec[time] =nEvents
      noisyXTVec[time] =noisyXT
      residualsVec[time] =residuals
      deadTotalVec[time] =deadTotal
      noisyTotalVec[time] =noisyTotal
      deadMaxVec[time] =deadMax
      noisyMaxVec[time] =noisyMax
      errorsVec[time] =errors
      clustAvgVec[time] =clustAvg
      clust4StripVec[time] =clust4Strip
      clustMPVRVec[time] =clustMPVR
      clustFWHMRVec[time] =clustFWHMR
      clustMPVPhiVec[time] =clustMPVPhi
      clustFWHMPhiVec[time] =clustFWHMPhi
      clustLargeEvtsVec[time] =clustLargeEvts
      occVec[time] =occ
      highOccVec[time]=highOcc
      lowADCM1Vec[time]=lowADCM1
      lowADCM2Vec[time]=lowADCM2
      clustersPerTrackVec[time]=clustersPerTrack
      moduleMismatchVec[time]=moduleMismatch
      highMismatchVec[time]=highMismatch
      pseudoEffVec[time]=pseudoEff
      lowPseudoEffVec[time]=lowPseudoEff
      avgResPullVec[time]=avgResPull
      pvXVec[time]=pvX
      pvYVec[time]=pvY
      pvZVec[time]=pvZ
      pvLRVec[time]=pvLR
      pvLRyVec[time]=pvLRy
      pvLRzVec[time]=pvLRz
      beamCentreXVec[time]=beamCentreX
      beamCentreYVec[time]=beamCentreY
      avLowMultVec[time]=avLowMult
      evWZeroClustVec[time]=evWZeroClust


  keys = occVec.keys()
  keys.sort()
  #print keys
  #Find the run range
  FIRSTRUN = int(min([int(v) for v in runVec.values()]))
  LASTRUN = int(max([int(v) for v in runVec.values()]))
  NBINSRUN = LASTRUN - FIRSTRUN +1
  print "There were ", len(keys), " DQS files found. The run range found is: ", FIRSTRUN,"--", LASTRUN
  for key in keys:
      runArray.append( float(runVec[key]))
      valuesArray[0].append( float( avgNoiseRVec[key]))
      valuesArray[1].append( float(avgNoisePhiVec[key]))
      valuesArray[2].append( float( noisyXTVec[key]))
      valuesArray[3].append( float( residualsVec[key]))
      valuesArray[4].append( float( deadTotalVec[key]))
      valuesArray[5].append( float( noisyTotalVec[key]))     
      valuesArray[6].append( float( deadMaxVec[key]))
      valuesArray[7].append( float(noisyMaxVec[key]))
      valuesArray[8].append( float( errorsVec[key]))
      valuesArray[9].append( float( clustAvgVec[key]))
      valuesArray[10].append( float( clust4StripVec[key]))
      valuesArray[11].append( float( clustMPVRVec[key]))
      valuesArray[12].append( float(clustFWHMRVec[key]))
      valuesArray[13].append( float(clustMPVPhiVec[key]))
      valuesArray[14].append( float( clustFWHMPhiVec[key]))
      valuesArray[15].append( float( clustLargeEvtsVec[key]))
      valuesArray[16].append( float( occVec[key]))
      valuesArray[17].append( float(highOccVec[key]))
      valuesArray[18].append( float(clustersPerTrackVec[key]))
      valuesArray[19].append( float(moduleMismatchVec[key]))
      valuesArray[20].append( float(highMismatchVec[key]))
      valuesArray[21].append( float(pseudoEffVec[key]))
      valuesArray[22].append( float(lowPseudoEffVec[key]))
      valuesArray[23].append( float(avgResPullVec[key]))
      if pvXVec[key]!=-99:
         valuesArray[24].append( float( pvXVec[key][:pvXVec[key].find(" +/-")]))
      else:
         valuesArray[24].append( float(pvXVec[key]))
      if pvYVec[key]!=-99:
         valuesArray[25].append( float( pvYVec[key][:pvYVec[key].find(" +/-")]))
      else:
         valuesArray[25].append( float(pvYVec[key]))
      if pvZVec[key]!=-99:
         valuesArray[26].append( float( pvZVec[key][:pvZVec[key].find(" +/-")]))
      else:
         valuesArray[26].append( float(pvZVec[key]))
      if pvLRVec[key]!=-99:
         valuesArray[27].append( float( pvLRVec[key][:pvLRVec[key].find(" +/-")]))
      else:
         valuesArray[27].append( float(pvLRVec[key]))
      if pvLRyVec[key]!=-99:
         valuesArray[28].append( float( pvLRyVec[key][:pvLRyVec[key].find(" +/-")]))
      else:
         valuesArray[28].append( float(pvLRyVec[key]))
      if pvLRzVec[key]!=-99:
         valuesArray[29].append( float( pvLRzVec[key][:pvLRzVec[key].find(" +/-")]))
      else:
         valuesArray[29].append( float(pvLRzVec[key]))
      if pvLRzVec[key]!=-99:
         valuesArray[29].append( float( pvLRzVec[key][:pvLRzVec[key].find(" +/-")]))
      else:
         valuesArray[29].append( float(pvLRzVec[key]))
      valuesArray[30].append( float(beamCentreXVec[key]))
      valuesArray[31].append( float(beamCentreYVec[key]))
      valuesArray[32].append( float(key) / 1.0e6 )
      valuesArray[33].append( float(lowADCM1Vec[key]))
      valuesArray[34].append( float(lowADCM2Vec[key]))
      valuesArray[35].append( float(avLowMultVec[key]))
      valuesArray[36].append( float(evWZeroClustVec[key]))

  graphList=[] 
  graphList.append(make_graph( "graph_avgNoiseR_vs_run", "Average noise (R)", len(keys), array('f', runArray) , array('f', valuesArray[0])))
  graphList.append(make_graph( "graph_avgNoisePhi_vs_run", "Average noise (Phi)", len(keys), array('f', runArray) , array('f', valuesArray[1])))
  graphList.append(make_graph( "graph_noisyXT_vs_run", "Number of noisy header links", len(keys), array('f', runArray) , array('f', valuesArray[2])))
  graphList.append(make_graph( "graph_residuals_vs_run", "Number of badly tuned pedestals", len(keys), array('f', runArray) , array('f', valuesArray[3])))
  graphList.append(make_graph( "graph_deadTotal_vs_run", "Bad Channels: Total number dead", len(keys), array('f', runArray) , array('f', valuesArray[4])))
  graphList.append(make_graph( "graph_noisyTotal_vs_run", "Bad Channels: Total number noisy", len(keys), array('f', runArray) , array('f', valuesArray[5])))  
  graphList.append(make_graph( "graph_deadMax_vs_run", "Bad Channels: Max number dead", len(keys), array('f', runArray) , array('f', valuesArray[6])))
  graphList.append(make_graph( "graph_noisyMax_vs_run", "Bad Channels: Max number noisy", len(keys), array('f', runArray) , array('f', valuesArray[7])))
  graphList.append(make_graph( "graph_errors_vs_run", "Number of TELL1 with >50 errors/1000", len(keys), array('f', runArray) , array('f', valuesArray[8])))
  graphList.append(make_graph( "graph_clustAvg_vs_run", "Clusters: Strips/cluster", len(keys), array('f', runArray) , array('f', valuesArray[9])))
  graphList.append(make_graph( "graph_clust4Strip_vs_run", "Clusters:% 4-strip clusters", len(keys), array('f', runArray) , array('f', valuesArray[10])))
  graphList.append(make_graph( "graph_clustMPVR_vs_run", "Clusters: MPV (R)", len(keys), array('f', runArray) , array('f', valuesArray[11])))
  graphList.append(make_graph( "graph_clustFWHMR_vs_run", "Clusters: FWHM (R)", len(keys), array('f', runArray) , array('f', valuesArray[12])))
  graphList.append(make_graph( "graph_clustMPVPhi_vs_run", "Clusters: MPV (Phi)", len(keys), array('f', runArray) , array('f', valuesArray[13])))
  graphList.append(make_graph( "graph_clustFWHMPhi_vs_run", "Clusters: FWHM (Phi)", len(keys), array('f', runArray) , array('f', valuesArray[14])))
  graphList.append(make_graph( "graph_clustLargeEvts_vs_run", "Clusters: Events with >7000 clusters / 1e4", len(keys), array('f', runArray) , array('f', valuesArray[15])))
  graphList.append(make_graph( "graph_occ_vs_run", "Avg strip occupancy %", len(keys), array('f', runArray) , array('f', valuesArray[16])))
  graphList.append(make_graph( "graph_highOcc_vs_run", "Number of high-occupancy strips", len(keys), array('f', runArray) , array('f', valuesArray[17])))
  graphList.append(make_graph( "graph_clustersPerTrack_vs_run", "Tracks: Cluster per track", len(keys), array('f', runArray) , array('f', valuesArray[18])))
  graphList.append(make_graph( "graph_moduleMismatch_vs_run", "Tracks: Avg module mismatch %", len(keys), array('f', runArray) , array('f', valuesArray[19])))
  graphList.append(make_graph( "graph_highMismatch_vs_run", "Tracks: Sensors with mismatch >20%", len(keys), array('f', runArray) , array('f', valuesArray[20])))
  graphList.append(make_graph( "graph_pseudoEff_vs_run", "Tracks: Pseudo-Efficiency", len(keys), array('f', runArray) , array('f', valuesArray[21])))
  graphList.append(make_graph( "graph_lowPseudoEff_vs_run", "Tracks: Pseudo-Efficiency <90%", len(keys), array('f', runArray) , array('f', valuesArray[22])))
  graphList.append(make_graph( "graph_avgResPull_vs_run", "Tracks: Avg Residual Pull", len(keys), array('f', runArray) , array('f', valuesArray[23])))
  graphList.append(make_graph( "graph_pvX_vs_run", "PV: Avg position (X)", len(keys), array('f', runArray) , array('f', valuesArray[24])))
  graphList.append(make_graph( "graph_pvY_vs_run", "PV: Avg position (Y)", len(keys), array('f', runArray) , array('f', valuesArray[25])))
  graphList.append(make_graph( "graph_pvZ_vs_run", "PV: Avg position (Z)", len(keys), array('f', runArray) , array('f', valuesArray[26])))
  graphList.append(make_graph( "graph_pvLR_vs_run", "PV: Avg L-R X position", len(keys), array('f', runArray) , array('f', valuesArray[27])))
  graphList.append(make_graph( "graph_pvLRy_vs_run", "PV: Avg L-R Y position", len(keys), array('f', runArray) , array('f', valuesArray[28])))
  graphList.append(make_graph( "graph_pvLRz_vs_run", "PV: Avg L-R Z position", len(keys), array('f', runArray) , array('f', valuesArray[29])))
  graphList.append(make_graph( "graph_beamCentreX_vs_run", "PV: Beam-centre local (X)", len(keys), array('f', runArray) , array('f', valuesArray[30])))
  graphList.append(make_graph( "graph_beamCentreY_vs_run", "PV: Beam-centre local (Y)", len(keys), array('f', runArray) , array('f', valuesArray[31])))
  graphList.append(make_graph( "graph_timeStamp_vs_run", "Timestamp for analysis", len(keys), array('d', runArray) , array('d', valuesArray[32])))
  graphList.append(make_graph( "graph_lowADCM1_vs_run", "Low ADC ratio close to M1 %", len(keys), array('d', runArray) , array('d', valuesArray[33])))
  graphList.append(make_graph( "graph_lowADCM2_vs_run", "Low ADC ratio close to M2 %", len(keys), array('d', runArray) , array('d', valuesArray[34])))
  graphList.append(make_graph( "graph_avLowMult_vs_run", "Mean low cluster multiplicity", len(keys), array('d', runArray) , array('d', valuesArray[35])))
  graphList.append(make_graph( "graph_evW0Clusters_vs_run", "Events with 0 clusters", len(keys), array('d', runArray) , array('d', valuesArray[36])))

  RootFile = "/tmp/DQSHistos.root"
  if len(sys.argv) > 1:
    RootFile = sys.argv[1]
  try:
    checkFile = open( RootFile )
  except IOError:
    pass
  else:
    os.remove( RootFile )
 
  ###Save the histograms to a ROOT file
  print "Writing trending histograms, please wait..." 
  fr = TFile( RootFile , "recreate")  
  fr.mkdir("Trends_vs_run")  
  fr.cd("Trends_vs_run")
  for i in range(0, len(graphList)):  
    graphList[i].Write()
  fr.Close()
  print "Finished writing trending histograms"
  for i in range(0, len(graphList)):  
    graphList[i].Delete()
  del  valuesArray, runArray
