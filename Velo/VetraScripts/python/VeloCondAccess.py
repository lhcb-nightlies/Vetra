#!/usr/bin/env python
from xml.etree import ElementTree
from CondDBUI import CondDB
import os
import sys
import time

class VeloCondAccess:
  def __init__( self, parameter_list ):
    # location of VeloCond SQLite data base
    sqlite_path = os.environ.get('VELOSQLITEDBPATH')
        
    # make PyCool accessible
    if os.path.exists('/sw/'):
      sw_dir = os.environ.get('SWDIR')
      cool_vers = os.environ.get('COOLVERS')
      cmt_cfg = os.environ.get('CMTCONFIG')
      cool_path = os.path.join(sw_dir,'lib','lcg','external','COOL',cool_vers,cmt_cfg,'python')
      sys.path.append(cool_path)
    elif os.path.exists('/afs/'):
      base_path = '/afs/cern.ch/sw/'
      cool_vers = os.environ.get('COOLVERS')
      cmt_cfg = os.environ.get('CMTCONFIG')
      cool_path = os.path.join(base_path,'lcg','app','releases','COOL',cool_vers,cmt_cfg,'python')
      sys.path.append(cool_path)
    else:
      print "Cannot find PyCool directory, please ensure you are working online as Velo_User or on lxplus. Have you used SetupProject Vetra?"

    # get current time in nanoseconds since the epoch.
    # NOTE: should be time of run, but we start out simple.
    now = long(time.mktime(time.gmtime())) * 1000000000L

    # open data base
    connection = 'sqlite_file:' + os.path.join(sqlite_path, 'VELOCOND.db') + '/VELOCOND' 
    db = CondDB(connection)

    # intitialize parameter dictionary
    self.cond_cache = {}
    sensors =  range(42) + range(64,106)
    for sensor in sensors:
      self.cond_cache[sensor] = {}
      node = '/VeloCondDB/TELL1/VeloTELL1Board%i.xml' % sensor
      xml_content = db.getXMLString(node,now)
      xml_tree = ElementTree.ElementTree()
      elem = ElementTree.fromstring(xml_content)
      xml_tree._setroot(elem)
      
      cond_elem = xml_tree.getroot().find('condition')
      param_elems = cond_elem.findall('param')
      if None != param_elems:
        param_elems.extend(cond_elem.findall('paramVector'))
      else:
        param_elems = cond_elem.findall('paramVector')
      for param in param_elems:
        param_name = param.get('name') 
        if param_name in parameter_list:
          param_type = param.get('type')
          param_values = param.text.split()

          if 'string' == param_type:
            self.cond_cache[sensor][param_name] = param_values
          elif 'int' == param_type:
            self.cond_cache[sensor][param_name] = [ int(v) for v in param_values ]
          elif 'double' == param_type or 'float' == param_type:
            self.cond_cache[sensor][param_name] = [ float(v) for v in param_values ]
          else:
            print "VeloCondAccess.__init__(): unknown parameter type '" + param_type + "'." 
            return
 
    db.closeDatabase()

  def getParameterValues( self, sensor, parameter_name ):
    return self.cond_cache[sensor][parameter_name]
  
if __name__ ==  "__main__":
  # example usage: create a dictionary of masked strips and channels.

  # if you need access to more than on parameter it is best to do
  # it from the same instance of VeloCondAccess for performance reasons.

  # if possible, put the data base access in a function such that the accessor
  # gets out of scope and frees its memory when done.

  def getMaskedStrips():
    # we'll need to convert from channels to strips
    import StripChannelMap
    channel_map = StripChannelMap.StripChannelMap()

    # parameters we are interested in. In this case only one.
    parameters = ['link_mask']
    conddb_access = VeloCondAccess(parameters)

    # create a zero-suppressed cache of masked strips
    masked_strips = {}
    sensors = range(42) + range(64,106)
    for sensor in sensors:
      masked_strips[sensor] = []
      masks = conddb_access.getParameterValues(sensor, 'link_mask')
      for tell1_channel in range(2048):
        # we don't store unmasked channels
        if 0 == masks[tell1_channel]:
          continue
  
        # the data base reflects the swapped cable order of the hardware,
        # we need to convert to chip channels
        channel = channel_map.swapCableOrder(tell1_channel)
        # and finally to strips
        strip = channel_map.channelToStrip(sensor,channel)
        
        # put strip and channel number into the zer-suppressed cache
        masked_strips[sensor].append((strip,channel))
    
    return masked_strips
 
  # get masked strips dictionary using above function
  masked_strips = getMaskedStrips()

  # example accessing  code
  sensor = 3
  print 'Sensor %i' % sensor
  print 'strip  channel'
  for (strip,channel) in masked_strips[sensor]:
    print ' %04i     %04i' % (strip, channel)


  


  

