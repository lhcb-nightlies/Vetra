//---------------------------------------------------------------------------//
//                                                                           //
//  VeloSensor class                                                         //
//  ================                                                         //
//                                                                           //
//  Helper class providing access to noise-related information.              //
//                                                                           //
//  Requirements:                                                            //
//  - needs input file with Non-Zero-Suppressed (NZS) data                   //
//    containing the directory structure                                     //
//    "Vetra/Noise/ADCCMSuppressed/TELL1_NNN/"                               //
//    and                                                                    //
//    "Vetra/Noise/DecodedADC/TELL1_NNN/"                                    //
//    with histograms RMSNoise_vs_ChipChannel and RMSNoise_vs_Strip.         //
//                                                                           //
//  Example usage:                                                           //
//  - in root, load the class with: .L sensor.h;                             //
//  - open the file you want to look at, e.g                                 //
//    TFile* f = new TFile( "filename.root" );                               //
//  - create the object specifying file and sensor number:                   //
//    VeloSensor sen( f, 77 );                                               //
//    This will create an object extracting data for sensor 77 of file f.    //
//  - create an "empty" object:                                              //
//    VeloSensor sen;                                                        //
//    And initialize it with                                                 //
//    sen.initializeSensor(f, 77);                                           //
//                                                                           //
//  @author Torkjell Huse                                                    //
//  @date   2008-10-30                                                       //
//                                                                           //
//---------------------------------------------------------------------------//

#ifndef VELOSENSOR_H
#define VELOSENSOR_H

// STL includes
#include <math.h>

// ROOT includes
#include <TH1F.h>
#include <TCanvas.h>
#include <TFile.h>

// Includes from Velo/VetraScripts
#include "helperTools.h"


//=============================================================================
// 
//=============================================================================
class VeloSensor {
private:
  
  int m_sensorNumber;
  TH1F *m_RMSNoiseADC_vs_ChipChannel;
  TH1F *m_RMSNoiseCMS_vs_ChipChannel;
  TH1F *m_RMSNoiseADC_vs_StripChannel;
  TH1F *m_RMSNoiseCMS_vs_StripChannel;
  TH1F *m_RMSNoiseADC_vs_Link;
  TH1F *m_RMSNoiseCMS_vs_Link;
  bool m_RMSNoise_vs_Link_calculated;
  bool m_sensorInitialized;
  int m_setRMSNoiseVsLink(bool exclude_first_ch=0);
  bool m_debug;

  std::string m_pathNoiseMonADCs;
  std::string m_pathNoiseMonCMSuppressedADCs;
  
public:
  VeloSensor();
  VeloSensor( TFile* f, int sensor_number, bool debug=false,
              helperTools::MoniHistosPaths* p=NULL );
  virtual ~VeloSensor();
  TH1F *getRMSNoiseADC(bool reordered=0);
  TH1F *getRMSNoiseCMS(bool reordered=0);
  //  TH1F *getNoiseVsLink(int mode, int sensor);
  int setSensorNumber(int sensor_number);
  int initializeSensor( TFile* f, int sensor_number,
                        bool debug=false,
                        helperTools::MoniHistosPaths* p=NULL );

  bool isR();
  bool isPhi();
  bool isA();
  bool isC();
  bool isTop();
  bool isBottom();
  bool isDownStream();
  int getStationNumber();
  int setRMSNoiseADC(TH1F* hist);
  int setRMSNoiseCMS(TH1F* hist);
  int drawRMSNoise(TCanvas* c, const char *option="hist");
  TH1F *getRMSNoiseVsLink(bool CMS=0);
  TH1F *getRMSNoiseVsLink(bool CMS, int first_ch);
  double getRMSNoiseSensor(bool CMS, int first_link_ch);
  double getLinkNoise(int link, bool CMS=0, int first_ch=0);
  double getLinkNoise(int link, bool CMS, int first_ch, double rms_trheshold, double absolute_threshold);
  double getLinkNoiseRMS(int link, bool CMS=0, int first_ch=0);
  double getLinkHeaderNoise(int link, bool CMS=0,bool normalized=1);
  void setDebug( bool debug=false );
  bool getDebug(){return m_debug;};

  ClassDef(VeloSensor, 0x0001); 

private:
  void setHistosPaths( helperTools::MoniHistosPaths* p );
};

//=============================================================================
#endif //VELOSENSOR_H
