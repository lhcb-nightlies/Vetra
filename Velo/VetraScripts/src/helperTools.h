//---------------------------------------------------------------------------//
//                                                                           //
//  helperTools                                                              //
//  ===========                                                              //
//                                                                           //
//  Helper tools for VELO monitoring.                                        //
//  They are typically used by other monintoring macros.                     //
//                                                                           //
//  helperTools contains for example:                                        //
//    - class to handle paths to monitoring histograms                       //
//    - function to get hold of a ROOT file or open it if not yet open       //
//    - etc.                                                                 //
//                                                                           //
//  @author Eduardo Rodrigues                                                //
//  @date   2009-07-17                                                       //
//                                                                           //
//---------------------------------------------------------------------------//
#ifndef HELPERTOOLS_H
#define HELPERTOOLS_H 1

// Includes from STL
#include <iostream>
#include <vector>
#include <string>
#include <map>

// ROOT includes
#include <TROOT.h>
#include <TStyle.h>
#include <TColor.h>
#include <TFile.h>
#include <TH1.h>
#include <TProfile.h>
#include <TClass.h>
#include <TDirectory.h>


namespace helperTools {


//=============================================================================
// Class to handle paths to monitoring histograms
//=============================================================================
class MoniHistosPaths {
 
public:
  MoniHistosPaths();
  virtual  ~MoniHistosPaths();
  std::string path( std::string algoName );
  void setPath( std::string algoName, std::string path );
  std::vector< std::string > algoNames();
  
private:
  std::map< std::string, std::string > m_paths;
};



 void setROOTColourPalette();
 TFile* getOrOpen( std::string filename );
 TProfile* ratioOf2TProfiles( TProfile* datahisto, TProfile* refhisto );
 void setHistoBlueColour( TH1* histo );

 bool isSensorRType(int sensor);
 bool isSensorPhiType(int sensor);
 

  /**
   * This class provides an interface to read histograms from ROOT
   * files by matching regular expressions to histogram names. It
   * recursively looks through subdirectories to find matches. All the
   * getter methods are templated, so essentially one can also get
   * other ROOT objects.
   *
   * NOTE: Note that with regex.h a match includes a partial match, so
   * it is recommended to anchor your regex
   *
   * + Anchored regex - ^hperBeetle.*_35_(0|1|2|3)$
   * + Normal regex - hperBeetle.*_35_(0|1|2|3)
   *
   * @author Suvayu Ali <Suvayu dot Ali at cern dot ch>
   * @date 2012-05-08 Tue
   *
   */
  class HistReader {

  public:

    /**
     * Instantiate HistReader with "dir" as the top level directory
     *
     * @param dir root directory
     */
    HistReader(TDirectory *dir) : _dir(dir) {}

    /**
     * Instantiate HistReader with "dir/subdir" as the top level directory
     *
     * @param dir root directory
     * @param subdir Subdirectory
     */
    HistReader(TDirectory *dir, std::string subdir)
    {
      _dir = dynamic_cast<TDirectory*>(dir->GetDirectory(subdir.c_str()));
    }

    ~HistReader() {}

    /**
     * Return the first histogram matching the regex
     *
     * Note that the only safe use of this method is when the regex is
     * an exact match to the object name.
     *
     * @param hregex Regular expression to match with histogram names
     * @param hist First matching histogram
     */
    template <class T>
    void getHist(std::string hregex, T* &hist)
    {
      std::vector<TObject*> histos;
      if(_dir_isvalid(_dir)) {
	_getHistVec(hregex, histos, _dir, T::Class());
	hist = dynamic_cast<T*>(histos[0]);
      }
      return;
    }

    /**
     * Get a vector of histograms matching the regular expression
     *
     * @param hregex Regular expression to match with histogram names
     * @param histograms Vector of matching histograms
     */
    template <class T>
    void getHistVec(std::string hregex, std::vector<T*> &histograms)
    {
      std::vector<TObject*> histos;
      if (_dir_isvalid(_dir)) {
	_getHistVec(hregex, histos, _dir, T::Class());
	for (unsigned i = 0; i < histos.size(); ++i) {
	  histograms.push_back(dynamic_cast<T*>(histos[i]));
	}
      }
      return;
    }

  private:

    /** 
     * Returns true if dir is valid, false otherwose.
     *
     * @param dir directory
     *
     * @return If directory is valid or not
     */
    bool _dir_isvalid(TDirectory *dir)
    {
      if (not _dir) {
	std::cerr << __func__ << " ERROR: Non-existent directory: "
		  << dir << std::endl;
	return false;
      } else {
	return true;
      }
    }

    /**
     * Get a vector of histograms matching the regular expression
     *
     * @param hregex Regular expression to match with histogram names
     * @param histos Vector of matching histograms
     * @param dir Start search in this directory
     * @param fclass TClass object for type checking
     */
    void _getHistVec(std::string hregex, std::vector<TObject*> &histos,
		     TDirectory *dir, TClass *fclass);

    TDirectory *_dir;		/**< Top level directory to search for histograms */
  };

} // End of namespace
#endif // VELORECMONITORS_HELPERTOOLS_H
