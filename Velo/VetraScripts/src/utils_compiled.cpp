/** @file utils_compiled.c
 *
 * VeloMoniGUI helper routines which need to be compiled
 *
 * @author Manuel Schiller <manuel.schiller@nikhef.nl>
 * @date 2012-03-22
 */
#include "utils_compiled.h"

// STL includes
#include <cstdio>
#include <unistd.h>
#include <cstdlib>
#include <cstdarg>
#include <iostream>

#include <TInterpreter.h>
#include <TPython.h>

long processLine(const char* fmtstr, ...)
{
  int size=512;
  std::string line;
  va_list ap;
  while (1) {
    line.resize(size);
    va_start(ap, fmtstr);
    int n = vsnprintf((char *)line.c_str(), size, fmtstr, ap);
    va_end(ap);
    if (n > -1 && n < size) {
      line.resize(n); // "line" is now exactly what we need
      break;
    }
    if (n > -1) // glibc 2.1 says how much was truncated 
      size=n+1;
    else        // glibc 2.0 says -1 if truncated, not how much
      size*=2;
  }
  std::cout << "line:: " << line << std::endl;
  return gInterpreter->ProcessLine(line.c_str());
}

void processPython(const char* fmtstr, ...)
{
  int size=512;
  std::string line;
  va_list ap;
  while (1) {
    line.resize(size);
    va_start(ap, fmtstr);
    int n = vsnprintf((char *)line.c_str(), size, fmtstr, ap);
    va_end(ap);
    if (n > -1 && n < size) {
      line.resize(n); // "line" is now exactly what we need
      break;
    }
    if (n > -1)// glibc 2.1 says how much was truncated 
      size=n+1;
    else       // glibc 2.0 says -1 if truncated, not how much
      size*=2;
  }
  std::cout << "line:: " << line << std::endl;
  TPython::Exec(line.c_str());
}
