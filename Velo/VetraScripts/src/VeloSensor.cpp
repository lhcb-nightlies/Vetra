#include "VeloSensor.h"

ClassImp(VeloSensor)

//=============================================================================
// Default constructor
//=============================================================================
VeloSensor::VeloSensor()
{
  m_RMSNoiseADC_vs_Link=new TH1F("RMSNoiseADC_vs_Link","RMS noise vs link, decoded ADC",64,-0.5,63.5);
  m_RMSNoiseCMS_vs_Link=new TH1F("RMSNoiseCMS_vs_Link","RMS noise vs link, CMS ADC",64,-0.5,63.5);
  m_RMSNoise_vs_Link_calculated=0;
}

//=============================================================================
// Constructor from an input file and a sensor number
//=============================================================================
VeloSensor::VeloSensor( TFile* f, int sensor_number, bool debug,
                        helperTools::MoniHistosPaths* p )
{
  m_debug = debug;  

  char ADC_path[100];
  char CMS_path[100];

  // get the paths to the histograms via the helperTools macro
  if ( p == NULL ) p = new helperTools::MoniHistosPaths(); // gets the defaults
  setHistosPaths( p );

  sprintf( ADC_path, "%sTELL1_%03d",
           m_pathNoiseMonADCs.c_str(), sensor_number );
  sprintf( CMS_path, "%sTELL1_%03d",
           m_pathNoiseMonCMSuppressedADCs.c_str(), sensor_number );
  
  if ( m_debug ) printf("%s \n", ADC_path);

  m_RMSNoiseADC_vs_Link=new TH1F("RMSNoiseADC_vs_Link","RMS noise vs link, decoded ADC",64,-0.5,63.5);
  m_RMSNoiseCMS_vs_Link=new TH1F("RMSNoiseCMS_vs_Link","RMS noise vs link, CMS ADC",64,-0.5,63.5);

  m_sensorNumber=sensor_number;
  if(f->GetDirectory(ADC_path)){
    f->cd(ADC_path);
    m_RMSNoiseADC_vs_ChipChannel=(TH1F*)gDirectory->Get("RMSNoise_vs_ChipChannel");
    m_RMSNoiseADC_vs_StripChannel=(TH1F*)gDirectory->Get("RMSNoise_vs_Strip");
    
    if ( m_debug ) printf("%s \n", CMS_path);
    f->cd(CMS_path);
    m_RMSNoiseCMS_vs_ChipChannel=(TH1F*)gDirectory->Get("RMSNoise_vs_ChipChannel");
    m_RMSNoiseCMS_vs_StripChannel=(TH1F*)gDirectory->Get("RMSNoise_vs_Strip");
    m_sensorInitialized=1;
  }
  else{m_sensorInitialized=0;}
  
  m_RMSNoise_vs_Link_calculated=0;
}

//=============================================================================
// Destructor
//=============================================================================
VeloSensor::~VeloSensor(){
  //  delete m_RMSNoiseADC_vs_ChipChannel;
  //  delete m_RMSNoiseCMS_vs_ChipChannel;
  
}

//=============================================================================
// 
//=============================================================================
int VeloSensor::setSensorNumber(int sensor_number){
  m_sensorNumber=sensor_number;
  return 1;
}

//=============================================================================
// 
//=============================================================================
TH1F* VeloSensor::getRMSNoiseADC(bool reordered){
  TH1F *hist=new TH1F();
  if(reordered){hist=(TH1F*)m_RMSNoiseADC_vs_StripChannel->Clone();}
  else{hist=(TH1F*)m_RMSNoiseADC_vs_ChipChannel->Clone();}
  return hist;
}

//=============================================================================
// 
//=============================================================================
TH1F* VeloSensor::getRMSNoiseCMS(bool reordered){
  TH1F *hist=new TH1F();
  if(reordered){hist=(TH1F*)m_RMSNoiseCMS_vs_StripChannel->Clone();}
  else{hist=(TH1F*)m_RMSNoiseCMS_vs_ChipChannel->Clone();}
  return hist;
}

//=============================================================================
// 
//=============================================================================
int VeloSensor::initializeSensor( TFile* f, int sensor_number,
                                  bool debug ,
                                  helperTools::MoniHistosPaths* p )
{
  char ADC_path[100];
  char CMS_path[100];

  // get the paths to the histograms via the helperTools macro
  if ( p == NULL ) p = new helperTools::MoniHistosPaths(); // gets the defaults
  setHistosPaths( p );

  sprintf( ADC_path, "%sTELL1_%03d",
           m_pathNoiseMonADCs.c_str(), sensor_number );
  sprintf( CMS_path, "%sTELL1_%03d",
           m_pathNoiseMonCMSuppressedADCs.c_str(), sensor_number );
  
  if ( debug ) printf("%s \n", ADC_path);

  m_sensorNumber=sensor_number;

  if( f -> GetDirectory( ADC_path ) ) {
    f -> cd( ADC_path );
    m_RMSNoiseADC_vs_ChipChannel=(TH1F*)gDirectory->Get("RMSNoise_vs_ChipChannel");
    m_RMSNoiseADC_vs_StripChannel=(TH1F*)gDirectory->Get("RMSNoise_vs_Strip");

    if ( m_debug ) printf("%s \n", CMS_path);
    f -> cd( CMS_path );
    m_RMSNoiseCMS_vs_ChipChannel=(TH1F*)gDirectory->Get("RMSNoise_vs_ChipChannel");
    m_RMSNoiseCMS_vs_StripChannel=(TH1F*)gDirectory->Get("RMSNoise_vs_Strip");
    m_sensorInitialized=1;
    return 1;
  } else{
    m_sensorInitialized=0;
    return 0;
  }
}

//=============================================================================
// 
//=============================================================================
int VeloSensor::setRMSNoiseADC(TH1F* hist){

  m_RMSNoiseADC_vs_ChipChannel=(TH1F*)hist->Clone();
  return 1;
}

//=============================================================================
// 
//=============================================================================
int VeloSensor::setRMSNoiseCMS(TH1F* hist){

  m_RMSNoiseCMS_vs_ChipChannel=(TH1F*)hist->Clone();
  return 1;
}

//=============================================================================
// 
//=============================================================================
int VeloSensor::drawRMSNoise(TCanvas* c, const char *option){

  c->cd();
  m_RMSNoiseADC_vs_ChipChannel->Draw(option);
  return 1;

}

//=============================================================================
// 
//=============================================================================
int VeloSensor::m_setRMSNoiseVsLink(bool exclude_first_ch){
  int channel;
  double noise_linkADC=0;
  double noise_linkCMS=0;
  int channels_in_link=0;
  for(int link=0;link<64;link++){
    for(int ch=0;ch<32;ch++){
      channel=32*link+ch;
      if(!(exclude_first_ch&&(ch==0))){
	noise_linkADC=noise_linkADC+m_RMSNoiseADC_vs_ChipChannel->GetBinContent(channel+1);
	noise_linkCMS=noise_linkCMS+m_RMSNoiseCMS_vs_ChipChannel->GetBinContent(channel+1);
	channels_in_link++;
      }
    }
    noise_linkADC=noise_linkADC/channels_in_link;
    noise_linkCMS=noise_linkCMS/channels_in_link;
    m_RMSNoiseADC_vs_Link->SetBinContent(link+1,noise_linkADC);
    m_RMSNoiseCMS_vs_Link->SetBinContent(link+1,noise_linkCMS);
    channels_in_link=0;
    noise_linkADC=0;
    noise_linkCMS=0;
  }
  m_RMSNoise_vs_Link_calculated=1;
  return 1;
}

//=============================================================================
// 
//=============================================================================
TH1F* VeloSensor::getRMSNoiseVsLink(bool CMS){
  TH1F *tmp;
  if(CMS){
    tmp=(TH1F*)m_RMSNoiseCMS_vs_Link->Clone();
  }
  else{
    tmp=(TH1F*)m_RMSNoiseADC_vs_Link->Clone();
  }
  return tmp;
}

//=============================================================================
// 
//=============================================================================
double VeloSensor::getRMSNoiseSensor(bool CMS, int first_link_ch){
  int channel;
  double noise_sensor=0;
  int channels_in_sensor=0;
  TH1F *RMSNoise;
  
  if(CMS){RMSNoise=m_RMSNoiseCMS_vs_ChipChannel;}
  
  else{RMSNoise=m_RMSNoiseADC_vs_ChipChannel;}
  
  for(int link=0;link<64;link++){
    for(int ch=first_link_ch;ch<32;ch++){
      channel=32*link+ch;
      noise_sensor=noise_sensor+RMSNoise->GetBinContent(channel+1);
      channels_in_sensor++;
    }
  }
  
  noise_sensor=noise_sensor/channels_in_sensor;
  
  return noise_sensor;
}

//=============================================================================
// 
//=============================================================================
TH1F *VeloSensor::getRMSNoiseVsLink(bool CMS, int first_ch){
  int channel;
  double chNoise;
  double link_noise=0;
  TH1F *RMSNoise_vs_ChipChannel;
  TH1F *linkHist=new TH1F("link","link RMS Noise",500,0,50);
  TH1F *RMSNoise_vs_Link=new TH1F("RMSNoise_vs_Link","RMS Noise vs Link",64,-0.5,63.5);
  if(CMS){RMSNoise_vs_ChipChannel=(TH1F*)m_RMSNoiseCMS_vs_ChipChannel->Clone();}
  else{RMSNoise_vs_ChipChannel=(TH1F*)m_RMSNoiseADC_vs_ChipChannel->Clone();}
  
  for(int link=0;link<64;link++){
    for(int ch=first_ch;ch<32;ch++){
      channel=32*link+ch;
      chNoise=RMSNoise_vs_ChipChannel->GetBinContent(channel+1);
      link_noise=link_noise+chNoise;
      linkHist->Fill(chNoise);
    }
    link_noise=link_noise/(32-first_ch);
    RMSNoise_vs_Link->Fill(link,link_noise);
    RMSNoise_vs_Link->SetBinError(link+1,linkHist->GetRMS());
    linkHist->Reset();
  }
  delete linkHist;
  return RMSNoise_vs_Link;
}

//=============================================================================
// 
//=============================================================================
double VeloSensor::getLinkNoise(int link, bool CMS, int first_ch){
  TH1F *RMSNoise;
  if(CMS){RMSNoise=m_RMSNoiseCMS_vs_ChipChannel;}
  else{RMSNoise=m_RMSNoiseADC_vs_ChipChannel;}
  double link_noise=0;
  int first_chip_channel=32*link+first_ch;
  int last_chip_channel=32*link+32;
  for(int ch=first_chip_channel;ch<last_chip_channel;ch++){
    link_noise=link_noise+RMSNoise->GetBinContent(ch+1);
  }
  link_noise=link_noise/(32-first_ch);

  return link_noise;
}

//=============================================================================
// 
//=============================================================================
double VeloSensor::getLinkNoiseRMS(int link, bool CMS, int first_ch){
  TH1F *RMSNoise;
  if(CMS){RMSNoise=m_RMSNoiseCMS_vs_ChipChannel;}
  else{RMSNoise=m_RMSNoiseADC_vs_ChipChannel;}
  double link_noise_mean=0;
  double link_noise_rms=0;
  double channel_noise;
  int first_chip_channel=32*link+first_ch;
  int last_chip_channel=32*link+32;
  for(int ch=first_chip_channel;ch<last_chip_channel;ch++){
    link_noise_mean=link_noise_mean+RMSNoise->GetBinContent(ch+1);
  }
  link_noise_mean=link_noise_mean/(32-first_ch);

  for(int ch=first_chip_channel;ch<last_chip_channel;ch++){
    channel_noise=RMSNoise->GetBinContent(ch+1);
    link_noise_rms=link_noise_rms+(link_noise_mean-channel_noise)*(link_noise_mean-channel_noise);
  }
  link_noise_rms=link_noise_rms/(32-first_ch);
  link_noise_rms=sqrt(link_noise_rms);

  return link_noise_rms;
}

//=============================================================================
// 
//=============================================================================
double VeloSensor::getLinkHeaderNoise(int link, bool CMS,bool normalized){

  TH1F *RMSNoise;
  if(CMS){RMSNoise=m_RMSNoiseCMS_vs_ChipChannel;}
  else{RMSNoise=m_RMSNoiseADC_vs_ChipChannel;}

  double header_noise=RMSNoise->GetBinContent(32*link+1);

  if(normalized){
    header_noise=header_noise/getLinkNoise(link, CMS, 1);
  }

  return header_noise;
}

//=============================================================================
// 
//=============================================================================
double VeloSensor::getLinkNoise(int link, bool CMS, int first_ch, double rms_threshold, double absolute_threshold){

  TH1F *RMSNoise;
  if(CMS){RMSNoise=m_RMSNoiseCMS_vs_ChipChannel;}
  else{RMSNoise=m_RMSNoiseADC_vs_ChipChannel;}

  double link_noise_mean=getLinkNoise(link,CMS,1);
  double link_noise_rms=getLinkNoiseRMS(link,CMS,1);

  int first_chip_channel=32*link+first_ch;
  int last_chip_channel=32*link+32;

  int good_channels=0;
  double link_noise=0;

  for(int ch=first_chip_channel;ch<last_chip_channel;ch++){
    double channel_noise=RMSNoise->GetBinContent(ch+1);
    double delta=sqrt((channel_noise-link_noise_mean)*(channel_noise-link_noise_mean));
    if((delta<rms_threshold*link_noise_rms)&&(channel_noise<absolute_threshold)){
      link_noise=link_noise+channel_noise;
      good_channels++;
    }
  }
  if(good_channels>0){link_noise=link_noise/good_channels;}
  else{link_noise=-1;}

  return link_noise;

}

//=============================================================================
// Returns true for Phi-sensors and false for R-sensors
//=============================================================================
bool VeloSensor::isPhi()
{
  if(m_sensorNumber>63){return 1;}
  else{return 0;}
}

//=============================================================================
// Returns true for R-sensors (sensor number>64), and false for Phi-sensors
//=============================================================================
bool VeloSensor::isR()
{
  if ( m_sensorNumber > 63 ) { return 0; }
  else { return 1; }
}

//=============================================================================
// Returns true if the sensor is on the A-side
//=============================================================================
bool VeloSensor::isA(){
  if(m_sensorNumber%2==0){return 1;}
  else{return 0;}

}

//=============================================================================
// Returns true if the sensor is on the C-side
//=============================================================================
bool VeloSensor::isC(){
  if(m_sensorNumber%2==0){return 0;}
  else{return 1;}

}

//=============================================================================
// Returns true if the sensor is a top sensor (i.e. cables on the top of VELO)
//=============================================================================
bool VeloSensor::isTop(){
  if(isR()){
    if((m_sensorNumber%4==0)||(m_sensorNumber%4==1)){return 0;}
    else{return 1;}
  }
  else{
    if((m_sensorNumber%4==2)||(m_sensorNumber%4==3)){return 0;}
    else{return 1;}
  }

}

//=============================================================================
// Returns true if the sensor is a botton sensor (i.e. cables under VELO)
//=============================================================================
bool VeloSensor::isBottom(){
  if(isR()){
    if((m_sensorNumber%4==0)||(m_sensorNumber%4==1)){return 1;}
    else{return 0;}
  }
  else{
    if((m_sensorNumber%4==2)||(m_sensorNumber%4==3)){return 1;}
    else{return 0;}
  }

}

//=============================================================================
// Returns true for downstream sensors
//=============================================================================
bool VeloSensor::isDownStream(){

  if(isA()){
    if(isPhi()){
      if(m_sensorNumber%4==0){return 1;}
      else{return 0;}
    }
    else if(isR()){
      if(m_sensorNumber%4==2){return 1;}
      else{return 0;}
    }
    else{return 0;}
  }
  
  else {
    if(isPhi()){
      if(m_sensorNumber%4==3){return 1;}
      else{return 0;}
    }
    else if(isR()){
      if(m_sensorNumber%4==1){return 1;}
      else{return 0;}
    }
    else{return 0;}
  }
}

//=============================================================================
// 
//=============================================================================
int VeloSensor::getStationNumber(){
  int station_number=0;
  if(m_sensorNumber<42){
    station_number=m_sensorNumber/2+1;
  }
  else if(m_sensorNumber>63){
    station_number=(m_sensorNumber-64)/2+1;
  }
  else{station_number=-1;}

  //skip station 17 and 18
  if(station_number>16){
    station_number=station_number+2;
  }

  //skip station 20 and 21
  if(station_number>19){
    station_number=station_number+2;
  }

  return station_number;
}

//=============================================================================
// 
//=============================================================================
void VeloSensor::setDebug( bool debug ) {
  m_debug = debug;
}

//=============================================================================
// Class private function. Allows to change the path to the monitoring histos
//=============================================================================
void VeloSensor::setHistosPaths( helperTools::MoniHistosPaths* p )
{
  // Full path to the NoiseMon histograms for raw ADCs
  m_pathNoiseMonADCs =
    p->path( "NoiseMon" ) + "/DecodedADC/";
  // Full path to the NoiseMon histograms for CMS ADCs
  m_pathNoiseMonCMSuppressedADCs =
    p -> path( "NoiseMon" ) + "/ADCCMSuppressed/";
}

//=============================================================================
