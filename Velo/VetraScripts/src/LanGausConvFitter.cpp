//---------------------------------------------------------------------------//
//                                                                           //
//  LanGausConvFitter                                                        //
//  =================                                                        //
//                                                                           //
//  Fit a Landau convolved with a Gaussian. See header file for James'       //
//  description                                                              //
//                                                                           //
//  @author James Mylroie-Smith                                              //
//  @date   2009-03-09                                                       //
//                                                                           //
//---------------------------------------------------------------------------//

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <sstream>
#include "Rtypes.h"
#include <TROOT.h>
#include <TObject.h>
#include <TF1.h>
#include <TH1.h>
#include <TTree.h>
#include <TMath.h>
#include <TVirtualFitter.h>
#include "TPaveStats.h"
#include <TSystem.h>
#include <TStyle.h>
#include <TPad.h>


#include "LanGausConvFitter.h"

double ClusterFitter::Evaluate (double *x, double *par){
  // Numeric constants
  Double_t invsq2pi = 0.3989422804014;   // (2 pi)^(-1/2)
  Double_t mpshift  = -0.22278298;       // Landau maximum location

  // Control constants
  Double_t np = 100.0;      // number of convolution steps
  Double_t sc =   5.0;      // convolution extends to +-sc Gaussian sigmas
  
  // Variables
  Double_t xx;
  Double_t mpc;
  Double_t fland;
  Double_t sum = 0.0;
  Double_t xlow,xupp;
  Double_t step;
  Double_t i;
  // MP shift correction
  mpc = par[1] - mpshift * par[0];
  // Range of convolution integral
  xlow = x[0] - sc * par[3];
  xupp = x[0] + sc * par[3];
  step = (xupp-xlow) / np;
  // Convolution integral of Landau and Gaussian by sum
  for(i=1.0; i<=np/2; i++) {
    xx = xlow + (i-.5) * step;
    fland = TMath::Landau(xx,mpc,par[0]) / par[0];
    sum += fland * TMath::Gaus(x[0],xx,par[3]);
    
    xx = xupp - (i-.5) * step;
    fland = TMath::Landau(xx,mpc,par[0]) / par[0];
    sum += fland * TMath::Gaus(x[0],xx,par[3]);
  }
  return (par[2] * step * sum * invsq2pi / par[3]);
}


//=============================================================================
// Standard constructors, initialises variables
//=============================================================================
ClusterFitter::ClusterFitter()
{
}
//=============================================================================
// Destructor
//=============================================================================
ClusterFitter::~ClusterFitter() {} 

TF1 *ClusterFitter::langaufit(TH1D *his, Double_t *fitrange, Double_t *startvalues, 
               Double_t *parlimitslo, Double_t *parlimitshi, Double_t fitparams[4],
               Double_t *fiterrors, Double_t *ChiSqr, Int_t *NDF)// const char *status)
{
   // Once again, here are the Landau * Gaussian parameters:
   //   par[0]=Width (scale) parameter of Landau density
   //   par[1]=Most Probable (MP, location) parameter of Landau density
   //   par[2]=Total area (integral -inf to inf, normalization constant)
   //   par[3]=Width (sigma) of convoluted Gaussian function
   //
   // Variables for langaufit call:
   //   his             histogram to fit
   //   fitrange[2]     lo and hi boundaries of fit range
   //   startvalues[4]  reasonable start values for the fit
   //   parlimitslo[4]  lower parameter limits
   //   parlimitshi[4]  upper parameter limits
   //   fitparams[4]    returns the final fit parameters
   //   fiterrors[4]    returns the final fit errors
   //   ChiSqr          returns the chi square
   //   NDF             returns ndf

   Int_t i;
   Char_t FunName[100];
   //Double_t FitResult(9);
   sprintf(FunName,"Fitfcn_%s",his->GetName());

   TF1 *ffitold = (TF1*)gROOT->GetListOfFunctions()->FindObject(FunName);
   if (ffitold) delete ffitold;
   ClusterFitter *test=new ClusterFitter();
   TF1 *ffit = new TF1(FunName,test, &ClusterFitter::Evaluate,fitrange[0],fitrange[1],4,"ClusterFitter","Evaluate");
   
   ffit->SetParameters(startvalues);

   ffit->SetParNames("Width","MP","Area","GSigma");
   TVirtualFitter::SetDefaultFitter("Minuit2") ;
   // sets the minimisation package 
   for (i=0; i<4; i++) {
     ffit->SetParLimits(i, parlimitslo[i], parlimitshi[i]);
   }
   // fit within specified range, use ParLimits, do not plot
   //FitResult=his->Fit(FunName,"RBQ");
   his->Fit(FunName,"RBQ");
   
   ffit->GetParameters(fitparams);    // obtain fit parameters
   for (i=0; i<4; i++) {
     fiterrors[i] = ffit->GetParError(i);     // obtain fit parameter errors
   }
   ChiSqr[0] = ffit->GetChisquare();  // obtain chi^2
   NDF[0] = ffit->GetNDF();           // obtain ndf
   return (ffit);              // return fit function
}


// //==========================================================================
// //  Finds MPV and FWHM
// //==========================================================================

Int_t ClusterFitter::langaupro(double *params, double &maxx, double &FWHM) {
  // Seaches for the location (x value) at the maximum of the
  // Landau-Gaussian convolute and its full width at half-maximum.
  //
  // The search is probably not very efficient, but it's a first try.
  
  Double_t p(0),x(0),fy,fxr,fxl;
  Double_t step;
  Double_t l,lold;
  Int_t i = 0;
  Int_t MAXCALLS = 10000;
  ClusterFitter * langaufun = new ClusterFitter();   
  // Search for maximum
  p = params[1] - 0.1 * params[0];
  step = 0.05 * params[0];
  lold = -2.0;
  l    = -1.0;
  
  while ( (l != lold) && (i < MAXCALLS) ) {
    i++;
    
    lold = l;
    x = p + step;
    l =  langaufun->Evaluate(&x,params);
    if (l < lold){
      step = -step/10;}
    p += step;
  }
  
  if (i == MAXCALLS)
    return (-1);
  maxx = x;
  fy = l/2;
  // Search for right x location of fy
  p = maxx + params[0];
  step = params[0];
  lold = -2.0;
  l    = -1e300;
  i    = 0;
  while ( (l != lold) && (i < MAXCALLS) ) {
    i++;
    lold = l;
    x = p + step;
    l = TMath::Abs(langaufun->Evaluate(&x,params) - fy);
    if (l > lold)
      step = -step/10;
    p += step;
  }
  if (i == MAXCALLS)
    return (-2);
  fxr = x;
  // Search for left x location of fy
  p = maxx - 0.5 * params[0];
  step = -params[0];
  lold = -2.0;
  l    = -1e300;
  i    = 0;
  while ( (l != lold) && (i < MAXCALLS) ) {
    i++;
    lold = l;
    x = p + step;
    l = TMath::Abs(langaufun->Evaluate(&x,params) - fy);
    if (l > lold)
      step = -step/10;
    p += step;
  }
  if (i == MAXCALLS)
    return (-3);
  fxl = x;
  FWHM = fxr - fxl;
  return (0);
}
//=============================================================================
//  Function Opening file and finding tree for plotting and then fitting
//=============================================================================

TF1* ClusterFitter::langausData(TH1D *hSNR,double &MPV,double &FWHM,
                                double FitRangeLow, double FitRangeHi){
  if(!hSNR){
    std::cout << "Could not read histogram name " << hSNR->GetName() << std::endl;
  }
  Double_t fr[2];
  fr[0]= FitRangeLow*(hSNR->GetMean());
  fr[1]= FitRangeHi*(hSNR->GetMean());  
  // Setting fit range and start values
  Double_t  pllo[4], plhi[4], fp[4], fpe[4], mean;
  mean = hSNR->GetMean();
    pllo[0]=(hSNR->GetRMS())/10;
  pllo[1]=0.07;
  pllo[2]=1;
  pllo[3]=0.00001;
  plhi[0]=(hSNR->GetRMS())*10;
  plhi[1]=2*mean;
  plhi[2]=hSNR->GetSum()*100;
  plhi[3]=100;
  sv[0]=(hSNR->GetRMS())*0.95;
  sv[1]=hSNR->GetBinCenter(hSNR->GetMaximumBin())*.98;
  sv[2]=hSNR->GetSum();//area;
  sv[3]=mean/7.2;
  
  Double_t chisqr;
  Int_t    ndf;
  Double_t SNRPeak, SNRFWHM;
  
  TF1 *fitsnr = langaufit(hSNR,fr,sv,pllo,plhi,fp,fpe,&chisqr,&ndf);//Status);

  langaupro(fp,SNRPeak,SNRFWHM);
  
  MPV = SNRPeak;
  FWHM = SNRFWHM;
  // Global style settings
  gStyle->SetOptStat(0100);
  gStyle->SetOptFit(2222);
  hSNR->Draw();
  fitsnr -> SetLineColor(2);
  fitsnr->Draw("lsame");
  return (fitsnr); 
}
