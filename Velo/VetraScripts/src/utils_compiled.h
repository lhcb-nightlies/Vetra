/** @file utils_compiled.h
 *
 * VeloMoniGUI helper routines which need to be compiled
 *
 * @author Manuel Schiller <manuel.schiller@nikhef.nl>
 * @date 2012-03-22
 */

#ifndef UTILS_COMPILED_H
#define UTILS_COMPILED_H 1

#include <cstdarg>
#include "TPython.h"

long processLine(const char* fmt, ...);
void processPython(const char* fmt, ...);

#endif // UTILS_COMPILED_H
