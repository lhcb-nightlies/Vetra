//---------------------------------------------------------------------------//
//                                                                           //
//  helperTools                                                              //
//  ===========                                                              //
//                                                                           //
//  Helper tools for VELO monitoring.                                        //
//  They are typically used by other monintoring macros.                     //
//                                                                           //
//  helperTools contains for example:                                        //
//    - class to handle paths to monitoring histograms                       //
//    - function to get hold of a ROOT file or open it if not yet open       //
//    - etc.                                                                 //
//                                                                           //
//  @author Eduardo Rodrigues                                                //
//  @date   2009-07-17                                                       //
//                                                                           //
//---------------------------------------------------------------------------//

// Includes from STL
#include <iostream>
#include <string>
#include <map>

// for POSIX regex
#include <sys/types.h>
#include <regex.h>

// ROOT includes
#include <TROOT.h>
#include <TStyle.h>
#include <TColor.h>
#include <TFile.h>
#include <TH1.h>
#include <TProfile.h>
#include <TKey.h>
#include <TCollection.h>

#include "helperTools.h"

namespace helperTools {
  
  //=============================================================================
  // Class constructor
  //=============================================================================
  MoniHistosPaths::MoniHistosPaths()
  {
    // default paths to histograms
    m_paths[ "NoiseMon"                   ] = "Vetra/NoiseMon";
    m_paths[ "ADCMon"                     ] = "Vetra/ADCMon";
    m_paths[ "GainMon"                    ] = "Vetra/GainMon";
    m_paths[ "BadChannelMon"              ] = "Vetra/BadChannelMon";
    m_paths[ "ErrorMon"                   ] = "Velo/ErrorMon";
    m_paths[ "VeloPedestalSubtractorMoni" ] = "Vetra/VeloPedestalSubtractorMoni";
    m_paths[ "VeloOccupancyMonitor"       ] = "Velo/VeloOccupancyMonitor";
    m_paths[ "VeloSamplingMonitor"        ] = "Velo/VSM";
    m_paths[ "VeloClusterMonitor"         ] = "Velo/VeloClusterMonitor";
    m_paths[ "VeloExpertClusterMonitor"   ] = "Velo/VeloExpertClusterMonitor";
    m_paths[ "VeloTrackMonitor"           ] = "Velo/VeloTrackMonitor";
    m_paths[ "VeloIPResolutionMonitor"    ] = "Velo/VeloIPResolutionMonitor";
    m_paths[ "TrackVertexMonitor"         ] = "Track/TrackVertexMonitor";
  }
  
  //=============================================================================
  // Class destructor. Does nothing
  //=============================================================================
  MoniHistosPaths::~MoniHistosPaths() {
  }
  
  //=============================================================================
  // Get the full path to the histograms produced by a given algorithm
  //=============================================================================
  std::string MoniHistosPaths::path( std::string algoName )
  {
    return m_paths[ algoName ];
  }
  
  //=============================================================================
  // Set the full path to the histograms produced by a given algorithm
  //=============================================================================
  void MoniHistosPaths::setPath( std::string algoName, std::string newpath )
  {
    m_paths[ algoName ] = newpath;
  }
  //=============================================================================
  // 
  //=============================================================================
  std::vector< std::string > MoniHistosPaths::algoNames()
  {
    std::vector< std::string > vecAlgoNames;
    for ( std::map< std::string, std::string >::iterator algoName = m_paths.begin();
	  algoName != m_paths.end(); ++algoName ) {
      vecAlgoNames.push_back( algoName -> first );
    }
    return vecAlgoNames;
  }

//=============================================================================
// Set a pretty ROOT colour palette
//=============================================================================
void setROOTColourPalette()
{
    const Int_t NRGBs = 5;
    const Int_t NCont = 255;
    
    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable( NRGBs, stops, red, green, blue, NCont );
    gStyle -> SetNumberContours( NCont );
}

//=============================================================================
// Get hold of a ROOT file or open it if not yet open
//=============================================================================
TFile* getOrOpen( std::string filename )
{
  // Look for the opened file searching by name. Open it if not found
  TFile* file =
    (TFile*) gROOT -> GetListOfFiles() -> FindObject( filename.c_str() );
  
  if ( file != NULL )
    return file;
  else
    return new TFile( filename.c_str() );      
}

//=============================================================================
// Perform the division of 2 profile histograms bin-by-bin
//=============================================================================
TProfile* ratioOf2TProfiles( TProfile* datahisto, TProfile* refhisto )
{
  TProfile* ratioHisto       = ( TProfile*) datahisto -> Clone();
  const TH1D* projXRefHisto  = refhisto -> ProjectionX();
  
  ratioHisto -> Divide( projXRefHisto );  
  
  projXRefHisto = NULL;
  delete projXRefHisto;
  
  return ratioHisto;
}

//=============================================================================
// Set a blue colour (and darker contour) to a histogram
//=============================================================================
  void setHistoBlueColour( TH1* histo )
{
  histo -> SetFillColor( kBlue-10 );
  histo -> SetLineColor( kBlue );
}

//=============================================================================
// Check if the sensor is an R-sensor
//=============================================================================
  bool isSensorRType( int sensor )
{
    return (sensor >= 0) && (sensor <= 41);
}

//=============================================================================
// Check if the sensor is an Phi-sensor
//=============================================================================
  bool isSensorPhiType( int sensor )
{
    return (sensor >= 64) && (sensor <= 105);
}

  //==================================================
  // HistReader implementation
  //==================================================
  void HistReader::_getHistVec(std::string hregex,
			       std::vector<TObject*> &histos,
			       TDirectory *dir, TClass *fclass)
  {
    TIter keylist(dir->GetListOfKeys());
    TKey *key;

    while ((key = dynamic_cast<TKey*> (keylist()))) {
      TClass *objClass = TClass::GetClass(key->GetClassName());

      // recurse through subdirectories
      if (objClass->InheritsFrom(TDirectory::Class())) {
	TDirectory *subdir = dynamic_cast<TDirectory*>(key->ReadObj());
	_getHistVec(hregex, histos, subdir, fclass);
      }

      // get histogram if there is a match
      if (objClass->InheritsFrom(fclass)) {
	std::string keyname(key->GetName());

	int status(0);
	regex_t regex;
	if (0 != (status = regcomp(&regex, hregex.c_str(), REG_EXTENDED))) {
	  int length = regerror(status, &regex, NULL, size_t(0));
	  char * errmsg = new char[length];
	  regerror(status, &regex, errmsg, length);
	  std::cout << "Bad regex: " << errmsg << std::endl;
	  delete errmsg;
	} else {
	  status = regexec(&regex, keyname.c_str(), size_t(0), NULL, 0);
	  regfree(&regex);
	  if (not status) {
	    histos.push_back(key->ReadObj());
	  }
	}
      }
    }

    return;
  }
//=============================================================================
} // End of namespace
