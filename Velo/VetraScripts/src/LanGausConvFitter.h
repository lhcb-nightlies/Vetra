//---------------------------------------------------------------------------//
//                                                                           //
//  LanGausConvFitter                                                        //
//  =================                                                        //
//  In the Landau distribution (represented by the CERNLIB approximation),   //
//  the maximum is located at x=-0.22278298 with the location parameter=0.   //
//  This shift is corrected within this function, so that the actual         //
//  maximum is identical to the MP parameter.                                //
//                                                                           //
//  Example usage ROOT: (use the header file in macros/ )                    //
/*
TFile file = TFile("~/Functions/fileADCsum.root")                      
TH1D *histoR = file.Get("histoR")  
histoR->Draw()                                      
Double_t MPV                                                             
Double_t FWHM                                                            
clust = ClusterFitter()                                    
clust.langausData(histoR,MPV,FWHM)         
TPaveStats *st = (TPaveStats*)histoR->GetListOfFunctions()->FindObject("stats");
TString mpv;
mpv = "MPV ";
mpv+= MPV;
st->AddText(mpv) 
st->DrawClone()       
*/
//  Example useage PyRoot:
/*
from ROOT import *
f=TFile("~/Functions/fileADCsum.root")
h1 = f.Get("histoP")
h1.Draw()
FWHM = Double()
MPV = Double()
Err = Double()
FH = Double()
FL = Double()
fit = TF1()
FH = 2.
FL = 0.2
cluster = ClusterFitter()
fit = cluster.langausData(h1,MPV,FWHM,FL,FH)
*/
//                                                                           //
//  @author James Mylroie-Smith                                              //
//  @date   2009-03-09                                                       //
//                                                                           //
//---------------------------------------------------------------------------//
#ifndef LANGAUSCONVFITTER_H
#define LANGAUSCONVFITTER_H

#include <TF1.h>
#include <TH1D.h>

class ClusterFitter {
public: 
  ClusterFitter();
  double Evaluate (double *x, double *par);
  virtual ~ClusterFitter( ); ///< Destructor
  //protected:
  //private:
  TF1 *langaufit(TH1D *his, Double_t *fitrange, Double_t *startvalues, 
                 Double_t *parlimitslo, Double_t *parlimitshi, 
                 Double_t *fitparams,
                 Double_t *fiterrors, Double_t *ChiSqr, Int_t *NDF);
  Int_t langaupro(double *params, double &maxx, double &FWHM) ;
  TF1* langausData(TH1D *hSNR,
                   double &MPV,
                   double &FWHM,
                   double FitRangeLow= .8 ,
                   double FitRangeHi=2.                 );
  Double_t sv[4];
};
#endif

