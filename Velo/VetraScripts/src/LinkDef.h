#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class VeloSensor+;
#pragma link C++ class ClusterFitter+;
#pragma link C++ class helperTools::MoniHistosPaths+;

#pragma link C++ namespace helperTools;
#pragma link C++ function processLine;
#pragma link C++ function processPython;

#endif
