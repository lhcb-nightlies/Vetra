//---------------------------------------------------------------------------//
//                                                                           //
//  drawNoiseSummaryPlots                                                    //
//  =====================                                                    //
//                                                                           //
//  Draws three summary plots:                                               //
//  1. noise versus link for all sensors                                     //
//  2. average sensor noise for all sensors                                  //
//  3. header noise versus link for all sensors                              //
//                                                                           //
//  Requirements:                                                            //
//  - needs input file with Non-Zero-Suppressed (NZS) data                   //
//    containing the directory structure                                     //
//    "Vetra/Noise/ADCCMSuppressed/TELL1_NNN/"                               //
//    and                                                                    //
//    "Vetra/Noise/DecodedADC/TELL1_NNN/"                                    //
//    with histograms RMSNoise_vs_ChipChannel and RMSNoise_vs_Strip.         //
//                                                                           //
//  Example usage:                                                           //
//  - in root, run with                                                      //
//    .L drawNoiseSummaryPlots.C                                             //
//    drawNoiseSummaryPlots("myfile.root")                                   //
//                                                                           //
//  @author Eduardo Rodrigues                                                //
//  @date   2009-03-05                                                       //
//  @author Torkjell Huse                                                    //
//  @date   2008-10-30                                                       //
//                                                                           //
//---------------------------------------------------------------------------//

// Includes from STL
#include <utility>
#include <iostream>

#include <TH2F.h>
// Includes from Velo/VetraScripts

#include "../src/VeloSensor.h"
//#include "../src/helperTools.h"


//=============================================================================
// Class encapsulating the work
//=============================================================================
class NoiseSummaryPlot {
  
public:
  NoiseSummaryPlot( std::string fileName, std::string refFileName );
  virtual ~NoiseSummaryPlot();
  void draw( TCanvas* canvas );
  void drawSummaryNoiseDists( TCanvas* canvas );
  std::pair<double,double> getNoiseSummary();
  std::vector<double> getBadChannels(std::string fileName);

private:
  TFile* m_file;
  TFile* m_StandReffile;

  
};

//=============================================================================
// 
//=============================================================================
NoiseSummaryPlot::NoiseSummaryPlot( std::string fileName, std::string refFileName )
{
  // either look at the opened file or open it!
  m_file = helperTools::getOrOpen( fileName );
  

  //standard noise file for dqs dead/noisy monitor
  m_StandReffile = helperTools::getOrOpen( refFileName );

  if ( m_file -> IsZombie() )
    printf( " ==> Zombie file !\n" );

  if ( m_StandReffile -> IsZombie() )
    printf( " ==> Zombie standard ref file !\n" );
}

//=============================================================================
// 
//=============================================================================
NoiseSummaryPlot::~NoiseSummaryPlot() {
}

//=============================================================================
// 
//=============================================================================
void NoiseSummaryPlot::draw( TCanvas* canvas )
{  
  canvas -> cd();
  
  VeloSensor sen;
  
  //set parameters for noise calculations
  bool CMS=1; //set CMS=1 for CMS data
  bool header_normalized=1; //set=1 to normalize header noise with average noise of link
  int first_ch=1; //first_ch=n skips the first n channels when average noise is calculated
  
  //set parameters for color palette, cut-off of 2D histograms
  double RMSNoiseLinkMaximum    = 3;
  double RMSNoiseLinkMinimum    = 1;
  double HeaderNoiseLinkMaximum = 1.5;
  double HeaderNoiseLinkMinimum = 0.5;
  
  TH1F* RMSNoiseSensors =
    new TH1F( "RMSNoiseSensors", "RMS noise vs sensors", 110, -0.5, 109.5 );
  //  TH1F* RMSNoiseRSensors =
  //  new TH1F( "RMSNoiseRSensors", "RMS noise vs sensors", 42,-0.5,41.5);
  //TH1F* RMSNoisePhiSensors =
  //  new TH1F( "RMSNoisePhiSensors", "RMS noise vs sensors", 42, 62.5, 103.5 );
  
  TH2F* RMSNoise_vs_linkSensors =
    new TH2F( "RMSNoise_vs_linkSensors", "RMS noise vs links for all sensors",
              110, -0.5, 109.5, 64, -0.5, 63.5 );
  TH2F* HeaderNoise_vs_linkSensors =
    new TH2F( "HeaderNoise_vs_linkSensors",
              "Header noise vs links for all sensors",
              110, -0.5, 109.5, 64, -0.5, 63.5 );
  
  canvas -> Divide( 1, 3 );
  
  for(int tell=0;tell<106;tell++){
    if(sen.initializeSensor(m_file,tell)){
      double sensor_noise=sen.getRMSNoiseSensor(CMS, first_ch);
      RMSNoiseSensors->Fill(tell, sensor_noise);
      for(int link=0;link<64;link++){
        double link_noise=sen.getLinkNoise(link, CMS, first_ch);
        double header_noise=sen.getLinkHeaderNoise(link, CMS, header_normalized);
        RMSNoise_vs_linkSensors->Fill(tell,link,link_noise);
        HeaderNoise_vs_linkSensors->Fill(tell,link,header_noise);
      }
    }

  }
  gStyle->SetPalette(1);

  RMSNoise_vs_linkSensors->SetMaximum(RMSNoiseLinkMaximum);
  RMSNoise_vs_linkSensors->SetMinimum(RMSNoiseLinkMinimum);

  HeaderNoise_vs_linkSensors->SetMaximum(HeaderNoiseLinkMaximum);
  HeaderNoise_vs_linkSensors->SetMinimum(HeaderNoiseLinkMinimum);

  RMSNoiseSensors->SetStats(kFALSE);
  RMSNoiseSensors->SetFillColor(kRed);
  RMSNoiseSensors->SetFillStyle(1001);
  RMSNoiseSensors->GetXaxis()->SetTitle("Sensor number");
  RMSNoiseSensors->GetYaxis()->SetTitle("RMS noise");

  RMSNoise_vs_linkSensors->SetStats(kFALSE);
  RMSNoise_vs_linkSensors->GetXaxis()->SetTitle("Sensor number");
  RMSNoise_vs_linkSensors->GetYaxis()->SetTitle("Link number");
  HeaderNoise_vs_linkSensors->SetStats(kFALSE);
  HeaderNoise_vs_linkSensors->GetXaxis()->SetTitle("Sensor number");
  HeaderNoise_vs_linkSensors->GetYaxis()->SetTitle("Link number");

  canvas -> cd(1);
  RMSNoiseSensors -> Draw();
  canvas -> cd(2);
  RMSNoise_vs_linkSensors -> Draw( "colz" );
  canvas -> cd(3);
  HeaderNoise_vs_linkSensors -> Draw( "colz" );
  
}

//=============================================================================
// Draw the RMS noise distributions for R/Phi sensors
//=============================================================================
void NoiseSummaryPlot::drawSummaryNoiseDists( TCanvas* canvas )
{
  if ( m_file -> IsZombie() ) {
    printf( "Zombie file ==> Nothing drawn !\n" );
    return;
  }
  
  canvas -> cd();
  canvas -> Clear();
  canvas -> Divide( 2, 1 );
  
  VeloSensor sen;
  
  //set parameters for noise calculations
  bool CMS=1; //set CMS=1 for CMS data
  int first_ch=1; //first_ch=n skips the first n channels when average noise is calculated

  TH1F* rmsNoiseRSensors   = new TH1F( "RMS noise R sensors",
                                     "RMS noise for R sensors", 50, 0.1, 5.1 );
  TH1F* rmsNoisePhiSensors = new TH1F( "RMS noise #Phi sensors",
                                       "RMS noise for #Phi sensors", 50, 0.1, 5.1 );
  
  //TH1F* RMSNoiseSensors =
  //  new TH1F( "RMSNoiseSensors", "RMS noise vs sensors", 110, -0.5, 109.5 );
  //TH1F* RMSNoiseRSensors =
  //  new TH1F( "RMSNoiseRSensors", "RMS noise vs sensors", 42,-0.5,41.5);
  //TH1F* RMSNoisePhiSensors =
  //  new TH1F( "RMSNoisePhiSensors", "RMS noise vs sensors", 42, 62.5, 103.5 );
  
  //TH2F* RMSNoise_vs_linkSensors =
  //  new TH2F( "RMSNoise_vs_linkSensors", "RMS noise vs links for all sensors",
  //            110, -0.5, 109.5, 64, -0.5, 63.5 );
  //TH2F* HeaderNoise_vs_linkSensors =
  //  new TH2F( "HeaderNoise_vs_linkSensors",
  //            "Header noise vs links for all sensors",
  //            110, -0.5, 109.5, 64, -0.5, 63.5 );
  
  for ( int tell=0; tell<106; ++tell ) {
    if ( sen.initializeSensor( m_file, tell ) ) {
      double sensor_noise = sen.getRMSNoiseSensor( CMS, first_ch );
      if (sensor_noise>0){
	if ( sen.isR() ){
        rmsNoiseRSensors -> Fill( sensor_noise );
	}
	else if ( sen.isPhi() ){
        rmsNoisePhiSensors -> Fill( sensor_noise );
	}
      }
    }
  }

  canvas -> cd(1);
  helperTools::setHistoBlueColour( rmsNoiseRSensors );
  rmsNoiseRSensors -> SetName( "" );
  rmsNoiseRSensors -> Draw();
  canvas -> cd(2);
  helperTools::setHistoBlueColour( rmsNoisePhiSensors );
  rmsNoisePhiSensors -> SetName( "" );
  rmsNoisePhiSensors -> Draw();
}

//=============================================================================
// Calculate the mean values of the RMS noise distributions for R/Phi sensors
//=============================================================================
std::pair<double,double> NoiseSummaryPlot::getNoiseSummary()
{
  // Default value !
  std::pair<double,double> noise = std::pair<double,double>( -999., -999. );
  
  if ( m_file -> IsZombie() ) {
    printf( "Zombie data file ==> Default (dummy) values returned !\n" );
    return noise;
  }
  
  VeloSensor sen;
  
  //set parameters for noise calculations
  bool CMS=1; //set CMS=1 for CMS data
  int first_ch=1; //first_ch=n skips the first n channels when average noise is calculated
  
  TH1F* rmsNoiseRSensors   = new TH1F( "RMS noise R sensors",
				       "RMS noise for R sensors", 50, 0., 5. );
  TH1F* rmsNoisePhiSensors = new TH1F( "RMS noise #Phi sensors",
                                       "RMS noise for #Phi sensors", 50, 0., 5. );
  
  for ( int tell=0; tell<106; ++tell ) {
    if ( sen.initializeSensor( m_file, tell ) ) {
      double sensor_noise = sen.getRMSNoiseSensor( CMS, first_ch );
      if ( sen.isR() ){
        rmsNoiseRSensors -> Fill( sensor_noise );
      }
      else if ( sen.isPhi() ){
        rmsNoisePhiSensors -> Fill( sensor_noise );
      }
    }
  }
  
  if ( rmsNoiseRSensors   -> GetEntries() > 0 ){
    rmsNoiseRSensors->GetXaxis()->SetRangeUser(0.1,5.0);
    noise.first  = rmsNoiseRSensors   -> GetMean();
  }
  if ( rmsNoisePhiSensors   -> GetEntries() > 0 ){
    rmsNoisePhiSensors->GetXaxis()->SetRangeUser(0.1,5.0);
    noise.second = rmsNoisePhiSensors -> GetMean();
  }
  return noise;
}

//=============================================================================
// 
//=============================================================================
void drawNoiseSummaryPlots( std::string fileName, std::string refFileName, TCanvas* canvas=0 )
{  
  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  gStyle -> SetOptStat(2210);
  gROOT -> ForceStyle();
  
  NoiseSummaryPlot*  plot = new NoiseSummaryPlot( fileName, refFileName );
  if ( canvas == 0 )
    canvas = new TCanvas( "VELONoiseSummary", "VELO noise summary", 1 );
  plot   -> draw( canvas );
  plot   -> drawSummaryNoiseDists( canvas );
  canvas -> Modified();
  canvas -> Update();
}

//=============================================================================
// 
//=============================================================================
std::pair<double,double> getNoiseSummary( std::string fileName, std::string refFileName )
{
  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  gStyle -> SetOptStat(2210);
  gROOT -> ForceStyle();
  
  NoiseSummaryPlot*  plot = new NoiseSummaryPlot( fileName, refFileName );

  return plot -> getNoiseSummary();
}

//=============================================================================
// DQS for noisy channels
//=============================================================================
std::vector<double> NoiseSummaryPlot::getBadChannels( std::string fileName )
{

    // Default value !
    std::vector<double> badch;

    //some default values
    badch.push_back(-99.);
    badch.push_back(-99.);
    badch.push_back(-99.);
    badch.push_back(-99.);  

    m_file = helperTools::getOrOpen( fileName );

    if ( m_file -> IsZombie() ) {
	printf( "Zombie data file ==> Default (dummy) values returned !\n" );
	return badch;
    }

    Int_t  dqs_badch_NoisyCounter=0;
    Int_t  dqs_badch_MaxNoisy=0;
    Int_t  dqs_badch_DeadCounter=0;
    Int_t  dqs_badch_MaxDead=0;

    Int_t  nSensorsFound=0;
    Int_t  nNoisyChannels = 0;
    Int_t  nDeadChannels = 0;

    //Double_t dqs_badch_MeanBad=0.0;
    char dqs_badch_hist_name[150];
    //Int_t  dqs_badch_SensorCounter=0;	
    TH1F* dqs_badch_HIST_HANDLE=NULL;
    TH1F* dqs_badch_REFHIST_HANDLE=NULL;

    Double_t chanNoise =0;
    Double_t chanRefNoise =0;
    Double_t noiseDiff =0;

    for(Int_t sensor=0; sensor<106; ++sensor){
	nNoisyChannels = 0;
	nDeadChannels = 0;
	if(dqs_badch_HIST_HANDLE) dqs_badch_HIST_HANDLE=NULL;
	if(dqs_badch_REFHIST_HANDLE) dqs_badch_REFHIST_HANDLE=NULL;	  
	sprintf(dqs_badch_hist_name, "Vetra/NoiseMon/DecodedADC/TELL1_%03d/RMSNoise_vs_ChipChannel", sensor);
	if(m_file->Get(dqs_badch_hist_name) && m_StandReffile->Get(dqs_badch_hist_name)){   
	    nSensorsFound++;
	    dqs_badch_HIST_HANDLE=(TH1F*)m_file->Get(dqs_badch_hist_name); 
	    dqs_badch_REFHIST_HANDLE=(TH1F*) m_StandReffile->Get(dqs_badch_hist_name); 
	    if (	dqs_badch_HIST_HANDLE->Integral() == 0 ){
		std::cout <<"Noise histo empty = > Nothing done!"<<std::endl;

		badch[0] = -99;
		badch[1] = -99;
		badch[2] = -99;
		badch[3] = -99;
		return badch;
	    }
	    for (Int_t ibin = 0 ; ibin < 2048; ibin++){
		chanNoise = dqs_badch_HIST_HANDLE->GetBinContent(ibin);
		chanRefNoise = dqs_badch_REFHIST_HANDLE->GetBinContent(ibin);
		noiseDiff = chanNoise  - chanRefNoise;
		if (noiseDiff < -.5 && chanNoise < 1){
		    dqs_badch_DeadCounter++;
		    nDeadChannels++ ;
		}
		if (noiseDiff > .5 && chanNoise > 2){
		    dqs_badch_NoisyCounter++  ;
		    nNoisyChannels++  ;
		}
	    }

	    if(nNoisyChannels>dqs_badch_MaxNoisy)dqs_badch_MaxNoisy=nNoisyChannels;
	    if(nDeadChannels>dqs_badch_MaxDead)dqs_badch_MaxDead=nDeadChannels;
	}
    }
    if(nSensorsFound>0){
	badch[0] = dqs_badch_DeadCounter;
	badch[1] = dqs_badch_NoisyCounter;
	badch[2] = dqs_badch_MaxDead;
	badch[3] = dqs_badch_MaxNoisy;
    }

	return badch;

}
//=============================================================================
