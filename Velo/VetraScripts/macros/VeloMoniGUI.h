//---------------------------------------------------------------------------//
//                                                                           //
//  VeloMoniGUI macro                                                        //
//  =================                                                        //
//                                                                           //
//  Graphical User Interface (GUI) for the VELO Monitoring                   //
//                                                                           //
//  Example usage:                                                           //
//  - in ROOT, load the GUI with                                             //
//    (Python scripts used in the GUI need also to be loaded!)               //
//    .L VeloMoniGUI.C                                                       //
//    and run with                                                           //
//    new VeloMoniGUI()                                                      //
//     or                                                                    //
//    new VeloMoniGUI( "Vetra.root", true )                                  //
//                                                                           //
//  @author Eduardo Rodrigues                                                //
//  @date   2009-02-27                                                       //
//                                                                           //
//---------------------------------------------------------------------------//

#ifndef VELOMONIGUI_H
#define VELOMONIGUI_H

// STL includes
#include <string>
#include <map>
#include <utility>
#include <time.h>
#include <cstdarg>

// ROOT includes
#include <TQObject.h>
#include <RQ_OBJECT.h>
#include <TApplication.h>
#include <TRootEmbeddedCanvas.h>
#include <TGTab.h>
#include <TFile.h>
#include <TGButton.h>
#include <TGLabel.h>
#include <TGTripleSlider.h>
#include <TGFrame.h>
#include <TGTextEdit.h>
#include <TGTextView.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGMsgBox.h>
#include <TGComboBox.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TPaveLabel.h>
#include <TText.h>

// Includes from Velo/VetraScripts
#include "../src/helperTools.h"

#include "channelMapper.h"
#include "SensorIVCurve.h"

using namespace std;


// Enumeration for the menu bar items
enum Identifiers {
  M_FILE_OPEN,
  M_FILE_OPEN_IV,
  M_FILE_OPEN_REF,
  M_FILE_OPEN_IV_REF,
  M_FILE_SAVE_REF,
  M_FILE_SAVE_IV_REF,
  M_FILE_RUN,
  M_FILE_CLOSE,
  M_FILE_CLOSE_IV,
  M_FILE_CLOSE_REF,
  M_FILE_CLOSE_IV_REF,
  M_FILE_EXIT,
  M_SET_HPATH_NOISEMON,
  M_SET_HPATH_ADCMON,
  M_SET_HPATH_GAINMON,
  M_SET_HPATH_BADCHMON,
  M_SET_HPATH_ERRORMON,
  M_SET_HPATH_PEDSUBMON,
  M_SET_HPATH_VSM,
  M_SET_HPATH_VOM,
  M_SET_HPATH_VCM,
  M_SET_HPATH_VECM,
  M_SET_HPATH_VTM,
  M_SET_HPATH_VIPM,
  M_SET_HPATH_TVM,
  M_SET_HPATH_PRINT,
  M_HELP_TWIKI_MAIN,
  M_HELP_TWIKI_DQ,
  M_HELP_TWIKI_DQM_ALGOS,
  M_HELP_TWIKI_DQM_SCRIPTS,
  M_HELP_TWIKI_PROBLEMS,
  M_HELP_TWIKIDQ_OVERVIEW,
  M_HELP_TWIKIDQ_DQS,
  M_HELP_TWIKIDQ_TRENDS,
  M_HELP_TWIKIDQ_NOISE_VELO,
  M_HELP_TWIKIDQ_NOISE_RPHI,
  M_HELP_TWIKIDQ_NOISE_LINK,
  M_HELP_TWIKIDQ_NOISE_TP,
  M_HELP_TWIKIDQ_NOISE_SENSOR,
  M_HELP_TWIKIDQ_NOISE_VOLTAGE,
  M_HELP_TWIKIDQ_XTALK,
  M_HELP_TWIKIDQ_XTALKDIM,
  M_HELP_TWIKIDQ_PEDSUM,
  M_HELP_TWIKIDQ_PEDSENSOR,
  M_HELP_TWIKIDQ_CLUSTER,
  M_HELP_TWIKIDQ_CLUSTERTWO,
  M_HELP_TWIKIDQ_OCC,
  M_HELP_TWIKIDQ_TRACKSUM,
  M_HELP_TWIKIDQ_TRACKIP,
  M_HELP_TWIKIDQ_ERRORBANK,
  M_HELP_TWIKIDQ_BADCHAN,
  M_HELP_TWIKIDQ_TIMESAMPLE,
  M_HELP_TWIKIDQ_GAIN,
  M_HELP_TWIKIDQ_IVCURVE,
  M_HELP_ABOUT,
  M_HELP_DEBUG
};
// A little class to automatically handle the generation of unique
// widget ids.
class IDList {
private:
   Int_t nID;               // Generates unique widget IDs.
public:
   IDList() : nID(0) {}
   ~IDList() {}
   Int_t GetUnID(void) { return ++nID ; }
};
//=============================================================================
//
//=============================================================================
class VeloMoniGUI {

  RQ_OBJECT( "VeloMoniGUI" )

private:
  TGPopupMenu*         m_menuFile;
  TGPopupMenu*         m_menuRef;
  TGPopupMenu*         m_menuHelp;
  TGMainFrame*         m_mainFrame;
  TGTransientFrame*    m_mainDQS;
  TGTab*               m_tabFrame;
  TFile*               m_currentFile;
  TFile*               m_currentRefFile;
  std::string          m_currentFileName;
  std::string          m_currentRefName;
  TFile*               m_HVOffFile;
  TFile*               m_DQFile;
  std::string          m_HVOffFileName;
  SensorIVCurve*       m_sensorIVCurve;
  std::string          m_currentIVFileName;
  std::string          m_currentIVRefName;
  TGGroupFrame*        m_datasetFrame;
  TGGroupFrame*        m_refdataFrame;
  TGGroupFrame*        m_curTempGroup;
  TGGroupFrame*        m_refTempGroup;
  TGLabel*             m_datasetFrameRootFile;
  TGLabel*             m_datasetFrameIVFile;
  TGLabel*             m_refdataFrameRootFile;
  TGLabel*             m_refdataFrameIVFile;
  TGTextEdit*          m_tabDocTextEdit;
  TGTextView*          m_dqsOverview_text;
  TGComboBox*          m_TrendsDropDownPlots;
  TGComboBox*          m_TrendsDropDownPlots2;
  //TGComboBox*          m_BCLTrendsDropDownPlots;//BCL
  //TGComboBox*          m_BCLTrendsDropDownPlots2;//BCL
  TGComboBox*          m_IVTrendsDropDownPlots;
  TGComboBox*          m_IVTrendsDropDownPlots2;
  TGComboBox*          m_IVTrendsDropDownPlotsVoltage;
  TGNumberEntry*       m_TrendsRunMin;
  TGNumberEntry*       m_TrendsRunMax;
  //TGNumberEntry*       m_BCLTrendsRunMin;//BCL
  //TGNumberEntry*       m_BCLTrendsRunMax;//BCL
  // Used by general drawing functions
  TPaveLabel           m_LoadingPave;
  TText			       m_DisplayedText;
  TStyle*                     m_resetStyle; /// has the default style to reset to


  // Pointers to all embedded canvases
  TRootEmbeddedCanvas* m_eCanvasClusters;
  TRootEmbeddedCanvas* m_eCanvasClusters2;
  TRootEmbeddedCanvas* m_eCanvasTracks;
  TRootEmbeddedCanvas* m_eCanvasPedestals;
  TRootEmbeddedCanvas* m_eCanvasNoise;
  TRootEmbeddedCanvas* m_eCanvasCrossTalk;
  TRootEmbeddedCanvas* m_eCanvasOccupancies;
  TRootEmbeddedCanvas* m_eCanvasGain;
  TRootEmbeddedCanvas* m_eCanvasTrends;
  TRootEmbeddedCanvas* m_eCanvasDetailedTrends;
  TRootEmbeddedCanvas* m_eCanvasIVCurveL;
  TRootEmbeddedCanvas* m_eCanvasIVCurveR;
  TRootEmbeddedCanvas* m_eCanvasTimeSamples;
  TRootEmbeddedCanvas* m_eCanvasTELL1Algs;
  TRootEmbeddedCanvas* m_eCanvasBadChannels;
  TRootEmbeddedCanvas* m_eCanvasErrorBanks;
  TRootEmbeddedCanvas* m_eCanvasIVTrends;
  TRootEmbeddedCanvas* m_eCanvasIVSlopes;
  TRootEmbeddedCanvas* m_eCanvasOverviewTemp;
  //TRootEmbeddedCanvas* m_eCanvasBCLTrends;//BCL

  // UI elements & data for Trends tab
  TGNumberEntry* m_TrendsTimeEntry;
  TGNumberEntry* m_TrendsEarliest;
  bool m_TrendsHistogramsGenerated;
  //bool m_BCLTrendsHistogramsGenerated;//BCL
  Int_t m_TrendsCurrentDisplayedPage;
  Int_t m_TrendsDQSFileUseCount;
  //Int_t m_TrendsBCLFileUseCount;//BCL

  // Text entries for the Data Quality Summary (DQS) boxes
  TGTextEntry* m_dqs_noise_txtentry[2];
  //TGTextEntry* m_dqs_badch_txtentry[6];  //Temporarily comment out bad channels...
  TGTextEntry* m_dqs_crosstalk_txtentry[1];
  TGTextEntry* m_dqs_pedestals_txtentry[1];
  TGTextEntry* m_dqs_errors_txtentry[1];
  TGTextEntry* m_dqs_info_txtentry[3];
  TGTextEntry* m_dqs_lowadc_txtentry[2];
  TGTextEntry* m_dqs_trks_txtentry[7];
  TGTextEntry* m_dqs_vtx_txtentry[6];
  std::string  m_dqs_vtx_txtentrySpare[2];
  TGTextEntry* m_dqs_occ_txtentry[2];
  TGTextEntry* m_dqs_clusters_txtentry[9];
  TGTextEntry* DQSSub_Shiftername;
  TGTextEntry* DQSSub_Username;
  TGTextEntry* DQSSub_Password;
  TGCheckButton* DQSSub_SaveCheck;
  std::string DQSComments;
  TGTextEdit* m_frameComments;

  std::string m_dqsSavedShifterName;
  std::string m_dqsSavedShifterUser;

  // Class to contain information that is read from Vetra log files
  struct ProcessingInfo 
  {
    std::string GPSTime;
    std::string RunNumber;
    std::string NumberOfEvents;
    // All values are N/A by default
    ProcessingInfo();
    void Reset();
  };

  //DQS run number and time string:
  ProcessingInfo m_dqs_ZSProcInfo, m_dqs_NZSProcInfo;
  char m_dqs_VersionNumber[256];
  Int_t m_dqs_Quality;
  Int_t m_dqs_EnableSubmit;

  // Buttons for the "Noise" tab
  TGCheckButton   *m_op_reorder;
  TGCheckButton   *m_op_ADC;
  TGCheckButton   *m_op_CMS;
  TGCheckButton   *m_op_Diff;

  //buttons for "Occupancies" tab
  TGTextButton  txtBtnOccupanciesOverview;
  TGTextButton  txtBtnOccupanciesSpectra;
  TGTextButton  txtBtnOccupanciesLayout;
  TGTextButton  txtBtnOccupanciesVsR;


  TGCheckButton*   writeLists;


  // buttons for Pedestals tab
  TGRadioButton   *m_op_Ped;
  TGRadioButton   *m_op_2DRawADC;
  TGRadioButton   *m_op_1DPedSub;
  TGRadioButton   *m_op_2DPedSub;
  TGRadioButton   *m_op_masked;

  // buttons for tabs
  TGCheckButton   *m_op_Gain_Scaled;
  TGCheckButton   *m_op_DQS;
  TGRadioButton   *m_op_DQS_Good;
  TGRadioButton   *m_op_DQS_Bad;
  TGRadioButton   *m_op_DQS_Prob;
  TGCheckButton   *m_op_MeanHistos;
  TGCheckButton   *m_op_Headers;
  TGCheckButton   *m_op_NoHeaders;
  TGCheckButton*   m_occupancies_useRef;
  TGCheckButton*   m_occupancies_subRef;
  TGRadioButton   *m_op_deads;
  TGRadioButton   *m_op_noisies;
  TGRadioButton   *m_op_allBad;
  TGRadioButton   *m_op_BadChanFromOccup;
  TGRadioButton   *m_op_BadChanFromBCRun;

  TGRadioButton   *m_op_OnTrack;
  TGRadioButton   *m_op_Raw;
  TGRadioButton   *m_op_Corrected;

  // Buttons and data members for the "Clusters" tabs
  TGRadioButton* m_op_Prev3;
  TGRadioButton* m_op_Prev2;
  TGRadioButton* m_op_Prev1;
  TGRadioButton* m_op_Default;
  TGRadioButton* m_op_Next1;
  TGRadioButton* m_op_Next2;
  TGRadioButton* m_op_Next3;
  TGRadioButton* m_op_RefOff;
  TGRadioButton* m_op_RefOn;
  TGRadioButton* m_op_RefDiff;
  TGCheckButton* m_op_MPVFWHMsn;
  TGCheckButton* m_clusters_useRef;
  TGCheckButton* m_clusters_highMult;
  int            m_clusters_currentCanvasButton;
  int            m_clusters_currentMPVFWHMCanvasButton;
  int            m_clusters_currentMPVFWHMSensorButton;

  // Buttons and data members for the "Tracks" tab
  TGCheckButton* m_trks_useRef;
  int            m_trks_currentCanvasButton;

  // Data member for the "IV Curves" tab
  int m_ivcurves_button;
  int m_currentSensor;
  TGCheckButton   *m_op_ivUp;
  TGCheckButton   *m_op_ivDown;
  TGCheckButton   *m_op_ivRef;
  TGRadioButton   *m_op_ivSlot;
  TGRadioButton   *m_op_ivHV;
  TGRadioButton   *m_op_ivSWNum;

  TGTripleHSlider *m_tripleSlider;
  TH1F            *m_curNoiseHist;
  TH2F            *m_curLinkNoiseHist;
  TGLabel         *m_curNTC1_L;
  TGLabel         *m_curNTC1_R;
  TGLabel         *m_curNTC2_L;
  TGLabel         *m_curNTC2_R;
  TGLabel         *m_refNTC1_L;
  TGLabel         *m_refNTC1_R;
  TGLabel         *m_refNTC2_L;
  TGLabel         *m_refNTC2_R;
  channelMapper   *m_chanMap;

  // Check boxex for IVTrending temps
  TGCheckButton* m_op_NTCs;
  TGCheckButton* m_op_AVG;
  TGRadioButton   *m_op_Currents;
  TGRadioButton   *m_op_Slopes;
  TGRadioButton   *m_op_Time;
  TGRadioButton   *m_op_Lumi;

  UInt_t m_w, m_h;
  bool   m_debug;
  helperTools::MoniHistosPaths* m_moniHistosPaths;


public:
  // Constructor
  VeloMoniGUI( const char* fileName=NULL,
               const char* refFileName=NULL,
               bool debug=false, UInt_t w=1211, UInt_t h=600 );
  // Destructor
  virtual ~VeloMoniGUI();
  // Pop out the "open file" dialog window
  Bool_t  ButtonOpenFile();
  Bool_t  ButtonOpenRefFile();
  Bool_t  ButtonOpenIVFile( string opt );
  Bool_t  ButtonArchiveIV();
  Bool_t  tabExists;
  // Handle the close button on the window
  void HandleCloseWindow();
  // Handle the functionality associated to the menu bar items
  void    HandleMenu( Int_t id );
  // Handle the initialisation of the tabs
  void    HandleTabsInitialisation( Int_t id );
  // Display documentation on the monitoring available in a tab
  void    DisplayTabDocumentation( Int_t id );
  // Describe the contents of the tabs
  void    ContentsTabOverview(    TGCompositeFrame* tf, const char* name );
  void    ContentsTabDQS(         TGCompositeFrame* tf, const char* name );
  void    ContentsTabClusters(    TGCompositeFrame* tf, const char* name );
  void    ContentsTabClusters2(   TGCompositeFrame* tf, const char* name );
  void    ContentsTabTracks(      TGCompositeFrame* tf, const char* name );
  void    ContentsTabPedestals(   TGCompositeFrame* tf, const char* name );
  void    ContentsTabNoise(       TGCompositeFrame* tf, const char* name );
  void    ContentsTabCrossTalk(   TGCompositeFrame* tf, const char* name );
  void    ContentsTabOccupancies( TGCompositeFrame* tf, const char* name );
  void    ContentsTabGain(        TGCompositeFrame* tf, const char* name );
  void    ContentsTabDetailedTrends(TGCompositeFrame* tf, const char* name );
  void    ContentsTabTrends(      TGCompositeFrame* tf, const char* name );
  void    ContentsTabIVCurves(    TGCompositeFrame* tf, const char* name );
  void    ContentsTabTimeSamples( TGCompositeFrame* tf, const char* name );
  //void    ContentsTabTELL1Algs(   TGCompositeFrame* tf, const char* name );
  void    ContentsTabBadChannels( TGCompositeFrame* tf, const char* name );
  void    ContentsTabErrorBanks(  TGCompositeFrame* tf, const char* name );
  void    ContentsTabIVTrends(    TGCompositeFrame* tf, const char* name );
  //void    ContentsTabIVSlopes(    TGCompositeFrame* tf, const char* name );
  //void    ContentsTabBCLTrends(    TGCompositeFrame* tf, const char* name );//BCL

  // Helper functions for handling of Vetra log files
  ProcessingInfo          GetInfoFromLogFile(const std::string& logfile) const;
  std::pair<std::string, std::string> 
                          ReadMergedLog(const std::string& logfile) const;
  // Handle the tab buttons in "DQS"
  void HandleTabDQS_txtBtnDQS();
  void HandleTabDQS_txtBtnElog();
  void DQSSubmitClose();
  void DQSDoSubmit();
  void DQSTryToClose();
  void DQSWrite();
  void DQSElogError();
  
  // Utility functions for drawing
  void DisplayTextInCanvas(TCanvas* c, const std::string& text );
  void ShowLoading(TCanvas* c, const std::string& text);

// Handle the tab buttons in "Clusters"
  void HandleTabClusters_txtBtnClustersStrips();
  void HandleTabClusters_txtBtnADCDists();
  void HandleTabClusters_txtBtnMPVFWHM(int Plots);
  void HandleTabClusters_txtBtnSensorLandau(int Sensor);
  void UpdateTabClustersCanvas();
  const char* GetTimeSampleDirectory();
  void UpdateTabClustersMPVFWHMCanvas();
  // Handle the tab buttons in "Tracks"
  void HandleTabTracks_txtBtnEfficiency();
  void HandleTabTracks_txtBtnClustersOnTrack();
  void HandleTabTracks_txtBtnAngles();
  void HandleTabTracks_txtBtnVertices();
  void HandleTabTracks_txtBtnPVhalfdistance();
  void HandleTabTracks_txtBtnLowADC();
  void HandleTabTracks_txtBtnResidual();
  void HandleTabTracks_txtBtnIPResolutions();
  void HandleTabTracks_txtBtnIPMeanResiduals();
  void UpdateTabTracksCanvas();
// Handle the tab buttons in "Pedestals"
  void HandleTabPedestal_txtBtnPedestalSummary();
  void HandleTabPedestal_txtBtnLinkPedestalMean();
  void HandleTabPedestal_txtBtnLinkPedestalRMS();
  void HandleTabPedestal_txtBtnChannelPedestal( int sensor );
  void UpdateTabPedestalsCanvas();
  // Handle the tab buttons in "Noise"
  void HandleTabNoise_txtBtnVeloGeom();
  void HandleTabNoise_txtBtnNoiseSummary();
  void HandleTabNoise_txtBtnLinkNoise();
  void HandleTabNoise_txtBtnTPNoise();
  //void HandleTabNoise_txtBtnHeaderNoise();
  void HandleTabNoise_txtBtnChannelNoise( int sensor );
  void UpdateNoiseOptions();
  // Handle the tab buttons in "Cross Talk"
  void HandleTabCrossTalk_txtBtnHeaderCrossTalk();
  void HandleTabCrossTalk_txtBtnBeetleCrossTalk2D();
  void HandleTabCrossTalk_txtBtnBeetleCrossTalk1D();
  // Handle the tab buttons in "Occupancies"
  void HandleTabOccupancies_txtBtnOccupanciesOverview();
  void HandleTabOccupancies_txtBtnOccupanciesSpectra();
  void HandleTabOccupancies_txtBtnOccupanciesLayout();
  void HandleTabOccupancies_txtBtnOccupanciesVsR();
  void HandleTabOccupancies_txtBtnSensorOccupancy( int sensor, int mode = 0 );
  void HandleTabOccupancies_txtBtnOccupanciesVsBCID();
  void UpdateTabOccupanciesCanvas();
  void UpdateTabBadChannelsCanvas();
  // Handle the tab buttons in "IV Curves"
  void HandleTabIVCurves_txtBtnChannelCurve( int sensor );
  void HandleTabIVCurves_txtBtnNext();
  void HandleTabIVCurves_txtBtnPrev();
  void UpdateTabIVCurvesCanvas();
  // Handle the tab buttons in "multi"
  void HandleMultiTab_BtnNext();
  void HandleMultiTab_BtnPrev();
  // Handle the tab buttons in "Overview"
  void HandleTabOverview_txtBtnGenerate(bool recent);
  // Handle buttons in tab "Trends"
  bool TrendsGenerateHistograms();
  void TrendsShowPage( Int_t PageNumber );
  void TrendsErrorClearCanvas();
  void HandleTabTrends_BtnPage ( int pageNumber );
  void HandleTabTrends_txtBtnRegenerate ();
  void HandleTabTrends_txtBtnUpdate ();
  // Handle the tab buttons in "Detailed Trends"
  void DetailedTrendsDrawSelectedPlot();
  void HandleTabDetailedTrends_txtBtnGenerate();
  void HandleTabDetailedTrends_dropDown();
  // Handle the tab buttons in "BCL Trends"
  //void HandleTabBCLTrends_txtBtnRegenerate ();//BCL
  //bool BCLTrendsGenerateHistograms();//BCL
  //void BCLTrendsDrawSelectedPlot();//BCL
  //void HandleTabBCLTrends_txtBtnGenerate();//BCL
  //void HandleTabBCLTrends_dropDown();//BCL
  // Buttons for the IV Trends tab
  void HandleTabIVTrends_txtBtnGenerate();
  void HandleTabIVTrends_txtBtnAverage();
  void HandleTabIVTrends_dropDown();
  void HandleTabIVTrends_txtBtnAll();
  void UpdateIVTrendsCanvas();
  // Handle the tab buttons in "Gain"
  void HandleTabGain_txtBtnFHS();
  void HandleTabGain_txtBtnAvg();
  void HandleTabGain_txtBtnTestPulse();
  // void HandleTabGain_txtBtnHighADC();
  void UpdateGainOptions();
  // Handle the tab named "TimeSamples"
  void HandleTabTimeSamples_txtBtnTimeSamplesPage1();
  void HandleTabTimeSamples_txtBtnTimeSamplesPage2();
  void HandleTabTimeSamples_txtBtnTimeSamplesPage3();
  void HandleTabTimeSamples_txtBtnTimeSamplesPage4();
  void HandleTabTimeSamples_txtBtnTimeSamplesPage5();
  // Handle the tab button is "TELL1 Algs"
  //void HandleTabTELL1Algs_txtBtnZSClusterADCs();
  //void HandleTabTELL1Algs_txtBtnPedestalsPage1();
  //void HandleTabTELL1Algs_txtBtnPedestalsPage2();
  //void HandleTabTELL1Algs_txtBtnBeetleCrossTalk();
  // Handle the tab button in "Bad Channels"
  void HandleTabBadChannels_txtBtnBadChannels();
  void HandleTabBadChannels_txtBtnSensorBadChannels( int sensor );

  // Handle the tab button in "ErrorBanks"
  void HandleTabErrorBanks_txtBtnOverview();
  void HandleTabErrorBanks_txtBtnDetails();
  void HandleTabErrorBanks_txtBtnSEUDetails();
  void HandleTabErrorBanks_txtBtnProcStatus();
  void HandleTabErrorBanks_txtBtnPCNDetails();
  void HandleTabErrorBanks_txtBtnPCNGoryDetails(int tell1id);
  void HandleTabErrorBanks_togglePCNGoryDetails();

  // Reset temporary quantities in memory
  void ResetBuffers();

  ClassDef(VeloMoniGUI, 0);

private:
  // Open a ROOT file
  Bool_t OpenFile( const char* fileName );
  // Open a ROOT reference file
  Bool_t OpenRefFile( const char* fileName );
  //Open the standard HV file
  Bool_t OpenHVOffFile(const char* fileName );
  // Close the ROOT file currently open
  Bool_t CloseFile();
  // Close the ROOT reference file currently open
  Bool_t CloseRefFile();
  Bool_t CloseIVFile();
  // Close the I/V reference file currently open
  Bool_t saveRootReference();
  Bool_t saveIVReference();

  // Clear all the info displayed in the various tabs (canvases, text, etc.)
  void ClearTabsDisplayedInfo();

  // Open a web browser
  void OpenBrowser( const char* url );

  // View or change the path to the histograms produced buy a given algorithm
  void ViewOrChangeMoniHistosPath( std::string algoName );
  //void VeloMoniGUI::ViewOrChangeMoniHistosPath( std::string algoName );
  Bool_t CloseIVRefFile();

  // Create a temporary directory and return the path. The function always
  // returns the same path for a given VeloMoniGUI instance
  // Directory is deleted when the VeloMoniGUI instance is deleted
  const std::string& TempDir();
  const std::string& FormatTempDir(const std::string& strTemplate);
  std::string m_TempDir, m_FormatTempDir;
  bool m_DeleteTempDir;
  void DeleteTempDir();

  // FIXME: hack
  std::vector<TGTextButton*> _EB_PCN_buttons;
  TGCheckButton *_EB_toggle_PCN;
};

//=============================================================================

// ================================================
// Documentation for the different tabs and buttons
// ================================================

// Doc for the "DQS" tab
// ---------------------
const string doc_TabDQS_title =
"Data Quality Summary: \n";

const string doc_TabDQS_def =
"(For more info see Help->Information on plots...->DQS Tab, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DqsTab)\n---------------------\nDQS information for RMS noise (NZS), occupancies (ZS), clusters (ZS), tracks (ZS), vertices (ZS), etc.\nPush \"Get the DQS\" first to fill this page, then \"ELOG submission\" to send the values to the Elog.\n";

// Doc for the "Clusters" tab
// --------------------------
const string doc_TabClusters_title =
"Contents of \"Clusters\" monitoring: Filled for ZS data \n(For more info see Help->Information on plots...->All Clusters Tab Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawClustersMoni) \n----------------------------------\n";

const string doc_TabClusters_def =
"The clusters-based monitoring displays basic distributions of clusters such as the number of (VELO) clusters per event, \n the number of strips per cluster and the Landau (ADC value) distributions.\n Certain 2D histograms allow a comparison versus the sensor number.";

const string doc_TabClusters_opts =
"\nThe options allow to select the time sample to analyse in 25ns slots: from \"Prev3\" to \"Next3\".\nReference plots can be used/not used according to the choice on the top-right corner.";

const string doc_TabClusters_txtBtnClustersStrips =
"- distribution of clusters per event and of number of strips per cluster\n- 2D histogram of number of strips per cluster versus sensor number\n- 2D histogram of active links versus sensor number\nSelect \"Extended plot\" to extend the axis of the first plot and show it on a log-scale.";

const string doc_TabClusters_txtBtnClustersComp =
"- 2D histogram of number of strips per cluster versus sensor number\n- 2D histogram of active links versus sensor number \n First row is actual data, second reference, and third is the ratio of the two. ";

const string doc_TabClusters_txtBtnADCDists =
"- cluster ADC (Landau) distributions for all/R/Phi sensors\n- cluster ADC (Landau) distributions for each sensor (2D histogram)";

// Doc for the "Clusters2" tab
// ---------------------------
const string doc_TabClusters2_title =
"Contents of \"Clusters2\" monitoring: Filled for ZS data \n(For more info see Help->Information on plots...->All Clusters 2 Tab Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawClustersMPVFWHMMoni) \n-----------------------------------\n";

const string doc_TabClusters2_def =
"Plots of MPV and FWHM for each sensor. MPV is the highest bin, FWHM is the interpulation between two bins at half maximum.\nOptions:\nOnTrack: Plots for clusters on tracks\n   Corrected: Plots for clusters on track with an angular correction (normalised to 300um of Silicon)'\n   Raw: using all raw clusters\n   S/N is defined as the MPV/Average Noise on the sensor\n";

const string doc_TabClusters2_opts =
"Options:\n   OnTrack: Plots for clusters on tracks\n   Corrected: Plots for clusters on track with an angular correction (normalised to 300um of Silicon)\n   Raw: using all raw clusters\n   S/N is defined as the MPV/Average Noise on the sensor\n";

const string doc_TabClusters_txtBtnMPVFWHM=
"MPV & FWHM for all sensors\nMPV is the highest bin, FWHM is the interpulation between two bins at half max for the quick method.\nS/N is defined as the MPV/Average Noise on the sensor\n";

const string doc_TabClusters_txtBtnSummary=
"\nThe \"MPV & FWHM Summary\" plot shows a projection of the ADC values in a histogram. The vertical axis has the number of entries\n(i.e., the number of sensors with a given ADC value) and the horizontal axis has the ADC values for FWHM and MPV, or the difference\nfrom the reference for the \"diff\" plot. The ADC counts are computed using the quick method only for the summary.\n";

const string doc_TabClusters_txtBtnLandau=
"Plot of the adcs for a sensor\n with the following ";


// Doc for the "Tracks" tab
// ------------------------
const string doc_TabTracks_title =
"Contents of \"Tracks\" monitoring: Filled for ZS data\n";

const string doc_TabTracks_def =
"Tracks-based monitoring comprises histograms with basic tracks distributions, a (biased) residual distributions,\nmonitoring of modules and sensors (pseudo-) efficiencies, and ADC Landau curves for clusters on tracks";

const string doc_TabTracks_txtBtnEfficiency =
"(For more info see Help->Information on plots...->All Track Summary Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawVeloTrackSummaryPlots) \n--------------------------------\nOn the top left, Module Mismatch is the number of time a sensor has no r (phi) cluster associated to a track \neven if the other sensor in the same module has a phi (r) cluster associated to it. The plot represents the number \nof mismatch divided by the total number of hits associated to a track.\nOn the top right, Number of Times a sensors has a hit associated to a track versus Module number.\nOn the bottom left, Pseudoefficiency per track by interpolation is evaluated by the ratio between the  number of \nreconstructed hits and the number of expected hits. The number of expected hits is evaluated between the first and last reconstructed points.\nOn the bottom right the pseudoefficiency by interpolation versus Module number. \nModule number = sensor number (R sensors)/ sensor number - 64 (Phi sensors).";

const string doc_TabTracks_txtBtnClustersOnTrack =
"(For more info see Help->Information on plots...->All Track Summary Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawVeloTrackSummaryPlots) \n--------------------------------\nOn the top left, Number of r and phi clusters associated to a track.\nOn the top right, profile histogram of the number of r and phi clusters associated to a track versus the polar angle.\nOn the bottom, the adc sum of the r (phi) clusters associated to a track on the left (on the right). ";

const string doc_TabTracks_txtBtnAngles =
"(For more info see Help->Information on plots...->All Track Summary Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawVeloTrackSummaryPlots) \n--------------------------------\nOn the top left, the phi coordinate of the 3D points on the track. The angle is in the half frame for the A side. \nThe angle for the C side is transformed to have the angle within (-90,90). For both halves the top part is betwen 0 \nand 90 and the bottom part between -90 and 0.\nOn the top right the azimuthal angle of the track.\nOn the bottom left the pseudo-rapidity of the track: eta=-ln(tan(theta/2)).\nOn the bottom right the polar angle of the track.";

const string doc_TabTracks_txtBtnResidual =
"(For more info see Help->Information on plots...->All Track Summary Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawVeloTrackSummaryPlots) \n--------------------------------\nMean of the pull of biased residual distribution (pull=biased residual/(pitch/sqrt(12)) versus sensor number.";

const string doc_TabTracks_txtBtnVertices =
"(For more info see Help->Information on plots...->All Track Summary Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawVeloTrackSummaryPlots) \n--------------------------------\n- 3D-position of primary vertices (PV)\n- difference in PV x-position obtained independently from each half";

const string doc_TabTracks_txtBtnPVhalfdistance =
"(For more info see Help->Information on plots...->All Track Summary Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawVeloTrackSummaryPlots) \n--------------------------------\nDistance between each half and the beam. This distance is the one between each \nhalf and the PV obtained independently from tracks reconstructed in that half.\nLeft column for the Left side, Right column for the right side.\nFirst row distance along x and second row distance along y.";

const string doc_TabTracks_txtBtnLowADC =
"(For more info see Help->Information on plots...->All Track Summary Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawVeloTrackSummaryPlots) \n--------------------------------\nRate of the low ADC cluter in the inner strip, each cluster is associated \nwith a physical track by the M2 routing line";


const string doc_TabTracks_txtBtnIPResolutions =
"(For more info see Help->Information on plots...->IP Resolution Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawIPResolutionsHistos) \n--------------------------------\nIP monitoring is not currently run by default using vetraOffline. To run IP monitoring do: vetraOffline -u $VETRASCRIPTSROOT/python/addIPMoni.py\nThese plots show the resolutions of the x and y components of IP measurements, calculated from the sigma of a single Gaussian fit to the residual distributions.\n\nThe top left plot shows the resolution of the x component as a function of 1/pT, and the top right likewise for the y component.\nThese should be roughly linear distributions that are consistent for x and y.\nNormal values for the linear fit are ~12 + 24/pT um.\n\nThe bottom left shows the IPx resolution as a function of azimuthal angle phi. This should be fairly flat but with two peaks about phi=+/- pi/2, where the overlap region increases the material budget.\nThe bottom right plot shows the IPx resolution as a function of pseudorapidity, eta. This should also increase roughly linearly with eta.";

const string doc_TabTracks_txtBtnIPMeanResiduals =
"(For more info see Help->Information on plots...->IP Resolution Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawIPResolutionsHistos) \n--------------------------------\nIP monitoring is not currently run by default using vetraOffline. To run IP monitoring do: vetraOffline -u $VETRASCRIPTSROOT/python/addIPMoni.py\nThese plots show the mean of the residuals of the x and y, from single Gaussian fits to the residual distributions, in bins of phi and eta.\nThese should be quite flat and consistent with zero. Small biases of <10 um are normal.\nA significantly larger bias would indicate a significant misalignment of the VELO.";

// Doc for the "Raw ADC" tab
// -----------------------
const string doc_TabRawADC_title =
"Contents of \"Raw ADC\" monitoring: Filled for NZS data\n";

const string doc_TabRawADC_def = " ";

const string doc_TabRawADC_opts =
"The sensor options are only relevant for the sensor buttons (\"R000\" to \"P105\").";

const string doc_TabRawADC_txtBtnChannelRawADC = " ";


// Doc for the "Pedestal" tab
// -----------------------
const string doc_TabPedestal_title =
"Contents of \"Pedestal\" monitoring: Filled for NZS data\n";

const string doc_TabPedestal_def =
"\n----------------------------------\nPedestal monitoring contains summary plots showing:\nthe average subtracted pedestal values; and the mean and rm of pedestal values per link and sensor.\nIn addition plots are provided per sensor of the value of the pedestal, the average pedestal subtracted value, and the value for each event.";

const string doc_TabPedestal_opts =
"The sensor options are only relevant for the sensor buttons (\"R000\" to \"P105\").";

const string doc_TabPedestal_txtBtnPedestalSummary=
"(For more info see Help->Information on plots...->Pedestal Summary Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawPedestalSummaryPlots) \n----------------------------------\nAverage pedestal subtracted ADC values in all channels.\nThere should be a very small percentage of channels with pedestals outside the range indicated by the red lines.";

const string doc_TabPedestal_txtBtnLinkPedestalMean =
"(For more info see Help->Information on plots...->Pedestal Summary Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawPedestalSummaryPlots) \n----------------------------------\nAverage value of the pedestal subtracted ADC in each A-link. Values should be close to 0.";

const string doc_TabPedestal_txtBtnLinkPedestalRMS =
"(For more info see Help->Information on plots...->Pedestal Summary Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawPedestalSummaryPlots) \n----------------------------------\nRMS value of the pedestal subtracted ADC in each A-link. Values should be less than 0.5.";

const string doc_TabPedestal_txtBtnChannelPedestal =
"(For more info see Help->Information on plots...->Individual Channel Pedestals, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawPedestalChannel) \n----------------------------------\nPedestal - Pedestal values that are subtracted (from VeloCond database).\n 1D - Average pedestal subtracted values in each channel.\n 2D - pedestal subtracted values in each channel.";

// Doc for the "Noise" tab
// -----------------------
const string doc_TabNoise_title =
"Contents of \"Noise\" monitoring: Filled for NZS data\n";

const string doc_TabNoise_def =
"\n----------------------------------\nNoise monitoring comprises a summary of VELO noise per station, RMS of noise for all links per sensor,\nand RMS noise per sensor channel";

const string doc_TabNoise_opts =
"The sensor options are only relevant for the sensor buttons (\"R000\" to \"P105\").";

const string doc_TabNoise_txtBtnVeloGeom =
"(For more info see Help->Information on plots...->VELO Layout, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawNoiseAverageOverview) \n-------------------------------\nSummary noise (CMS) in all VELO stations, separated for the A- and C-side, and for R and Phi sensors";

const string doc_TabNoise_txtBtnNoiseSummary =
"(For more info see Help->Information on plots...->R/Phi Summary, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawNoiseSummaryPlots) \n-------------------------------\nSummary RMS noise (CMS) distributions separated for R and Phi sensors";

const string doc_TabNoise_txtBtnLinkNoise =
"(For more info see Help->Information on plots...->Link Noise, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#LinkNoisePlot) \n-------------------------------\nAverage RMS noise of an analogue link plotted vs link number and sensor number";

const string doc_TabNoise_txtBtnTPNoise =
"(For more info see Help->Information on plots...->TP Noise, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#TPLinkNoisePlot) \n-------------------------------\nDisplays Test Pulse noise for channel 4, for all links on all sensors.";

const string doc_TabNoise_txtBtnHeaderNoise =
"DOCUMENTATION for txtBtnHeaderNoise ...";

const string doc_TabNoise_txtBtnChannelNoise =
"(For more info see Help->Information on plots...->Individual Sensor Noise, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#NoiseDiff) \n-------------------------------\nRMS noise of each channel, plotted against chip channel number\nthe sensor options in the box at the top allow the plotting of\nADC - the raw noise\nCMS - common mode subtracted noise\nDiff - difference from a reference file (requires loading reference file)";

// Doc for the "Cross Talk" tab
// ----------------------------
const string doc_TabCrossTalk_title =
"Contents of \"Cross Talk\" monitoring: Filled for NZS data\n";

const string doc_TabCrossTalk_def =
"(For more info see Help->Information on plots...->Cross Talk Tab, provides links to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawHeaderCrossTalkPlot and https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawBHXTSummaryPlot) \n------------------------------------\nBeetle Header Cross-Talk (BHCT) affects the first channel of each analogue\nlink. The BHCT can be observed by measuring raw noise using the non-zero\nsuppressed data as spikes that appear on every 32th channel.\n \nBeetle header cross-talk 2D \n----------------------------\nThis plot shows the difference between the RMS noise measured in the first channel and the RMS noise for the reference channel in that link is calculated.\nThese values are plotted as a function of sensor number and analogue link number";

// Doc for the "Occupancies" tab
// -----------------------------
const string doc_TabOccupancies_title =
"Contents of \"Occupancies\" monitoring: Filled for ZS data \n(For more info see Help->Information on plots...->All Occupancies Tab Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#OccupanciesMoni) \n-------------------------------------\n";

const string doc_TabOccupancies_def =
"This tab generates a range of plots detailing the occupancy of the VELO for the input dataset.\nThe macros run behind the scenes require the VeloOccupancyMonitor algorithm to have run when the ROOT file was produced.";

const string doc_TabOccupancies_txtBtnOccupanciesSensor =
"The individulal sensor buttons produce plots of % cluster occupancy as a function of channel number.\n     The tick boxes allow header channels to be plotted (red), all other channels (blue)\n	 or all channels together (black).";

const string doc_TabOccupancies_txtBtnOccupanciesOverview =
"The \"Overview\" button produces a histogram showing the % cluster occupancy of each VELO sensor.\nThe three tick boxes enable the user to view the sensor occupancy due to header channels only (red dots),\nall other channels (blue dots) or a mean histogram of al channels (black line).\nThe vertical red lines denote the ranges in which sensors are present, and the blue denotes the start of the pileup sensors.";

const string doc_TabOccupancies_txtBtnOccupanciesSpectra =
"The \"Spectra\" button produces histograms showing the % cluster occupancy spectrum for all the VELO strips.\nThe first histogram shows the entire spectrum, the second histogram focuses on the 0% - 20% occupancy region.\nThe tick boxes can be used to display header channels (red dots), all other channels (blue dots) or a combined hostogram of all channels (black line).";

const string doc_TabOccupancies_txtBtnOccupanciesLayout =
"The \"Layout\" button produces 2D profile histograms which show the occupancies of the VELO in the RZ projection.\nThe first histogram shows focuses on strip occupancies in the range 0% - 6%, the second histogram focuses on strip occupancies in the range 0% - 2%.";

const string doc_TabOccupancies_txtBtnOccupanciesVsR =
"This button displays the cluster occupancy versus R(mm) for the R type sensors.";

const string doc_TabOccupancies_txtBtnOccupanciesVsBCID =
"Shows the average cluster occupancy on the A-side and on the C-side versus the bunch ID.\nThe bunch ID identifies the LHC proton bunches, hence this plot allows \nto look if there is background coming from any particular bunches.";

// Doc for the "Overview" tab
// ----------------------
const string doc_TabOverview_title =
"Contents of \"Overview\": \n(For more info see Help->Information on plots...->Overview Tab, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#OverviewTab) \n------------------------------\n";

const string doc_TabOverview_def ="This displays the runs found in the dqm directory and compares them to the DQ Summary directory.\n";

// Doc for the "Trends" tab
// ----------------------
const string doc_TabTrends_title =
"Contents of \"Trends\": Selection of important histograms for trending \n(For more info see Help->Information on plots...->Trends and Detailed Trends Tabs, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#TrendsTab) \n------------------------------\n";

const string doc_TabTrends_def =
"Load a page using the buttons at the top. It may take some time to load the first page.";

const string doc_TabTrends_common =
"The histograms show different DQ-related quantities on the y-axis, and run "
"number on the x-axis. It is possible to zoom just like in ROOT, by selecting a\n"
"region along one of the axes.\n";

const string doc_TabTrends_page[] = {
  // Page 1
  "Page 1: NZS: Trends for average noise, noisy header links, bad pedestals and error banks.",
  "Page 2: Clusters: The average number of strips per cluster. Landau MPV for R and Phi. Average strip occupancy. Number of "
            "high occupancy strips.",
  "Page 3: Tracks: Clusters per track, average module mismatch percentage, pseudo-efficiency.",
  "Page 4: Vertex: Average primary vertex position in 3 dimensions. Average delta-x between left and right halves."
  };


const string doc_TabTrends_error =
"An error occurred. Check standard output for details.";

// Doc for the "Detailed Trends" tab
// ----------------------
const string doc_TabDetailedTrends_title =
"Contents of \"Detailed Trends\": Users should push the \"Generate\" button first to make the histograms. \n(For more info see Help->Information on plots...->Trends and Detailed Trends Tabs, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DetailedTrendsTab) \n------------------------------\n";

const string doc_TabDetailedTrends_def =
"After the histograms have been made, users can use the drop-down menu to plot the time variation.\nBy default the X axis is set to \"Run number\", but users can choose to correlate any quantity with any \nother by the second drop-down menu.";

const string doc_TabDetailedTrends_txtBtnGenerate =
"The Generate Plots button will run a script which produces a ROOT file containing the time-variation of DQS quantities. \n";

const string doc_TabDetailedTrends_DropDown =
"After generating plots, they can be displayed via the drop-down menus. \nBy default the X axis is set to \"Run number\".";

// Doc for the "BCL Trends" tab//BCL
// ----------------------
//const string doc_TabBCLTrends_title =
//"Contents of \"BCL Trends\": Users should push the \"Generate\" button first to make the histograms. \n------------------------------\n";
//
//const string doc_TabBCLTrends_def =
//"After the histograms have been made, users can use the drop-down menu to plot the time variation.\nBy default the X axis is set to \"Run number\", but users can choose to correlate any quantity with any \nother by the second drop-down menu.";
//
//const string doc_TabBCLTrends_txtBtnGenerate =
//"The Generate Plots button will run a script which produces a ROOT file containing the time-variation of BCL quantities. \n";
//
//const string doc_TabBCLTrends_DropDown =
//"After generating plots, they can be displayed via the drop-down menus. \nBy default the X axis is set to \"Run number\".";

// Doc for the "Gain" tab
// ----------------------
const string doc_TabGain_title =
"Contents of \"Gain\" monitoring: Filled for NZS data with GAIN option.\n(For more info see Help->Information on plots...->All Gain Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawFHSPlots) \n------------------------------\n";

const string doc_TabGain_def =
"Plots of full header swing (FHS), test pulse height and mean header height. Toggle whether to\nrescale by link noise with the button at bottom. \n";

const string doc_TabGain_txtBtnFHS =
"FHS Button: \nThere is a button at the bottom of the page to toggle noise-scaled plots or\nnot. The more important setting for shifters is to turn this off.\nWithout noise-scaling, the 2D plot on the left shows the \"full header swing\" or \nFHS (the distance between the high and low header values). If the gain is correctly calibrated\nthis plot should be mostly the same colour, with a value of around 90\nADC counts. \"Dead\" or mis-calibrated links will show up as gaps in this plot.  \n\nThe 2D plot is summarized by two histograms on the right where the R (top) and\nPhi (bottom) sensors are separated. If the gain is calibrated these should\nboth have means ~90 with RMS <1.  \n\nBy selecting to noise-scale the plots, they show the signal:noise for Beetle\nheaders. Again the 2D plot should be mostly the same colour, with dead or\nespecially noisy links appearing in a different colour. \n";

const string doc_TabGain_txtBtnAvg =
"Avg Button:\nThere is a button at the bottom of the page to toggle noise-scaled plots or\nnot. The more important setting for shifters is to turn this off.\nWithout noise-scaling, these plots show the average Beetle header value (i.e. \nthe mid-way point between a high and a low header. These should be distributed\nwith a mean of around 512 ADC counts. \"Dead\" or mis-calibrated links will show \nup as gaps in this plot.\nThe 2D plot is summarized by two histograms on the right where the R (top) and\nPhi (bottom) sensors are separated. If the gain is calibrated these should\nboth have means ~512 with RMS <7.  \nBy selecting to noise-scale the plots, they show the signal:noise for Beetle\nheader mid-points. Again the 2D plot should be mostly the same colour, with dead or\nespecially noisy links appearing in a different colour. \n";

const string doc_TabGain_txtBtnTestPulse =
"Test Pulse button:\nThere is a button at the bottom of the page to toggle noise-scaled plots or not.\nThe more important setting for shifters is to turn this off. These plots\nwill only display if some test pulses have been injected into the VELO.\nWithout noise-scaling, these plots show the heights of the test pulses. If\nthe gain is calibrated well, the test pulse heights should be uniform throughout\nthe VELO, and should have a mean value close to 30 ADC counts. \"Dead\" or mis-calibrated links will show up as gaps in this plot.\nThe 2D plot is summarized by two histograms on the right where the R (top) and\nPhi (bottom) sensors are separated. If the gain is calibrated these should both have means ~33 with RMS < 2.\nBy selecting to noise-scale the plots, they show the test pulse signal:noise ratio.\nAgain the 2D plot should be mostly the same colour, with dead or especially noisy links appearing in a different colour.";

// Doc for the "IV Curves" tab
// ---------------------------
const string doc_TabIVCurves_title =
"Compare the current I/V curve with a reference on the two sensors of one module showed in one frame \n(For more info see Help->Information on plots...->All IV Curve Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawSensorIVCurve) \n--------------------------------------------------------------------------------------------\n";

const string doc_TabIVCurves_def =
"o Open current I/V and reference I/V curves from \"Data\" and \"Reference\" menus\no Select module to display by pressing the buttons with desired slot numbers.\no Navigate through the modules using the \"Prev module\" and \"Next module\" buttons\no Select the graphs to show from the tick boxes\no Select the labels on the plots from the radio buttons (module slot name, sensor software number or HV channel number)\no The temperatures shown are the average over all voltage points for each one of the four NTCs on that module\n   (the +/- value is the RMS of all readings during the I/V)";

// Doc for the "Time Samples" tab
// ------------------------------
const string doc_TabTimeSamples_title =
"Contents of \"Time Samples\" monitoring: filled for ZS data, requires analysis with TAE or EXCM option.\n(For more info see Help->Information on plots...->All Time Samples Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawTAE) \n--------------------------------------\n";

const string doc_TabTimeSamples_def =
"To be filled ...\n";

// Doc for the "TELL1 Algs" tab
// ----------------------------
const string doc_TabTELL1Algs_title =
"Contents of \"TELL1 Algs\" monitoring:\n------------------------------------\n";

const string doc_TabTELL1Algs_def =
"To be filled ...\n";

// Doc for the "Bad Channels" tab
// ------------------------------
const string doc_TabBadChannels_title =
"Contents of \"Bad Channels\" monitoring:\nTwo methods: occupancy based (works for ZS&NZS data), or dedicated bad channel analysis (works only for NZS, analysis requires BadChannels option)\n(For more info see Help->Information on plots...->Bad Channels Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawNoisyChannelMaps) \n--------------------------------------\n";

const string doc_TabBadChannels_def =
  "Here you can display the bad channel histograms created by BadChannelMon.cpp. For the histograms to display, there must be a suitable ROOT file loaded\n containing the histgrams produced by BadChannelMon.cpp. To process the HVoff data use vetraOffline on plus:vetraOffline -a -t BADSTRIPS runNumber 20000 20001\n. To process the HVon data:vetraOffline -a -t BADSTRIPS runNumber 20000 40001"  ;
// To create a suitable ROOT file, the -t BADSTRIPS option can be used\n with the standard vetraOffline script ";
const string doc_TabBadChannelsOcc_def =
  "Here you can display the bad channel histograms created by the occupancy monitoring.\nNote that there may be false positives for dead channels and false negatives for noisy channels if the number of events processed is low (dead/noisy channels\nare recognised by imposing occupancy thresholds)."  ;

// Doc for the IV trending tab
// ---------------------------

const string doc_IVTrending_title=
  "Contents of the IV Trending Monitoring:";

const string doc_IVTrending_def =
  "Here you can see the I for selected V as a function of the IV scan since 2010.\n-------------------------------------\nPlease make sure you generate the plots for the histograms before trying to view a sensor.";

const string doc_IVTrending_common =
  "These histograms are designed to show the current as a function of time of particular sensors. Please note that they are not necessarily homogenously spaced - e.g. some IV scans are taken much closer in time than others.";

// Doc for the IV slopes tab
// ---------------------------

const string doc_IVSlopes_title=
  "Contents of the IV Slopes Monitoring:";

const string doc_IVSlopes_def =
  "Here you see the trendint of the IV slopes from 2010.\nThe IV curve is fitted from 50V to the max voltage for the IV scan.\n-------------------------------------\nPlease make sure you generate the plots for the histograms before trying to view a sensor.";

const string doc_IVSlopes_common =
  "These histograms are designed to show the IV slope as a function of time of particular sensors. Please note that they are not necessarily homogenously spaced - e.g. some IV scans are taken much closer in time than others.";


// Doc for the "Error Banks" tab
// -----------------------------
const string doc_TabErrorBanks_title =
"Contents of \"Error Banks\" monitoring: Filled for ZS data \n(For more info see Help->Information on plots...->All Error Bank Plots, provides link to https://lbtwiki.cern.ch/bin/view/VELO/VetraScripts#DrawErrorBanksMoni) \n-------------------------------------\n";

const string doc_TabErrorBanks_def =
"Overview plots of pseudo-header errors, pipeline column number (PCN) errors, channel errors and ADC_FIFO errors.";

const string doc_TabErrorBanks_txtBtnOverview =
"Overview plots for ErrorBanks; error counter, frequency, and types.";

const string doc_TabErrorBanks_txtBtnDetails =
"Plot showing number of error banks per sensor, frequency of the errors per sensor and\ntypes of error (on y-axis: 0=pseudoheader error, 1=PCN error, 2=adc_FIFO error, 3=channel error) per sensor.";

const string doc_TabErrorBanks_txtBtnSEUDetails =
"Single event upset monitoring: filled only for NZS data.\n\nPlot showing statistics on events usable for single event upset monitoring.\nPlot showing number of single event upsets per sensor.\nPlot showing number of single event upsets counter desynchronisations per sensor (should be few).";

const string doc_TabErrorBanks_txtBtnProcStatus =
  "Number of ProcStatus objects in the TES.\n\nEach entry represents when the decoding or pattern recognition found an condition that required it to do something abnormal.\nThe most common cases should be TooManyClusters where the decoding found more that the max cluster limit,\nBeamSplashFastVelo where the FastVelo PR made too many clone and needed the additional clone killing step,\nHeaderErrorVeloBuffer where the TELL1 flagged the header bit of one buffer so the sensor information was ignored.";

const string doc_TabErrorBanks_txtBtnPCNDetails =
"1-D (per sensor) and 2-D (per sensor, per Beetle) PCN error maps";

//=============================================================================
//Trending drop down menu:
//Bad channels removed temporarily
//TString DQSplotNames[33] = {"Average noise (R)", "Average noise (Phi)", "Number of noisy header links",
//	"Number of large residuals", "Bad Channels: Total number dead", "Bad Channels: Total number noisy",
//	"Bad Channels: Max number dead","Bad Channels: Max number noisy" ,"Number of TELL1 with >50 errors/1000", "Clusters: Strips/cluster",
//	"Clusters:% 4-strip clusters","Clusters: MPV (R)","Clusters: FWHM (R)", "Clusters: MPV (Phi)", "Clusters: FWHM (Phi)",
//	"Avg cluster occupancy %","Number of high-occupancy strips","Tracks: Cluster per track", "Tracks: Avg module mismatch %",
//	"Tracks: Sensors with mismatch >20%", "Tracks: Pseudo-Efficiency", "Tracks: Pseudo-Efficiency <90%","Tracks: Avg Residual Pull",
//        "PV: Avg position (X)",  "PV: Avg position (Y)", "PV: Avg position (Z)","PV: Avg L-R X position","PV: Avg L-R Y position","PV: Avg L-R Z position","PV: Beam-centre local (X)",
//        "PV: Beam-centre local (Y)"};

#endif //VELOMONIGUI_H
