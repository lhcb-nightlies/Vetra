//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jan 25 11:42:18 2011 by ROOT version 5.26/00c
// from TTree tree/this is a test tree
// found on file: /scratch/everyone/cfarinel/81811_NZS_ntuple_all.root
//////////////////////////////////////////////////////////

#ifndef GetThresholds_h
#define GetThresholds_h

#include <vector>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

class GetThresholds {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   std::vector<float>   *header_vector;
   std::vector<float>   *channel_vector;
   std::vector<float>   *Tell1_vector;

   // List of branches
   TBranch        *b_header_vector;   //!
   TBranch        *b_channel_vector;   //!
   TBranch        *b_Tell1_vector;   //!

   GetThresholds(TTree *tree=0);
   virtual ~GetThresholds();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef GetThresholds_cxx
GetThresholds::GetThresholds(TTree *tree)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/scratch/everyone/cfarinel/81811_NZS_ntuple_all.root");
      if (!f) {
         f = new TFile("/scratch/everyone/cfarinel/81811_NZS_ntuple_all.root");
      }
      tree = (TTree*)gDirectory->Get("tree");

   }
   Init(tree);
}

GetThresholds::~GetThresholds()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t GetThresholds::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t GetThresholds::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (!fChain->InheritsFrom(TChain::Class()))  return centry;
   TChain *chain = (TChain*)fChain;
   if (chain->GetTreeNumber() != fCurrent) {
      fCurrent = chain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void GetThresholds::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   header_vector = 0;
   channel_vector = 0;
   Tell1_vector = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("header_vector", &header_vector, &b_header_vector);
   fChain->SetBranchAddress("channel_vector", &channel_vector, &b_channel_vector);
   fChain->SetBranchAddress("Tell1_vector", &Tell1_vector, &b_Tell1_vector);
   Notify();
}

Bool_t GetThresholds::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void GetThresholds::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t GetThresholds::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef GetThresholds_cxx
