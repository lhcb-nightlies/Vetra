//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Feb 26 13:23:56 2013 by ROOT version 5.34/03
// from TTree IVtree/IV Tree
// found on file: IVtrending.root
//////////////////////////////////////////////////////////

#ifndef IVAnalysis_h
#define IVAnalysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.

class IVAnalysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Double_t        hour;
   Double_t        minutes;
   Double_t        year;
   Double_t        month;
   Double_t        day;
   Double_t        lumi;
   Double_t        lastfill;
   vector<double>  *ntc1;
   vector<double>  *ntc2;
   vector<double>  *IVvalues;
   vector<double>  *IVslopes1;
   vector<double>  *IVslopes2;
   vector<double>  *IVslopes3;

   // List of branches
   TBranch        *b_hour;   //!
   TBranch        *b_minutes;   //!
   TBranch        *b_year;   //!
   TBranch        *b_month;   //!
   TBranch        *b_day;   //!
   TBranch        *b_lumi;   //!
   TBranch        *b_lastfill;   //!
   TBranch        *b_ntc1;   //!
   TBranch        *b_ntc2;   //!
   TBranch        *b_IVvalues;   //!
   TBranch        *b_IVslopes1;   //!
   TBranch        *b_IVslopes2;   //!
   TBranch        *b_IVslopes3;   //!
   
   // File paths for input / output
   string histoFile;

   IVAnalysis(string histoFile, string rootFile = "");
   virtual ~IVAnalysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   string           map_sensor(int number);
   double           rel_time(double ryear, double rmonth, double rday, double rhours, double rminutes);  
   void             view_graph();
   void             view_sensors(string sensorname, string sensorstation, TCanvas* c1, bool debug,int voltage, int NTCS, int avgtemp, int trend, int xaxis);     
   void             view_all(TCanvas* c1, bool debug, int voltage, int xaxis);  
   string           string_time(double tyear, double tmonth, double tdays, double thours, double tminutes);
   string           string_histYTitle(int voltage);
   string           string_slopeYTitle();
   string           string_histname(int voltage, string sensorname);
   string           string_histnamelumi(int voltage, string sensorname);
   string           string_ntc1name(int voltage, string sensorname);
   string           string_slopename(string sensorname, int number);
   string           string_slopenamelumi(string sensorname, int number);
   string           string_ntc2name(int voltage, string sensorname);
   string           string_average(int voltage);
   string           string_averagelumi(int voltage);
   string           string_branch(int voltage);
   void             view_average(TCanvas *c1, bool m_debug, int voltage,int xaxis);  
   void             calculate_average(TH1F* averagehist, int voltage);
   void             calculate_averagelumi(TH1F* averagehist, int voltage);
};

#endif

#ifdef IVAnalysis_cxx
IVAnalysis::IVAnalysis(string histoFile,string rootFile) : histoFile(histoFile) 
{
  // if parameter rootFile is empty, no NTuple is opened, and only the view 
  // commands will work
  fChain = NULL;
  TFile *f = NULL;
  if (!rootFile.empty())
    {
      f = new TFile(rootFile.c_str(), "READ");
      TTree* tree = (TTree*)f->Get("IVtree");
      Init(tree);
    }
  else
    {
      Init(NULL);
    }

}

IVAnalysis::~IVAnalysis()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t IVAnalysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t IVAnalysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (!fChain->InheritsFrom(TChain::Class())) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void IVAnalysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   ntc1 = 0;
   ntc2 = 0;
   IVvalues = 0;
   IVslopes1 = 0;
   IVslopes2 = 0;
   IVslopes3 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("hour", &hour, &b_hour);
   fChain->SetBranchAddress("minutes", &minutes, &b_minutes);
   fChain->SetBranchAddress("year", &year, &b_year);
   fChain->SetBranchAddress("month", &month, &b_month);
   fChain->SetBranchAddress("day", &day, &b_day);
   fChain->SetBranchAddress("lumi", &lumi, &b_lumi);
   fChain->SetBranchAddress("lastfill", &lastfill, &b_lastfill);
   fChain->SetBranchAddress("ntc1", &ntc1, &b_ntc1);
   fChain->SetBranchAddress("ntc2", &ntc2, &b_ntc2);
   fChain->SetBranchAddress("IVvalues", &IVvalues, &b_IVvalues);
   fChain->SetBranchAddress("IVslopes1", &IVslopes1, &b_IVslopes1);
   fChain->SetBranchAddress("IVslopes2", &IVslopes2, &b_IVslopes2);
   fChain->SetBranchAddress("IVslopes3", &IVslopes3, &b_IVslopes3);
   Notify();
}

Bool_t IVAnalysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void IVAnalysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t IVAnalysis::Cut(Long64_t /*entry*/)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef IVAnalysis_cxx
