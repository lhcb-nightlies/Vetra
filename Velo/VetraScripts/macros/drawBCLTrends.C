//---------------------------------------------------------------------------//
//                                                                           //
//  drawBCLTrends                                                            //
//  ============                                                             //
// This ROOT macro is used by the VeloMoniGUI to show the time-variation     //
// of Bad Channel quantities. The histograms are produced by a python script:        //
//      $VETRASCRIPTSROOT/python/drawBadChannelTrending.py                              //
//                                                                           //
// It can be run outside of the GUI by:                                      //
//      root> .L $VETRASCRIPTSROOT/macros/drawBCLTrends.C+                   //
//      root> drawBCLTrends("BCLHistos.root",DrawOption, DrawOption2, runMin,// 
//                                                                 runMax)   //
//                                                                           //
//      DrawOption 2 is for correlation plots (0 is by run).                 //
// Each quantity has a different number to draw the correct plot:            //
//                                               //
// 1 : histo_deadTotal_vs_run                                                //
// 2 : histo_noisyTotal_vs_run                                               //
// 3 : histo_badTotal_vs_run                                                 //
// @date: 15-06-2011
// @author:Wenchao Zhang                                                                          //
//---------------------------------------------------------------------------//

#include <iostream>
#include <string>
#include <TROOT.h>
#include <TStyle.h>
#include <TH1D.h>
#include <TF1.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TPython.h>
#include <TStyle.h>
#include <map>

class BCLTrends {

    public:
	BCLTrends( 
		std::string fileName,
		bool debug=false );
	virtual ~BCLTrends();
	void draw( TCanvas* canvas, Int_t DrawOption, Int_t DrawOption2, Int_t RunMin, Int_t RunMax,  Bool_t debug );
	void drawPage( TCanvas* canvas, Int_t PageNumber, Bool_t debug, double age , int earliest);
	std::pair<double,double>  findRangeY(TGraph* graph);
	std::pair<double,double>  findRangeX(TGraph* graph, bool XisRun);

    private:
	TFile *m_file;
	bool   m_debug;
	TGraph *makeCorrelation(Int_t graphX, Int_t graphY, Int_t RunMin, Int_t RunMax);
	std::pair<double,double> graphRange; 
	TGraph* m_timestampGraph;

	TGraph* prepareGraph( const std::string& graphTag, const std::string& nameSuffix = "",
          double timeMin = -1, double timeMax = -1);

    void plotGraphSingle( const std::string& graphTag, const std::string& yLabel, 
                          double cutTimeAbs , int earliest);
    void plotGraphTwo( const std::string& title, 
        const std::string& graphTag1, const std::string& label1,
        const std::string& graphTag2, const std::string& label2,
        const std::string& ylabel, double cutTimeAbs, int earliest );
    TList m_legends;
    TList m_graphsPage;
    TGraph* m_currentGraph;
    std::map<double,double> m_runTime;
    const double m_GreatestTime;
};

BCLTrends::BCLTrends( std::string fileName,  bool debug ) 
          : m_currentGraph(NULL), m_GreatestTime(1.0/0.0){
    m_debug = debug;
    m_file = new TFile( fileName.c_str() );
    if(m_debug){
	printf( " drawBCLTrends ... looking for file \"%s\"\n",
		fileName.c_str() );
    }

    if (m_file->IsZombie())
	printf( "Zombie data file ==> Nothing drawn !\n" );


    TGraph* timegr = (TGraph*)m_file->Get("Trends_vs_run/graph_timeStamp_vs_run");
    if ( timegr ){
      // Load all run->time mappings 
      size_t nruns = timegr->GetN();
      double time, run;
      timegr->GetPoint(0, run, time);
      for (size_t i=0; i<nruns; i++){
        timegr->GetPoint((int)i, run, time);
        if (m_runTime[run] == 0.0)
          m_runTime[run] = time;
      }
    }
    else{
      printf("\"drawBCLTrends.C\" could not map run numbers to real time\n");
    }
}

BCLTrends::~BCLTrends() {
	m_graphsPage.Delete();
	m_legends.Delete();
  m_file->Close();
}


TGraph* BCLTrends::makeCorrelation(Int_t graphY, Int_t graphX, Int_t DrawRunMin, Int_t DrawRunMax){
    if(m_debug){
	printf("\"drawBCLTrends.C\" is making correlation plot \n ");
    }
    TGraph *correlatedGraph = new TGraph();
    TGraph* graphToDrawY = (TGraph*)m_file->Get("Trends_vs_run/graph_deadTotal_vs_run");
    TGraph* graphToDrawX = NULL;
    int nGraphPoints = graphToDrawY->GetN();
    Double_t temp1X=0.0;
    Double_t temp2X=0.0;
    Double_t temp1Y=0.0;
    Double_t temp2Y=0.0;
    double * x;
    double * y;
    x = new double [nGraphPoints];
    y = new double [nGraphPoints];        
    if(graphY==0)graphToDrawY=(TGraph*)m_file->Get("Trends_vs_run/graph_deadTotal_vs_run");
    if(graphY==1)graphToDrawY=(TGraph*)m_file->Get("Trends_vs_run/graph_noisyTotal_vs_run");
    if(graphY==2)graphToDrawY=(TGraph*)m_file->Get("Trends_vs_run/graph_badTotal_vs_run");        
        
    if(graphX!=0){
	
	if(graphX==1)graphToDrawX=(TGraph*)m_file->Get("Trends_vs_run/graph_deadTotal_vs_run");
	if(graphX==2)graphToDrawX=(TGraph*)m_file->Get("Trends_vs_run/graph_noisyTotal_vs_run");
	if(graphX==3)graphToDrawX=(TGraph*)m_file->Get("Trends_vs_run/graph_badTotal_vs_run");

	for(Int_t i=0; i<nGraphPoints; i++){
	    graphToDrawX->GetPoint(i, temp1X,temp1Y); 
	    graphToDrawY->GetPoint(i, temp2X,temp2Y); 
	    x[i]= temp1Y; y[i]= temp2Y;
	    if(temp1X>DrawRunMax || temp1X<DrawRunMin){
		x[i]=-99;
		y[i]=-99;
	    }
	}
	correlatedGraph = new TGraph(nGraphPoints, x, y);
	correlatedGraph->GetXaxis()->SetTitle(graphToDrawX->GetTitle());
	correlatedGraph->GetYaxis()->SetTitle(graphToDrawY->GetTitle());
	correlatedGraph->SetTitle("BCL: Correlation plot");
    }
    else{    
	for(Int_t i=0; i<nGraphPoints; i++){
	    graphToDrawY->GetPoint(i, temp1X,temp1Y); 
	    x[i]= temp1X; y[i]= temp1Y;
	    if(temp1X>DrawRunMax || temp1X<DrawRunMin){
		x[i]=-99;
		y[i]=-99;
	    }
	}
	correlatedGraph = new TGraph(nGraphPoints, x, y);
	correlatedGraph->GetXaxis()->SetTitle("Run number");
	correlatedGraph->SetTitle(graphToDrawY->GetTitle());
    }
    delete m_currentGraph;
    m_currentGraph = correlatedGraph;
    delete[] x;
    delete[] y;
    return correlatedGraph;
}

std::pair<double,double> BCLTrends::findRangeY(TGraph* graph){
    if(m_debug){
	printf("\"drawBCLTrends.C\" is finding plot Y range \n ");
    }
    Double_t minVal(0.0), maxVal(0.0);
    Double_t tempX=0.0;
    Double_t tempY=0.0;
    Int_t isNegative=0;
    Double_t RangeMin(0.), RangeMax(0.);
    for(Int_t i=0; i<graph->GetN(); i++){
	graph->GetPoint(i, tempX,tempY); 
	if( (Int_t)tempY!=-99 && tempY<minVal){
	    minVal=tempY;
	    isNegative=1;
	}
	if( (Int_t)tempY!=-99 && tempY>maxVal){
	    maxVal=tempY;
	}
    }
    Double_t   theRange = fabs( TMath::Max( fabs(1.1*minVal), 1.1*maxVal));
    if (theRange==0.0){
	theRange=10.0;
    }
    if(isNegative){ RangeMin=-theRange; RangeMax=theRange;}
    else{ RangeMin=0; RangeMax=theRange;}
    return std::make_pair(RangeMin, RangeMax);
}
std::pair<double,double> BCLTrends::findRangeX(TGraph* graph, bool XisRun){
    if(m_debug){
	printf("\"drawBCLTrends.C\" is finding plot X range \n ");
    }
    Double_t minVal(0.0), maxVal(0.0);
    Double_t tempX=0.0;
    Double_t tempY=0.0;
    Int_t isNegative=0;
    Double_t RangeMin(0.), RangeMax(0.);
    for(Int_t i=0; i<graph->GetN(); i++){
	graph->GetPoint(i, tempX,tempY); 

	if( (Int_t)tempX!=-99 && tempX<minVal){
	    minVal=tempX;
	    isNegative=1;
	}
	if( (Int_t)tempX!=-99 && tempX>maxVal){
	    maxVal=tempX;
	}
    }
    Double_t theRange(0.);
    if(!XisRun)theRange = fabs( TMath::Max( fabs(1.1*minVal), 1.1*maxVal));
    else theRange = fabs( TMath::Max( fabs(minVal), maxVal)); 
    if (theRange==0.0){
	theRange=10.0;
    }
    if(isNegative){ RangeMin=-theRange; RangeMax=theRange;}
    else{ RangeMin=0; RangeMax=theRange;}
    return std::make_pair(RangeMin, RangeMax);
}


// This function removes all entries at -99, and filters by time.
// Time is in units of GPSTime / 1e6 ( ~ unix timestamp )
TGraph* BCLTrends::prepareGraph( const std::string& nameTag, 
        const std::string& nameSuffix,
        double timeMin, double timeMax ){
    if ( m_debug )
        printf( "\"drawBCLTrends.C\" preparing histogram %s\n", 
                (nameTag + nameSuffix).c_str() );
    TGraph* gr = (TGraph*)m_file->Get (
            ("Trends_vs_run/graph_" + nameTag + "_vs_run").c_str() );
    size_t n = gr -> GetN();
    double* x = new double[n], * y = new double[n];
    unsigned int np = 0; // Number of entries in new graph

    bool filterTime = !(timeMin == -1 && timeMax == -1);
    for (size_t i=0; i < n; i++ ){
        gr -> GetPoint( i, x[np], y[np]);
        if ( y[np] != -99.0 )
        {
            if (filterTime){
                double time = m_runTime[ x[np] ];
                if ( time >= timeMin && time < timeMax )
                    np++;
            }
            else{
                np++;
            }
        }
    }

    if ( np > 0 )
    {
        TGraph* newgraph = new TGraph( np, x, y );
        newgraph -> SetTitle( gr -> GetTitle() );
        newgraph -> SetName ( (nameTag + nameSuffix).c_str() );
        //Make the plot look nice...
        newgraph -> SetMarkerStyle( kFullCircle );
        newgraph -> SetMarkerSize(0.5);
        newgraph -> SetMarkerColor(kRed);
        return newgraph;
    }
    else{
        return NULL;
    }
}


void BCLTrends::draw( TCanvas* canvas, Int_t DrawOption, Int_t DrawOption2, Int_t DrawRunMin, Int_t DrawRunMax, Bool_t debug ){

    m_debug = debug;
    if( m_debug ){
	printf( "Running \"drawBCLTrends.C\" draw() function \n");
    }

    if ( m_file -> IsZombie() ) {
	printf( "Zombie data file ==> Nothing drawn !\n" );
	return;
    }  

    TDirectory* main_dir=NULL;
    main_dir=(TDirectory*)m_file->Get("Trends_vs_run");
    if(!main_dir){
	std::cout<< "  --> Cannot open folder: " 
	    << std::endl;
	return;
    }

    //Check flag of user input for header plots:
    if(DrawOption==-1){
	DrawOption=0;
	std::cout<<"-->> DrawOption not set: automatically set to 0" <<std::endl;
    }
    if(DrawOption2==-1){
	DrawOption2=0;
	std::cout<<"-->> DrawOption2 not set: automatically set to 0" <<std::endl;
    }

    //Which quantity to draw:
    if(!(DrawOption>-1 && DrawOption<=2)){
	std::cout<<"Error: Select DrawOption 0-2" <<std::endl;
	return;
    }
    if(!(DrawOption2>-1 && DrawOption2<=3)){
	std::cout<<"Error: Select DrawOption2 0-3" <<std::endl;
	return;
    }

    canvas -> Clear();
    canvas -> cd();
    //Set some ROOT options:
    gROOT->SetStyle("Plain");
    gStyle->SetOptStat(111);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetPalette(1,0);

    //Get the plots from the root file:
    if( m_debug ){
	printf( "\"drawBCLTrends.C\" is getting plots from file. \n" );
    }

    if(m_debug){//Check for existence of files...
	if(!m_file->Get("Trends_vs_run/graph_deadTotal_vs_run")){
	    printf( "\"drawBCLTrends.C\" cannot find \"graph_deadTotal_vs_run\".\n");
	    return;
	}
    }
    if(m_debug){
	printf("\"drawBCLTrends.C\" is going to draw plots now... \n ");
    }

    //Draw the plots
    if(m_debug){
	printf("\"drawBCLTrends.C\" is plotting for DrawOption %d.\n", DrawOption);
    }
    canvas->cd(1);
    TGraph *gr1 = makeCorrelation(DrawOption,DrawOption2, DrawRunMin, DrawRunMax);
    std::pair<double,double> graphRangeY = findRangeY(gr1);
    std::pair<double,double> graphRangeX = findRangeX(gr1, DrawOption2==0);
    //Make the plot look nice... 
    gr1->SetMarkerStyle(20);
    gr1->SetMarkerSize(0.5);
    gr1->SetMarkerColor(kRed);
    gr1->GetYaxis()->SetRangeUser( graphRangeY.first, graphRangeY.second);
    gr1->GetXaxis()->SetRangeUser( graphRangeX.first, graphRangeX.second);
    if(DrawOption2==0) gr1->GetXaxis()->SetRangeUser(DrawRunMin, TMath::Min(graphRangeX.second, (Double_t)DrawRunMax)) ;
    //cout<<"DrawOption is "<<DrawOption<<" "<<"DrawOption2 is "<<DrawOption2<<endl;
    gr1->Draw("AP");
}






void drawBCLTrends( std::string fileName,
	Int_t DrawOption=-1, Int_t DrawOption2=-1, Int_t RunMin=0, Int_t RunMax=0, TCanvas* canvas=0,
  bool isFileUpdated=true, bool debug=false )
{
    static BCLTrends* bcl_instance = 0;
    if ( debug ) {
	printf( "Running \"drawBCLTrends.C\" on file \"%s\"\n",
		fileName.c_str() );
    }

    BCLTrends* del = NULL;
    if (isFileUpdated || !bcl_instance){
      del = bcl_instance;
      bcl_instance = new BCLTrends( fileName, debug );
    }
    if ( canvas == 0 )
	canvas = new TCanvas( "BCL Trends", "BCL Trends", 1 );
    bcl_instance -> draw( canvas, DrawOption, DrawOption2, RunMin, RunMax,  debug );
    canvas -> Modified();
    canvas -> Update();

    delete del;
}


//=============================================================================
