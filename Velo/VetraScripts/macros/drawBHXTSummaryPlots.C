//---------------------------------------------------------------------------//
//                                                                           //
//  drawBHXTSummaryPlots.C                                                   //
//  =========================                                                //
//                                                                           //
//  Draws a 2d histogram of sensor number versus analogue                    //
//  link with the noise in the first channel of each analogue link           //
//                                                                           //
//                                                                           //
//  Requirements:                                                            //
//  - run VeloBeetlHeaderXTalkCorrectionMoni                                 //
//                                                                           //
//  Example usage:                                                           //
//  - in root, run with                                                      //
//    .L drawBeetleXTalk.C                                                   //
//    drawBHXTSummaryPlots("myfile.root")                                    //
//  - from VELOMoniGUI plotted in XTalk tab                                  //
//                                                                           //
//  @author Tomasz Szumlak, Chris Parkes                                     //
//  @date   2009-01-06                                                       //
//                                                                           //
//---------------------------------------------------------------------------//

// Include files 
#include <iostream>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>

// local includes
#include "../src/helperTools.h"


//=============================================================================
// Class encapsulating all scripts
//=============================================================================
class BHXTSummaryPlots{
  
public:

  BHXTSummaryPlots(std::string file_name);
  virtual ~BHXTSummaryPlots();
  void draw_2d_summary(TCanvas* canvas);
  void draw_1d_summary(TCanvas* canvas);
  
private:

  TFile* m_file;
  
};

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
BHXTSummaryPlots::BHXTSummaryPlots(std::string file_name) {

  // use the default tool to get ptr to a root filr
  m_file=helperTools::getOrOpen(file_name);
  
  if(m_file->IsZombie()){

    printf( " --> File is corrupted!\n" );
    // if corrupted, do not leave it unattended
    m_file=0;

  }

}
//=============================================================================
// Destructor
//=============================================================================
BHXTSummaryPlots::~BHXTSummaryPlots() {} 

//=============================================================================
void BHXTSummaryPlots::draw_2d_summary(TCanvas* canvas)
{

  gStyle->SetOptStat(kFALSE);

  // --> Summary plots for BHXT
  const char* name="Vetra/VeloBeetleHeaderXTalkCorrectionMoni/BHXT_SummaryPlot_2D";
  TH2D* summ_2d = NULL;
  if(m_file){

    summ_2d = (TH2D*)m_file->Get(name);
    
  }else{
    
    std::cout<< " --> Cannot open root file " <<std::endl;
    return;
    
  }

  if(!summ_2d) {
    std::cout<< " --> The Beetle Header summary plot does not exist" 
                       << std::endl;
    return;
  }
  
  summ_2d->SetMinimum(-2.0);
  summ_2d->SetMaximum(5.0);
  summ_2d->GetXaxis()->SetTitle("Sensor");
  summ_2d->GetXaxis()->SetTickLength(0.01);
  summ_2d->GetYaxis()->SetTitle("Analogue Link");
  summ_2d->GetYaxis()->SetTickLength(0.01);
  summ_2d->GetXaxis()->SetLabelSize(0.03);
  summ_2d->GetYaxis()->SetLabelSize(0.03);
  summ_2d->SetTitle("Noise on 1st Channel Minus Average Noise of Link");

  canvas->cd();
  canvas->Clear();
  summ_2d->Draw("COLZ");
 
  std::cout << "2D summary drawn" << std::endl;

  return;
  
}

//=============================================================================
void BHXTSummaryPlots::draw_1d_summary(TCanvas* canvas)
{

  gStyle->SetOptStat(kTRUE);

  // --> Summary plots for BHXT
  const char* name="Vetra/VeloBeetleHeaderXTalkCorrectionMoni/BHXT_DiffPlot_1D";
  TH1D* summ_1d = NULL;
  if(m_file){

    summ_1d= (TH1D*)m_file->Get(name);
    
  }else{
    
    std::cout<< " --> Cannot open root file " <<std::endl;
    return;
    
  }

  if(!summ_1d) {
    std::cout<< " --> The Beetle Header summary plot does not exist" 
                       << std::endl;
    return;
  }

  summ_1d->SetMaximum(900.);
  summ_1d->GetXaxis()->SetTitle("ADC");
  summ_1d->GetXaxis()->SetTickLength(0.01);
  summ_1d->GetYaxis()->SetTitle("# of entries");
  summ_1d->GetYaxis()->SetTickLength(0.01);
  summ_1d->GetXaxis()->SetLabelSize(0.03);
  summ_1d->GetYaxis()->SetLabelSize(0.03);
  summ_1d->SetTitle("Noise difference between the first and the reference channel");

  canvas->cd();
  canvas->Clear();
  summ_1d->Draw("P");
 
  std::cout << "1D summary drawn" << std::endl;

  return;
  
}

//=============================================================================
// main macro
//=============================================================================
void drawBHXTSummaryPlots(std::string fileName, Int_t page, TCanvas* canvas=0)
{

  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  gStyle->SetOptStat(2210);
  gROOT->ForceStyle();

  BHXTSummaryPlots* plot=new BHXTSummaryPlots(fileName);

  if(canvas==0) {
    canvas=new TCanvas("canvas", "Beetle Header X-Talk",
                       10, 10, 900, 700);
  }
 
  if(1==page) plot->draw_2d_summary(canvas);
  if(2==page) plot->draw_1d_summary(canvas);
  
  canvas->Modified();
  canvas->Update();

  // --> remove dynamically created objects
  delete plot;

  return;
 
}
