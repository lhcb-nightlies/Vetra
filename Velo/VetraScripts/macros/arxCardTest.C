#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <TFile.h>
#include <TDirectory.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TText.h>
#include <TPaveText.h>

void styleSingle();
void styleStamp();

void arxCardTest(Char_t inFile[], Int_t tell1Id, int dummy=-1)
{
	if(dummy==-1) dummy=tell1Id;
	

  // expected number of events per active link

  const Int_t nExpectedEvents = 500;

  // number of links
  const Int_t nLinks = 64;

  Int_t iBinLow, iBinHigh, nExpectedEventsActual;

  const Float_t ampMin=455., ampMax = 485., freqMin=0.9861, freqMax=1.0139, offsetMin=500., offsetMax=530., residualsMin=-5., residualsMax = 5.;
  Float_t xMin, xMax, yMin, yMax;
  Double_t binIntegral;

  Char_t dirName[50], rootFileName[400], plotBaseName[400], plotName[400],
    histBaseName[50], histName[50], primitiveName[5], xAxisTitle[50], yAxisTitle[50], linkNumber[2];

  TPaveText *pt;
  TText *ttext;

  TH1 *hTest;
  TH1D *hArxTest;
  TH2D *h2dArxTest;

  // Open ROOT file and access (sub)directories

  sprintf(rootFileName,"%s.root",inFile);
  TFile *ff=TFile::Open(rootFileName);
  if ( ff == NULL ) {
    std::cout << " ==> nothing done!" << std::endl;
    exit( -1 );
  }
 
  sprintf(dirName, "ArxTestTELL1%d", dummy);
  TDirectory *tdArxTest = (TDirectory*) ff->Get(dirName);
  if ( tdArxTest == NULL ) {
    std::cout << " Directory Vetra/" << dirName << " does not exist" << std::endl;
    exit( -1 );
  }

  // check number of events per active link

  Int_t iActiveConnector;
  styleSingle(); 
  hArxTest = (TH1D*) tdArxTest->Get("hEventsPerActiveLink");
  Int_t nBins = hArxTest->GetNbinsX();
  for (Int_t j=1; j<nBins+1; j++) {
    if (hArxTest->GetBinContent(j) > (nExpectedEvents/2)) {
      iActiveConnector = (j-1)/16;
      break;
    }
  }

  Int_t iLink;

  std::cout << std::endl;
  for (Int_t j=0; j<16; j++) {
    iLink = 16*iActiveConnector + j;
    if (hArxTest->GetBinContent(iLink+1) != nExpectedEvents) {
      std::cout << "WARNING: For link " << iLink << " " << nExpectedEvents << " events are expected, but " << hArxTest->GetBinContent(iLink+1) << " are present!!!" << std::endl;
    }
  }

  std::cout << std::endl;

  // plot events per actve link

  TCanvas *c1 = new TCanvas("c1", "Events per Active Link", 400, 300);
  sprintf(plotBaseName,"%s_eventsperlink",inFile);
  hArxTest->SetTitle("Events per active link");
  hArxTest->GetXaxis()->SetTitle("link");
  hArxTest->GetYaxis()->SetTitle("#events");
  hArxTest->DrawCopy("histo");
  pt = new TPaveText(0.1,0.01,0.9,0.05,"NDC");
  ttext = pt->AddText(plotBaseName);
  pt->SetBorderSize(0);
  pt->SetFillColor(0);
  pt->Draw();
  c1->Update();
  sprintf(plotName,"%s.ps",plotBaseName);
  c1->Print(plotName);
  sprintf(plotName,"%s.png",plotBaseName);
  c1->Print(plotName);

  // plot indication for noisy links

  hArxTest = (TH1D*) tdArxTest->Get("hIndicationNoisyLinks");
  if (hArxTest != 0) {
    TCanvas *c10 = new TCanvas("c10", "Indication of Noisy Links", 400, 300);
    sprintf(plotBaseName,"%s_noisylinks",inFile);
    hArxTest->SetTitle("Indication of Noisy Links");
    hArxTest->GetXaxis()->SetTitle("link");
    hArxTest->GetYaxis()->SetTitle("#entries");
    hArxTest->DrawCopy("histo");
    pt = new TPaveText(0.1,0.01,0.9,0.05,"NDC");
    ttext = pt->AddText(plotBaseName);
    pt->SetBorderSize(0);
    pt->SetFillColor(0);
    pt->Draw();
    c10->Update();
    sprintf(plotName,"%s.ps",plotBaseName);
    c10->Print(plotName);
    sprintf(plotName,"%s.png",plotBaseName);
    c10->Print(plotName);
  }
  
  // plot quantities per link
  
  styleStamp(); 

  for (Int_t j=0; j<7; j++){
    // for (Int_t j=0; j<1; j++) {
    TCanvas *c2 = 0, *c3 = 0, *c4 = 0, *c5 = 0, *c6 = 0, *c7 = 0, *c8 = 0, *tempCanvas = 0;

    switch(j)
    {
    case 0:
      c2 = new TCanvas("c2", "Sine Fit Amplitude", 750, 900);
      tempCanvas = c2;
      sprintf(histBaseName,"hSineAmp");
      sprintf(plotBaseName,"%s_sineAmplitude",inFile);
      sprintf(xAxisTitle,"amplitude [ADC counts]");
      xMin = ampMin;
      xMax = ampMax;
      break;
    case 1:
      c3 = new TCanvas("c3", "Sine Fit Frequency", 750, 900);
      tempCanvas = c3;
      sprintf(histBaseName,"hSineFreq");
      sprintf(plotBaseName,"%s_sineFrequency",inFile);
      sprintf(xAxisTitle,"frequency [MHz]");
      xMin = freqMin;
      xMax = freqMax;
      break;
    case 2:
      c4 = new TCanvas("c4", "Sine Fit Offset", 750, 900);
      tempCanvas = c4;
      sprintf(histBaseName,"hSineOffset");
      sprintf(plotBaseName,"%s_sineOffset",inFile);
      sprintf(xAxisTitle,"offset [ADC counts]");
      xMin = offsetMin;
      xMax = offsetMax;
      break;
    case 3:
      c5 = new TCanvas("c5", "Residuals w.r.t. Sine Fit", 750, 900);
      tempCanvas = c5;
      sprintf(histBaseName,"hResiduals");
      sprintf(plotBaseName,"%s_residuals",inFile);
      sprintf(xAxisTitle,"residual [ADC counts]");
      xMin = residualsMin;
      xMax = residualsMax;
      break;
    case 4:
      c6 = new TCanvas("c6", "Occupancy per ADC Count", 750, 900);
      tempCanvas = c6;
      sprintf(histBaseName,"hAdcVsChannel");
      sprintf(plotBaseName,"%s_occupancy",inFile);
      sprintf(xAxisTitle,"ADC counts");
      break;
    case 5:
      c7 = new TCanvas("c7", "Residuals vs ADC Counts", 900, 750);
      tempCanvas = c7;
      sprintf(histBaseName,"hResidualsVsAdcCounts");
      sprintf(plotBaseName,"%s_linearity",inFile);
      sprintf(xAxisTitle,"ADC counts");
      sprintf(yAxisTitle,"residual [ADC counts]");
      yMin = residualsMin;
      yMax = residualsMax;
      break;
    case 6:
      c8 = new TCanvas("c8", "Bit Occupancy", 750, 900);
      tempCanvas = c8;
      sprintf(histBaseName,"hBitHighOccupancy");
      sprintf(plotBaseName,"%s_bitOccupancy",inFile);
      sprintf(xAxisTitle,"Bit");
      break;
    }

    std::cout << std::endl;
    std::cout << tempCanvas->GetTitle() << std::endl;
    std::cout << std::endl;

    tempCanvas->Divide(4,4);
    for (Int_t i=1; i<=16; i++) {
      sprintf(primitiveName,"c%d_%d",j+2,i);
      ((TPad *)(tempCanvas->GetPrimitive(primitiveName)))->SetGrid();
      if (j==4) {
        ((TPad *)(tempCanvas->GetPrimitive(primitiveName)))->SetLogy();
      }
    }

    if (j==5) {
      gStyle->SetTitleOffset(0.9,"y");
    }

    for(Int_t i=0; i<nLinks; i++){

      hArxTest = 0;
      h2dArxTest = 0;

      sprintf(histName,"%s_Link_%d",histBaseName,i);
      // std::cout << histName << std::endl;
      hTest = (TH1*) tdArxTest->Get(histName);
      if (hTest != 0) {
        if (hTest->GetDimension() == 1) {
          hArxTest = (TH1D*) tdArxTest->Get(histName);
          if ((j != 4) && (j != 6)) {
            hArxTest->SetMaximum(1.4*hArxTest->GetMaximum());
          }
          if (j == 6) {
            hArxTest->SetMinimum(0);
          }
        } else {
          h2dArxTest = (TH2D*) tdArxTest->Get(histName);
        }
        if (j == 4) {
          if (h2dArxTest != 0) {
            hArxTest = h2dArxTest->ProjectionY();
            h2dArxTest = 0;
          } else {
            hArxTest = 0;
          }
        }
        tempCanvas->cd((i%16)+1);
        if (j>=4) {
          gStyle->SetOptStat(0);
        }
      }
      
      if (hArxTest != 0) {
        // std::cout << "Histogram for link " << i << " exists!" << std::endl;
        if ((j != 4) && (j != 6)) {
          hArxTest->GetXaxis()->SetRangeUser(xMin,xMax);
        }
        hArxTest->GetXaxis()->SetTitle(xAxisTitle);
        hArxTest->SetFillColor(17);
        hArxTest->Draw("histo");
      }
      if (h2dArxTest != 0) {
        // std::cout << "Histogram for link " << i << " exists!" << std::endl;
        h2dArxTest->GetYaxis()->SetRangeUser(yMin,yMax);
        h2dArxTest->GetXaxis()->SetTitle(xAxisTitle);
        h2dArxTest->GetYaxis()->SetTitle(yAxisTitle);
        h2dArxTest->Draw("");
      }

      // draw link number
      
      if ((hArxTest != 0) || h2dArxTest != 0) {
        sprintf(linkNumber,"%i",i);
        pt = new TPaveText(0.8,0.2,0.88,0.28,"NDC");
        ttext = pt->AddText(linkNumber);
        pt->SetBorderSize(0);
        pt->SetFillColor(0);
        pt->Draw();
      }

      // check for links with less than the expected number of events ...
            
      if ((j != 5) && (j != 6)) {
        
        if (hArxTest != 0) {

          if ((j != 3) && (j != 4)) {
            nExpectedEventsActual = nExpectedEvents;
          } else {
            nExpectedEventsActual = 36*nExpectedEvents;
          }
          
          // ... in the displayed histogram range
          
          if (j != 4) {
            iBinLow = hArxTest->FindBin(xMin,0,0);
            iBinHigh = hArxTest->FindBin(xMax,0,0);
          } else {
            iBinLow = 0;
            iBinHigh = hArxTest->GetNbinsX();
          }
          // std::cout << iBinLow << " " << iBinHigh << std::endl;
          binIntegral = hArxTest->Integral(iBinLow,iBinHigh);
          if (binIntegral < nExpectedEventsActual) {
            std::cout << "Link " << i << " has only " << binIntegral << " events in the displayed histogram range!" << std::endl;
          }
          
          // ... in the whole histogram range
          
          binIntegral = hArxTest->Integral(1,hArxTest->GetNbinsX());
          if (binIntegral < nExpectedEventsActual) {
            std::cout << "Link " << i << " has only " << binIntegral << " events in the whole histogram range!" << std::endl;
          }
        }
      }
    }
    tempCanvas->cd(0);
    pt = new TPaveText(0.1,0.24,0.9,0.26,"NDC");
    ttext = pt->AddText(plotBaseName);
    pt->SetBorderSize(0);
    pt->SetFillColor(0);
    pt->Draw();
    tempCanvas->Update();
    sprintf(plotName,"%s.ps",plotBaseName);
    tempCanvas->Print(plotName);
    sprintf(plotName,"%s.png",plotBaseName);
    tempCanvas->Print(plotName);
  }
    
}

void styleSingle() {

  TStyle *st1 = new TStyle("st1", "my style");

  st1->SetCanvasColor(0);
  st1->SetCanvasBorderMode(0);

  st1->SetPadColor(0);
  st1->SetPadBorderMode(0);
  st1->SetPadLeftMargin(0.1);
  st1->SetPadRightMargin(0.05);
  st1->SetPadTopMargin(0.08);
  st1->SetPadBottomMargin(0.15);

  // histogram title

  st1->SetOptTitle(1);
  st1->SetTitleFont(132,"");
  st1->SetTitleBorderSize(1);
  st1->SetTitleFillColor(0);
  st1->SetTitleX(0.1);

  // statistic box

  st1->SetStatColor(0);
  // st1->SetOptStat(111110);
  st1->SetOptStat(0);
  // st1->SetOptDate(3);
  st1->SetStatFont(132);

  // axis labels

  st1->SetLabelOffset(0.01,"x");
  st1->SetLabelOffset(0.01,"y");
  st1->SetLabelFont(132,"xyz");

  // axis title

  st1->SetTitleOffset(1.2,"x");
  st1->SetTitleOffset(1.4,"y");
  st1->SetTitleFont(22,"xyz");
  st1->SetPaperSize(19.,27.);
  st1->SetPalette(1);

  // paper size

  st1->SetPaperSize(19.5,29.);

  // apply style

  st1->cd();
  gROOT->ForceStyle();
  
}


void styleStamp() {

  TStyle *st1 = new TStyle("st1", "my style");

  st1->SetCanvasColor(0);
  st1->SetCanvasBorderMode(0);

  st1->SetPadColor(0);
  st1->SetPadBorderMode(0);
  st1->SetPadLeftMargin(0.13);
  st1->SetPadRightMargin(0.05);
  st1->SetPadTopMargin(0.02);
  st1->SetPadBottomMargin(0.15);

  // histogram title

  st1->SetOptTitle(0);
  st1->SetTitleFont(132,"");
  st1->SetTitleBorderSize(1);
  st1->SetTitleFillColor(0);
  st1->SetTitleX(0.1);

  // statistic box

  st1->SetStatColor(0);
  st1->SetOptStat(1001100);
  st1->SetStatBorderSize(1);
  //st1->SetStatW(0.5);
  //st1->SetStatH(0.2);
  st1->SetStatX(1.-(st1->GetPadRightMargin()));
  st1->SetStatY(1.-(st1->GetPadTopMargin()));
  // st1->SetOptStat(0);
  // st1->SetOptDate(3);
  st1->SetStatFont(132);
  st1->SetStatFontSize(0.07);
  
  // axis labels

  st1->SetLabelOffset(0.01,"x");
  st1->SetLabelOffset(0.01,"y");
  st1->SetLabelFont(132,"xyz");
  st1->SetLabelSize(0.07,"xyz");

  // axis title

  st1->SetTitleOffset(1.1,"x");
  st1->SetTitleOffset(1.1,"y");
  st1->SetTitleFont(22,"xyz");
  st1->SetTitleSize(0.07,"xyz");
  st1->SetPaperSize(19.,27.);
  st1->SetPalette(1);

  // paper size

  st1->SetPaperSize(19.5,29.);
  
  // apply style

  st1->cd();
  gROOT->ForceStyle();
  
}
