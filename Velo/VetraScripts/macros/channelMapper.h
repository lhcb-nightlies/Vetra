//---------------------------------------------------------------------------//
//                                                                           //
//  channelMapper.h                                                          //
//  ===============                                                          //
//                                                                           //
// Prototype class that maps between different types                         //
// channel numbers in the system. For instance between                       //
// HV channel number and slot number.                                        //
//                                                                           //
// Should be replaced by a more complete and generic                         //
// implementation of the mapping.                                            //
//                                                                           //
// @author Lars Eklund                                                       //
// @date   2009-04-24                                                        //
//                                                                           //
//---------------------------------------------------------------------------//

#ifndef CHANNELMAPPER_H
#define CHANNELMAPPER_H 1

// Includes from STL
#include <string>


//=============================================================================
// Class
//=============================================================================
class channelMapper {
  
public:
  channelMapper( bool debug = false );
  virtual ~channelMapper();
  
  std::string slotToHVChan( std::string slotNum);
  int         slotToSWNum( std::string slotNum);
  std::string nextSlot( std::string slotNum);
  std::string prevSlot( std::string slotNum);
  std::string nextHVChan( std::string hvChan);
  std::string prevHVChan( std::string hvChan);
  std::string hvChanToSlot( std::string hvChan);
  int         intToSWNum(int i);
  std::string intToHVChan(int i);
  std::string intToSlot(int i);
  std::string intToSlotName(int i);
  std::string intToSide(int i);
  
private:
  const static int nSensors = 88;
  std::string m_hvChan[nSensors];
  std::string m_slotNum[nSensors];
  int         m_SWNum[nSensors];
};

//=============================================================================

#endif // CHANNELMAPPER_H
