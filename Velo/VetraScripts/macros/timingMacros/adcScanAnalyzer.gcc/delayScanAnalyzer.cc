//--------------------------------------------------------------------------------------//
//                                                                                      //
//  delayScanAnalyzer.C                                                                 //
//  ===================                                                                 //
//                                                                                      //
//  About macro:                                                                        //
//  1. reads the right mapping of sensors to Tell1 from a list (Tell1IDtoSensor.list)   //
//  2.  Open the delay scan file and add the even and odd pulse profiles into an array  //
//      of histograms called hSampling Phase                                            //
//  3.  The hSampling Phase contains at most 64 links * 88 sensosrs = 5632  plots       //
//  4.  For each link,optimal delay is found as following                               //
//  i)  Rising and Falling edges of pulse in channel 23 are found                       //
//  ii) Starting from the centre bin on plateau, it is required that the difference of  //
//      the current bin value and ceter value is less than 20% of the ceter value. This //
//      makes sure that value is closed to the center. Second, the difference of the    //
//      current bin value and next bin value is required to be less than 15% of the     //
//      current bin value. This prevents the optimal point to be too close to the       //
//      falling edge. Otherwsie, we move back one bin and go back to the first condition//
//  iii)The optimal Delay steps are stored in an array.                                 //  
//  5.  For a set of 16 links to each of the 4 PP-FPGA card, an average is found and is //
//      displayed as horizontal red line.                                               //  
//  6.  For all the links, ADC values corresponding to optimal phase value for TP23 is  //
//      plotted. An average line is also drawn.                                         //
//  7.  Four sets of .text and .xml files are written with optimal delay setting at     //
//      at optimal point on plateau, rising edge, falling edge and asymmetric settings  //
//      such that sensor 0 has link 0 at rising edge and rest at optimal point, sensor 1//
//      has link 1 on rising edge and rest on optimal point and so on.                  //
//      An option "SpecialPattern = true" can be set off if one requires to display     //
//      normal delay settings.                                                          //
//  8.  Three canvases are set to show the following                                    //
//  i)  overlayed set of 16 links corresponding to each PP-FPGA card in 4 pads          //
//  ii) Zoom of channel 4 for the above overlayed links                                 //
//  iii)Zoom of channel 23 for the above overlayed links, with the sampling point shown // 
//      as the average line over all the 16 links.                                      //
//  8.  A set of 4 canvases, divided into 16 pads corresponding to each link in PP-FPGA //
//      are shown. A red line shows the sampling point.                                 //
//                                                                                      //
//  Requirements:                                                                       //
//  - needs input file with Non-Zero-Suppressed (NZS) data                              //
//    containing the directory structure                                                //
//    "Vetra/TELL1_NNN/"                                                                //
//    or                                                                                //
//    "Vetra/TELL1_NNN/"                                                                //
//    with histograms RMSNoise_vs_ChipChannel.                                          //
//                                                                                      //
//  Example usage:                                                                      //
//  - in root, execute with                                                             //
//    .L noiseMoni.C                                                                    //
//     analyzeAllTell1s("fileName", false)                                       //
//  - if one wants to run with Debugmode, issue the command                             //
//     analyzeAllTell1s("fileName", true)                                        //                   
//                                                                                      //
//  @author Kazu Akiba, Sadia Khalil                                                    //
//  @date   2008-12-02                                                                  //
//                                                                                      //
//--------------------------------------------------------------------------------------//

#include "TProfile.h"
#include "TObject.h"
#include <map>
#include <stdio.h>
#include "TArray.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TH1D.h"
#include "TAxis.h"
#include "TH2F.h"
#include "TF1.h"
#include <TMath.h>
#include <TMinuit.h>
#include "TCanvas.h"
#include "TPad.h"
#include "TLine.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TText.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TSystem.h"
#include <string>
#include <vector>
#include <iostream>
#include <ostream>
#include <sstream>
#include "delayScanAnalyzer.h"


TCanvas *c2;
TCanvas* cplots[4];//= {NULL};
TPad *pplots[4][4];
// sensorToTell1ID.clear();
using namespace std;
// Delay Scan for a single Tell1
// -----------------------------
void profileDelayscan(char inFile[], int sensorId, bool debugMode){
  bool usingChipChannel = true;
  // if (sensorToTell1ID.begin() == sensorToTell1ID.end() ) readSensorTell1File();
  if (sensorToTell1ID.size() == 0 ) readSensorTell1File();
  char histBaseName[] = "hSamplingPhaseScan"; //profile histos. 
  char dirName[50], rootFileName[400], histName[50], plotBaseName[400], plotName[400], padName[50], legendName[50];
  // Open ROOT file and access (sub)directories
  sprintf(rootFileName,"%s",inFile);
  TFile *file=TFile::Open(rootFileName);
  TDirectory *tdTell1;
  if ( file == NULL ) {
	cout << " No such file!" << endl;
	return;
  }
  else{
	sprintf(dirName, "TELL1_%03d", sensorId);
	if(tdPhaseScan) tdTell1 = (TDirectory*) tdPhaseScan->Get(dirName); 
	else{ 
	  tdPhaseScan = (TDirectory*) file->Get("Vetra/PhaseScan");
	  tdTell1 = (TDirectory*) tdPhaseScan->Get(dirName); 
	  if (tdTell1 == NULL ) {
		cout << " Directory " << dirName << " does not exist" << endl;
		return;
	  }
	}
  }

  int tell1Id = sensorToTell1ID[sensorId];//NOTICE IT IS TELL1 ID
  cout<<"___________________________________________________________"<<endl;
  printf("sensorId = %d, tell1Id = %d -- taking proper action! \n ", sensorId, tell1Id);
  // Temporaray objects used in plotting later
  // Histogram settings
  TLegend *legend[4]; 
  style(); 
  // fill difference of even and odd channels to TObjArray
  TObjArray histArray(64); // array of 64 histograms one for each link. (prof)
  for(int iLink=0; iLink<nLinks; iLink++) {
	sprintf(histName, "%sEven_Link_%d", histBaseName, iLink);
	hSamplingPhaseEven = (TProfile*) tdTell1->Get(histName);
	sprintf(histName, "%sOdd_Link_%d", histBaseName, iLink);
	hSamplingPhaseOdd = (TProfile*) tdTell1->Get(histName);
	sprintf(histName, "%sEvenMinusOdd_%d", histBaseName, iLink);
	hSamplingPhase = addHistograms(histName, hSamplingPhaseEven, hSamplingPhaseOdd, 1., -1.);
	histArray.AddAt(hSamplingPhase,iLink);
  }
  
  // Max and Min y-axis settings for the display canvas
  // --------------------------------------------------
  // The lines below find the minimum and maximum ADC counts
  // in each group of 16 plots that are overlayed later on in the macro.
  double minPlot=0, maxPlot=0;
  getMinAndMaxFromHistArray(histArray, minPlot, maxPlot); 

  // Determine the optimal delay
  // ---------------------------
  //int iTPChannel = 23; // expected beetle channel pulsed
  // int iWindow = 3; //+- 3 channel shift from the bin of expected Center Plateau
  // int iOptimalShiftFromPlateauCenter = 5; //+5 bin shift to the bin number corresponding to found platea 
  // int plateauWidth = 10; // number of steps where the  test pulse should have stable adc counts.
  // int channelWidth = 16; // full width of the channel -- usually the same as the number of steps.
  ///SSSSSS/////// double centerPlateauExpected = 22.5001; // channel 23 goes from 22.5 to 23.5
  //int TPHeight = 20;
  TH1D *hPlateauCenter = new TH1D("hPlateauCenter", "plateau center per link", 64, -0.5, 63.5); 
  TH1D *hOptimalDelay = new TH1D("hOptimalDelay", "optimal delay per link", 64, -0.5, 63.5); // Optimal delay 0..65 (2*25 +15)
  TH1D *hOptimalPhase = new TH1D("hOptimalPhase", "optimal Phase per link", 64, -0.5, 63.5); // optimal phase 0..16
  TH1D *hOptimalCycle = new TH1D("hOptimalCycle", "optimal Cycle per link", 64, -0.5, 63.5); // optimal cycle (times 25 ns)
  TH1D *hHeaderCrossTalk;
 

  c2->cd();
  sprintf(plotBaseName,"%s_OptimalDelay_Tell1_%03d",inFile, tell1Id);
  TPaveText *ptc2 = new TPaveText(0.1,0.01,0.9,0.05,"NDC");
  TText *tc2 = ptc2->AddText(plotBaseName);
  ptc2->SetBorderSize(0);
  ptc2->SetFillColor(0);
  ptc2->Draw();
  hOptimalDelay->SetMarkerStyle(20);
  hOptimalDelay->SetMarkerSize(0.5);
  hOptimalDelay->Draw("p");
  hOptimalDelay->GetXaxis()->SetTitle("link");
  hOptimalDelay->GetYaxis()->SetTitle("optimal delay [phase delay]");
  c2->Update();

  //FILE *logfile;
  //logfile = fopen("log","w");
  for (int iLink=0; iLink<nLinks; iLink++) {
	int iRisingEdge, iFallingEdge;// edges of the test pulse. 
	int iPlateauBin; // bin number corresponding to found plateau 
	int iOptimalDelay; // Optimal sampling phase to be determined.
	hSamplingPhase = (TH1D*) histArray[iLink];
	double centerPlateauExpected = 22.5001; // channel 24 goes from 23.5 to 24.5
	// get the bin where we should expect the test pulse.
	int iCenterPlateauExpectedBin = hSamplingPhase->GetXaxis()->FindBin(centerPlateauExpected); 
	// Rising and falling edge
	iOptimalDelay = findRisingAndFallingEdges(hSamplingPhase, iRisingEdge, iFallingEdge);
	//transform this value from a bin number to a delta number of steps.
	iOptimalDelay = iOptimalDelay - iCenterPlateauExpectedBin;// expect the test pulse to come in channel 23.
	iRisingEdge = iRisingEdge - iCenterPlateauExpectedBin;
	iFallingEdge = iFallingEdge - iCenterPlateauExpectedBin;
	cout << " The chip link # " << iLink << " is delayed by delta " << iOptimalDelay << " steps" << endl;
	int optimalPhase = (iOptimalDelay)%16; // to transform it to a phase setting
	int optimalcycle = ((int)(iOptimalDelay/16)); // to get the cycle delay.
	float optimaldelay = 16*optimalcycle + optimalPhase; // to confirm if it's not some crazy value.
	cout << "Optimal Phase:  " << optimalPhase << ", Optimal Cycle: " << optimalcycle << ", Optimal Delay: " << optimaldelay << endl;
	hPlateauCenter->SetBinContent(iLink+1, iPlateauBin-iCenterPlateauExpectedBin);
	hOptimalDelay->SetBinContent(iLink+1, optimaldelay); 
	hOptimalPhase->SetBinContent(iLink+1, optimalPhase);
	hOptimalCycle->SetBinContent(iLink+1, optimalcycle);
	hOptimalDelay->SetBinError(iLink+1, 0.0001);
    
    
   
  }
   
  writeXmlFile(tell1Id,  hOptimalDelay, "testXml", true);
  TLine *averageLine[4];// display line on optimal delay
  // Get the average over delay sampling points
  //-------------------------------------------
  int optimalLinkPhase[64] ={0};
  int optimalLinkCycle[64] ={0};
  int optimalLinkDelay[64] ={0};
  double averagePhasePerPP[4]={0};
  double averageCyclePerPP[4]={0};
  double averageDelayPerPP[4]={0};
  for(int ipp =0; ipp < nPPFPGAs; ipp++){
	// first, get the averages:
	double linksInAverage =0;
	for(int iLink =0; iLink < nLinksPerPPFPGA; iLink++){
	  int linkNumber = ipp*nLinksPerPPFPGA +iLink;
	  //cout << " linkNumber " << ipp*nLinksPerPPFPGA +iLink << endl;    
	  //if(usingChipChannel == true) linkNumber = convertFromChipToTell1Link(linkNumber);
	  int optimalDelay = (int) (hOptimalDelay->GetBinContent(linkNumber+1)); 
	  //cout << " optimal delays "  <<   optimalDelay << endl;
	  if(optimalDelay < 47 &&  optimalDelay  >= 0) {
		averageDelayPerPP[ipp] += optimalDelay;
		linksInAverage++;
	  }
	}
	if(linksInAverage !=0){
	  averageDelayPerPP[ipp]= averageDelayPerPP[ipp]/linksInAverage;
	  averagePhasePerPP[ipp]= ((int) (floor(averageDelayPerPP[ipp]) ))%16;
	  averageCyclePerPP[ipp]=  (int) (floor( (averageDelayPerPP[ipp]/16.) ));
	  averageDelayPerPP[ipp]= (int) floor(averageDelayPerPP[ipp] +0.5);
	}
	else{
	  averagePhasePerPP[ipp]= 0; 
	  averageCyclePerPP[ipp]= 0; 
	  averageDelayPerPP[ipp]= 0;
	}
	cout << " averagePhasePerPP = " <<  averagePhasePerPP[ipp] 
         << " averageCyclePerPP = " <<  averageCyclePerPP[ipp]
         << " averageDelayPerPP = " <<  averageDelayPerPP[ipp] << endl;
	if (averageDelayPerPP[ipp]<0){ 
	  cout << "\n\n\n Something is wrong, Could you please check this Tell1? \n"
		<< "It's Tell1 " << tell1Id << " also known as sensor " << sensorId 
		<< "\n\n\n  " <<  endl;
	  cout << " hit Enter to continue " << endl; 
	  getchar(); 
	}

	averageLine[ipp] = new TLine(-0.5+((double) ipp*nLinksPerPPFPGA),
		((double) averagePhasePerPP[ipp]+ nSteps*averageCyclePerPP[ipp] ),
		-0.5+((double)(ipp+1)*nLinksPerPPFPGA),
		((double) averagePhasePerPP[ipp]+ nSteps*averageCyclePerPP[ipp]));
	averageLine[ipp]->SetLineColor(kRed);
	averageLine[ipp]->Draw();
  } 
  c2->Update();
  // --------------------------------
  // Canvas Settings to plot full picture, TP4 and TP23
  // --------------------------------------------------
  double xAveLine;
  TLine *optimalDelayAveLine;
  TLine *optimalDelayAveLineCh0;

  // Plotting
  // --------
  for(int ipp =0; ipp < nPPFPGAs; ipp++){
	// first, get the averages:
	double linksInAverage =0;
	for(int iLink =0; iLink < nLinksPerPPFPGA; iLink++){
	  int linkNumber = ipp*nLinksPerPPFPGA +iLink;
	  int pad = ipp+1;
	  hSamplingPhase = (TH1D*) histArray[linkNumber];
	  hSamplingPhase->SetLineColor(colorTable[iLink]);// setting different colours for 16 links
	  hSamplingPhase->SetLineWidth(2);
 
      //Fill the histogram with the header cross talk;
      hHeaderCrossTalk = (TH1D*) hSamplingPhase->Clone();
      int ntotalbins = hHeaderCrossTalk->GetNbinsX() +1;
      for(int jbin = 1; jbin < ntotalbins; jbin++){
		hHeaderCrossTalk->SetBinContent(jbin, hSamplingPhase->GetBinError(jbin));
      }
      
   
	  // adc axis and legend settings for a set of 16 links
	  cplots[0]->cd(); 
	  pplots[0][ipp]->cd(); 
	  if(iLink == 0){ 
		sprintf(histName, "PP-FPGA %d", ipp);
		hSamplingPhase->SetTitle(histName);
		hSamplingPhase->GetXaxis()->SetTitle("channel");
		hSamplingPhase->GetYaxis()->SetTitle("ADC counts");
		hSamplingPhase->GetXaxis()->SetRangeUser(-4.5,31.5);
		hSamplingPhase->GetYaxis()->SetRangeUser(1.1*minPlot,1.1*maxPlot);
		hSamplingPhase->DrawCopy("histo");
	  }
	  else hSamplingPhase->DrawCopy("histosame");

	  cplots[1]->cd(); 
	  pplots[1][ipp]->cd(); 
	  if(iLink == 0){ 
		hSamplingPhase->GetXaxis()->SetRangeUser(1.,6.);
		hSamplingPhase->GetYaxis()->SetRangeUser(-20,maxPlot); 
		hSamplingPhase->DrawCopy("histo");
	  }
	  else hSamplingPhase->DrawCopy("histosame");

	  cplots[2]->cd(); 
	  pplots[2][ipp]->cd(); 
	  if(iLink == 0){ 
		hSamplingPhase->GetXaxis()->SetRangeUser(21.,26.);
		hSamplingPhase->GetYaxis()->SetRangeUser(1.1*minPlot,20); 
		hSamplingPhase->DrawCopy("histo");
	  }
	  else hSamplingPhase->DrawCopy("histosame");
	  cplots[3]->cd(); 
	  pplots[3][ipp]->cd(); 
	  if(iLink == 0){ 
		hHeaderCrossTalk->GetXaxis()->SetRangeUser(-1.5,2.5);
		hHeaderCrossTalk->GetYaxis()->SetRangeUser(-0.5, 10);
		hHeaderCrossTalk->DrawCopy("histo");
	  }
	  else hHeaderCrossTalk->DrawCopy("histosame");
	  if(iLink == 0){ 
	  legend[ipp] = new TLegend((pplots[2][ipp]->GetUxmin()+0.12*(pplots[2][ipp]->GetUxmax()-pplots[2][ipp]->GetUxmin())), 
		  (pplots[2][ipp]->GetUymin()+0.6*(pplots[2][ipp]->GetUymax()-pplots[2][ipp]->GetUymin())), 
		  (pplots[2][ipp]->GetUxmin()+0.27*(pplots[2][ipp]->GetUxmax()-pplots[2][ipp]->GetUxmin())), 
		  (pplots[2][ipp]->GetUymin()+0.97*(pplots[2][ipp]->GetUymax()-pplots[2][ipp]->GetUymin())),"","");
	  legend[ipp]->SetFillColor(4000);
	  legend[ipp]->SetBorderSize(1);
      }
	  sprintf(legendName, "Link %d", linkNumber);
	  legend[ipp]->AddEntry(hSamplingPhase,legendName,"l");
	}
	for (int k=-1; k<=1; k++) {
	  cplots[2]->cd(); 
	  pplots[2][ipp]->cd(); 
	  int iCenterPlateauExpectedBin = hSamplingPhase->GetXaxis()->FindBin(22.5001); // get the bin where we should expect the test pulse.
	  int linebin = iCenterPlateauExpectedBin + (int) (averageCyclePerPP[ipp]*16) + (int)averagePhasePerPP[ipp]; 
	  //int linebin = iCenterPlateauExpectedBin + (int) averageDelayPerPP[ipp]; 
	  cout << " average delay pp " << ipp << " = " <<  linebin << endl;
	  xAveLine = hSamplingPhase->GetXaxis()->GetBinCenter(linebin) +  k;
	  optimalDelayAveLine = new TLine(xAveLine,minPlot,xAveLine,20);
	  if (k==0) optimalDelayAveLine->SetLineStyle(1);
	  else optimalDelayAveLine->SetLineStyle(2);
	  optimalDelayAveLine->SetLineColor(kRed);
	  optimalDelayAveLine->Draw();
	  cplots[3]->cd(); 
	  pplots[3][ipp]->cd(); 
	  iCenterPlateauExpectedBin = hSamplingPhase->GetXaxis()->FindBin(-0.4999); // get the bin where we should expect the test pulse.
	  linebin = iCenterPlateauExpectedBin + (int) (averageCyclePerPP[ipp]*16) + (int)averagePhasePerPP[ipp]; 
	  xAveLine = hSamplingPhase->GetXaxis()->GetBinCenter(linebin) + k;
	  optimalDelayAveLineCh0 = new TLine(xAveLine,minPlot,xAveLine,20);
	  if (k==0) optimalDelayAveLine->SetLineStyle(1);
	  else optimalDelayAveLine->SetLineStyle(2);
	  optimalDelayAveLineCh0->SetLineColor(kRed);
	  optimalDelayAveLineCh0->Draw();
	}
	for(int icanvas=0; icanvas<4; icanvas++){
	  for(int ipad =0; ipad<4; ipad++){
		cplots[icanvas]->cd();
		pplots[icanvas][ipad]->cd();
		if (icanvas == 2 ) legend[ipp]->Draw();
	  }
	  cplots[icanvas]->Update();
	}
  }
}
// ---------------------------------------------------------

//////////////////////////////////////////////////
// Main function
// ------------- 
void analyzeAllTell1s(char infile[], bool debugMode){
  cplots[0] = new TCanvas("cplots0", "Digitisation Delay Scan", 800, 600);
  cplots[1] = new TCanvas("cplots1", "Digitisation Delay Scan Zoom TP4", 800, 600);
  cplots[3] = new TCanvas("cplots3", "Digitisation Delay Scan Zoom Ch0", 800, 600);
  cplots[2] = new TCanvas("cplots2", "Digitisation Delay Scan Zoom TP23", 800, 600);
  c2 =   new TCanvas("c2", "Optimal Delay Per Link", 700, 500, 600, 400); 
  setupCanvases();
  //if (sensorToTell1ID.begin() == sensorToTell1ID.end()) readSensorTell1File();
  if (sensorToTell1ID.size() == 0){
	cout << " size of the map " << sensorToTell1ID.size() << endl;
    cout << " reading tell1 mapping data"  << endl;
	readSensorTell1File();
     
  }
  logfile = fopen ("delays.log", "w+");// output file of optimal delay info for each Tell1
  char rootfilename[300];
  char dirName[50], rootFileName[400], histName[50], plotBaseName[400], plotName[400], padName[50], legendName[50];
  sprintf(rootfilename,"%s",infile);
  TFile *ff=TFile::Open(rootfilename);
  if ( ff == NULL ) {
	cout << " No such file!" << endl;
	return;
  }
  tdPhaseScan = (TDirectory*) ff->Get("Vetra/PhaseScan");
  if ( tdPhaseScan == NULL ) {
	cout << " Directory " << dirName << " does not exist" << endl;
	return;
  }
  else{
	TList *listOfTell1s = tdPhaseScan->GetListOfKeys();
	TIter Tell1(listOfTell1s);
	TObject *tell1inlist;
	int outofhere= 1;
	while((tell1inlist = Tell1())!=0 && outofhere !=0){
	  printf("Got %s from the Tree\n", tell1inlist->GetName());
	  char tell1dirname[200];
	  sprintf(tell1dirname, "%s", tell1inlist->GetName());
	  int tell1tempid;
	  sscanf(tell1dirname, "TELL1_%03d", &tell1tempid);
	  //     iSensors.push_back(tell1tempid);    
	  profileDelayscan(infile, tell1tempid, debugMode);
	  //outofhere++;
      //getchar();
	  if(outofhere >2) outofhere = 0;
	}
  }
}

//////////////////////////////////////////////////
void readPUSensorTell1File(){
  ifstream Tell1ToSensorList("Tell1IDtoSensor_PU.list");
  while(Tell1ToSensorList.good()){
	int tell1=0, sensor = 0;
	Tell1ToSensorList >> tell1 >> sensor; // >> endl;
	if(tell1 ==0 && sensor ==0) continue;
	sensorToTell1ID[sensor] = tell1;
	printf(" sensor %d mapped to tell1ID %d \n", sensor, sensorToTell1ID[sensor]);
  }
}
//////////////////////////////////////////////////
void readSensorTell1File(){
  gROOT->ProcessLine(".! /group/velo/python/TELL1Map.py");
  ifstream Tell1ToSensorList("/group/velo/config/livdb_tell1_sensor_map.txt");
  char header[256];
  if (Tell1ToSensorList.good()) Tell1ToSensorList.getline(header,256);
  else{
    cout << " got the tell1 file but it's empty " << endl;
    return;
  }
  cout << header  << endl;
  while(Tell1ToSensorList.good()){
	int tell1=0, sensor = 0;
	char tell1name[50];
	Tell1ToSensorList >> tell1name >> sensor; // >> endl;
	sscanf(tell1name,"TL-%d",&tell1);
	if(tell1 ==0 && sensor ==0) continue;
	sensorToTell1ID[sensor] = tell1;
	printf(" sensor %d mapped to tell1ID %d \n", sensor, sensorToTell1ID[sensor]);
  }
}

//////////////////////////////////////////////////
// Function to add two histograms of equal bin number
// --------------------------------------------------
/*
TH1D* addHistograms(char addHistName[], TProfile *h1One, TProfile *h1Two, double cOne, double cTwo) {
  double contOne, contTwo;
  int nBinsOne =  h1One->GetXaxis()->GetNbins();
  double lowXOne =  h1One->GetXaxis()->GetXmin();
  double hiXOne = h1One->GetXaxis()->GetXmax();
  int nBinsTwo =  h1Two->GetXaxis()->GetNbins();
  double lowXTwo = h1Two->GetXaxis()->GetXmin();
  double hiXTwo = h1Two->GetXaxis()->GetXmax();
  TH1D* add = new TH1D(addHistName,addHistName,nBinsOne,lowXOne,hiXOne);
  if ((nBinsOne == nBinsTwo) && (lowXOne == lowXTwo) && (hiXOne == hiXTwo)) {
	for (int i=1; i<nBinsOne; i++) {
	  contOne = h1One->GetBinContent(i);
	  contTwo = h1Two->GetBinContent(i);
	  add->SetBinContent(i,cOne*contOne+cTwo*contTwo);
	}
  } else {
	cout << "addHistograms: The two histgrams " << " and " << " do not have the same binning!!! Adding cannot be performed!" << endl;
  }
  return add;
}
*/
TH1D* addHistograms(char addHistName[], TProfile *h1One, TProfile *h1Two, double cOne, double cTwo) {
  double contOne, contTwo;
  double errorOne, errorTwo;
  int nBinsOne =  h1One->GetXaxis()->GetNbins();
  double lowXOne =  h1One->GetXaxis()->GetXmin();
  double hiXOne = h1One->GetXaxis()->GetXmax();
  int nBinsTwo =  h1Two->GetXaxis()->GetNbins();
  double lowXTwo = h1Two->GetXaxis()->GetXmin();
  double hiXTwo = h1Two->GetXaxis()->GetXmax();
  TH1D* add = new TH1D(addHistName,addHistName,nBinsOne,lowXOne,hiXOne);
  if ((nBinsOne == nBinsTwo) && (lowXOne == lowXTwo) && (hiXOne == hiXTwo)) {
    for (int i=1; i<nBinsOne; i++) {
      contOne = h1One->GetBinContent(i);
      contTwo = h1Two->GetBinContent(i);
      errorOne = cOne*h1One->GetBinError(i);
      errorTwo = cTwo*h1Two->GetBinError(i);
      double adderror = sqrt(errorOne*errorOne + errorTwo*errorTwo);
      add->SetBinContent(i,cOne*contOne+cTwo*contTwo);
      add->SetBinError(i,adderror);
    }
  } else {
    cout << "addHistograms: The two histgrams " << " and " << " do not have the same binning!!! Adding cannot be performed!" << endl;
  }
  return add;
}
//////////////////////////////////////////////////////////////////////////
int convertFromChipToTell1Link(int inputlink){
  int ipp = (int) (inputlink/16);
  int linkNumber = -200;
  if(ipp ==0 ) linkNumber= (3)*nLinksPerPPFPGA +(inputlink%16);
  if(ipp ==1 ) linkNumber= (2)*nLinksPerPPFPGA +(inputlink%16);
  if(ipp ==2 ) linkNumber= (1)*nLinksPerPPFPGA +(inputlink%16);
  if(ipp ==3 ) linkNumber= (0)*nLinksPerPPFPGA +(inputlink%16);
  //cout << "from link " << inputlink << " to link " << linkNumber << endl;
  return linkNumber;


}
//////////////////////////////////////////////////

void writeXmlFile(int tell1Id, TH1D *hOptimalDelay, char directory[], bool usingChipChannel ){

  int optCableDelay = 23; // cable delay used during data taking

  FILE *xmlFile;
  char command[500]; sprintf(command, ".! mkdir %s > /dev/null ",directory);
  gROOT->ProcessLine(command);
  char plotName[500];
  sprintf(plotName,"%s/digitisationDelays_tell1_%d.xml", directory, tell1Id);
  xmlFile = fopen(plotName,"w");
  if(xmlFile==0){
	cout << "xml files could not be written to the specified directory: "<< directory << endl;
	return;
  }
  int optimalPhase[64] ={0};
  int optimalCycle[64] ={0};
  double averagePhasePerPP[4]={0};
  double averageCyclePerPP[4]={0};
  for(int ipp =0; ipp < nPPFPGAs; ipp++){
	// first, get the averages:
	double linksInAverage =0;
	for(int iLink =0; iLink < nLinksPerPPFPGA; iLink++){
	  int linkNumber = ipp*nLinksPerPPFPGA +iLink;
	  //cout << " linkNumber " << ipp*nLinksPerPPFPGA +iLink << endl;    
	  if(usingChipChannel == true) linkNumber = convertFromChipToTell1Link(linkNumber);
	  int optimalDelay = (int) (hOptimalDelay->GetBinContent(linkNumber+1)); 
	  //cout << " optimal delays "  <<   optimalDelay << endl;
	  if( optimalDelay < 100 && 
		  optimalDelay  >= 0   ) {
		int phase = (optimalDelay)%16;
		int cycle = (optimalDelay)/16;
		averagePhasePerPP[ipp] += phase; 
		averageCyclePerPP[ipp] += cycle;
		linksInAverage++;
		if(cycle >2  || cycle < 0  ){
		  cout << "Cycle delay out of the allowed range. Check it! " <<  cycle << endl;
		}
	  }
	}
	if(linksInAverage !=0){
	  averagePhasePerPP[ipp]= (int) floor(averagePhasePerPP[ipp]/linksInAverage +0.5);
	  averageCyclePerPP[ipp]= (int) floor(averageCyclePerPP[ipp]/linksInAverage +0.5);
	}
	else{
	  averagePhasePerPP[ipp]= 0; 
	  averageCyclePerPP[ipp]= 0; 
	}
	for(int iLink =0; iLink < nLinksPerPPFPGA; iLink++){
	  int linkNumber = ipp*nLinksPerPPFPGA +iLink;
	  // this is to remap in case of running with chip channel instead of tell1 channel;
	  if(usingChipChannel == true) linkNumber = convertFromChipToTell1Link(linkNumber);
	  int optimalDelay = (int) (hOptimalDelay->GetBinContent(linkNumber+1));
	  if( optimalDelay < 100 && 
		  optimalDelay  >= 0   ) {
		optimalPhase[linkNumber] = (optimalDelay)%16; // to transform it to a phase setting
		optimalCycle[linkNumber] = ((int)(optimalDelay/16)); // to get the cycle delay.
	  }
	  else{
		optimalPhase[linkNumber] =(int) (averagePhasePerPP[ipp]);
		optimalCycle[linkNumber] = (int) (averageCyclePerPP[ipp]); 
	  }
	}
  }
  fprintf(xmlFile, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  fprintf(xmlFile, "<!DOCTYPE DDDB SYSTEM \"conddb:/DTD/structure.dtd\">\n");
  fprintf(xmlFile, "<DDDB>\n");
  fprintf(xmlFile, "	<condition name=\"DigitisationDelays\">\n");
  fprintf(xmlFile, "		<param name=\"CableDelay\" type=\"int\">\n");
  fprintf(xmlFile, "		%d\n", optCableDelay);
  fprintf(xmlFile, "		</param>\n");
  fprintf(xmlFile, "		<paramVector name=\"CycleDelays\" type=\"int\">\n");
  fprintf(xmlFile, "		");
  for (int jLink=0; jLink<nLinks; jLink++) {
	fprintf(xmlFile, "%d ", optimalCycle[jLink]); 
	printf( "%d ", optimalCycle[jLink]); 
  }
  fprintf(xmlFile, "\n");
  printf("\n");
  fprintf(xmlFile, "		</paramVector>\n");
  fprintf(xmlFile, "		<paramVector name=\"PhaseDelays\" type=\"int\">\n");
  fprintf(xmlFile, "		");
  for (int jLink=0; jLink<nLinks; jLink++) {
	fprintf(xmlFile, "%d ", optimalPhase[jLink]);
	printf("%d ", optimalPhase[jLink]);
  }
  fprintf(xmlFile, "\n");
  printf("\n");
  fprintf(xmlFile, "		</paramVector>\n");
  fprintf(xmlFile, "	</condition>\n");
  fprintf(xmlFile, "</DDDB>\n");
  fclose(xmlFile);
}


int findRisingAndFallingEdges(TH1D *hSamplingPhase, int &iRisingEdge,int &iFallingEdge){ // returns the mean value between rising and falling edges.
  int iWindow = 3; //+- 3 channel shift from the bin of expected Center Plateau
  int iOptimalShiftFromPlateauCenter = 5; // Try to shift to the "left" in order to minimize crosstalk.
  int plateauWidth = 10; // number of steps where the test pulse should have stable adc counts.
  int channelWidth = 16; // full width of the channel -- usually the same as the number of steps.
  double centerPlateauExpected = 22.5; // channel 23 goes from 22.5 to 23.5
  int iCenterPlateauExpectedBin = hSamplingPhase->GetXaxis()->FindBin(centerPlateauExpected); // get the bin where we should expect the test pulse.
  int iCenterOfPlateauBin =0; // the middle of the test pulse channel.
  double binValue, binValueNext;// auxiliaries
  int iOptimalDelay = 0; // the determined delay with respect to the bin iCenterPlateauExpectedBin.
  int TPHeight = 30; // the test pulse height should be at least about this many adc counts.
  iRisingEdge = -7000; // bin number of the rising edge
  iFallingEdge = -8000;// bin number of the falling edge  
  double binValueHalfWay=0; // value of the bin shifting half number of steps from the rising/falling edge
  double binValueAllWay=0; // value of the bin shifting to the next channel.
  int halfway = nSteps/2 - 2; // -2 to add some more stability and make sure we dont fall out of the plateau.
  int lowerTimeRange = iCenterPlateauExpectedBin - iWindow*nSteps; 
  int upperTimeRange = iCenterPlateauExpectedBin + iWindow*nSteps;

  for (int iTimeStep = lowerTimeRange; iTimeStep<= upperTimeRange; iTimeStep++) {
	binValue = fabs(hSamplingPhase->GetBinContent(iTimeStep));
	binValueNext = fabs(hSamplingPhase->GetBinContent(iTimeStep+1));   
	binValueHalfWay = fabs(hSamplingPhase->GetBinContent(iTimeStep+halfway));
	// determine the rising edge:
	binValueAllWay = fabs(hSamplingPhase->GetBinContent(iTimeStep+nSteps));
	if(binValueHalfWay > TPHeight/2  // some adc count around the TP23 height over 2.
		&& (binValue <=  binValueHalfWay/2. ) // current bin equal or smaller than half height 
		&& (binValueNext >= binValueHalfWay/2.) // next bin equal or greater than half height
	  ){
	  iRisingEdge = iTimeStep;
	}
	// determine the falling edge:
	binValueHalfWay = fabs(hSamplingPhase->GetBinContent(iTimeStep-halfway));
	if(binValueHalfWay > TPHeight/2  // some adc count around the test pulse height times 2.
		&& (binValue >=  binValueHalfWay/2. ) // current bin equal or greater than half height 
		&& (binValueNext <= binValueHalfWay/2.) // next bin equal or smaller than half height
	  ){
	  iFallingEdge = iTimeStep;
	}
  }
  // if failed to find the  falling edge, set it to the rising edge +nSteps:
  if(iFallingEdge == -8000 && iRisingEdge >0)  iFallingEdge = iRisingEdge+nSteps; 
  // if failed to find the  rising edge, set it to the falling edge +nSteps:
  else if(iFallingEdge >0  && iRisingEdge == -7000)   iRisingEdge = iFallingEdge -nSteps; 
  // if failed to find both, set both to an impossible value:
  else if(iFallingEdge < -200   && iRisingEdge < -200)   {
	iFallingEdge = iRisingEdge = -2000;
	return - 5000;
  } 

  double average = (double)(iRisingEdge + iFallingEdge)/2.; 
  iCenterOfPlateauBin = (int) floor(average+0.5); // find the middle pont between the edges
  cout << "iRisingEdge: " << iRisingEdge << ", iFallingEdge: " << iFallingEdge << ", iCenterOfPlateauBin: " << iCenterOfPlateauBin << endl;
  // determine optimal delay
  iOptimalDelay = iCenterOfPlateauBin; // starting point for the optimal delay is the center bin.
  for(int ibin = iCenterOfPlateauBin; ibin < (iCenterOfPlateauBin +iOptimalShiftFromPlateauCenter); ibin++){ 
	double plateauval = fabs(hSamplingPhase->GetBinContent(iCenterOfPlateauBin));
	double binval = fabs(hSamplingPhase->GetBinContent(ibin));
	double binvalnext = fabs(hSamplingPhase->GetBinContent(ibin+2));
	double difference = fabs(binval - plateauval)/plateauval;
	double ratio = fabs(binvalnext/binval);
	//cout << " plateau val" << plateauval << " binval " << binval <<  " binvalnext " 
	//     << binvalnext << " ratio "  <<  ratio <<  " difference " << difference  << endl;
	if(difference < 0.10      // make sure that current bin is close to the plateau value.
		&& ( ratio  > 0.85) ){ // make sure that the optimal delay is not too close to the edge.
	  iOptimalDelay = ibin;
	}
	else{
	  iOptimalDelay = ibin-1; // if the plateau is too close to the edge, move 1 bin back.
	  break;       // if we moved to some place also too close to the edge, move one back and break the loop. 
	}
  } 
  cout << "iOptimalDelay: "<< iOptimalDelay <<", iCenterPlateauExpectedBin: "<< iCenterPlateauExpectedBin << endl;
  return iOptimalDelay;
}

void setupCanvases(){
  char padstring[50];
  for(int icanvas =0; icanvas<4; icanvas++){
	cplots[icanvas]->Divide(2,2);
	for(int ipad =0; ipad<4; ipad++){
	  sprintf(padstring, "cplots%d_%d",icanvas, ipad+1);
	  pplots[icanvas][ipad] = (TPad *)(cplots[icanvas]->GetPrimitive(padstring));
	  pplots[icanvas][ipad]->SetGrid();
	}
  }
}

///////////////////////////////////////////////////////////////////////////////////////////
void getMinAndMaxFromHistArray(TObjArray histArray, double &minPlot, double &maxPlot){
  double actMin, actMax;
  maxPlot =0; minPlot = 0;
  for (int i=0; i<nLinks; i++){
	hSamplingPhase = (TH1D*) histArray[i];
	actMin = hSamplingPhase->GetMinimum();
	actMax = hSamplingPhase->GetMaximum();
	if (actMin < minPlot) minPlot = actMin;
	if (actMax > maxPlot) 	maxPlot = actMax;
  }
  if(minPlot < -100) minPlot = -100;
  if(maxPlot > 100) maxPlot = 100;
}

int main(int argc, char  *argv[]){
  analyzeAllTell1s(argv[1], true);
  return 1;
}
