#include "TProfile.h"
#include "TObject.h"
#include <map>
#include <stdio.h>
#include "TArray.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TAxis.h"
#include "TH2F.h"
#include "TF1.h"
#include <TMath.h>
#include <TMinuit.h>
#include "TCanvas.h"
#include "TPad.h"
#include "TLine.h"
#include "TPaveText.h"
#include "TText.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TSystem.h"
#include <string>
#include <vector>
#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>

TDirectory *tdVetra, *tdPhaseScan;
TProfile *hSamplingPhaseEven, *hSamplingPhaseOdd; 
TH1D *hSamplingPhase;
TH1D *hSamplingPhaseStoN;
FILE *logfile;
TFile *hfile;
std::map<int, int> sensorToTell1ID;
std::vector<int> iSensors;
bool SpecialPattern = false; //special asymmetric optimal delay scan settings
bool RisingEdge = true;
bool FallingEdge = true;
bool OptimalSetting = true;
const int nLinks = 64;
const int nLinksPerPPFPGA = 16;
const int nSteps = 16; // number of steps used in the scan function (step thingy in pvss)
const int nPPFPGAs = 4; 
int colorTable[nLinksPerPPFPGA] ={50,41,38,30,28,25,15,9,8,7,6,5,4,3,2,1}; // definition of the color for each link
void readSensorTell1File();
void setupCanvases(); 
void readPUSensorTell1File();
TH1D* addHistograms(char *, TProfile *, TProfile *, double , double );
void histoSetting(int , int , TH1D &, TLine &);
void profileDelayscan(char , int , bool);
void analyzeAllTell1s(char , bool ); 
int findRisingAndFallingEdges(TH1D *,  int &, int &); // returns the mean value between rising and falling edges.
//void writeXmlFile(int , TH1D *, char );

//void writeXmlFile(int tell1Id, TH1D *hOptimalDelay, TH1D *hOptimalCycle, TH1D *hOptimalPhase, 
//                  int *optPhaseDelayPerPpfpga, int *optCycleDelayPerPpfpga, char directory[]);
//void writeXmlFile(int a,  TH1D *b, char  c[], bool d);
void writeXmlFile(int tell1Id, TH1D *hOptimalDelay, char directory[], bool usingChipChannel = true);
int convertFromChipToTell1Link(int);
void getMinAndMaxFromHistArray(TObjArray histArray, double &minPlot, double &maxPlot);

void style() {
  TStyle *st1 = new TStyle("st1", "my style");
  st1->SetCanvasColor(0);
  st1->SetCanvasBorderMode(0);
  st1->SetPadColor(0);
  st1->SetPadBorderMode(0);
  st1->SetPadLeftMargin(0.1);
  st1->SetPadRightMargin(0.05);
  st1->SetPadTopMargin(0.08);
  st1->SetPadBottomMargin(0.15);
  // histogram title
  st1->SetOptTitle(1);
  st1->SetTitleFont(132,"");
  st1->SetTitleBorderSize(1);
  st1->SetTitleFillColor(0);
  st1->SetTitleX(0.1);
  // statistic box
  st1->SetStatColor(0);
  // st1->SetOptStat(111110);
  st1->SetOptStat(0);
  // st1->SetOptDate(3);
  st1->SetStatFont(132);
  // axis labels
  st1->SetLabelOffset(0.01,"x");
  st1->SetLabelOffset(0.01,"y");
  st1->SetLabelFont(132,"xyz");
  // axis title
  st1->SetTitleOffset(1.2,"x");
  st1->SetTitleOffset(1.4,"y");
  st1->SetTitleFont(22,"xyz");
  st1->SetPaperSize(19.,27.);
  st1->SetPalette(1);
  // Legend
  // apply style
  st1->cd();
  gROOT->ForceStyle();
}

