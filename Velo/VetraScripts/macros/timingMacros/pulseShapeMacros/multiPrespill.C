
multiPrespill(){
  //TFile *file0 = TFile::Open("cmstpscan38159.root");
 // TFile *file0 = TFile::Open("cmstpscan47614_arasDebuged.root");
 TFile *file0 = TFile::Open("../cmstpscan46126_arasDebuged.root");
  TDirectory *dVelo= file0->Get("Velo");
  dVelo->cd(); 
  TDirectory *dPulseShape = dVelo->Get("PulseShape");
  dPulseShape->cd();
  TH1F *hprespill = new TH1F("hprespill", "hprespill", 51, -0.1,1.00);
  TH1F *hprespill1plus = new TH1F("hprespill1plus", "prespill after 1ns", 51, -0.1,1.00);
  TH1F *hprespill2plus = new TH1F("hprespill2plus", "prespill after 2ns", 51, -0.1,1.00);
  TH1F *hprespill3plus = new TH1F("hprespill3plus", "prespill after 3ns", 51, -0.1,1.00);
  TH1F *hprespill4plus = new TH1F("hprespill4plus", "prespill after 4ns", 51, -0.1,1.00);
  TH1F *hprespill5plus = new TH1F("hprespill5plus", "prespill after 5ns", 51, -0.1,1.00);
  TH1F *hprespill6plus = new TH1F("hprespill6plus", "prespill after 6ns", 51, -0.1,1.00);
  
  int fixpeaktimebin =0;
 // int sensornumber = sensorNumber;
  for(int ichannel = 0; ichannel < 2048; ichannel++){
    if((ichannel % 32 == 4)  || (ichannel %32 ==23)){
	  for(int isensor = 0; isensor < 164; isensor++){
    //if((ii % 32 == 23) ){
	  char histoeven[200], histoodd[200];
		sprintf(histoeven, "profcmsTestPulseShape_Tell1_%03d_Even_ch_%04d",isensor,ichannel );
		sprintf(histoodd, "profcmsTestPulseShape_Tell1_%03d_Odd_ch_%04d",isensor,ichannel );
        //cout << " histoodd " <<  histoodd << endl;
        //cout << " histoeven " <<  histoeven << endl;
        TProfile *hpEven = (TProfile*) gDirectory->Get(histoeven);
        TProfile *hpOdd = (TProfile*) gDirectory->Get(histoodd);
        if(hpEven == 0) continue;
        if(hpOdd == 0) continue;
        int polarity = pow(-1.0,ichannel%2);
        //int evenodd = (1+polarity)/2;
        //cout << "evenodd " << evenodd << endl;
        TH1D *temphist = (TH1D*) addHistograms("result", hpOdd, hpEven, polarity*0.5, -1*polarity*0.5);
        int  peaktimebin = temphist->GetMaximumBin();
        //if(ichannel == 4) fixpeaktimebin = peaktimebin;
        fixpeaktimebin = peaktimebin;
        double  peaktime = temphist->GetBinCenter(peaktimebin);
        double  maxamplitude = temphist->GetBinContent(fixpeaktimebin);
        double  prespill = temphist->GetBinContent(fixpeaktimebin-25)/maxamplitude;
		cout << "prespill at the peak " << prespill <<endl ;
		double prespillvalueplus1 = -5;
		double prespillvalueplus2 = -5;
		double prespillvalueplus3 = -5;
		double prespillvalueplus4 = -5;
		double prespillvalueplus5 = -5;
		double prespillvalueplus6 = -5;
      if(temphist->GetBinContent(fixpeaktimebin+1))  prespillvalueplus1 = temphist->GetBinContent(fixpeaktimebin-25+1)/temphist->GetBinContent(fixpeaktimebin+1); 
      if(temphist->GetBinContent(fixpeaktimebin+2))  prespillvalueplus2 = temphist->GetBinContent(fixpeaktimebin-25+2)/temphist->GetBinContent(fixpeaktimebin+2); 
      if(temphist->GetBinContent(fixpeaktimebin+3))  prespillvalueplus3 = temphist->GetBinContent(fixpeaktimebin-25+3)/temphist->GetBinContent(fixpeaktimebin+3); 
      if(temphist->GetBinContent(fixpeaktimebin+4))  prespillvalueplus4 = temphist->GetBinContent(fixpeaktimebin-25+4)/temphist->GetBinContent(fixpeaktimebin+4); 
      if(temphist->GetBinContent(fixpeaktimebin+5))  prespillvalueplus5 = temphist->GetBinContent(fixpeaktimebin-25+5)/temphist->GetBinContent(fixpeaktimebin+5); 
      if(temphist->GetBinContent(fixpeaktimebin+6))  prespillvalueplus6 = temphist->GetBinContent(fixpeaktimebin-25+6)/temphist->GetBinContent(fixpeaktimebin+6); 
      cout << "prespill at the peak +5ns" << prespillvalueplus5 <<endl ;
      hprespill->Fill(prespill);
      hprespill1plus->Fill(prespillvalueplus1);
      hprespill2plus->Fill(prespillvalueplus2);
      hprespill3plus->Fill(prespillvalueplus3);
      hprespill4plus->Fill(prespillvalueplus4);
      hprespill5plus->Fill(prespillvalueplus5);
      hprespill6plus->Fill(prespillvalueplus6);
      }
    }
  }
  hprespill->Draw();
  hprespill->Fit("gaus");
  char filename[200];
  sprintf(filename,"prespill%s.png","Aside" ); 
  c1->Print(filename);  
  hprespill1plus->SetLineColor(4);
  hprespill2plus->SetLineColor(2);
  hprespill3plus->SetLineColor(4);
  hprespill4plus->SetLineColor(4);
  hprespill5plus->SetLineColor(1);
  hprespill6plus->SetLineColor(6);
  hprespill1plus->Fit("gaus");
  hprespill2plus->Fit("gaus");
  hprespill3plus->Fit("gaus");
  hprespill4plus->Fit("gaus");
  hprespill5plus->Fit("gaus");
  hprespill6plus->Fit("gaus");
  hprespill1plus->Draw();  sprintf(filename,"prespill%s_plus1ns.png","Aside");   c1->Print(filename);  
  hprespill2plus->Draw();  sprintf(filename,"prespill%s_plus2ns.png","Aside");   c1->Print(filename);  
  hprespill3plus->Draw();  sprintf(filename,"prespill%s_plus3ns.png","Aside");   c1->Print(filename);  
  hprespill4plus->Draw();  sprintf(filename,"prespill%s_plus4ns.png","Aside");   c1->Print(filename);  
  hprespill5plus->Draw();  sprintf(filename,"prespill%s_plus5ns.png","Aside");   c1->Print(filename);  
  hprespill6plus->Draw();  sprintf(filename,"prespill%s_plus6ns.png","Aside");   c1->Print(filename);  
  

}

TH1D* addHistograms(char addHistName[], TProfile *h1One, TProfile *h1Two, double cOne, double cTwo) {
  double contOne, contTwo;
  int nBinsOne =  h1One->GetXaxis()->GetNbins();
  double lowXOne =  h1One->GetXaxis()->GetXmin();
  double hiXOne = h1One->GetXaxis()->GetXmax();
  int nBinsTwo =  h1Two->GetXaxis()->GetNbins();
  double lowXTwo = h1Two->GetXaxis()->GetXmin();
  double hiXTwo = h1Two->GetXaxis()->GetXmax();
  TH1D* add = new TH1D(addHistName,addHistName,nBinsOne,lowXOne,hiXOne);
  if ((nBinsOne == nBinsTwo) && (lowXOne == lowXTwo) && (hiXOne == hiXTwo)) {
    for (int i=1; i<=nBinsOne; i++) {
      contOne = h1One->GetBinContent(i);
      contTwo = h1Two->GetBinContent(i);

      add->SetBinContent(i,cOne*contOne+cTwo*contTwo);
    }
  } else {
    cout << "addHistograms: The two histgrams " << " and " << " do not have the same binning!!! Adding cannot be performed!" << endl;
  }
  return add;
}

