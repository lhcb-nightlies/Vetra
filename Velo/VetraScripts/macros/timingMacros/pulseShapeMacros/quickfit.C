#include <iostream>
#include <fstream>
#include <vector>

TH1D *referenceplot;// = new TH1D("ref", "ref", 201, -100.5, 100.5);

double myhistofitfunction(double *x, double *par){
  //cout << "x = " << x[0] << " par[1] "  << par[1] << endl;
  double shiftedValue =  x[0] + par[1];
  int nbins = referenceplot->GetXaxis()->GetNbins();
  //int binNumber = getBinFromAxis(referenceplot, shiftedValue); // ->GetBinCenter(x+par[1]);
  int binNumber = referenceplot->FindBin(shiftedValue); // ->GetBinCenter(x+par[1]);
  //if(binNumber < 0 || binNumber > nbins ){return 0;} 
  //double result=0*par[0]*par[1]*par[2]*x;
  double result = par[0]*(referenceplot->GetBinContent(binNumber)) + par[2];
  //cout <<  "WTF? "<<  " shift = " << shiftedValue  << " bin number = " <<  binNumber <<  referenceplot->GetBinContent(binNumber) << endl;
  return result; 
}
int getBinFromAxis(TH1D *testhisto, double xval){
  int nbins = testhisto->GetXaxis()->GetNbins();
  double loweredge = testhisto->GetXaxis()->GetBinLowEdge(1);
  double upperedge = testhisto->GetXaxis()->GetBinLowEdge(nbins+1);
  double binsize = (upperedge-loweredge)/((double)nbins);
  double xbin = (xval-loweredge)/binsize;
  int binNumber =floor(xbin + 0.5);
  return binNumber; 
}
double fitsimple(double *x, double *par){
  double fitval;
  if(x[0]<par[2]) fitval = par[0]+par[1]*(exp(-((x[0]-par[2])/par[3])*((x[0]-par[2])/par[3])));
  else fitval = par[0]+par[1]*(exp(-((x[0]-par[2])/par[4])*((x[0]-par[2])/par[4])));
  return fitval;
}
void quickfit(TProfile *htobefitted){
  //gDirectory->cd("Velo");
  //gDirectory->cd("PulseShape");
//  double  m_width1=15.75;
 // double  m_width2=37.36;
  double  m_width1=13.68;
  double  m_width2=26.76;
  TF1 *func = new TF1("fit",fitsimple,-100,100,5);
  func->SetParameters(0,20,-10,m_width1,m_width2);
  //if(m_fixedOffset) func->FixParameter(0,0);
  //func->FixParameter(3, m_width1);
  //func->FixParameter(4, m_width2);
  func->SetParNames("Offset","Amplitude","Peaktime");
  htobefitted->Fit("fit");
  htobefitted->Draw();
}

TH1D* addHistograms(char addHistName[], TProfile *h1One, TProfile *h1Two, double cOne, double cTwo) {
  double contOne, contTwo;
  int nBinsOne =  h1One->GetXaxis()->GetNbins();
  double lowXOne =  h1One->GetXaxis()->GetXmin();
  double hiXOne = h1One->GetXaxis()->GetXmax();
  int nBinsTwo =  h1Two->GetXaxis()->GetNbins();
  double lowXTwo = h1Two->GetXaxis()->GetXmin();
  double hiXTwo = h1Two->GetXaxis()->GetXmax();
  TH1D* add = new TH1D(addHistName,addHistName,nBinsOne,lowXOne,hiXOne);
  if ((nBinsOne == nBinsTwo) && (lowXOne == lowXTwo) && (hiXOne == hiXTwo)) {
	for (int i=1; i<=nBinsOne; i++) {
	  contOne = h1One->GetBinContent(i);
	  contTwo = h1Two->GetBinContent(i);
	  add->SetBinContent(i,cOne*contOne+cTwo*contTwo);
	}
  } else {
	cout << "addHistograms: The two histgrams " << " and " << " do not have the same binning!!! Adding cannot be performed!" << endl;
  }
  return add;
}
