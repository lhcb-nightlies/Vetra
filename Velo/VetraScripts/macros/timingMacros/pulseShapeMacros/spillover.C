
spillover(int sensorNumber){
  TFile *file0 = TFile::Open("cmstpscan38159.root");
  TDirectory *dVelo= file0->Get("Velo");
  dVelo->cd(); 
  TDirectory *dPulseShape = dVelo->Get("PulseShape");
  dPulseShape->cd();
  TH1F *spillover = new TH1F("spillover", "spillover", 51, -0.1,1.00);
  TH1F *spillover1plus = new TH1F("spillover1plus", "spilloverplus1ns", 51, -0.1,1.00);
  TH1F *spillover2plus = new TH1F("spillover2plus", "spilloverplus2ns", 51, -0.1,1.00);
  TH1F *spillover3plus = new TH1F("spillover3plus", "spilloverplus3ns", 51, -0.1,1.00);
  TH1F *spillover4plus = new TH1F("spillover4plus", "spilloverplus4ns", 51, -0.1,1.00);
  TH1F *spillover5plus = new TH1F("spillover5plus", "spilloverplus5ns", 51, -0.1,1.00);
  TH1F *spillover6plus = new TH1F("spillover6plus", "spilloverplus6ns", 51, -0.1,1.00);
  
  int sensornumber = sensorNumber;
  for(int ii = 0; ii < 2048; ii++){
    if((ii % 32 == 4)  || (ii %32 ==23)){
    //if((ii % 32 == 23) ){
	  char name[200];
      
	  sprintf(name, "cmsTestPulseShape_Tell1_%3.3d_Even_ch_%4.4d", sensornumber, ii);
  	  TH2F *heven = (TH2F*) gDirectory->Get(name);
	  sprintf(name, "cmsTestPulseShape_Tell1_%3.3d_Odd_ch_%4.4d", sensornumber, ii);
	  TH2F *hodd = (TH2F*) gDirectory->Get(name);
      heven->FitSlicesY();
      hodd->FitSlicesY();
      if((ii % 32 == 23) ){
		sprintf(name, "cmsTestPulseShape_Tell1_%3.3d_Even_ch_%4.4d_1", sensornumber, ii);
		TH1F *heven_1 = (TH1F*) gDirectory->Get(name);
		sprintf(name, "cmsTestPulseShape_Tell1_%3.3d_Odd_ch_%4.4d_1", sensornumber, ii);
		TH1F *hodd_1 = (TH1F*) gDirectory->Get(name);
      }
	  if((ii % 32 == 4)){
		sprintf(name, "cmsTestPulseShape_Tell1_%3.3d_Even_ch_%4.4d_1", sensornumber, ii);
		TH1F *hodd_1 = (TH1F*) gDirectory->Get(name);
		sprintf(name, "cmsTestPulseShape_Tell1_%3.3d_Odd_ch_%4.4d_1", sensornumber, ii);
		TH1F *heven_1 = (TH1F*) gDirectory->Get(name);
      }
      heven_1->Add(hodd_1,-1.0);
      heven_1->GetMaximum();
      cout << "channel number =  " << ii << endl;
      cout << heven_1->GetMaximum() << endl;
      int maxbin = heven_1->GetMaximumBin();
      cout << "time of max = " << heven_1->GetMaximumBin() << "value of max = " << heven_1->GetMaximum() << endl;
      cout << "value at 6ns later " << heven_1->GetBinContent(maxbin+6) << endl;
      double spillovervalue = -5; 
      if(heven_1->GetBinContent(maxbin)) spillovervalue =  heven_1->GetBinContent(maxbin+25)/heven_1->GetBinContent(maxbin);      
	  cout << "spillover at the peak " << spillovervalue <<endl ;
      double spillovervalueplus1 = -5;
      double spillovervalueplus2 = -5;
      double spillovervalueplus3 = -5;
      double spillovervalueplus4 = -5;
      double spillovervalueplus5 = -5;
      double spillovervalueplus6 = -5;
      if(heven_1->GetBinContent(maxbin+1))  spillovervalueplus1 = heven_1->GetBinContent(maxbin+25+1)/heven_1->GetBinContent(maxbin+1); 
      if(heven_1->GetBinContent(maxbin+2))  spillovervalueplus2 = heven_1->GetBinContent(maxbin+25+2)/heven_1->GetBinContent(maxbin+2); 
      if(heven_1->GetBinContent(maxbin+3))  spillovervalueplus3 = heven_1->GetBinContent(maxbin+25+3)/heven_1->GetBinContent(maxbin+3); 
      if(heven_1->GetBinContent(maxbin+4))  spillovervalueplus4 = heven_1->GetBinContent(maxbin+25+4)/heven_1->GetBinContent(maxbin+4); 
      if(heven_1->GetBinContent(maxbin+5))  spillovervalueplus5 = heven_1->GetBinContent(maxbin+25+5)/heven_1->GetBinContent(maxbin+5); 
      if(heven_1->GetBinContent(maxbin+6))  spillovervalueplus6 = heven_1->GetBinContent(maxbin+25+6)/heven_1->GetBinContent(maxbin+6); 
      cout << "spillover at the peak +5ns" << spillovervalueplus5 <<endl ;
      spillover->Fill(spillovervalue);
      spillover1plus->Fill(spillovervalueplus1);
      spillover2plus->Fill(spillovervalueplus2);
      spillover3plus->Fill(spillovervalueplus3);
      spillover4plus->Fill(spillovervalueplus4);
      spillover5plus->Fill(spillovervalueplus5);
      spillover6plus->Fill(spillovervalueplus6);
    }
  }
  spillover->Draw();
  spillover->Fit("gaus");
  char filename[200];
  sprintf(filename,"spillover%d.png",sensornumber); 
  c1->Print(filename);  
  spillover1plus->SetLineColor(4);
  spillover2plus->SetLineColor(2);
  spillover3plus->SetLineColor(4);
  spillover4plus->SetLineColor(4);
  spillover5plus->SetLineColor(1);
  spillover6plus->SetLineColor(6);
  spillover1plus->Fit("gaus");
  spillover2plus->Fit("gaus");
  spillover3plus->Fit("gaus");
  spillover4plus->Fit("gaus");
  spillover5plus->Fit("gaus");
  spillover6plus->Fit("gaus");
  spillover1plus->Draw();  sprintf(filename,"spillover%d_plus1ns.png",sensornumber); 
  c1->Print(filename);  
  spillover2plus->Draw();  sprintf(filename,"spillover%d_plus2ns.png",sensornumber); 
  c1->Print(filename);  
  spillover3plus->Draw();  sprintf(filename,"spillover%d_plus3ns.png",sensornumber); 
  c1->Print(filename);  
  spillover4plus->Draw();  sprintf(filename,"spillover%d_plus4ns.png",sensornumber); 
  c1->Print(filename);  
  spillover5plus->Draw();  sprintf(filename,"spillover%d_plus5ns.png",sensornumber); 
  c1->Print(filename);  
  spillover6plus->Draw();  sprintf(filename,"spillover%d_plus6ns.png",sensornumber); 
  c1->Print(filename);  
  

}
