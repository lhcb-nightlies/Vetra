#include <map.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <math.h>
#include <unistd.h>

using namespace std;


int splitTestPulseShape(char* filename, bool plot, bool varwidth=false, 
			 double width1=15.71,double width2=23.25,int firstsensor=0, int lastsensor=200){
  gROOT->SetStyle("Plain");
   
  gStyle->SetOptStat(10);
  gStyle->SetOptFit(1);
  TH1D *peakhist25 = new TH1D("Peak_hist25","Peak : 25 finesteps",801,-100.125,100.125);
  TH1D *peakhist12 = new TH1D("Peak_hist12","Peak : 12 finesteps",801,-100.125,100.125);
  TH1D *peakhist6 = new TH1D("Peak_hist6","Peak : 6 finesteps",801,-100.125,100.125);
  TH1D *peakhist4 = new TH1D("Peak_hist4","Peak : 4 finesteps",801,-100.125,100.125);
  TH1D *peakhist2 = new TH1D("Peak_hist2","Peak : 2 finesteps",801,-100.125,100.125);
  TH1D *peakhist1 = new TH1D("Peak_hist1","Peak : 1 finesteps",801,-100.125,100.125);
  TH1D *maximumhist25 = new TH1D("Maximum_hist25","Maximum - Peak : 25 finesteps",21,-10.5,10.5);
  TH1D *maximumhist12 = new TH1D("Maximum_hist12","Maximum - Peak : 12 finesteps",21,-10.5,10.5);
  TH1D *maximumhist6 = new TH1D("Maximum_hist6","Maximum - Peak : 6 finesteps",21,-10.5,10.5);
  TH1D *maximumhist4 = new TH1D("Maximum_hist3","Maximum - Peak : 4 finesteps",21,-10.5,10.5);
  TH1D *maximumhist2 = new TH1D("Maximum_hist2","Maximum - Peak : 2 finesteps",21,-10.5,10.5);
  TH1D *maximumhist1 = new TH1D("Maximum_hist1","Maximum - Peak : 1 finesteps",21,-10.5,10.5);

  TCanvas *c1= new TCanvas("Fit_results","Fit_results",1200,800);

  c1->Divide(3,2);

  const int sensorlistlength=lastsensor-firstsensor+1;
  int activesensorlength[4]={0,0,0,0};
  int sensorlist[4][sensorlistlength/4];
  bool statuslist[4][sensorlistlength/4];
  double dsensorlist[4][sensorlistlength/4];
  double delaylist[4][sensorlistlength/4];
  double delaylisterror[4][sensorlistlength/4];
  double noerror[sensorlistlength];

  
  TF1 *func = new TF1("fit",fitsimple,-200,200,5);
  func->SetParameters(0,20,-10,width1,width2);
  //if(m_fixedOffset) func->FixParameter(0,0);
  // func->FixParameter(3, width1);
  //func->FixParameter(4, width2);
  //func->SetParNames("Offset","Amplitude","Peaktime");
  

  TFile *input = new TFile(filename);
  input->cd();

  TProfile* profile25;
  TProfile* profile12;
  TProfile* profile6;
  TProfile* profile4;
  TProfile* profile2;
  TProfile* profile1;

  TF1* gaussian1 = new TF1("gaus1","gaus",-200,200);
  TF1* gaussian2 = new TF1("gaus2","gaus",-200,200);


  char graphnameweighted[100] ;
  char graphname[100] ;
  char profilename[100] ;
  char profilenameFitted[100] ;

  char peaktimeText[6][100];
  double peaktime[6];
  double peaktimeSigma[6]; 
  int statuscolor[6];
  //  double profilepeak,profilepeakSigma,profilepeakFitted,profilepeakSigmaFitted;

  int deviationMesurement;
  int sector;
  int secsensor;
  double deviationMax;
  bool activelines=true;
  double copywidth1[6],copywidth2[6];

  for(int sensor=firstsensor; sensor<=lastsensor;sensor++){

    if(sensor<64) sector=sensor%4;
    else sector=(sensor+2)%4;
    secsensor=(sensor-firstsensor)/4;
    statuslist[sector][secsensor]=false;

    sprintf(profilename,"Velo/PulseShape/ClustersVsTime_%dprof",sensor);
    profile25=(TProfile*) input->Get(profilename);
    if(profile25==0){
      sensorlist[sector][secsensor]=-100;
      dsensorlist[sector][secsensor]=-100;
      delaylist[sector][secsensor]=0;  
      delaylisterror[sector][secsensor]=0;
      noerror[sensor]=0;
      continue;
    }
    profile25->GetXaxis()->SetTitle("Sample time (ns)");
    profile25->GetYaxis()->SetTitle("ADC counts");

    //profile12=(TProfile*) input->Get(profilename);
    //profile6=(TProfile*) input->Get(profilename);
    //profile4=(TProfile*) input->Get(profilename);
    profile12=(TProfile*) profile25->Clone();
    profile6=(TProfile*) profile25->Clone();
    profile4=(TProfile*) profile25->Clone();
    profile2=(TProfile*) profile25->Clone();
    profile1=(TProfile*) profile25->Clone();

    for(int ibin=0;ibin<profile25->GetNbinsX();ibin++){
      if(ibin%4!=0&&ibin%4!=1){
	profile12->SetBinContent(ibin,0);
	profile12->SetBinEntries(ibin,0);
      }
      
      if(ibin%8!=0&&ibin%8!=1){
	profile6->SetBinContent(ibin,0);
	profile6->SetBinEntries(ibin,0);
      }
      if(ibin%12!=0&&ibin%12!=1){
	profile4->SetBinContent(ibin,0);
	profile4->SetBinEntries(ibin,0);
      }
      if(ibin%24!=0&&ibin%24!=1){
	profile2->SetBinContent(ibin,0);
	profile2->SetBinEntries(ibin,0);
      }
       if(ibin%50!=0&&ibin%50!=1){
	profile1->SetBinContent(ibin,0);
	profile1->SetBinEntries(ibin,0);
      }     
    }
    
    c1->cd(1);
    //profile25->Draw();
    profile25->SetTitle("25 finesteps");
    quickfit1(profile25, peaktime[0], peaktimeSigma[0], copywidth1[0], copywidth2[0], width1, width2, varwidth);
    c1->cd(2);
    //profile12->Draw();
    profile12->SetTitle("12 finesteps");
    quickfit2(profile12, peaktime[1], peaktimeSigma[1], copywidth1[1], copywidth2[1], width1, width2, varwidth);
    c1->cd(3);
    //profile25->Draw();
    profile6->SetTitle("6 finesteps");
    quickfit3(profile6, peaktime[2], peaktimeSigma[2], copywidth1[2], copywidth2[2], width1, width2, varwidth);
    c1->cd(4);
    //profile25->Draw();
    profile4->SetTitle("4 finesteps");
    quickfit4(profile4, peaktime[3], peaktimeSigma[3], copywidth1[3], copywidth2[3], width1, width2, varwidth);
    c1->cd(5);
    //profile25->Draw();
    profile2->SetTitle("2 finesteps");
    quickfit5(profile2, peaktime[4], peaktimeSigma[4], copywidth1[4], copywidth2[4], width1, width2, varwidth);
    c1->cd(6);
    //profile25->Draw();
    profile1->SetTitle("1 finesteps");
    quickfit6(profile1, peaktime[5], peaktimeSigma[5], copywidth1[5], copywidth2[5], width1, width2, varwidth);
    if(plot){
      c1->Update();
      sleep(1);
    }
    //if(sensor==12) return 0;
    peakhist25->Fill(peaktime[0]);
    peakhist12->Fill(peaktime[1]);
    peakhist6->Fill(peaktime[2]);
    peakhist4->Fill(peaktime[3]);
    peakhist2->Fill(peaktime[4]);
    peakhist1->Fill(peaktime[5]);
    maximumhist25->Fill(profile25->GetBinCenter(profile25->GetMaximumBin())-peaktime[0]);
    maximumhist12->Fill(profile25->GetBinCenter(profile25->GetMaximumBin())-peaktime[1]);
    maximumhist6->Fill(profile25->GetBinCenter(profile25->GetMaximumBin())-peaktime[2]);
    maximumhist4->Fill(profile25->GetBinCenter(profile25->GetMaximumBin())-peaktime[3]);
    maximumhist2->Fill(profile25->GetBinCenter(profile25->GetMaximumBin())-peaktime[4]);
    maximumhist1->Fill(profile25->GetBinCenter(profile25->GetMaximumBin())-peaktime[5]);

    cout<<"Sensor "<<setw(3)<<sensor<<"  "<<peaktime[0]<<"  "<<peaktime[1]<<"  ";
    cout<<peaktime[2]<<"  "<<peaktime[3]<<"  "<<profile25->GetBinCenter(profile25->GetMaximumBin())<<endl;
 
    //if(sensor==23) c1->Print("example23.png");
    //if(sensor==102) c1->Print("example102.png");
    //    peakhist->Fill(peaktime[5]);
    //    maximumhist->Fill(profile->GetBinCenter(profile->GetMaximumBin())-peaktime[2]);
    //    width1hist->Fill(copywidth1);
    //    width2hist->Fill(copywidth2);

    statuslist[sector][secsensor]=true;
    profile25->Reset();
    profile12->Reset();
    profile6->Reset();
    profile4->Reset();
    profile2->Reset();
    profile1->Reset();

  }
  //delete profile25;
  //delete profile12;
  //delete profile6;
  //delete profile4;
  //delete gaussian1;
  //delete gaussian2;
  //delete c1;
  input->Close();

  gStyle->SetOptStat(1);
  TCanvas *c2= new TCanvas("Fit_results2","Fit_results 2",1200,600);
  c2->Divide(3,2);
  c2->cd(1);
  peakhist25->Fit("gaus1","N");
  peakhist25->DrawCopy();
  gaussian1->DrawCopy("same");
  c2->cd(2);
  peakhist12->Fit("gaus1","N");
  peakhist12->DrawCopy();
  gaussian1->DrawCopy("same");
  c2->cd(3);
  peakhist6->Fit("gaus1","N");
  peakhist6->DrawCopy();
  gaussian1->DrawCopy("same");
  c2->cd(4);
  peakhist4->Fit("gaus1","N");
  peakhist4->DrawCopy();
  gaussian1->DrawCopy("same");
  c2->cd(5);
  peakhist2->Fit("gaus1","N");
  peakhist2->DrawCopy();
  gaussian1->DrawCopy("same");
  c2->cd(6);
  peakhist1->Fit("gaus1","N");
  peakhist1->DrawCopy();
  gaussian1->DrawCopy("same");

  c2->Update();
  TCanvas *c3= new TCanvas("Maximum","Maximum - Fitted Peak",1);
  c3->Divide(3,2);
  c3->cd(1);
  maximumhist25->Draw();
  c3->cd(2);
  maximumhist12->Draw();
  c3->cd(3);
  maximumhist6->Draw();
  c3->cd(4);
  maximumhist4->Draw();
  c3->cd(5);
  maximumhist2->Draw();
  c3->cd(6);
  maximumhist1->Draw();

  //delete gr1;
  // delete gr2;
  //delete gr3;
  //delete gr4;

  return 0;
}

double fitsimple(double *x, double *par){
  double fitval;
  if(x[0]<par[2]) fitval = par[0]+par[1]*(exp(-((x[0]-par[2])/par[3])*((x[0]-par[2])/par[3])));
  else fitval = par[0]+par[1]*(exp(-((x[0]-par[2])/par[4])*((x[0]-par[2])/par[4])));
  return fitval;
}
void  quickfit1(TProfile *htobefitted, 
		double &peak, double &error, double &width1, double &width2,
		double inwidth1, double inwidth2, bool varwidth){
  TF1 *func = new TF1("fit",fitsimple,-200,200,5);
  func->SetParameters(0,20,-10,inwidth1,inwidth2);
  //if(m_fixedOffset) func->FixParameter(0,0);
  if(!varwidth){
    func->FixParameter(3, inwidth1);
    func->FixParameter(4, inwidth2);
  }else{
    func->SetParLimits(3,0.001,1000);
    func->SetParLimits(4,0.001,1000);
  }
  func->SetParNames("Offset","Amplitude","Peaktime","Rampup","Rampdown");
  htobefitted->Fit("fit","QW");    
  peak = func->GetParameter(2);
  error = func->GetParError(2);
  width1 = func->GetParameter(3);
  width2 = func->GetParameter(4);

}

void  quickfit2(TProfile *htobefitted, 
		double &peak, double &error, double &width1, double &width2,
		double inwidth1, double inwidth2, bool varwidth){
  TF1 *func = new TF1("fit",fitsimple,-200,200,5);
  func->SetParameters(0,20,-10,inwidth1,inwidth2);
  //if(m_fixedOffset) func->FixParameter(0,0);
  if(!varwidth){
    func->FixParameter(3, inwidth1);
    func->FixParameter(4, inwidth2);
  }else{
    func->SetParLimits(3,0.001,1000);
    func->SetParLimits(4,0.001,1000);
  }
  func->SetParNames("Offset","Amplitude","Peaktime","Rampup","Rampdown");
  htobefitted->Fit("fit","QW");    
  peak = func->GetParameter(2);
  error = func->GetParError(2);
  width1 = func->GetParameter(3);
  width2 = func->GetParameter(4);

}

void  quickfit3(TProfile *htobefitted, 
		double &peak, double &error, double &width1, double &width2,
		double inwidth1, double inwidth2, bool varwidth){
  TF1 *func = new TF1("fit",fitsimple,-200,200,5);
  func->SetParameters(0,20,-10,inwidth1,inwidth2);
  //if(m_fixedOffset) func->FixParameter(0,0);
  if(!varwidth){
    func->FixParameter(3, inwidth1);
    func->FixParameter(4, inwidth2);
  }else{
    func->SetParLimits(3,0.001,1000);
    func->SetParLimits(4,0.001,1000);
  }
  func->SetParNames("Offset","Amplitude","Peaktime","Rampup","Rampdown");
  htobefitted->Fit("fit","QW");    
  peak = func->GetParameter(2);
  error = func->GetParError(2);
  width1 = func->GetParameter(3);
  width2 = func->GetParameter(4);

}

void  quickfit4(TProfile *htobefitted, 
		double &peak, double &error, double &width1, double &width2,
		double inwidth1, double inwidth2, bool varwidth){
  TF1 *func = new TF1("fit",fitsimple,-200,200,5);
  func->SetParameters(0,20,-10,inwidth1,inwidth2);
  //if(m_fixedOffset) func->FixParameter(0,0);
  if(!varwidth){
    func->FixParameter(3, inwidth1);
    func->FixParameter(4, inwidth2);
  }else{
    func->SetParLimits(3,0.001,1000);
    func->SetParLimits(4,0.001,1000);
  }
  func->SetParNames("Offset","Amplitude","Peaktime","Rampup","Rampdown");
  htobefitted->Fit("fit","QW");    
  peak = func->GetParameter(2);
  error = func->GetParError(2);
  width1 = func->GetParameter(3);
  width2 = func->GetParameter(4);


}


void  quickfit5(TProfile *htobefitted, 
		double &peak, double &error, double &width1, double &width2,
		double inwidth1, double inwidth2, bool varwidth){
  TF1 *func = new TF1("fit",fitsimple,-200,200,5);
  func->SetParameters(0,20,-10,inwidth1,inwidth2);
  //if(m_fixedOffset) func->FixParameter(0,0);
  if(!varwidth){
    func->FixParameter(3, inwidth1);
    func->FixParameter(4, inwidth2);
  }else{
    func->SetParLimits(3,0.001,1000);
    func->SetParLimits(4,0.001,1000);
  }
  func->SetParNames("Offset","Amplitude","Peaktime","Rampup","Rampdown");
  htobefitted->Fit("fit","QW");    
  peak = func->GetParameter(2);
  error = func->GetParError(2);
  width1 = func->GetParameter(3);
  width2 = func->GetParameter(4);


}
void  quickfit6(TProfile *htobefitted, 
		double &peak, double &error, double &width1, double &width2,
		double inwidth1, double inwidth2, bool varwidth){
  TF1 *func = new TF1("fit",fitsimple,-200,200,5);
  func->SetParameters(0,20,-10,inwidth1,inwidth2);
  //if(m_fixedOffset) func->FixParameter(0,0);
  if(!varwidth){
    func->FixParameter(3, inwidth1);
    func->FixParameter(4, inwidth2);
  }else{
    func->SetParLimits(3,0.001,1000);
    func->SetParLimits(4,0.001,1000);
  }
  func->SetParNames("Offset","Amplitude","Peaktime","Rampup","Rampdown");
  htobefitted->Fit("fit","QW");    
  peak = func->GetParameter(2);
  error = func->GetParError(2);
  width1 = func->GetParameter(3);
  width2 = func->GetParameter(4);


}