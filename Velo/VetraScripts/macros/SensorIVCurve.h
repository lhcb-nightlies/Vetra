//---------------------------------------------------------------------------//
//                                                                           //
// Header file for the "SensorIVCurve" Class                                 //
// =========================================                                 //
//                                                                           //
// Contains the mapping between the HV channels and the slot numbers.        //
//                                                                           //
//---------------------------------------------------------------------------//

#ifndef SENSORIVCURVE_H
#define SENSORIVCURVE_H 1

#include <TROOT.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>
#include <string>
using namespace std;

class SensorIVCurve {
  
 public:
  SensorIVCurve(string fileName, string refName, bool debug);
  virtual ~SensorIVCurve();
  void     draw(string tag, string opt, TCanvas* canvas);
  void     setSlot(string slotNum, string hvChan);
  string   getSlot();
  string   getHVChan();
  Float_t  getNTC1Mean();
  Float_t  getNTC2Mean();
  Float_t  getNTC1RefMean();
  Float_t  getNTC2RefMean();
  Float_t  getNTC1RMS();
  Float_t  getNTC2RMS();
  Float_t  getNTC1RefRMS();
  Float_t  getNTC2RefRMS();
  void     usage();
  
 private:
  bool   m_debug;
  void   setGraphOptions(TGraph *graph, Int_t color, Int_t marker);
  void   setStyleOptions(TCanvas *canvas);
  Int_t  getIVGraphs();

  string  m_fileName;
  string  m_refName;
  FILE   *m_inFile;
  FILE   *m_refFile;
  TH2F   *m_blankHisto;

  TGraph *m_ivUp;
  TGraph *m_ivDown;
  TGraph *m_ivRef;
  TH1F   *m_hNTC1;
  TH1F   *m_hNTC2;
  TH1F   *m_hNTC1Ref;
  TH1F   *m_hNTC2Ref;
  Float_t m_vMax;
  Float_t m_iMax;
  string  m_hvChan;
  string  m_slotName;
};

#endif //SENSORIVCURVE_H
