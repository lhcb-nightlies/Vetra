//---------------------------------------------------------------------------//
//                                                                           //
//  drawRMSNoiseVsLinks                                                      //
//  ===================                                                      //
//                                                                           //
//                                                                           //
//  Example usage:                                                           //
//  - in root, run with                                                      //
//    .L drawRMSNoiseVsLinks.C                                               //
//    drawRMSNoiseVsLinks( "myfile.root" )                                   //
//                                                                           //
//  @author Torkjell Huse                                                    //
//  @date   2008-10-30                                                       //
//                                                                           //
//---------------------------------------------------------------------------//

#include <TCanvas.h>
#include <TFile.h>
#include <TROOT.h>
#include <TH1F.h>

#include "../src/VeloSensor.h"

class RMSNoiseVsLinks {

public:
  RMSNoiseVsLinks( std::string fileName );
  virtual ~RMSNoiseVsLinks();
  void draw( TCanvas* canvas );

private:
  TFile *m_file;

};

RMSNoiseVsLinks::RMSNoiseVsLinks( std::string fileName ) {

  m_file = new TFile( fileName.c_str() );

}

RMSNoiseVsLinks::~RMSNoiseVsLinks() {
}

void RMSNoiseVsLinks::draw( TCanvas* canvas ) {
   
  gROOT->SetStyle("Plain");

  canvas -> cd();

  int divideCanX=3;
  int divideCanY=2;
  int plots_per_canvas=divideCanX*divideCanY;
  
  int first_tell=0;
  int last_tell=10;

  char name[100];
  char title[100];
  char can_name[100];
  
  VeloSensor sen;
  bool CMS=0; //set =0/1 for before/after CMS
  int first_ch=0; //set =1, 2, 3... to exclude the first, first two, first 3 channels etc.
  double rms_threshold=10; //excluding channels with noise outside 10 rms of link dist
  double absolute_threshold=100; //exclude channels with noise over 100 ADC's

  int tell1s=0;
  
  TCanvas *can = 0;
  for(int tell=first_tell;tell<last_tell;tell++){
    sprintf(name,"RMSNoiseVsLink%03d",tell);
    sprintf(title,"RMS noise vs link, sensor %d", tell);
    TH1F *RMSNoise_vs_link=new TH1F(name,title,64,-0.5,63.5);
    if(sen.initializeSensor(m_file,tell)){
	for(int link=0;link<64;link++){
	  double noise_link=sen.getLinkNoise(link, CMS, first_ch, rms_threshold, absolute_threshold);
	  RMSNoise_vs_link->Fill(link,noise_link);
	}
	if((tell1s%plots_per_canvas)==0){
	  sprintf(can_name,"canvas%d",tell1s/plots_per_canvas);
	  can=new TCanvas(can_name,"RMS noise vs link",1);
	  can->Divide(divideCanX,divideCanY);      
	}
	can->cd(tell1s%plots_per_canvas+1);
	RMSNoise_vs_link->SetStats(kFALSE);
	RMSNoise_vs_link->GetXaxis()->SetTitle("Link (0-63)");
	RMSNoise_vs_link->GetYaxis()->SetTitle("RMS noise [ADC]");
	RMSNoise_vs_link->GetYaxis()->SetRangeUser(0,5);
	RMSNoise_vs_link->Draw();
	tell1s++;
      }
  }    
  
}

void drawRMSNoiseVsLinks( std::string fileName, TCanvas* canvas=0 ) 
{
  RMSNoiseVsLinks *plot = new RMSNoiseVsLinks( fileName );
  if ( canvas == 0 )
    TCanvas* canvas = new TCanvas();
  plot   -> draw( canvas );
  canvas -> Modified();
  canvas -> Update();
}

//=============================================================================
