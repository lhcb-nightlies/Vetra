//---------------------------------------------------------------------------//
//                                                                           //
//  compareNoiseBefAftCMS macro                                              //
//  ===========================                                              //
//                                                                           //
//  Plots all the noise histograms before and after                          //
//  Common Mode Suppression (CMS).                                           //
//  By default six plots are displayed in each canvas, but this is           //
//  easy to change in the macro.                                             //
//                                                                           //
//  Requirements:                                                            //
//  - needs input file with Non-Zero-Suppressed (NZS) data                   //
//    containing the directory structure                                     //
//    "Vetra/Noise/ADCCMSuppressed/TELL1_NNN/"                               //
//    and                                                                    //
//    "Vetra/Noise/DecodedADC/TELL1_NNN/"                                    //
//    with histograms RMSNoise_vs_ChipChannel and RMSNoise_vs_Strip.         //
//                                                                           //
//  Example usage:                                                           //
//  - in root, execute with                                                  //
//    .x compareNoiseBefAftCMS.C                                             //
//                                                                           //
//  @author Torkjell Huse                                                    //
//  @date   2008-10-30                                                       //
//                                                                           //
//---------------------------------------------------------------------------//

#include <iostream>
#include <cstdio>

#include <TFile.h>
#include <TROOT.h>
#include <TH1F.h>
#include <TCanvas.h>

void compareNoiseBefAftCMS( const char* filename )
{
  gROOT->SetStyle("Plain");
  TFile f(filename);
  char path[100];
  int channel;
  int tell1s=0;
  char can_name[100];
  
  for(int tell=0;tell<106;tell++){
    sprintf(path,"Vetra/Noise/ADCCMSuppressed/TELL1_%03d",tell);
    //std::cout<<"check if path exists"<<std::endl;
    TCanvas *can = 0;
    if(f.cd(path)){
      std::cout<<path<<std::endl;
      TH1F *CMS_noise=(TH1F*)gDirectory->Get("RMSNoise_vs_ChipChannel");
      sprintf(path,"Vetra/Noise/DecodedADC/TELL1_%03d",tell);
      f.cd(path);
      TH1F *ADC_noise=(TH1F*)gDirectory->Get("RMSNoise_vs_ChipChannel");
      if((tell1s%6)==0){
        sprintf(can_name,"canvas%d",tell1s/6);
        //std::cout<<"creating canvas "<<can_name<<std::endl;
        can=new TCanvas(can_name,"RMS noise vs chip channel",1);
        can->Divide(3,2);      
      }
      can->cd(tell1s%6+1);
      ADC_noise->SetLineColor(kRed);
      ADC_noise->SetStats(kFALSE);
      ADC_noise->GetYaxis()->SetRangeUser(0,5);
      CMS_noise->SetLineColor(kBlue);
      ADC_noise->Draw("hist");
      CMS_noise->Draw("histSAME");
      tell1s++;
      can->Modified();
      can->Update();
    }

    else{
      printf("TELL1 %03d does not exist.... \n",tell);
    }

  }


}

//=============================================================================
