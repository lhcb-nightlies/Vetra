//---------------------------------------------------------------------------//
//                                                                           //
//  drawIPResolutionsHistos                                                  //
//  ========================                                                 //
//                                                                           //
//  Draws the IP resolution profiles produced by VeloIPResolutionMonitor     //
//                                                                           //
//  Requirements:                                                            //
//  - needs input file made by VeloIPResolutionMonitor in the                //
//    Velo/VeloRecMonitors package, containing the directory                 //
//    "Velo/VeloIPResolutionMonitor", with the histograms                    //
//    "GaussWidth_IP_X_Vs_InversePT",                                        //
//    "GaussWidth_IP_Y_Vs_InversePT",                                        //
//    "GaussWidth_IP_Z_Vs_InversePT",                                        //
//    "Mean_unsigned3DIP_Vs_InversePT",                                      //
//    "NTracks_Vs_InversePT", and                                            //
//    "TrackMultiplicity".                                                   //
//                                                                           //
//  Example usage:                                                           //
//  - in ROOT, run with                                                      //
//    .L drawIPResolutionsHistos.C                                           //
//    drawIPResolutionsHistos( "myfile.root" )                               //
//    to draw IP resolutions profiles                                        //
//    or                                                                     //
//    drawIPResolutionsHistos( "myfile.root", 2 )                            //
//    to draw 1/PT and PV track multiplicity frequency plots                 //
//    or                                                                     //
//    drawIPResolutionsHistos( "myfile.root", int drawOption, 0, true )      //
//    where drawOption = 1 or 2, to run in debug mode                        //
//                                                                           //
//  @author Michael Alexander                                                //
//  @date   2009-07-10                                                       //
//                                                                           //
//---------------------------------------------------------------------------//


// STL ncludes 
#include <vector>
#include <string>
#include <sstream>

// ROOT includes
#include <TROOT.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TF1.h>
#include <TPaveText.h>

// Includes from Velo/VetraScripts
#include "../src/helperTools.h"



class IPResolutionsHistos 
{
public:
  IPResolutionsHistos( std::string fileName,
                       bool debug,
                       helperTools::MoniHistosPaths* p=NULL,
                       bool isRef=false );
  virtual ~IPResolutionsHistos();
  void draw( TCanvas* canvas, int drawType);
  
private:

  void setHistosPath( helperTools::MoniHistosPaths* p );
  void setROOTLooks() const ;
  void setHistoStyleAndDraw( TH1D* h, std::string, std::string, double, double ) const ;
  TPaveText* fitResult( TF1*, TH1D* );
  
  TFile* m_file;
  bool   m_debug;
  std::string m_pathVeloIPResolutionMonitor;

  TH1D *m_ipxVsInversePT, *m_ipyVsInversePT, *m_ipxVsPhi, *m_ipxVsEta ;
  TH1D *m_ipxVsPhi_mean, *m_ipyVsPhi_mean, *m_ipxVsEta_mean, *m_ipyVsEta_mean ;
  bool m_isRef ;
  
  TH1D* getHisto( std::string ) const ;
  void plotNotFound(std::string) const ;
  
};

//-----------------------------------------------------------------------------
// Implementation file for class : drawIPResolutionsHistos
//
// 2009-07-09 : Michael Thomas Alexander
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
IPResolutionsHistos::IPResolutionsHistos( std::string fileName,
                                          bool debug,
                                          helperTools::MoniHistosPaths* p,
                                          bool isRef ):
  m_file(0),
  m_debug( debug ),
  m_pathVeloIPResolutionMonitor(),
  m_ipxVsInversePT(0), 
  m_ipyVsInversePT(0), 
  m_ipxVsPhi(0), 
  m_ipxVsEta(0),
  m_ipxVsPhi_mean(0), 
  m_ipyVsPhi_mean(0), 
  m_ipxVsEta_mean(0), 
  m_ipyVsEta_mean(0),
  m_isRef(isRef)
{
  // set the ROOT looks for the histograms
  setROOTLooks();
  
  // get the paths to the histograms via the helperTools macro
  if ( p == NULL ) p = new helperTools::MoniHistosPaths(); // gets the defaults
  setHistosPath( p );

  // either look at the opened file or open it!
  m_file = helperTools::getOrOpen( fileName );  
  if ( m_file -> IsZombie() )
    printf( " ==> Zombie file !\n" );
  else{
      Bool_t test = m_file->cd( m_pathVeloIPResolutionMonitor.c_str() );
      if( !test )
        printf( "Cannot find histo directory ==> Nothing drawn!\n" );
      else{
        m_ipxVsInversePT = getHisto("IPX-Vs-InversePT-LongTracks-Sigma");
        m_ipyVsInversePT = getHisto("IPY-Vs-InversePT-LongTracks-Sigma");
        m_ipxVsPhi = getHisto("IPX-Vs-Phi-LongTracks-Sigma") ;
        m_ipxVsEta = getHisto("IPX-Vs-Eta-LongTracks-Sigma") ;
        m_ipxVsPhi_mean = getHisto("IPX-Vs-Phi-LongTracks-Mean");
        m_ipyVsPhi_mean = getHisto("IPY-Vs-Phi-LongTracks-Mean");
        m_ipxVsEta_mean = getHisto("IPX-Vs-Eta-LongTracks-Mean");
        m_ipyVsEta_mean = getHisto("IPY-Vs-Eta-LongTracks-Mean");
      }
  
  }
  
}

//=============================================================================
// Destructor
//=============================================================================
IPResolutionsHistos::~IPResolutionsHistos() {
  if(0 != m_ipxVsInversePT)
    delete m_ipxVsInversePT ;
  if(0 != m_ipyVsInversePT)
    delete m_ipyVsInversePT ;
  if (0 != m_ipxVsPhi)
    delete m_ipxVsPhi ;
  if(0 != m_ipxVsEta)
    delete m_ipxVsEta ;
  if(0 != m_ipxVsPhi_mean)
    delete m_ipxVsPhi_mean ;
  if(0 != m_ipyVsPhi_mean)
    delete m_ipyVsPhi_mean ; 
  if(0 != m_ipxVsEta_mean)
    delete m_ipxVsEta_mean ;
  if(0 != m_ipyVsEta_mean)
    delete m_ipyVsEta_mean ;

} 

//=============================================================================
// Get a histo from the current working directory.
//=============================================================================
TH1D* IPResolutionsHistos::getHisto( std::string name ) const 
{
  if( m_debug ) printf( "Getting histo \"%s\"\n", name.c_str() );
  TH1D* hc(0) ;
  TH1D* h =  (TH1D*)gDirectory->Get(name.c_str());
  if( m_debug && 0 == h ) printf( "Histo not found!\n" );
  if( 0 != h ){
    hc = new TH1D(*h) ;
    hc->SetTitleSize(0.08) ;
    hc->SetStats( kFALSE );
    hc->GetYaxis()->CenterTitle();
    hc->Scale(1000) ;
    hc->GetYaxis()->SetTitle("#mum") ;
    hc->GetYaxis()->SetTitleSize(0.07) ;
    hc->GetYaxis()->SetTitleOffset(0.7) ;
    hc->GetXaxis()->SetTitleSize(0.06) ;
    hc->GetXaxis()->SetTitleOffset(0.75) ;
    if(m_isRef){
      hc->SetLineColor(2) ;
      hc->SetMarkerStyle(24) ;
      hc->SetMarkerColor(24) ;
    }
    
  }

  return hc ;
}

//=============================================================================
// Draw a message if the plot is not found.
//=============================================================================
void IPResolutionsHistos::plotNotFound(std::string name) const
{
  if(!m_isRef){
    TPaveText* msg = new TPaveText(0.1, 0.35, 0.9, 0.65, "ndc") ;
    msg->SetFillStyle(0) ;
    std::ostringstream notFoundMsg ;
    notFoundMsg << "Histo \"" << name << "\" not found!" ;
    msg->AddText(notFoundMsg.str().c_str()) ;
    msg->AddText("IP monitoring is not run by default in vetraOffline.") ;
    msg->AddText("To run it do: vetraOffline -u $VETRASCRIPTSROOT/python/addIPMoni.py ...") ;
    msg->Draw() ;
  }
}

//=============================================================================
// Class main function. Does the drawing
//=============================================================================
void IPResolutionsHistos::draw( TCanvas* canvas, int drawType )
{
  if ( m_file -> IsZombie() ) {
    printf( "Zombie file ==> Nothing drawn !\n" );
    return;
  }

  canvas->cd();
  if(!m_isRef){
    canvas->Clear();
    canvas->Divide(2,2);
  }
  
  if( drawType == 1 ){

    double yMaxVsInversePT(100) ;
    double yMaxVsEtaAndPhi(120) ;
    
    canvas->cd(1);
    if(0 != m_ipxVsInversePT){
      m_ipxVsInversePT->SetStats(true) ;
      if(!m_isRef)
        m_ipxVsInversePT->Fit("pol1", "QO") ;
      else{
        m_ipxVsInversePT->Fit("pol1", "QO", "same") ;
        if( m_ipxVsInversePT->GetFunction("pol1") != 0 )
          m_ipxVsInversePT->GetFunction("pol1")->SetLineColor(2) ;
      }
      setHistoStyleAndDraw(m_ipxVsInversePT, "IP_{x} Resolution vs 1/p_{T}", "1/p_{T} [GeV/c]", 
                           0., yMaxVsInversePT) ;

    }
    else
      plotNotFound("IPX-Vs-InversePT-LongTracks-Sigma") ;
    
    canvas->cd(2);
    if(0 != m_ipyVsInversePT){
      m_ipyVsInversePT->SetStats(true) ;
      if(!m_isRef)
        m_ipyVsInversePT->Fit("pol1", "QO") ;
      else{
        m_ipyVsInversePT->Fit("pol1", "QO","same") ;
        if( m_ipyVsInversePT->GetFunction("pol1") != 0 )
          m_ipyVsInversePT->GetFunction("pol1")->SetLineColor(2) ;
      }
      
      setHistoStyleAndDraw(m_ipyVsInversePT, "IP_{y} Resolution vs 1/p_{T}", "1/p_{T} [GeV/c]", 
                           0., yMaxVsInversePT) ;
    }
    else
      plotNotFound("IPY-Vs-InversePT-LongTracks-Sigma") ;

    canvas->cd(3);
    if(0 != m_ipxVsPhi)
      setHistoStyleAndDraw(m_ipxVsPhi, "IP_{x} Resolution vs #phi", "#phi [rad]", 
                           0., yMaxVsEtaAndPhi) ;
    else
      plotNotFound("IPX-Vs-Phi-LongTracks-Sigma") ;
    
    canvas->cd(4);
    if(0 != m_ipxVsEta)
      setHistoStyleAndDraw(m_ipxVsEta, "IP_{x} Resolution vs #eta", "#eta", 0., yMaxVsEtaAndPhi) ;
    else 
      plotNotFound("IPX-Vs-Eta-LongTracks-Sigma") ;
    
  }
  else if( drawType == 2 ){

    double yMax = 20 ;
    
    canvas->cd(1);
    if(0 != m_ipxVsPhi_mean)
      setHistoStyleAndDraw(m_ipxVsPhi_mean, "Mean IP_{x} Residual vs #phi", "#phi [rad]", 
                           -yMax, yMax) ;
    else
      plotNotFound("IPX-Vs-Phi-LongTracks-Mean") ;
    
    canvas->cd(2);
    if( 0 != m_ipyVsPhi_mean)
      setHistoStyleAndDraw(m_ipyVsPhi_mean, "Mean IP_{y} Residual vs #phi", "#phi [rad]", 
                           -yMax, yMax) ;
    else
      plotNotFound("IPY-Vs-Phi-LongTracks-Mean") ;
    
    canvas->cd(3);
    if(0 != m_ipxVsEta_mean)
      setHistoStyleAndDraw(m_ipxVsEta_mean, "Mean IP_{x} Residual vs #eta", "#eta", -yMax, yMax) ;
    else
      plotNotFound("IPX-Vs-Eta-LongTracks-Mean") ;

    canvas->cd(4);
    if(0 != m_ipyVsEta_mean)
      setHistoStyleAndDraw(m_ipyVsEta_mean, "Mean IP_{y} Residual vs #eta", "#eta", -yMax, yMax) ;
    else
      plotNotFound("IPY-Vs-Eta-LongTracks-Mean") ;
    
  }  
  else printf( "Draw option must be 1 or 2!" );
  
}

//====================================================================
// Set the ROOT appearance
//====================================================================

void IPResolutionsHistos::setROOTLooks() const
{
  gStyle->SetOptStat("0");  
  gStyle->SetMarkerSize(0.5);
  gStyle->SetOptFit() ;
  gStyle->SetLineWidth(2) ;
  gStyle->SetHistLineWidth(2) ;
  gStyle->SetFitFormat("4.2f") ;
  gStyle->SetTitleSize(0.1,"0") ;
  gStyle->SetStatH(0.25) ;
  gStyle->SetStatW(0.12) ;
  if(!m_isRef){
    gStyle->SetMarkerStyle(8);
    gStyle->SetMarkerColor(4);
  }
  else{
    gStyle->SetMarkerStyle(24);
    gStyle->SetMarkerColor(2);
    gStyle->SetHistLineColor(2);
    gStyle->SetStatX(0.7) ;
    gStyle->SetStatTextColor(2) ;
    gStyle->SetFuncColor(2) ;
  } 

}

//====================================================================
// Set the histos path
//====================================================================

void IPResolutionsHistos::setHistosPath( helperTools::MoniHistosPaths* p )
{
  m_pathVeloIPResolutionMonitor = p -> path( "VeloIPResolutionMonitor" );
}

//====================================================================
// Set histo style
//====================================================================

void IPResolutionsHistos::setHistoStyleAndDraw( TH1D* h, std::string title, std::string xName, 
                                                double yMin, double yMax ) const
{
  static TLegend* refLeg(0) ;
  if( m_isRef && refLeg == 0 ){
    refLeg = new TLegend(0.1, 0.7, 0.25, 0.8) ;
    refLeg->SetFillColor(0) ;
    refLeg->AddEntry(m_ipxVsInversePT, "Ref.") ;
  }
  h->SetTitle(title.c_str()) ;
  h->GetXaxis()->SetTitle(xName.c_str()) ;
  h->GetYaxis()->SetRangeUser(yMin, yMax) ;
  gPad->SetGridx() ;
  gPad->SetGridy() ;
  if(!m_isRef)
    h->Draw() ;
  else {
    h->Draw("sames") ;
    refLeg->Draw() ;
  }
  
}

//====================================================================
// 
//====================================================================

TPaveText* IPResolutionsHistos::fitResult( TF1* quad, TH1D* h )
{
  quad->SetParameters( h->GetMinimum(), 
                       (h->GetMaximum() - h->GetMinimum())/
                       (h->GetXaxis()->GetXmax() - h->GetXaxis()->GetXmin()),
                       0 );
  h->Fit( quad, "QO" );
  
  TPaveText* t = new TPaveText( 0.45, 0.1, 0.9, 0.2, "NDC" );
  t->SetBorderSize(1);
  std::ostringstream fit3D;
  fit3D << "Fit: " << quad->GetParameter(0)*1000. << " + " << 
    quad->GetParameter(1)*1000. << "/PT + " << quad->GetParameter(2)*1000. << "/PT^2";
  t->AddText( fit3D.str().c_str() );

  return t;
}

//=============================================================================
// Main macro function. Produces the IP resolutions histos
//=============================================================================

void drawIPResolutionsHistos( std::string fileName,
                              std::string refFile,
                              TCanvas* canvas=0, 
                              int drawType=1,
                              bool debug=false, 
                              helperTools::MoniHistosPaths* p=NULL )
{
  if ( debug )
    printf( "Running \"drawIPResolutionsHistos.C\" on file \"%s\"\n",
            fileName.c_str() );
  
  gROOT -> SetStyle( "Plain" );
  gROOT -> ForceStyle();
  
  if ( canvas == 0 )
    canvas = new TCanvas();

  static IPResolutionsHistos *plot(0) ;
  static std::string prevFileName ;
  if( 0 == plot ){
    prevFileName = fileName ;
    plot = new IPResolutionsHistos( fileName, debug, p );
  }
  else if( fileName != prevFileName ){
    prevFileName = fileName ;
    delete plot ;
    plot = new IPResolutionsHistos( fileName, debug, p ); 
  }
  
  plot   -> draw( canvas, drawType );

  if( refFile != std::string("") ){
    static IPResolutionsHistos *refPlot(0) ;
    static std::string prevRefFileName ;
    if( 0 == refPlot ){
      prevRefFileName = refFile ;
      refPlot = new IPResolutionsHistos( refFile, debug, p, true );
    }
    else if( refFile != prevRefFileName ){
      prevRefFileName = refFile ;
      delete refPlot ;
      refPlot = new IPResolutionsHistos( refFile, debug, p, true ); 
    }
    refPlot->draw(canvas, drawType) ;
  }
  
  canvas -> Modified();
  canvas -> Update();
}

//=============================================================================
