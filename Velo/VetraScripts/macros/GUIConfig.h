/*
 File: GUIConfig.h

 Add definitions global to the GUI here.

 A python parser that creates a dictionary of all symbol definitions
 is available in $VETRASCRIPTSROOT/python/GUIConfig.py
 */
#ifndef GUICONFIG_H
#define GUICONFIG_H 1

/*
 High occupancy cut in %.
 Must be an integer. 
 If you change this cut you also must
 make sure it is in the list of used cuts below. Otherwise DQS 
 trending will fall over.
 */
#define HIGH_OCC_CUT 1

/*
 List of used cuts.
 Since the occupancy changes depending on beam coditions,
 what is considered high occupancy might also change. This list
 is a list of comma speparated integers, each representing
 a cut in %. 
 The list does NOT reflect the history of cuts actually used. But
 each cut ever used should be contained in the list or the trending
 for high occupancy strips won't work. 
 
 This is a valid C preprocessor macro that can be used to
 initialise an arry in compiled code like this:

 #include "GuiConfig.h"
 int a[] = {USED_HIGH_OCC_CUTS};

 However CINT can't digest this because it has a very limited
 preprocessor. So we hide it from CINT.

 Currently this macro is only used by the python code for
 DQS trending.
 */
#ifndef __CINT__
#define USED_HIGH_OCC_CUTS 1,2,3,4,5 
#endif

#endif
