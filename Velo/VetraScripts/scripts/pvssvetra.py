#!/usr/bin/env python

###############################################################################
"""

 pvssvetra.py
 ============

 Script to interface PVSS to Vetra
 
 Description:
   Called by the "Analyse with Vetra" button in the ECS_Domain_User.pnl.
   It determines the run type, number and data location from the ECS and 
   passes it on to the "vetraOffline" script.
   It also runs the VELO monitoring GUI on the resultant root file.

 Requirements:


 Example usage:
   python pvssvetra.py <directory> <recipe>

 Options:
   

 @author Karol Hennessy
 @date   2009-11-27

"""
###############################################################################

import sys, os, string, commands

datadir = sys.argv[1]
recipe  = sys.argv[2]
nevts = sys.argv[3]
startevt = sys.argv[4]
heartbeat = sys.argv[5]

ind = datadir.rindex('/')
runnum = datadir[(ind+1):]
rundir = datadir[0:ind]

# =============================================================================
def determine_task( recipe ):
    """Determine the task name from the recipe name"""
    task='NZS+ZS'
    if recipe in ['PHYSICS', 'DEFAULT' ] :
	task = 'ZS'
    elif recipe in [ 'NZS', 'TESTPULSENZS' ] :
	task = 'NZS'
    elif (recipe == 'ADCDELAYSCAN') :
	task = 'ADCDELAYSCAN'
    elif (recipe == 'TED') :
	task = 'TED'
    return task
# =============================================================================

#task = determine_task( recipe )
task = recipe

#nevts = '2000'

prog = '${VETRASCRIPTSROOT}/scripts/vetraOffline'
hbfile = ""
if heartbeat == "hb" :
        hbfile = "-u /calib/velo/dqm/heartbeat.py"

os.system("ssh plus '/bin/bash -l -c \". ${VETRASCRIPTSROOT}/scripts/setup_shifters.sh; cd /calib/velo/dqm ; "+prog+" -a -t "+task+" -d "+rundir+" "+hbfile+" "+runnum+" "+nevts+" "+startevt+"\" ' ")

print 'Hit enter to close'

cmd = "ls -rt /calib/velo/dqm/"+runnum+"*/*.root | tail -1 "
rootfile = commands.getoutput( cmd )
print 'ROOT file:', rootfile

os.system("""ssh plus '/bin/bash -l -c \". ${VETRASCRIPTSROOT}/scripts/setup_shifters.sh; ${VETRASCRIPTSROOT}/scripts/VeloMoniGUI """+rootfile+"""  \" ' """ )

print 'Hit <enter> to close'

raw_input()

# =============================================================================
