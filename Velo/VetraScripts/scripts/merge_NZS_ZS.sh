#!/bin/bash --login
#################################################
#
# merge_NZS_ZS.sh
#
# @author: Marco Gersabeck
# @date: 2011/12/05
#
# Bash script to merge automatic analysis root files
#
#################################################

echo "shifter vetra version" $SHIFTERVETRAVERSION

source /sw/lib/lhcb/VETRA/VETRA_${SHIFTERVETRAVERSION}/Velo/VetraScripts/scripts/setup_shifters.sh
echo "host1" $HOSTNAME
export HOSTNAME;echo "import os;print os.environ['HOSTNAME']" | /sw/lib/lcg/external/Python/2.6.5/x86_64-slc5-gcc43-opt/bin/python
echo "host2" $HOSTNAME

# get latest run in DQS directory
runs=`ls /calib/velo/dqm/DQS/cron/ZSstreamDone/ToBeMerged`
echo ${runs}

# loop over list of candidate runs and analyse the first one found
ranrun=false
for run in $runs
do
  # check for a lock file and create one if there is none
  dothis=true
  if [ -f "/calib/velo/dqm/DQS/cron/NZSstreamDone/ToBeMerged/"$run ]; then
    echo "Found corresponding NZS run for run " $run
  else
    echo "Corresponding NZS run for run " $run " not found"
    dothis=false
  fi  
  zsfile=`ls -rt /calib/velo/dqm/$run/*_ZS.root |tail --lines=1`
  nzsfile=`ls -rt /calib/velo/dqm/$run/*_NZS.root |tail --lines=1`
  if $dothis && [ "$zsfile" ] && [ "$nzsfile" ] && [ -f "$zsfile" ] && [ -f "$nzsfile" ]; then
    echo "Found root files"
  else
    echo "Did not find root files"
    dothis=false
  fi

  echo "trying" $run
  lock_file="/calib/velo/dqm/DQS/cron/"$run".Merging.lock"
  if [ -f $lock_file ]; then
    echo "Found lock file for run" $run
    dothis=false
  else
    touch $lock_file 
    echo "Creating lock file for run" $run
    echo "Creating" $lock_file
  fi

  if $dothis; then
    targetFile=`echo $nzsfile|awk -F '_' '{print $1 "_" $2 "_" $3 "_" $4 "_NZS_ZS.root" }'`
    logfname=`echo $nzsfile|awk -F '_' '{print $1 "_" $2 "_" $3 "_" $4 "_NZS_ZS.log" }'`
    hadd $targetFile $zsfile $nzsfile
    echo $nzsfile > $logfname
    echo $zsfile >> $logfname
    ranrun=true
    touch /calib/velo/dqm/DQS/cron/NZSZSMerged/${run}
    rm /calib/velo/dqm/DQS/cron/ZSstreamDone/ToBeMerged/${run}
    rm /calib/velo/dqm/DQS/cron/NZSstreamDone/ToBeMerged/${run}
  else
    echo "nothing to do for run" $run  
  fi

  # delete the lock file if it was created earlier
  if $dothis && [ -f $lock_file ]; then
    rm $lock_file
    echo "Deleted" $lock_file
  fi

  # only analyse one run at a time to avoid endless running (could remove this)
  if $ranrun; then
    break
  fi
done
