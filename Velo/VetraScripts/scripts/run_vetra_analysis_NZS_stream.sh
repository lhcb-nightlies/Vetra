#!/bin/bash --login
#################################################
#
# run_vetra_analysis.sh
#
# @author: Marco Gersabeck
# @date: 2010/08/25
#
# Bash script to analyse runs with vetraOffline
# which have note yet been analysed
#
#################################################

# initialise environment
#source /sw/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v6r1p3/InstallArea/scripts/LbLogin.sh
#echo ${LBSCRIPTS_HOME}

#SETSHIFTERVERSION=`grep "export SHIFTERVETRAVERSION" /group/velo/sw/scripts/velo_login.sh`
#eval $SETSHIFTERVERSION
echo "shifter vetra version" $SHIFTERVETRAVERSION
#SHIFTERVETRAVERSION="v10r0"
#echo "manual change of vetra version to " $SHIFTERVETRAVERSION

source /sw/lib/lhcb/VETRA/VETRA_${SHIFTERVETRAVERSION}/Velo/VetraScripts/scripts/setup_shifters.sh
ulimit -Sm 2000000
ulimit -Sv 2000000
echo "host1" $HOSTNAME
export HOSTNAME;echo "import os;print os.environ['HOSTNAME']" | /sw/lib/lcg/external/Python/2.6.5/x86_64-slc5-gcc43-opt/bin/python
echo "host2" $HOSTNAME

# Vetra v9r4 fix for VeloCondDB
#export VELOSQLITEDBPATH=/home/krinnert/velocond/current

# get latest run in DQS directory
#latest_run=`ls /calib/velo/dqm/?????/VELODQM_*_NZS.root | awk -F '_' '{print $2}'|tail --lines=1`
latest_run=`ls -rt /calib/velo/dqm/DQS/cron/NZSstreamDone/ |tail --lines=1`
echo ${latest_run}

#latest_run=78013  # for testing only
#echo ${latest_run}

# define run range to check
incr=1     # this should be 1
incrmore=2000 # this should be >1

# finish definition of run range to check
target_run=$(($latest_run + $incrmore))
latest_run=$(($latest_run + $incr))
echo ${latest_run} ${target_run}

# get list of run in run range from rdbt which are in the bookkeeping
runs=`/group/online/scripts/rdbt -p LHCb -n "$latest_run" "$target_run" | grep -B2 -A2 "IN BKK" | grep -B1 OFFLINE | grep run | grep -v runType | awk '{print $2}'`
echo "Found runs" $runs

# loop over list of candidate runs and analyse the first one found
ranrun=false
for run in $runs
do
  # check for a lock file and create one if there is none
  echo "trying" $run
  lock_file="/calib/velo/dqm/DQS/cron/"$run".NZS.lock"
  dothis=true
  if [ -f $lock_file ]; then
    echo "Found lock file for run" $run
    dothis=false
  else
    touch $lock_file 
    echo "Creating lock file for run" $run
    echo "Creating" $lock_file
  fi

  # check for minimum run duration >30'? 
  if $dothis && [ "$run" ] && [ "$run" -gt 0 ]; then
    # conversion factors and threshold
    minute=60
    hour=3600
    day=86400
    threshold=1800
    # get start and end time of run
    start_hour=`/group/online/scripts/rdbt -n "$run"|grep startTime|awk '{print $3}'|awk -F ':' '{print $1}'`
    start_min=`/group/online/scripts/rdbt -n "$run"|grep startTime|awk '{print $3}'|awk -F ':' '{print $2}'`
    start_sec=`/group/online/scripts/rdbt -n "$run"|grep startTime|awk '{print $3}'|awk -F ':' '{print $3}'`
    end_hour=`/group/online/scripts/rdbt -n "$run"|grep startTime|awk '{print $6}'|awk -F ':' '{print $1}'`
    end_min=`/group/online/scripts/rdbt -n "$run"|grep startTime|awk '{print $6}'|awk -F ':' '{print $2}'`
    end_sec=`/group/online/scripts/rdbt -n "$run"|grep startTime|awk '{print $6}'|awk -F ':' '{print $3}'`
    if [ ${start_hour:0:1} -eq 0 ]; then
      start_hour=${start_hour:1:1} 
    fi
    if [ ${start_min:0:1} -eq 0 ]; then
      start_min=${start_min:1:1} 
    fi
    if [ ${start_sec:0:1} -eq 0 ]; then
      start_sec=${start_sec:1:1} 
    fi
    if [ ${end_hour:0:1} -eq 0 ]; then
      end_hour=${end_hour:1:1} 
    fi
    if [ ${end_min:0:1} -eq 0 ]; then
      end_min=${end_min:1:1} 
    fi
    if [ ${end_sec:0:1} -eq 0 ]; then
      end_sec=${end_sec:1:1} 
    fi
    # calculate difference
    start_hour=$(($start_hour * $hour))
    start_min=$(($start_min * $minute))
    start_time=$(($start_hour + $start_min + $start_sec))
    end_hour=$(($end_hour * $hour))
    end_min=$(($end_min * $minute))
    end_time=$(($end_hour + $end_min + $end_sec))
    difference=$(($end_time - $start_time))
    if [ "$difference" -lt 0 ]; then
      # if difference negative add 1 day, no run should be longer than 24h
      difference=$(($difference + $day))
    fi
    if [ "$difference" -lt "$threshold" ]; then
      echo "Run" $run "is too short with" $difference "seconds"
      if [ -f $lock_file ]; then
        rm $lock_file
        echo "Deleted" $lock_file
      fi
      dothis=false
    else
      echo "Run" $run "has sufficient length with" $difference "seconds"
    fi
  fi

  useCastorData=true
  if $dothis && [ "$run" ] && [ "$run" -gt 0 ]; then
    run_dir="/castorfs/cern.ch/grid/lhcb/data/2011/RAW/CALIB/LHCb/COLLISION11/"$run
    if [ -d $run_dir ]; then
      echo "Found run directory on castor"
    else
      echo "No run directory on castor with name" $run_dir
      run_dir="/daqarea/lhcb/data/2011/RAW/CALIB/LHCb/COLLISION11/"$run
      if [ -d $run_dir ]; then
        echo "Found run directory in daqarea"
        useCastorData=false
      else
        echo "No run directory in daqarea with name" $run_dir
        if [ -f $lock_file ]; then
          rm $lock_file
          echo "Deleted" $lock_file
        fi
        dothis=false
      fi  
    fi
  fi

  # perform the analysis
  nevts=-1
  if $useCastorData; then
    opts_nzs="-a -t NZS -d /castorfs/cern.ch/grid/lhcb/data/2011/RAW/CALIB/LHCb/COLLISION11 -q"
  else
    opts_nzs="-a -t NZS -d /daqarea/lhcb/data/2011/RAW/CALIB/LHCb/COLLISION11 -q"
  fi  
  if $dothis && [ "$run" ] && [ "$run" -gt 0 ]; then
    echo "Running Vetra on run" $run
    cd /calib/velo/dqm
    vetraOffline $opts_nzs $run $nevts
    # possibly check success of analysis here
    ranrun=true
    touch /calib/velo/dqm/DQS/cron/NZSstreamDone/${run}
    touch /calib/velo/dqm/DQS/cron/NZSstreamDone/ToBeMerged/${run}
  else
    echo "nothing to do for run" $run  
  fi

  # delete the lock file if it was created earlier
  if $dothis && [ -f $lock_file ]; then
    rm $lock_file
    echo "Deleted" $lock_file
  fi

  # only analyse one run at a time to avoid endless running (could remove this)
  if $ranrun; then
    break
  fi
done
