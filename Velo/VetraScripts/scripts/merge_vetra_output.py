"""Merge Clusters and NZS Vetra output in to a single file for the GUIs."""
import argparse
import glob
import os
from subprocess import call

from crontools.utils import add_runs, get_dir_tree
from crontools.rundbquery import RunDBQuery

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--run-data-dir', dest='jobdir', help='Job directory.',
                    default='/calib/velo/dqm/VeloView/VetraOutput')
_cliopts = parser.parse_args()
jobdir = _cliopts.jobdir

nzsrunlist = jobdir+'/RunList_NZS.txt'
clustersrunlist = jobdir+'/RunList_Clusters.txt'
mergedrunlist = jobdir+'/RunList_Merged.txt'

bruneldirbase = '/hist/Savesets/'
bruneldirmid = '/DQ/DataQuality/'

f = file(mergedrunlist)
lines = f.readlines()
if len( lines ) > 0:
  lastRun = int(lines[-1])
else:
  lastRun = 0

f = file(nzsrunlist)
lines = f.readlines()
nzsTODO = []
for line in lines:
  run = int(line)
  if run > lastRun:
    nzsTODO.append( run )

f = file(clustersrunlist)
lines = f.readlines()
clustersTODO = []
for line in lines:
  run = int(line)
  if run > lastRun:
    clustersTODO.append( run )

for run in nzsTODO:
  if run in clustersTODO:
    # check if all root files exist including Online Brunel
    arxivdir = get_dir_tree(run, prefix=jobdir)
    nzsfiles = filter(os.path.isfile, glob.glob(arxivdir + "/*NZS.root"))
    clustersfiles = filter(os.path.isfile, glob.glob(arxivdir + "/*Clusters.root"))
    if len( nzsfiles ) > 0 and len ( clustersfiles ) > 0:
      nzsfiles.sort(key=lambda x: os.path.getmtime(x))
      clustersfiles.sort(key=lambda x: os.path.getmtime(x))
      nzsfile = nzsfiles[-1]
      clustersfile = clustersfiles[-1]
      query = RunDBQuery([run])
      query.parse()
      endTime = query.get_run_info(run)['endTime']
      year = endTime[0:4]
      month = endTime[5:7]
      bruneldir = bruneldirbase + year + bruneldirmid + month
      brunelfiles = filter(os.path.isfile, glob.glob(bruneldir+'/*/DataQuality-' + str(run) + '*-EOR.root'))
      if len( brunelfiles ) == 0:
        bruneldir = bruneldirbase + year + bruneldirmid + str(int(month)+1)
        brunelfiles = filter(os.path.isfile, glob.glob(bruneldir+'/*/DataQuality-' + str(run) + '*-EOR.root'))
      if len( brunelfiles ) > 0:
        brunelfiles.sort(key=lambda x: os.path.getmtime(x))
        brunelfile = brunelfiles[-1]
        outname = nzsfile[:-8] + 'NZS_Clusters_Brunel.root'
        command = ['hadd', '-f', outname, nzsfile, clustersfile, brunelfile]
        # execute command
        retcode = call(command)
        if 0 == retcode:
          fo = open(outname[:-4]+'log','w')
          fo.write(' '.join(command))
          fo.close()
          add_runs( run, mergedrunlist )
          break # do just one run per cron instance to avoid system overload
        else:
          print 'hadd gave return code', retcode
      else:
        print 'No matching Brunel saveset found for run', run
    else:
      print 'Found only', len( nzsfiles ), 'NZS files and', len ( clustersfiles ), 'Clusters files for run', run
  else:
    print 'Run', run, 'found in NZS list but not in Clusters list'
