#!/bin/bash

#------------------------------------------------------------------------------
#
# Sets up the VELO shifters environment to run the official
# offline version of Vetra
#
# @author Eduardo Rodrigues
# @author Karol Hennessy
# @date   2008-09-01
# 
#------------------------------------------------------------------------------

# check whether the user is on the plus farm!
# -------------------------------------------
host=${HOSTNAME:0:4}
if [ $host != "plus" ]; then
    echo
    echo "      #################################################"
    echo "      WARNING: You are NOT running on the PLUS farm !!!"
    echo "               --> unable to correctly run vetraOffline"
    echo "      #################################################"
    echo
fi

# set default User_release_area=/calib/velo/dqm
# ---------------------------------------------
export User_release_area=/calib/velo/dqm

####################################################
### Vetra version - to be set by the velosexp!!! ###
####################################################
VETRAVERSION=
if [ -z $SHIFTERVETRAVERSION ] ; then
    VETRAVERSION=v16r1
else
    VETRAVERSION=$SHIFTERVETRAVERSION
fi

# set the environment
# -------------------
source ${LBSCRIPTS_HOME}/InstallArea/scripts/SetupProject.sh Vetra $VETRAVERSION

# set the environment for the ORACLE database (needed by the runDB tool)
# (somehow LD_LIBRARY_PATH is changed in a way that prevents rdbt from working)
# -----------------------------------------------------------------------------
opts='--arch 64'
source /sw/oracle/set_oraenv.sh $opts

# shortcuts
# ---------
export VETRASCRIPTSROOT=${User_release_area}/Vetra_${VETRAVERSION}/Velo/VetraScripts
alias vetraOffline=$VETRASCRIPTSROOT/scripts/vetraOffline
alias VeloMoniGUI=$VETRASCRIPTSROOT/scripts/VeloMoniGUI

cd /calib/velo/dqm
