#!/bin/bash --login

# for debugging
# set -o xtrace

# setup
declare script=$(readlink -e $BASH_SOURCE)
declare srcdir="${script%/*}"

# save job arguments
declare jobargs="$@"
shift $#


# initialise environment; LbLogin et al.
source /cvmfs/lhcb.cern.ch/group_login.sh &> /dev/null
echo ${LBSCRIPTS_HOME}
declare vetra=$(sed -ne 's/\s\+export \+SHIFTERVETRAVERSION=\(.\+\)/\1/p' /group/velo/sw/scripts/velo_login.sh)
echo "Vetra version:" $vetra

# set resource limits
ulimit -Sm 2000000
ulimit -Sv 2000000
echo "Running on:" $HOSTNAME

export VELOSQLDDDBROOT="/group/velo/config/scratch_recipe/velocond"
export VELOSQLITEDBPATH="/group/velo/config/scratch_recipe/velocond/db"
export NO_GIT_CONDDB=1

# run job
exec /calib/velo/dqm/Vetra_HEAD/build.$CMTCONFIG/run python /calib/velo/dqm/Vetra_HEAD/Velo/VetraScripts/scripts/merge_vetra_output_step2.py $jobargs
