"""Merge Clusters and NZS Vetra output in to a single file for the GUIs."""
import argparse
import glob
import os
from subprocess import call

from crontools.utils import add_runs, get_dir_tree
from crontools.rundbquery import RunDBQuery

def touch(fname, times=None):
  fhandle = open(fname, 'a')
  try:
    os.utime(fname, times)
  finally:
    fhandle.close()

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--run-data-dir', dest='jobdir', help='Job directory.',
                    default='/calib/velo/dqm/VeloView/VetraOutput')
_cliopts = parser.parse_args()
jobdir = _cliopts.jobdir

nzsrunlist = jobdir+'/RunList_NZS.txt'
clustersrunlist = jobdir+'/RunList_Clusters.txt'
mergedrunlist = jobdir+'/RunList_Merged_NZS_Clusters.txt'
touch(nzsrunlist)
touch(clustersrunlist)
touch(mergedrunlist)

f = file(mergedrunlist)
lines = f.readlines()
if len( lines ) > 0:
  lastRun = int(lines[-1])
else:
  lastRun = 0

# can be deleted once new scheme has taken over
oldmergedrunlist = jobdir+'/RunList_Merged.txt' 
f = file(oldmergedrunlist)
lines = f.readlines()
if len( lines ) > 0:
  oldLastRun = int(lines[-1])
  if lastRun < oldLastRun:
    lastRun = oldLastRun

f = file(nzsrunlist)
lines = f.readlines()
nzsTODO = []
for line in lines:
  run = int(line)
  if run > lastRun:
    nzsTODO.append( run )

f = file(clustersrunlist)
lines = f.readlines()
clustersTODO = []
for line in lines:
  run = int(line)
  if run > lastRun:
    clustersTODO.append( run )

for run in nzsTODO:
  if run in clustersTODO:
    # check if all root files exist including Online Brunel
    arxivdir = get_dir_tree(run, prefix=jobdir)
    nzsfiles = filter(os.path.isfile, glob.glob(arxivdir + "/*NZS.root"))
    clustersfiles = filter(os.path.isfile, glob.glob(arxivdir + "/*Clusters.root"))
    if len( nzsfiles ) > 0 and len ( clustersfiles ) > 0:
      # use respective latest NZS and Clusters file
      nzsfiles.sort(key=lambda x: os.path.getmtime(x))
      clustersfiles.sort(key=lambda x: os.path.getmtime(x))
      nzsfile = nzsfiles[-1]
      clustersfile = clustersfiles[-1]

      outname = nzsfile[:-8] + 'NZS_Clusters.root'
      command = ['hadd', '-f', outname, nzsfile, clustersfile]
      # execute command
      retcode = call(command)
      if 0 == retcode:
          fo = open(outname[:-4]+'log','w')
          fo.write(' '.join(command))
          fo.close()
          add_runs( run, mergedrunlist )
          break # do just one run per cron instance to avoid system overload
      else:
          print 'hadd gave return code', retcode
    else:
      print 'Found only', len( nzsfiles ), 'NZS files and', len ( clustersfiles ), 'Clusters files for run', run
  else:
    print 'Run', run, 'found in NZS list but not in Clusters list'
