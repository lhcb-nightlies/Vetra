"""Merge Clusters and NZS Vetra output in to a single file for the GUIs."""
import argparse
import glob
import os
from subprocess import call

from crontools.utils import add_runs, get_dir_tree
from crontools.rundbquery import RunDBQuery

def touch(fname, times=None):
  fhandle = open(fname, 'a')
  try:
    os.utime(fname, times)
  finally:
    fhandle.close()

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--run-data-dir', dest='jobdir', help='Job directory.',
                    default='/calib/velo/dqm/VeloView/VetraOutput')
_cliopts = parser.parse_args()
jobdir = _cliopts.jobdir

inputrunlist = jobdir+'/RunList_Merged_NZS_Clusters.txt'
mergedrunlist = jobdir+'/RunList_Merged_NZS_Clusters_Brunel.txt'
touch(inputrunlist)
touch(mergedrunlist)

bruneldirbase = '/hist/Savesets/'
bruneldirmid = '/DQ/DataQuality/'

f = file(mergedrunlist)
lines = f.readlines()
mergeDone = []
for line in lines:
  mergeDone.append( int(line) )

f = file(inputrunlist)
lines = f.readlines()
inputTODO = []
for line in lines:
  run = int(line)
  if run not in mergeDone:
    inputTODO.append( run )

print 'attempting to merge', len(inputTODO), 'runs'

count = 0
for run in inputTODO:
    # check if all root files exist including Online Brunel
    print 'attempting to merge run', run
    arxivdir = get_dir_tree(run, prefix=jobdir)
    inputfiles = filter(os.path.isfile, glob.glob(arxivdir + "/*NZS_Clusters.root"))
    if len( inputfiles ) > 0:
      inputfiles.sort(key=lambda x: os.path.getmtime(x))
      inputfile = inputfiles[-1]

      query = RunDBQuery([run])
      query.parse()
      endTime = query.get_run_info(run)['endTime']
      year = endTime[0:4]
      month = endTime[5:7]
      bruneldir = bruneldirbase + year + bruneldirmid + month
      brunelfiles = filter(os.path.isfile, glob.glob(bruneldir+'/*/DataQuality-' + str(run) + '*-EOR.root'))
      if len( brunelfiles ) == 0:
        lead = ''
        if 9 > int(month): lead = '0' # leading 0 required in directory structure
        bruneldir = bruneldirbase + year + bruneldirmid + lead + str(int(month)+1)
        brunelfiles = filter(os.path.isfile, glob.glob(bruneldir+'/*/DataQuality-' + str(run) + '*-EOR.root'))
      if len( brunelfiles ) > 0:
        brunelfiles.sort(key=lambda x: os.path.getmtime(x))
        brunelfile = brunelfiles[-1]
        outname = inputfile[:-8] + 'NZS_Clusters_Brunel.root'
        command = ['hadd', '-f', outname, inputfile, brunelfile]
        # execute command
        retcode = call(command)
        if 0 == retcode:
          fo = open(outname[:-4]+'log','w')
          fo.write(' '.join(command))
          fo.close()
          add_runs( run, mergedrunlist )
          count += 1
          if count >= 10: break # merge up to 10 runs per cron instance
        else:
          print 'hadd gave return code', retcode
      else:
        print 'No matching Brunel saveset found for run', run
    else:
      print 'Found no NZS_Clusters files for run', run
