################################################################################
# Package: TELL1Engine
################################################################################
gaudi_subdir(TELL1Engine v2r11)

gaudi_depends_on_subdirs(DAQ/Tell1Kernel)

gaudi_add_library(TELL1Engine
                  src/*.cpp
                  PUBLIC_HEADERS TELL1Engine
                  INCLUDE_DIRS DAQ/Tell1Kernel)

