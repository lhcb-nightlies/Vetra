// $Id: STTELL1ZSProcessEngine.h,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
#ifndef TELL1ENGINE_STTELL1ZSPROCESSENGINE_H 
#define TELL1ENGINE_STTELL1ZSPROCESSENGINE_H 1

// Include files
#include "STTELL1Engine.h"

/** @class STTELL1ZSProcessEngine STTELL1ZSProcessEngine.h TELL1Engine/STTELL1ZSProcessEngine.h
 *  
 *
 *  @author Tomasz Szumlak, Anne Keune
 *  @date   2007-08-12
 */

class STTELL1ZSProcessEngine: public STTELL1Engine{
public: 
  /** Standard constructor **/
  STTELL1ZSProcessEngine( );

  /** Destructor **/
  virtual ~STTELL1ZSProcessEngine( );
  void runZeroSuppression();
  int st_zs_process(int zs_enable,
                    HitThreshold hit_threshold,
                    ConfirmationThreshold confirmation_threshold,
                    SpillOverThreshold spill_over_threshold,
                    BoundaryStrip strip_start_temp,
                    Data in_data,
                    STTELL1::u_int32_t pp_max_clusters, 
                    STTELL1::u_int16_t *cluster, 
                    int *clus_numb, 
                    STTELL1::u_int8_t *adc_list, 
                    int *adc_numb);

  int st_clusterization_process(int *zs_line,
                                int *zs_h_line, 
                                STTELL1::u_int16_t *cluster, 
                                STTELL1::u_int8_t *adc_list, 
                                int *pp_clus_cnt, 
                                int *pp_adc_cnt,
                                STTELL1::u_int32_t pp_max_clusters,
                                int one_confirmation_threshold,
                                int one_spill_over_threshold,
                                STTELL1::u_int16_t strip_start, 
                                int boundary_strip);
  
  void setBoundaryStrips(const BoundaryStripVec& inVec);
  void setHitThresholds(const HitThresholdVec& inVec);
  void setConfirmationThresholds(const ConfirmationThresholdVec& inVec);
  void setSpillOverThresholds(const SpillOverThresholdVec& inVec);
  void setPPMaxClusters(const unsigned int nClusters);
  STTELL1::u_int32_t producedClusters() const;
  int includedADCs() const;
  void setOutClusters(TELL1ClusterVec& clusters) {m_rawClusters=&clusters;}
  void setOutADCS(TELL1ADCVec& adcs) {m_rawADCS=&adcs;}
  virtual void initTableMembers();
  
protected:

  void flushMemory();

private:

  HitThreshold m_hitThresholds;
  ConfirmationThreshold m_confirmationThresholds;
  SpillOverThreshold m_spillOverThresholds;
  BoundaryStrip m_boundaryStrips;
  int m_producedClusters;
  TELL1Cluster m_clusters;
  TELL1ADC m_adcs;
  int m_includedADCS;
  TELL1ClusterVec* m_rawClusters;
  TELL1ADCVec* m_rawADCS;
  unsigned int m_ppMaxClusters;
  Data m_cModuleData;
  
};
#endif // STTELL1ENGINE_TELL1ZSPROCESSENGINE_H
