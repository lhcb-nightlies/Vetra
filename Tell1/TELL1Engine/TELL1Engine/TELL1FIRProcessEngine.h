// $Id: TELL1FIRProcessEngine.h,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
#ifndef TELL1ENGINE_TELL1FIRPROCESSENGINE_H 
#define TELL1ENGINE_TELL1FIRPROCESSENGINE_H 1

// Include files
#include "TELL1Engine.h"

/** @class TELL1FIRProcessEngine TELL1FIRProcessEngine.h TELL1Engine/TELL1FIRProcessEngine.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2007-08-01
 */

class TELL1FIRProcessEngine: public TELL1Engine{
public: 
  /// Standard constructor
  TELL1FIRProcessEngine( );
  virtual ~TELL1FIRProcessEngine( ); ///< Destructor
  void runFilter();
  int velo_fir_process(int fir_enable,
                       VeloTELL1::FIRCoeff fir_coefficient,
                       VeloTELL1::Data in_Data);
  void setFIRCoeff(const VeloTELL1::FIRCoeffVec& inData);
  void saturate_for_8bits(int *data, int line_numb);

protected:

  virtual void initTableMembers();

private:
  
  VeloTELL1::FIRCoeff m_firCoeff;
  VeloTELL1::Data m_cModuleData;

};
#endif // TELL1ENGINE_TELL1FIRPROCESSENGINE_H
