// $Id: TELL1MCMSProcessEngine.h,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
#ifndef TELL1ENGINE_TELL1MCMSPROCESSENGINE_H 
#define TELL1ENGINE_TELL1MCMSPROCESSENGINE_H 1

// Include files
#include "TELL1Engine.h"

/** @class TELL1MCMSProcessEngine TELL1MCMSProcessEngine.h TELL1Engine/TELL1MCMSProcessEngine.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2008-03-08
 */
class TELL1MCMSProcessEngine: public TELL1Engine {
public: 
  /// Standard constructor
  TELL1MCMSProcessEngine( );
  virtual ~TELL1MCMSProcessEngine( ); ///< Destructor
  int velo_mcms_process(int mcms_enable, VeloTELL1::Data in_data);
  void saturate_for_8bits(int *data, int line_numb);
  void runMCMS();

protected:

private:

  VeloTELL1::Data m_cModuleData;

};
#endif // TELL1ENGINE_TELL1MCMSPROCESSENGINE_H
