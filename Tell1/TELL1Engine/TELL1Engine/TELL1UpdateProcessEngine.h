// $Id: TELL1UpdateProcessEngine.h,v 1.2 2008-10-17 16:43:01 szumlat Exp $
#ifndef TELL1ENGINE_TELL1UPDATEPROCESSENGINE_H 
#define TELL1ENGINE_TELL1UPDATEPROCESSENGINE_H 1

// Include files
#include "TELL1Engine.h"

/** @class TELL1UpdateProcessEngine TELL1UpdateProcessEngine.h TELL1Engine/TELL1UpdateProcessEngine.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2007-08-10
 */

class TELL1UpdateProcessEngine: public TELL1Engine{
public: 
  /// Standard constructor
  TELL1UpdateProcessEngine( );
  virtual ~TELL1UpdateProcessEngine( ); ///< Destructor
  void runUpdate();
  /*int velo_pedestal_update(int pedestal_auto_update_enable,
      int header_correction_enable, 
      HeaderStrip0 header_correction_value_strip_0,
      Header header, Data in_data, Data pedestal_sum);*/
  int velo_pedestal_update(int pedestal_auto_update_enable,
      int header_correction_enable,
      VeloTELL1::HeaderThresholds header_corr_threshold,
      VeloTELL1::HeaderCorrValues header_corr_value,
      VeloTELL1::LinkMask link_mask,
      VeloTELL1::Header header,
      VeloTELL1::Data in_data, VeloTELL1::PedSum self_maintain_pedestal_sum);
  void setHeaderCorrectionFlag(const int inValue);
  //void setHeaderCorrectionValues(const HeaderStrip0 inVec);
  void setHeaderThresholds(const VeloTELL1::HeaderThresholdsVec& inData);
  void setHeaderCorrValues(const VeloTELL1::HeaderCorrValuesVec& inData);
  void setLinkMask(const VeloTELL1::LinkMaskVec& inData);    
  void setHeaders(const VeloTELL1::EngineVec& inVec);
  void setPedestalSum(VeloTELL1::EngineVec& inVec);
  int headerCorrectionFlag() const;
  void setWhich2Run(const int inValue);
  
protected:

  virtual void initTableMembers();

private:

  int m_headerCorrectionFlag;        /// header corrections
  size_t m_headerStripSize;
  VeloTELL1::HeaderThresholds m_headerThresholds;
  VeloTELL1::HeaderCorrValues m_headerCorrValues;
  VeloTELL1::LinkMask m_linkMask;              /// pedestal masks
  const VeloTELL1::EngineVec* m_header;        /// header data
  VeloTELL1::EngineVec* m_pedestalSum;         /// pedestal sum  
  VeloTELL1::Data m_cModuleData;
  VeloTELL1::PedSum m_cModulePedestalSum;
  VeloTELL1::Header m_cModuleHeader;
  
};
#endif // TELL1ENGINE_TELL1UPDATEPROCESSENGINE_H
