// $Id: STTELL1PedestalProcessEngine.h,v 1.2 2010-01-13 12:51:26 akeune Exp $
#ifndef STTELL1PEDESTALPROCESSENGINE_H 
#define STTELL1PEDESTALPROCESSENGINE_H 1

// Include files
#include "STTELL1Engine.h"

/** @class STTELL1PedestalSubtractor STTELL1PedestalSubtractor.h
 *  
 *
 *  @author Tomasz Szumlak, Anne Keune
 *  @date   2007-05-25
 */

class STTELL1PedestalProcessEngine: public STTELL1Engine{
public:
  
  /** Standard constructor **/
  STTELL1PedestalProcessEngine( );

  /** Destructor **/
  virtual ~STTELL1PedestalProcessEngine( );
  void runSubtraction();

  int st_pedestal_process(int pedestal_enable,
                          int zs_enable, 
                          int header_correction_enable, 
                          HeaderThreshold header_corr_threshold,
                          HeaderCorrection header_corr_value,
                          Header header,
                          LinkMask link_mask,
                          LinkPedestal link_pedestal,
                          Data in_data,
                          STTELL1::u_int32_t  Pseudo_header_lo_thr,//---Alex 2009-11-10, header-corrrection-for-6-strips  
                          STTELL1::u_int32_t  Pseudo_header_hi_thr  );

  void setZSEnable(const int inValue);
  void setHeaderCorrectionFlag(const int inValue);
  void setHeaderCorrectionValues(const HeaderCorrectionVec& inData);
  void setHeaderThresholdValues(const HeaderThresholdVec& inData);
  void setPseudoHeaderThresholdValues(const HeaderThresholdVec& inData);
  void setHeaders(const EngineVec& inData);
  void setLinkPedestal(const LinkVec& inData);
  void setLinkMask(const LinkVec& inData);
  
 
protected:

  virtual void initTableMembers();

private:

  int m_zsEnable;                  /// clusterization enable flag
  int m_headerCorrectionFlag;      /// header correction flag 
  HeaderCorrection m_headerCorrections; /// header corrections
  HeaderThreshold m_headerThresholds; /// header thresholds
  HeaderThreshold m_pseudoHeaderThresholds; /// pseudo header thresholds
  const EngineVec* m_header;
  LinkPedestal m_linkPedestal;     /// pedestals to subtract
  LinkMask m_linkMask;             /// pedestal masks

  Data m_cModuleData;
  Header m_cModuleHeader;

};
#endif // STTELL1PEDESTALSUBTRACTOR_H
