// $Id: TELL1PedestalProcessEngine.h,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
#ifndef TELL1PEDESTALPROCESSENGINE_H 
#define TELL1PEDESTALPROCESSENGINE_H 1

// Include files
#include "TELL1Engine.h"

/** @class TELL1PedestalSubtractor TELL1PedestalSubtractor.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2007-05-25
 */

class TELL1PedestalProcessEngine: public TELL1Engine{
public:
  
  /// Standard constructor
  TELL1PedestalProcessEngine( );
  virtual ~TELL1PedestalProcessEngine( ); ///< Destructor
  void runSubtraction();
  /*
  int velo_pedestal_process(int pedestal_enable, int scaling_mode,
      int zs_enable, int header_correction_enable,
      HeaderStrip0 header_correction_value_strip_0,
      Header header, LinkMask link_mask,
      LinkPedestal link_pedestal, Data in_data);
  */
  int velo_pedestal_process(int pedestal_enable, int scaling_mode,
      int zs_enable, int header_correction_enable,
      VeloTELL1::HeaderThresholds header_corr_threshold,
      VeloTELL1::HeaderCorrValues header_corr_value,
      VeloTELL1::Header header, VeloTELL1::LinkMask link_mask,
      VeloTELL1::LinkPedestal link_pedestal, VeloTELL1::Data in_data);
  void setScalingMode(const int inValue);
  void setZSEnable(const int inValue);
  void setHeaderCorrectionFlag(const int inValue);
  //void setHeaderCorrectionValues(const HeaderStrip0Vec& inData);
  void setHeaderThresholds(const VeloTELL1::HeaderThresholdsVec& inData);
  void setHeaderCorrValues(const VeloTELL1::HeaderCorrValuesVec& inData);
  void setHeaders(const VeloTELL1::EngineVec& inData);
  void setLinkPedestal(const VeloTELL1::LinkPedestalVec& inData);
  void setLinkMask(const VeloTELL1::LinkMaskVec& inData);
  void setWhich2Run(const int inValue);
  void setPrintDetails(const int inValue);
  void state(const int ppfpga=0, const int procChan=0) const;
  void ioStreams(const int ppfpga=0, const int procChan=0) const;
  
protected:

  virtual void initTableMembers();
  
private:

  int m_scalingMode;               /// decide how the bits shall be cut
  int m_zsEnable;                  /// clusterization enable flag
  int m_headerCorrectionFlag;      /// header corrections
  size_t m_headerStripSize;
  //HeaderStrip0 m_headerCorrectionValueStrip0;
  VeloTELL1::EngineVec m_header;              /// header data
  VeloTELL1::LinkPedestal m_linkPedestal;     /// pedestals to subtract
  VeloTELL1::LinkMask m_linkMask;             /// pedestal masks
  VeloTELL1::Data m_cModuleData;
  VeloTELL1::Header m_cModuleHeader;
  VeloTELL1::HeaderThresholds m_headerThresholds;
  VeloTELL1::HeaderCorrValues m_headerCorrValues;
  int m_which2Run;
  int m_printDetails;

};
#endif // TELL1PEDESTALSUBTRACTOR_H
