// $Id: TELL1Engine.h,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
#ifndef TELL1ENGINE_TELL1ENGINE_H 
#define TELL1ENGINE_TELL1ENGINE_H 1

#include <iostream>

// Include files
#include "Tell1Kernel/VeloTell1Core.h"

/** @class TELL1Engine TELL1Engine.h TELL1Engine/TELL1Engine.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2007-08-09
 */

class TELL1Engine {
public: 
  /// Standard constructor
  TELL1Engine( );
  virtual ~TELL1Engine( ); ///< Destructor
  size_t dataSize() const { return m_dataSize; } 
  size_t linkPedestalSize() const { return m_linkPedestalSize; }
  size_t linkMaskSize() const { return m_linkMaskSize; }
  void setProcessEnable(const int inValue) { m_processEnable=inValue; }
  int processEnable() const { return m_processEnable; }
  void setInData(const VeloTELL1::EngineVec& inVec) { 
    m_inData=&inVec;
  }
  const VeloTELL1::EngineVec& inData()  { return *m_inData; }
  VeloTELL1::EngineVec& outData() { return m_outData; }

protected:
  
  virtual void initTabMembers() { };
 
  template<class T>
  void tableInit(T* inData, const size_t size, int init=0)
  {
    // the tables are meant to store integers
    T* begin=inData;
    T* end=begin+size;
    std::fill(begin, end, init);
  }

private:

  int m_processEnable;
  size_t m_dataSize;
  size_t m_linkPedestalSize;
  size_t m_linkMaskSize;
  const VeloTELL1::EngineVec* m_inData;
  VeloTELL1::EngineVec m_outData;

};
#endif // TELL1ENGINE_TELL1ENGINE_H
