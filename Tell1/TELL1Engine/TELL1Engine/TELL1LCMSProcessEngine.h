// $Id: TELL1LCMSProcessEngine.h,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
#ifndef TELL1ENGINE_TELL1LCMSPROCESSENGINE_H 
#define TELL1ENGINE_TELL1LCMSPROCESSENGINE_H 1

// Include files
#include "TELL1Engine.h"

/** @class TELL1LCMSProcessEngine TELL1LCMSProcessEngine.h TELL1Engine/TELL1LCMSProcessEngine.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2007-08-12
 */

class TELL1LCMSProcessEngine: public TELL1Engine{
public: 
  /// Standard constructor
  TELL1LCMSProcessEngine( );
  virtual ~TELL1LCMSProcessEngine( ); ///< Destructor
  int velo_lcms_process(int cm_enable, VeloTELL1::Data in_data);
  void saturate_for_8bits(int *data, int line_numb);
  void runLCMS();

private:

  VeloTELL1::Data m_cModuleData;

};
#endif // TELL1ENGINE_TELL1LCMSPROCESSENGINE_H
