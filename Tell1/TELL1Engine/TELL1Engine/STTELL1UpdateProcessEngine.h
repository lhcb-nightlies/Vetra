// $Id: STTELL1UpdateProcessEngine.h,v 1.2 2010-01-13 12:51:26 akeune Exp $
#ifndef TELL1ENGINE_STTELL1UPDATEPROCESSENGINE_H 
#define TELL1ENGINE_STTELL1UPDATEPROCESSENGINE_H 1

// Include files
#include "STTELL1Engine.h"

/** @class STTELL1UpdateProcessEngine STTELL1UpdateProcessEngine.h TELL1Engine/STTELL1UpdateProcessEngine.h
 *  
 *
 *  @author Tomasz Szumlak, Anne Keune
 *  @date   2007-08-10
 */

class STTELL1UpdateProcessEngine: public STTELL1Engine{
public: 
  /** Standard constructor **/
  STTELL1UpdateProcessEngine( );

  /** Destructor **/
  virtual ~STTELL1UpdateProcessEngine( );
  void runUpdate();

  int st_pedestal_update(int pedestal_auto_update_enable, 
                         int header_correction_enable, 
                         HeaderThreshold header_corr_threshold,
                         HeaderCorrection header_corr_value, 
                         Header header, 
                         LinkMask link_mask,
                         Data in_data, 
                         Data pedestal_sum,
                         STTELL1::u_int32_t  Pseudo_header_lo_thr,//---Alex 2009-11-10, header-corrrection-for-6-strips  
                         STTELL1::u_int32_t  Pseudo_header_hi_thr); //---Alex 2009-11-10, header-corrrection-for-6-strips);

  void setHeaderCorrectionFlag(const int inValue);
  void setHeaderCorrectionValues(const HeaderCorrectionVec& inVec);
  void setHeaderThresholdValues(const HeaderThresholdVec& inVec);
  void setPseudoHeaderThresholdValues(const HeaderThresholdVec& inVec);
  void setLinkMask(const LinkVec& inVec);
  void setHeaders(const EngineVec& inVec);
  void setPedestalSum(EngineVec& inVec);
  int headerCorrectionFlag() const;
  virtual void initTableMembers();
  
protected:

private:

  int m_headerCorrectionFlag;        /// header correction flag 
  HeaderCorrection m_headerCorrections; /// header corrections
  HeaderThreshold m_headerThresholds; /// header thresholds
  HeaderThreshold m_pseudoHeaderThresholds; /// pseudo header thresholds
  LinkMask m_linkMask;
  const EngineVec* m_header;                /// header data
  EngineVec* m_pedestalSum;        /// updated pedestal sum

  Data m_cModuleData;
  Data m_cModulePedestalSum;
  Header m_cModuleHeader;

  
};
#endif // TELL1ENGINE_STTELL1UPDATEPROCESSENGINE_H
