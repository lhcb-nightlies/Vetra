// $Id: STTELL1LCMSProcessEngine.h,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
#ifndef TELL1ENGINE_STTELL1LCMSPROCESSENGINE_H 
#define TELL1ENGINE_STTELL1LCMSPROCESSENGINE_H 1

// Include files
#include "STTELL1Engine.h"

/** @class STTELL1LCMSProcessEngine STTELL1LCMSProcessEngine.h TELL1Engine/STTELL1LCMSProcessEngine.h
 *  
 *
 *  @author Tomasz Szumlak, Anne Keune
 *  @date   2007-08-12
 */

class STTELL1LCMSProcessEngine: public STTELL1Engine{
public: 
  /** Standard constructor **/
  STTELL1LCMSProcessEngine( );

  /** Destructor **/
  virtual ~STTELL1LCMSProcessEngine( );
  int st_lcms_process(int cm_enable, CMSThreshold cms_threshold, Data in_data);
  void saturate_for_8bits(int *data, int line_numb);
  void runLCMS();
  void setCMSThreshold(const CMSThresholdVec& inData);
  virtual void initTableMembers();

protected:

private:

  CMSThreshold m_cmsThreshold;
  Data m_cModuleData;

};
#endif // TELL1ENGINE_STTELL1LCMSPROCESSENGINE_H
