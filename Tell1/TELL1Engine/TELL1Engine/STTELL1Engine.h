// $Id: STTELL1Engine.h,v 1.2 2009-09-15 14:01:21 szumlat Exp $
#ifndef TELL1ENGINE_STTELL1ENGINE_H 
#define TELL1ENGINE_STTELL1ENGINE_H 1

// raw memory operation
#include <cstring>

// Include files
#include "Tell1Kernel/STTell1Core.h"

/** @class TELL1Engine STTELL1Engine.h TELL1Engine/STTELL1Engine.h
 *  
 *
 *  @author Tomasz Szumlak, Anne Keune
 *  @date   2007-08-09
 */

using namespace STTELL1;

class STTELL1Engine {
public: 
  /** Standard constructor **/
  STTELL1Engine( );

  /** Destructor **/
  virtual ~STTELL1Engine( );
  size_t dataSize() const {return m_dataSize;}
  void setProcessEnable(const int inValue) {m_processEnable = inValue;}
  int processEnable() const { return m_processEnable; }
  void setInData(const EngineVec& inVec) {m_inData = &inVec;}
  const EngineVec& inData() {return *m_inData;}
  EngineVec& outData() {return m_outData;} 

 
protected:

  template<class T>
  void tableInit(T* inData, const size_t size, int init=0) {
    // the tables are meant to store integers
    T* begin=inData;
    T* end=begin+size;
    std::fill(begin, end, init);
  }

private:

  int m_processEnable;
  const EngineVec* m_inData;
  EngineVec m_outData;
  size_t m_dataSize;
  size_t m_headerSize;
  size_t m_headerCorrectionSize;
  size_t m_headerThresholdSize;
  size_t m_linkPedestalSize;
  size_t m_linkMaskSize;

};
#endif // TELL1ENGINE_STTELL1ENGINE_H
