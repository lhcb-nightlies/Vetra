// $Id: TELL1ReorderProcessEngine.h,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
#ifndef TELL1ENGINE_TELL1REORDERPROCESSENGINE_H 
#define TELL1ENGINE_TELL1REORDERPROCESSENGINE_H 1

// Include files
#include "TELL1Engine.h"

/** @class TELL1ReorderProcessEngine TELL1ReorderProcessEngine.h TELL1Engine/TELL1ReorderProcessEngine.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2007-08-10
 */

class TELL1ReorderProcessEngine: public TELL1Engine {
public: 
  /// Standard constructor
  TELL1ReorderProcessEngine( );
  virtual ~TELL1ReorderProcessEngine( ); ///< Destructor
  void runReordering();
  int velo_reorder_channel(int reorder_mode, VeloTELL1::Data in_data);
  void setReorderMode(const int inValue);
  void setCableCorrection(const int inValue);
  int reorderMode() const;

protected:

  int velo_reorder_convert(int mode, int direction,
                           int *old_pos, int *new_pos);

private:

  int m_reorderMode;
  int m_cableError;
  VeloTELL1::Data m_cModuleData;

};
#endif // TELL1ENGINE_TELL1REORDERPROCESSENGINE_H
