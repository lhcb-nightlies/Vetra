// $Id: TELL1ReorderProcessEngine.cpp,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
// Include files 
#include <cstring>

// STL
#include<vector>
#include<algorithm>

// local
#include "TELL1Engine/TELL1ReorderProcessEngine.h"

// std
#include <iostream>

//-----------------------------------------------------------------------------
// Implementation file for class : TELL1ReorderProcessEngine
//
// 2007-08-10 : Tomasz Szumlak
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TELL1ReorderProcessEngine::TELL1ReorderProcessEngine():
  TELL1Engine(),
  m_reorderMode ( 0 ),
  m_cableError ( 0 )
{ }
//=============================================================================
// Destructor
//=============================================================================
TELL1ReorderProcessEngine::~TELL1ReorderProcessEngine() { }
//=============================================================================
int TELL1ReorderProcessEngine::velo_reorder_channel(int reorder_mode,
    VeloTELL1::Data in_data)
{
  int err = 0;
  int ppx,channel,t;
  int old_pos, new_pos;

  int data_format[4][9][64];
  if(reorder_mode == 0) 
  {
    return(err);
  }
  for(ppx=0;ppx<4;ppx++)
    for(channel=0;channel<9;channel++)
      for(t=0;t<64;t++)
      {
        data_format[ppx][channel][t] = in_data[ppx][channel][t];
        in_data[ppx][channel][t] = 0;
      }
  //std::cout<< " data inside reorder: " << data_format[2][6][36] <<std::endl;
  if(reorder_mode==1)
  {
    //std::cout<< " inner strips - module " <<std::endl;
    new_pos = 0;
    for(t=0;t<171;t++)
    {
      new_pos = t;
      velo_reorder_convert(reorder_mode, 0, &old_pos, &new_pos) ;
      //std::cout<< " old pos: " << old_pos << " new pos: " << new_pos <<std::endl;
      ppx = old_pos/512;
      channel = (old_pos%512)/64;
      in_data[0][t/64][t%64] = data_format[ppx][channel][old_pos%64];

    }	  
    for(t=0;t<171;t++)
    {
      new_pos = t+171;
      velo_reorder_convert(reorder_mode, 0, &old_pos, &new_pos) ;
      ppx = old_pos/512;
      channel = (old_pos%512)/64;
      in_data[1][t/64][t%64] = data_format[ppx][channel][old_pos%64];

    }	  
    for(t=0;t<170;t++)
    {
      new_pos = t+171+171;
      velo_reorder_convert(reorder_mode, 0, &old_pos, &new_pos) ;
      //std::cout<< " old pos: " << old_pos << " new pos: " << new_pos <<std::endl;
      ppx = old_pos/512;
      channel = (old_pos%512)/64;
      in_data[2][t/64][t%64] = data_format[ppx][channel][old_pos%64];

    }	  
    for(t=0;t<171;t++)
    {
      new_pos = t+171+171+170;
      velo_reorder_convert(reorder_mode, 0, &old_pos, &new_pos) ;
      //std::cout<< " old pos: " << old_pos << " new pos: " << new_pos <<std::endl;
      ppx = old_pos/512;
      channel = (old_pos%512)/64;
      in_data[3][t/64][t%64] = data_format[ppx][channel][old_pos%64];

    }	  
    // outer strips
    for(t=0;t<341;t++)
    {
      new_pos = t+683;
      velo_reorder_convert(reorder_mode, 0, &old_pos, &new_pos) ;
      //std::cout<< " old pos: " << old_pos << " new pos: " << new_pos <<std::endl;
      ppx = old_pos/512;
      channel = (old_pos%512)/64;
      in_data[0][t/64+3][t%64] = data_format[ppx][channel][old_pos%64];

    }	  
    for(t=0;t<341;t++)
    {
      new_pos = t+683+341;
      velo_reorder_convert(reorder_mode, 0, &old_pos, &new_pos) ;
      //std::cout<< " old pos: " << old_pos << " new pos: " << new_pos <<std::endl;
      ppx = old_pos/512;
      channel = (old_pos%512)/64;
      in_data[1][t/64+3][t%64] = data_format[ppx][channel][old_pos%64];

    }	  
    for(t=0;t<342;t++)
    {
      new_pos = t+683+341+341;
      velo_reorder_convert(reorder_mode, 0, &old_pos, &new_pos) ;
      //std::cout<< " old pos: " << old_pos << " new pos: " << new_pos <<std::endl;
      ppx = old_pos/512;
      channel = (old_pos%512)/64;
      in_data[2][t/64+3][t%64] = data_format[ppx][channel][old_pos%64];

    }	  
    for(t=0;t<341;t++)
    {
      new_pos = t+683+341+341+342;
      velo_reorder_convert(reorder_mode, 0, &old_pos, &new_pos) ;
      //std::cout<< " old pos: " << old_pos << " new pos: " << new_pos <<std::endl;
      ppx = old_pos/512;
      channel = (old_pos%512)/64;
      in_data[3][t/64+3][t%64] = data_format[ppx][channel][old_pos%64];

    }	  
  }   // set the data generater array  
  else if(reorder_mode==2)
  {
    for(t=0;t<2048;t++)
    {
      new_pos = t;
      velo_reorder_convert(reorder_mode, 0, &old_pos, &new_pos) ;
      ppx = t/512;
      in_data[ppx][(new_pos%512)/64][(new_pos%512)%64] = data_format[ppx][(old_pos%512)/64][(old_pos%512)%64];
    }
  }

  return (err);		
}
//=============================================================================
int TELL1ReorderProcessEngine::velo_reorder_convert(int mode, int direction,
    int *old_pos, int *new_pos)
{
  int err = 0;

  int pos_temp;
  int pos_sect;
  int pos_in_sect;
  int inner_cnt = 0, outer_cnt = 0;

  int base_pos;

  switch(mode)
  {
    //------------------------------------------------------
    ///mode = 0   =>  no reorder
    ///
    ///nothing is done except making them equal according to different direction
    ///
    //------------------------------------------------------
    case 0:
      if (direction==1)     (*new_pos) = (*old_pos);
      else if(direction==0) (*old_pos) = (*new_pos);
      else 
      { err = 1;
        return(err);
      }
      break;
      //------------------------------------------------------
      ///mode = 1   =>  PHI reorder
      ///
      ///According to the table of PHI sensor reordering,
      ///it has a loop for every 12 strips, inside which 8 are outer strips and 4 are inner strips.
      ///So dealing with each strip according to its postion in a sector of 12, 
      ///we can implement the reordering easily.
      //------------------------------------------------------
    case 1:
      if (direction==1)            //old pos -> new pos
      {
        //        calculate the sector number and the position in a sector
        //        according to the table
        pos_temp = (*old_pos) - (*old_pos)%512;   //start position of the PP fpga
        pos_temp+= 511-(*old_pos)%512;            //to understand this line, the reorder table is needed

        pos_sect = pos_temp/12;      //get the sector number
        pos_in_sect = pos_temp%12;   //postion in the sector of 12

        switch(pos_in_sect)          //12 strips in every loop
        {
          case 0:
            outer_cnt = 8*pos_sect;     // outer 1   
            inner_cnt = 683;    
            *new_pos  = outer_cnt + inner_cnt;     
            break;
          case 1:
            outer_cnt = 8*pos_sect+1;   // outer 2
            inner_cnt = 683;    
            *new_pos  = outer_cnt + inner_cnt;     
            break;
          case 2:
            outer_cnt = 0;              // 
            inner_cnt = 4*pos_sect+0;   // inner 1 
            *new_pos  = outer_cnt + inner_cnt;     
            break;
          case 3:
            outer_cnt = 0;              // 
            inner_cnt = 4*pos_sect+1;   // inner 2 
            *new_pos  = outer_cnt + inner_cnt;     
            break;
          case 4:
            outer_cnt = 8*pos_sect+2;   // outer 3
            inner_cnt = 683;    
            *new_pos  = outer_cnt + inner_cnt;     
            break;
          case 5:
            outer_cnt = 8*pos_sect+4;   // outer 5
            inner_cnt = 683;    
            *new_pos  = outer_cnt + inner_cnt;     
            break;
          case 6:
            outer_cnt = 8*pos_sect+3;   // outer 4
            inner_cnt = 683;    
            *new_pos  = outer_cnt + inner_cnt;     
            break;
          case 7:
            outer_cnt = 0;              // 
            inner_cnt = 4*pos_sect+2;   // inner 3 
            *new_pos  = outer_cnt + inner_cnt;     
            break;
          case 8:
            outer_cnt = 8*pos_sect+5;   // outer 6
            inner_cnt = 683;    
            *new_pos  = outer_cnt + inner_cnt;     
            break;
          case 9:
            outer_cnt = 8*pos_sect+6;   // outer 7
            inner_cnt = 683;    
            *new_pos  = outer_cnt + inner_cnt;     
            break;
          case 10:
            outer_cnt = 0;              // 
            inner_cnt = 4*pos_sect+3;   // inner 4 
            *new_pos  = outer_cnt + inner_cnt;     
            break;
          case 11:
            outer_cnt = 8*pos_sect+7;   // outer 7
            inner_cnt = 683;    
            *new_pos  = outer_cnt + inner_cnt;     
            break;
          default:
            err = 1; return(err);
            break;
        }
      }
      else if(direction==0)             //new pos -> old pos
      {
        if(((*new_pos)>=683)&& ((*new_pos)<2048))     //outer part
        {
          outer_cnt = *new_pos - 683;
          pos_sect = outer_cnt/8;                    //to get the sector order of the outer strip. there are 8 outer strips in a 12 sector. 
          pos_in_sect = outer_cnt%8;                 //order inside a sector
          switch(pos_in_sect)
          {
            case 0:
              *old_pos = pos_sect*12 + 0; break;
            case 1:
              *old_pos = pos_sect*12 + 1; break;
            case 2:
              *old_pos = pos_sect*12 + 4; break;
            case 4:
              *old_pos = pos_sect*12 + 5; break;
            case 3:
              *old_pos = pos_sect*12 + 6; break;
            case 5:
              *old_pos = pos_sect*12 + 8; break;
            case 6:
              *old_pos = pos_sect*12 + 9; break;
            case 7:
              *old_pos = pos_sect*12 + 11; break;
            default: 
              err = 1;	return(err); break;
          }
        }		
        else if(((*new_pos)<683)&&((*new_pos)>=0))             //inner strips
        {
          inner_cnt = *new_pos; 
          pos_sect = inner_cnt/4;           //to get the sector order of the inner strip. there are 4 inner strips in a 12 sector. 
          pos_in_sect = inner_cnt%4;        //order inside a sector
          //std::cout<< " inner counter: " << pos_in_sect 
          //<< " pos_sec " << pos_sect <<std::endl;
          switch(pos_in_sect)
          {
            case 0:
              *old_pos = pos_sect*12 + 2; break;
            case 1:
              *old_pos = pos_sect*12 + 3; break;
            case 2:
              *old_pos = pos_sect*12 + 7; break;
            case 3:
              *old_pos = pos_sect*12 + 10; break;
            default: 
              err = 1;	return(err); break;
          }
        }
        else
        { err  = 1;		return(err);}

        pos_temp = (*old_pos) - (*old_pos)%512;
        pos_temp+= 511-(*old_pos)%512;
        *old_pos = pos_temp;
        //std::cout<< " old pos: " << *old_pos <<std::endl;

      }
      else 
      { 
        err = 1;
        return(err);
      }
      break;
      //------------------------------------------------------
      ///mode = 1   =>  R sensor reorder
      ///
      ///According to the table of R sensor reordering,
      ///the first 1024 strips have the same ordering as the other 1024 strips.
      ///And inside each 1024, every sector of 128 strips has a different ordering.
      ///So dealing with each strip according to its postion in a sector of 128, 
      ///we can implement the reordering easily.
      //------------------------------------------------------

    case 2:
      if (direction==1)  
      {
        base_pos = ((*old_pos)/1024)*1024;  //because the first 1024 has the same structure as the last, so...
        pos_sect = ((*old_pos)%1024)/128;   //sector order of 128 strips
        switch(pos_sect)                    //different sector has a different ordering.
        {                                   //to understand the ordering well, the reordering table is needed :-)
          case 0:
            *new_pos = *old_pos;
            break;                                             //0
          case 1:
            pos_in_sect = 127-((*old_pos)%1024)%128;
            pos_sect = pos_sect+2;                             //3
            *new_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          case 2:
            pos_in_sect = 127-((*old_pos)%1024)%128;
            pos_sect = pos_sect;                               //2
            *new_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          case 3:
            pos_in_sect = 127-((*old_pos)%1024)%128;
            pos_sect = pos_sect-2;                             //1
            *new_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          case 4:
            pos_in_sect = ((*old_pos)%1024)%128;
            pos_sect = pos_sect+1;                             //5
            *new_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          case 5:
            pos_in_sect = ((*old_pos)%1024)%128;
            pos_sect = pos_sect+1;                             //6
            *new_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          case 6:
            pos_in_sect = ((*old_pos)%1024)%128;
            pos_sect = pos_sect+1;                             //7
            *new_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          case 7:
            pos_in_sect = 127-((*old_pos)%1024)%128;
            pos_sect = pos_sect-3;                             //4
            *new_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          default:
            break;
        }
      }
      else if(direction==0)
      {
        base_pos = ((*new_pos)/1024)*1024;
        pos_sect = ((*new_pos)%1024)/128;
        switch(pos_sect)
        {
          case 0:
            *old_pos = *new_pos;
            break;                                             //0
          case 3:
            pos_in_sect = 127-((*new_pos)%1024)%128;
            pos_sect = pos_sect-2;                             //1
            *old_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          case 2:
            pos_in_sect = 127-((*new_pos)%1024)%128;
            pos_sect = pos_sect;                               //2
            *old_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          case 1:
            pos_in_sect = 127-((*new_pos)%1024)%128;
            pos_sect = pos_sect+2;                             //3
            *old_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          case 5:
            pos_in_sect = ((*new_pos)%1024)%128;
            pos_sect = pos_sect-1;                             //4
            *old_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          case 6:
            pos_in_sect = ((*new_pos)%1024)%128;
            pos_sect = pos_sect-1;                             //5
            *old_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          case 7:
            pos_in_sect = ((*new_pos)%1024)%128;
            pos_sect = pos_sect-1;                             //6
            *old_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          case 4:
            pos_in_sect = 127-((*new_pos)%1024)%128;
            pos_sect = pos_sect+3;                             //7
            *old_pos = base_pos+pos_sect*128+pos_in_sect;
            break;
          default:
            break;
        }
      }
      else 
      { 
        err = 1;
        return(err);
      }
      break;
    default:
      err = 1;
      return(err);	
      break;

  }
  if(m_cableError){  
    switch(*old_pos/512){
      case 0:
        *old_pos+=1536; break;
      case 1:
        *old_pos+=512; break;
      case 2:
        *old_pos-=512; break;
      case 3:
        *old_pos-=1536; break;
      default:
        break;
    }
  }
  return(err);	
}
//=============================================================================
void TELL1ReorderProcessEngine::setReorderMode(const int inValue)
{
  m_reorderMode=inValue;
}
//=============================================================================
int TELL1ReorderProcessEngine::reorderMode() const
{
  return ( m_reorderMode );
}
//=============================================================================
void TELL1ReorderProcessEngine::setCableCorrection(const int inValue)
{
  m_cableError=inValue;
}
//=============================================================================
void TELL1ReorderProcessEngine::runReordering()
{
  // prepare data for c-module
  std::memcpy(**m_cModuleData,&(*inData().begin()),sizeof(VeloTELL1::Data));

  // run the reorder c-module
  int error=velo_reorder_channel(reorderMode(),
                                 m_cModuleData);

  // write reordered data
  if(!error){    
    std::memcpy(&(*outData().begin()),(**m_cModuleData),
                sizeof(VeloTELL1::Data));
  }else if(error){
    std::cout<< " --> Reordering Module FAILED! " <<std::endl;
  }
}
//--
