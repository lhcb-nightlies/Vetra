// $Id: STTELL1UpdateProcessEngine.cpp,v 1.3 2010-01-13 12:51:26 akeune Exp $
// Include files 
// STL
#include<vector>
#include<algorithm>

// local
#include "TELL1Engine/STTELL1UpdateProcessEngine.h"

// std
#include <iostream>

//-----------------------------------------------------------------------------
// Implementation file for class : STTELL1UpdateProcessEngine
//
// 2007-08-10 : Tomasz Szumlak, Anne Keune
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STTELL1UpdateProcessEngine::STTELL1UpdateProcessEngine(  ):
  STTELL1Engine(),
  m_headerCorrectionFlag ( 0 ),
  m_header ( ),
  m_pedestalSum ( )
{
  this->initTableMembers();
}
//=============================================================================
// Destructor
//=============================================================================
STTELL1UpdateProcessEngine::~STTELL1UpdateProcessEngine() {}
//=============================================================================

int STTELL1UpdateProcessEngine::st_pedestal_update(
                                                   int pedestal_auto_update_enable, 
                                                   int header_correction_enable, 
                                                   STTELL1::u_int32_t header_corr_threshold[2],
                                                   int header_corr_value[96][6][8],    ////---Alex 2009-11-10, header-corrrection-for-6-strips 
                                                   int header[4][6][4][4], 
                                                   STTELL1::u_int8_t link_mask[96][32],
                                                   int in_data[4][6][4][32], 
                                                   int self_maintain_pedestal_sum[4][6][4][32],
                                                   STTELL1::u_int32_t  Pseudo_header_lo_thr,//---Alex 2009-11-10, header-corrrection-for-6-strips  
                                                   STTELL1::u_int32_t  Pseudo_header_hi_thr //---Alex 2009-11-10, header-corrrection-for-6-strips 
                                                   )
{
	int ppx,beetle,link,strip,data_sub_l,header_correction,err = 0;
	int header_zero_threshold,header_one_threshold;
  
  //int ppx_show=0,beetle_show=0,link_show=0;
  
  int header_b[3];
  int header_err_b;
  int pcn_lsb;
  int header_corr_v;
  
	header_zero_threshold  = header_corr_threshold[0];
	header_one_threshold   = header_corr_threshold[1];
  
	if (pedestal_auto_update_enable==1)		
	{
		for(ppx=0;ppx<4;ppx++) 
		{
			for(beetle=0;beetle<6;beetle++)
			{
        for(link=0;link<4;link++)
        {
          //looks like this isn't used but fix to remove compiler errors
          if (      header[ppx][beetle][0][3]    > (int) Pseudo_header_hi_thr)   pcn_lsb = 1;
          else if ( header[ppx][beetle][0][3]    < (int) Pseudo_header_lo_thr)   pcn_lsb = 0;
          else                                                            pcn_lsb = 0;
          pcn_lsb *= 1;
          header_err_b = 0;
          if      (header[ppx][beetle][link][1] > header_one_threshold)   {header_b[2] = 1; }
          else if (header[ppx][beetle][link][1] < header_zero_threshold)  {header_b[2] = 0; }
          else                                                            {header_b[2] = 0; header_err_b = 1;}
	  
          if      (header[ppx][beetle][link][2] > header_one_threshold)   {header_b[1] = 1; }
          else if (header[ppx][beetle][link][2] < header_zero_threshold)  {header_b[1] = 0; }
          else                                                            {header_b[1] = 0; header_err_b = 1;}
          
          if      (header[ppx][beetle][link][3] > header_one_threshold)   {header_b[0] = 1; }
          else if (header[ppx][beetle][link][3] < header_zero_threshold)  {header_b[0] = 0; }
          else                                                            {header_b[0] = 0; header_err_b = 1;}
          
          header_corr_v = (header_b[2]<<2) + (header_b[1]<<1) + header_b[0];
          
          //-----------
          for(strip=0;strip<32;strip++) 
          {
            if ((strip<6) && (header_correction_enable==1) && (header_err_b ==0) )	
            {
              // header correction for first 6 strips
              header_correction = header_corr_value[ppx*24+beetle*4+link][strip][header_corr_v];
              
            }
            else
              header_correction = 0;
            
            data_sub_l = in_data[ppx][beetle][link][strip]-(self_maintain_pedestal_sum[ppx][beetle][link][strip]>>10) - header_correction; // header correction
            if (link_mask[ppx*24+beetle*4+link][strip] == 1) // added to avoid negative values if masked
            {  	self_maintain_pedestal_sum[ppx][beetle][link][strip] = 0x80000;
            
            }
            else if (data_sub_l > 15)
            {	self_maintain_pedestal_sum[ppx][beetle][link][strip] += 15;  // saturated update
            
            }
            else if (data_sub_l < -15) 
            {	self_maintain_pedestal_sum[ppx][beetle][link][strip] -= 15;  // saturated update
            
            }
            else
            {	self_maintain_pedestal_sum[ppx][beetle][link][strip]+= data_sub_l; 
            
            }
          }
          //------------
          /*
          if ((ppx==ppx_show) && (beetle==beetle_show) && (link==link_show) )
          { PRINTF_DEBUG("\npedestal self :");
            for(strip=0;strip<32;strip++) PRINTF_DEBUG(" %3d",self_maintain_pedestal_sum[ppx][beetle][link][strip]>>10);
          } 
          */    
        }
		  }
		} 
	}
	
	return(err);
						   
  
}


//=============================================================================
void STTELL1UpdateProcessEngine::setHeaderCorrectionFlag(const int inValue)
{
  m_headerCorrectionFlag=inValue;
}
//=============================================================================
void STTELL1UpdateProcessEngine::setHeaderCorrectionValues(
                                 const HeaderCorrectionVec& inData)
{
  std::memcpy(*m_headerCorrections,&(*inData.begin()),sizeof(HeaderCorrection));

}
//=============================================================================
void STTELL1UpdateProcessEngine::setHeaderThresholdValues(
                                 const HeaderThresholdVec& inData)
{
  std::memcpy(m_headerThresholds,&(*inData.begin()),sizeof(HeaderThreshold));
}
//=============================================================================
void STTELL1UpdateProcessEngine::setPseudoHeaderThresholdValues(
                                 const HeaderThresholdVec& inData)
{
  std::memcpy(m_pseudoHeaderThresholds,&(*inData.begin()),sizeof(HeaderThreshold));
}
//=============================================================================
void STTELL1UpdateProcessEngine::setLinkMask(const LinkVec& inData)
{
  std::memcpy(m_linkMask,&(*inData.begin()),sizeof(LinkMask));
}
//=============================================================================
void STTELL1UpdateProcessEngine::setHeaders(const EngineVec& inData)
{
  m_header=&inData;
}
//=============================================================================
void STTELL1UpdateProcessEngine::setPedestalSum(EngineVec& inVec)
{
  m_pedestalSum=&inVec;
}
//=============================================================================
void STTELL1UpdateProcessEngine::initTableMembers()
{
  std::memset(*m_headerCorrections,0,sizeof(HeaderCorrection));
  std::memset(m_headerThresholds,0,sizeof(HeaderThreshold));
  std::memset(m_pseudoHeaderThresholds,0,sizeof(HeaderThreshold));
  std::memset(*m_linkMask,0,sizeof(LinkMask));
}
//=============================================================================
void STTELL1UpdateProcessEngine::runUpdate()
{
  // prepare data and headers for c-module
  std::memcpy(***m_cModuleData,&(*inData().begin()),sizeof(Data));
  std::memcpy(***m_cModulePedestalSum,&(*m_pedestalSum->begin()),sizeof(Data));
  std::memcpy(***m_cModuleHeader,&(*m_header->begin()),sizeof(Header));

  // run the c-module
  int error = st_pedestal_update(processEnable(), 
                                 m_headerCorrectionFlag, 
                                 m_headerThresholds,
				 m_headerCorrections,
                                 m_cModuleHeader, 
                                 m_linkMask,
                                 m_cModuleData, 
                                 m_cModulePedestalSum,
                                 m_pseudoHeaderThresholds[0],
                                 m_pseudoHeaderThresholds[1]);
  
  // write the subtracted pedestals to out container
  if(!error){    
    std::memcpy(&(*m_pedestalSum->begin()),***m_cModulePedestalSum,sizeof(Data));
  }
}
//--
