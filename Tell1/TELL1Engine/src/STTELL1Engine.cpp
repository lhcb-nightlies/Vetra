// $Id: STTELL1Engine.cpp,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
// Include files

// local
#include "TELL1Engine/STTELL1Engine.h"

//-----------------------------------------------------------------------------
// Implementation file for class : STTELL1Engine
//
// 2007-08-09 : Tomasz Szumlak, Anne Keune
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STTELL1Engine::STTELL1Engine(  ):
  m_processEnable ( 0 ),
  m_inData ( ), 
  m_outData ( ALL_STRIPS, 0 ),
  m_dataSize ( sizeof(Data)/sizeof(int) )
{ }
//=============================================================================
// Destructor
//=============================================================================
STTELL1Engine::~STTELL1Engine() {}
//=============================================================================

