// $Id: TELL1Engine.cpp,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
// Include files

// local
#include "TELL1Engine/TELL1Engine.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TELL1Engine
//
// 2007-08-09 : Tomasz Szumlak
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TELL1Engine::TELL1Engine(  ):
  m_processEnable ( 0 ),
  m_dataSize ( sizeof(VeloTELL1::Data)/sizeof(int) ),
  m_linkPedestalSize ( sizeof(VeloTELL1::LinkPedestal)/sizeof(VeloTELL1::u_int16_t) ),
  m_linkMaskSize ( sizeof(VeloTELL1::LinkMask)/sizeof(VeloTELL1::u_int8_t) ),
  m_inData ( 0 ),
  m_outData ( m_dataSize, 0 )
{ }
//=============================================================================
// Destructor
//=============================================================================
TELL1Engine::~TELL1Engine() {}
//--
