// LIBC
#include <cstring>
 
// STL
#include<vector>
#include<algorithm>

// local
#include "TELL1Engine/TELL1PedestalProcessEngine.h"

// std
#include <iostream>

//-----------------------------------------------------------------------------
// Implementation file for class : TELL1PedestalProcessEngine
//
// 2007-06-14 : Tomasz Szumlak
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TELL1PedestalProcessEngine::TELL1PedestalProcessEngine(  ):
  TELL1Engine(),
  m_scalingMode ( 0 ),
  m_zsEnable ( 0 ),
  m_headerCorrectionFlag ( 0 ),
  m_header ( ),
  m_printDetails ( 0 )
{ 
  this->initTableMembers();
}
//=============================================================================
// Destructor
//=============================================================================
TELL1PedestalProcessEngine::~TELL1PedestalProcessEngine() 
{ }
//=============================================================================
int TELL1PedestalProcessEngine::velo_pedestal_process(int pedestal_enable,
                                int scaling_mode, int zs_enable, 
                                int header_correction_enable,
                                VeloTELL1::HeaderThresholds header_corr_threshold,
                                VeloTELL1::HeaderCorrValues header_corr_value,
                                VeloTELL1::Header header,
                                VeloTELL1::LinkMask link_mask,
                                VeloTELL1::LinkPedestal link_pedestal,
                                VeloTELL1::Data in_data)
{
	int i,j,k,t = 0;
  //  int strip = 0; //< only used for debugging? (MT 5/7/13)
  int err = 0;
	int header_correction;
	int header_zero_threshold,header_one_threshold;

	header_zero_threshold  = header_corr_threshold[0];
	header_one_threshold   = header_corr_threshold[1];
 
   //	printf("\npeds_in :"); for(strip=0;strip<32;strip++) printf(" %3d",in_data[0][0][strip+32]); 
   //	printf("\npedestal used :"); for(strip=0;strip<32;strip++) printf(" %3d",link_pedestal[1][strip]); 

	if(pedestal_enable == 1)
	{
        for(i=0;i<4;i++) //pp
		{
			for(j=0;j<8;j++)//proc channel
			{
				for(k=0;k<64;k++) //strip
				{
					if ((k==0) && (header_correction_enable==1))	{
						if (header[i][j][3] > header_one_threshold) 
							header_correction = header_corr_value[i*16+j*2+0][1]; // one corr;
						else if(header[i][j][3] < header_zero_threshold)
							header_correction = header_corr_value[i*16+j*2+0][0]; // zero corr;
					    else 
                            header_correction = 0;}
					else if ((k==32)	&& (header_correction_enable==1)) 	{
						if (header[i][j][7] > header_one_threshold) 
							header_correction = header_corr_value[i*16+j*2+1][1]; // one corr;
						else if(header[i][j][7] < header_zero_threshold)
							header_correction = header_corr_value[i*16+j*2+1][0]; // zero corr;
						else 
                            header_correction = 0;}
					else
						header_correction = 0;                    
                     
					in_data[i][j][k] = in_data[i][j][k] - header_correction; // header correction
				}
			}
		}
	}
	for(i=0;i<4;i++) //pp
	{
		if(pedestal_enable == 0) continue;
		for(j=0;j<4;j++)//beetle
		{
			for(k=0;k<4;k++) //link
			{
				for(t=0;t<32;t++)
				{
					if((zs_enable==0)||(link_mask[i*16+j*4+k][t]==1))
						  in_data[i][j*2+k/2][32*(k%2)+t] = 0;
					else if(pedestal_enable == 1) 
          {
                      in_data[i][j*2+k/2][32*(k%2)+t] -= (link_pedestal[i*16+j*4+k][t]); // changed to (else if) Guido 7.3.2007
					}
					switch(scaling_mode)
					{
						case 0:
//							printf(RED"\nscaling_mode = 0\n");
							if(in_data[i][j*2+k/2][32*(k%2)+t]>127) in_data[i][j*2+k/2][32*(k%2)+t] =127;
							else if(in_data[i][j*2+k/2][32*(k%2)+t] < -128)in_data[i][j*2+k/2][32*(k%2)+t] = -128;
//							else in_data[i][j][k][4+t] &= 0xFF;
							break;
						case 1:
//							printf(RED"\nscaling_mode = 1\n");
							if(in_data[i][j*2+k/2][32*(k%2)+t]>255) in_data[i][j*2+k/2][32*(k%2)+t] =127;
							else if(in_data[i][j*2+k/2][32*(k%2)+t] < -256)in_data[i][j*2+k/2][32*(k%2)+t] = -128;
							else in_data[i][j*2+k/2][32*(k%2)+t] = (in_data[i][j*2+k/2][32*(k%2)+t]>>1);
							break;
						
						case 2:
//							printf(RED"\nscaling_mode = 2\n");
							if(in_data[i][j*2+k/2][32*(k%2)+t]>511) in_data[i][j*2+k/2][32*(k%2)+t] =127;
							else if(in_data[i][j*2+k/2][32*(k%2)+t] < -512)in_data[i][j*2+k/2][32*(k%2)+t] = -128;
							else in_data[i][j*2+k/2][32*(k%2)+t] = (in_data[i][j*2+k/2][32*(k%2)+t]>>2);
							break;
						
						default:
							in_data[i][j*2+k/2][32*(k%2)+t] = 0;
							break;
					} //switch
				}//for 32 strip
			}//for 4 link
		}//for 4 beetle
  } //for 4 pp



/* 				for(t=0;t<32;t++) */
/*                    printf("ped_sub_data=%4d pedestal=%4d\n",in_data[0][0][t],link_pedestal_sum[0][t]); */

  // 	printf("\npeds_out:"); for(strip=0;strip<32;strip++) printf("%3d ",in_data[0][0][strip+32]); 

	return(err);
}
//=============================================================================
void TELL1PedestalProcessEngine::setScalingMode(const int inValue)
{
  m_scalingMode=inValue;
}
//=============================================================================
void TELL1PedestalProcessEngine::setZSEnable(const int inValue)
{
  m_zsEnable=inValue;
}
//=============================================================================
void TELL1PedestalProcessEngine::setHeaderCorrectionFlag(const int inValue)
{
  m_headerCorrectionFlag=inValue;
}
//=============================================================================
/*void TELL1PedestalProcessEngine::setHeaderCorrectionValues(
                                 const HeaderStrip0Vec& inData)
{
  std::memcpy(m_headerCorrectionValueStrip0,&(*inData.begin()),sizeof(HeaderStrip0));
  
}*/
//=============================================================================
void TELL1PedestalProcessEngine::setHeaders(const VeloTELL1::EngineVec& inData)
{
  m_header=inData;
}
//=============================================================================
void TELL1PedestalProcessEngine::setLinkPedestal(
                                 const VeloTELL1::LinkPedestalVec& inData)
{
  std::memcpy(*m_linkPedestal,&(*inData.begin()),
              sizeof(VeloTELL1::LinkPedestal));
}
//=============================================================================
void TELL1PedestalProcessEngine::setLinkMask(const VeloTELL1::LinkMaskVec& inData)
{
  std::memcpy(*m_linkMask,&(*inData.begin()),
              sizeof(VeloTELL1::LinkMask));
}
//=============================================================================
void TELL1PedestalProcessEngine::setHeaderThresholds(
                                 const VeloTELL1::HeaderThresholdsVec& inData)
{
  std::memcpy(m_headerThresholds, &(*inData.begin()),
              sizeof(VeloTELL1::HeaderThresholds));
}
//=============================================================================
void TELL1PedestalProcessEngine::setHeaderCorrValues(
                                 const VeloTELL1::HeaderCorrValuesVec& inData)
{
  std::memcpy(*m_headerCorrValues, &(*inData.begin()),
              sizeof(VeloTELL1::HeaderCorrValues));
}
//=============================================================================
void TELL1PedestalProcessEngine::initTableMembers()
{
  //std::memset(m_headerCorrectionValueStrip0,0,sizeof(HeaderStrip0));
  std::memset(m_headerThresholds, 0, sizeof(VeloTELL1::HeaderThresholds));
  std::memset(*m_headerCorrValues, 0, sizeof(VeloTELL1::HeaderCorrValues));
  std::memset(*m_linkPedestal,0,sizeof(VeloTELL1::LinkPedestal));
  std::memset(*m_linkMask,0,sizeof(VeloTELL1::LinkMask)); 
}
//=============================================================================
/*void TELL1PedestalProcessEngine::setWhich2Run(const int inValue)
{
  m_which2Run=inValue;
}*/
//=============================================================================
void TELL1PedestalProcessEngine::setPrintDetails(const int inValue)
{
  m_printDetails=inValue;
}
//=============================================================================
void TELL1PedestalProcessEngine::state(const int ppfpga,
                                       const int procChan) const
{
  std::cout<< " ---------------------------------------------------- ";
  std::cout<<std::endl;
  std::cout<< " --> Printing state of the pedestal process engine -- ";
  std::cout<<std::endl;
  std::cout<< " ---------------------------------------------------- ";
  std::cout<<std::endl;
  std::cout<< " - Print details: " << m_printDetails <<std::endl;
  std::cout<< " - Process enable flag: " << processEnable() <<std::endl;
  std::cout<< " - Scaling mode: " << m_scalingMode <<std::endl;
  std::cout<< " - Zero suppression enable flag: " << m_zsEnable <<std::endl;
  std::cout<< " - Header corr flag: " << m_headerCorrectionFlag <<std::endl;
  std::cout<< " - Header Thresholds: "
           << " " << m_headerThresholds[0] << ", "
           << m_headerThresholds[1] <<std::endl;
  std::cout<< " - Header corr values: " <<std::endl;
  int aLinkLocation=2*(ppfpga*VeloTELL1::REAL_ALINKS+procChan);
  if(ppfpga<3&&procChan<7){
    std::cout<< " ";
    for(int chan=0; chan<VeloTELL1::HEADER_STATES; ++chan){
      std::cout<< m_headerCorrValues[aLinkLocation][chan];
      std::cout<< ", ";
    }
    std::cout<<std::endl;
    std::cout<< " ";
    for(int chan=0; chan<VeloTELL1::HEADER_STATES; ++chan){
      std::cout<< m_headerCorrValues[aLinkLocation+1][chan];
      std::cout<< ", ";
    }
    std::cout<<std::endl;
  }else{
   std::cout<< " Wrong processor or processing channel value! ";
   std::cout<<std::endl;
  } 
  std::cout<< " - Header data for ppfpga: " << ppfpga;
  std::cout<< ", and processing channel: " << procChan <<std::endl;
  if(ppfpga<3&&procChan<7){
    std::cout<< " ";
    for(int chan=0; chan<VeloTELL1::HEADERS_PER_ALINK; ++chan){
      std::cout<< m_cModuleHeader[ppfpga][procChan][chan];
      std::cout<< ", ";
    }
    std::cout<<std::endl;
  }else{
    std::cout<< " Wrong processor or processing channel value! ";
    std::cout<<std::endl;
  }
  std::cout<< " - Masks for ALink: " << aLinkLocation <<std::endl;
  if(procChan<8){
    std::cout<< " ALink I (0-31) " <<std::endl;
    std::cout<< " ";
    for(int chan=0; chan<VeloTELL1::CHANNELS_PER_ALINK; ++chan){
      std::cout<< static_cast<int>(m_linkMask[aLinkLocation][chan]);
      std::cout<< ", ";
    }
    std::cout<<std::endl;
    std::cout<< " ALink I (32-63) " <<std::endl;
    std::cout<< " ";
    for(int chan=0; chan<VeloTELL1::CHANNELS_PER_ALINK; ++chan){
      std::cout<< static_cast<int>(m_linkMask[aLinkLocation+1][chan]);
      std::cout<< ", ";
    }
    std::cout<<std::endl;
  }else{
    std::cout<< " Bad processing channel value! " <<std::endl;
  }
  std::cout<< " -------------------------------- ";
  std::cout<<std::endl;
  std::cout<< " --> End of the state printout -- ";
  std::cout<<std::endl;
  std::cout<< " -------------------------------- ";
  std::cout<<std::endl;
}
//=============================================================================
void TELL1PedestalProcessEngine::ioStreams(const int ppfpga,
                                           const int procChan) const
{
  std::cout<< " ---------------------------------------------------- ";
  std::cout<<std::endl;
  std::cout<< " --> Printing I/O data streams -- ";
  std::cout<<std::endl;
  std::cout<< " ---------------------------------------------------- ";
  std::cout<<std::endl;
  std::cout<< " - Pedestal data for ppfpga: " << ppfpga
           << ", and procChan: " << procChan <<std::endl;
  int aLinkLocation=2*(ppfpga*VeloTELL1::REAL_ALINKS+procChan);
  std::cout<< " Pedestal Link I " <<std::endl;
  for(int chan=0; chan<VeloTELL1::CHANNELS_PER_ALINK; ++chan){
    std::cout<< m_linkPedestal[aLinkLocation][chan];
    std::cout<< ", ";
    if(chan%16==15&&chan!=0) std::cout<<std::endl;
  }
  std::cout<< " Pedestal Link II " <<std::endl;
  for(int chan=0; chan<VeloTELL1::CHANNELS_PER_ALINK; ++chan){
    std::cout<< m_linkPedestal[aLinkLocation+1][chan];
    std::cout<< ", ";
    if(chan%16==15&&chan!=0) std::cout<<std::endl;
  }  
  std::cout<< " - Data for ppfpga: " << ppfpga
           << ", and procChan: " << procChan <<std::endl;
  for(int chan=0; chan<VeloTELL1::ALINKS; ++chan){
    std::cout<< m_cModuleData[ppfpga][procChan][chan];
    std::cout<< ", ";
    if(chan%16==15&&chan!=0) std::cout<<std::endl;
  }
  std::cout<<std::endl;  
  std::cout<< " -------------------------------- ";
  std::cout<<std::endl;
  std::cout<< " --> End of the I/O printout -- ";
  std::cout<<std::endl;
  std::cout<< " -------------------------------- ";
  std::cout<<std::endl;
}
//=============================================================================
void TELL1PedestalProcessEngine::runSubtraction()
{
  // prepare data and headers for c-module
  std::memcpy((**m_cModuleData),&(*inData().begin()),
              sizeof(VeloTELL1::Data));
  std::memcpy((**m_cModuleHeader),&(*m_header.begin()),
              sizeof(VeloTELL1::Header));  
  // run the c-module
  int error=0;
  if(m_printDetails) {
    std::cout<< " --> Before subtraction -- " <<std::endl;
    this->ioStreams();
  }
  /*
  if(m_which2Run==0){
    error=velo_pedestal_process(processEnable(),
                                  m_scalingMode,
                                  m_zsEnable, 
                                  m_headerCorrectionFlag,
                                  m_headerCorrectionValueStrip0,
                                  m_cModuleHeader,
                                  m_linkMask,
                                  m_linkPedestal,
                                  m_cModuleData);
  }else if(m_which2Run==1){
  */
    error=velo_pedestal_process(processEnable(),
                                  m_scalingMode,
                                  m_zsEnable, 
                                  m_headerCorrectionFlag,
                                  m_headerThresholds,
                                  m_headerCorrValues,
                                  m_cModuleHeader,
                                  m_linkMask,
                                  m_linkPedestal,
                                  m_cModuleData);
    //}
  if(m_printDetails){
    std::cout<< " --> After subtraction -- " <<std::endl;
    this->ioStreams();
  }
  // write the subtracted pedestals to out container
  if(!error&&dataSize()==outData().size()){  
    std::memcpy(&(*outData().begin()),(**m_cModuleData),
                sizeof(VeloTELL1::Data));
  }else if(error){
    std::cout<< " --> Pedestal Process Module Failed! " <<std::endl;
  }
}
//--
