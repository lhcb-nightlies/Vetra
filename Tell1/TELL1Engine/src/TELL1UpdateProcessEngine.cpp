// $Id: TELL1UpdateProcessEngine.cpp,v 1.3 2008-10-17 16:43:01 szumlat Exp $
// Include files 

#include <cstring>

// STL
#include<vector>
#include<algorithm>

// local
#include "TELL1Engine/TELL1UpdateProcessEngine.h"

// std
#include <iostream>

using namespace VeloTELL1;

//-----------------------------------------------------------------------------
// Implementation file for class : TELL1UpdateProcessEngine
//
// 2007-08-10 : Tomasz Szumlak
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TELL1UpdateProcessEngine::TELL1UpdateProcessEngine(  ):
  TELL1Engine(),
  m_headerCorrectionFlag ( 0 ),
  m_header ( ),
  m_pedestalSum ( 0 )
{
  this->initTableMembers();
}
//=============================================================================
// Destructor
//=============================================================================
TELL1UpdateProcessEngine::~TELL1UpdateProcessEngine() {}
//=============================================================================
int TELL1UpdateProcessEngine::velo_pedestal_update(
                              int pedestal_auto_update_enable,
                              int header_correction_enable,
                              VeloTELL1::HeaderThresholds header_corr_threshold,
                              VeloTELL1::HeaderCorrValues header_corr_value,
                              VeloTELL1::LinkMask link_mask,
                              VeloTELL1::Header header,
                              VeloTELL1::Data in_data,
                              VeloTELL1::PedSum self_maintain_pedestal_sum)
{
	int i,j,k,t,header_correction,err = 0;
	int header_zero_threshold,header_one_threshold;
  int data_sub_l[32];

	header_zero_threshold  = header_corr_threshold[0];
	header_one_threshold   = header_corr_threshold[1];

	if (pedestal_auto_update_enable==1)		{
		for(i=0;i<4;i++) //pp
		{
			for(j=0;j<4;j++)//beetle
			{
				for(k=0;k<4;k++)//link
				{
					for(t=0;t<32;t++) //strip
					{ 
						header_correction = 0;
						if ((t==0) && (header_correction_enable==1))	
					  {
										if     (header[i][j*2+k/2][(k%2)*4+3] > header_one_threshold) 
										   header_correction = header_corr_value[i*16+j*4+k][1]; // one corr;
								  	else if(header[i][j*2+k/2][(k%2)*4+3] < header_zero_threshold)
									     header_correction = header_corr_value[i*16+j*4+k][0]; // zero corr;
								    else 
			                 header_correction = 0;

            }
						
	
						data_sub_l[k] = in_data[i][j*2+k/2][(k%2)*32+t]-(self_maintain_pedestal_sum[i][j][k][t]>>10) - header_correction; // header correction
						
						if(link_mask[i*16+j*4+k][t]==1)
							self_maintain_pedestal_sum[i][j][k][t]=self_maintain_pedestal_sum[i][j][k][t]; 
						else if (data_sub_l[k] > 15) 
							self_maintain_pedestal_sum[i][j][k][t]=self_maintain_pedestal_sum[i][j][k][t]+15;  // saturated update
						else if (data_sub_l[k] < -15) 
							self_maintain_pedestal_sum[i][j][k][t]=self_maintain_pedestal_sum[i][j][k][t]-15;  // saturated update
						else
							self_maintain_pedestal_sum[i][j][k][t]=self_maintain_pedestal_sum[i][j][k][t]+data_sub_l[k]; 

	   			}//t
	   			
	   		/*	if((i==0)&&(j==0)&&(k==1))
				  {
	   			  printf("\npedestal maint:");
	   			  for(t=0;t<32;t++) printf(" %3d",self_maintain_pedestal_sum[i][j][k][t]>>10);
	   			}
	   			*/
	   			
				}//k
			}//j
		}//i
          
	}
	
	return(err);
}
//=============================================================================
void TELL1UpdateProcessEngine::setHeaderCorrectionFlag(const int inValue)
{
  m_headerCorrectionFlag=inValue;
}
//=============================================================================
/*void TELL1UpdateProcessEngine::setHeaderCorrectionValues(
                                 const HeaderStrip0 inData)
{
  std::memcpy(m_headerCorrectionValueStrip0,inData,sizeof(HeaderStrip0));
}*/
//=============================================================================
void TELL1UpdateProcessEngine::setHeaders(const VeloTELL1::EngineVec& inData)
{
  m_header=&inData;
}
//=============================================================================
void TELL1UpdateProcessEngine::setPedestalSum(VeloTELL1::EngineVec& inVec)
{
  m_pedestalSum=&inVec;
}
//=============================================================================
void TELL1UpdateProcessEngine::setLinkMask(const VeloTELL1::LinkMaskVec& inData)
{
  std::memcpy(*m_linkMask,&(*inData.begin()),sizeof(VeloTELL1::LinkMask));
}
//=============================================================================
void TELL1UpdateProcessEngine::setHeaderThresholds(
                                 const VeloTELL1::HeaderThresholdsVec& inData)
{
  std::memcpy(m_headerThresholds, &(*inData.begin()),
              sizeof(VeloTELL1::HeaderThresholds));
}
//=============================================================================
void TELL1UpdateProcessEngine::setHeaderCorrValues(
                                 const VeloTELL1::HeaderCorrValuesVec& inData)
{
  std::memcpy(*m_headerCorrValues, &(*inData.begin()),
              sizeof(VeloTELL1::HeaderCorrValues));
}
//=============================================================================
void TELL1UpdateProcessEngine::initTableMembers()
{
  std::memset(m_headerThresholds, 0, sizeof(VeloTELL1::HeaderThresholds));
  std::memset(*m_headerCorrValues, 0, sizeof(VeloTELL1::HeaderCorrValues));
  std::memset(*m_linkMask,0,sizeof(VeloTELL1::LinkMask));
}
//=============================================================================
/*void TELL1UpdateProcessEngine::setWhich2Run(const int inValue)
{
  m_which2Run=inValue;
}*/
//=============================================================================
void TELL1UpdateProcessEngine::runUpdate()
{
  // prepare data and headers for c-module
  std::memcpy(**m_cModuleData,&(*inData().begin()),sizeof(Data));
  std::memcpy(**m_cModulePedestalSum,&(*m_pedestalSum->begin()),
              sizeof(VeloTELL1::PedSum));
  std::memcpy(**m_cModuleHeader,&(*m_header->begin()),
              sizeof(VeloTELL1::Header));

  // run the c-module
  int error=0;
  error=velo_pedestal_update(processEnable(),
                             m_headerCorrectionFlag,
                             m_headerThresholds,
                             m_headerCorrValues,
                             m_linkMask,
                             m_cModuleHeader,
                             m_cModuleData,
                             m_cModulePedestalSum);

  // write the subtracted pedestals to out container
  if(!error){
    std::memcpy(&(*m_pedestalSum->begin()),**m_cModulePedestalSum,
                sizeof(VeloTELL1::PedSum));
  }else if(error){
    std::cout<< " --> Update Pedestal Module FAILED! " <<std::endl;
  }
}
//--
