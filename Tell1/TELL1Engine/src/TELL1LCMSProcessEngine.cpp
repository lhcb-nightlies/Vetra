// $Id: TELL1LCMSProcessEngine.cpp,v 1.2 2008-10-17 16:43:01 szumlat Exp $
// Include files 
#include <cstring>

// STL
#include<vector>
#include<algorithm>

// local
#include "TELL1Engine/TELL1LCMSProcessEngine.h"

// std
#include <iostream>

// message printing - defined it this way to comply
// with c-module
// Commented out DEH 29/8/2014: not used in live code (macro causes warnings)
//#define DEBUG_LCMS 0
//#if !defined(_WIN32) && DEBUG_LCMS
//  #define PRINTF_DEBUG(fmt, arg...)   printf(fmt, ##arg)
//#elif !defined(_WIN32) && !DEBUG_LCMS
//  #define PRINTF_DEBUG(fmt, arg...)   
//#elif defined(_WIN32) && DEBUG_LCMS
//  #define PRINTF_DEBUG(fmt, __VA_ARGS__)   printf(fmt, __VA_ARGS__)
//#elif defined(_WIN32) && !DEBUG_LCMS
//  #define PRINTF_DEBUG(fmt, __VA_ARGS__)   
//#endif

//-----------------------------------------------------------------------------
// Implementation file for class : TELL1LCMSProcessEngine
//
// 2007-08-12 : Tomasz Szumlak
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TELL1LCMSProcessEngine::TELL1LCMSProcessEngine(  ):
  TELL1Engine()
{ }
//=============================================================================
// Destructor
//=============================================================================
TELL1LCMSProcessEngine::~TELL1LCMSProcessEngine() {}
//=============================================================================
int TELL1LCMSProcessEngine::velo_lcms_process(int cm_enable,
                                              VeloTELL1::Data in_data)
{
	int err = 0;
	int i, j, k, t, strip;
	
	long int mean_format;
	int  mean_temp;
	long int  slope_format;
	int  slope_temp;
	int data_without_hit[32], data_with_hit[32];
	
	long int variance_format;
	long int variance_temp;
	long int square;
	int F1 = 3;
	

	
	for(i=0; i<4; i++) //pp
	{
    if(cm_enable==0) continue;
		for(j=0;j<9;j++) //channel
		{
			for(k=0;k<2;k++) //link
			{ 
				 
				for(t=0;t<32;t++)			data_with_hit[t] = in_data[i][j][k*32+t]; 
				
				//if( (i==0)&&(j==0)&&(k==0)) { printf("\nlcms in     :");for(strip= 0;strip<32;strip++) printf("%4d ",data_with_hit[strip]); }
	     
				//-----------first iteration----------------------
  			//------------------ average
   			mean_format = 0x10;                     //sum_1
				for(strip=0;strip<32;strip++)		mean_format += data_with_hit[strip]; //sum_1
				mean_temp = mean_format>>5;   //average
				for(strip=0;strip<32;strip++)     data_with_hit[strip] -= mean_temp; 
				saturate_for_8bits(data_with_hit,32);  //saturate to -128~127
        
        //if( (i==0)&&(j==0)&&(k==0)) { printf("\n1st ave %d :",mean_temp);for(strip= 0;strip<32;strip++) printf("%4d ",data_with_hit[strip]); }
	     
				//------------------ slope
				slope_format = 0x20;
				for(strip=0;strip<32;strip++)		slope_format += (strip-16)*data_with_hit[strip];
				slope_temp = slope_format>>6;
				if(slope_temp >127) slope_temp =127;
				else if(slope_temp <-128) slope_temp = -128;				
				for(strip=0;strip<32;strip++)	  data_with_hit[strip] -= ((slope_temp*(strip-16)*3)+64)>>7; // add 64 for symmetric rounding Guido 8.1.2008
  			saturate_for_8bits(data_with_hit,32);  //saturate to -128~127
	      
	      //if( (i==0)&&(j==0)&&(k==0)) { printf("\n1st slp %d :",slope_temp);for(strip= 0;strip<32;strip++) printf("%4d ",data_with_hit[strip]); }
	     
				//---------------------variance
				variance_format = 0x10;
				for(t=0;t<32;t++)
				{     square = data_with_hit[t]*data_with_hit[t];
							variance_format += square;
			  }
			  variance_temp = variance_format>>5; 
				for(t=0;t<32;t++)
				{ 
					if((data_with_hit[t]*data_with_hit[t]*F1) > variance_temp) 
					  data_without_hit[t]=0; 
					else 
						data_without_hit[t]=data_with_hit[t]; 
				}
				//if( (i==0)&&(j==0)&&(k==0)) { printf("\n1st var %ld :",variance_temp);for(strip= 0;strip<32;strip++) printf("%4d ",data_without_hit[strip]); }
	     
				
			//------------second iteration---------
  			//--------------- average
   			mean_format = 0x10;                     //sum_1
				for(t=0;t<32;t++)					mean_format += data_without_hit[t]; //sum_1
				mean_temp = mean_format>>5;   //average
				for(t=0;t<32;t++)
				{ 
					data_with_hit[t]    -= mean_temp; 
					data_without_hit[t] -= mean_temp; 
				}
				saturate_for_8bits(data_with_hit,32);  //saturate to -128~127
				saturate_for_8bits(data_without_hit,32);  //saturate to -128~127
		    if((i==0)&&(k==0))
        {
			     //printf("\nmean_swh %d =",j); for(strip=0;strip<32;strip++) printf("%3d ",data_with_hit[strip]);
           //printf("\nmean_snh="); for(strip=0;strip<32;strip++) printf("%3d ",data_without_hit[strip]);
        }

				//---------------- slope
				slope_format = 0x20;
				for(t=0;t<32;t++)   slope_format += (t-16)*data_without_hit[t];
				slope_temp = slope_format>>6;
				if(slope_temp >127) slope_temp =127;
				else if(slope_temp <-128) slope_temp = -128;

//     if((i==0)&&(j==0)&&(k==0)) printf("\nslope ="); printf("%3d ",slope_temp); 

				for(t=0;t<32;t++)	  data_with_hit[t] -= ((slope_temp*(t-16)*3)+64)>>7; // add 64 for symmetric rounding Guido 8.1.2008
				saturate_for_8bits(data_with_hit,32);  //saturate to -128~127
				//-----------------------------over
				for(t=0;t<32;t++)  	in_data[i][j][k*32+t] = data_with_hit[t]; 

   	    //if((i==0)&&(j==0)&&(k==0))  {  printf("\nlcms out: "); for(strip=0;strip<32;strip++) printf("%3d ",data_with_hit[strip]);  printf("\n");   }
 
			}
	  }//channel
	}//pp

	return(err);
}
//=============================================================================
void TELL1LCMSProcessEngine::saturate_for_8bits(int *data, int line_numb)
{
  int i;
  for(i=0;i<line_numb;i++)
  {
    if(data[i]>127) data[i] =127;
    else if(data[i] < -128) data[i] = -128;
  }
}
//=============================================================================
void TELL1LCMSProcessEngine::runLCMS()
{
  // prepare data for c-module
  std::memcpy(**m_cModuleData,&(*inData().begin()),sizeof(VeloTELL1::Data));

  // run the reorder c-module
  int error=velo_lcms_process(processEnable(),
                              m_cModuleData);

  // write reordered data
  if(!error){    
    std::memcpy(&(*outData().begin()),(**m_cModuleData),sizeof(VeloTELL1::Data));
  }else if(error){
    std::cout<< " --> LCMS Module FAILED! " <<std::endl;
  }
}
//--

