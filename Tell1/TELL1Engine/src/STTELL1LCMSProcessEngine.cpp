// $Id: STTELL1LCMSProcessEngine.cpp,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
// Include files 
// STL
#include<vector>
#include<algorithm>

// local
#include "TELL1Engine/STTELL1LCMSProcessEngine.h"

// std
#include <iostream>

// message printing - defined it this way to comply
// with c-module
// Commented out DEH 29/8/2014: not used in live code (macro causes warnings)
//#define DEBUG_LCMS 1
//#if !defined(_WIN32) && DEBUG_LCMS
//  #define PRINTF_DEBUG(fmt, arg...)   printf(fmt, ##arg)
//#elif !defined(_WIN32) && !DEBUG_LCMS
//  #define PRINTF_DEBUG(fmt, arg...)   
//#elif defined(_WIN32) && DEBUG_LCMS
//  #define PRINTF_DEBUG(fmt, __VA_ARGS__)   printf(fmt, __VA_ARGS__)
//#elif defined(_WIN32) && !DEBUG_LCMS
//  #define PRINTF_DEBUG(fmt, __VA_ARGS__)   
//#endif

//-----------------------------------------------------------------------------
// Implementation file for class : STTELL1LCMSProcessEngine
//
// 2007-08-12 : Tomasz Szumlak, Anne Keune
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STTELL1LCMSProcessEngine::STTELL1LCMSProcessEngine(  ):
  STTELL1Engine()
{
  this->initTableMembers();
}
//=============================================================================
// Destructor
//=============================================================================
STTELL1LCMSProcessEngine::~STTELL1LCMSProcessEngine() {}
//=============================================================================
void STTELL1LCMSProcessEngine::initTableMembers()
{
  std::memset(*m_cmsThreshold,0,sizeof(CMSThreshold));
}
//=============================================================================
int STTELL1LCMSProcessEngine::st_lcms_process(int cm_enable, CMSThreshold cms_threshold, Data in_data)  
{
  int err = 0;
  int  ppx,beetle,link,strip;
  
  long int  mean_format;
  int       mean_temp;
  long int  slope_format;
  int       slope_temp;
  int       data_without_hit[32], data_with_hit[32];
  
  for(ppx=0; ppx<4; ppx++) //pp
    {
      
      if(cm_enable==0) continue;
      
      for(beetle=0;beetle<6;beetle++) //beetle
	{
	  for(link=0;link<4;link++) //link
	    { 
	      for(strip=0;strip<32;strip++)
		{
		  data_with_hit[strip] = in_data[ppx][beetle][link][strip]; 
		}
	      
	      //--------------------------------------first iteration
	      
	      mean_format = 0x10;    
	      
	      for(strip=0;strip<32;strip++)
		{	mean_format += data_with_hit[strip]; //sum_1
		}
	      mean_temp = mean_format>>5;   //average

	      for(strip=0;strip<32;strip++)
		{
		  data_without_hit[strip] = data_with_hit[strip] - mean_temp; 
		  data_with_hit[strip]    = data_with_hit[strip] - mean_temp; 	
		}
	      saturate_for_8bits(data_without_hit,32);  //saturate to -128~127
	      saturate_for_8bits(data_with_hit,32);  //saturate to -128~127
	      
	      if(abs(data_with_hit[0])>cms_threshold[ppx*24+beetle*4+link][0])
		{
		  data_without_hit[0] = 0; 
		  data_without_hit[1] = 0; 
		}
	      for(strip=1;strip<31;strip++)
		{
		  if(abs(data_with_hit[strip])>cms_threshold[ppx*24+beetle*4+link][strip])
		    {
		      data_without_hit[strip-1] = 0; 
		      data_without_hit[strip] = 0; 
		      data_without_hit[strip+1] = 0; 
		    }
		}
	      if(abs(data_with_hit[31])>cms_threshold[ppx*24+beetle*4+link][31])
		{
		  data_without_hit[30] = 0; 
		  data_without_hit[31] = 0; 

		}
	   	      
	      //--------------------------------------second iteration
	      //------------------------------------- average
	      
	      mean_format = 0x10;                     //sum_1
	      for(strip=0;strip<32;strip++)
		{	mean_format += data_without_hit[strip]; //sum_1
		}
	      mean_temp = mean_format>>5;   //average

	     	      
	      for(strip=0;strip<32;strip++)
		{ 
		  data_with_hit[strip] -= mean_temp; 
		  data_without_hit[strip] -= mean_temp; 
		}
	      saturate_for_8bits(data_with_hit,32);  //saturate to -128~127
	      saturate_for_8bits(data_without_hit,32);  //saturate to -128~127
	      
	    	      
	      //------------------------------------- slope
	      
	      
	      slope_format = 0x20;
	      for(strip=0;strip<32;strip++)
		{
		  slope_format += (strip-16)*data_without_hit[strip];
		}
	  
	      slope_temp = slope_format>>6;

	      if(slope_temp >127) slope_temp =127;
	      else if(slope_temp <-128) slope_temp = -128;
	  
	      for(strip=0;strip<32;strip++)
		{ 
		  data_with_hit[strip] -= ((slope_temp*(strip-16)*3)+64)>>7; 
		}
	      saturate_for_8bits(data_with_hit,32);  //saturate to -128~127
	  
	      
	      //------------------------------------------over
	      for(strip=0;strip<32;strip++)
		{	in_data[ppx][beetle][link][strip] = data_with_hit[strip]; 
		}
	      
	    }
	}
    }
  return(err);
}

//=============================================================================
void STTELL1LCMSProcessEngine::saturate_for_8bits(int *data, int line_numb)
{
	int i;
	for(i=0;i<line_numb;i++)
	{
		if(data[i]>127) data[i] =127;
		else if(data[i] < -128)data[i] = -128;
	}
}
//=============================================================================
void STTELL1LCMSProcessEngine::setCMSThreshold(const CMSThresholdVec& inData)
{
  std::memcpy(*m_cmsThreshold,&(*inData.begin()),sizeof(CMSThreshold));
}
//=============================================================================
void STTELL1LCMSProcessEngine::runLCMS()
{
  // prepare data for c-module
    std::memcpy(***m_cModuleData,&(*inData().begin()),sizeof(Data));

  //run the reorder c-module
  int error=st_lcms_process(processEnable(),
                          m_cmsThreshold,
                          m_cModuleData);
 
  // write reordered data
  if(!error){    
    std::memcpy(&(*outData().begin()),***m_cModuleData,sizeof(Data));
  }
}
//--

