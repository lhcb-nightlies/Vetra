// $Id: STTELL1ZSProcessEngine.cpp,v 1.3 2009-08-10 09:36:55 akeune Exp $
// Include files 
#include<vector>
#include<algorithm>

// local
#include "TELL1Engine/STTELL1ZSProcessEngine.h"

// std
#include <iostream>

//-----------------------------------------------------------------------------
// Implementation file for class : STTELL1ZSProcessEngine
//
// 2007-08-12 : Tomasz Szumlak, Anne Keune
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STTELL1ZSProcessEngine::STTELL1ZSProcessEngine(  ):
  STTELL1Engine(),
  m_producedClusters ( 0 ),
  m_includedADCS ( 0 ),
  m_rawClusters (  ),
  m_rawADCS (  )
{
  this->initTableMembers();
}
//=============================================================================
// Destructor
//=============================================================================
STTELL1ZSProcessEngine::~STTELL1ZSProcessEngine() {}
//=============================================================================
void STTELL1ZSProcessEngine::initTableMembers()
{
  std::memset(*m_hitThresholds,0,sizeof(HitThreshold));
  std::memset(m_confirmationThresholds,0,sizeof(ConfirmationThreshold));
  std::memset(m_spillOverThresholds,0,sizeof(SpillOverThreshold));
  std::memset(m_boundaryStrips,0,sizeof(BoundaryStrip));
  std::memset(m_clusters,0,sizeof(TELL1Cluster));
  std::memset(m_adcs,0,sizeof(TELL1ADC));
}
//=============================================================================
int STTELL1ZSProcessEngine::st_zs_process(int zs_enable,
                                          HitThreshold hit_threshold,
                                          ConfirmationThreshold confirmation_threshold, 
                                          SpillOverThreshold spill_over_threshold,
                                          BoundaryStrip strip_start_temp,
                                          Data in_data,
                                          STTELL1::u_int32_t pp_max_clusters,
                                          STTELL1::u_int16_t *cluster, 
                                          int *clus_numb,
                                          STTELL1::u_int8_t *adc_list, 
                                          int *adc_numb)  //beetle, link, strip
{
  int err = 0;
  int ppx,beetle;
  //int strip;
  //int alink;  // links enumerated from 0 to 95   (data structured in only links and strips)
  int pstrip; // processor strips run from 0 to 68, it inlcludes left and right neighours to 2x32 strips
  int boundary_strip = 0;
  int zs_line[69];
  int zs_h_line[69];        //4 left +64 +1 right
  int pp_clus_cnt, pp_adc_cnt;
  
  //printf(RED"\nEnter ZS ...\n");

  (*clus_numb)= 0;
  (*adc_numb) =0;
  
  for(ppx=0; ppx<4; ppx++) {        //4 pp
 
      if(zs_enable==0) continue;
      
      pp_clus_cnt = 0;
      pp_adc_cnt = 0;
      
      for(beetle=0;beetle<6;beetle++) {        //6beetle
		
	//channel 1   substract high threshold 
	for(pstrip=0;pstrip<4;pstrip++) {     //channel 1
	  
	  zs_line[pstrip]=0; zs_h_line[pstrip] = -1;
          
	}	    
	for(pstrip=0;pstrip<64;pstrip++) {
	  
	  zs_h_line[pstrip+4] = in_data[ppx][beetle][pstrip/32][pstrip%32] - hit_threshold[ppx*24+beetle*4+pstrip/32][pstrip%32]; 
	  zs_line[pstrip+4]   = in_data[ppx][beetle][pstrip/32][pstrip%32];
	}
	zs_line[68]   = in_data[ppx][beetle][2][0];

  if(zs_h_line[67]>0&&zs_h_line[66]>0&&zs_h_line[65]>0&&zs_h_line[64]>0) zs_h_line[68] = 0;
	else zs_h_line[68] = in_data[ppx][beetle][2][0] - hit_threshold[ppx*24+beetle*4+2][0];
	
	boundary_strip = 0;
	err |= st_clusterization_process(zs_line,zs_h_line, cluster, adc_list, 
					 &pp_clus_cnt, &pp_adc_cnt,pp_max_clusters, 
					 confirmation_threshold[ppx*12+beetle*2], spill_over_threshold[ppx*12+beetle*2],
					 strip_start_temp[ppx*12+beetle*2],boundary_strip);
	
	
	if(pp_clus_cnt == (int)pp_max_clusters) break; //made it int
	
	//channel 2  substract high threshold 
	for(pstrip=0;pstrip<4;pstrip++) {     //channel 2
	  
	  zs_line[pstrip] = in_data[ppx][beetle][1][28+pstrip];
	  zs_h_line[pstrip] = in_data[ppx][beetle][1][28+pstrip] - hit_threshold[ppx*24+beetle*4+1][28+pstrip];
	}	    
	for(pstrip=0;pstrip<64;pstrip++) {
	  
	  zs_line[pstrip+4] = in_data[ppx][beetle][pstrip/32+2][pstrip%32];
	  zs_h_line[pstrip+4] = in_data[ppx][beetle][pstrip/32+2][pstrip%32] - hit_threshold[ppx*24+beetle*4+pstrip/32+2][pstrip%32]; 
	}
	zs_line[68] = 0;
	zs_h_line[68] = -1;
	boundary_strip = 1;
	
	err |= st_clusterization_process(zs_line,zs_h_line, cluster, adc_list, &pp_clus_cnt, &pp_adc_cnt,pp_max_clusters,  
					 confirmation_threshold[ppx*12+beetle*2+1], spill_over_threshold[ppx*12+beetle*2+1],
					 strip_start_temp[ppx*12+beetle*2+1],boundary_strip);
	
	if(pp_clus_cnt == (int)pp_max_clusters)   break;  //made it int
	
      }//6beetle
      cluster = cluster+pp_clus_cnt;
      adc_list = adc_list+pp_adc_cnt;
      (*clus_numb) += pp_clus_cnt;
      (*adc_numb) += pp_adc_cnt;
  }//4 pp
  
  return(err);
}


//=============================================================================
int STTELL1ZSProcessEngine::st_clusterization_process(int *zs_line,
                                                      int *zs_h_line, 
                                                      STTELL1::u_int16_t *cluster, 
                                                      STTELL1::u_int8_t *adc_list, 
                                                      int *pp_clus_cnt, 
                                                      int *pp_adc_cnt,
                                                      STTELL1::u_int32_t pp_max_clusters,
                                                      int one_confirmation_threshold, 
                                                      int one_spill_over_threshold,
                                                      STTELL1::u_int16_t strip_start, 
                                                      int /*boundary_strip*/) //< boundary_strip never used (fix for slc6/gcc4.6)
{
	int err = 0;
	int i;
	int sum_temp,pos;
	int neigh_sum;
	int sum_pos;
	int pos_shift;
	ST_CLUSTER_UNIT cluster_unit;
	ST_ADC_UNIT     adc_unit;
  
	for(i=4; i<69; i++)
	{
    if(     ((i==4) && (zs_h_line[4]>0)) 
            || ((i>4)  && (i<68))
            || ((i==68)&& (zs_h_line[68]<=0))  
            )
    {
      if((zs_h_line[i]<=0)&&(zs_h_line[i-1]>0)&&(zs_h_line[i-2]<=0))   //010
      {
        zs_h_line[i-1] = 0;
        sum_temp = zs_line[i-1];
        
        
        if(sum_temp>one_spill_over_threshold) cluster_unit.Sepe.CLUSTER_SO = 1;
        else             cluster_unit.Sepe.CLUSTER_SO = 0;
        
        cluster_unit.Sepe.CLUSTER_SIZE = 0;
        
        pos_shift = 1;
        sum_pos = (pos_shift-1)*zs_line[i-1];
        
        pos = (sum_pos*((int)(65536.0F/(float)sum_temp+0.5))+0x2000)/16384;
        cluster_unit.Sepe.CLUSTER_POS = (strip_start+i-4-pos_shift+ ((pos>>2)&0xFFF))&0xFFF;
        
        cluster_unit.Sepe.CLUSTER_ISP = (pos&0x3);
        
        *(cluster+(*pp_clus_cnt)) = cluster_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_clus_cnt)++;
        
        neigh_sum = zs_line[i]+zs_line[i-2];
        if(neigh_sum >15 ) neigh_sum = 15;
        else if(neigh_sum < -16 ) neigh_sum = -16;
        *(adc_list+(*pp_adc_cnt)) = neigh_sum;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
        
        adc_unit.Sepe.ADC_VALUE = zs_line[i-1];
        adc_unit.Sepe.ADC_EOC = 1;
        *(adc_list+(*pp_adc_cnt)) = adc_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
      }
      if((zs_h_line[i]<=0)&&(zs_h_line[i-1]>0)&&(zs_h_line[i-2]>0)&&(zs_h_line[i-3]<=0))   //0110
      {
        zs_h_line[i-1] = 0;
        zs_h_line[i-2] = 0;
        sum_temp = zs_line[i-1]+zs_line[i-2];
        
        if(sum_temp>one_spill_over_threshold) cluster_unit.Sepe.CLUSTER_SO = 1;
        else             cluster_unit.Sepe.CLUSTER_SO = 0;
        
        cluster_unit.Sepe.CLUSTER_SIZE = 0;
        pos_shift = 2;
        sum_pos = (pos_shift-1)*zs_line[i-1]+(pos_shift-2)*zs_line[i-2];
        
        pos = (sum_pos*((int)(65536.0F/(float)sum_temp+0.5))+0x2000)/16384;
        cluster_unit.Sepe.CLUSTER_POS = (strip_start+i-4-pos_shift+ ((pos>>2)&0xFFF))&0xFFF;
        
        cluster_unit.Sepe.CLUSTER_ISP = (pos&0x3);
        
        *(cluster+(*pp_clus_cnt)) = cluster_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_clus_cnt)++;
        
        neigh_sum = zs_line[i]+zs_line[i-3];
        if(neigh_sum >15 ) neigh_sum = 15;
        else if(neigh_sum < -16 ) neigh_sum = -16;
        *(adc_list+(*pp_adc_cnt)) = neigh_sum;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
        
        adc_unit.Sepe.ADC_VALUE = zs_line[i-2];
        adc_unit.Sepe.ADC_EOC = 0;
        *(adc_list+(*pp_adc_cnt)) = adc_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
        
        adc_unit.Sepe.ADC_VALUE = zs_line[i-1];
        adc_unit.Sepe.ADC_EOC = 1;
        *(adc_list+(*pp_adc_cnt)) = adc_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
      }
      if((zs_h_line[i]<=0)&&(zs_h_line[i-1]>0)&&(zs_h_line[i-2]>0)&&(zs_h_line[i-3]>0)&&(zs_h_line[i-4]<=0))   //01110
      {
        zs_h_line[i-1] = 0;
        zs_h_line[i-2] = 0;
        zs_h_line[i-3] = 0;
        sum_temp = zs_line[i-1]+zs_line[i-2]+zs_line[i-3];
        
        if(sum_temp>one_spill_over_threshold) cluster_unit.Sepe.CLUSTER_SO = 1;
        else             cluster_unit.Sepe.CLUSTER_SO = 0;
        
        cluster_unit.Sepe.CLUSTER_SIZE = 1;
        pos_shift = 3;
        sum_pos = (pos_shift-1)*zs_line[i-1]+(pos_shift-2)*zs_line[i-2]+(pos_shift-3)*zs_line[i-3];
        
        pos = (sum_pos*((int)(65536.0F/(float)sum_temp+0.5))+0x2000)/16384;
        cluster_unit.Sepe.CLUSTER_POS = (strip_start+i-4-pos_shift+ ((pos>>2)&0xFFF))&0xFFF;
        
        cluster_unit.Sepe.CLUSTER_ISP = (pos&0x3);
        
        *(cluster+(*pp_clus_cnt)) = cluster_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_clus_cnt)++;
        
        neigh_sum = zs_line[i]+zs_line[i-4];
        if(neigh_sum >15 ) neigh_sum = 15;
        else if(neigh_sum < -16 ) neigh_sum = -16;
        *(adc_list+(*pp_adc_cnt)) = neigh_sum;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
        
        adc_unit.Sepe.ADC_VALUE = zs_line[i-3];
        adc_unit.Sepe.ADC_EOC = 0;
        *(adc_list+(*pp_adc_cnt)) = adc_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
        
        adc_unit.Sepe.ADC_VALUE = zs_line[i-2];
        adc_unit.Sepe.ADC_EOC = 0;
        *(adc_list+(*pp_adc_cnt)) = adc_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
        
        adc_unit.Sepe.ADC_VALUE = zs_line[i-1];
        adc_unit.Sepe.ADC_EOC = 1;
        *(adc_list+(*pp_adc_cnt)) = adc_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
      }
      if((zs_h_line[i]>0)&&(zs_h_line[i-1]>0)&&(zs_h_line[i-2]>0)&&(zs_h_line[i-3]>0)&&(zs_h_line[i-4]<=0))   //01111
      {
        zs_h_line[i]   = 0;
        zs_h_line[i-1] = 0;
        zs_h_line[i-2] = 0;
        zs_h_line[i-3] = 0;
        sum_temp = zs_line[i] + zs_line[i-1]+zs_line[i-2]+zs_line[i-3];
        
        if(sum_temp>one_spill_over_threshold) cluster_unit.Sepe.CLUSTER_SO = 1;
        else             cluster_unit.Sepe.CLUSTER_SO = 0;
        
        cluster_unit.Sepe.CLUSTER_SIZE = 1;
        
        pos_shift = 3;
        sum_pos = (pos_shift-0)*zs_line[i]+(pos_shift-1)*zs_line[i-1]+(pos_shift-2)*zs_line[i-2]+(pos_shift-3)*zs_line[i-3];
        
        
        pos = (sum_pos*((int)(65536.0F/(float)sum_temp+0.5))+0x2000)/16384;
        cluster_unit.Sepe.CLUSTER_POS = (strip_start+i-4-pos_shift+ ((pos>>2)&0xFFF))&0xFFF;
        
        cluster_unit.Sepe.CLUSTER_ISP = (pos&0x3);
        
        *(cluster+(*pp_clus_cnt)) = cluster_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_clus_cnt)++;
        
        neigh_sum = zs_line[i+1]+zs_line[i-4];
        if(neigh_sum >15 ) neigh_sum = 15;
        else if(neigh_sum < -16 ) neigh_sum = -16;
        *(adc_list+(*pp_adc_cnt)) = neigh_sum;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
        
        adc_unit.Sepe.ADC_VALUE = zs_line[i-3];
        adc_unit.Sepe.ADC_EOC = 0;
        *(adc_list+(*pp_adc_cnt)) = adc_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
        
        adc_unit.Sepe.ADC_VALUE = zs_line[i-2];
        adc_unit.Sepe.ADC_EOC = 0;
        *(adc_list+(*pp_adc_cnt)) = adc_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
        
        adc_unit.Sepe.ADC_VALUE = zs_line[i-1];
        adc_unit.Sepe.ADC_EOC = 0;
        *(adc_list+(*pp_adc_cnt)) = adc_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
        
        adc_unit.Sepe.ADC_VALUE = zs_line[i];
        adc_unit.Sepe.ADC_EOC = 1;
        *(adc_list+(*pp_adc_cnt)) = adc_unit.All;
        if(sum_temp>one_confirmation_threshold) // the cluster is invalid if confirmation threshold is not exceeded
          (*pp_adc_cnt)++;
      }
      if((zs_h_line[i]>0)&&(zs_h_line[i-1]>0)&&(zs_h_line[i-2]>0)&&(zs_h_line[i-3]>0)&&(zs_h_line[i-4]>0))   //11111
      {
        zs_h_line[i-1]   = 0;
        zs_h_line[i-2]   = 0;
        zs_h_line[i-3]   = 0;
        zs_h_line[i-4]   = 0;
      }
    }
    if(static_cast<STTELL1::u_int32_t>(*pp_clus_cnt)==pp_max_clusters) return(err);  // fix signed/unsigned comparison DEH 2013-07-09
		
	}
	
	return(err);
}

//=============================================================================
void STTELL1ZSProcessEngine::setBoundaryStrips(const BoundaryStripVec& inData)
{
  std::memcpy(m_boundaryStrips,&(*inData.begin()),sizeof(BoundaryStrip));
}
//=============================================================================
void STTELL1ZSProcessEngine::setHitThresholds(const HitThresholdVec& inData)
{
  std::memcpy(*m_hitThresholds,&(*inData.begin()),sizeof(HitThreshold));
}
//=============================================================================
void STTELL1ZSProcessEngine::setConfirmationThresholds(const ConfirmationThresholdVec& inData)
{
  std::memcpy(m_confirmationThresholds,&(*inData.begin()),sizeof(ConfirmationThreshold));
}
//=============================================================================
void STTELL1ZSProcessEngine::setSpillOverThresholds(const SpillOverThresholdVec& inData)
{
  std::memcpy(m_spillOverThresholds,&(*inData.begin()),sizeof(SpillOverThreshold));
}
//=============================================================================
void STTELL1ZSProcessEngine::setPPMaxClusters(const unsigned int nClusters)
{
  m_ppMaxClusters = nClusters;
}
//=============================================================================
void STTELL1ZSProcessEngine::flushMemory()
{
  std::memset(m_clusters,0,sizeof(TELL1Cluster));
  std::memset(m_adcs,0,sizeof(TELL1ADC));
  m_producedClusters=0;
  m_includedADCS=0;
}
//=============================================================================
void STTELL1ZSProcessEngine::runZeroSuppression()
{
  // prepare data for the c-module
  flushMemory();
  std::memcpy(***m_cModuleData,&(*inData().begin()),sizeof(Data));
  
  int error=st_zs_process(processEnable(),
                          m_hitThresholds,
                          m_confirmationThresholds,
                          m_spillOverThresholds,
                          m_boundaryStrips,
                          m_cModuleData,
                          m_ppMaxClusters,
                          m_clusters, 
                          &m_producedClusters,
                          m_adcs,
                          &m_includedADCS); 

  // return created clusters and added adcs
  if(!error){
    m_rawClusters->resize(m_producedClusters);
    std::memcpy(&(*m_rawClusters->begin()),m_clusters,m_producedClusters*sizeof(STTELL1::u_int16_t));
    
    m_rawADCS->resize(m_includedADCS);
    std::memcpy(&(*m_rawADCS->begin()),m_adcs,m_includedADCS*sizeof(STTELL1::u_int8_t));
  }

}
//--
