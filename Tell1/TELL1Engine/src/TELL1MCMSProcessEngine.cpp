// $Id: TELL1MCMSProcessEngine.cpp,v 1.2 2008-10-17 16:43:01 szumlat Exp $
// Include files 

#include <cstring>

// STL
#include<vector>
#include<algorithm>

// local
#include "TELL1Engine/TELL1MCMSProcessEngine.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TELL1MCMSProcessEngine
//
// 2008-03-08 : Tomasz Szumlak
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TELL1MCMSProcessEngine::TELL1MCMSProcessEngine(  ) {
}
//=============================================================================
// Destructor
//=============================================================================
TELL1MCMSProcessEngine::~TELL1MCMSProcessEngine() {}
//=============================================================================
int TELL1MCMSProcessEngine::velo_mcms_process(int mcms_enable,
                                              VeloTELL1::Data in_data)
{
	int err = 0;
	int ppx,channel,strip;
	int sum,ave;
	
	if(mcms_enable==0) return(err);

	for(ppx=0;ppx<4;ppx++)
	{
	    for(channel=0;channel<8;channel++)
	    { 
	     // if( (ppx==0)&&(channel==0))
	     // {  printf("\nmcms_in :");
	     // 	 for(strip= 0;strip<16;strip++) printf("%4d ",in_data[ppx][channel][strip+32]); 
	     // 	 for(strip=16;strip<32;strip++) printf("%4d ",in_data[ppx][channel][strip+32]); 
	     // }
			
			//--------------			
	   	  sum = 16;
	    	for(strip=0;strip<32;strip++)
				  if (in_data[ppx][channel][strip] < 10)	sum += in_data[ppx][channel][strip];  // signal is masked with fixed threshold = 10
				ave = sum>>5;   	
	    	for(strip=0;strip<32;strip++)
				  in_data[ppx][channel][strip]-= ave;			
			//--------------	  
	    	sum = 16;
	    	for(strip=32;strip<64;strip++)
				  if (in_data[ppx][channel][strip] < 10) sum += in_data[ppx][channel][strip];  // signal is masked with fixed threshold = 10
	    	ave = sum>>5; 
	    	for(strip=32;strip<64;strip++)
				  in_data[ppx][channel][strip]-= ave;
	  		//if( (ppx==0)&&(channel==0)){ printf("\nsum is %d, ave is %d",sum,ave);  }	  
  	    //--------------  
			    saturate_for_8bits(in_data[ppx][channel],64);			    
	     // if( (ppx==0)&&(channel==1))
	     // { printf("\nmcms_out:");
	     // 	for(strip= 0;strip<16;strip++) printf("%4d ",in_data[ppx][channel][strip+32]); 
	     //   //printf("\n        :");
	     //    for(strip=16;strip<32;strip++) printf("%4d ",in_data[ppx][channel][strip+32]); 
	     // }
	   }
  }
	return(err);
}
//=============================================================================
void TELL1MCMSProcessEngine::saturate_for_8bits(int *data, int line_numb)
{
  int i;
  for(i=0;i<line_numb;i++)
  {
    if(data[i]>127) data[i] =127;
    else if(data[i] < -128)data[i] = -128;
  }
}
//=============================================================================
void TELL1MCMSProcessEngine::runMCMS()
{
  // prepare data for c-module
  std::memcpy(**m_cModuleData,&(*inData().begin()),sizeof(VeloTELL1::Data));

  // run the reorder c-module
  int error=velo_mcms_process(processEnable(),
                              m_cModuleData);

  // write reordered data
  if(!error){    
    std::memcpy(&(*outData().begin()),(**m_cModuleData),sizeof(VeloTELL1::Data));
  }else if(error){
    std::cout<< " --> MCMS Module FAILED! " <<std::endl;
  }
}
//--
