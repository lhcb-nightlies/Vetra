// $Id: TELL1FIRProcessEngine.cpp,v 1.1.1.1 2008-05-23 13:10:24 szumlat Exp $
#include <cstring>

// STL
#include<vector>
#include<algorithm>

// local
#include "TELL1Engine/TELL1FIRProcessEngine.h"

// std
#include <iostream>

//-----------------------------------------------------------------------------
// Implementation file for class : TELL1FIRProcessEngine
//
// 2007-08-10 : Tomasz Szumlak
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TELL1FIRProcessEngine::TELL1FIRProcessEngine(  ):
  TELL1Engine()
{ 
  this->initTableMembers();
}
//=============================================================================
// Destructor
//=============================================================================
TELL1FIRProcessEngine::~TELL1FIRProcessEngine() {}
//=============================================================================
int TELL1FIRProcessEngine::velo_fir_process(int fir_enable,
                                            VeloTELL1::FIRCoeff fir_coefficient,
                                            VeloTELL1::Data in_data)
{
	int err = 0;
	int ppx,channel,strip;
	int data_buf[68];
	
	if(fir_enable==0) return(err);

   	//printf("\nfir_en=%d, fir_coeff = %d,%d,%d,%d",fir_enable,fir_coefficient[0][0][0],fir_coefficient[0][0][1],fir_coefficient[0][0][2],fir_coefficient[0][0][3]);
   	//printf("\nfir_in  ="); for(strip=0;strip<32;strip++) printf("%3X ",in_data[0][0][strip]); 

  for(ppx=0;ppx<4;ppx++)
    for(channel=0;channel<8;channel++)
    {     
        // for the first 32 strips
          // set the boundary condition (two left and two right neibours are 0)
		  data_buf[0] = 0;   
    	  data_buf[1] = 0; 
    	  for(strip=0;strip<32;strip++)  data_buf[strip+2] = in_data[ppx][channel][strip]; 
    	  data_buf[34] = 0; 
    	  data_buf[35] = 0; 
    	  
    	
     	  for(strip=2;strip<34;strip++)
			in_data[ppx][channel][strip-2] = data_buf[strip] - ((data_buf[strip-2]*fir_coefficient[ppx][channel][0]+
                                                         data_buf[strip-1]*fir_coefficient[ppx][channel][1]+
														 data_buf[strip+1]*fir_coefficient[ppx][channel][2]+
													 	 data_buf[strip+2]*fir_coefficient[ppx][channel][3]+
														 0x100) >>9);
        // for the second 32 strips
          // set the boundary condition (two left and two right neibours are 0)
		  data_buf[32] = 0;   
    	  data_buf[33] = 0; 
    	  for(strip=32;strip<64;strip++)  data_buf[strip+2] = in_data[ppx][channel][strip]; 
    	  data_buf[66] = 0; 
    	  data_buf[67] = 0; 
    	  
    	
     	  for(strip=34;strip<66;strip++)
			in_data[ppx][channel][strip-2] = data_buf[strip] - ((data_buf[strip-2]*fir_coefficient[ppx][channel][0]+
                                                         data_buf[strip-1]*fir_coefficient[ppx][channel][1]+
														 data_buf[strip+1]*fir_coefficient[ppx][channel][2]+
													 	 data_buf[strip+2]*fir_coefficient[ppx][channel][3]+
														 0x100) >>9);
    	  
        	saturate_for_8bits(in_data[ppx][channel],64);  

    } // end ppx and channel

  //printf("\nfir_out ="); for(strip=0;strip<32;strip++) printf("%3d ",in_data[0][0][strip]); 

	return(err);
}
//=============================================================================
void TELL1FIRProcessEngine::initTableMembers()
{
  std::memset(**m_firCoeff,0,sizeof(VeloTELL1::FIRCoeff));
}
//=============================================================================
void TELL1FIRProcessEngine::setFIRCoeff(const VeloTELL1::FIRCoeffVec& inData)
{
   std::memcpy(**m_firCoeff,&(*inData.begin()),
               sizeof(VeloTELL1::FIRCoeff));
}
//=============================================================================
void TELL1FIRProcessEngine::saturate_for_8bits(int *data, int line_numb)
{
  int i;
  for(i=0;i<line_numb;i++)
  {
    if(data[i]>127) data[i] =127;
    else if(data[i] < -128)data[i] = -128;
  }
}
//=============================================================================
void TELL1FIRProcessEngine::runFilter()
{
  // prepare data and headers for c-module
  std::memcpy(**m_cModuleData,&(*inData().begin()),sizeof(VeloTELL1::Data));
  
  // run the c-module
  int error=velo_fir_process(processEnable(),
                             m_firCoeff,
                             m_cModuleData);
  // write the subtracted pedestals to out container
  if(!error){    
    std::memcpy(&(*outData().begin()),**m_cModuleData,sizeof(VeloTELL1::Data));
  }else if(error){
    std::cout<< " --> FIR Module FAILED! " <<std::endl;
  }
}
//--
