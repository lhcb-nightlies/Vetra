// $Id: STTELL1PedestalProcessEngine.cpp,v 1.4 2010-01-13 12:51:26 akeune Exp $
// Include files

// STL
#include<vector>
#include<algorithm>

// local
#include "TELL1Engine/STTELL1PedestalProcessEngine.h"

// std
#include <iostream>

//-----------------------------------------------------------------------------
// Implementation file for class : STTELL1PedestalProcessEngine
//
// 2007-06-14 : Tomasz Szumlak, Anne Keune
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STTELL1PedestalProcessEngine::STTELL1PedestalProcessEngine(  ):
  STTELL1Engine(),
  m_zsEnable ( 0 ),
  m_headerCorrectionFlag ( 0 ),
  m_header ( )
{ 
  this->initTableMembers();
}
//=============================================================================
// Destructor
//=============================================================================
STTELL1PedestalProcessEngine::~STTELL1PedestalProcessEngine() 
{ }
//=============================================================================

int STTELL1PedestalProcessEngine::st_pedestal_process(
                                                      int pedestal_enable,
                                                      int zs_enable, 
                                                      int header_correction_enable, 
                                                      STTELL1::u_int32_t header_corr_threshold[2],
                                                      int header_corr_value[96][6][8],
                                                      int header[4][6][4][4],
                                                      STTELL1::u_int8_t link_mask[96][32],
                                                      STTELL1::u_int8_t link_pedestal[96][32],
                                                      int in_data[4][6][4][32],
                                                      STTELL1::u_int32_t  Pseudo_header_lo_thr,//---Alex 2009-11-10, header-corrrection-for-6-strips  
                                                      STTELL1::u_int32_t  Pseudo_header_hi_thr //---Alex 2009-11-10, header-corrrection-for-6-strips 
                                                      ) 
{

	int ppx,beetle,link,strip,err = 0;
	
	int ppx_show=0,beetle_show=0; //link_show=3;
	
	int header_correction;
	int header_zero_threshold,header_one_threshold;
  int header_b[3];
  int header_err_b;
  int pcn_lsb;
  int header_corr_v;

	header_zero_threshold  = header_corr_threshold[0];
	header_one_threshold   = header_corr_threshold[1]; 
  
  /*
  for(ppx=0;ppx<4;ppx++) 
		for(beetle=0;beetle<6;beetle++)
			for(link=0;link<4;link++) 
			{
				if ((ppx==ppx_show) && (beetle==beetle_show) && (link==link_show) )
        {       PRINTF_DEBUG(RED"\nin_data:"BLACK);
        for(strip=0;strip<32;strip++)
          PRINTF_DEBUG(" %2d",in_data[ppx][beetle][link][strip]);
        }
        
			}
  */
  
	if(pedestal_enable == 1)
	{
    for(ppx=0;ppx<4;ppx++) 
		{
			for(beetle=0;beetle<6;beetle++)
			{
				for(link=0;link<4;link++) 
				{
	  
          if (     header[ppx][beetle][0][3]    > (int) Pseudo_header_hi_thr)   pcn_lsb = 1;
          else if (header[ppx][beetle][0][3]    < (int) Pseudo_header_lo_thr)   pcn_lsb = 0;
          else                                                            pcn_lsb = 0;
          pcn_lsb *= 1;// fix compiler warning (MT 5/7/13)
	  header_err_b = 0;
	  if      (header[ppx][beetle][link][1] > header_one_threshold)   {header_b[2] = 1; }
	  else if (header[ppx][beetle][link][1] < header_zero_threshold)  {header_b[2] = 0; }
	  else                                                            {header_b[2] = 0; header_err_b = 1;}
		          
          if      (header[ppx][beetle][link][2] > header_one_threshold)   {header_b[1] = 1; }
          else if (header[ppx][beetle][link][2] < header_zero_threshold)  {header_b[1] = 0; }
          else                                                            {header_b[1] = 0; header_err_b = 1;}
          
          if      (header[ppx][beetle][link][3] > header_one_threshold)   {header_b[0] = 1; }
          else if (header[ppx][beetle][link][3] < header_zero_threshold)  {header_b[0] = 0; }
          else                                                            {header_b[0] = 0; header_err_b = 1;}
          
          header_corr_v = (header_b[2]<<2) + (header_b[1]<<1) + header_b[0];
          
          if ((ppx==ppx_show) && (beetle==beetle_show) )
          { //PRINTF_DEBUG("\n%d head_cor:",header_corr_v);
          }
          for(strip=0;strip<32;strip++) 
          {
            if ((strip<6) && (header_correction_enable==1) && (header_err_b ==0) )	
            {
              // header correction for first 6 strips
              header_correction = (int)header_corr_value[ppx*24+beetle*4+link][strip][header_corr_v];
            }
            else 	header_correction = 0;

            in_data[ppx][beetle][link][strip] -= header_correction; // header correction
            
            if ((ppx==ppx_show) && (beetle==beetle_show)   )
            { //PRINTF_DEBUG(" %d",header_correction);
            }
          }
			  }//link
	    }//beelte
    }//ppx
	}
  for(ppx=0;ppx<4;ppx++) 
	{
		if(pedestal_enable == 0) continue;
		for(beetle=0;beetle<6;beetle++)
		{
			for(link=0;link<4;link++) 
			{
				for(strip=0;strip<32;strip++)
				{
					if((zs_enable==0)||(link_mask[ppx*24+beetle*4+link][strip]==1))  // masked strips set to zero
            in_data[ppx][beetle][link][strip] = 0;
					else if(pedestal_enable == 1) 
          { 
            in_data[ppx][beetle][link][strip] -= (link_pedestal[ppx*24+beetle*4+link][strip]); 
					}
                    // here is the saturation to -128..+127
					if     (in_data[ppx][beetle][link][strip] > 127) 
            in_data[ppx][beetle][link][strip] =127;
					else if(in_data[ppx][beetle][link][strip] < -128)
            in_data[ppx][beetle][link][strip] = -128;
          
				}
			}
		}
  } 
  /*
	for(ppx=0;ppx<4;ppx++) 
		for(beetle=0;beetle<6;beetle++)
			for(link=0;link<4;link++) 
			{
				if ((ppx==ppx_show) && (beetle==beetle_show) && (link==link_show) )
        {       PRINTF_DEBUG(RED"\npedestal sub data:"BLACK);
        for(strip=0;strip<32;strip++)
          PRINTF_DEBUG(" %2d",in_data[ppx][beetle][link][strip]);
        } 
			}
  */

	return(err);
  
}

//=============================================================================
void STTELL1PedestalProcessEngine::setZSEnable(const int inValue)
{
  m_zsEnable=inValue;
}
//=============================================================================
void STTELL1PedestalProcessEngine::setHeaderCorrectionFlag(const int inValue)
{
  m_headerCorrectionFlag=inValue;
}
//=============================================================================
void STTELL1PedestalProcessEngine::setHeaderCorrectionValues(
                                 const HeaderCorrectionVec& inData)
{
  std::memcpy(*m_headerCorrections,&(*inData.begin()),sizeof(HeaderCorrection));
}
//=============================================================================
void STTELL1PedestalProcessEngine::setHeaderThresholdValues(
                                 const HeaderThresholdVec& inData)
{
  std::memcpy(m_headerThresholds,&(*inData.begin()),sizeof(HeaderThreshold));
}
//=============================================================================
void STTELL1PedestalProcessEngine::setPseudoHeaderThresholdValues(
                                 const HeaderThresholdVec& inData)
{
  std::memcpy(m_pseudoHeaderThresholds,&(*inData.begin()),sizeof(HeaderThreshold));
}
//=============================================================================
void STTELL1PedestalProcessEngine::setHeaders(const EngineVec& inData)
{
  m_header=&inData; 
}
//=============================================================================
void STTELL1PedestalProcessEngine::setLinkPedestal(const LinkVec& inData)
{
  std::memcpy(*m_linkPedestal,&(*inData.begin()),sizeof(LinkPedestal));
}
//=============================================================================
void STTELL1PedestalProcessEngine::setLinkMask(const LinkVec& inData)
{
  std::memcpy(*m_linkMask,&(*inData.begin()),sizeof(LinkMask));
}
//=============================================================================
void STTELL1PedestalProcessEngine::initTableMembers()
{
  std::memset(*m_headerCorrections,0,sizeof(HeaderCorrection));
  std::memset(m_headerThresholds,0,sizeof(HeaderThreshold));
  std::memset(m_pseudoHeaderThresholds,0,sizeof(HeaderThreshold));
  std::memset(*m_linkPedestal,0,sizeof(LinkPedestal));
  std::memset(*m_linkMask,0,sizeof(LinkMask));
}
//=============================================================================
void STTELL1PedestalProcessEngine::runSubtraction()
{

  // prepare data and headers for c-module
  std::memcpy(***m_cModuleData,&(*inData().begin()),sizeof(Data));
  std::memcpy(***m_cModuleHeader,&(*m_header->begin()),sizeof(Header));

  // run the c-module
  int error = st_pedestal_process(processEnable(),
                                  m_zsEnable,
                                  m_headerCorrectionFlag,
                                  m_headerThresholds,
                                  m_headerCorrections,
                                  m_cModuleHeader,
                                  m_linkMask,
                                  m_linkPedestal,
                                  m_cModuleData,
                                  m_pseudoHeaderThresholds[0],
                                  m_pseudoHeaderThresholds[1]);
  // write the subtracted pedestals to out container

  if(!error && dataSize()==outData().size()){   
    std::memcpy(&(*outData().begin()),(***m_cModuleData),sizeof(Data));
  }
}
//--

