// $Id: ISTVetraCfg.h,v 1.3 2010-03-10 10:24:08 jluisier Exp $
#ifndef KERNEL_ISTVETRACFG_H 
#define KERNEL_ISTVETRACFG_H 1

// Include files
// from STL
#include <string>

#include <iostream>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_ISTVetraCfg ( "ISTVetraCfg", 1, 0 );

class STVetraCondition;

/** @class ISTVetraCfg ISTVetraCfg.h Kernel/ISTVetraCfg.h
 *  
 *
 *  @author Johan Luisier
 *  @date   2009-04-24
 */
class ISTVetraCfg : virtual public IAlgTool {
public: 

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_ISTVetraCfg; }

  virtual STVetraCondition* getSTVetraCond(const unsigned int& ID) const = 0;

  virtual void setType( const std::string& detType ) = 0;

  virtual const std::string& getDetType() const = 0;

  virtual std::ostream& printOut(std::ostream& os) const = 0;
  
protected:

private:

};
#endif // KERNEL_ISTVETRACFG_H
