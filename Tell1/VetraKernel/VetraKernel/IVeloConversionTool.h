#ifndef KERNEL_IVELOCONVERSIONTOOL_H 
#define KERNEL_IVELOCONVERSIONTOOL_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_IVeloConversionTool ( "IVeloConversionTool", 1, 0 );

/** @class IVeloConversionTool IVeloConversionTool.h Kernel/IVeloConversionTool.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2006-05-20
 */
class IVeloConversionTool : virtual public IAlgTool {
public: 

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IVeloConversionTool; }

  virtual unsigned int strip2ChipChannel(
                                           const unsigned int strip,
                                           const unsigned int sensNumber)=0;
  virtual unsigned int chipChannel2Strip(
                                           const unsigned int channel,
                                           const unsigned int sensNumber)=0;

protected:

private:

};
#endif // KERNEL_IVELOCONVERSIONTOOL_H
