// $Id: compareSensor.h,v 1.1.1.1 2008-05-23 12:54:39 szumlat Exp $
#ifndef KERNEL_COMPARESENSOR_H 
#define KERNEL_COMPARESENSOR_H 1

// Include files

/** @class compareSensor compareSensor.h Kernel/compareSensor.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2006-05-22
 */

template <class T>
class compareSensor:
public std::unary_function<T, bool> {
public: 
  /// Standard constructor
  compareSensor(const int inSens): m_sensor ( inSens )
  { }
  virtual ~compareSensor( ){ }   ///< Destructor
  inline bool operator()(T& inObj)
  {
    return ( (!inObj) ? false : inObj->sensor()!=m_sensor );
  }

protected:

private:

  int m_sensor;    /// number of current sensor

};
#endif // KERNEL_COMPARESENSOR_H
