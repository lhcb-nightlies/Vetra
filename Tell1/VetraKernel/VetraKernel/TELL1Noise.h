// $Id: TELL1Noise.h,v 1.1.1.1 2008-05-23 12:54:39 szumlat Exp $
#ifndef TELL1NOISE_H 
#define TELL1NOISE_H 1
// ============================================================================
// Include files
// ============================================================================
// STD&STL
// ============================================================================
#include <vector>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/StatEntity.h"
// ============================================================================


// ============================================================================
namespace LHCb { 
  class VeloTELL1Data ; 
}
class DeVeloRType;
class DeVeloPhiType;

// ============================================================================
namespace Velo 
{
  namespace Monitoring 
  {
    /** @class TELL1Noise TELL1Noise.h
     *  The simple structure which holds the noise data for the sensor 
     *  It could be used as a set of the cumulative counters as well as set 
     *  of running counters 
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu 
     *  @date   2007-10-25
     */
    class TELL1Noise 
    {
    public:
      /// the actual type of counters 
      typedef std::vector<StatEntity>  Counters ;
    private:
      /// the actual type of input data 
      typedef std::vector<signed int> Data ;
    public: 
      /// Standard constructor
      TELL1Noise () ; 
      /// the destructor (trivial) 
      ~TELL1Noise() ;
    public:
      /// get all counters 
      const Counters& counters  () const { return m_stats ; }
    public:
      // ======================================================================
      /** fill the counters with the data for one event 
       * 
       *  All counters are updated with the input data information
       *
       *  If <c>nReset</c> is positive and the number of entries for 
       *  the counter exceeds it, the counter is reset.
       *  It is a useful ingredient for calculation of "runnig mean".
       *  The pair of desynchronized counters efficiently acts and 
       *  the counter of the "running mean"
       *
       *  Very large signals could be suppressed if parameter 
       *  <c>nSigma</c> is positive, the signals which exceed 
       *  the value of "mean+nSigma*rms", where "mena" and "rms" are 
       *  current vallued of the mean and rms for the counter, 
       *  are discarded. The method could be used only if the 
       *  current estimate of the mean and rms are more or less 
       *  reliable, thus it is applicable only for the channels with 
       *  number of entries in excess of <c>nMin</c>
       * 
       *  @param data  input data 
       *  @param nReset  a handle to reset the counters 
       *  @param nSigma a handle to suppress too large signals 
       *  @param nMin minimal number of entries to allow supression of large signals 
       *  @return number of updated counters 
       */
      unsigned int fill  
      ( const LHCb::VeloTELL1Data& data        , 
        const unsigned int         nReset  = 0 , 
        const double               nSigma = -1 ,
        const unsigned int         nMin   = 50 ) ;
      // ======================================================================
      /** fill the counters with the data for one event 
       * 
       *  All counters are updated with the input data information
       *
       *  If <c>nReset</c> is positive and the number of entries for 
       *  the counter exceeds it, the counter is reset.
       *  It is a useful ingredient for calculation of "runnig mean".
       *  The pair of desynchronized counters efficiently acts and 
       *  the counter of the "running mean"
       *
       *  Channels with ADC counts higher than their signal thresholds
       *  will not be counted. There is one threshold per channel.
       *  The thresholds have to provided in software strip order.
       * 
       *  @param data  input data
       *  @param thresholds signal thresholds 
       *  @param nReset  a handle to reset the counters 
       *  @return number of updated counters 
       */
      unsigned int fill  
      ( const LHCb::VeloTELL1Data& data        , 
        const std::vector<unsigned char>& thresholds,
        const unsigned int         nReset  = 0  ) ;
      // ======================================================================
    public:
      /// evaluate the average number of entries
      double meanEntries () const ;
      /// compare two objects  (by the average number of entries)
      bool operator< ( const TELL1Noise& other ) const ;
      /// reset all counters 
      void reset() ;
    private:
      // the actual data-holder:
      Counters m_stats ; ///< the actual data-holder:
     
      const DeVeloRType* m_rSensor; 
      const DeVeloPhiType* m_phiSensor; 
    } ;
    // ========================================================================
    /** @class NoisePair
     *  Relatively good and simple 
     *  implementation of counter, which mimics the "running mean" counter
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu 
     *  @date   2007-10-25
     */
    class NoisePair 
    {
    public:
      // constructor 
      NoisePair () ;
      // copy constructor 
      NoisePair           ( const NoisePair& ) ;
      // destcruct
      ~NoisePair() ;
    public:
      /// get the first object:
      TELL1Noise*       first  ()       { return &m_first  ; }
      /// get the first object:
      const TELL1Noise* first  () const { return &m_first  ; }
      /// get the second object:
      TELL1Noise*       second ()       { return  m_second ; }
      /// get the second object:
      const TELL1Noise* second () const { return  m_second ; }
      /// get the most statistically significant noise information
      const TELL1Noise* best() const ;
    public:
      /// check if the second object is active:
      bool active   () const { return 0 != m_second ; }
      /// activate the second object! 
      void activate () ;
      /// reset the counters and deactivate the pair 
      void reset () ;
    private: 
      // assignement is not allowed!
      NoisePair& operator=( const NoisePair& ) ;
    private:
      TELL1Noise  m_first  ;
      TELL1Noise* m_second ;
    } ;
  } // end of namespace Velo::Monitoring 
} // end of namespace Velo
// ============================================================================
// The END 
// ============================================================================  
#endif // TELL1_H
// ============================================================================  
