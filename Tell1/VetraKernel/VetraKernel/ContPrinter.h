// $Id: ContPrinter.h,v 1.1.1.1 2008-05-23 12:54:39 szumlat Exp $
#ifndef KERNEL_CONTPRINTER_H
#define KERNEL_CONTPRINTER_H 1

// includes

#include <iostream>
#include <vector>
#include "Tell1Kernel/VeloDecodeCore.h"

/*
 * helper templated function for printing elements from
 * collection, the function is meant to facilitate testing
 * and debugging TELL1 algorithms; templated type indicates
 * collection
 *
 * @author Tomasz Szumlak
 * @date   2006-05-24
 *
 */

using namespace VeloTELL1;

template<class T>
void contPrinter(const T& inCont, const int& inThreshold=0)
{
  typename T::const_iterator IT;
  std::cout<< " ==> contPrinter: The passed container size: " << inCont.size();
  std::cout<<std::endl;
  //
  int channel=0;
  for(IT=inCont.begin(); IT!=inCont.end(); ++IT){
    // check threshold is not zero and if so check if current
    // value from container is greater then passed threshold
    // and if this is the case print the value, if there is no
    // threshold given just print the value from container
    if(inThreshold!=0){
      if((*IT)>=inThreshold){
        std::cout<< (*IT) << ", channel: " << channel << "; ";
      }
    }else{
      std::cout<< (*IT) << ", ";
    }
    channel++;
  }
  std::cout<<std::endl;
} 
#endif // KERNEL_CONTPRINTER_H
