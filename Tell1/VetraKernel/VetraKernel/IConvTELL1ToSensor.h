// $Id: IConvTELL1ToSensor.h,v 1.1.1.1 2008-05-23 12:54:39 szumlat Exp $
#ifndef KERNEL_ICONVTELL1TOSENSOR_H 
#define KERNEL_ICONVTELL1TOSENSOR_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_IConvTELL1ToSensor ( "IConvTELL1ToSensor", 1, 0 );

/** @class IConvTELL1ToSensor IConvTELL1ToSensor.h Kernel/IConvTELL1ToSensor.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2006-08-25
 */

class IConvTELL1ToSensor : virtual public IAlgTool {
public: 

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IConvTELL1ToSensor; }

  virtual unsigned int tell1ToSensor(unsigned int tell1)=0;
  
protected:

private:

};
#endif // KERNEL_ICONVTELL1TOSENSOR_H
