// $Id: INoiseEvaluator.h,v 1.2 2009-11-19 17:21:35 krinnert Exp $
// ============================================================================
#ifndef INOISEEVALUATOR_H 
#define INOISEEVALUATOR_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL 
// ============================================================================
#include <map>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IAlgTool.h"
// ============================================================================
// VeloTELL1Event 
// ============================================================================
#include "VeloEvent/VeloTELL1Data.h"
// ============================================================================
namespace Velo 
{
  namespace Monitoring 
  {
    // ========================================================================
    /// the class which represent the noise infomration for TELL1
    class TELL1Noise ;
    // ========================================================================
    /** @class INoiseEvaluator  
     *  An abstract interface for noise-evaluator tool   
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-10-25
     */
    class INoiseEvaluator : public virtual IAlgTool 
    {
    public:
      /// the actual type of map-noise 
      typedef std::map<int,const Velo::Monitoring::TELL1Noise*> INoiseMap ;
      /// the actual type of the vector of TELL1-IDs
      typedef std::vector<int>                                  TELL1s    ;

    public:
      // ======================================================================
      /** process the data from one TELL1 board
       *  @see LHCb::VeloTELL1data
       *  @param data Velo TELL1 data 
       *  @param tell1 ID of TELL1 board 
       *  @param headers the container with link headers (if needed)
       *  @return status code 
       */
      virtual StatusCode process 
      ( const LHCb::VeloTELL1Data::Container* data        , 
        const int                             tell1       , 
        const LHCb::VeloTELL1Data::Container* headers = 0 ) = 0 ;
      // ======================================================================
      /** process the data from several TELL1 boards 
       *  @see LHCb::VeloTELL1data
       *  @param data Velo TELL1 data 
       *  @param tell1 ID of TELL1 board 
       *  @param headers the container with link headers (if needed)
       *  @return status code 
       */
      virtual StatusCode process 
      ( const LHCb::VeloTELL1Data::Container* data        , 
        const TELL1s&                         tell1s      ,
        const LHCb::VeloTELL1Data::Container* headers = 0 ) = 0 ;
      // ======================================================================
      /** process the data from all TELL1 boards
       *  @see LHCb::VeloTELL1data
       *  @param data Velo TELL1 data 
       *  @param headers the container with link headers (if needed)
       *  @return status code 
       */
      virtual StatusCode process 
      ( const LHCb::VeloTELL1Data::Container* data        ,
        const LHCb::VeloTELL1Data::Container* headers = 0 ) = 0 ;
      // ======================================================================
      /** process the data from one TELL1 board
       *  @see LHCb::VeloTELL1data
       *  @param data Velo TELL1 data 
       *  @param headers the container with link headers (if needed)
       *  @return status code 
       */
      virtual StatusCode process 
      ( const LHCb::VeloTELL1Data*            data        ,
        const LHCb::VeloTELL1Data::Container* headers     ) = 0 ;
      // ======================================================================
      /** process the data from one TELL1 board
       *  @see LHCb::VeloTELL1data
       *  @param data Velo TELL1 data 
       *  @param headers the link headers (if needed)
       *  @return status code 
       */
      virtual StatusCode process 
      ( const LHCb::VeloTELL1Data*            data        ,
        const LHCb::VeloTELL1Data*            headers = 0 ) = 0 ;
      // ======================================================================
    public:
      // ======================================================================
      /** get all noise information for the given TELL1 
       *  @param tell1 the ID of the TELL1 board 
       *  @return pointer to TELL1 noise information 
       */
      virtual const Velo::Monitoring::TELL1Noise* 
      noise ( const int tell1  ) const  = 0 ;
      /** get all noise information for some TELL1 boards 
       *  @param tell1s the list of IDs for the TELL1 boards 
       *  @return container of TELL1 noise information
       */
      virtual INoiseMap noise ( const TELL1s& tell1s ) const = 0 ;
      /** get all noise information for all TELL1 boards 
       *  @return container of TELL1 noise information
       */
      virtual INoiseMap noise () const = 0 ;

      /** check whether the TES container the tool is operating on
       * is redordered.
       * @return reorder flag
       */
      virtual bool isReordered() const = 0;

      /** reset all counters for a given TELL1.
       */
      virtual void reset( const unsigned int TELL1 ) = 0;
      
      // ======================================================================
    public:
      // ======================================================================
      /// the unique interface identifier 
      static const InterfaceID& interfaceID() ;
      // ======================================================================
    protected:
      // ======================================================================
      // virtual and protected destructor 
      virtual ~INoiseEvaluator() ; ///< virtual and protected destructor 
      // ======================================================================
    };
  } // end of namespace Velo::Monitoring
} // end of namespace Velo
// ============================================================================
// The END 
// ============================================================================
#endif // INOISEEVALUATOR_H
// ============================================================================
