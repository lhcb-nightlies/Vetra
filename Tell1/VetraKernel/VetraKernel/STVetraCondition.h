// $Id: STVetraCondition.h,v 1.7 2010-04-06 09:06:13 jluisier Exp $
#ifndef STVETRACOND_H 
#define STVETRACOND_H 1

// Include files
#include "DetDesc/Condition.h"
#include "VetraKernel/CLIDSTVetraCondition.h"

/** @class STVetraCond STVetraCond.h
 *  
 *
 *  @author Johan Luisier
 *  @date   2009-04-24
 */
class STVetraCondition : public Condition
{
public: 
  /// Standard constructor
  STVetraCondition( ); 
  STVetraCondition( Condition* cond ); 

  virtual ~STVetraCondition( ); ///< Destructor

  virtual StatusCode initialize() override;

  /// Class ID of this instance
  inline virtual const CLID& clID() const override { return classID(); } 
  /// Class ID of this class
  inline static  const CLID& classID() { return CLID_STVetraCondition; };

  bool isAnyDisable() const;
  
  std::ostream& printOut(std::ostream& os) const;
  
  friend std::ostream& operator<<(std::ostream& os, const STVetraCondition& stCond);
  friend std::ostream& operator<<(std::ostream& os, STVetraCondition* stCond);

  // access methods
  unsigned int cmsEnable() const;
  const std::vector< int >* cmsThreshold() const;
  const std::vector< int >* confirmationThreshold() const;
  unsigned int consecTrigger() const;
  unsigned int dataGenEnabled() const;
  const std::vector< int >* destIp() const;
  const std::vector< std::string >* destMac() const;
  unsigned int detectorId() const;
  const std::vector< int >* disableBeetles() const;
  const std::vector< std::string >* disableLinksPerBeetle() const;
  unsigned int ecsMepFactor() const;
  unsigned int ecsTrigType() const;
  unsigned int errBankDisable() const;
  std::string ethernetTime() const;
  std::string ethernetType() const;
  unsigned int extTriggerInputEnable() const;
  std::string feDataCheck() const;
  std::string firmware() const;
  std::string gbeCtrl() const;
  std::string gbePortSel() const;

  const std::vector< int >* dataRAMGenerator(const unsigned int& pp, const unsigned int& beetle, const unsigned int& link) const;

  const std::vector< int >*  headerCorrectionAnalogLink(const unsigned int& link) const;

  unsigned int headerEnable() const;
  std::string headerLength() const;
  int headerLowThreshold() const;
  int headerHighThreshold() const;
  const std::vector< int >* hitThreshold() const;
  std::string ipVersion() const;
  unsigned int infoBankDisable() const;
  unsigned int infoBankEnable() const;
  unsigned int mtuSize() const;
  std::string nzsBankOffset() const;
  std::string nzsBankPeriod() const;
  unsigned int nzsErrorEnable() const;
  std::string nextProtocol() const;
  const std::vector< std::string >* pcnSelect() const;
  unsigned int ppDerandomThreshold() const;
  unsigned int ppMaxClusters() const;
  unsigned int pedBankSchedule() const;
  unsigned int pedestalEnable() const;
  const std::vector< int >* pedestalMask() const;
  const std::vector< int >* pedestalValue() const;
  std::string pseudoHeaderHighThreshold() const;
  std::string pseudoHeaderLowThreshold() const;
  unsigned int sepGenEnabled() const;
  std::string seriousErrThroEnable() const;
  std::string serviceType() const;
  const std::vector< int >* sourceIp() const;
  const std::vector< std::string >* sourceMac() const;
  unsigned int specHeaderEnable() const;
  const std::vector< int >* spilloverThreshold() const;
  unsigned int ttcDestIpAvailable() const;
  unsigned int ttcInfoEnable() const;
  unsigned int ttcTriggerTypeAvailable() const;
  std::string tellName() const;
  std::string triggerNumber() const;
  unsigned int updateEnable() const;
  unsigned int waitCycle() const;
  unsigned int zsEnable() const;
  std::string classNzsBank() const;
  std::string classZsBank() const;
  std::string classErrBank() const;
  std::string classPedBank() const;
  std::string mepPid() const;
  std::string sourceId() const;
  const std::vector< int >* startStripChan0_23() const;
  const std::vector< int >* startStripChan24_47() const;
  std::string version() const;

  // set methods
  void pedestalMask(const std::vector< int >& vec);
  void pedestalValue(const std::vector< int >& vec);
  void pedestalMask(const std::vector< int >* ptrVec);
  void pedestalValue(const std::vector< int >* ptrVec);

  void hitThreshold(const std::vector< int >& vec);
  void hitThreshold(const std::vector< int >* ptrVec);
  void cmsThreshold(const std::vector< int >& vec);
  void cmsThreshold(const std::vector< int >* ptrVec);
  void confirmationThreshold(const std::vector< int >& vec);
  void confirmationThreshold(const std::vector< int >* ptrVec);
  void spilloverThreshold(const std::vector< int >& vec);
  void spilloverThreshold(const std::vector< int >* ptrVec);

  void disableBeetles(const std::vector< int >& vec);
  void disableBeetles(const std::vector< int >* ptrVec);
  
  void disableLinksPerBeetle(const std::vector< std::string >& vec);
  void disableLinksPerBeetle(const std::vector< std::string >* ptrVec);

  void headerCorrectionAnalogLink(const unsigned int& link,
                                  const std::vector< int >& vec);
  void headerCorrectionAnalogLink(const unsigned int& link,
                                  const std::vector< int >* ptrVec);

  void updateEnable(const unsigned int& value);
  void headerEnable(const unsigned int& value);
  void pseudoHeaderHighThreshold(const std::string& str);
  void pseudoHeaderLowThreshold(const std::string& str);

protected:

private:
  bool isRead;

  // Misc information
  std::string m_tellName;

  // GBE parameters
  std::vector< int > m_destIp;
  std::vector< std::string > m_destMac;
  std::vector< int > m_sourceIp;
  std::vector< std::string > m_sourceMac;
  std::string m_ethernetType;
  std::string m_ipVersion;
  std::string m_headerLength;
  std::string m_serviceType;
  std::string m_ethernetTime;
  std::string m_nextProtocol;
  std::string m_gbeCtrl;

  // General params TTC and process info
  unsigned int m_detectorId;
  unsigned int m_pedBankSchedule;
  unsigned int m_ttcInfoEnable;
  unsigned int m_extTriggerInputEnable;
  unsigned int m_ttcTriggerTypeAvailable;
  unsigned int m_ttcDestIpAvailable;
  unsigned int m_dataGenEnabled;
  unsigned int m_sepGenEnabled;
  unsigned int m_ppDerandomThreshold;
  std::string m_mepPid;
  std::string m_sourceId;
  std::string m_version;
  std::string m_classZsBank;
  std::string m_classNzsBank;
  std::string m_classPedBank;
  std::string m_classErrBank;
  std::string m_feDataCheck;
  std::string m_seriousErrThroEnable;
  std::string m_nzsBankPeriod;
  std::string m_nzsBankOffset;
  std::string m_triggerNumber;
  unsigned int m_consecTrigger;
  unsigned int m_waitCycle;
  unsigned int m_ecsTrigType;
  unsigned int m_errBankDisable;
  unsigned int m_infoBankDisable;
  unsigned int m_infoBankEnable;
  unsigned int m_nzsErrorEnable;
  unsigned int m_specHeaderEnable;
  unsigned int m_ecsMepFactor;
  unsigned int m_mtuSize;
  std::string m_gbePortSel;
  std::string m_firmware;

  // ST specific part
  std::string m_pseudoHeaderLowThreshold;
  std::string m_pseudoHeaderHighThreshold;
  unsigned int m_zsEnable;
  unsigned int m_headerEnable;
  int m_headerLowThreshold, m_headerHighThreshold;
  std::vector< std::vector< int > > m_headerCorrectionAnalogLinks;
  unsigned int m_pedestalEnable;
  unsigned int m_updateEnable;
  unsigned int m_cmsEnable;
  unsigned int m_ppMaxClusters;
  std::vector< int > m_disableBeetles;
  std::vector< std::string > m_disableLinksPerBeetle;
  std::vector< std::string > m_pcnSelect;
  std::vector< int > m_pedestalValue;
  std::vector< int > m_pedestalMask;
  std::vector< int > m_hitThreshold;
  std::vector< int > m_cmsThreshold;
  std::vector< int > m_spilloverThreshold;
  std::vector< int > m_confirmationThreshold;
  std::vector< int > m_startStripChan023;
  std::vector< int > m_startStripChan2447;

  // Data ram generator
  std::vector< std::vector< std::vector< std::vector< int > > > > m_dataRAMGenerator;
};
#endif // STVETRACOND_H
