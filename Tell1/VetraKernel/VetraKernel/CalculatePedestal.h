// $Id: CalculatePedestal.h,v 1.1.1.1 2008-05-23 12:54:39 szumlat Exp $
#ifndef KERNEL_CALCULATEPEDESTAL_H 
#define KERNEL_CALCULATEPEDESTAL_H 1

// Include files

/** @class CalculatePedestal CalculatePedestal.h Kernel/CalculatePedestal.h
 *  
 *  in principle the functor may be used to calculate difference
 *  between objects stored in two containers of values, but it is
 *  assumed that minued is casted to the type of subtrahend
 *  it is better to use CalculateDifference predicate in the case
 *  when both minued and subtrahend are of the same type
 *
 *  @author Tomasz Szumlak
 *  @date   2006-06-10
 */

template <class T1, class T2> 
class CalculatePedestal {
public: 

  T2 operator()(T1& inData, T2& pedSubData)
  {
    T2 tempData=static_cast<T2>(inData);
    return ( tempData-pedSubData );
  }

protected:

private:

};
#endif // KERNEL_CALCULATEPEDESTAL_H
