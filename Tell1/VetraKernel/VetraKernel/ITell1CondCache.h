// $Id: $
#ifndef KERNEL_ITELL1CONDCACHE_H 
#define KERNEL_ITELL1CONDCACHE_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

namespace Velo 
{
  static const InterfaceID IID_ITell1CondCache ( "ITell1CondCache", 1, 0 );

  /** @class ITell1CondCache ITell1CondCache.h Kernel/ITell1CondCache.h
   *  
   *  Provides access to VELO Tell1 conditions.
   *
   *  Implementations should provide reasonable default values
   *  for all parameters in case CondDB access is not available
   *  or turned off by the user.
   * 
   *  @author Kurt Rinnert
   *  @date   2010-07-29
   */

  class ITell1CondCache : virtual public IAlgTool {

    public: 

      // Return the interface ID
      static const InterfaceID& interfaceID() { return IID_ITell1CondCache; }

      /// get strip-ordered hit thresholds for given sensor number, including dummies
      virtual const std::vector<unsigned char>& hitThresholds( unsigned int sensorNumber )=0;

      /// get strip-ordered hit thresholds for given sensor number, no dummies
      virtual const std::vector<unsigned char>& hitThresholdsNoDummy( unsigned int sensorNumber )=0;

  };
}
#endif // KERNEL_ITELL1CONDCACHE_H
