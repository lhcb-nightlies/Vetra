// $Id: CLIDSTVetraCondition.h,v 1.1 2009-05-12 11:48:06 jluisier Exp $
#ifndef VETRAKERNEL_CLIDSTVETRACONDITION_H 
#define VETRAKERNEL_CLIDSTVETRACONDITION_H 1

// Include files

/** @class CLIDSTVetraCondition CLIDSTVetraCondition.h VetraKernel/CLIDSTVetraCondition.h
 *  
 *
 *  @author Johan Luisier
 *  @date   2009-05-08
 */
static const CLID CLID_STVetraCondition = 9105;

#endif // VETRAKERNEL_CLIDSTVETRACONDITION_H
