// $Id: IVeloBadChannels.h,v 1.1.1.1 2008-05-23 12:54:39 szumlat Exp $
#ifndef IVELOBADCHANNELS_H 
#define IVELOBADCHANNELS_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_IVeloBadChannels ( "IVeloBadChannels", 1, 0 );

/** @class IVeloBadChannels IVeloBadChannels.h
 *  
 *
 *  @author Jianchun Wang
 *  @date   2007-10-11
 */
class IVeloBadChannels : virtual public IAlgTool 
{
public:
  
  typedef std::vector<std::pair<int,int> > ChannelList ;
   
public:
  
  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IVeloBadChannels; }
  virtual const ChannelList& badTell1Channels()  const = 0 ;
  virtual const ChannelList& badTell1Strips()    const = 0 ;
  virtual const ChannelList& badSensorChannels() const = 0 ;
  virtual const ChannelList& badSensorStrips()   const = 0 ;

protected:
  
private:

};
#endif // IVELOBADCHANNELS_H
