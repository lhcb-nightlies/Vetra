// $Id: TableInOut.h,v 1.1.1.1 2008-05-23 12:54:39 szumlat Exp $
#ifndef KERNEL_TABLEINOUT_H 
#define KERNEL_TABLEINOUT_H 1

// Include files
#include <sys/types.h>
#include <algorithm>

/**  TableInOut TableInOut.h Kernel/TableInOut.h
 *  A set of useful functions for initialization and
 *  writing of c tables used throughout TELL1Engine package
 *
 *  @see TELL1Engine
 *  @author Tomasz Szumlak
 *  @date   2007-07-31
 */

template<class T>
void tableInit(T* inData, const size_t size, int init=0)
{
  // the tables are meant to store integers
  T* begin=inData;
  T* end=begin+size;
  std::fill(begin, end, init);
}
#endif // KERNEL_TABLEINOUT_H
