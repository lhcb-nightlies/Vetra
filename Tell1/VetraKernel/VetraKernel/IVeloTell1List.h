// $Id: IVeloTell1List.h,v 1.1.1.1 2008-05-23 12:54:39 szumlat Exp $
#ifndef IVELOTELL1LIST_H 
#define IVELOTELL1LIST_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_IVeloTell1List ( "IVeloTell1List", 1, 0 );

/** @class IVeloTell1List IVeloTell1List.h
 *  
 *
 *  @author Jianchun Wang
 *  @date   2007-10-12
 */
class IVeloTell1List : virtual public IAlgTool {
public:
  //  typedef std::vector<std::int> VeloTell1List;
  typedef std::vector<int> sdataVec;

public:

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IVeloTell1List; }
  virtual const sdataVec& veloTell1List()   const=0;
  virtual const sdataVec& veloTell1Index()  const=0;
  virtual const sdataVec& veloSensorList()  const=0;
  virtual const sdataVec& veloSensorIndex() const=0;
  virtual int veloNumberOfTell1s() const=0;
  
protected:

private:

};
#endif // IVELOTELL1LIST_H
