// $Id: VeloTELL1Algorithm.h,v 1.4 2009-11-17 20:30:15 szumlat Exp $
#ifndef VELOTELL1ALGORITHM_H 
#define VELOTELL1ALGORITHM_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// form TELL1 Kernel
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloTell1Core.h"

// from TELL1 event
#include "VeloEvent/VeloTELL1Data.h"
#include "VeloEvent/VeloProcessInfo.h"
#include "VeloNZSInfo.h"
//#include "VeloEvent/VeloNZSInfo.h"

// DAQ Event
#include "Event/RawEvent.h"
#include "Event/RawBank.h"

// VeloDet
#include "VeloDet/DeVelo.h"

// boost
#include <boost/lexical_cast.hpp>
#include <boost/assign/std/vector.hpp>

/** @class VeloTELL1Algorithm VeloTELL1Algorithm.h
 *  
 *  base class for any TELL1 algorithm
 *
 *  @author Tomasz Szumlak
 *  @date   2006-12-06
 */

class VeloTELL1Algorithm : public GaudiAlgorithm {
public: 

  /// Standard constructor
  VeloTELL1Algorithm( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloTELL1Algorithm( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:
  
  virtual StatusCode getData();
  virtual StatusCode getNZSSvc();
  virtual VeloNZSInfo* nzsSvc() const;
  virtual StatusCode INIT();
  virtual StatusCode writeData();
  virtual StatusCode cloneData();
  virtual void setInputDataLoc(const std::string inValue);
  virtual void setOutputDataLoc(const std::string inValue);
  virtual void setTELL1Process(const unsigned int inValue);
  virtual void setAlgoName(const std::string inValue);
  virtual const std::string algoName() const;
  virtual void setAlgoType(const std::string inValue);
  virtual const std::string algoType() const;
  virtual LHCb::VeloTELL1Datas* inputData() const;
  virtual void outputData(LHCb::VeloTELL1Data* inData);
  virtual LHCb::VeloTELL1Datas* outputData();
  virtual void setIsEnable(bool flag);
  virtual bool isEnable() const;
  virtual unsigned int convergenceLimit() const;
  virtual unsigned int runType() const;
  virtual bool isInitialized() const override;
  virtual StatusCode readDataFromFile(const char* fileName,
                                      VeloTELL1::sdataVec& inVec);
  virtual StatusCode writeDataToFile(const char* fileName,
                                     VeloTELL1::sdataVec& inVec);
  virtual StatusCode inputStream(LHCb::VeloTELL1Datas* inVec) const;
  virtual void prepareData(bool insertAtEnd);
  virtual void prepareData(VeloTELL1::sdataVec& inVec) const;
  virtual DeVelo* deVelo() const;
  virtual void propagateReorderingInfo(const LHCb::VeloTELL1Data* inObj,
                                       LHCb::VeloTELL1Data* outObj);
  virtual VeloTELL1::AListPair getSrcIdList() const;
  virtual void setSrcIdList(std::vector<unsigned int> inVec);
  virtual StatusCode i_cacheSrcIdList();
  virtual StatusCode i_cacheConditions();

  /**
   * Register the function i_cacheConditions and i_cacheSrcIdList with the 
   * UpdateManagerSvc for updates of all conditions for the TELL1 boards and
   * the source ID list.
   * Note: When a class inherits from this class, the subclass's version of the
   * callbacks will be used.
   */
  virtual void regUpdateTell1Conds();


  bool m_isDebug;
  unsigned int m_evtNumber;
  bool m_validationRun;
  unsigned int m_dbConfig;
  std::string m_inputDataLoc;
  std::string m_outputDataLoc;
  std::string m_algorithmName;
  std::string m_condPath;
  std::vector<unsigned int> m_srcIdList;  /// list of the TELL1 numbers
  bool m_forceEnable;
  VeloTELL1::sdataVec m_dataBuffer;
  
private:
  StatusCode callCacheConditions();
  StatusCode callCacheSrcIdList();

  LHCb::RawEvent* m_rawEvent;
  std::string m_rawEventLoc;
  DeVelo* m_detElement;
  LHCb::VeloTELL1Datas* m_inputData;
  LHCb::VeloTELL1Datas* m_outputData;
  VeloNZSInfos* m_nzsSvc;
  std::string m_nzsSvcLoc;
  VeloProcessInfos* m_procInfos;
  std::string m_procInfosLoc;
  std::string m_algorithmType;
  unsigned int m_tell1Process;
  bool m_isEnable;
  unsigned int m_convergenceLimit;
  bool m_isInitialized;
  unsigned int m_runType;
  bool m_buildSrcIdFromData;

};
#endif // VELOTELL1ALGORITHM_H
