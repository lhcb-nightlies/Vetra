// $Id: STVetraCfg.h,v 1.4 2010-03-10 10:24:08 jluisier Exp $
#ifndef STVETRACFG_H 
#define STVETRACFG_H 1

// Include files
// from Gaudi
#include "Kernel/STToolBase.h"
#include "VetraKernel/ISTVetraCfg.h"            // Interface

#include "Kernel/STBoardMapping.h"

#include <iostream>

#include <map>
#include <string>

/** @class STVetraCfg STVetraCfg.h
 *  
 *
 *  @author Johan Luisier
 *  @date   2009-04-24
 */
class STVetraCfg : public ST::ToolBase, virtual public ISTVetraCfg {
public: 
  /// Standard constructor
  STVetraCfg( const std::string& type, 
              const std::string& name,
              const IInterface* parent);

  virtual ~STVetraCfg( ); ///< Destructor

  StatusCode initialize() override;
  
  STVetraCondition* getSTVetraCond(const unsigned int& ID) const override;

  void setType( const std::string& detType ) override;

  const std::string& getDetType() const override;

  std::ostream& printOut(std::ostream& os) const override;

protected:

  StatusCode initCondition();

private:
  STBoardMapping::Map detMap;

  unsigned int m_NbrOfBoards;

  std::string m_condPath;

  std::map< unsigned int, STVetraCondition* > m_Conditions;
  
};
#endif // STVETRACFG_H
