!-----------------------------------------------------------------------------
! Package     : Velo/VetraKernel
! Responsible : Tomasz Szumlak
! Purpose     : Kernel package of the Vetra project
!-----------------------------------------------------------------------------

!========================= VetraKernel v3r3 2015-11-06 =======================
! 2014-09-01 - David Hutchcroft
 - removed ; from the end of a namespace (fixes cmake warnings)

! 2014-08-29 - David Hutchcroft
 - removed ; from DECLARE_ALGORITHM_FACTORY lines (fixes cmake warnings)

!========================= VetraKernel v3r2 2014-04-03 =======================
! 2014-03-18 - Marco Clemencic
 - Added CMake configuration file.
 - Added missing #include (required with the latest Gaudi).

!========================= VetraKernel v3r1 2013-07-12 =======================
! 2013-07-08 - David Hutchcroft
 - Removed Reflex factory from base class VeloTELL1Algorithm.cpp
   Fixes genconf.exe warnings

!========================= VetraKernel v3r0 2012-10-11 =======================
! 2012-10-09 - David Hutchcroft
 - Removed compilation warnings: changes an IVeloTell1List.h return type, needs a new version

!========================= VetraKernel v2r0 2012-04-01 =======================
! 2012-02-14 - Eduardo Rodrigues
 - Dummy entry: package version should be changed to a new major number (v2r0)
   given the backward-incompatible changes in ST code

!========================= VetraKernel v1r19 2012-02-13 ======================
! 2012-02-11 - Marius Bjoernstad
 - Fixed a bug causing the conditions for TELL1 #41 to not be updated for the 
   correct time

!========================= VetraKernel v1r18 2012-02-08 ======================
! 2012-02-07 - F Dupertuis
 - Add methods pseudoHeaderHighThreshold and pseudoHeaderLowThreshold in 
	 STVetraCondition

!========================= VetraKernel v1r17 2011-10-03 ======================
! 2011-09-28 - Marius Bjoernstad
 - Added a function to correctly register TELL1-related conditions with the 
   UpdateManagerSvc

!========================= VetraKernel v1r16 2011-02-08 ======================
! 2011-02-07 - F Dupertuis
 - Add method void headerEnable(const unsigned int& value) in STVetraCondition

!========================= VetraKernel v1r15 2010-09-11 ======================
! 2010-09-11 - Tomasz Szumlak
 - VeloTELL1Algorithm base class modification to use nzsInfo service

!========================= VetraKernel v1r14 2010-08-05 ======================
! 2010-08-05 - Kurt Rinnert
 - added tool interface for TELL1 conditions cache
 - added ability to use hit thresholds for signal filtering in TELL1Noise
   utility

! ========= 2010-04-20 - T Szumlak v1r13 ======================================
! 2010-04-06 - Eduardo Rodrigues
 - Added a new algorithm VetraInit (basically the same as DaVinciInit)

! 2010-04-06 J Luisier
 - Added a set method for pedestal update in STVetraCondition.

 ========= 2010-03-25 - T Szumlak v1r12 ======================================
! 2010-03-25 - Tomasz Szumlak
 - move the tag for the new Vetra release

! 2010-03-25 - Tomasz Szumlak
 - remove genConf directory and install.history file
 
! 2010-03-10 J Luisier
 - Changed the way STVetraCondition were retrieved in STVetraCfg.
 - Added a printOut method in STVetraCfg, which writes all the
    STVetraConditions in a file.
 - Added some set methods in STVetraCondition (to allow updates)

! 2010-02-18 - Tomasz Szumlak
 - modification to the base class - made a change that was suppose to 
   be harmless but it affected the jobs that runs over Round-Robin data

 ========= 2010-02-17 - T Szumlak v1r11 ======================================
! 2010-02-17 - Tomasz Szumlak
 - move tag for the patch release

! 2010-02-17 - Kurt Rinnert
 - added PU sensors to default src ID list in TELL1 algorithm base class

 ========= 2010-02-14 - T Szumlak v1r10 ======================================
! 2010-02-14 - Tomasz Szumlak
 - bug fix in the VeloTELL1Algorithm base clase, it was introduced by mistake
   and caused VeloTELL1ClusterMaker run idley
 - move tag for the new release

! 2010-01-14 - Jeroen van Tilburg
 - Fix in STVetraCfg to allow for different paths in the Tell1 conditions using
   job options.

 ========= 2009-12-15 - T Szumlak v1r10 ======================================
 - move tag for the new release

! 2009-11-30 - Tomasz Szumlak
 - add install_python_modules in requirements

 ========= 2009-11-20 - T Szumlak v1r9 ======================================
! 2009-11-20 - Tomasz Szumlak
 - release candidate for Vetra v8r0

! 2009-11-19 - Kurt Rinnert
 - extended noise evaluator interface to allow for access to reordering state
   of underlying container and explicit reset.
 
! 2009-11-18 J Luisier
 - Changes in StVetraCfg and ISTVetraCfg, add a getDetType() method

! ========= 2009-11-17 - T Szumlak v1r8 ======================================
! 2009-09-16 - T Szumlak
 - adapt to runnig with the round-robin data
 
! 2009-09-22 J Luisier
 - Changed the XML tag names in STVetraCondition, the access methods were
   untouched.


! ========= 2009-09-16 - T Szumlak v1r7 ======================================
! 2009-09-16 - T Szumlak
 - release candidate for Vetra v7r2

! 2009-09-08 - Kurt Rinnert
 - removed too interfaces now residing in Tell1Kernel

! 2009-08-27 - Kurt Rinnert
 - moved new tool interfaces into Velo namespace

! 2009-08-21 - Kurt Rinnert
 - added tool inteface for time stamp providers

! 2009-08-11 - Kurt Rinnert
 - added tool interface for mapping TELL1 hardware IDs to sofware sensor
   numbers and vice versa.

! 2009-08-03 - T Szumlak
 - changes to VeloTELL1Algorithm that allow proper Tell1 data formatting
   necessary for the dataTranslator - first step in emulation

! 2009-07-31 - J Luisier
 - Propagated changes in ST Vetra CondDB to STVetraCondition

! 2009-06-11 - J Luisier
 - commit the forgotten XML converter...

! 2009-05-15 - T Szumlak v1r6
 - release candidate for Vetra v7r0

! 2009-05-15 - J Luisier
 - Changed the way to access disabled ports and beetles

! 2009-05-12 - J Luisier
 - Add STVetraCondition class, with CLID_STVetraCondition
 - Add ISTVetraCfg interface and STVetraCfg tool
 - Now requires ST/STKernel
 - Python dictionnary added

! 2008-09-02 - T Szumlak - v1r5
 - change in VeloTELL1Algorithm base class - return StatusCode::SUCCESS
   when the algorithm is disabled to be in concert with subequent
   checkings done in specialized algortithms

! 2008-09-02 - T Szumlak - v1r4
 - change in VeloTELL1Algorithm base calass - it is now allowed to have
   convergence limit equal to 0 - this is fine if the job is run with
   trained conditions database 

! 2008-08-31 - Kurt Rinnert
 - the TELL1 algorithm base class no longer treats missing input container as
   an error.

! 2008-05-25 - T Szumlak - v1r3
 - updated VeloTELL1Algorithm base class code - new condition method added
   to get Tell1 src id list from the data base when running in Dynamic mode

! 2008-05-25 - T Szumlak
 - First import of this package at new location Tell1/VetraKernel

! 2008-03-03 - Kurt Rinnert
 - speed up of VeloTell1Algorithm::prepareData() methods by a factor of >100

! 2007-12-05 - Tomasz Szumlak
 - first import of the package
