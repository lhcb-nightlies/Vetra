// $Id: TELL1Noise.cpp,v 1.1.1.1 2008-05-23 12:54:39 szumlat Exp $
// ============================================================================
// Include files 
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/GaudiException.h"
// ============================================================================
// GaudiAlgorithm
// ============================================================================
#include "GaudiAlg/GaudiAlgorithm.h"
// ============================================================================
// VeloTELL1Event
// ============================================================================
#include "VeloEvent/VeloTELL1Data.h"
// ============================================================================
// VeloTELL1Kernel
// ============================================================================
#include "Tell1Kernel/VeloDecodeConf.h"
#include "Tell1Kernel/VeloDecodeCore.h"
#include "Tell1Kernel/VeloTell1Core.h"


#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloRType.h"
#include "VeloDet/DeVeloPhiType.h"


// ============================================================================
// local
// ============================================================================
#include "VetraKernel/TELL1Noise.h"
// ============================================================================

/** @file 
 *  The implementation file for the class Velo::Monitoring::TELL1Noise 
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu 
 *  @date   2007-10-25
 */
// ============================================================================
// Standard constructor
// ============================================================================
Velo::Monitoring::TELL1Noise::TELL1Noise() 
  : m_stats ()
{
  
}

// ============================================================================
// destructor
// ============================================================================
Velo::Monitoring::TELL1Noise::~TELL1Noise(){}

// ============================================================================
/*  fill the counters with the data for one event 
 *  @param data    input data 
 *  @param nReset  a handle to reset the counters if number of entries is large 
 *  @param nSigma  a handle to suppress too large  signals 
 *  @param nMin    if number of entries is too small, one can not rely on sigma
 *  @return number of updated counters 
 */
// ============================================================================
unsigned int Velo::Monitoring::TELL1Noise::fill 
( const LHCb::VeloTELL1Data& data   , 
  const unsigned int         nReset , 
  const double               nSigma ,
  const unsigned int         nMin   ) 
{

  if ( m_stats.size() < VeloTELL1::SENSOR_CHANNELS ) 
  { 
    m_stats.resize ( VeloTELL1::SENSOR_CHANNELS ) ; 
    reset() ;
  }
  unsigned long success = 0 ;


  for(int index=0; index<VeloTELL1::NumberOfALinks*VeloTELL1::CHANNELS; ++index){
    StatEntity&  stat  = m_stats [ index ] ;      
    double value =0;
    if(data.isReordered() == false)
      value = data.channelADC(index);
    else
      value = data.stripADC ( index );

    // reset the counter (if required)
    if ( 0 < nReset  && nReset < stat.nEntries() ) { stat.reset() ; }
    // skip "too-large" signals (if required)
    if ( 0     < nSigma           && 
        nMin  < stat.nEntries () && 
        value > stat.flagMean () + nSigma * stat.flagRMS () ) { continue ; }
    stat += value;      
    ++success ;


  }
  return success ;
}

// ============================================================================
/*  fill the counters with the data for one event 
 *  @param data    input data 
 *  @param thesholds signal thresholds
 *  @param nReset  a handle to reset the counters if number of entries is large 
 *  @return number of updated counters 
 */
// ============================================================================
unsigned int Velo::Monitoring::TELL1Noise::fill 
( const LHCb::VeloTELL1Data& data   ,
  const std::vector<unsigned char>& thresholds, 
  const unsigned int         nReset )
{

  if ( m_stats.size() < VeloTELL1::SENSOR_CHANNELS ) 
  { 
    m_stats.resize ( VeloTELL1::SENSOR_CHANNELS ) ; 
    reset() ;
  }
  unsigned long success = 0 ;


  for(int index=0; index<VeloTELL1::NumberOfALinks*VeloTELL1::CHANNELS; ++index){
    StatEntity&  stat  = m_stats [ index ] ;      
    double value =0;
    int strip = index;
    if(data.isReordered() == false) {
      value = data.channelADC(index);
      if (data.isPhi()) {
        strip = m_phiSensor->ChipChannelToStrip(strip);  
      } else {
        strip = m_rSensor->ChipChannelToStrip(strip);  
      }
    } else {
      value = data.stripADC ( index );
    }
      
    // reset the counter (if required)
    if ( 0 < nReset  && nReset < stat.nEntries() ) { stat.reset() ; }

    // skip signals, protect against empty list for PU
    double thresh = ( thresholds.empty() ? 10.0 : static_cast<char>(thresholds[strip]) ); 
    if ( value > thresh )  { continue ; }

    stat += value;      
    ++success ;
  }
      
  return success ;
}

// ============================================================================
// evaluate the average number of entries
// ============================================================================
double Velo::Monitoring::TELL1Noise::meanEntries () const 
{
  StatEntity sum ;
  for ( Counters::const_iterator ic = m_stats.begin() ; m_stats.end() != ic ; ++ic ) 
  { sum += ic->nEntries() ; }
  return sum.flagMean () ;
}
// ============================================================================
// compare two objects  (by the average number of entries)
// ============================================================================
bool Velo::Monitoring::TELL1Noise::operator<
  ( const Velo::Monitoring::TELL1Noise& other ) const 
{
  if ( this == &other ) { return false ; }
  return meanEntries() < other.meanEntries() ;
}  
// ============================================================================
// reset all counters 
// ============================================================================
void Velo::Monitoring::TELL1Noise::reset() 
{
  for ( Counters::iterator ic = m_stats.begin() ; m_stats.end() != ic ; ++ic ) 
  { ic -> reset () ; }
}
// ============================================================================


// ============================================================================
// constructor:
// ============================================================================
Velo::Monitoring::NoisePair::NoisePair() 
  : m_first  (   ) 
  , m_second ( 0 ) 
{}
// ============================================================================
// destructor
// ============================================================================
Velo::Monitoring::NoisePair::~NoisePair() 
{ if ( 0 != m_second ) { delete m_second ; m_second = 0 ; } }
// ============================================================================
// activate the second 
// ============================================================================
void Velo::Monitoring::NoisePair::activate() 
{ if ( 0 == m_second ) { m_second = new Velo::Monitoring::TELL1Noise() ; } }
// ============================================================================
// get the best 
// ============================================================================
const Velo::Monitoring::TELL1Noise* 
Velo::Monitoring::NoisePair::best() const 
{
  if ( 0 == m_second ) { return first() ; }
  return  ( m_first < *m_second ) ? second() : first () ;
}
// ============================================================================
// copy constructor 
// ============================================================================
Velo::Monitoring::NoisePair::NoisePair
( const Velo::Monitoring::NoisePair& right ) 
  : m_first ( right.m_first ) 
  , m_second ( 0 ) 
{
  if ( 0 != right.m_second )
  { m_second = new Velo::Monitoring::TELL1Noise( *(right.m_second) ) ; }
}
// ============================================================================
// reset all counters 
// ============================================================================
void Velo::Monitoring::NoisePair::reset() 
{
  m_first.reset() ;
  // hm....
  if ( 0 != m_second )
  {
    delete m_second ;
    m_second = 0 ;
  }
}
// ============================================================================


// ============================================================================
// The END
// ============================================================================
