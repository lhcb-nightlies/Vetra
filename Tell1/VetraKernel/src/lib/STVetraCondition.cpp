// $Id: STVetraCondition.cpp,v 1.7 2010-04-06 09:06:13 jluisier Exp $
// Include files 

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/StatusCode.h"
#include "GaudiAlg/GaudiAlg.h"
#include "DetDesc/DetDesc.h"

#include "Kernel/STLexicalCaster.h"

// local
#include "VetraKernel/STVetraCondition.h"

using namespace std;
using namespace ST;

// to be removed
#include <iostream>

//-----------------------------------------------------------------------------
// Implementation file for class : STVetraCondition
//
// 2009-04-24 : Johan Luisier
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STVetraCondition::STVetraCondition(  )
  : Condition (),
    isRead ( false ),
    m_ppMaxClusters ( 0 )
{
  vector< int > tensor1;
  vector< vector< int > > tensor2(4, tensor1);
  vector< vector< vector< int > > > tensor3(6, tensor2);
  m_dataRAMGenerator.assign( 4, tensor3 );
}

STVetraCondition::STVetraCondition( Condition* cond )
  : Condition ( *cond ),
    isRead ( false ),
    m_ppMaxClusters ( 0 )
{
}

//=============================================================================
// Destructor
//=============================================================================
STVetraCondition::~STVetraCondition() {}

StatusCode STVetraCondition::initialize()
{
  try
  {
    m_cmsEnable = param< int >("lcms_enable");
    m_cmsThreshold = param< vector< int > >("cms_threshold");
    //m_commonVersion = param< int >("common_ver");
    m_confirmationThreshold = param< vector< int > >("confirmation_threshold");
    m_consecTrigger = param< int >("consecutive_n");
    m_dataGenEnabled = param< int >("detector_data_generator_enabled");
    m_destIp = param< vector< int > >("ip_dest_addr");
    m_destMac = param< vector< string > >("mac_dest_addr");
    m_detectorId = param< int >("DETECTOR_ID");
    m_disableBeetles = param< vector< int > >("orx_beetle_disable_0");
    vector< int > tmpBeetle = param< vector< int > >("orx_beetle_disable_1");
    m_disableBeetles. insert( m_disableBeetles.end(), tmpBeetle.begin(), tmpBeetle.end() );
    m_disableLinksPerBeetle = param< vector< string > >("arx_beetle_disable_0");
    vector< string > tmpLink = param< vector< string > >("arx_beetle_disable_1"); 
    m_disableLinksPerBeetle . insert( m_disableLinksPerBeetle.end(), tmpLink.begin(), tmpLink.end() );
    m_ecsMepFactor = param< int >("MEP_FACTOR");
    m_ecsTrigType = param< int >("TRIGGER_TYPE");
    m_errBankDisable = param< int >("error_bank_disable");
    m_ethernetTime = param< string >("TTL");
    m_ethernetType = param< string >("ethernet_type");
    m_extTriggerInputEnable = param< int >("External_trigger_input_enable");
    m_feDataCheck = param< string >("FE_data_check_enable");
    m_firmware = param< string >("Firmware");
    m_gbeCtrl = param< string >("gbe_forced_stop_cycle");
    m_gbePortSel = param< string >("HLT_PORT");

    for (unsigned int pp(0), beetle, link; pp < 4; pp++)
    {
      for (beetle = 0; beetle < 6; beetle++)
      {
        for (link = 0; link < 4; link++)
          m_dataRAMGenerator[pp][beetle][link] = param< vector< int > >( "st_data_gen_array_" + toString(pp) + "_"
                                                                         + toString(beetle) + "_" + toString(link) );
      }
    }

    for (unsigned int i(0); i < 96; i++)
      m_headerCorrectionAnalogLinks.push_back( param< vector< int > >("header_corr_value_" + toString( i, 2 ) ) );

    m_headerEnable = param< int >("header_correction_enable");
    m_headerLength = param< string >("IHL");
    m_headerLowThreshold  = param< int >("header_corr_threshold_lo");
    m_headerHighThreshold = param< int >("header_corr_threshold_hi");
    m_hitThreshold = param< vector< int > >("hit_threshold");
    m_ipVersion = param< string >("ip_version");
    m_infoBankDisable = param< int >("force_info_disable");
    m_infoBankEnable = param< int >("force_info_enable");
    m_mtuSize = param< int >("MTU_SIZE");
    m_nzsBankOffset = param< string >("nzs_bank_offset");
    m_nzsBankPeriod = param< string >("nzs_bank_period_factor");
    m_nzsErrorEnable = param< int >("nzs_for_error_enable");
    m_nextProtocol = param< string >("next_level_protocol");
    m_pcnSelect = param< vector< string > >("pcn_select");
    m_ppDerandomThreshold = param< int >("pp_derandmizer_event_threshold");
    m_ppMaxClusters = param< int >("cluster_number_max");
    m_pedBankSchedule = param< int >("pedestal_bank_schedule");
    m_pedestalEnable = param< int >("pedestal_enable");
    m_pedestalMask = param< vector< int > >("pedestal_mask");
    m_pedestalValue = param< vector< int > >("pedestal");
    m_pseudoHeaderHighThreshold = param< string >("Pseudo_header_hi_thr");
    m_pseudoHeaderLowThreshold  = param< string >("Pseudo_header_lo_thr");
    m_sepGenEnabled = param< int >("SEP_generator_enabled");
    m_seriousErrThroEnable = param< string >("serious_err_throttle_enable");
    m_serviceType = param< string >("service_type");
    m_sourceIp = param< vector< int > >("ip_source_addr");
    m_sourceMac = param< vector< string > >("mac_source_addr");
    m_specHeaderEnable = param< int >("detector_specific_bank_header_enable");
    m_spilloverThreshold = param< vector< int > >("spill_over_threshold");
    m_ttcDestIpAvailable = param< int >("TTC_dest_ip_available");
    m_ttcInfoEnable = param< int >("TTC_info_enable");
    m_ttcTriggerTypeAvailable = param< int >("TTC_trigger_type_available");
    m_tellName = param< string >("Tell_name");
    m_triggerNumber = param< string >("trigger_n");
    m_updateEnable = param< int >("pedestal_auto_update_enable");
    m_waitCycle = param< int >("wait_cycle");
    m_zsEnable = param< int >("zerosuppress_enable");
    m_classNzsBank = param< string >("non_zs_class");
    m_classZsBank = param< string >("zs_class");
    m_classErrBank = param< string >("error_class");
    m_classPedBank = param< string >("pedestal_class");
    m_mepPid = param< string >("mep_pid");
    m_sourceId = param< string >("source_id");
    m_startStripChan023 = param< vector< int > >("start_strip_n_0");
    m_startStripChan2447 = param< vector< int > >("start_strip_n_1");
    m_version = param< string >("version");
  }
  catch(GaudiException &e)
  {
    throw e;
    return StatusCode::FAILURE;
  }

  isRead = true;

  return StatusCode::SUCCESS;
}

bool STVetraCondition::isAnyDisable() const
{
  for (unsigned int i(0); i < 24; i++)
  {
    if ( m_disableBeetles[i] != 0 )
      return true;
    else if ( m_disableLinksPerBeetle[i] != "0x0" )
      return true;
  }

  return false;
}

std::ostream& STVetraCondition::printOut(std::ostream& os) const
{
  unsigned int i;
  
  for (i = 0; i < 24; i++)
  {
    if ( m_disableBeetles[i] != 0 )
      os << "Link " << i << " is disabled" << endmsg;
  }
  
  for (i = 0; i < 24; i++)
  {
    if ( m_disableBeetles[i] == 0 &&
         m_disableLinksPerBeetle[i] != "0x0")
      os << "Link " << i << " has disabled port(s) : "
         << m_disableLinksPerBeetle[i] << endmsg;
  }
  
  return os;
}

std::ostream& operator<<(std::ostream& os, const STVetraCondition& stCond)
{
  return stCond.printOut( os );
}

std::ostream& operator<<(std::ostream& os, STVetraCondition* stCond)
{
  return stCond -> printOut( os );
}


// Access methods

unsigned int STVetraCondition::cmsEnable() const
{
  return m_cmsEnable;
}

const vector< int >* STVetraCondition::cmsThreshold() const
{
  return &m_cmsThreshold;
}

const vector< int >* STVetraCondition::confirmationThreshold() const
{
  return &m_confirmationThreshold;
}

unsigned int STVetraCondition::consecTrigger() const
{
  return m_consecTrigger;
}

unsigned int STVetraCondition::dataGenEnabled() const
{
  return m_dataGenEnabled;
}

const vector< int >* STVetraCondition::destIp() const
{
  return &m_destIp;
}

const vector< string >* STVetraCondition::destMac() const
{
  return &m_destMac;
}

unsigned int STVetraCondition::detectorId() const
{
  return m_detectorId;
}

const vector< int >* STVetraCondition::disableBeetles() const
{
  return &m_disableBeetles;
}

const vector< string >* STVetraCondition::disableLinksPerBeetle() const
{
  return &m_disableLinksPerBeetle;
}

unsigned int STVetraCondition::ecsMepFactor() const
{
  return m_ecsMepFactor;
}

unsigned int STVetraCondition::ecsTrigType() const
{
  return m_ecsTrigType;
}

unsigned int STVetraCondition::errBankDisable() const
{
  return m_errBankDisable;
}

string STVetraCondition::ethernetTime() const
{
  return m_ethernetTime;
}

string STVetraCondition::ethernetType() const
{
  return m_ethernetType;
}

unsigned int STVetraCondition::extTriggerInputEnable() const
{
  return m_extTriggerInputEnable;
}

string STVetraCondition::feDataCheck() const
{
  return m_feDataCheck;
}

string STVetraCondition::firmware() const
{
  return m_firmware;
}

string STVetraCondition::gbeCtrl() const
{
  return m_gbeCtrl;
}

string STVetraCondition::gbePortSel() const
{
  return m_gbePortSel;
}

const std::vector< int >* STVetraCondition::dataRAMGenerator(const unsigned int& pp, const unsigned int& beetle,
                                                             const unsigned int& link) const
{
  if ( pp >= 4 || beetle >= 6 || link >= 4 )
    return 0;
  else
    return &m_dataRAMGenerator[pp][beetle][link];
}

const vector< int >* STVetraCondition::headerCorrectionAnalogLink(const unsigned int& link) const
{
  if ( link < m_headerCorrectionAnalogLinks.size() )
    return &m_headerCorrectionAnalogLinks[ link ];
  else
    return 0;
}

unsigned int STVetraCondition::headerEnable() const
{
  return m_headerEnable;
}

string STVetraCondition::headerLength() const
{
  return m_headerLength;
}

int STVetraCondition::headerLowThreshold() const
{
  return m_headerLowThreshold;
}

int STVetraCondition::headerHighThreshold() const
{
  return m_headerHighThreshold;
}

const vector< int >* STVetraCondition::hitThreshold() const
{
  return &m_hitThreshold;
}

string STVetraCondition::ipVersion() const
{
  return m_ipVersion;
}

unsigned int STVetraCondition::infoBankDisable() const
{
  return m_infoBankDisable;
}

unsigned int STVetraCondition::infoBankEnable() const
{
  return m_infoBankEnable;
}

unsigned int STVetraCondition::mtuSize() const
{
  return m_mtuSize;
}

string STVetraCondition::nzsBankOffset() const
{
  return m_nzsBankOffset;
}

string STVetraCondition::nzsBankPeriod() const
{
  return m_nzsBankPeriod;
}

unsigned int STVetraCondition::nzsErrorEnable() const
{
  return m_nzsErrorEnable;
}

string STVetraCondition::nextProtocol() const
{
  return m_nextProtocol;
}

const vector< string >* STVetraCondition::pcnSelect() const
{
  return &m_pcnSelect;
}

unsigned int STVetraCondition::ppDerandomThreshold() const
{
  return m_ppDerandomThreshold;
}

unsigned int STVetraCondition::ppMaxClusters() const
{
  return m_ppMaxClusters;
}

unsigned int STVetraCondition::pedBankSchedule() const
{
  return m_pedBankSchedule;
}

unsigned int STVetraCondition::pedestalEnable() const
{
  return m_pedestalEnable;
}

const vector< int >* STVetraCondition::pedestalMask() const
{
  return &m_pedestalMask;
}

const vector< int >* STVetraCondition::pedestalValue() const
{
  return &m_pedestalValue;
}

string STVetraCondition::pseudoHeaderHighThreshold() const
{
  return m_pseudoHeaderHighThreshold;
}

string STVetraCondition::pseudoHeaderLowThreshold() const
{
  return m_pseudoHeaderLowThreshold;
}

unsigned int STVetraCondition::sepGenEnabled() const
{
  return m_sepGenEnabled;
}

string STVetraCondition::seriousErrThroEnable() const
{
  return m_seriousErrThroEnable;
}

string STVetraCondition::serviceType() const
{
  return m_serviceType;
}

const vector< int >* STVetraCondition::sourceIp() const
{
  return &m_sourceIp;
}

const vector< string >* STVetraCondition::sourceMac() const
{
  return &m_sourceMac;
}

unsigned int STVetraCondition::specHeaderEnable() const
{
  return m_specHeaderEnable;
}

const vector< int >* STVetraCondition::spilloverThreshold() const
{
  return &m_spilloverThreshold;
}

unsigned int STVetraCondition::ttcDestIpAvailable() const
{
  return m_ttcDestIpAvailable;
}

unsigned int STVetraCondition::ttcInfoEnable() const
{
  return m_ttcInfoEnable;
}

unsigned int STVetraCondition::ttcTriggerTypeAvailable() const
{
  return m_ttcTriggerTypeAvailable;
}

string STVetraCondition::tellName() const
{
  return m_tellName;
}

string STVetraCondition::triggerNumber() const
{
  return m_triggerNumber;
}

unsigned int STVetraCondition::updateEnable() const
{
  return m_updateEnable;
}

unsigned int STVetraCondition::waitCycle() const
{
  return m_waitCycle;
}

unsigned int STVetraCondition::zsEnable() const
{
  return m_zsEnable;
}

string STVetraCondition::classNzsBank() const
{
  return m_classNzsBank;
}

string STVetraCondition::classZsBank() const
{
  return m_classZsBank;
}

string STVetraCondition::classErrBank() const
{
  return m_classErrBank;
}

string STVetraCondition::classPedBank() const
{
  return m_classPedBank;
}

string STVetraCondition::mepPid() const
{
  return m_mepPid;
}

string STVetraCondition::sourceId() const
{
  return m_sourceId;
}

const vector< int >* STVetraCondition::startStripChan0_23() const
{
  return &m_startStripChan023;
}

const vector< int >* STVetraCondition::startStripChan24_47() const
{
  return &m_startStripChan2447;
}

string STVetraCondition::version() const
{
  return m_version;
}

// set methods

void STVetraCondition::pedestalMask(const vector< int >& vec)
{
  if ( vec.size() == m_pedestalMask.size() )
  {
    m_pedestalMask.assign( vec.begin(), vec.end() );
    param< vector< int > >("pedestal_mask") = m_pedestalMask;
  }
}

void STVetraCondition::pedestalMask(const vector< int >* ptrVec)
{
  if ( ptrVec -> size() == m_pedestalMask.size() )
  {
    m_pedestalMask.assign( ptrVec -> begin(), ptrVec -> end() );
    param< vector< int > >("pedestal_mask") = m_pedestalMask;
  }
}

void STVetraCondition::pedestalValue(const vector< int >& vec)
{
  if ( vec.size() == m_pedestalValue.size() )
  {
    m_pedestalValue.assign( vec.begin(), vec.end() );
    param< vector< int > >("pedestal") = m_pedestalValue;
  }
}

void STVetraCondition::pedestalValue(const vector< int >* ptrVec)
{
  if ( ptrVec -> size() == m_pedestalValue.size() )
  {
    m_pedestalValue.assign( ptrVec -> begin(), ptrVec -> end() );
    param< vector< int > >("pedestal") = m_pedestalValue;
  }
}

void STVetraCondition::hitThreshold(const vector< int >& vec)
{
  if ( vec.size() == m_hitThreshold.size() )
  {
    m_hitThreshold.assign( vec.begin(), vec.end() );
    param< vector< int > >("hit_threshold") = m_hitThreshold;
  }
}

void STVetraCondition::hitThreshold(const vector< int >* ptrVec)
{
  if ( ptrVec -> size() == m_hitThreshold.size() )
  {
    m_hitThreshold.assign( ptrVec -> begin(), ptrVec -> end() );
    param< vector< int > >("hit_threshold") = m_hitThreshold;
  }
}

void STVetraCondition::cmsThreshold(const vector< int >& vec)
{
  if ( vec.size() == m_cmsThreshold.size() )
  {
    m_cmsThreshold.assign( vec.begin(), vec.end() );
    param< vector< int > >("cms_threshold") = m_cmsThreshold;
  }
}

void STVetraCondition::cmsThreshold(const vector< int >* ptrVec)
{
  if ( ptrVec -> size() == m_cmsThreshold.size() )
  {
    m_cmsThreshold.assign( ptrVec -> begin(), ptrVec -> end() );
    param< vector< int > >("cms_threshold") = m_cmsThreshold;
  }
}

void STVetraCondition::confirmationThreshold(const vector< int >& vec)
{
  if ( vec.size() == m_confirmationThreshold.size() )
  {
    m_confirmationThreshold.assign( vec.begin(), vec.end() );
    param< vector< int > >("confirmation_threshold") = m_confirmationThreshold;
  }
}

void STVetraCondition::confirmationThreshold(const vector< int >* ptrVec)
{
  if ( ptrVec -> size() == m_confirmationThreshold.size() )
  {
    m_confirmationThreshold.assign( ptrVec -> begin(), ptrVec -> end() );
    param< vector< int > >("confirmation_threshold") = m_confirmationThreshold;
  }
}

void STVetraCondition::spilloverThreshold(const vector< int >& vec)
{
  if ( vec.size() == m_spilloverThreshold.size() ){
    m_spilloverThreshold.assign( vec.begin(), vec.end() );
    param< vector< int > >("spill_over_threshold") = m_spilloverThreshold;
  }
}

void STVetraCondition::spilloverThreshold(const vector< int >* ptrVec)
{
  if ( ptrVec -> size() == m_spilloverThreshold.size() ){
    m_spilloverThreshold.assign( ptrVec -> begin(), ptrVec -> end() );
    param< vector< int > >("spill_over_threshold") = m_spilloverThreshold;
  }
}

void STVetraCondition::disableBeetles(const vector< int >& vec)
{
  if ( vec.size() == m_disableBeetles.size() )
  {
    m_disableBeetles.assign( vec.begin(), vec.end() );
    vector< int > tmp1, tmp2;
    tmp1.assign( m_disableBeetles.begin(), m_disableBeetles.begin() + 12 );
    tmp2.assign( m_disableBeetles.begin() + 12, m_disableBeetles.end() );
    param< vector< int > >("orx_beetle_disable_0") = tmp1;
    param< vector< int > >("orx_beetle_disable_1") = tmp2;
  }
}

void STVetraCondition::disableBeetles(const vector< int >* ptrVec)
{
  if ( ptrVec -> size() == m_disableBeetles.size() )
  {
    m_disableBeetles.assign( ptrVec -> begin(), ptrVec -> end() );
    vector< int > tmp1, tmp2;
    tmp1.assign( m_disableBeetles.begin(), m_disableBeetles.begin() + 12 );
    tmp2.assign( m_disableBeetles.begin() + 12, m_disableBeetles.end() );
    param< vector< int > >("orx_beetle_disable_0") = tmp1;
    param< vector< int > >("orx_beetle_disable_1") = tmp2;
  }
}

void STVetraCondition::disableLinksPerBeetle(const vector< string >& vec)
{
   if ( vec.size() == m_disableLinksPerBeetle.size() )
   {
     m_disableLinksPerBeetle.assign( vec.begin(), vec.end() );     
     vector< string > tmp1, tmp2;     
     tmp1.assign( m_disableLinksPerBeetle.begin(),
                  m_disableLinksPerBeetle.begin() + 12 );     
     tmp2.assign( m_disableLinksPerBeetle.begin() + 12,
                  m_disableLinksPerBeetle.end() );     
     param< vector< string > >("arx_beetle_disable_0") = tmp1;     
     param< vector< string > >("arx_beetle_disable_1") = tmp2;     
   }
}

void STVetraCondition::disableLinksPerBeetle(const vector< string >* ptrVec)
{
   if ( ptrVec -> size() == m_disableLinksPerBeetle.size() )
   {
     m_disableLinksPerBeetle.assign( ptrVec -> begin(), ptrVec -> end() );     
     vector< string > tmp1, tmp2;     
     tmp1.assign( m_disableLinksPerBeetle.begin(),
                  m_disableLinksPerBeetle.begin() + 12 );     
     tmp2.assign( m_disableLinksPerBeetle.begin() + 12,
                  m_disableLinksPerBeetle.end() );     
     param< vector< string > >("arx_beetle_disable_0") = tmp1;     
     param< vector< string > >("arx_beetle_disable_1") = tmp2;     
   }
}

void STVetraCondition::headerCorrectionAnalogLink(const unsigned int& link,
                                                  const vector< int >& vec)
{
  if ( link <  m_headerCorrectionAnalogLinks.size() )
  {
    if ( vec.size() == m_headerCorrectionAnalogLinks[ link ].size() )
    {
      m_headerCorrectionAnalogLinks[ link ].assign( vec.begin(), vec.end() );
      param< vector< int > >("header_corr_value_" + toString( link, 2 ) ) =
        m_headerCorrectionAnalogLinks[ link ];
    }
  }
}

void STVetraCondition::headerCorrectionAnalogLink(const unsigned int& link,
                                                  const vector< int >* ptrVec)
{
  if ( link <  m_headerCorrectionAnalogLinks.size() )
  {
    if ( ptrVec -> size() == m_headerCorrectionAnalogLinks[ link ].size() )
    {
      m_headerCorrectionAnalogLinks[ link ].assign( ptrVec -> begin(), ptrVec -> end() );
      param< vector< int > >("header_corr_value_" + toString( link, 2 ) ) =
        m_headerCorrectionAnalogLinks[ link ];
    }
  }
}

void STVetraCondition::updateEnable(const unsigned int& value)
{
  int & updateEnValue = param< int >( "pedestal_auto_update_enable" );
  if ( value == 0 )
  {
    updateEnValue = 0;
    m_updateEnable = updateEnValue;
    //param< int >( "pedestal_auto_update_enable" ) = 0;
  }
  else
  {
    updateEnValue = 1;
    m_updateEnable = updateEnValue;
    //param< int >( "pedestal_auto_update_enable" ) = 1;
  }
}

void STVetraCondition::headerEnable(const unsigned int& value)
{
  if ( value == 0 )
  {
    m_headerEnable = 0;
    param< int >( "header_correction_enable" ) = 0;
  }
  else
  {
    m_headerEnable = 1;
    param< int >( "header_correction_enable" ) = 1;
  }
}

void STVetraCondition::pseudoHeaderHighThreshold(const string& str)
{
  param< string >( "Pseudo_header_hi_thr" ) = str;
}

void STVetraCondition::pseudoHeaderLowThreshold(const string& str)
{
  param< string >( "Pseudo_header_lo_thr" ) = str;
}

//=============================================================================
