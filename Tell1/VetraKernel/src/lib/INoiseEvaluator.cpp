// $Id: INoiseEvaluator.cpp,v 1.1.1.1 2008-05-23 12:54:39 szumlat Exp $
// ============================================================================
// Include files 
// ============================================================================
// Local
// ============================================================================
#include "VetraKernel/INoiseEvaluator.h"
// ============================================================================
/** @file 
 *  Implementation file for class Velo::Monitoring::INoiseEvaluator
 *  @date 2007-10-25 
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
// the unique interface identifier 
// ============================================================================
const InterfaceID& Velo::Monitoring::INoiseEvaluator::interfaceID() 
{
  static const InterfaceID s_ID = 
    InterfaceID ( "Velo::Monitoring::INoiseEvalutor" , 1 , 1 ) ;
  return s_ID ;
}
// ============================================================================
// virtual and protected destructor
// ============================================================================
Velo::Monitoring::INoiseEvaluator::~INoiseEvaluator(){} 
// ============================================================================
// The END 
// ============================================================================
