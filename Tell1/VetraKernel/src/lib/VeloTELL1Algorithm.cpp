// $Id: VeloTELL1Algorithm.cpp,v 1.11 2010-02-18 19:30:13 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 

// local
#include "VetraKernel/VeloTELL1Algorithm.h"

// boost
#include <boost/format.hpp>

// standard lib
#include <iostream>
#include <fstream>

//-----------------------------------------------------------------------------
// Implementation file for class : VeloTELL1Algorithm
//
// 2006-12-06 : Tomasz Szumlak
//-----------------------------------------------------------------------------

using namespace boost::assign;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloTELL1Algorithm::VeloTELL1Algorithm( const std::string& name,
                                        ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator ),
    m_isDebug ( false ),
    m_evtNumber ( 0 ),
    m_validationRun ( false ),
    m_dbConfig ( false ),
    m_inputDataLoc ( "" ),
    m_outputDataLoc ( "" ),
    m_algorithmName ( "" ),
    m_condPath ( "VeloCondDB" ),
    m_srcIdList ( ),
    m_dataBuffer(VeloTELL1::ALL_STRIPS),
    m_rawEvent ( 0 ),
    m_rawEventLoc ( LHCb::RawEventLocation::Default ),
    m_detElement ( 0 ),
    m_inputData ( 0 ),
    m_outputData ( 0 ),
    m_nzsSvc ( 0 ),
    m_nzsSvcLoc ( "Raw/Velo/NZSInfo" ),
    m_procInfos ( 0 ),
    m_procInfosLoc ( VeloProcessInfoLocation::Default ),
    m_algorithmType ( ),
    m_isEnable ( false ),
    m_isInitialized ( false )
{
  declareProperty("ForceEnable", m_forceEnable=false);
  declareProperty("BuildSrcIdFromData", m_buildSrcIdFromData=false);
}
//=============================================================================
// Destructor
//=============================================================================
VeloTELL1Algorithm::~VeloTELL1Algorithm() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloTELL1Algorithm::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  m_isDebug=msgLevel(MSG::DEBUG);
  if(m_isDebug) debug() << "==> Initialize" << endmsg;
  m_detElement=getDet<DeVelo>(DeVeloLocation::Default);
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloTELL1Algorithm::execute() {

  if(m_isDebug) debug() << "==> Execute" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VeloTELL1Algorithm::finalize() {

  if(m_isDebug) debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode VeloTELL1Algorithm::getData()
{
  if(m_isDebug) debug()<< " ==> getData() " <<endmsg;
  //
  if(!exist<LHCb::VeloTELL1Datas>(m_inputDataLoc)){
    if(m_isDebug) debug()<< " ==> There is no input data: "
           << m_inputDataLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{  
    // get data banks from default TES location
    m_inputData=get<LHCb::VeloTELL1Datas>(m_inputDataLoc);
    if(m_isDebug) debug()<< " ==> The data have been read-in from location: "
           << m_inputDataLoc
           << ", size of the data container: "
           << m_inputData->size() <<endmsg;
    // vector for output data if output location defined
    if(m_isDebug) debug()<< " out loc: " << (m_outputDataLoc)
           << " input data size: " << (m_inputData->size()) <<endmsg;
    if((!m_outputDataLoc.empty())&&(0!=m_inputData->size())){
      if(m_isDebug) debug()<< " --> Will create new Tell1 data structure " <<endmsg;
      m_outputData=new LHCb::VeloTELL1Datas();
    }else{
      if(!(this->algoName()=="CLUSTER_MAKER")) return ( StatusCode::FAILURE );
    }
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1Algorithm::getNZSSvc()
{
  if(m_isDebug) debug()<< " ==> getNZSSvc() " <<endmsg;
  //
  if(!exist<VeloNZSInfos>(m_nzsSvcLoc)){
    if(m_isDebug) debug()<< " ==> Could not retrieve the NZS Svc at: "
           << m_nzsSvcLoc <<endmsg;
    return ( StatusCode::FAILURE );
  }else{  
    // get data banks from default TES location
    m_nzsSvc=get<VeloNZSInfos>(m_nzsSvcLoc);
    if(m_isDebug) debug()<< " ==> The data have been read-in from location: "
           << m_nzsSvcLoc <<endmsg;    
  }
  //
  return ( StatusCode::SUCCESS );  
}
//=============================================================================
VeloNZSInfo* VeloTELL1Algorithm::nzsSvc() const
{
  if(m_isDebug) debug()<< " ==> nzsSvc() " <<endmsg;
  //
  VeloNZSInfo* infoSvc=*(m_nzsSvc->begin());
  //
  return ( infoSvc );
}
//=============================================================================
StatusCode VeloTELL1Algorithm::INIT()
{
  if(m_isDebug) debug()<< " ==> INIT() " <<endmsg;
  //
  if(!m_isInitialized){
    if(!exist<VeloProcessInfos>(m_procInfosLoc)){
      if(m_isDebug) debug()<< " ==> No init info available! " <<endmsg;
      return ( StatusCode::FAILURE );
    }else{
      m_procInfos=get<VeloProcessInfos>(m_procInfosLoc);
      m_isInitialized=true;
    }
    // build list of source IDs
    if(m_srcIdList.empty()){
      if(!exist<LHCb::VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::ADCs)){
        if(m_isDebug) debug() << " --> Input NZS data stream is EMPTY!" << endmsg;
        return ( StatusCode::FAILURE );
      }
      if(m_buildSrcIdFromData){
        LHCb::VeloTELL1Datas* lDat=
          get<LHCb::VeloTELL1Datas>(LHCb::VeloTELL1DataLocation::ADCs);
        LHCb::VeloTELL1Datas::const_iterator datIT=lDat->begin();
        for( ; datIT!=lDat->end(); ++datIT){
          const unsigned int tell1=static_cast<unsigned int>((*datIT)->key());
          m_srcIdList.push_back(tell1);
        }
      }else{
        // --> R sensors 0 - 41
        for(int tell1=0; tell1<42; ++tell1){
          m_srcIdList.push_back(tell1);
        }
        // --> Phi sensors 
        for(int tell1=64; tell1<106; ++tell1){
          m_srcIdList.push_back(tell1);
        }
        // --> PU sensors 
        for(int tell1=128; tell1<132; ++tell1){
          m_srcIdList.push_back(tell1);
        }
      }
    }
    //
    VeloProcessInfos::const_iterator procIt=m_procInfos->begin();
    bool bit=false;
    if(m_tell1Process!=VeloTELL1::CLUSTER_MAKER){
      bit=(*procIt)->isEnable(m_tell1Process);
    }else if(m_tell1Process==VeloTELL1::CLUSTER_MAKER){
      bit=true;
    }else if(m_tell1Process==VeloTELL1::MEAN_COMMON_MODE_SUBTRACTION){
      bit=true;
    }
    //
    //if((*procIt)->convLimit()<=0){
    //  Error("Convergence Limit not set!");
    //  return ( StatusCode::FAILURE );
    //}else{
    m_convergenceLimit=(*procIt)->convLimit();
    //}
    m_runType=(*procIt)->runType();
    //
    if(bit){
      setIsEnable(true);
      info()<< " ==> " << m_algorithmName << " Configured ";
      if(!m_algorithmType.empty()){
        info()<< " type: " << m_algorithmType <<endmsg;
      }else{
        info()<<endmsg;
      }
      return ( StatusCode::SUCCESS );
    }else{
      setIsEnable(false);
      info()<< " ==> " << m_algorithmName << " Configured (disabled)" <<endmsg;
      return ( StatusCode::SUCCESS );
    }
  }else{
    return ( StatusCode::SUCCESS );
  }
}
//=============================================================================
StatusCode VeloTELL1Algorithm::writeData()
{
  if(m_isDebug) debug()<< " ==> writeData() " <<endmsg;
  //
  if(m_outputData!=0){
    put(m_outputData, m_outputDataLoc);
    return ( StatusCode::SUCCESS );
  }else{
    error()<< " ==> empty output container! " <<endmsg;
    return ( StatusCode::FAILURE );
  }
}
//=============================================================================
StatusCode VeloTELL1Algorithm::cloneData()
{
  if(m_isDebug) debug()<< " ==> cloneData() " <<endmsg;
  // if CMS algorithm is disabled copy the data for next algo
  LHCb::VeloTELL1Datas::const_iterator sensIt=m_inputData->begin();
  for( ; sensIt!=m_inputData->end(); ++sensIt){
    const int sens=(*sensIt)->key();
    LHCb::VeloTELL1Data* out=new LHCb::VeloTELL1Data(sens, VeloTELL1::VeloFull);
    // check and propagate Reordering informations
    if((*sensIt)->isReordered()){
        this->propagateReorderingInfo(*sensIt, out);
    }
    VeloTELL1::sdataVec replica=(*sensIt)->data();
    out->setDecodedData(replica);
    m_outputData->insert(out);
  }
  StatusCode writer=writeData();
  return ( writer );
}
//=============================================================================
LHCb::VeloTELL1Datas* VeloTELL1Algorithm::inputData() const
{
  if(m_isDebug) debug()<< " ==> inputData() " <<endmsg;
  //
  return ( m_inputData );
}
//=============================================================================
void VeloTELL1Algorithm::outputData(LHCb::VeloTELL1Data* inData)
{
  if(m_isDebug) debug()<< " ==> inputData() " <<endmsg;
  //
  m_outputData->insert(inData);
}
//=============================================================================
LHCb::VeloTELL1Datas* VeloTELL1Algorithm::outputData()
{
  return ( m_outputData );
}
//=============================================================================
void VeloTELL1Algorithm::setInputDataLoc(const std::string inValue)
{
  m_inputDataLoc=inValue;
}
//=============================================================================
void VeloTELL1Algorithm::setOutputDataLoc(const std::string inValue)
{
  m_outputDataLoc=inValue;
}
//=============================================================================
void VeloTELL1Algorithm::setTELL1Process(const unsigned int inValue)
{
  m_tell1Process=inValue;
}
//=============================================================================
void VeloTELL1Algorithm::setAlgoName(const std::string inValue)
{
  m_algorithmName=inValue;
}
//=============================================================================
const std::string VeloTELL1Algorithm::algoName() const
{
  return ( m_algorithmName );
}
//=============================================================================
void VeloTELL1Algorithm::setAlgoType(const std::string inValue)
{
  m_algorithmType=inValue;
}
//=============================================================================
const std::string VeloTELL1Algorithm::algoType() const
{
  return ( m_algorithmType );
}
//=============================================================================
bool VeloTELL1Algorithm::isEnable() const
{
  return ( m_isEnable );
}
//=============================================================================
void VeloTELL1Algorithm::setIsEnable(bool flag)
{
   m_isEnable=flag;
}
//=============================================================================
unsigned int VeloTELL1Algorithm::convergenceLimit() const
{
  return ( m_convergenceLimit );
}
//=============================================================================
bool VeloTELL1Algorithm::isInitialized() const
{
  return ( m_isInitialized );
}
//=============================================================================
unsigned int VeloTELL1Algorithm::runType() const
{
  return ( m_runType );
}
//=============================================================================
StatusCode VeloTELL1Algorithm::readDataFromFile(
                             const char* fileName, VeloTELL1::sdataVec& dataVec)
{
  if(m_isDebug) debug()<< " readDataFromFile() " <<endmsg;
  //
  VeloTELL1::sdataVec localStorage;
  std::ifstream inData(fileName);
  signed int aValue=0;
  if(!inData){
    Error(" Can't open the specified file! ");
    info()<< " ===> " << fileName << " <=== " <<endmsg;
    Error(" Mind the input and output file paths ");
    return ( StatusCode::FAILURE );
  }else{
    info()<< " Reading the file " << fileName <<endmsg;
    for(int chan=0; chan<VeloTELL1::ALL_STRIPS; ++chan){
      inData>>aValue;
      localStorage.push_back(aValue);
    }
    inData.close();
  }
  //
  dataVec=localStorage;
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode VeloTELL1Algorithm::writeDataToFile(
                             const char* fileName, VeloTELL1::sdataVec& dataVec)
{
  if(m_isDebug) debug()<< " writeDataToFile() " <<endmsg;
  //
  std::ofstream outData(fileName);
  VeloTELL1::sdatIt datIT=dataVec.begin();
  if(outData.bad()){
    Error(" Cannot open file to write ");
    return ( StatusCode::FAILURE );
  }else{
    for( ; datIT!=dataVec.end(); ++datIT){
      outData<<(*datIT)<<std::endl;
    }
    outData.close();
  }
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloTELL1Algorithm::prepareData(bool insertAtEnd=false)
{
  if(m_isDebug) debug()<< " ==> prepareData() " <<endmsg;
  //
  VeloTELL1::sdataVec::iterator start,stop;
  LHCb::VeloTELL1Datas::const_iterator sensIt;
  sensIt=inputData()->begin();
  for( ; sensIt!=inputData()->end(); ++sensIt){
    const VeloTELL1::sdataVec& data=(*sensIt)->data();
    if(!insertAtEnd){
      for (unsigned int proc=0; proc<VeloTELL1::NumberOfPPFPGA; ++proc) {
        start  = m_dataBuffer.begin()+proc*(VeloTELL1::STRIPS_IN_PPFPGA+VeloTELL1::DUMMY_STRIPS);
        std::copy(data.begin()+proc*VeloTELL1::STRIPS_IN_PPFPGA,data.begin()+(proc+1)*VeloTELL1::STRIPS_IN_PPFPGA,start);

        start  = m_dataBuffer.begin()+proc*(VeloTELL1::STRIPS_IN_PPFPGA+VeloTELL1::DUMMY_STRIPS)+VeloTELL1::STRIPS_IN_PPFPGA;
        stop  = m_dataBuffer.begin()+(proc+1)*(VeloTELL1::STRIPS_IN_PPFPGA+VeloTELL1::DUMMY_STRIPS);
        std::fill(start,stop,0);
      } 
    }else{
      std::copy(data.begin(),data.end(),m_dataBuffer.begin());
      std::fill(m_dataBuffer.begin()+VeloTELL1::NumberOfPPFPGA*VeloTELL1::STRIPS_IN_PPFPGA,m_dataBuffer.end(),512);
    }
    // check if there is propre number of strips
    (*sensIt)->setDecodedData(m_dataBuffer);
  }
}
//=============================================================================
void VeloTELL1Algorithm::prepareData(VeloTELL1::sdataVec& inVec) const
{
  if(m_isDebug) debug()<< " ==> prepareData(sdataVec) " <<endmsg;
  //

  inVec.resize(inVec.size()+VeloTELL1::NumberOfPPFPGA*VeloTELL1::DUMMY_STRIPS);
  VeloTELL1::sdataVec::iterator start, stop, target;
  for (unsigned int proc=0; proc<VeloTELL1::NumberOfPPFPGA; ++proc) {
    target = inVec.begin() + (VeloTELL1::NumberOfPPFPGA-1-proc)*(VeloTELL1::STRIPS_IN_PPFPGA+VeloTELL1::DUMMY_STRIPS);
    start  = inVec.begin() + (VeloTELL1::NumberOfPPFPGA-1-proc)*VeloTELL1::STRIPS_IN_PPFPGA;
    stop   = start + VeloTELL1::STRIPS_IN_PPFPGA;
    std::copy(start,stop,target);
    start  = target + VeloTELL1::STRIPS_IN_PPFPGA;
    stop   = start + VeloTELL1::DUMMY_STRIPS;
    std::fill(start,stop,0);
  }  
}
//=============================================================================
DeVelo* VeloTELL1Algorithm::deVelo() const
{
  if(m_isDebug) debug()<< " ==> deVelo() "<<endmsg;
  return ( m_detElement );
}
//=============================================================================
StatusCode VeloTELL1Algorithm::inputStream(LHCb::VeloTELL1Datas* inVec) const
{
  if(m_isDebug) debug()<< " ==> dataStatus() " <<endmsg;
  if(inVec->empty()){
    if(m_isDebug) debug()<< "Input stream is empty!" <<endmsg;
    return ( StatusCode::FAILURE );
  }else{
    return ( StatusCode::SUCCESS);
  }
}
//=============================================================================
void VeloTELL1Algorithm::propagateReorderingInfo(
     const LHCb::VeloTELL1Data* inObj, LHCb::VeloTELL1Data* outObj)
{
  if(m_isDebug) debug()<< " ==> propagateReorderingInfo() " <<endmsg;
  //
  outObj->setIsReordered(true);
  outObj->setSensorType(inObj->sensorType());
}
//=============================================================================
StatusCode VeloTELL1Algorithm::i_cacheConditions()
{
  if(m_isDebug) debug()<< " ==> cacheConditions() " <<endmsg;
  // do something interesting inside derived classes only
  return ( StatusCode::SUCCESS );
}
//=============================================================================
VeloTELL1::AListPair VeloTELL1Algorithm::getSrcIdList() const
{
  if(m_isDebug) debug()<< " ==> getSourceIds " <<endmsg;
  VeloTELL1::AListPair aPair;
  aPair.first=m_srcIdList.begin();
  aPair.second=m_srcIdList.end();
  //
  return ( aPair );
}
//=============================================================================
void VeloTELL1Algorithm::setSrcIdList(std::vector<unsigned int> inVec)
{
  m_srcIdList=inVec;
}
//=============================================================================
StatusCode VeloTELL1Algorithm::i_cacheSrcIdList()
{
  if(m_isDebug) debug()<< " --> getSrcIdListFromDB() " <<endmsg;
  std::vector<unsigned int> srcIdList;
  std::string tell1Path=m_condPath+"/Tell1Map";
  Condition* tell1Cond=getDet<Condition>(tell1Path);
  std::vector<int> tempList=tell1Cond->param<std::vector<int> >("tell1_map");
  if(tempList.empty())
    return ( Error(" --> Cannot retrieve Tell1 Map!", StatusCode::FAILURE) );
  std::vector<int>::const_iterator iT=tempList.begin();
  for( ; iT!=tempList.end(); ++iT){
    srcIdList.push_back(static_cast<unsigned int>(*iT));
  }
  this->setSrcIdList(srcIdList);
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void VeloTELL1Algorithm::regUpdateTell1Conds()
{
  if(m_isDebug) debug()<< " --> regUpdateTell1Conds() " <<endmsg;

  registerCondition(m_condPath + "/Tell1Map", 
              &VeloTELL1Algorithm::callCacheSrcIdList);

  // Update conditions when src id list changes
  registerCondition(m_condPath + "/Tell1Map", 
              &VeloTELL1Algorithm::callCacheConditions);

  // R sensors
  for (int sens=0; sens<42; ++sens)
  {
    boost::format cond ("%s/VeloTELL1Board%d");
    cond % m_condPath;
    cond % sens;
    registerCondition(cond.str(), 
              &VeloTELL1Algorithm::callCacheConditions);
  }

  // Phi sensors
  for (int sens=64; sens<106; ++sens)
  {
    boost::format cond ("%s/VeloTELL1Board%d");
    cond % m_condPath;
    cond % sens;
    registerCondition(cond.str(), 
              &VeloTELL1Algorithm::callCacheConditions);
  }
}
//=============================================================================
StatusCode VeloTELL1Algorithm::callCacheConditions()
{
  return i_cacheConditions();
}
//=============================================================================
StatusCode VeloTELL1Algorithm::callCacheSrcIdList()
{
  return i_cacheSrcIdList();
}

//--
