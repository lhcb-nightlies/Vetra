#ifndef REMOVETELL1RAWBANK_H 
#define REMOVETELL1RAWBANK_H 1

// Include files from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

class CopyTELL1RawBank : public GaudiAlgorithm {
public: 
  /// Standard constructor
  CopyTELL1RawBank( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CopyTELL1RawBank( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization
  
protected:

private:
  int  m_bankID;
  std::string m_originalRawEventLocation;
  std::string m_newRawEventLocation;
};

#endif // REMOVETELL1RAWBANK_H
