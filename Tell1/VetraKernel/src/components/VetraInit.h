#ifndef VETRAINIT_H 
#define VETRAINIT_H 1

// Include files
// -------------
// from LHCbKernel
#include "Kernel/LbAppInit.h"

// Forward declarations
class IGenericTool;
class IIncidentSvc;

/** @class VetraInit VetraInit.h
 *
 *  Algorithm to initialize Vetra
 *
 *  @author Eduardo Rodrigues (simple copy of original from Patrick Koppenburg)
 *  @date   2010-04-06
 */

class VetraInit : public LbAppInit {
public: 
  /// Standard constructor
  VetraInit( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VetraInit( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution

protected:

private:
  IGenericTool* m_memoryTool;   ///< Pointer to (private) memory histogram tool
  IIncidentSvc* m_incidentSvc;  ///< Pointer to the incident service.

  bool m_print ; ///< Print event and run
  unsigned int m_increment ; ///< Number of events to measure memory on
  int m_lastMem ; ///< Last memory
  // flag for DEBUG level
  bool m_debugLevel;
};

#endif // VETRAINIT_H
