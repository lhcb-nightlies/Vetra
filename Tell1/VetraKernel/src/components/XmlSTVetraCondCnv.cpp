
// Condition
#include "VetraKernel/STVetraCondition.h"

// DetDesc
#include "DetDescCnv/XmlUserConditionCnv.h"

typedef XmlUserConditionCnv< STVetraCondition > XmlSTVetraCondition;
DECLARE_CONVERTER_FACTORY(XmlSTVetraCondition)
