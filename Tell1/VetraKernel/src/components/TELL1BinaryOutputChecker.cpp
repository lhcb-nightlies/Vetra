// $Id: TELL1BinaryOutputChecker.cpp,v 1.2 2008-07-10 10:15:50 szumlat Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h" 

// local
#include "TELL1BinaryOutputChecker.h"

// event model
#include "Event/RawBank.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TELL1BinaryOutputChecker
//
// 2006-03-22 : Tomasz Szumlak
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( TELL1BinaryOutputChecker )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TELL1BinaryOutputChecker::TELL1BinaryOutputChecker( const std::string& name,
                                                    ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator ),
    m_eventsReceived ( 0 ),
    m_eventsDropped ( 0 )
{ 
  declareProperty("PrintBankInfo", m_printBankInfo=false);
  declareProperty("ExpectedNumberOfTELL1s",m_expectedNumberOfTELL1s=88);
  declareProperty("DropIncompleteEvents",m_dropIncompleteEvents=false);
  declareProperty("RawEventLocation",
                  m_rawDataLocation=LHCb::RawEventLocation::Default);
  declareProperty("RunWithODIN", m_runWithODIN=true);
}
//=============================================================================
// Destructor
//=============================================================================
TELL1BinaryOutputChecker::~TELL1BinaryOutputChecker() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode TELL1BinaryOutputChecker::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode TELL1BinaryOutputChecker::execute() {

  debug() << "==> Execute" << endmsg;
  //
  debug()<< "Getting Raw Bank!" <<endmsg;  
  StatusCode sc=getRawBank();
  //
  return sc;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode TELL1BinaryOutputChecker::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode TELL1BinaryOutputChecker::getRawBank()
{
    debug()<< "===> executing getRawBank routine" <<endmsg;
    debug()<< "---------------------------------" <<endmsg;
  //
  if(!exist<LHCb::RawEvent>(m_rawDataLocation)){
    info()<< " ==> There is no RawEvent at: "
          << m_rawDataLocation <<endmsg;
  }
  // get the RawEvent from default TES location
  m_rawEvent=get<LHCb::RawEvent>(m_rawDataLocation);
  debug()<< " ==> The RawEvent has been read-in from location: "
        << m_rawDataLocation <<endmsg;
  //
  ++m_eventsReceived;
  bool incompleteEventBanks=false;  // protection against incomplete events
  bool incompleteEventODIN=false;
  for(int i=0; i<LHCb::RawBank::LastType; ++i){
    LHCb::RawBank::BankType type=LHCb::RawBank::BankType(i);
    const std::vector<LHCb::RawBank*>& banks=m_rawEvent->banks(type);
    // check if the VELO+ODIN banks are complete
    if(8==i&&banks.size()!=m_expectedNumberOfTELL1s){
      incompleteEventBanks=true;  // the VELO bank
      info()<< " Event corrupted or YOU RUN WITH WRONGLY SET " <<endmsg;
      info()<< " ExpectedNumberOfTELL1s variable - CHECK OPTIONS! " <<endmsg;
      info()<< " There are " << ( banks.size() ) << " banks present! " <<endmsg;
    }else if(16==i && banks.size()!=1){
      if(m_runWithODIN) incompleteEventODIN=true; // the ODIN bank
    }
    if(banks.size()!=0){
      debug()<< " ==> bank size: " << banks.size() <<endmsg;
      std::vector<LHCb::RawBank*>::const_iterator bankIt;
      for(bankIt=banks.begin(); bankIt!=banks.end(); ++bankIt){
        const LHCb::RawBank* aBank=(*bankIt);
        if(m_printBankInfo){ 
          info()<< " size: " << int(aBank->size()) << ", "
                << " sourceID: " << int(aBank->sourceID()) << ", "
                << " version: " << int(aBank->version()) << ", "
                << " bank type: " << type << ", "
                << " magic pattern: "
                << std::hex << aBank->magic() << std::dec 
                << " pointer to the data body: "
                << aBank->data() <<endmsg;
        }
        const unsigned int* ptrToData=aBank->data();
        const unsigned int* ptrDat=ptrToData;
        debug()<< "dec data: " << (*(ptrDat+1)) <<endmsg;
        debug()<< "dec data: " << (*(ptrDat+14)) <<endmsg;
        debug()<< "dec data: " << (*(ptrDat+27)) <<endmsg;
        debug()<< "dec data: " << (*(ptrDat+40)) <<endmsg;
      }
    }
  }
  // protection against incomplete events
  if(incompleteEventODIN&&m_dropIncompleteEvents){
    ++m_eventsDropped;
    setFilterPassed( false );
    info()<< " ODIN banks is missing! " <<endmsg;
    info()<< "Incomplete event rate = " << m_eventsDropped 
          << "/" << m_eventsReceived
          <<" ("<< 100.*((float)m_eventsDropped/m_eventsReceived)
          << "%)" << endmsg;
    return ( StatusCode::FAILURE );
  }
  if(incompleteEventBanks&&m_dropIncompleteEvents){
    info()<< " Bank alignment or number is wrong " <<endmsg;
  }
  //
  return ( StatusCode::SUCCESS );
}
