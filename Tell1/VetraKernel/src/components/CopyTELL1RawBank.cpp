// from Gaudi
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"


#include "Event/RawEvent.h"
#include "Event/RawBank.h"

#include "CopyTELL1RawBank.h"

using namespace LHCb;

///-----------------------------------------------------------------------------
// Implementation file for class : RemoveTELL1RawBank
//
// 2006-12-04 Aras Papadelis
//
// Removes a user-defined bank from the RawBank. Good for decreasing file size
// of data files with ZS and NZS banks. 
// Bank types of interest for the VELO are:
// Velo(ZS)        8
// DAQ            15
// ODIN           16
// VeloFull(NZS)  18
// VeloError      29
// VeloPedestal   30
// VeloProcFull   31
//
//Use the algorithm combination with MDF writer to produce smaller data files.
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY(CopyTELL1RawBank)

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CopyTELL1RawBank::CopyTELL1RawBank( const std::string& name,ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
{
  // Copy NZS bank by default:
  declareProperty( "BankID"		, m_bankID = -1);
  declareProperty( "OriginalLocation", m_originalRawEventLocation = RawEventLocation::Default);
  declareProperty( "NewLocation"		 , m_newRawEventLocation = "/Event/DAQ/DAQEvent");  
}

//=============================================================================
// Destructor
//=============================================================================
CopyTELL1RawBank::~CopyTELL1RawBank() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode CopyTELL1RawBank::initialize() {
  debug() << "==> Initialize" << endmsg;
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  return StatusCode::SUCCESS;
}


StatusCode CopyTELL1RawBank::finalize() {  
  debug() << "==> Finalize" << endmsg;
  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

StatusCode CopyTELL1RawBank::execute() {
  SmartDataPtr<RawEvent> evt(eventSvc(),m_originalRawEventLocation);
  if ( !evt ){
    warning() << "Connot find a RawEvent at " << m_originalRawEventLocation << endmsg;
    return StatusCode::SUCCESS;
  }
  // create a new raw event location - that is the output of the algorithm
  RawEvent* newEvt;
  if(exist<RawEvent>(m_newRawEventLocation)){
    newEvt=get<RawEvent>(m_newRawEventLocation);
  }else{
    newEvt=new RawEvent();
    eventSvc()->registerObject(m_newRawEventLocation, newEvt);
    debug()<< " registered a new RawEvent at " << m_newRawEventLocation <<endmsg;
  }
   
  typedef std::vector<RawBank*> BankV;
  int nbanks=0;
  BankV banks = evt->banks(RawBank::BankType(m_bankID)); // need to copyvector!
  for(BankV::iterator j=banks.begin(); j != banks.end(); ++j)  {
    newEvt->adoptBank(*j,false);
    nbanks++;
  }
  
  if(nbanks!=0)
    debug() << "Copied " << nbanks << " banks of type " << m_bankID << " from " 
            << m_originalRawEventLocation << " to " << m_newRawEventLocation << endmsg;

  return StatusCode::SUCCESS;
}
