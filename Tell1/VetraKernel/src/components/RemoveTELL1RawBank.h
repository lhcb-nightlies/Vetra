
#ifndef REMOVETELL1RAWBANK_H 
#define REMOVETELL1RAWBANK_H 1

// Include files from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/************************************************/
/* RemoveTELL1RawBank.h                         */
/* 2006-12-04 Aras Papadelis                    */
/************************************************/


class RemoveTELL1RawBank : public GaudiAlgorithm {
public: 
  /// Standard constructor
  RemoveTELL1RawBank( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~RemoveTELL1RawBank( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

  
protected:


private:

  int  m_bankID;

  bool m_debug();

};

#endif // REMOVETELL1RAWBANK_H
