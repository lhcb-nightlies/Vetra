// $Id: TELL1BinaryOutputChecker.h,v 1.1.1.1 2008-05-23 12:54:39 szumlat Exp $
#ifndef TELL1BINARYOUTPUTCHECKER_H 
#define TELL1BINARYOUTPUTCHECKER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/RawEvent.h"

/** @class TELL1BinaryOutputChecker TELL1BinaryOutputChecker.h
 *  
 *
 *  @author Tomasz Szumlak
 *  @date   2006-03-22
 */

class TELL1BinaryOutputChecker : public GaudiAlgorithm {
public: 
  /// Standard constructor
  TELL1BinaryOutputChecker( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~TELL1BinaryOutputChecker( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization
  StatusCode getRawBank();

protected:
  
private:

  LHCb::RawEvent* m_rawEvent;
  bool m_printBankInfo;

  // protection against incomplete events
  unsigned int m_expectedNumberOfTELL1s;
  bool m_dropIncompleteEvents;
  long unsigned int m_eventsReceived;
  long unsigned int m_eventsDropped;
  bool m_runWithODIN;
  std::string m_rawDataLocation;
};
#endif // TELL1BINARYOUTPUTCHECKER_H
