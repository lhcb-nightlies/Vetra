// $Id: STVetraCfg.cpp,v 1.5 2010-03-10 10:24:09 jluisier Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/ToolFactory.h" 

// local
#include "VetraKernel/STVetraCfg.h"

#include "Kernel/STLexicalCaster.h"

#include "VetraKernel/STVetraCondition.h"

using namespace std;
using namespace LHCb;
using namespace ST;
using namespace STBoardMapping;

//-----------------------------------------------------------------------------
// Implementation file for class : STVetraCfg
//
// 2009-04-24 : Johan Luisier
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( STVetraCfg )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STVetraCfg::STVetraCfg( const string& type,
                        const string& name,
                        const IInterface* parent )
  : ToolBase ( type, name , parent ),
    m_NbrOfBoards ( 0 )
{
  declareInterface<ISTVetraCfg>(this);

  declareProperty("CondPath", m_condPath = "CondDB" );

}
//=============================================================================
// Destructor
//=============================================================================
STVetraCfg::~STVetraCfg()
{}

StatusCode STVetraCfg::initialize()
{
  StatusCode sc( ToolBase::initialize() );

  if ( sc.isFailure() )
    return Error("Failed to initialize", sc);

  if ( detType() == "IT" )
  {
    m_NbrOfBoards = 42;
    detMap = ITNumberToSourceIDMap();
  }
  else if ( detType() == "TT" )
  {
    m_NbrOfBoards = 48;
    detMap = TTNumberToSourceIDMap();
  }
  else
    return Error("Unknown detector type : " + detType() + "must be IT or TT");

  string path;

  unsigned int tell1ID;

  for (unsigned int i(1); i <= m_NbrOfBoards; i++)
  {
    tell1ID = detMap.find( i ) -> second;

    path = m_condPath + "/TELL1Board" + toString( tell1ID );

    registerCondition( path, &STVetraCfg::initCondition );
  }
  
  sc = runUpdate();  
  if ( sc.isFailure() )
    return Error ( "Failed first UMS update", sc );

  return sc;
}

STVetraCondition* STVetraCfg::getSTVetraCond(const unsigned int& ID) const
{
  map< unsigned int, STVetraCondition*>::const_iterator it;

  it = m_Conditions.find( ID );
  
  if ( it == m_Conditions.end() )
    return 0;
  else
    return it -> second;
}

void STVetraCfg::setType( const string& detType )
{
  setDetType( detType );
}

const string& STVetraCfg::getDetType() const
{
  return detType();
}

StatusCode STVetraCfg::initCondition()
{
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "==> initConditions()" << endmsg;

  string path;
  unsigned int tell1ID;

  for (unsigned int i(1); i <= m_NbrOfBoards; i++)
  {
    tell1ID = detMap.find( i ) -> second;
    if ( msgLevel(MSG::DEBUG) )
      debug() << "Reading TELL1 " << tell1ID << endmsg;

    path = m_condPath + "/TELL1Board" + toString( tell1ID );
    
    try
    {
      m_Conditions[ tell1ID ] = getDet<STVetraCondition>( path );
    }
    catch (GaudiException &e)
    {
      fatal() << e << endmsg;
      return StatusCode::FAILURE;
    }  
  }

  return StatusCode::SUCCESS;
}

ostream& STVetraCfg::printOut(ostream& os) const
{
  Map reverse;
  
  if ( detType() == "IT" )
    reverse = ITSourceIDToNumberMap();
  else
    reverse = TTSourceIDToNumberMap();
  
  Map::const_iterator mapIt, mapEnd( reverse.end() );
  
  for (mapIt = reverse.begin(); mapIt != mapEnd; mapIt++)
    os << getSTVetraCond( mapIt -> first ) -> toXml ( "TELL1Board" + toString( mapIt -> first ), false ) << endl;
  
  return os;
}


//=============================================================================
