
// from Gaudi
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"


#include "Event/RawEvent.h"
#include "Event/RawBank.h"

#include "RemoveTELL1RawBank.h"

using namespace LHCb;

///-----------------------------------------------------------------------------
// Implementation file for class : RemoveTELL1RawBank
//
// 2006-12-04 Aras Papadelis
//
// Removes a user-defined bank from the RawBank. Good for decreasing file size
// of data files with ZS and NZS banks. 
// Bank types of interest for the VELO are:
// Velo(ZS)        8
// DAQ            15
// ODIN           16
// VeloFull(NZS)  18
// VeloError      29
// VeloPedestal   30
// VeloProcFull   31
//
//Use the algorithm combination with MDF writer to produce smaller data files.
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY(RemoveTELL1RawBank)


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RemoveTELL1RawBank::RemoveTELL1RawBank( const std::string& name,ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )


{

  // Remove NZS bank by default:
  declareProperty( "BankID"		, m_bankID = 18);

  
}

//=============================================================================
// Destructor
//=============================================================================
RemoveTELL1RawBank::~RemoveTELL1RawBank() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode RemoveTELL1RawBank::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;
  
  return StatusCode::SUCCESS;
}


StatusCode RemoveTELL1RawBank::finalize() {

  
  debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

StatusCode RemoveTELL1RawBank::execute() {
  SmartDataPtr<RawEvent> evt(eventSvc(),RawEventLocation::Default);
  if ( evt )  {
    typedef std::vector<RawBank*> BankV;
    int nbanks=0;

    BankV banks = evt->banks(RawBank::BankType(m_bankID)); // need to copyvector!
    for(BankV::iterator j=banks.begin(); j != banks.end(); ++j)  {
      evt->removeBank(*j);
      nbanks++;
    }
  
    //    if(nbanks!=0)
    //      debug() << "Removed " << nbanks << " banks of type " << m_bankID << " from the RawBank " << endmsg;

    return StatusCode::SUCCESS;
  }
  return StatusCode::FAILURE;
}
