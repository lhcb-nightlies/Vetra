// $Id: VetraKernelDict.h,v 1.1 2009-05-12 11:48:06 jluisier Exp $
#ifndef DICT_VETRAKERNELDICT_H 
#define DICT_VETRAKERNELDICT_H 1

#include "VetraKernel/ISTVetraCfg.h"
#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

struct _Instantiations{

  STVetraCondition m_stVetraCondition;
  std::map< unsigned int, STVetraCondition* > m_uInt_vtcond_map;
};

#endif // DICT_VETRAKERNELDICT_H
