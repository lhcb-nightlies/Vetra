"""
##############################################################################
#                                                                            #
#  Vetra option file for the "TELL1Processing" phase of a Vetra job.         #
#  Defines the decoding of the Error Banks (EB).                             #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @author  Tomasz Szumlak     ( t.szumlak@physics.gla.ac.uk )               #
#  @date    17/11/2008                                                       #
#                                                                            #
##############################################################################
"""

from Configurables import GaudiSequencer
from Configurables import SmartVeloErrorBankDecoder

decoding_EB = GaudiSequencer( 'Decoding_EB' )

decoding_EB.Members = [ SmartVeloErrorBankDecoder() ]

GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ decoding_EB ]

##############################################################################
