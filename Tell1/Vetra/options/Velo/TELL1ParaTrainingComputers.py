"""
##############################################################################
#                                                                            #
#  These algorithms must be added to the main training sequence              #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Tomasz Szumlak     ( t.szumlak@physics.gla.ac.uk   )             #
#  @date    21/04/2009                                                       #
#                                                                            #
##############################################################################
"""

# =============================================================================
# Import necessary modules
# =============================================================================

from Gaudi.Configuration import *
from Configurables import Vetra
from Configurables import GaudiSequencer
from Configurables import VeloBeetleHeaderXTalkComputer    as BHXTComp
from Configurables import VeloClusteringThresholdsComputer as CTComp
from Configurables import VeloPedestalSubtractorMoni       as PedSubMoni

BHXTComp().ConvergenceLimit = Vetra().getProp( 'EmuConvLimit' )
CTComp().ConvergenceLimit = Vetra().getProp( 'EmuConvLimit' )
PedSubMoni().StoreSumBank = True

training = GaudiSequencer( 'TELL1Emulation' )

training.Members += [   BHXTComp()
                       ,CTComp()
                       ,PedSubMoni()
                      ]

GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ training ]
