"""
##############################################################################
#                                                                            #
#  Vetra option file for the "TELL1Processing" phase of a Vetra job.         #
#  Defines the decoding of the Non-Zero-Suppressed (NZS) data.               #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @author  Tomasz Szumlak     ( t.szumlak@physics.gla.ac.uk )               #
#  @date    17/11/2008                                                       #
#                                                                            #
##############################################################################
"""

from Configurables import GaudiSequencer

from Configurables import PrepareVeloFullRawBuffer, DecodeVeloFullRawBuffer
from Configurables import Vetra

from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters


prepareVeloFullRawBuffer = PrepareVeloFullRawBuffer()

decodeVeloFullRawBuffer  = DecodeVeloFullRawBuffer()

decoding_NZS = GaudiSequencer( 'Decoding_NZS' )

decoding_NZS.Members += [   prepareVeloFullRawBuffer
                         , decodeVeloFullRawBuffer
                           ]
prepareVeloFullRawBuffer.RoundRobin = True

# expert stuff
if Vetra().getProp( 'ExpertMoni' ):
   decodeEmuClusters = DefaultDecoderToVeloClusters( 'DecodeToVeloClustersEmu' )
   decodeEmuClusters.DecodeToVeloClusters     = True
   decodeEmuClusters.DecodeToVeloLiteClusters = False
   decodeEmuClusters.VeloClusterLocation = 'Emu/Velo/Clusters' 
   decodeEmuClusters.RawEventLocation    = 'Emu/RawEvent'
   decoding_NZS.Members += [ decodeEmuClusters ]

# no "prepare" algorithm for simulation !
if Vetra().getProp( 'Simulation' ) :
    decoding_NZS.Members.remove( prepareVeloFullRawBuffer )

GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ decoding_NZS ]


###############################################################################
