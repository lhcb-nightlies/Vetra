///////////////////////////////////////////////////////////////////////////////
//  Job options for Vetra running on Non Zero Suppressed REAL DATA           //
///////////////////////////////////////////////////////////////////////////////

// Option file for main application for VeloTELL1 emulation package Vetra
// Output level (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL)
MessageSvc.OutputLevel = 3;
//========================================================================
// General settings
#include "$STDOPTS/LHCbApplication.opts"
#include "$STDOPTS/DigiDicts.opts"
#include "$STDOPTS/RawDataIO.opts"

// Monitoring - histogram persistency Svc
#include "$STDOPTS/RootHist.opts"

RootHistCnv.ForceAlphaIds = true;

#pragma print off
/// time profiling
AuditorSvc.Auditors = { "ChronoAuditor" };
ApplicationMgr.ExtSvc += { "AuditorSvc" };
ChronoAuditor.Enable = true;
///
//========================================================================
//DetectorDataSvc.DetDbLocation = "$XMLDDDBROOT/DDDB/lhcb-200601.xml";
// or ACDC specific
#include "$DDDBROOT/options/DC06.opts"

//#include "$DDDBROOT/options/DDDB.opts"
//MagneticFieldSvc.FieldMapFile = "$FIELDMAPROOT/cdf/field047.cdf";
//DetectorDataSvc.DetDbLocation = "$XMLDDDBROOT/DDDB/Velo/VeloACDC/ACDC3.xml";
EventPersistencySvc.CnvServices += {"LHCb::RawDataCnvSvc"};




// Vetra sequences - analysis of Non-zero suppressed real data
ApplicationMgr.TopAlg += {
			  "ProcessPhase/TELL1Processing"

                         };

TELL1Processing.DetectorList+={ "VELO" };
TELL1ProcessingVELOSeq.Members+={ "PrepareVeloFullRawBuffer" };
TELL1ProcessingVELOSeq.Members+={ "DecodeVeloFullRawBuffer" };


/// --> TELL1 emulation
#include "$VETRAROOT/options/Velo/TELL1Emulator.opts"

ApplicationMgr.AuditAlgorithms = True;
ApplicationMgr.ExtSvc += { "ToolSvc", "AuditorSvc"};
AuditorSvc.Auditors += {"TimingAuditor/TIMER" } ;

VeloTELL1EmulatorInit.ConvergenceLimit=4000;

//Override CondDB with our TELL1s:
#include "CommissioningConditionsTrivial.opts"


/* ++++++++++++++ Step 1. How many events? ++++++++++++++++++ */

// -1 all events
ApplicationMgr.EvtMax = -1;
// to skip some events
EventSelector.FirstEvent = 1;
// printing freq
EventSelector.PrintFreq = 100;

/* ++++++++++++++ Step 2. Define input file(s) +++++++++++++++++ */

//Sample file on CASTOR:
EventSelector.Input += {
//"DATAFILE='rfio:///castor/cern.ch/user/a/arasp/slicetest/uniformityTest_ntp_tell207_odinControl_071112_1530.dat' SVC='LHCb::MDFSelector'"

//Sample file on the online network:
//"DATAFILE='file://$INPUT_FILE' SVC='LHCb::MDFSelector'"
"DATAFILE='rfio://$INPUT_FILE' SVC='LHCb::MDFSelector'"

};

/* ++++++++++++++ Step 3. Define output root file ++++++++++++++ */

HistogramPersistencySvc.OutputFile = "$ROOT_FILE";

/* ++++++++++++++ Step 4. Options for low level plots +++++++++ */

// The 3 standard low level algorithms are
// * PedestalMon for pedestal monitoring
// * ADCMon for plots of raw ADC spectra
// * Noise for noise monitoring

//Include standard options for noise and pedestal monitoring:
#include "$VELODATAMONITORROOT/options/VeloDataMonitor.opts"

//Uncomment this line to remove the ADC monitor, which is slow:
//TELL1ProcessingVELOSeq.Members-={"Velo::Monitoring::ADCMon/ADCMon"};

//Change here if you want to specify which TELL1s to check. Default is all:
ADCMon.TELL1s = {};
FeedNoise.TELL1s = {};
Noise.TELL1s = {};
PedestalMon.TELL1s = {};

//Specify which containers will be monitored. Default is all
//For more examples, see VeloDataMonitor.opts in VeloDataMonitor package
//Noise.Containers={"Raw/Velo/ADCReordered"};
//FeedNoise.Containers={"Raw/Velo/ADCReordered"};


//Set how often the noise histograms are published. Every nth event:
Noise.PublishingFrequency=1000;

//Uncomment to turn on noise following:
//FeedNoise.NoiseEvaluator.Reset = 10000;

//Uncomment to set a S/N limit in the noise calculation:
//FeedNoise.NoiseEvaluator.Sigma = 5;


/* ++++++++++ Debugging steps +++++++++++++++++++*/

//TELL1 binary output checker

//Uncomment this if you want to run the corrupt event checker:
//TELL1ProcessingVELOSeq.Members+={ "TELL1BinaryOutputChecker" };
//TELL1BinaryOutputChecker.DropIncompleteEvents=true;

//Make sure that this number corresponds to the #TELL1s in the data:
//TELL1BinaryOutputChecker.ExpectedNumberOfTELL1s=1;

//Uncomment these lines if you want to print out the bank info:
//TELL1BinaryOutputChecker.PrintBankInfo=true;
//TELL1BinaryOutputChecker.OutputLevel=3;


//ODIN bank print-outs:
//TELL1ProcessingVELOSeq.Members+={ "PrintODINBank" };
//PrintODINBank.PrintExtendedInfo=true;

//How often should the algorithm print out:
//PrintODINBank.PrintFrequency=1;

/*  ++++++++++++ Backwards compatibility... +++++++++++ */
/*
//To turn on old monitoring: 
TELL1ProcessingVELOSeq.Members+={"FullDataMonitorPedestalsAndNoise/PedAndNoise"};
PedAndNoise.ChosenTELL1Number = 207;
PedAndNoise.PedestalsAndNoiseFirstEvent=4000;
PedAndNoise.PlotHistos=true;
PedAndNoise.PlotADCData=true;
*/
