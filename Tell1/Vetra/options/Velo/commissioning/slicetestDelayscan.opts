///////////////////////////////////////////////////////////////////////////////
//  Job options for Vetra running on Non Zero Suppressed Real Phas Scan Data //
///////////////////////////////////////////////////////////////////////////////

// Option file for main application for VeloTELL1 emulation package Vetra
// Output level (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL)
MessageSvc.OutputLevel = 3;
//========================================================================
// General settings
#include "$STDOPTS/LHCbApplication.opts"
#include "$STDOPTS/DigiDicts.opts"
#include "$STDOPTS/RawDataIO.opts"

// Monitoring - histogram persistency Svc
#include "$STDOPTS/RootHist.opts"

RootHistCnv.ForceAlphaIds = true;

#pragma print off
/// time profiling
AuditorSvc.Auditors = { "ChronoAuditor" };
ApplicationMgr.ExtSvc += { "AuditorSvc" };
ChronoAuditor.Enable = true;
///
//========================================================================
//DetectorDataSvc.DetDbLocation = "$XMLDDDBROOT/DDDB/lhcb-200601.xml";
// or ACDC specific
#include "$DDDBROOT/options/DC06.opts"

//#include "$DDDBROOT/options/DDDB.opts"
//MagneticFieldSvc.FieldMapFile = "$FIELDMAPROOT/cdf/field047.cdf";
//DetectorDataSvc.DetDbLocation = "$XMLDDDBROOT/DDDB/Velo/VeloACDC/ACDC3.xml";
EventPersistencySvc.CnvServices += {"LHCb::RawDataCnvSvc"};


// Vetra sequences - analysis of Non-zero suppressed real data
ApplicationMgr.TopAlg += {
			  "ProcessPhase/TELL1Processing"

                         };

TELL1Processing.DetectorList+={ "VELO" };
TELL1ProcessingVELOSeq.Members+={ "PrepareVeloFullRawBuffer" };
TELL1ProcessingVELOSeq.Members+={ "DecodeVeloFullRawBuffer" };

ApplicationMgr.AuditAlgorithms = True;
ApplicationMgr.ExtSvc += { "ToolSvc", "AuditorSvc"};
AuditorSvc.Auditors += {"TimingAuditor/TIMER" } ;

//We don't run the emulator to save time, but this is needed for the dummy links:
TELL1ProcessingVELOSeq.Members += {
 "dataTranslator"
};

//Override CondDB with our TELL1s:
#include "CommissioningConditions.opts"


/* ++++++++++++++ Step 1. How many events? ++++++++++++++++++ */

// -1 all events
ApplicationMgr.EvtMax = -1;
// to skip some events
EventSelector.FirstEvent = 1;
// printing freq
EventSelector.PrintFreq = 100;

/* ++++++++++++++ Step 2. Define input file(s) +++++++++++++++++ */

//Sample file on CASTOR:
EventSelector.Input += {
//"DATAFILE='rfio:///castor/cern.ch/user/a/arasp/slicetest/uniformityTest_ntp_tell207_odinControl_071112_1530.dat' SVC='LHCb::MDFSelector'"

//Sample file on the online network:
"DATA='file://$INPUT_FILE'  SVC='LHCb::MDFSelector'"

};

/* ++++++++++++++ Step 3. Define output root file ++++++++++++++ */

HistogramPersistencySvc.OutputFile = "$ROOT_FILE";

/* ++++++++++++++ Step 4. Options for phase scan algorithm +++++++++ */

TELL1ProcessingVELOSeq.Members += {"Velo::Monitoring::ADC_DigitisationDelayScan/PhaseScan"};	

PhaseScan.BurstSize = 1000;
PhaseScan.NBursts  = 16;
PhaseScan.BurstId = 1;
PhaseScan.PhaseChangeMethod=3;
PhaseScan.OutputLevel = 3;


/* ++++++++++ Debugging steps +++++++++++++++++++*/

//TELL1 binary output checker
/*
//Uncomment this if you want to run the corrupt event checker:
TELL1ProcessingVELOSeq.Members+={ "TELL1BinaryOutputChecker" };
TELL1BinaryOutputChecker.DropIncompleteEvents=true;

//Make sure that this number corresponds to the #TELL1s in the data:
TELL1BinaryOutputChecker.ExpectedNumberOfTELL1s=1;

//Uncomment these lines if you want to print out the bank info:
TELL1BinaryOutputChecker.PrintBankInfo=true;
TELL1BinaryOutputChecker.OutputLevel=3;
*/

//ODIN bank print-outs:
//TELL1ProcessingVELOSeq.Members+={ "PrintODINBank" };
//PrintODINBank.PrintExtendedInfo=true;

//How often should the algorithm print out:
//PrintODINBank.PrintFrequency=1;

