///////////////////////////////////////////////////////////////////////////////
//  Job options for Vetra running on Non Zero Suppressed REAL DATA           //
///////////////////////////////////////////////////////////////////////////////

// Option file for main application for VeloTELL1 emulation package Vetra
// Output level (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL)
MessageSvc.OutputLevel = 3;
//========================================================================
// General settings
#include "$STDOPTS/LHCbApplication.opts"
#include "$STDOPTS/DigiDicts.opts"
#include "$STDOPTS/RawDataIO.opts"

// Monitoring - histogram persistency Svc
#include "$STDOPTS/RootHist.opts"

RootHistCnv.ForceAlphaIds = true;

#pragma print off
/// time profiling
AuditorSvc.Auditors = { "ChronoAuditor" };
ApplicationMgr.ExtSvc += { "AuditorSvc" };
ChronoAuditor.Enable = true;
///
//========================================================================
//DetectorDataSvc.DetDbLocation = "$XMLDDDBROOT/DDDB/lhcb-200601.xml";
// or ACDC specific
#include "$DDDBROOT/options/DC06.opts"

//#include "$DDDBROOT/options/DDDB.opts"
//MagneticFieldSvc.FieldMapFile = "$FIELDMAPROOT/cdf/field047.cdf";
//DetectorDataSvc.DetDbLocation = "$XMLDDDBROOT/DDDB/Velo/VeloACDC/ACDC3.xml";
EventPersistencySvc.CnvServices += {"LHCb::RawDataCnvSvc"};




// Vetra sequences - analysis of Non-zero suppressed real data
ApplicationMgr.TopAlg += {
			  "ProcessPhase/TELL1Processing"

                         };

TELL1Processing.DetectorList+={ "VELO" };
TELL1ProcessingVELOSeq.Members+={ "PrepareVeloFullRawBuffer" };
//PrepareVeloFullRawBuffer.OutputLevel = 2;
//TELL1ProcessingVELOSeq.Members+={ "DecodeVeloFullRawBuffer" };


/// --> TELL1 emulation
//#include "$VETRAROOT/options/Velo/TELL1Emulator.opts"
//VeloTELL1EmulatorInit.ConvergenceLimit=4000;

ApplicationMgr.AuditAlgorithms = True;
ApplicationMgr.ExtSvc += { "ToolSvc", "AuditorSvc"};
AuditorSvc.Auditors += {"TimingAuditor/TIMER" } ;


//Override CondDB with our TELL1s:
#include "$VETRAROOT/options/Velo/commissioning/CommissioningConditions.opts"


/* ++++++++++++++ Step 1. How many events? ++++++++++++++++++ */

// -1 all events
ApplicationMgr.EvtMax = -1;
// to skip some events
EventSelector.FirstEvent = 1;
// printing freq
EventSelector.PrintFreq = 100;

/* ++++++++++++++ Step 2. Define input file(s) +++++++++++++++++ */

//Sample file on CASTOR:
EventSelector.Input += {
//"DATAFILE='rfio:///castor/cern.ch/user/a/arasp/slicetest/uniformityTest_ntp_tell207_odinControl_071112_1530.dat' SVC='LHCb::MDFSelector'"

//Sample file on the online network:
"DATAFILE='file://$INPUT_FILE' SVC='LHCb::MDFSelector'"

};

/* ++++++++++++++ Step 3. Define output root file ++++++++++++++ */

HistogramPersistencySvc.OutputFile = "$ROOT_FILE";

/* ++++++++++++++ Step 4. Options for low level plots +++++++++ */

// The 3 standard low level algorithms are
// * PedestalMon for pedestal monitoring
// * ADCMon for plots of raw ADC spectra
// * Noise for noise monitoring
// * BadChannel for bad channel monitoring

//Include standard options for noise and pedestal monitoring:
#include "$VELODATAMONITORROOT/options/ErrorMonitor.opts"


/* ++++++++++ Debugging steps +++++++++++++++++++*/

//TELL1 binary output checker

//Uncomment this if you want to run the corrupt event checker:
//TELL1ProcessingVELOSeq.Members+={ "TELL1BinaryOutputChecker" };
//TELL1BinaryOutputChecker.DropIncompleteEvents=true;

//Make sure that this number corresponds to the #TELL1s in the data:
//TELL1BinaryOutputChecker.ExpectedNumberOfTELL1s=1;

//Uncomment these lines if you want to print out the bank info:
//TELL1BinaryOutputChecker.PrintBankInfo=true;
//TELL1BinaryOutputChecker.OutputLevel=3;


//ODIN bank print-outs:
//TELL1ProcessingVELOSeq.Members+={ "PrintODINBank" };
//PrintODINBank.PrintExtendedInfo=true;

//How often should the algorithm print out:
//PrintODINBank.PrintFrequency=1;

/*  ++++++++++++ Backwards compatibility... +++++++++++ */
/*
//To turn on old monitoring: 
TELL1ProcessingVELOSeq.Members+={"FullDataMonitorPedestalsAndNoise/PedAndNoise"};
PedAndNoise.ChosenTELL1Number = 207;
PedAndNoise.PedestalsAndNoiseFirstEvent=4000;
PedAndNoise.PlotHistos=true;
PedAndNoise.PlotADCData=true;
*/
