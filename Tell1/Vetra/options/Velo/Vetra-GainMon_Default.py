"""
###############################################################################
#                                                                             #
#  Example default file to run Gain Monitoring                                #
#                                                                             #
#  Example usage:                                                             #
#      gaudirun.py Vetra-GainMon_Default.py inputData.py                      #
#                                                                             #
#   Produces default plots for non-Test Pulse NZS data which can be read by   #
#   $VETRASCRIPTS/macros/drawFHSPlots.C                                       #
#                                                                             #
#  @package Velo/VeloDataMonitor                                              #
#  @author  Grant McGregor  (Grant.McGregor@hep.manchester.ac.uk )            #
#  @author  Karol Hennessy  (karol.hennessy@cern.ch)                          #
#  @date    10/10/2009                                                        #
#                                                                             #
###############################################################################
"""
from Gaudi.Configuration import EventSelector

from Configurables import Vetra
from Configurables import GaudiSequencer
from Configurables import Velo__Monitoring__GainMon as GainMon

from GaudiKernel.Configurable import appendPostConfigAction
from Configurables import VeloPedestalSubtractorMoni      as PedestalMon


vetra = Vetra()

vetra.DataBanks    = 'NZS'
vetra.RunEmulation = True
vetra.EvtMax       = 5100    #You should run on a number above the NoiseMon convergence limit.
vetra.FilterBeamBeam = False

EventSelector().PrintFreq = 100

MoniVELOSeq=GaudiSequencer('MoniVELOSeq') 
MoniVELOSeq.Members+=[GainMon('GainMon')]

##Some options. They are set by default, but you can change them here:
##GainMon( 'GainMon').PlotHeadersHighLow=True
##GainMon( 'GainMon').PlotHeaders=True
##GainMon( 'GainMon').MaxToPlot=750 
##GainMon( 'GainMon').MinToPlot=400
##GainMon( 'GainMon').NForAvgCalc=500
##GainMon( 'GainMon').isRR=True   #Is is Round Robin data?

def removeAlgo():
    algo =  PedestalMon( )
    GaudiSequencer( 'Moni_NZS').Members.remove(algo)
appendPostConfigAction( removeAlgo )    

vetra.HistogramFile = 'gainMon_Default_Histos.root'

EventSelector().Input = [
    "DATAFILE='/daqarea/lhcb/data/2009/RAW/FULL/VELO/TESTPULSENZS/58814/058814_0000000001.raw' SVC='LHCb::MDFSelector'"
    ]

###############################################################################
