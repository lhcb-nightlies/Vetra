"""
##############################################################################
#                                                                            #
#  These algorithms must be added to check the trained processing            #
#  parameters
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Tomasz Szumlak     ( t.szumlak@physics.gla.ac.uk   )             #
#  @date    21/04/2009                                                       #
#                                                                            #
##############################################################################
"""

# =============================================================================
# Import necessary modules
# =============================================================================

from Gaudi.Configuration import *
from Configurables       import GaudiSequencer
from Configurables       import VeloBeetleHeaderXTalkComputer
from Configurables       import VeloPedestalSubtractorMoni

# --> configure computer algos
VeloBeetleHeaderXTalkComputer().ConvergenceLimit = 0
VeloPedestalSubtractorMoni().PlotALinks = False
VeloPedestalSubtractorMoni().RunOffline = 1

training = GaudiSequencer( 'TELL1Emulation' )

training.Members += [   VeloBeetleHeaderXTalkComputer()
                       ,VeloPedestalSubtractorMoni()
                      ]

GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ training ]
