"""
###############################################################################
#                                                                             #
#  Example default file to run Vetra on NZS data with emulation               #
#  *and* with expert-mode NZS monitoring                                      #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-Default-NZS_Data-Emul.py                               #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Karol Hennessy                                                    #
#  @date    08/10/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration      import EventSelector
from GaudiKernel.Configurable import appendPostConfigAction

from Configurables import GaudiSequencer
from Configurables import Vetra

from Configurables import Velo__Monitoring__FeedNoise     as FeedNoise
from Configurables import Velo__Monitoring__NoiseMon      as NoiseMon
from Configurables import VeloPedestalSubtractorMoni      as PedSubMon
from Configurables import Velo__Monitoring__ADCMon        as ADCMon
from Configurables import Velo__Monitoring__GainMon       as GainMon
from Configurables import Velo__Monitoring__BadChannelMon as BadChannelMon
from Configurables import Velo__Monitoring__ErrorMon      as ErrorMon
from Configurables import VeloPedestalSubtractorMoni as PedestalMon
from Configurables import VeloBeetleHeaderXTalkCorrectionMoni as BHXTMon


vetra = Vetra()

vetra.DataBanks     = 'NZS'
vetra.RunEmulation  = True
vetra.EmuConvLimit  = 1000
vetra.SkipEvents    = 1000
vetra.EvtMax        = 2005
vetra.HistogramFile = 'VeloDebug_histos.root'

EventSelector().PrintFreq = 100

EventSelector().Input = [
    "DATAFILE='/castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/NZS/58746/058746_0000000001.raw' SVC='LHCb::MDFSelector'"
    ]


def configExpertNZSMoni():
        moni_NZS = GaudiSequencer( 'Moni_NZS' )
	
	gainMon = GainMon('GainMon')
	
        moni_NZS.Members = [    FeedNoise()
                               ,NoiseMon('NoiseMon')
                               #,ADCMon('ADCMon')
                               ,gainMon
                               ,PedestalMon()
                               #,BHXTMon()
                               ]

	gainMon.MaxEvts            = 500
        gainMon.NForAvgCalc        = 250 
        gainMon.PlotHeaders        = True
        gainMon.PlotHeadersHighLow = False
        NoiseMon('NoiseMon').PublishingFrequency = 500
        FeedNoise().PublishingFrequency = 500
        ADCMon('ADCMon').RefreshRate = 200
        ADCMon('ADCMon').MaxEvts     = 201
	#Uncomment to specify TELL1s:
        #ADCMon().TELL1s = [ 25, 23 ]
        #Uncomment to turn on 2D histograms:
        ADCMon('ADCMon').Plot2DHistos = True
        #Uncomment to select only some containers for monitoring:
        ADCMon('ADCMon').Containers = [  'Raw/Velo/DecodedADC'
        #                                  ,'Raw/Velo/SubtractedPedADCs'
        #                                  ,'Raw/Velo/FIRcorrected'
        #                                  ,'Raw/Velo/ADCCMSuppressed'			
                                           ]

	#PedSubMon().DeltaPed = 127.5

        GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_NZS ]

appendPostConfigAction( configExpertNZSMoni )

###############################################################################
