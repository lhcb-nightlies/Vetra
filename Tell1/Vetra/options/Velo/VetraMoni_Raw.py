"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of non-zero-suppressed data.         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Tomasz Szumlak ( szumlak@agh.edu.pl )                            #
#  @date    01/03/2012                                                       #
#                                                                            #
##############################################################################
"""

from GaudiKernel.ProcessJobOptions import importOptions

from Configurables import GaudiSequencer
from Configurables import VeloTELL1ADCReader as RawReader
from Configurables import Vetra

moni_NZS = GaudiSequencer( 'Moni_NZS' )

moni_NZS.Members += [  RawReader('RawReader')
                     ]
RawReader('RawReader').PlotAllData = True

##############################################################################
