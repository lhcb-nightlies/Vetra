"""
###############################################################################
#                                                                             #
#  Example default file to run Vetra on first collisions data (12/2009)       #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-2009-Collisions.py                                     #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )               #
#  @date   09/12/2009                                                         #
#                                                                             #
###############################################################################
"""

from GaudiKernel.ProcessJobOptions import importOptions
from GaudiKernel.Configurable      import appendPostConfigAction
from Gaudi.Configuration           import EventSelector

from Configurables import GaudiSequencer, ProcessPhase
from Configurables import Vetra
from Configurables import RecSysConf, TrackSys
from Configurables import DecodeVeloRawBuffer
from Configurables import TrackVertexMonitor

from Configurables import PatPVOffline
from Configurables import PVOfflineTool, LSAdaptPVFitter, PVSeedTool


vetra = Vetra()

vetra.DataBanks = 'ZS'

EventSelector().PrintFreq = 1

# ============================================================================
# Additional options for accessing the Oracle conditions DB
# ============================================================================

import os
if os.environ['HOSTNAME'].startswith('plus'):
        print "Running on plus"
        importOptions( '/home/clemenci/public/Online-Oracle.py' )
        
importOptions( '$APPCONFIGOPTS/UseOracle.py' )

# ============================================================================
# Set up the reconstruction phase: PR + fitting + PV finding
# ============================================================================
ProcessPhase( 'Reco' ).Context = 'Offline'

RecSysConf().DataType     = vetra.getProp( 'DataType' )
# Do all tracking, for now (but no RICH, CALO, MUON, PROTO process phases)!
RecSysConf().RecoSequence = [ 'VELO', 'TT', 'IT', 'OT', 'Tr', 'Vertex' ]
# By default the VELO is open ;-)
RecSysConf().SpecialData += [ 'veloOpen' ]

# Uncomment to run only VELO PR
#TrackSys().TrackPatRecAlgorithms = [ 'Velo' ]

def vetraChanges() :
    # Loose PV options from Mariusz !
    myPVSeedTool      = PVSeedTool()
    myLSAdaptPVFitter = LSAdaptPVFitter()
    
    myPVOfflineTool = PVOfflineTool()
    
    myPVSeedTool.minClusterMult          = 2
    myPVSeedTool.minCloseTracksInCluster = 2
    
    myLSAdaptPVFitter.MinTracks = 2
    
    myPVOfflineTool.addTool( myPVSeedTool, 'PVSeedTool' )
    myPVOfflineTool.addTool( myLSAdaptPVFitter, 'LSAdaptPVFitter' )
    
    myPVOffline = PatPVOffline()
    myPVOffline.addTool( myPVOfflineTool, 'PVOfflineTool' )

    myPVOffline.PropertiesPrint = True

    # Simplify Brunel's Vertex sequence to the bare minimum required
    GaudiSequencer( 'RecoVertexSeq' ).Members = [ myPVOffline ]
    
    # Add the monitoring of PVs
    GaudiSequencer( 'Moni_ZS' ).Members.append( TrackVertexMonitor() )

    # Remove this double instantiation in Brunel sequence to avoid conflicts
    GaudiSequencer( 'RecoVELOSeq' ).Members.remove( DecodeVeloRawBuffer( 'DecodeVeloClusters' ) )

appendPostConfigAction( vetraChanges )

###############################################################################
