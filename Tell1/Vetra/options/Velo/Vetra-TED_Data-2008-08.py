"""
###############################################################################
#                                                                             #
#  Example file to run Vetra on TED data taken in August 2008                 #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-TED_Data-2008-08.py                                    #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @date    10/03/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration import EventSelector
from Configurables       import UpdateManagerSvc

from Configurables import Vetra

from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters

vetra = Vetra()

# only ZS data present
vetra.DataBanks = 'ZS'
# do the time-alignment study
vetra.CheckTAEs = True
vetra.TEDTracks = True
vetra.MagnetOn = False

# set the appropriate DB tags
vetra.DDDBtag        = 'head-20080908'
vetra.CondDBtag      = 'head-20080908'

EventSelector().PrintFreq = 1

# Special TED data settings for the DecodeVeloRawBuffer algorithm
# (see $VETRAOPTS/Velo/VetraDecoding_ZS.py)
decodeToVeloClusters = DefaultDecoderToVeloClusters( 'DecodeToVeloClusters' )
decodeToVeloClusters.ForceBankVersion = 3
decodeToVeloClusters.RawEventLocation = 'Prev2/DAQ/RawEvent'

# Special TED data settings for the DecodeVeloRawBuffer algorithm
# (see $VETRAOPTS/Velo/VetraDecoding_TAE.py)
DefaultDecoderToVeloClusters( 'DecodePrev1' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodePrev2' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodePrev3' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodePrev4' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodePrev5' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodePrev6' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodePrev7' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodeNext1' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodeNext2' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodeNext3' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodeNext4' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodeNext5' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodeNext6' ).ForceBankVersion = 3
DefaultDecoderToVeloClusters( 'DecodeNext7' ).ForceBankVersion = 3
 

UpdateManagerSvc().ConditionsOverride +=  ["Conditions/Online/Velo/MotionSystem := double ResolPosRC =-28.5 ; double ResolPosLA = 28.5 ;"]

# August TED1 data, runs 30930-30933
EventSelector().Input = [
  "DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30928/030928_0000077695.raw' SVC='LHCb::MDFSelector'"
, "DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30929/030929_0000077696.raw' SVC='LHCb::MDFSelector'"
, "DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30930//030930_0000077697.raw' SVC='LHCb::MDFSelector'"
, "DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30931/030931_0000077700.raw' SVC='LHCb::MDFSelector'"
, "DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30932/030932_0000077701.raw' SVC='LHCb::MDFSelector'"
, "DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30933/030933_0000077704.raw' SVC='LHCb::MDFSelector'"
    ]

###############################################################################
