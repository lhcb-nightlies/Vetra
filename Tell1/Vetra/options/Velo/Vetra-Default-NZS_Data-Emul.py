"""
###############################################################################
#                                                                             #
#  Example default file to run Vetra on NZS data with emulation               #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-Default-NZS_Data-Emul.py                               #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @date    12/03/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration import EventSelector

from Configurables import Vetra
from Configurables import VetraMoniConf


vetra = Vetra()

vetra.DataBanks    = 'NZS'
vetra.RunEmulation = True
vetra.EvtMax       = 1000

VetraMoniConf().PlotRawData  = True

EventSelector().PrintFreq = 100

#EventSelector().Input = [
#    "DATAFILE='root:/castor/cern.ch/grid/lhcb/data/2012/RAW/FULL/VELO/NZS/108580/108580_0000000001.raw' SVC='LHCb::MDFSelector'"
#    ]
###############################################################################
