"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of ODIN data.                        #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author Marco Gersabeck (marco.gersabeck@cern.ch)                         #
#  @date   12/03/2010                                                        #
#                                                                            #
##############################################################################
"""

from GaudiKernel.ProcessJobOptions import importOptions

from Configurables import GaudiSequencer
from Configurables import PrintODINBank


moni_Odin = GaudiSequencer( 'Moni_Odin' )

moni_Odin.Members = [ PrintODINBank( 'PrintODINBank' )
                    ]

printOdin = PrintODINBank( 'PrintODINBank' )
printOdin.PrintExtendedInfo = True
printOdin.PrintFrequency = 1000000000 # effectively print info only for first event
printOdin.UseVeloOdin = False

GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_Odin ]

##############################################################################
