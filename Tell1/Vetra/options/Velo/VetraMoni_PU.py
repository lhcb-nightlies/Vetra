"""
##############################################################################
#                                                                            #
#  Monitoring of the PU digital banks                                        #
#                                                                            #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Serena Oggero   				                     #
#  @date    22/10/2009                                                       #
#                                                                            #
##############################################################################
"""

from Configurables           import GaudiSequencer
from Configurables           import L0DUFromRawAlg
from Configurables           import PUClustersMoni

importOptions("$L0TCK/L0DUConfig.opts")

## prefix for output ROOT files
name = 'Vetra_histos-PU'

## decode L0DU
l0DuFromRaw = L0DUFromRawAlg( 'L0DUFromRaw' )
l0DuFromRaw.OutputLevel = 3
 
## PU moni
moni_PU = GaudiSequencer( 'Moni_PU' )

puMoni = PUClustersMoni()
puMoni.OutputFileName   = name + ".root"
puMoni.OnLineMonitoring = True
puMoni.MoniBinaryNZS    = False

puMoniPrev1 = PUClustersMoni( 'PUClusterMoniPrev1' )
puMoniPrev1.OutputFileName   = name + "_prev1.root"
puMoniPrev1.RawEventLocation = 'Prev1/DAQ/RawEvent'
puMoniPrev1.PUclusterContZS  = 'Raw/Velo/Prev1/PUClusters'
puMoniPrev1.PUclusterContNZS = 'Raw/Velo/Prev1/PUClustersNZS'
# the following are not needed since we put in the default location the clusters
# just accessing the RawEvt from the Prev location
#puMoniPrev1.PUanalogclusterContNZS = 'Raw/Velo/Prev1/DecodedADC' 
#puMoniPrev1.PUanalogheaderContNZS = 'Raw/Velo/Prev1/DecodedHeaders'

puMoniPrev2 = PUClustersMoni( 'PUClusterMoniPrev2' )
puMoniPrev2.OutputFileName   = name + "_prev2.root"
puMoniPrev2.RawEventLocation = 'Prev2/DAQ/RawEvent'
puMoniPrev2.PUclusterContZS  = 'Raw/Velo/Prev2/PUClusters'
puMoniPrev2.PUclusterContNZS = 'Raw/Velo/Prev2/PUClustersNZS'

puMoniNext1 = PUClustersMoni( 'PUClusterMoniNext1' )
puMoniNext1.OutputFileName   = name + "_next1.root"
puMoniNext1.RawEventLocation = 'Next1/DAQ/RawEvent'
puMoniNext1.PUclusterContZS  = 'Raw/Velo/Next1/PUClusters'
puMoniNext1.PUclusterContNZS = 'Raw/Velo/Next1/PUClustersNZS'

puMoniNext2 = PUClustersMoni( 'PUClusterMoniNext2' )
puMoniNext2.OutputFileName   = name + "_next2.root"
puMoniNext2.RawEventLocation = 'Next2/DAQ/RawEvent'
puMoniNext2.PUclusterContZS  = 'Raw/Velo/Next2/PUClusters'
puMoniNext2.PUclusterContNZS = 'Raw/Velo/Next2/PUClustersNZS'

moni_PU.Members = [ l0DuFromRaw,
		    puMoni
                    #puMoniPrev1
                    #puMoniPrev2
                    #puMoniNext1
                    #puMoniNext2		     		    		     
                  ]

GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_PU ]
