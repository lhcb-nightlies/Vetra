"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of non-zero-suppressed data.         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
##############################################################################
"""

from Gaudi.Configuration     import EventSelector, MessageSvc
msgsvc = MessageSvc()
msgsvc.OutputLevel = 3
from Configurables import LHCbApp
from Configurables import Vetra
vetra = Vetra()
vetra.DataBanks      = 'NONE'
vetra.RunEmulation   = False
#Will include my  own version of the emulator.
#vetra.EmuMode        = 'Dynamic'
vetra.EmuMode        = 'Static'
vetra.BitPerfectness = False
vetra.OutputType     = 'NONE'
vetra.FilterBeamBeam = False
# --> generated data are taken wihtout the ODIN bank
# set the appropriate DB tags
#LHCbApp().DDDBtag   = 'head-20080905'
#LHCbApp().CondDBtag = 'head-20080905'
#LHCbApp().CondDBtag = ''
from GaudiKernel.ProcessJobOptions import importOptions
importOptions("$VETRAROOT/options/Velo/TAEDecodingNZS-PulseShapeScan.py")
from Configurables import CondDB
CondDB().IgnoreHeartBeat = True

#vetra.DataType  =   '2010'
#vetra.DataBanks  =   'NZS+ZS'
#vetra.Context  =   'Offline'
#vetra.VELOCONDtag  =   ''
#vetra.DDDBtag  =   ''
#vetra.CondDBtag  =   ''
#vetra.UseOracle  =   True
#vetra.Simulation  =   False
vetra.RunEmulation  =   False
vetra.EmuMode  =   'Dynamic'
vetra.EmuConvLimit  =   0
vetra.MoniDataFilter  =   'NONE'
vetra.CheckEBs  =   True
vetra.MagnetOn  =   True
vetra.TEDTracks  =   False
vetra.ParaTraining  =   False
vetra.BitPerfectness  =   False

from Configurables import GaudiSequencer
from Configurables import Velo__PulseShapePlotter as PulseShape

moni_pulseshape = GaudiSequencer( 'Moni_PulseShape' )
pulseShape = PulseShape( 'PulseShape' )
moni_pulseshape.Members = [pulseShape]
pulseShape.StepSize = 100000000
pulseShape.NSteps  = 50
#EventSelector().FirstEvent = 0
EventSelector().PrintFreq = 100
vetra.EvtMax = -1
#vetra.EvtMax = 1000
#vetra.SkipEvents = 5000 #commented out for collision!
pulseShape.SkipStepZero = True
#pulseShape.SamplingLocations = ["Prev3", "Prev2", "Prev1","","Next1","Next2","Next3"] 
pulseShape.SamplingLocations = [ "Prev2", "Prev1","","Next1","Next2"]
#pulseShape.SamplingLocations = [ ""]
pulseShape.OutputLevel = 4
pulseShape.DataLocation = "Raw/Velo/PreparedADCs"
#pulseShape.DataLocation = "Raw/Velo/DecodedADC"
#pulseShape.SamplingLocations = [ ""]
pulseShape.CMSCut = 25
#pulseShape.ProgressiveCMS = False 
pulseShape.SignalThreshold = 11
pulseShape.EventsPerFile=-1
pulseShape.NumberOfCMSIterations = 3
pulseShape.IntegralCut = 38
pulseShape.NEventsPerStep = -1
#pulseShape.TELL1s=[23]
#pulseShape.WidthBefore=15.7 from tp
#pulseShape.WidthAfter=23.3  from tp
#out from june ted: 
#pulseShape.WidthBefore=17.0
#pulseShape.WidthAfter=23.1
# @ -30
#pulseShape.WidthBefore=13.4
#pulseShape.WidthAfter=19.9
pulseShape.RunWithChosenBunchCounters= True
#pulseShape.ChosenBunchCounters = [1886, 995]
#pulseShape.RunNumbers = [63462, 63468, 63470,63472,63475] 
#pulseShape.RunNumbersDelay = [0,  0, 12.5, 19, 6.5]


def mystuff():
  from Configurables import Velo__OdinFilter as OdinFilter
  odinfilter = OdinFilter() 
  odinfilter.ChosenBunchCounters = [995, 1886]
  odinfilter.RunWithChosenBunchCounters = True 
  odinfilter.SkipStepZero = True
  odinfilter.SkipSteps = [0,1]  # in case of skipping other steps as well
  odinfilter.NEventsPerStep = -1
  odinfilter.OutputLevel = 4
  GaudiSequencer( 'MoniVELOSeq' ).Members = [moni_pulseshape]
  GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members.insert(0,odinfilter)

appendPostConfigAction( mystuff )

#importOptions("$VETRAROOT/options/Velo/filesTimingScan2011.py")
importOptions("$VETRAROOT/options/Velo/filesTimingScanTest.py")
vetra.HistogramFile  = 'TimingRun87214.root'
