from GaudiKernel.ProcessJobOptions import importOptions

from Configurables import GaudiSequencer

from Configurables import Vetra
from Configurables import PrepareVeloFullRawBuffer, DecodeVeloFullRawBuffer
from Configurables import VeloTELL1EmulatorInit
from Configurables import dataTranslator


decodingTAENZS = GaudiSequencer( 'DecodingTAENZS' )

preparePrev1  = PrepareVeloFullRawBuffer("preparePrev1")
preparePrev2  = PrepareVeloFullRawBuffer("preparePrev2")
preparePrev3  = PrepareVeloFullRawBuffer("preparePrev3")
preparePrev4  = PrepareVeloFullRawBuffer("preparePrev4")
preparePrev5  = PrepareVeloFullRawBuffer("preparePrev5")
preparePrev6  = PrepareVeloFullRawBuffer("preparePrev6")
preparePrev7  = PrepareVeloFullRawBuffer("preparePrev7")
prepareNext1  = PrepareVeloFullRawBuffer("prepareNext1")
prepareNext2  = PrepareVeloFullRawBuffer("prepareNext2")
prepareNext3  = PrepareVeloFullRawBuffer("prepareNext3")
prepareNext4  = PrepareVeloFullRawBuffer("prepareNext4")
prepareNext5  = PrepareVeloFullRawBuffer("prepareNext5")
prepareNext6  = PrepareVeloFullRawBuffer("prepareNext6")
prepareNext7  = PrepareVeloFullRawBuffer("prepareNext7")
prepareCentral= PrepareVeloFullRawBuffer("prepareCentral")
decodePrev1  = DecodeVeloFullRawBuffer("decodePrev1")
decodePrev2  = DecodeVeloFullRawBuffer("decodePrev2")
decodePrev3  = DecodeVeloFullRawBuffer("decodePrev3")
decodePrev4  = DecodeVeloFullRawBuffer("decodePrev4")
decodePrev5  = DecodeVeloFullRawBuffer("decodePrev5")
decodePrev6  = DecodeVeloFullRawBuffer("decodePrev6")
decodePrev7  = DecodeVeloFullRawBuffer("decodePrev7")
decodeNext1  = DecodeVeloFullRawBuffer("decodeNext1")
decodeNext2  = DecodeVeloFullRawBuffer("decodeNext2")
decodeNext3  = DecodeVeloFullRawBuffer("decodeNext3")
decodeNext4  = DecodeVeloFullRawBuffer("decodeNext4")
decodeNext5  = DecodeVeloFullRawBuffer("decodeNext5")
decodeNext6  = DecodeVeloFullRawBuffer("decodeNext6")
decodeNext7  = DecodeVeloFullRawBuffer("decodeNext7")
decodeCentral= DecodeVeloFullRawBuffer("decodeCentral")
decodingTAENZS.Members = [
                             prepareCentral
                            ,decodeCentral
                            ,preparePrev1        
                            ,preparePrev2  
                            ,preparePrev3  
#                            ,preparePrev4  
#                            ,preparePrev5  
#                            ,preparePrev6  
#                            ,preparePrev7  
                            ,prepareNext1  
                            ,prepareNext2  
                            ,prepareNext3  
#                            ,prepareNext4  
#                            ,prepareNext5  
#                            ,prepareNext6  
#                            ,prepareNext7  
                            ,decodePrev1  
                            ,decodePrev2  
                            ,decodePrev3  
#                            ,decodePrev4  
#                            ,decodePrev5  
#                            ,decodePrev6  
#                            ,decodePrev7  
                            ,decodeNext1  
                            ,decodeNext2  
                            ,decodeNext3  
#                            ,decodeNext4  
#                            ,decodeNext5  
#                            ,decodeNext6  
 #                           ,decodeNext7
 ] 


#prepareCentral.RawEventLocation="DAQ/RawEvent" 
preparePrev1.RawEventLocation="Prev1/DAQ/RawEvent"
preparePrev2.RawEventLocation="Prev2/DAQ/RawEvent"
preparePrev3.RawEventLocation="Prev3/DAQ/RawEvent"
preparePrev4.RawEventLocation="Prev4/DAQ/RawEvent"
preparePrev5.RawEventLocation="Prev5/DAQ/RawEvent"
preparePrev6.RawEventLocation="Prev6/DAQ/RawEvent"
preparePrev7.RawEventLocation="Prev7/DAQ/RawEvent"
prepareNext1.RawEventLocation="Next1/DAQ/RawEvent"
prepareNext2.RawEventLocation="Next2/DAQ/RawEvent"
prepareNext3.RawEventLocation="Next3/DAQ/RawEvent"
prepareNext4.RawEventLocation="Next4/DAQ/RawEvent"
prepareNext5.RawEventLocation="Next5/DAQ/RawEvent"
prepareNext6.RawEventLocation="Next6/DAQ/RawEvent"
prepareNext7.RawEventLocation="Next7/DAQ/RawEvent"



preparePrev1.ADCLocation="Prev1/Raw/Velo/ADCBank" 
preparePrev2.ADCLocation="Prev2/Raw/Velo/ADCBank" 
preparePrev3.ADCLocation="Prev3/Raw/Velo/ADCBank" 
preparePrev4.ADCLocation="Prev4/Raw/Velo/ADCBank" 
preparePrev5.ADCLocation="Prev5/Raw/Velo/ADCBank" 
preparePrev6.ADCLocation="Prev6/Raw/Velo/ADCBank" 
preparePrev7.ADCLocation="Prev7/Raw/Velo/ADCBank" 
prepareNext1.ADCLocation="Next1/Raw/Velo/ADCBank" 
prepareNext2.ADCLocation="Next2/Raw/Velo/ADCBank" 
prepareNext3.ADCLocation="Next3/Raw/Velo/ADCBank" 
prepareNext4.ADCLocation="Next4/Raw/Velo/ADCBank" 
prepareNext5.ADCLocation="Next5/Raw/Velo/ADCBank" 
prepareNext6.ADCLocation="Next6/Raw/Velo/ADCBank" 
prepareNext7.ADCLocation="Next7/Raw/Velo/ADCBank" 
#prepareCentral.ADCLocation="Raw/Velo/ADCBank"




decodePrev1.ADCLocation="Prev1/Raw/Velo/ADCBank" 
decodePrev2.ADCLocation="Prev2/Raw/Velo/ADCBank" 
decodePrev3.ADCLocation="Prev3/Raw/Velo/ADCBank" 
decodePrev4.ADCLocation="Prev4/Raw/Velo/ADCBank" 
decodePrev5.ADCLocation="Prev5/Raw/Velo/ADCBank" 
decodePrev6.ADCLocation="Prev6/Raw/Velo/ADCBank" 
decodePrev7.ADCLocation="Prev7/Raw/Velo/ADCBank" 
decodeNext1.ADCLocation="Next1/Raw/Velo/ADCBank" 
decodeNext2.ADCLocation="Next2/Raw/Velo/ADCBank" 
decodeNext3.ADCLocation="Next3/Raw/Velo/ADCBank" 
decodeNext4.ADCLocation="Next4/Raw/Velo/ADCBank" 
decodeNext5.ADCLocation="Next5/Raw/Velo/ADCBank" 
decodeNext6.ADCLocation="Next6/Raw/Velo/ADCBank" 
decodeNext7.ADCLocation="Next7/Raw/Velo/ADCBank" 


decodePrev1.DecodedADCLocation="Prev1/Raw/Velo/DecodedADC" 
decodePrev2.DecodedADCLocation="Prev2/Raw/Velo/DecodedADC" 
decodePrev3.DecodedADCLocation="Prev3/Raw/Velo/DecodedADC" 
decodePrev4.DecodedADCLocation="Prev4/Raw/Velo/DecodedADC" 
decodePrev5.DecodedADCLocation="Prev5/Raw/Velo/DecodedADC" 
decodePrev6.DecodedADCLocation="Prev6/Raw/Velo/DecodedADC" 
decodePrev7.DecodedADCLocation="Prev7/Raw/Velo/DecodedADC" 
decodeNext1.DecodedADCLocation="Next1/Raw/Velo/DecodedADC" 
decodeNext2.DecodedADCLocation="Next2/Raw/Velo/DecodedADC" 
decodeNext3.DecodedADCLocation="Next3/Raw/Velo/DecodedADC" 
decodeNext4.DecodedADCLocation="Next4/Raw/Velo/DecodedADC" 
decodeNext5.DecodedADCLocation="Next5/Raw/Velo/DecodedADC" 
decodeNext6.DecodedADCLocation="Next6/Raw/Velo/DecodedADC" 
decodeNext7.DecodedADCLocation="Next7/Raw/Velo/DecodedADC" 


decodePrev1.DecodedHeaderLocation="Prev1/Raw/Velo/DecodedHeaders" 
decodePrev2.DecodedHeaderLocation="Prev2/Raw/Velo/DecodedHeaders" 
decodePrev3.DecodedHeaderLocation="Prev3/Raw/Velo/DecodedHeaders" 
decodePrev4.DecodedHeaderLocation="Prev4/Raw/Velo/DecodedHeaders" 
decodePrev5.DecodedHeaderLocation="Prev5/Raw/Velo/DecodedHeaders" 
decodePrev6.DecodedHeaderLocation="Prev6/Raw/Velo/DecodedHeaders" 
decodePrev7.DecodedHeaderLocation="Prev7/Raw/Velo/DecodedHeaders" 
decodeNext1.DecodedHeaderLocation="Next1/Raw/Velo/DecodedHeaders" 
decodeNext2.DecodedHeaderLocation="Next2/Raw/Velo/DecodedHeaders" 
decodeNext3.DecodedHeaderLocation="Next3/Raw/Velo/DecodedHeaders" 
decodeNext4.DecodedHeaderLocation="Next4/Raw/Velo/DecodedHeaders" 
decodeNext5.DecodedHeaderLocation="Next5/Raw/Velo/DecodedHeaders" 
decodeNext6.DecodedHeaderLocation="Next6/Raw/Velo/DecodedHeaders" 
decodeNext7.DecodedHeaderLocation="Next7/Raw/Velo/DecodedHeaders" 





decodePrev1.EventInfoLocation="Prev1/Raw/Velo/EvtInfo" 
decodePrev2.EventInfoLocation="Prev2/Raw/Velo/EvtInfo" 
decodePrev3.EventInfoLocation="Prev3/Raw/Velo/EvtInfo" 
decodePrev4.EventInfoLocation="Prev4/Raw/Velo/EvtInfo" 
decodePrev5.EventInfoLocation="Prev5/Raw/Velo/EvtInfo" 
decodePrev6.EventInfoLocation="Prev6/Raw/Velo/EvtInfo" 
decodePrev7.EventInfoLocation="Prev7/Raw/Velo/EvtInfo" 
decodeNext1.EventInfoLocation="Next1/Raw/Velo/EvtInfo" 
decodeNext2.EventInfoLocation="Next2/Raw/Velo/EvtInfo" 
decodeNext3.EventInfoLocation="Next3/Raw/Velo/EvtInfo" 
decodeNext4.EventInfoLocation="Next4/Raw/Velo/EvtInfo" 
decodeNext5.EventInfoLocation="Next5/Raw/Velo/EvtInfo" 
decodeNext6.EventInfoLocation="Next6/Raw/Velo/EvtInfo" 
decodeNext7.EventInfoLocation="Next7/Raw/Velo/EvtInfo" 

decodePrev1.DecodedPedestalLocation="Prev1/Raw/Velo/DecodedPed" 
decodePrev2.DecodedPedestalLocation="Prev2/Raw/Velo/DecodedPed" 
decodePrev3.DecodedPedestalLocation="Prev3/Raw/Velo/DecodedPed" 
decodePrev4.DecodedPedestalLocation="Prev4/Raw/Velo/DecodedPed" 
decodePrev5.DecodedPedestalLocation="Prev5/Raw/Velo/DecodedPed" 
decodePrev6.DecodedPedestalLocation="Prev6/Raw/Velo/DecodedPed" 
decodePrev7.DecodedPedestalLocation="Prev7/Raw/Velo/DecodedPed" 
decodeNext1.DecodedPedestalLocation="Next1/Raw/Velo/DecodedPed" 
decodeNext2.DecodedPedestalLocation="Next2/Raw/Velo/DecodedPed" 
decodeNext3.DecodedPedestalLocation="Next3/Raw/Velo/DecodedPed" 
decodeNext4.DecodedPedestalLocation="Next4/Raw/Velo/DecodedPed" 
decodeNext5.DecodedPedestalLocation="Next5/Raw/Velo/DecodedPed" 
decodeNext6.DecodedPedestalLocation="Next6/Raw/Velo/DecodedPed" 
decodeNext7.DecodedPedestalLocation="Next7/Raw/Velo/DecodedPed" 



decodePrev1.OutputLevel=3 
decodePrev2.OutputLevel=3 
decodePrev3.OutputLevel=3 
decodePrev4.OutputLevel=6 
decodePrev5.OutputLevel=6 
decodePrev6.OutputLevel=6 
decodePrev7.OutputLevel=6 
decodeNext1.OutputLevel=3 
decodeNext2.OutputLevel=3 
decodeNext3.OutputLevel=3 
decodeNext4.OutputLevel=6 
decodeNext5.OutputLevel=6 
decodeNext6.OutputLevel=6 
decodeNext7.OutputLevel=6 



preparePrev1.OutputLevel=3 
preparePrev2.OutputLevel=3 
preparePrev3.OutputLevel=3 
preparePrev4.OutputLevel=6 
preparePrev5.OutputLevel=6 
preparePrev6.OutputLevel=6
preparePrev7.OutputLevel=6 
prepareNext1.OutputLevel=3 
prepareNext2.OutputLevel=3
prepareNext3.OutputLevel=3 
prepareNext4.OutputLevel=6 
prepareNext5.OutputLevel=6 
prepareNext6.OutputLevel=6 
prepareNext7.OutputLevel=6 



decodeCentral.SectorCorrection=True 
decodeCentral.CableOrder=[3, 2, 1, 0] 

decodePrev1.SectorCorrection=True 
decodePrev2.SectorCorrection=True 
decodePrev3.SectorCorrection=True 
decodePrev4.SectorCorrection=True 
decodePrev5.SectorCorrection=True 
decodePrev6.SectorCorrection=True 
decodePrev7.SectorCorrection=True 
decodeNext1.SectorCorrection=True 
decodeNext2.SectorCorrection=True 
decodeNext3.SectorCorrection=True 
decodeNext4.SectorCorrection=True 
decodeNext5.SectorCorrection=True 
decodeNext6.SectorCorrection=True 
decodeNext7.SectorCorrection=True 

decodePrev1.CableOrder=[3, 2, 1, 0] 
decodePrev2.CableOrder=[3, 2, 1, 0] 
decodePrev3.CableOrder=[3, 2, 1, 0] 
decodePrev4.CableOrder=[3, 2, 1, 0] 
decodePrev5.CableOrder=[3, 2, 1, 0] 
decodePrev6.CableOrder=[3, 2, 1, 0] 
decodePrev7.CableOrder=[3, 2, 1, 0] 
decodeNext1.CableOrder=[3, 2, 1, 0] 
decodeNext2.CableOrder=[3, 2, 1, 0] 
decodeNext3.CableOrder=[3, 2, 1, 0] 
decodeNext4.CableOrder=[3, 2, 1, 0] 
decodeNext5.CableOrder=[3, 2, 1, 0] 
decodeNext6.CableOrder=[3, 2, 1, 0] 
decodeNext7.CableOrder=[3, 2, 1, 0] 


GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ decodingTAENZS ]
#GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members.remove( GaudiSequencer( 'Decoding_NZS' ) )

emulation = GaudiSequencer( 'TELL1Emulation' )
#emulation.Members = [   
translatePrev7  = dataTranslator("translatePrev7")
translatePrev6  = dataTranslator("translatePrev6")
translatePrev5  = dataTranslator("translatePrev5") 
translatePrev4  = dataTranslator("translatePrev4") 
translatePrev3  = dataTranslator("translatePrev3") 
translatePrev2  = dataTranslator("translatePrev2") 
translatePrev1  = dataTranslator("translatePrev1") 
translateNext1  = dataTranslator("translateNext1") 
translateNext2  = dataTranslator("translateNext2") 
translateNext3  = dataTranslator("translateNext3") 
translateNext4  = dataTranslator("translateNext4") 
translateNext5  = dataTranslator("translateNext5") 
translateNext6  = dataTranslator("translateNext6") 
translateNext7  = dataTranslator("translateNext7") 
emulation.Members = [VeloTELL1EmulatorInit() 
                         ,dataTranslator() 
#                         ,translatePrev7    
#                         ,translatePrev6   
#                         ,translatePrev5   
#                         ,translatePrev4   
                         ,translatePrev3   
                         ,translatePrev2   
                         ,translatePrev1   
                         ,translateNext1   
                         ,translateNext2   
                         ,translateNext3   
#                         ,translateNext4   
#                         ,translateNext5   
#                         ,translateNext6   
#                         ,translateNext7   
                            ]
translatePrev7.InputDataLoc ="Prev1/Raw/Velo/DecodedADC" 
translatePrev6.InputDataLoc ="Prev2/Raw/Velo/DecodedADC" 
translatePrev5.InputDataLoc ="Prev3/Raw/Velo/DecodedADC" 
translatePrev4.InputDataLoc ="Prev4/Raw/Velo/DecodedADC" 
translatePrev3.InputDataLoc ="Prev5/Raw/Velo/DecodedADC" 
translatePrev2.InputDataLoc ="Prev6/Raw/Velo/DecodedADC" 
translatePrev1.InputDataLoc ="Prev7/Raw/Velo/DecodedADC" 
translateNext1.InputDataLoc ="Next1/Raw/Velo/DecodedADC" 
translateNext2.InputDataLoc ="Next2/Raw/Velo/DecodedADC" 
translateNext3.InputDataLoc ="Next3/Raw/Velo/DecodedADC" 
translateNext4.InputDataLoc ="Next4/Raw/Velo/DecodedADC" 
translateNext5.InputDataLoc ="Next5/Raw/Velo/DecodedADC" 
translateNext6.InputDataLoc ="Next6/Raw/Velo/DecodedADC" 
translateNext7.InputDataLoc ="Next7/Raw/Velo/DecodedADC" 

GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ emulation ]

