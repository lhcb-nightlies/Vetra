"""
###############################################################################
#                                                                             #
#  Main script to run Vetra in "online" mode                                  #
#  Vetra is run with all default configurations and algorithms settings       #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py VetraOnline.py SomeInputFiles.py                             #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @date    12/03/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration import *
from Configurables       import Vetra


vetra = Vetra()

vetra.Context = 'HLT'
