# Options for NZS Charge Collection Efficiency code:
#Author: Jon Harrison, Dermot Moran
#Date: 17/02/2016

from Configurables import Vetra, VetraRecoConf, RecSysConf, TrackSys, GaudiSequencer, CondDB, CondDBAccessSvc
vetra = Vetra()

### Start user config ##########################################################

vetra.TupleFile = "CCE_Scan.root"
vetra.DataType = '2015'
vetra.EvtMax = 10
Sensors = []
for i in range(0, 41):
   Sensors.append(i) 

# Set to true for CCE scan data, false for physics data
CCEScanData = False

# To overwrite CondDB
#CondDB(). addLayer(
#    CondDBAccessSvc("CCEscans",
#        ConnectionString = "sqlite_file:CCEscan.db/LHCBCOND",
#        DefaultTAG = "HEAD"))

# If using recent data
CondDB().Online = True

### End user config ############################################################

RecSysConf().DataType = vetra.getProp( 'DataType' )
vetra.CCEscan = True
vetra.RunEmulation = True
vetra.CheckEBs = False
vetra.UseDBSnapshot = False
Run1 = False
if vetra.DataType != '2015':
   Run1 = True

# Change sequence depending on Run and add decoding back in
if Run1: 
   VetraRecoConf().Sequence = ["Decoding"] + RecSysConf().getProp( 'DefaultTrackingSubdets' )
   VetraRecoConf().TrackPatRecAlgorithms = TrackSys().getProp  ( 'DefaultPatRecAlgorithms' )
else: 
   VetraRecoConf().Sequence = ["Decoding"] + RecSysConf().getProp( 'DefaultTrackingSubdetsRun2' )
   VetraRecoConf().TrackPatRecAlgorithms = TrackSys().getProp  ( 'DefaultPatRecAlgorithmsRun2' )

# Configure pattern recognition for tracks from CCE tracking 
from Configurables import PatMatch, PatForward, PatVeloTT, PatVeloTTHybrid
PatMatch("PatMatch").VeloInput="Rec/Track/VeloExcl"
if Run1:
   PatForward("PatForward").InputTracksName="Rec/Track/VeloExcl"
   PatVeloTT("PatVeloTT").InputTracksName="Rec/Track/VeloExcl"
else:
   PatForward("PatForwardHLT1").InputTracksName="Rec/Track/VeloExcl"
   PatForward("PatForwardHLT2").InputTracksName="Rec/Track/VeloExcl"
   PatVeloTTHybrid("PatVeloTTHybrid").InputTracksName="Rec/Track/VeloExcl"

# Get the event time (for CondDb) from ODIN
from Configurables import EventClockSvc
EventClockSvc().EventTimeDecoder = "OdinTimeDecoder"

# Configure Vetra sequence
from TrackSys.Configuration import *
from Configurables import Tf__PatVeloGeneralTracking, Tf__PatVeloRHitManager, Tf__PatVeloPhiHitManager, TrackStateInitTool, TrackBestTrackCreator
from GaudiKernel.Configurable import appendPostConfigAction
def doMyChanges():
    # Remove DecodeVeloClusters this from "DecodeVeloClusters" (not a robust method): 
    del GaudiSequencer( 'RecoDecodingSeq' ).Members[0]
    
    GaudiSequencer("RecoVELOSeq").Members = [

              Tf__PatVeloGeneralTracking("PatVeloGeneralTracking",
                                          RHitManagerName=
                                          "PatVeloRHitManagerKill",
                                          PhiHitManagerName=
                                          "PatVeloPhiHitManagerKill",
                                          OutputTracksLocation=
                                          "Rec/Track/VeloExcl",
                                          CCEscan=CCEScanData
					)
        ]

    Tf__PatVeloRHitManager("PatVeloRHitManagerKill", CCEscan=CCEScanData)
    Tf__PatVeloPhiHitManager("PatVeloPhiHitManagerKill", CCEscan=CCEScanData)

    TrackBestTrackCreator().addTool(TrackStateInitTool(), name="TrackStateInitTool")
    TrackBestTrackCreator().TracksInContainers = [ 'Rec/Track/Forward', 'Rec/Track/Seed', 'Rec/Track/Match', 'Rec/Track/Downstream', 'Rec/Track/VeloTT', 'Rec/Track/VeloExcl' ]
    TrackBestTrackCreator().TrackStateInitTool.VeloFitterName = "Tf::PatVeloFitLHCbIDs/FitVelo"

    # Configure CCE ntuple maker
    from Configurables import Velo__VeloCCEScanMonitor, VeloClusterPosition

    moni = GaudiSequencer( 'Moni_CCEscan' )
    moni.Members = [ ]

    for sen in Sensors:
      vccR = Velo__VeloCCEScanMonitor( 'VeloCCEScanMonR%i'%(sen)
                                       ,TracksInContainer = 'Rec/Track/Best'
                                       ,TestSensor = sen
                                       ,CCEscan = CCEScanData
                                     )
      vccR.addTool(VeloClusterPosition, name="VeloClusterPosition")
      vccR.VeloClusterPosition.OutputLevel = 3
      moni.Members += [vccR]

      vccPhi = Velo__VeloCCEScanMonitor( 'VeloCCEScanMonPhi%i'%(sen+64)
                                         ,TracksInContainer = 'Rec/Track/Best'
                                         ,TestSensor = sen+64
                                         ,CCEscan = CCEScanData
                                       )
      vccPhi.addTool(VeloClusterPosition, name="VeloClusterPosition")
      vccPhi.VeloClusterPosition.OutputLevel = 3
      moni.Members += [vccPhi]

    GaudiSequencer( 'MoniVELOSeq' ).Members = [ moni ]

appendPostConfigAction(doMyChanges)
