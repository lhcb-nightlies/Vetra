from Configurables import Vetra

Vetra().FilterBeamBeam = True

from Configurables import CondDB

CondDB().IgnoreHeartBeat = True
CondDB().Online = True
CondDB().EnableRunStampCheck = False
