"""
##############################################################################
#                                                                            #
#  Monitoring of the bit perfectness confirmation phase                      #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Tomasz Szumlak  ( t.szumlak@physics.gla.ac.uk )                  #
#  @date    28/04/2009                                                       #
#                                                                            #
##############################################################################
"""

from Configurables           import GaudiSequencer
from Configurables           import VeloClusterCompare


moni_BPC = GaudiSequencer( 'Moni_BPC' )

vcc = VeloClusterCompare()
moni_BPC.Members += [ vcc ]

GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_BPC ]
