"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of zero-suppressed data.             #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author Shanzhen Chen                                                     #
#  @date   13/07/2015                                                        #
#                                                                            #
#  @author Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @date   03/10/2008                                                        #
#                                                                            #
##############################################################################
"""


from Configurables import Vetra
from GaudiKernel.ProcessJobOptions import importOptions

from Configurables import GaudiSequencer
from Configurables import EventClockSvc

from Configurables import Velo__VeloClusterMonitor
from Configurables import Velo__VeloExpertClusterMonitor as EXCMMon
from Configurables import Velo__VeloOccupancyMonitor
from Configurables import Velo__VeloTrackMonitor
from Configurables import Velo__ProcStatusMonitor
from Configurables import TrackVertexMonitor
from Configurables import Velo__Monitoring__ADCMon      as ADCMon

moni_ZS = GaudiSequencer( 'Moni_ZS' )

ADCMon     = ADCMon('ADCMon')
cluster_mon = Velo__VeloClusterMonitor("VeloClusterMonitor")
cluster_mon.HighMultiplicityPlot = True
cluster_mon.ADCFitParamters = True
cluster_mon.PerSensorPlots = True
cluster_mon.VeloOfflineDQPlots = True

if Vetra().getProp('TEDTracks') == False:
	moni_ZS.Members += [  cluster_mon
	                    , EXCMMon(                    'VeloExpertClusterMonitor' )
	                    , Velo__VeloOccupancyMonitor( 'VeloOccupancyMonitor'     ) 
	                    , Velo__VeloTrackMonitor    ( 'VeloTrackMonitor'         )
	                    , TrackVertexMonitor(         'TrackVertexMonitor'       )
	                    , ADCMon
	                    , Velo__ProcStatusMonitor   ( 'ProcStatusMonitor'        )
	                       ]
else:
	moni_ZS.Members += [  cluster_mon
	                    , EXCMMon(                    'VeloExpertClusterMonitor' )
	                    , Velo__VeloOccupancyMonitor( 'VeloOccupancyMonitor'     ) 
	                    , Velo__VeloTrackMonitor    ( 'VeloTrackMonitor'         )
	                    , ADCMon
	                    , Velo__ProcStatusMonitor   ( 'ProcStatusMonitor'        )
	                       ]

#Uncomment to specify TELL1s:

#ADCMon.TELL1s = [92]

#Uncomment to plot all links:

#ADCMon.LinkPlot = True

#Uncomment to specify Links:

#ADCMon.Links = [1,17,50,32]

#Uncomment to select only some containers for monitoring:

ADCMon.Containers = [  'Raw/Velo/DecodedADC'
                      ,'Raw/Velo/SubtractedPedADCs'
                       ]

GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_ZS ]

##############################################################################
