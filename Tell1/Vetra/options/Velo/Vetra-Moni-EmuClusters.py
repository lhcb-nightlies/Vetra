"""
###############################################################################
#                                                                             #
#  Example file to run Vetra on NZS data, create emulated clusters,   #
#  and the run the ZS monitoring on thses emulated clusters.                  #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-Default-Moni-EmuClusters.py                            #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  James Michael Keaveney  ( james.keaveney@ucdconnect.ie )          #
#  @date    15/09/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration import EventSelector
from GaudiKernel.Configurable import appendPostConfigAction

from Configurables import LHCbApp
from Configurables import Vetra
from Configurables import GaudiSequencer
from Configurables import Velo__VeloOccupancyMonitor
from Configurables import Velo__VeloClusterMonitor


vetra = Vetra()
vetra.DataBanks      = 'NZS+ZS'
vetra.RunEmulation   = True


#Need EvtMax greater than convergence limit (default = 4096) to produce emulated clusters
vetra.EvtMax = 5000

EventSelector().PrintFreq = 100

# =================
# Emulated ZS Banks
# =================
# change some default options of the DecodeVeloRawBuffer algo. in the Decoding_ZS sequence
# so that it looks at the result of the emulation
decoding_MoniEmuClu = GaudiSequencer( 'Decoding_MoniEmuClu' )
decodeEmu = DefaultDecoderToVeloClusters( 'DecodeEmu' )
decodeEmu.RawEventLocation="Emu/RawEvent"
decodeEmu.VeloClusterLocation = 'Emu/Velo/Clusters'
decoding_MoniEmuClu.Members += [ decodeEmu ]

# ===============================
# Monitoring on emulated ZS Banks
# ===============================
#options for monitoring algorithms to read emulated data
# change some default options in the VeloOccupancyMonitor in the Moni_ZS sequence
Velo__VeloClusterMonitor( 'VeloClusterMonitor' ).VeloClusterLocation = 'Emu/Velo/Clusters'
Velo__VeloOccupancyMonitor( 'VeloOccupancyMonitor' ).VeloClusterLocation = 'Emu/Velo/Clusters'
Velo__VeloOccupancyMonitor( 'VeloOccupancyMonitor' ).WriteXML = True
Velo__VeloOccupancyMonitor( 'VeloOccupancyMonitor' ).XMLDirectory = './xml'
Velo__VeloOccupancyMonitor( 'VeloOccupancyMonitor' ).HighOccCut = 1


def ConfigForEmu():
    # remove the NZS monitoring as not relevant for this study
    GaudiSequencer('MoniVELOSeq').Members.remove( GaudiSequencer( 'Moni_NZS' ) )
    #replace standard ZS decoding with decoding for emulated data.
    GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members.remove( GaudiSequencer('Decoding_ZS')  )
    GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members.append( decoding_MoniEmuClu )

appendPostConfigAction( ConfigForEmu )
