"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring for VeloExpertClusterMon (EXCM).     #
#  only in ofline                                                            #
#  @package Tell1/Vetra                                                      #
#  @author James Mylroie-Smith                                               #
#  @date    23/11/2008                                                       #
#                                                                            #
##############################################################################
"""

from Configurables import GaudiSequencer
from Configurables import Velo__VeloExpertClusterMonitor as EXCMMon

moni_EXCM = GaudiSequencer( 'Moni_EXCM' )

moni_EXCM.Members = [ EXCMMon('VeloExpertClusterMonitor') ]

GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_EXCM ]

##############################################################################
