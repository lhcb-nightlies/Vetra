"""
###############################################################################
#
# Example default file to run Vetra with only the occupancy monitoring in place
#
# @package Tell1/Vetra
# @author  Manuel Schiller <manuel.schiller@nikhef.nl
# @date    14/12/2011
#
###############################################################################
"""

from Configurables import Vetra, Velo__VeloOccupancyMonitor
from Gaudi.Configuration import EventSelector

########################################################################
# user-settable options
########################################################################
# runOnEmuZS - use emulated ZS data from NZS data for the occupancy
#              monitoring
runOnEmuZS = False
# ignoreMasks - optionally ignore channel masks in Tell1 emulator code
ignoreMasks = False

########################################################################
# set up Vetra the way you like it
########################################################################
EventSelector().PrintFreq = 100
#Vetra().EvtMax = 100

########################################################################
# occupancy monitoring setup
########################################################################
# below which occupancy do we consider a channel dead (in percent)
Velo__VeloOccupancyMonitor('VeloOccupancyMonitor').LowOccCut = 0.0
# above which occupancy do we consider a channel noisy (in percent)
Velo__VeloOccupancyMonitor('VeloOccupancyMonitor').HighOccCut = 5.0
# the auto-reset feature of the algorithm only makes sense in online
# monitoring, so disable it
Velo__VeloOccupancyMonitor('VeloOccupancyMonitor').OccupancyResetFrequency = 0
# write XML files with channel masks
Velo__VeloOccupancyMonitor('VeloOccupancyMonitor').WriteXML = True
# do not put XML files into separate directory (makes life easier on the grid)
Velo__VeloOccupancyMonitor('VeloOccupancyMonitor').XMLDirectoryDate = False


########################################################################
# end of user-servicable parts ;)
########################################################################
from GaudiKernel.ProcessJobOptions import importOptions
from Gaudi.Configuration import appendPostConfigAction
from Configurables import DecodeVeloRawBuffer, VeloTELL1PedestalSubtractor
from Configurables import GaudiSequencer, VetraTELL1ProcessingConf

# post config action to make the cluster decoding aware of the Tell1
# emulator output
def decodeZSFromEmuNZS():
    DecodeVeloRawBuffer('DecodeToVeloClusters').RawEventLocation = 'Emu/RawEvent'
# are we supposed to use emulated ZS data instead of real ZS data?
if runOnEmuZS:
    Vetra().DataBanks = 'NZS+ZS'
    Vetra().RunEmulation = True
    # decode NZS first, then generate emulated ZS
    GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members = [
	    GaudiSequencer('Decoding_NZS'), GaudiSequencer('TELL1Emulation') ]
    appendPostConfigAction(decodeZSFromEmuNZS)
    # optionally switch of masks on emulated Tell1
    if ignoreMasks:
        VeloTELL1PedestalSubtractor('VeloTELL1PedestalSubtractor').IgnoreMasks = True
else:
    Vetra().DataBanks = 'ZS'

# disable tracking - we do not need it for Occupancy monitoring
Vetra().MainSequence = [ 'GaudiSequencer/Init', 'ProcessPhase/TELL1Processing',
	'ProcessPhase/Moni', 'ProcessPhase/Output' ]

# post config action which disables all monitoring except the occupancy
# monitoring algorithm
def onlyOccupancyMonitoring():
    if GaudiSequencer('Moni_NZS') in GaudiSequencer('MoniVELOSeq').Members:
        # no NZS monitoring
        GaudiSequencer('MoniVELOSeq').Members.remove(GaudiSequencer('Moni_NZS'))
    # only run occupancy monitoring for ZS data
    GaudiSequencer('Moni_ZS').Members = [
	    Velo__VeloOccupancyMonitor('VeloOccupancyMonitor') ]
appendPostConfigAction(onlyOccupancyMonitoring)

###############################################################################
