"""
###############################################################################
#                                                                             #
#  Example default file to analyse generated data                             #
#  with the VeloClusterCompare algorithm                                      #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-Default-GenData.py                                     #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Karol Hennessy  ( karol.hennessy@cern.ch )                        #
#  @date    15/04/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration import EventSelector

from Configurables import GaudiSequencer

from Configurables import Vetra

from Configurables import DecodeVeloFullRawBuffer, PrepareVeloFullRawBuffer
from Configurables import VeloTELL1Reordering
from Configurables import VeloTELL1EmulatorInit
from Configurables import VeloTELL1PedestalSubtractor
from Configurables import VeloTELL1FIRFilter
from Configurables import VeloTELL1MCMS
from Configurables import VeloTELL1Reordering
from Configurables import VeloTELL1LCMS
from Configurables import VeloTELL1ClusterMaker
from Configurables import VeloClusterCompare

from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters
from GaudiKernel.ProcessJobOptions import importOptions

import os, sys

if not os.access('$VETRAROOT/VetraCondDB/Velo/VELOCOND.db', os.F_OK) :
        print "ERROR: No Conditions Database file!"
        print "Create using python create_base_line_db.py 'sensor' in the $VETRAROOT/python directory"
        sys.exit(-1)


importOptions( "$VETRAOPTS/Velo/VeloCondDB.py" )

Vetra().RunEmulation = True

EventSelector().PrintFreq = 100

# no ODIN bank in generated data usually
PrepareVeloFullRawBuffer().RunWithODIN = False

# by default we use DYNAMIC mode when validate the data
VeloTELL1EmulatorInit().ConvergenceLimit = 0
VeloTELL1PedestalSubtractor().DBConfig = 1
VeloTELL1FIRFilter().DBConfig          = 1
VeloTELL1MCMS().DBConfig               = 1
VeloTELL1Reordering().DBConfig         = 1
VeloTELL1LCMS().DBConfig               = 1
VeloTELL1ClusterMaker().DBConfig       = 1

# decode both the TELL1 and Emulated clusters to Full Velo Clusters
DecodeTELL1Clusters = DefaultDecoderToVeloClusters( 'DecodeToVeloClusters' )
DecodeEmuClusters   = DefaultDecoderToVeloClusters( 'DecodeEmuClusters'    )

DecodeEmuClusters.RawEventLocation    = "Emu/RawEvent"
DecodeEmuClusters.VeloClusterLocation = "Emu/Velo/Clusters"

# special decoding sequence
decoding_GenData = GaudiSequencer( 'Decoding_GenData' )
decoding_GenData.Members.extend( [ DecodeTELL1Clusters, DecodeEmuClusters ] )
GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ decoding_GenData ]

# monitoring sequence. Just one monitoring alg - VeloClusterCompare
moni_GenData = GaudiSequencer( 'Moni_GenData' )
moni_GenData.Members = [ VeloClusterCompare() ]
GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_GenData ]
#GaudiSequencer( 'MoniVELOSeq' ).Members.remove( GaudiSequencer( 'Moni_NZS' ) )
#GaudiSequencer( 'MoniVELOSeq' ).Members.remove( GaudiSequencer( 'Moni_ZS' ) )

###############################################################################
