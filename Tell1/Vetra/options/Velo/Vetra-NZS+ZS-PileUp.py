"""
###############################################################################
#                                                                             #
#  Example default file to run Vetra on NZS+ZS data with emulation            #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-NZS+ZS_PileUp.py                                       #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Serena Oggero                                                     #
#  @date    22/10/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration import EventSelector

from Configurables import EventClockSvc
from Configurables import LHCbApp, GaudiSequencer
from Configurables import Vetra


vetra = Vetra()

vetra.RunEmulation = True
vetra.PileUp       = True 

EventSelector().PrintFreq = 1

EventSelector().Input = [
"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/COLLISION09/63814/063814_0000000001.raw' SVC='LHCb::MDFSelector'"
    ]

###############################################################################
