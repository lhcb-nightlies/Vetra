"""
###############################################################################
#                                                                             #
#  Example default file to run Bad Channel monitoring on NZS data             #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-HVOff-BADSTIPS_Data-Emul.py                             #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  James Keaveney
#  @author  Wenchao Zhang
#  @author  Marius Bjoernstad
#  @date    18/08/2011                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration import EventSelector
from Configurables import Vetra, GaudiSequencer
from Configurables import VeloPedestalSubtractorMoni  as PedSubMon
from GaudiKernel.Configurable import appendPostConfigAction
from Configurables import Velo__Monitoring__NoiseMon as NoiseMon
from Configurables import Velo__Monitoring__BadChannelMon as BadChannelMon
from Configurables import VeloBeetleHeaderXTalkCorrectionMoni as BHXTMon

from Configurables import LoKi__ODINFilter 
from LoKiHlt.functions import ODIN_CALSTEP 




vetra = Vetra()

vetra.DataBanks    = 'NZS'
vetra.RunEmulation = True
vetra.EvtMax = 1000 
vetra.EmuConvLimit = 0
vetra.EmuMode = "Dynamic"
EventSelector().PrintFreq = 100
BadChannelMon('BadChannelMon').OutputLevel = 5
BadChannelMon('BadChannelMon').CheckFrequency = 9000

def changeSeq():
        PedSubAlg =  PedSubMon()
        NoiseAlg = NoiseMon('NoiseMon')
	BHXTAlg = BHXTMon()
	BadStripsFilter=LoKi__ODINFilter("FilterHVOff")
	BadStripsFilter.Code = "ODIN_CALSTEP == 1" # HV Off
	GaudiSequencer('VetraSequencer').Members.insert(1, BadStripsFilter)
        BadChannelAlg = BadChannelMon('BadChannelMon')
	GaudiSequencer('Moni_NZS').Members.remove(PedSubAlg)
        GaudiSequencer('Moni_NZS').Members.remove(NoiseAlg)
        GaudiSequencer('Moni_NZS').Members.remove(BHXTAlg)
	GaudiSequencer('Moni_NZS').Members.insert(1,BadChannelAlg)
	

appendPostConfigAction( changeSeq )
        
EventSelector().Input = [
#HVON
#    "DATAFILE='rfio:/castorfs/cern.ch/grid/lhcb/data/2011/RAW/FULL/VELO/NZS/90672/090672_0000000001.raw' svc='LHCb::MDFSelector'"
   
#HVOFF
#"DATAFILE='rfio:/castorfs/cern.ch/grid/lhcb/data/2011/RAW/FULL/VELO/NZS/90669/090669_0000000001.raw' svc='LHCb::MDFSelector'"

#HVON
#"DATAFILE='rfio:/castorfs/cern.ch/grid/lhcb/data/2011/RAW/FULL/VELO/NZS/89074/089074_0000000001.raw' svc='LHCb::MDFSelector'"

#HVOFF
"DATAFILE='rfio:/castorfs/cern.ch/grid/lhcb/data/2011/RAW/FULL/VELO/NZS/89077/089077_0000000001.raw' svc='LHCb::MDFSelector'"
    ]


###############################################################################
