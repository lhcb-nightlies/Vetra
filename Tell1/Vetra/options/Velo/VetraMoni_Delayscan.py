"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of ADC delay scan data.              #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Karol Hennessy  ( karol.hennessy@cern.ch )                       #
#  @date    30/03/2009                                                       #
#                                                                            #
##############################################################################
"""

from GaudiKernel.ProcessJobOptions import importOptions
from Gaudi.Configuration           import EventSelector
from Configurables                 import GaudiSequencer
from Configurables import dataTranslator

from Configurables import Velo__Monitoring__ADC_DigitisationDelayScan as PhaseScan

Moni_Delayscan = GaudiSequencer( 'Moni_Delayscan' )

phasescan = PhaseScan( 'PhaseScan' )

Moni_Delayscan.Members = [ dataTranslator(), phasescan ]
                      

phasescan.BurstSize             = 100
phasescan.NBursts               = 16
phasescan.BurstId               = 0
phasescan.PhaseChangeMethod     = 3
#phasescan.Plot2d                = 0
phasescan.PlotCMS               = 1
#phasescan.NEventsToSkip         = 900
phasescan.CommonModeCut         = 20
phasescan.PlotRawProfile        = 0
phasescan.PlotProfile           = 1
phasescan.PlotMeanAndRMS        = False
phasescan.InvertedDelaySequence = False
#phasescan.OutputLevel           = 3
#phasescan.TELL1s                = [ 0, 1 ]

EventSelector().FirstEvent = 101

GaudiSequencer( 'MoniVELOSeq' ).Members += [ Moni_Delayscan ]

# Remove the NZS monitoring sequence
GaudiSequencer( 'MoniVELOSeq' ).Members.remove( GaudiSequencer( 'Moni_NZS' ) )


##############################################################################
