"""
###############################################################################
#                                                                             #
#  Example file to run Vetra on TED data taken in November 2014               #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-TED_Data-2014-11.py                                    #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Kurt Rinnert (kurt.rinnert@cern.ch)                               #
#  @date    21/11/2014                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration import EventSelector
from Configurables import GaudiSequencer
from Configurables import ProcessPhase
from Configurables       import UpdateManagerSvc
from Gaudi.Configuration import appendPostConfigAction


from Configurables import Vetra

from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters
from GaudiKernel.ProcessJobOptions import importOptions
from Gaudi.Configuration import EventSelector

vetra = Vetra()

vetra.DataBanks = 'NZS+ZS'
vetra.RunEmulation = True
vetra.CheckTAEs = False
vetra.TEDTracks = True
vetra.MagnetOn = False

EventSelector().PrintFreq = 1

# Special TED data settings for the DecodeVeloRawBuffer algorithm (TAE)
tae = 2

decoding_TAE = GaudiSequencer( 'Decoding_TAE' )

tae_tags = ['Prev','Next']
tae_range = range(1,tae+1)
decoders = []

for tag in tae_tags:
	for slot in tae_range:
		decoders.append(DefaultDecoderToVeloClusters( 'Decode' + tag + str(slot) ))
		decoders[-1].DecodeToVeloClusters = True
		decoders[-1].DecodeToVeloLiteClusters = True
		decoders[-1].VeloClusterLocation = 'Raw/Velo/' + tag + str(slot) + '/Clusters'
		decoders[-1].VeloLiteClustersLocation = 'Raw/Velo/' + tag + str(slot) + '/LiteClusters'
		decoders[-1].RawEventLocations   = [ tag + str(slot) + '/DAQ/RawEvent']
		decoding_TAE.Members += [ decoders[-1] ]

GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ decoding_TAE ]

from Configurables import Velo__VeloClusterMonitor
from Configurables import Velo__VeloOccupancyMonitor
from Configurables import Velo__VeloSamplingMonitor

moni_TAE = GaudiSequencer( 'Moni_TAE' )

for i in range(1,tae+1) :
   moniPrev = [ Velo__VeloClusterMonitor( 'VCMPrev%1d' % i ),
                Velo__VeloOccupancyMonitor( 'VOCCPrev%1d' % i )
                ]
   Velo__VeloClusterMonitor  ( 'VCMPrev%1d' % i  ).VeloClusterLocation = 'Raw/Velo/Prev%1d/Clusters' % i
   Velo__VeloOccupancyMonitor( 'VOCCPrev%1d' % i ).VeloClusterLocation = 'Raw/Velo/Prev%1d/Clusters' % i
   
   moni_TAE.Members += moniPrev

for i in range(1,tae+1) :
   moniNext = [ Velo__VeloClusterMonitor( 'VCMNext%1d' % i ),
                Velo__VeloOccupancyMonitor( 'VOCCNext%1d' % i )
                ]
   Velo__VeloClusterMonitor  ( 'VCMNext%1d' % i  ).VeloClusterLocation = 'Raw/Velo/Next%1d/Clusters' % i
   Velo__VeloOccupancyMonitor( 'VOCCNext%1d' % i ).VeloClusterLocation = 'Raw/Velo/Next%1d/Clusters' % i
   
   moni_TAE.Members += moniNext


moni_TAE.Members += [ Velo__VeloSamplingMonitor( 'VeloSamplingMonitor' ) ]


GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_TAE ]



# configuration of special tracking sequence is delegated
importOptions("$VETRAOPTS/Velo/VetraPR.py")

# avoid data base errors and don't filter on beam crossings
from Configurables import CondDB
from Configurables import PrepareVeloFullRawBuffer

CondDB().IgnoreHeartBeat = True
Vetra().FilterBeamBeam = False 
PrepareVeloFullRawBuffer().IgnoreErrorBanks = True

# force VELO halfs to +/- 0.0 mm
UpdateManagerSvc().ConditionsOverride += [ 'Conditions/Online/Velo/MotionSystem := double ResolPosRC = 0.0; double ResolPosLA = 0.0; double ResolPosY = 0.0;' ]



