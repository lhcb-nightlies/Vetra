from Gaudi.Configuration import EventSelector

from Configurables import LHCbApp, GaudiSequencer

from Configurables import Vetra

from Configurables import Velo__Monitoring__ArxTest as ArxTest
from Configurables import PrepareVeloFullRawBuffer
from Configurables import DecodeVeloFullRawBuffer
from Configurables import VeloTELL1Reordering


vetra = Vetra()

vetra.DataBanks     = 'NZS'
vetra.RunEmulation = False
##vetra.MainSequence = ['ProcessPhase/TELL1Processing', 'ProcessPhase/AnalogMonitor']
vetra.HistogramFile = "arxtest-tell028_arx102-20100208-1608.root"

EventSelector().Input = [
    "DATAFILE='file:///localdisk/scratch/lab8data/arxtest-tell028_arx102-20100208-1608.dat' SVC='LHCb::MDFSelector'"
    ]

EventSelector().PrintFreq = 100

# no ODIN bank in generated data usually
PrepareVeloFullRawBuffer().RunWithODIN = False

# ordering corrections 
DecodeVeloFullRawBuffer().SectorCorrection = True
DecodeVeloFullRawBuffer().CableOrder       = [ 3, 2, 1, 0 ]
VeloTELL1Reordering().SectorCorrection = True

moni_AnalogMon = GaudiSequencer( 'Moni_AnalogMon' )
tell1 = 409 
arxtest = ArxTest("ArxTestTELL1"+str(tell1))
moni_AnalogMon.Members = [ arxtest ]

GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_AnalogMon ]
#arxtest.OutputLevel = 2
arxtest.ChosenTell1Number = 1
arxtest.PlotHistos = True
arxtest.PlotArxTest = True
arxtest.ThresholdActiveLink = 8000
arxtest.ThresholdNoisyLink = 16000

# list of links to be excluded from the analysis
# (DO NOT forget to comment this line for next card!!!)
# arxtest.ListMaskedLinks ={10}
# plot ADC counts for a given link for the first 10 events for which
# it is active
arxtest.PlotHistosDebug = False
arxtest.NumberDebugLink = 8
