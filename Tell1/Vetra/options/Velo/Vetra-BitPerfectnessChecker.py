"""
###############################################################################
#                                                                             #
#  Main options file to check bit perfectness of the Tell1 emulation          #
#                                                                             #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Tomasz Szumlak  ( t.szumlak@physics.gla.ac.uk )                   #
#  @date    25/04/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration     import EventSelector

from Configurables import Vetra
from Configurables import PrepareVeloFullRawBuffer

from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters

vetra = Vetra()

vetra.DataBanks      = 'NZS'
vetra.RunEmulation   = True
vetra.BitPerfectness = True
vetra.OutputType     = 'DIGI'
vetra.HistogramFile  = 'GenData.root'

# --> generated data are taken wihtout the ODIN bank
PrepareVeloFullRawBuffer().RunWithODIN = False

EventSelector().PrintFreq = 100

EventSelector().Input = [
    "DATAFILE='rfio:/castor/cern.ch/user/s/szumlat/clusterGeneration_R_38_20081116_firmware_v3.1.raw' SVC='LHCb::MDFSelector'"
    ]

###############################################################################
