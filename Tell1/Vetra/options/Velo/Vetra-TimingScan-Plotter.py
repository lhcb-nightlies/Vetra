"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of non-zero-suppressed data.         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
##############################################################################
"""

from Gaudi.Configuration     import EventSelector, MessageSvc
from GaudiKernel.Configurable import appendPostConfigAction

msgsvc = MessageSvc()
msgsvc.OutputLevel = 3
from Configurables import LHCbApp
from Configurables import Vetra
vetra = Vetra()
vetra.DataBanks      = 'NONE'
vetra.RunEmulation   = False
#Will include my  own version of the emulator.
#vetra.EmuMode        = 'Dynamic'
vetra.EmuMode        = 'Static'
vetra.BitPerfectness = False
vetra.OutputType     = 'NONE'
vetra.FilterBeamBeam = False
# --> generated data are taken wihtout the ODIN bank
# set the appropriate DB tags
#LHCbApp().DDDBtag   = 'head-20080905'
#LHCbApp().CondDBtag = 'head-20080905'
#LHCbApp().CondDBtag = ''
from GaudiKernel.ProcessJobOptions import importOptions
importOptions("$VETRAROOT/options/Velo/TAEDecodingNZS-PulseShapeScan.py")
from Configurables import CondDB
CondDB().IgnoreHeartBeat = True #Only use this for running on current data

#vetra.DataType  =   '2010'
#vetra.DataBanks  =   'NZS+ZS'
#vetra.Context  =   'Offline'
#vetra.VELOCONDtag  =   ''
#vetra.DDDBtag  =   ''
#vetra.CondDBtag  =   ''
#vetra.UseOracle  =   True
#vetra.Simulation  =   False
vetra.RunEmulation  =   False
vetra.EmuMode  =   'Dynamic'
vetra.EmuConvLimit  =   0
vetra.MoniDataFilter  =   'NONE'
vetra.CheckEBs  =   True
vetra.MagnetOn  =   True
vetra.TEDTracks  =   False
vetra.ParaTraining  =   False
vetra.BitPerfectness  =   False

from GaudiConf import IOHelper
IOHelper( Input = "MDF", Output = "MDF" )

from Configurables import GaudiSequencer
from Configurables import Velo__PulseShapePlotter as PulseShape

moni_pulseshape = GaudiSequencer( 'Moni_PulseShape' )
pulseShape = PulseShape( 'PulseShape' )
moni_pulseshape.Members = [pulseShape]
#EventSelector().FirstEvent = 0
EventSelector().PrintFreq =2000
vetra.EvtMax = -1
#vetra.SkipEvents = 5000 #commented out for collision!
#vetra.SkipEvents = 100 #for fakebeamscan data

#pulseShape.SamplingLocations = ["Prev3", "Prev2", "Prev1","","Next1","Next2","Next3"]
#pulseShape.SamplingLocations = [ ""]
pulseShape.SamplingLocations = [ "Prev2", "Prev1","","Next1","Next2"]
pulseShape.OutputLevel = 4 #Level 1 = output all; level 4 = No output
pulseShape.DataLocation = "Raw/Velo/PreparedADCs"
#pulseShape.DataLocation = "Raw/Velo/DecodedADC"
pulseShape.CMSCut = 12#25
#pulseShape.ProgressiveCMS = False 
pulseShape.SignalThreshold = 20#11
pulseShape.NumberOfCMSIterations = 3
pulseShape.IntegralCut = 35#38
#pulseShape.RunWithChosenBunchCounters= True
#pulseShape.ChosenBunchCounters = [1886, 995]
#pulseShape.RunNumbers = [63462, 63468, 63470,63472,63475] 
#pulseShape.RunNumbersDelay = [0,  0, 12.5, 19, 6.5]


####### Configurables #########
pulseShape.SelectBunchCrossing = True # Set to True if only beam-beam crossings should be selected, with prev1, prev2 and next1 non-bb
pulseShape.SkipStepZero = True # Usually set to True
pulseShape.NEventsPerStep = -1 # to run over data quickly: set to 100 or 10
#pulseShape.TELL1s=[19] # Select only one sensor
pulseShape.RemoveHeaders = False #to remove channels %32 which could contain header info
pulseShape.RejectHighADCOuterSamples = True # to select only the middle pulseshape for 50-ns bunch spacing
pulseShape.MaxADCOuterSamples = 20 #max ADC value of Prev2 and Next2 to select only the middle pulseshape
##############################



def mystuff():
##   from Configurables import Velo__OdinFilter as OdinFilter
##   odinfilter = OdinFilter() 
##   odinfilter.ChosenBunchCounters = [995, 1886]
##   odinfilter.RunWithChosenBunchCounters = True
##   odinfilter.SkipStepZero = True
##   odinfilter.SkipSteps = [0,1]  # in case of skipping other steps as well
##   odinfilter.NEventsPerStep = -1
##   odinfilter.OutputLevel = 4
  GaudiSequencer( 'MoniVELOSeq' ).Members = [moni_pulseshape]
##   GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members.insert(0,odinfilter)

appendPostConfigAction( mystuff )

#importOptions("$VETRAROOT/options/Velo/filesTimingScan2011.py")
#importOptions("$VETRAROOT/options/Velo/filesTimingScanTest.py")

a = []

##############################################################################
##
##TIMINGSCAN september 2011, where the stepper didn't step..
#for i in range(1,292):
#   a.append("DATAFILE='file:///daqarea/lhcb/data/2011/RAW/FULL/LHCb/CALIBRATION11/101371/101371_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")
#   a.append("DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/data/2011/RAW/FULL/LHCb/CALIBRATION11/101371/101371_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")

##FAKEBEAMSCAN 7 march 2012:
#for i in range(1,2):
#  a.append("DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2012/RAW/FULL/VELO/FAKEBEAMSCAN/108582/108582_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'" )

## global TIMINGSCAN 27 march 2012:(500 ev per step)
#for i in range(1,8):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110296/110296_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")
#for i in range(1,8):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110309/110309_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")
#  a.append("DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110309/110309_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'" )

## Test global TIMINGSCAN for TAE events March 28 2012: (100 ev per step), error banks on (only step0 appears in data)
#for i in range(1,5):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110490/110490_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")

## Test global TIMINGSCAN to test TAE with passthrough trigger March 28 2012:(100 ev per step), Error banks off (Has all steps, but no ADC values cause velo Voltage is off)
#for i in range(1,5):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110499/110499_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")
  
## Passthrough Markus, Error banks off,(has all steps, but no ADC values cause velo Voltage is off)
#for i in range(1,5):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110501/110501_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")

## TIMINGSCAN without beam, but with LV on, 2 April 2012, error banks off, 5000 ev per step - error: Data generator mode so all ADCs are zero
#for i in range(1,67):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110888/110888_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")

## TIMINGSCAN without beam, but with LV on, 2 April 2012, error banks off, 500 ev per step
#for i in range(1,9):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110985/110985_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")
##
##############################################################################


## TIMINGSCAN with beam, but with LV on, 5April 2012, error banks off, 5000 ev per step
#for i in range(1,118):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/111176/111176_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")
#  a.append("DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/111176/111176_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")

## Comparison with 2011 run 87214, taken on 15 March 2011
#for i in range(1,278):
#  a.append("DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/data/2011/RAW/FULL/LHCb/CALIBRATION11/87214/087214_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")
        
EventSelector().Input = a

##############################################################################
vetra.HistogramFile  = 'TimingRun111176_plotter.root'
