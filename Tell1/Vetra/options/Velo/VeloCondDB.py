"""
##############################################################################
#                                                                            #
#  Options needed to connect to the VELOCOND data base, import when run      #
#  emulation in Dynamic mode                                                 #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Tomasz Szumlak     ( t.szumlak@physics.gla.ac.uk   )             #
#  @date    21/04/2009                                                       #
#                                                                            #
##############################################################################
"""

# =============================================================================
# Import necessary modules
# =============================================================================

from Gaudi.Configuration import *
from Configurables import CondDB, DDDBConf, CondDBAccessSvc
from Configurables import VeloTELL1PedestalSubtractor
from Configurables import VeloTELL1FIRFilter
from Configurables import VeloTELL1MCMS
from Configurables import VeloTELL1Reordering
from Configurables import VeloTELL1LCMS
from Configurables import VeloTELL1ClusterMaker

# --> created appropriate Layering service and connect to the data base
connection="sqlite_file:$VELOSQLITEDBPATH/VELOCOND.db/VELOCOND"
CondDB().addLayer(CondDBAccessSvc("VELOCOND",
                                  ConnectionString=connection))

# --> configure the tell1 algorithms to work in Dynamic mode
VeloTELL1PedestalSubtractor().DBConfig = 1
VeloTELL1FIRFilter().DBConfig = 1
VeloTELL1MCMS().DBConfig = 1
VeloTELL1Reordering().DBConfig = 1
VeloTELL1LCMS().DBConfig = 1
VeloTELL1ClusterMaker().DBConfig = 1
