"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of non-zero-suppressed data.         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
##############################################################################
"""

from GaudiKernel.ProcessJobOptions import importOptions

from Configurables import GaudiSequencer
from Configurables import Velo__Monitoring__NoiseMon as NoiseMon
from Configurables import Velo__Monitoring__FeedNoise as FeedNoise
from Configurables import Velo__Monitoring__BadChannelMon as BadChannelMon
from Configurables import VeloPedestalSubtractorMoni as PedestalMon
from Configurables import VeloBeetleHeaderXTalkCorrectionMoni as BHXTMon
from Configurables import Velo__VeloClusterMonitor
from Configurables import Velo__Monitoring__CMSMon as CMSMon
from Configurables import Velo__SEUMonitor as SEUMonitor
from Configurables import Vetra

moni_NZS = GaudiSequencer( 'Moni_NZS' )

moni_NZS.Members += [  PedestalMon()
                     ,BHXTMon() 
                     ,FeedNoise( 'FeedNoise' ) 
                     ,NoiseMon( 'NoiseMon' )
		     ,SEUMonitor('SEUMonitor')
                     ]


#PedestalMon().PlotSubADCs = False

FeedNoise( 'FeedNoise' ).Containers = [ 'Raw/Velo/DecodedADC'
                                   ,'Raw/Velo/ADCCMSuppressed' ]

NoiseMon('NoiseMon').Containers = [ 'Raw/Velo/DecodedADC'
                                   ,'Raw/Velo/ADCCMSuppressed' ]

FeedNoise( 'FeedNoise' ).PublishingFrequency = 100
NoiseMon('NoiseMon').PublishingFrequency     = 100

# expert stuff
if Vetra().getProp( 'ExpertMoni' ):
    EmuClusterMoni =  Velo__VeloClusterMonitor  ( 'EmuClusterMonitor' )
    EmuClusterMoni.VeloClusterLocation = "Emu/Velo/Clusters"
    CMSMon = CMSMon( 'CMSMon' )
    CMSMon.TELL1s = [0,1,11,12,64,65,76,75]
    moni_NZS.Members = [ EmuClusterMoni, CMSMon ]

GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_NZS ]

##############################################################################
