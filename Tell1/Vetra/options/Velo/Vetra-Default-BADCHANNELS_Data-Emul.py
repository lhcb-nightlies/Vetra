"""
###############################################################################
#                                                                             #
#  Example default file to run Vetra on NZS data with emulation               #
#  for bad channels analysis                                                  #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-Default-BADCHANNELS_Data-Emul.py                       #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @date    12/03/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration import EventSelector

from Configurables import Vetra


vetra = Vetra()

vetra.DataBanks    = 'NZS'
vetra.RunEmulation = True
vetra.EmuConvLimit = 4096

EventSelector().PrintFreq = 100

EventSelector().Input = [
 "DATAFILE='rfio:/castorfs/cern.ch/grid/lhcb/data/2011/RAW/FULL/VELO/NZS/86024/086024_0000000002.raw' SVC='LHCb::MDFSelector'"
]

###############################################################################
