"""
##############################################################################
#                                                                            #
# Options file to run the TELL1 emulation chain                              #
# At present the emulator consists of the following algorithms:              #
#   1. EmulatorInit - facilitate configuration of the emulator               #
#   2. Pedestal Subtraction/Following (bit perfect)                          #
#   3. Pedestal subtractor is based on the TELL1 library now                 #
#   4. Finite Impulse Respond digital filtering                              #
#   5. LCMS algorithm based on the TELL1 lib                                 #
#   6. Channel reorderer                                                     #
#   7. Cluster Maker (current implementation is based on TELL1 library)      #
# Computer algorithms can be added in any place since they are not           #
# suppose to change NZS data in any way                                      #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Tomasz Szumlak     ( t.szumlak@physics.gla.ac.uk   )             #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    13/10/2008                                                       #
#                                                                            #
##############################################################################
"""

# =============================================================================
# Import necessary modules
# =============================================================================
from GaudiKernel.ProcessJobOptions import importOptions

from Configurables import GaudiSequencer
from Configurables import VeloTELL1EmulatorInit
from Configurables import dataTranslator
from Configurables import VeloTELL1PedestalSubtractor
from Configurables import VeloTELL1FIRFilter
from Configurables import VeloTELL1MCMS
from Configurables import VeloTELL1Reordering
from Configurables import VeloTELL1LCMS
from Configurables import VeloTELL1ClusterMaker
from Configurables import nzsStreamListener

emulation = GaudiSequencer( 'TELL1Emulation' )

errDataTranslator=dataTranslator('ErrorStreamTranslator')
errDataTranslator.InputDataLoc='Raw/Velo/PartialADCs'
errDataTranslator.OutputDataLoc='Raw/Velo/PreparedPartialADCs'

emulation.Members += [   VeloTELL1EmulatorInit()
                      , dataTranslator()
                      , errDataTranslator
                      , VeloTELL1PedestalSubtractor()
                      , VeloTELL1FIRFilter()
                      , VeloTELL1MCMS()
                      , VeloTELL1Reordering()
                      , VeloTELL1LCMS()
                      , VeloTELL1ClusterMaker()
                        ]

# --> Pedestal Subtractor - use this if you want to exclude any sensor(s)
#VeloTELL1PedestalSubtractor().ExcludedTell1List = [ 21, 23 ]

# --> MCMS - this one is new - we need to force it to be operational
VeloTELL1MCMS().MCMSProcessEnable = 1
VeloTELL1MCMS().ForceEnable       = True

GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ emulation ]

##############################################################################
