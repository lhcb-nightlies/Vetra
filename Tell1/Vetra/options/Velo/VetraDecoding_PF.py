"""
##############################################################################
#                                                                            #
#  Vetra option file for the "TELL1Processing" phase of a Vetra job.         #
#  Defines the decoding of the "ProcFull" (PF) bank.                         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @author  Tomasz Szumlak     ( t.szumlak@physics.gla.ac.uk )               #
#  @date    17/11/2008                                                       #
#                                                                            #
##############################################################################
"""

from Configurables import GaudiSequencer

from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters

decodeToVeloClusters = DefaultDecoderToVeloClusters( 'DecodeToVeloClusters' )

decoding_PF = GaudiSequencer( 'Decoding_PF' )

decoding_PF.Members = [ decodeToVeloClusters ]

GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ decoding_PF ]

##############################################################################
