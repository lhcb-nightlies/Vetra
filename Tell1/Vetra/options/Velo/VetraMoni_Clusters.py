"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of zero-suppressed data.             #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @date   03/10/2008                                                        #
#                                                                            #
##############################################################################
"""


from Configurables import Vetra
from GaudiKernel.ProcessJobOptions import importOptions

from Configurables import GaudiSequencer
from Configurables import EventClockSvc

from Configurables import Velo__VeloClusterMonitor
from Configurables import Velo__VeloOccupancyMonitor

moni_Clusters = GaudiSequencer( 'Moni_Clusters' )

cluster_mon = Velo__VeloClusterMonitor("VeloPrivateClusterMonitor")
cluster_mon.HighMultiplicityPlot = True
cluster_mon.ADCFitParamters = True
cluster_mon.PerSensorPlots = True
cluster_mon.VeloOfflineDQPlots = True

occupancy_mon = Velo__VeloOccupancyMonitor("VeloPrivateOccupancyMonitor")

if Vetra().getProp('TEDTracks') == False:
        moni_Clusters.Members += [  cluster_mon
                                  , occupancy_mon
                               ]
else:
        moni_Clusters.Members += [  cluster_mon
                                  , occupancy_mon
                               ]

GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_Clusters ]

##############################################################################
