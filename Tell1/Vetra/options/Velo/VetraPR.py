"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of zero-suppressed data.             #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author Silvia Borghi                                                     #
#  @date   04/06/2009                                                        #
#                                                                            #
##############################################################################
"""

from GaudiKernel.ProcessJobOptions import importOptions

from Configurables import GaudiSequencer
from TrackSys.Configuration import *
from Configurables import ProcessPhase, GaudiSequencer
from Configurables import (Tf__PatVeloRTracking, Tf__PatVeloSpaceTool,
                           Tf__PatVeloSpaceTracking, Tf__PatVeloGeneralTracking,
                           Tf__PatVeloGeneric, Tf__PatVeloTrackTool,
                           DecodeVeloRawBuffer)

from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters
from Configurables import (RawBankToSTClusterAlg, RawBankToSTLiteClusterAlg,
                           UpdateManagerSvc )
from Configurables import (ToolSvc,Tf__DefaultVeloRHitManager,Tf__DefaultVeloPhiHitManager)
from Configurables import (MeasurementProviderT_MeasurementProviderTypes__VeloR_,MeasurementProviderT_MeasurementProviderTypes__VeloPhi_)
from Configurables import ( TrackSys, RecSysConf )
from Configurables import TrackContainerCopy
from Configurables import Velo__VeloTrackMonitor
from Configurables import EventClockSvc
from Configurables import Vetra

vetra = Vetra()

RecSysConf().RecoSequence = ["VELO","Tr"]
# to reconstruct also vertex, needed for TrackVertexMonitor
#RecSysConf().RecoSequence = ["VELO","Tr","Vertex"]
TrackSys().TrackPatRecAlgorithms=["Velo"]
TrackContainerCopy("CopyVelo").inputLocation="Rec/Track/PreparedVelo"

Velo__VeloTrackMonitor("VeloTrackMonitor").TrackLocation  = "Rec/Track/Best"

def doMyChanges():
    from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters
    decodeToVeloClusters = DefaultDecoderToVeloClusters( 'DecodeToVeloClusters' )
    decodeToVeloClusters.DecodeToVeloClusters=True
    decodeToVeloClusters.DecodeToVeloLiteClusters=True
    #Redefine the RecoVeloSeq, because the cluster decoding is defined in other file
    if vetra.getProp( 'TEDTracks' )  == False:
            GaudiSequencer("RecoVELOSeq").Members = [ Tf__PatVeloRTracking("PatVeloRTracking")
                                              ,Tf__PatVeloSpaceTracking("PatVeloSpaceTracking")
                                              ,Tf__PatVeloGeneralTracking("PatVeloGeneralTracking")
                                              ]    
        
    ###-----------------------------------------------------------------------------------------------
    #Special configuration for TED 2009 

    ###-----------------------------------------------------------------------------------------------
    #Field off, so straightline fit in the kalman with no correction
    if vetra.getProp( 'TEDTracks' ) == True :
            from TrackFitter.ConfiguredFitters import TrackEventFitter
            
            #No magnetic field and straightline fit and no Multiple scatt. and en. loss correction
            if vetra.MagnetOn == False :
                    TrackSys().SpecialData=["fieldOff"]
            eventfitter = TrackEventFitter('FitVelo')
            #eventfitter.Fitter.ZPositions=[]
            eventfitter.Fitter.ApplyMaterialCorrections = False
            eventfitter.Fitter.Extrapolator.ApplyMultScattCorr = False
            eventfitter.Fitter.Extrapolator.ApplyEnergyLossCorr = False
            eventfitter.Fitter.Extrapolator.ApplyElectronEnergyLossCorr = False
            #eventfitter.Fitter.Extrapolator.MaterialLocator='DetailedMaterialLocator'
            eventfitter.Fitter.StateAtBeamLine = False
            eventfitter.Fitter.MaxNumberOutliers = 0
            eventfitter.Fitter.NumberFitIterations = 3
            eventfitter.Fitter.UpdateTransport = True
            eventfitter.Fitter.MakeNodes=True
            eventfitter.OutputLevel = 3
            
            #Special PR Generic to find long tracks
            GaudiSequencer("RecoVELOSeq").Members = [ Tf__PatVeloGeneric("PatVeloGeneric")
                                                       #,Tf__PatVeloRTracking("PatVeloRTracking")
                                                       #,Tf__PatVeloSpaceTracking("PatVeloSpaceTracking")
                                                       #Tf__PatVeloGeneralTracking("PatVeloGeneralTracking")
                                                       ]
            Tf__PatVeloGeneric("PatVeloGeneric").FullAlignment        = True
            Tf__PatVeloGeneric("PatVeloGeneric").MinModules           = 5
            Tf__PatVeloGeneric("PatVeloGeneric").ClusterCut           = 3000
            Tf__PatVeloGeneric("PatVeloGeneric").KalmanState          = 3
            Tf__PatVeloGeneric("PatVeloGeneric").ForwardProp          = True
            Tf__PatVeloGeneric("PatVeloGeneric").CleanSeed            = True
            Tf__PatVeloGeneric("PatVeloGeneric").PrivateBest          = True
            Tf__PatVeloGeneric("PatVeloGeneric").ACDC                 = False
            Tf__PatVeloGeneric("PatVeloGeneric").ConsiderOverlaps     = False
            Tf__PatVeloGeneric("PatVeloGeneric").CheckReadOut         = True
            Tf__PatVeloGeneric("PatVeloGeneric").MaxSkip              = 2
            Tf__PatVeloGeneric("PatVeloGeneric").SigmaTol             = 5
            Tf__PatVeloGeneric("PatVeloGeneric").RAliTol              = 0.05
            Tf__PatVeloGeneric("PatVeloGeneric").PAliTol              = 0.005
    ###-----------------------------------------------------------------------------------------------

    ###-----------------------------------------------------------------------------------------------
    #Special option to use the cluster in no standard container
    ###-----------------------------------------------------------------------------------------------
    #timeForTracking='Prev1'
    ## ToolSvc().addTool( Tf__DefaultVeloRHitManager, name="DefaultVeloRHitManager")
    ## ToolSvc().DefaultVeloRHitManager.ClusterLocation =     'Raw/Velo/'+timeForTracking+'/Clusters'
    ## ToolSvc().DefaultVeloRHitManager.LiteClusterLocation = 'Raw/Velo/'+timeForTracking+'/LiteClusters'
    ## ToolSvc().addTool( Tf__DefaultVeloPhiHitManager, name="DefaultVeloPhiHitManager")
    ## ToolSvc().DefaultVeloPhiHitManager.ClusterLocation =   'Raw/Velo/'+timeForTracking+'/Clusters'
    ## ToolSvc().DefaultVeloPhiHitManager.LiteClusterLocation='Raw/Velo/'+timeForTracking+'/LiteClusters'


    ## ToolSvc().addTool( Tf__DefaultVeloRHitManager, name="DefaultVeloRHitManager")
    ## ToolSvc().DefaultVeloRHitManager.ClusterLocation = 'Raw/Velo/'+timeForTracking+'/Clusters'
    ## ToolSvc().DefaultVeloRHitManager.LiteClusterLocation = 'Raw/Velo/'+timeForTracking+'/LiteClusters'
    ## ToolSvc().addTool( Tf__DefaultVeloPhiHitManager, name="DefaultVeloPhiHitManager")
    ## ToolSvc().DefaultVeloPhiHitManager.ClusterLocation = 'Raw/Velo/'+timeForTracking+'/Clusters'
    ## ToolSvc().DefaultVeloPhiHitManager.LiteClusterLocation = 'Raw/Velo/'+timeForTracking+'/LiteClusters'

    ## Velo__VeloTrackMonitor('VeloTrackMonitor').ClusterLocation =  'Raw/Velo/'+timeForTracking+'/Clusters'
    ## # changing default cluster locations for VeloExpertClusterMonitor
    ## Velo__VeloExpertClusterMonitor('VeloExpertClusterMonitor').ClusterLocation =  'Raw/Velo/'+timeForTracking+'/Clusters'

    ## ToolSvc().addTool(MeasurementProviderT_MeasurementProviderTypes__VeloR_, name="VeloRMeasurementProvider")
    ## ToolSvc().addTool(MeasurementProviderT_MeasurementProviderTypes__VeloPhi_, name="VeloPhiMeasurementProvider")
    ## ToolSvc().VeloRMeasurementProvider.ClusterLocation = 'Raw/Velo/'+timeForTracking+'/Clusters'
    ## ToolSvc().VeloPhiMeasurementProvider.ClusterLocation = 'Raw/Velo/'+timeForTracking+'/Clusters'
    ###-----------------------------------------------------------------------------------------------
appendPostConfigAction(doMyChanges)
