"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of non-zero-suppressed data.         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
##############################################################################
"""

from Gaudi.Configuration     import EventSelector, MessageSvc
from GaudiKernel.Configurable import appendPostConfigAction
msgsvc = MessageSvc()
msgsvc.OutputLevel = 4
from Configurables import LHCbApp
from Configurables import Vetra
vetra = Vetra()
vetra.DataBanks      = 'NONE'
vetra.RunEmulation   = False
#Will include my  own version of the emulator.
#vetra.EmuMode        = 'Dynamic'
vetra.EmuMode        = 'Static'
vetra.BitPerfectness = False
vetra.OutputType     = 'NONE'
# --> generated data are taken wihtout the ODIN bank
# set the appropriate DB tags
#LHCbApp().DDDBtag   = 'head-20080905'
#LHCbApp().CondDBtag = 'head-20080905'

from GaudiKernel.ProcessJobOptions import importOptions
#importOptions("$VETRAROOT/options/Velo/TAEDecodingNZS.py")
importOptions("$VETRAROOT/options/Velo/TAEDecodingNZS-PulseShapeScan.py")
from Configurables import CondDB

from GaudiConf import IOHelper
IOHelper( Input = "MDF", Output = "MDF" )

#importOptions("$VETRAROOT/options/Velo/TAEEmulatorNZS.py")
#VeloTELL1Reordering().SectorCorrection = True
##############################################################################


###############################################################################

from Configurables import GaudiSequencer
from Configurables import Velo__PulseShapeFitterTedTree as PulseShape
moni_pulseshape = GaudiSequencer( 'Moni_PulseShape' )
pulseShape = PulseShape( 'PulseShape' )
moni_pulseshape.Members = [pulseShape]
EventSelector().FirstEvent = 0
EventSelector().PrintFreq = 1000
vetra.EvtMax = -1
#pulseShape.SamplingLocations = ["Prev3", "Prev2", "Prev1","","Next1","Next2","Next3"] 
pulseShape.SamplingLocations = [ "Prev2", "Prev1","","Next1","Next2"]
#pulseShape.DataLocation = "Raw/Velo/DecodedADC"
pulseShape.OutputLevel = 4
pulseShape.CMSCut = 12
pulseShape.ExpectedPeakTime = -3 
pulseShape.ChisqCut = 50 # 300
pulseShape.SignalThreshold = 20
pulseShape.MaxAmplitudeCut= 500 #2000
pulseShape.OffsetCut= 50
pulseShape.IntegralCut= 35# 30
pulseShape.TimeWindow= 50#10
pulseShape.FixOffset= True
pulseShape.TakeNeighbours=1
pulseShape.NTimeBins=500
pulseShape.UseLogLikeFit=False  
pulseShape.TryToClusterize = False 
pulseShape.PlotSeparateStrips= False#make histo's per channel
pulseShape.TELL1s=[19]#to select one tell1

#pulseShape.WidthBefore=17.6#quickfit4 value-avg
#pulseShape.WidthAfter=21.7#quickfit4 value-avg
pulseShape.WidthBefore=15.7#quickfit3 value-landau
pulseShape.WidthAfter=17.1#quickfit3 value-landau

pulseShape.DetailedOutput=False
#pulseShape.RunWithChosenBunchCounters=True
#pulseShape.ChosenBunchCounters = [1786]
##pulseShape.RunNumbersDelay = [ 0 ]

###########
pulseShape.SelectBunchCrossing = False
pulseShape.RemoveHeaders = True #to remove channels %32 which contain header info
pulseShape.SkipStepZero = False #to do the step0 data only, def = true
pulseShape.OnlyStepZero = False #def = false
pulseShape.NEventsPerStep = 1000 #to take less stats
pulseShape.RunNumbers = [ 101371 ]
#########

def mystuff():
  GaudiSequencer( 'MoniVELOSeq' ).Members = [moni_pulseshape]
appendPostConfigAction( mystuff )

#############################################
a = []
for i in range(1,292):
  a.append("DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/data/2011/RAW/FULL/LHCb/CALIBRATION11/101371/101371_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")

EventSelector().Input = a



