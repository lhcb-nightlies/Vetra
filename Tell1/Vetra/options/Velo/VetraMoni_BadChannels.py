"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of bad channels.                     #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
##############################################################################
"""

from GaudiKernel.ProcessJobOptions import importOptions

from Configurables import GaudiSequencer
from Configurables import Velo__Monitoring__FeedNoise as FeedNoise
from Configurables import Velo__Monitoring__BadChannelMon as BadChannelMon
from Configurables import Velo__VeloClusterMonitor
from Configurables import Velo__Monitoring__CMSMon as CMSMon
from Configurables import Vetra

moni_BADCHANNELS = GaudiSequencer( 'Moni_BADCHANNELS' )

moni_BADCHANNELS.Members += [
                     FeedNoise( 'FeedNoise' ) 
                     ,BadChannelMon('BadChannelMon')
                     ]



FeedNoise( 'FeedNoise' ).Containers = [ 'Raw/Velo/DecodedADC'
                                   ,'Raw/Velo/ADCCMSuppressed' ]

FeedNoise( 'FeedNoise' ).PublishingFrequency = 100
BadChannelMon('BadChannelMon').OutputLevel = 1
BadChannelMon('BadChannelMon').CheckFrequency = 1000



# expert stuff
if Vetra().getProp( 'ExpertMoni' ):
    EmuClusterMoni =  Velo__VeloClusterMonitor  ( 'EmuClusterMonitor' )
    EmuClusterMoni.VeloClusterLocation = "Emu/Velo/Clusters"
    CMSMon = CMSMon( 'CMSMon' )
    CMSMon.TELL1s = [0,1,11,12,64,65,76,75]
    moni_BADCHANNELS.Members = [ EmuClusterMoni, CMSMon ]

GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_BADCHANNELS ]

##############################################################################
