"""
###############################################################################
#                                                                             #
#  Example default file to run Vetra on NZS data with emulation               #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-Default-NZS_Data-Emul.py                               #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @date    12/03/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration import *

from Configurables import ( EventSelector, Vetra )

from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters

vetra = Vetra()

vetra.DataBanks    = 'NZS+ZS'
vetra.RunEmulation = True
vetra.EvtMax       = 500000

EventSelector().PrintFreq = 1000

EventSelector().Input = [

  #round robin sample - 08/11/2009
    "DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/PHYSICS/61120/061120_0000000001.raw' SVC='LHCb::MDFSelector'"
    ,"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/PHYSICS/61120/061120_0000000002.raw' SVC='LHCb::MDFSelector'"
    ,"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/PHYSICS/61120/061120_0000000003.raw' SVC='LHCb::MDFSelector'"
    ,"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/PHYSICS/61120/061120_0000000004.raw' SVC='LHCb::MDFSelector'"
    ,"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/PHYSICS/61120/061120_0000000005.raw' SVC='LHCb::MDFSelector'"
    ,"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/PHYSICS/61120/061120_0000000006.raw' SVC='LHCb::MDFSelector'"
    ,"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/PHYSICS/61120/061120_0000000007.raw' SVC='LHCb::MDFSelector'"
    ,"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/PHYSICS/61120/061120_0000000008.raw' SVC='LHCb::MDFSelector'"
    ,"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/PHYSICS/61120/061120_0000000009.raw' SVC='LHCb::MDFSelector'"
    ,"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/PHYSICS/61120/061120_0000000010.raw' SVC='LHCb::MDFSelector'"
    ,"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/PHYSICS/61120/061120_0000000011.raw' SVC='LHCb::MDFSelector'"
    ,"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/PHYSICS/61120/061120_0000000012.raw' SVC='LHCb::MDFSelector'"
    
    ]

###############################################################################
