"""
##############################################################################
#                                                                            #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#                                                                            #
##############################################################################
"""

from Gaudi.Configuration     import EventSelector
from Configurables import CondDB
from Configurables import Vetra
from Configurables import PrepareVeloFullRawBuffer
from Configurables import DecodeVeloFullRawBuffer
from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters
from GaudiKernel.Configurable import appendPostConfigAction

vetra = Vetra()

vetra.DataBanks      = 'NZS'
vetra.RunEmulation   = True
vetra.BitPerfectness = False
vetra.OutputType     = 'NONE'
CondDB().IgnoreHeartBeat = True
Vetra().FilterBeamBeam = False 
Vetra().VELOCONDtag = 'HEAD'

# --> generated data are taken wihtout the ODIN bank
PrepareVeloFullRawBuffer().RunWithODIN = False

DecodeVeloFullRawBuffer().SectorCorrection = True
DecodeVeloFullRawBuffer().CableOrder = [ 3, 2, 1, 0 ]

#VeloTELL1Reordering().SectorCorrection = True
##############################################################################


##############################################################################


###############################################################################

from Configurables import GaudiSequencer
from Configurables import Velo__Monitoring__ADCDelayScan  as PhaseScan
moni_phasescan = GaudiSequencer( 'Moni_PhaseScan' )
phaseScan = PhaseScan( 'PhaseScan' )
moni_phasescan.Members = [phaseScan]
phaseScan.BurstSize = 100
phaseScan.NBursts  = 16
phaseScan.BurstId = 0
EventSelector().FirstEvent = 101
EventSelector().PrintFreq = 100
phaseScan.PhaseChangeMethod=1
phaseScan.OutputLevel = 3
#phaseScan.TELL1s = [3]
phaseScan.Plot2D = 0
phaseScan.PlotCMS = 0
phaseScan.NEventsToSkip =0
phaseScan.CommonModeCut = 20
phaseScan.PlotRaw = 1
phaseScan.PlotProfile = 1
phaseScan.InvertedDelaySequence = False 

def mystuff():
  GaudiSequencer( 'MoniVELOSeq' ).Members = [ moni_phasescan ]
appendPostConfigAction( mystuff )

##############################################################################
EventSelector().Input = [
     "DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/VELO/ADCDELAYSCAN/108513/108513_0000000001.raw' SVC='LHCb::MDFSelector'"
]
###############################################################################
vetra.HistogramFile  = 'DelayScan108513.root'
