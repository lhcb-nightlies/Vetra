"""
###############################################################################
#                                                                             #
#  Example file to run monitoring on a Gain Scan                              #
#                                                                             #
#  Example usage:                                                             #
#  gaudirun.py Vetra-GainMon_GainScan.py inputData.py                         #
#                                                                             #
#   Produces plots for non-Test Pulse gain scan data which can be read by     #
#   $VETRASCRIPTS/macros/drawFHSPlots.C                                       #
#                                                                             #
#  @package Velo/VeloDataMonitor                                              #
#  @author  Grant McGregor  (Grant.McGregor@hep.manchester.ac.uk )            #
#  @date    24/08/2009                                                        #
#                                                                             #
###############################################################################
"""
from GaudiKernel.Configurable import appendPostConfigAction
from Gaudi.Configuration      import EventSelector

from Configurables import GaudiSequencer
from Configurables import Velo__Monitoring__GainMon  as GainMon
from Configurables import VeloPedestalSubtractorMoni as PedSubMon
from Configurables import Vetra


vetra = Vetra()

vetra.DataBanks      = 'NZS'
vetra.RunEmulation   = True
vetra.FilterBeamBeam = False
vetra.HistogramFile  = 'gainMon_GainScan_Histos.root'
vetra.EvtMax         = 43990

def removeAlgo():
    algo =  PedSubMon( )
    GaudiSequencer( 'Moni_NZS').Members.remove(algo)

appendPostConfigAction( removeAlgo )    

MoniVELOSeq=GaudiSequencer('MoniVELOSeq') 
MoniVELOSeq.Members+=[GainMon('GainMon')]

GainMon( 'GainMon').isGainScan=True
GainMon( 'GainMon').nEvtGainScan=2000   ##These two options should match the scan.
GainMon( 'GainMon').nStepsGainScan=23

##Some options. They are set by default, but you can change them here:
##GainMon( 'GainMon').TELL1s=[]
##GainMon( 'GainMon').PlotHeadersHighLow=True
##GainMon( 'GainMon').PlotHeaders=True
##GainMon( 'GainMon').MaxToPlot=750 
##GainMon( 'GainMon').MinToPlot=400
##GainMon( 'GainMon').NForAvgCalc=500
###############################################################################
