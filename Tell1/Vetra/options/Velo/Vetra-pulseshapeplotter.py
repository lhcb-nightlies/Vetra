from Gaudi.Configuration     import EventSelector, MessageSvc
msgsvc = MessageSvc()
msgsvc.OutputLevel = 4
from Configurables import LHCbApp
from Configurables import Vetra
vetra = Vetra()
vetra.DataBanks      = 'NONE'
vetra.RunEmulation   = False
#Will include my  own version of the emulator.
#vetra.EmuMode        = 'Dynamic'
vetra.EmuMode        = 'Static'
vetra.BitPerfectness = False
vetra.OutputType     = 'NONE'
vetra.FilterBeamBeam = False
# --> generated data are taken wihtout the ODIN bank
from GaudiKernel.ProcessJobOptions import importOptions
importOptions("$VETRAROOT/options/Velo/TAEDecodingNZS-pulseshapeplotter.py")
from Configurables import CondDB
CondDB().IgnoreHeartBeat = True

#vetra.DataType  =   '2011'
#vetra.DataBanks  =   'NZS+ZS'
#vetra.Context  =   'Offline'
#vetra.VELOCONDtag  =   'HEAD'
#vetra.DDDBtag  =   ''
#vetra.CondDBtag  =   ''
#vetra.UseOracle  =   True
#vetra.Simulation  =   False
vetra.RunEmulation  =   False
vetra.EmuMode  =   'Dynamic'
vetra.EmuConvLimit  =   0
vetra.MoniDataFilter  =   'NONE'
vetra.CheckEBs  =   True
vetra.MagnetOn  =   True
vetra.TEDTracks  =   False
vetra.ParaTraining  =   False
vetra.BitPerfectness  =   False

from Configurables import GaudiSequencer
from Configurables import Velo__PulseShapePlotter as PulseShape

moni_pulseshape = GaudiSequencer( 'Moni_PulseShape' )
pulseShape = PulseShape( 'PulseShape' )
moni_pulseshape.Members = [pulseShape]
pulseShape.StepSize = 100000000
pulseShape.NSteps  = 50
#EventSelector().FirstEvent = 0
#EventSelector().PrintFreq = 100
vetra.EvtMax = -1
vetra.SkipEvents = 100
#pulseShape.SamplingLocations = ["Prev3", "Prev2", "Prev1","","Next1","Next2","Next3"] 
pulseShape.SamplingLocations = [ "Prev2", "Prev1","","Next1","Next2"]
#pulseShape.SamplingLocations = [ ""]
pulseShape.OutputLevel = 4
pulseShape.DataLocation = "Raw/Velo/PreparedADCs"
#pulseShape.DataLocation = "Raw/Velo/DecodedADC"
#pulseShape.SamplingLocations = [ ""]
pulseShape.CMSCut = 12
#pulseShape.ProgressiveCMS = False 
pulseShape.SignalThreshold = 20
pulseShape.EventsPerFile=-1
#pulseShape.TELL1s=[0,4]
#pulseShape.WidthBefore=15.7 from tp
#pulseShape.WidthAfter=23.3  from tp
#out from june ted: 
#pulseShape.WidthBefore=17.0
#pulseShape.WidthAfter=23.1
# @ -30
#pulseShape.WidthBefore=13.4
#pulseShape.WidthAfter=19.9
pulseShape.RunWithChosenBunchCounters=False
#pulseShape.ChosenBunchCounters = [2685,2686,2687,2688,2689]
#pulseShape.RunNumbers = [63462, 63468, 63470,63472,63475] 
#pulseShape.RunNumbersDelay = [0,  0, 12.5, 19, 6.5]


def mystuff():
  GaudiSequencer( 'MoniVELOSeq' ).Members = [moni_pulseshape]
appendPostConfigAction( mystuff )


EventSelector().Input = [
# daqarea file examples
#"DATAFILE='file:///daqarea/lhcb/data/2011/RAW/FULL/VELO/FAKEBEAMSCAN/84430/084430_0000000001.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2011/RAW/FULL/VELO/FAKEBEAMSCAN/84430/084430_0000000002.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2011/RAW/FULL/VELO/FAKEBEAMSCAN/84430/084430_0000000003.raw' SVC='LHCb::MDFSelector'",
#"DATAFILE='file:///daqarea/lhcb/data/2011/RAW/FULL/VELO/FAKEBEAMSCAN/84430/084430_0000000004.raw' SVC='LHCb::MDFSelector'"

  #castor file example
#  "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55033/055033_0000000001.raw' SVC='LHCb::MDFSelector'"
 #castor file example for lxplus
"DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55033/055033_0000000001.raw' SVC='LHCb::MDFSelector'"
  
  ]

vetra.HistogramFile  = 'TestPulse2011_1.root'

