"""
##############################################################################
#                                                                            #
#  Convergence Limit for the TELL1 emulation.                                #
#  The default is set to 4096; this is not a 'magic' number, one cycle of    #
#  the pedestal following is 1024 events. This means that we need usually 4  #
#  cycles to get convergence on the pedestals in each channel.               #
#  The limit should be set to 0 for checking the bit-perfectness.            #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Tomasz Szumlak     ( t.szumlak@physics.gla.ac.uk   )             #
#  @date    02/05/2009                                                       #
#                                                                            #
##############################################################################
"""

# =============================================================================
# Import necessary modules
# =============================================================================
from Configurables           import Vetra
from Configurables           import VeloTELL1EmulatorInit

# --> convergence limit, this will be propagated to all TELL1 algos
VeloTELL1EmulatorInit().ConvergenceLimit = Vetra().getProp( 'EmuConvLimit' )
