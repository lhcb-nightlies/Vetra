"""
##############################################################################
#                                                                            #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#                                                                            #
##############################################################################
"""

from Gaudi.Configuration     import EventSelector
from Configurables import Vetra
from Configurables import PrepareVeloFullRawBuffer
from Configurables import DecodeVeloFullRawBuffer
from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters
from Configurables import CondDB

#CondDB().IgnoreHeartBeat = True
#CondDB().UseOracle = False

vetra = Vetra()

vetra.DataBanks      = 'NZS'
vetra.RunEmulation   = True
vetra.BitPerfectness = False
vetra.OutputType     = 'NONE'
vetra.TupleFile = "CMSTuple.root"
vetra.EvtMax = -1

# --> generated data are taken wihtout the ODIN bank
PrepareVeloFullRawBuffer().RunWithODIN = False
DecodeVeloFullRawBuffer().SectorCorrection = True
DecodeVeloFullRawBuffer().CableOrder = [ 3, 2, 1, 0 ]

###############################################################################

from Configurables import GaudiSequencer
from Configurables import Velo__Monitoring__CMSTuple as CMSTuple
cmsseq = GaudiSequencer( 'Moni_CMS' )
cmttuple = CMSTuple( "CMSTuple" )
cmsseq.Members = [cmttuple]

def mystuff():
  GaudiSequencer( 'MoniVELOSeq' ).Members = [ cmsseq ]
appendPostConfigAction( mystuff )

##############################################################################
EventSelector().Input = [
     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2011/RAW/CALIB/LHCb/COLLISION11/91556/091556_0000000001.raw' SVC='LHCb::MDFSelector'",
     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2011/RAW/CALIB/LHCb/COLLISION11/91556/091556_0000000010.raw' SVC='LHCb::MDFSelector'"
]
###############################################################################
vetra.HistogramFile  = 'Hist.root'
