"""
###############################################################################
#                                                                             #
#  Example file to run Vetra on TED data taken in August 2008                 #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-TED_Data-2008-08.py                                    #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @date    10/03/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration import EventSelector
from Configurables       import UpdateManagerSvc

from Configurables import Vetra

from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters
from GaudiKernel.ProcessJobOptions import importOptions
from Gaudi.Configuration import EventSelector

vetra = Vetra()

# only ZS data present
vetra.DataBanks = 'ZS'
vetra.RunEmulation = False
vetra.CheckTAEs = True
vetra.TEDTracks = True
vetra.MagnetOn = False
#vetra.EmuConvLimit = 5
#vetra.EmuConvLimit = 0
# do the time-alignment study

# set the appropriate DB tags
vetra.DDDBtag        = 'head-20080905'
vetra.CondDBtag      = 'head-20080905'

EventSelector().PrintFreq = 1

# Special TED data settings for the DecodeVeloRawBuffer algorithm
# (see $VETRAOPTS/Velo/VetraDecoding_ZS.py)
#decodeToVeloClusters = DefaultDecoderToVeloClusters( 'DecodeToVeloClusters' )
#decodeToVeloClusters.RawEventLocation = 'Prev1/DAQ/RawEvent'

# Special TED data settings for the DecodeVeloRawBuffer algorithm
# (see $VETRAOPTS/Velo/VetraDecoding_TAE.py)
importOptions("$VETRAOPTS/Velo/VetraPR.py")
EventSelector().Input = [
    "DATAFILE='/castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/TED/59038/059038_0000000001.raw' SVC='LHCb::MDFSelector'"
    ]
