"""
##############################################################################
#                                                                            #
#  Decoding of the emulated and real ZS banks for bit perfectness            #
#  checking                                                                  #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Tomasz Szumlak  ( t.szumlak@physics.gla.ac.uk )                  #
#  @date    28/04/2009                                                       #
#                                                                            #
##############################################################################
"""

from Configurables           import GaudiSequencer
from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters

decoding_BPC = GaudiSequencer( 'Decoding_BPC' )

# =================
# Emulated ZS Banks
# =================
decodeEmu = DefaultDecoderToVeloClusters( 'DecodeEmu' )
decodeEmu.RawEventLocation="Emu/RawEvent";
decodeEmu.VeloClusterLocation = 'Emu/Velo/Clusters'
decodeEmu.DumpVeloClusters = False
decoding_BPC.Members += [ decodeEmu ]

# ==============
# TELL1 ZS Banks
# ==============
decodeTell1 = DefaultDecoderToVeloClusters( 'DecodeTell1' )
decodeTell1.RawEventLocation="DAQ/RawEvent";
decodeTell1.VeloClusterLocation = 'Raw/Velo/Clusters'
decodeTell1.DumpVeloClusters = False
decoding_BPC.Members += [ decodeTell1 ]

GaudiSequencer( 'MoniVELOSeq' ).Members = [ decoding_BPC ]
