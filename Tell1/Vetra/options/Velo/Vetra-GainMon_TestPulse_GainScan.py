"""
##################################################################################
#                                                                                #
#  Example file to run monitoring on a Gain Scan with test pulses                #
#                                                                                #
#  Example usage:                                                                #
#gaudirun.py Vetra-Default-NZS_Data-Emul.py Vetra-GainMon_TestPulse_GainScan.py..#
#                                                                inputData.py    #
#                                                                                #
#   Produces plots for gain scan data with test pulses which can be read by      #
#   $VETRASCRIPTS/macros/drawFHSPlots.C                                          #
#                                                                                #
#  @package Velo/VeloDataMonitor                                                 #
#  @author  Grant McGregor  (Grant.McGregor@hep.manchester.ac.uk )               #
#  @date    24/08/2009                                                           #
#                                                                                #
##################################################################################
"""
from Configurables import GaudiSequencer
from Configurables import Vetra
from Configurables import CondDB
from Configurables import PrepareVeloFullRawBuffer
from Configurables import Velo__Monitoring__GainMon as GainMon
from Configurables import Velo__Monitoring__GainMonTestPulse as GainMonTestPulse
from GaudiKernel.Configurable import appendPostConfigAction
from Configurables import VeloPedestalSubtractorMoni      as PedSubMon
def removeAlgo():
    algo =  PedSubMon( )
    GaudiSequencer( 'Moni_NZS').Members.remove(algo)
appendPostConfigAction( removeAlgo )    


vetra = Vetra()
vetra.HistogramFile = 'gainMon_TestPulse_GainScan_Histos.root'
vetra.EvtMax=43990   
vetra.DataBanks    = 'NZS'
vetra.FilterBeamBeam = False 
vetra.VELOCONDtag = 'HEAD'

CondDB().IgnoreHeartBeat = True

PrepareVeloFullRawBuffer().IgnoreErrorBanks = True

MoniVELOSeq=GaudiSequencer('MoniVELOSeq') 
MoniVELOSeq.Members+=[GainMon('GainMon')]
MoniVELOSeq.Members+=[GainMonTestPulse('GainMonTestPulse')]

GainMon( 'GainMon').isGainScan=True
GainMon( 'GainMon').nEvtGainScan=2000   ##These four options should match the scan.
GainMon( 'GainMon').nStepsGainScan=23
GainMonTestPulse('GainMonTestPulse').isGainScan=True
GainMonTestPulse('GainMonTestPulse').nEvtGainScan=2000

##Some options. They are set by default, but you can change them here:
##GainMon( 'GainMon').PlotHeadersHighLow=True
##GainMon( 'GainMon').PlotHeaders=True
##GainMon( 'GainMon').MaxToPlot=750 
##GainMon( 'GainMon').MinToPlot=400
##GainMon( 'GainMon').NForAvgCalc=500
##and some for GainMonTestPulse:
##GainMonTestPulse('GainMonTestPulse').PublishingFrequency = 500
##GainMonTestPulse('GainMonTestPulse').HistoPrint = True 
###############################################################################
