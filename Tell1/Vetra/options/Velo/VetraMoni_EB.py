"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of Error Banks (EB).                 #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @author  Tomasz Szumlak     ( t.szumlak@physics.gla.ac.uk )               #
#  @date    17/11/2008                                                       #
#                                                                            #
##############################################################################
"""

from Configurables import GaudiSequencer
from Configurables import Velo__Monitoring__ErrorMon as ErrorMon
from Configurables import Velo__Monitoring__PCNErrorMon as PCNErrorMon

moni_EB = GaudiSequencer( 'Moni_EB' )

moni_EB.Members = [ ErrorMon('ErrorMon'), PCNErrorMon('PCNErrorMon') ]

GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_EB ]

##############################################################################
