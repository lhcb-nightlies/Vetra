"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of non-zero-suppressed data.         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
##############################################################################
"""

from GaudiKernel.ProcessJobOptions import importOptions
from Gaudi.Configuration           import EventSelector

from Configurables import GaudiSequencer, LHCbApp
from Configurables import Vetra
from Configurables import Velo__TestPulseShape as PulseShape


vetra = Vetra()
vetra.DataBanks      = 'NONE'
vetra.RunEmulation   = False
#Will include my  own version of the emulator.
vetra.BitPerfectness = False
vetra.OutputType     = 'NONE'
# --> generated data are taken wihtout the ODIN bank


importOptions("$VETRAROOT/options/Velo/TAEDecodingNZS.py")
importOptions("$VETRAROOT/options/Velo/TAEEmulatorNZS.py")

#VeloTELL1Reordering().SectorCorrection = True
##############################################################################

moni_pulseshape = GaudiSequencer( 'Moni_PulseShape' )
pulseShape = PulseShape( 'PulseShape' )
moni_pulseshape.Members = [pulseShape]
pulseShape.StepSize = 100
pulseShape.NSteps  = 25
#EventSelector().FirstEvent = 2599
EventSelector().FirstEvent = 101
EventSelector().PrintFreq = 1
vetra.EvtMax = -1
#EventSelector().EvtMax = 2
#pulseShape.PhaseChangeMethod=1
pulseShape.PulsedChannels = [4,23] 
pulseShape.OutputLevel = 3
pulseShape.TELL1s = [80]
pulseShape.Plot2D = 0
pulseShape.PlotCMS = 1
#pulseShape.NEventsToSkip =0
#pulseShape.CommonModeCut = 10
pulseShape.PlotRaw = 0
pulseShape.PlotProfile = 1
pulseShape.WrapPoint = 250000
pulseShape.WrappedSteps = 0
pulseShape.SamplingLocations = ["Prev3", "Prev2", "Prev1","","Next1","Next2","Next3"] 
#pulseShape.SamplingLocations = { "Prev2", "Prev1","","Next1","Next2"};
#pulseShape.PlotOnlyLink = 1
GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_pulseshape ]
#GaudiSequencer( 'MoniVELOSeq' ).Members.remove( GaudiSequencer( 'Moni_NZS' ) )

##############################################################################

##############################################################################
EventSelector().Input = [
     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55845/055845_0000000001.raw' SVC='LHCb::MDFSelector'",
     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55845/055845_0000000002.raw' SVC='LHCb::MDFSelector'",
     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55845/055845_0000000003.raw' SVC='LHCb::MDFSelector'",
     "DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELO/TESTPULSESCAN/55845/055845_0000000004.raw' SVC='LHCb::MDFSelector'"
]
###############################################################################
vetra.HistogramFile  = 'TPScan55845.root'
