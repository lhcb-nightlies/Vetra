"""
###############################################################################
#                                                                             #
#  This options file is used as for running training job                      #
#                                                                             #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Tomasz Szumlak  ( t.szumlak@physics.gla.ac.uk )                   #
#  @date    24/04/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration     import EventSelector

from Configurables import Vetra, CondDB
from Configurables import VeloTELL1PedestalSubtractor as vtps
from Configurables import VeloClusteringThresholdsComputer as vctc
from Configurables import VeloBeetleHeaderXTalkComputer as vbhxc
vetra = Vetra()

vetra.DataBanks    = 'NZS'
vetra.RunEmulation = True
vetra.EmuConvLimit = 4096
vetra.ParaTraining = True
vctc().ConvergenceLimit = 4096
vbhxc().ConvergenceLimit = 4096
vetra.HistogramFile = 'Tell1_Params_Storage.root'
EventSelector().PrintFreq = 100
CondDB().IgnoreHeartBeat = True

#
# Get the files from an automatically generated list of files
# in the current working directory. For old behaviour see below.
#
data_list_file = open("./param_training_data.txt")
data_files = []
for line in data_list_file:
	data_files.append("DATAFILE='%s' SVC='LHCb::MDFSelector'" % line[:-1])
data_list_file.close()

EventSelector().Input = data_files

#
# -> note! the file may not be the one that is the most recent
#    but it is guaranteed that the whole chain will work! make any
#    necessary change here when want to calculate new paramters
#EventSelector().Input = [
#    "DATAFILE='/castorfs/cern.ch/grid/lhcb/data/2012/RAW/CALIB/LHCb/EOF_CALIB12/125116/125116_0000000005.raw' SVC='LHCb::MDFSelector'" 
#    ,"DATAFILE='/castorfs/cern.ch/grid/lhcb/data/2012/RAW/CALIB/LHCb/EOF_CALIB12/125116/125116_0000000006.raw' SVC='LHCb::MDFSelector'" 
#    ]

###############################################################################
