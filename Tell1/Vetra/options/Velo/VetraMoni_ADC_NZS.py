"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of non-zero-suppressed data.         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
#  @author  Chiara Farinelli   <cfarinel@cern.ch>                            #
#  @date    02-02-2011                                                       #
#                                                                            #
##############################################################################
"""

from GaudiKernel.ProcessJobOptions import importOptions

from Configurables import GaudiSequencer

from Configurables import Velo__Monitoring__ADCMon      as ADCMon
from Configurables import Velo__Monitoring__ADCTuple    as ADCTuple


moni_NZS = GaudiSequencer( 'Moni_NZS' )

ADCMon     = ADCMon('ADCMon')
ADCTuple   = ADCTuple('ADCTuple')
moni_NZS.Members = [ADCMon,
                    ADCTuple,
                   ]

#ADCMon( 'ADCMon' ).RefreshRate = 10000000
#ADCTuple( 'ADCTuple' ).RefreshRate = 10000000

#To change location of output file

ADCTuple.NtupleLocation = "/tmp/Ntuple.root"

#Uncomment to specify TELL1s:

#ADCMon.TELL1s = [92]
#ADCTuple.TELL1s = [92]

#Uncomment to plot all links:

ADCTuple.LinkPlot = True

#Uncomment to specify Links:

#ADCTuple.Links = [1,17,50,32]

#Uncomment to select only some containers for monitoring:

ADCMon.Containers = [  'Raw/Velo/DecodedADC',
                       ]
ADCTuple.Containers = [  'Raw/Velo/DecodedADC',
                         ]

GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_NZS ]

##############################################################################
