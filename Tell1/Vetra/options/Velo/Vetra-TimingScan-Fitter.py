"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring of non-zero-suppressed data.         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#
#   usage: gaudirun.py pulseshapefitterNew.py 
##############################################################################
"""

from Gaudi.Configuration     import EventSelector, MessageSvc

msgsvc = MessageSvc()
msgsvc.OutputLevel = 4
from Configurables import LHCbApp
from Configurables import Vetra
vetra = Vetra()
vetra.DataBanks      = 'NONE'
vetra.RunEmulation   = False
#Will include my  own version of the emulator.
#vetra.EmuMode        = 'Dynamic'
vetra.EmuMode        = 'Static'
vetra.BitPerfectness = False
vetra.OutputType     = 'NONE'

from GaudiConf import IOHelper
IOHelper( Input = "MDF", Output = "MDF" )

# --> generated data are taken wihtout the ODIN bank
# set the appropriate DB tags
#LHCbApp().DDDBtag   = 'head-20080905'
#LHCbApp().CondDBtag = 'head-20080905'
#LHCbApp().CondDBtag = ''
#from GaudiKernel.ProcessJobOptions import importOptions
#importOptions("$VETRAROOT/options/Velo/TAEDecodingNZS.py")
from GaudiKernel.ProcessJobOptions import importOptions
importOptions("$VETRAROOT/options/Velo/TAEDecodingNZS-PulseShapeScan.py")
from Configurables import CondDB
#CondDB().IgnoreHeartBeat = True

#importOptions("$VETRAROOT/options/Velo/TAEEmulatorNZS.py")
#VeloTELL1Reordering().SectorCorrection = True
##############################################################################


from Configurables import GaudiSequencer
from GaudiKernel.Configurable import appendPostConfigAction
from Configurables import Velo__PulseShapeFitterTed as PulseShape
moni_pulseshape = GaudiSequencer( 'Moni_PulseShape' )
pulseShape = PulseShape( 'PulseShape' )
moni_pulseshape.Members = [pulseShape]
EventSelector().FirstEvent = 0
EventSelector().PrintFreq = 2000
vetra.EvtMax = -1

#pulseShape.SamplingLocations = ["Prev3", "Prev2", "Prev1","","Next1","Next2","Next3"] 
pulseShape.SamplingLocations = [ "Prev2", "Prev1","","Next1","Next2"]
#pulseShape.DataLocation = "Raw/Velo/DecodedADC"
pulseShape.OutputLevel = 4#1
pulseShape.CMSCut = 12
pulseShape.ExpectedPeakTime = -3 
pulseShape.ChisqCut = 50#300
pulseShape.SignalThreshold = 20
pulseShape.MaxAmplitudeCut= 500#2000
pulseShape.OffsetCut= 50
pulseShape.IntegralCut=35# 30
pulseShape.FixOffset= True
pulseShape.TakeNeighbours=1
pulseShape.NTimeBins=500
pulseShape.UseLogLikeFit=False  
pulseShape.TryToClusterize = False 
pulseShape.PlotSeparateStrips=False
#pulseShape.TELL1s=[19]
##pulseShape.WidthBefore=13.7
#pulseShape.WidthAfter=26.8 
#pulseShape.WidthBefore=13.4
#pulseShape.WidthAfter=19.9
#pulseShape.WidthBefore=14.0# original
#pulseShape.WidthAfter=20.0# original
pulseShape.WidthBefore=15.7#quickfit3 value-landau
pulseShape.WidthAfter=17.1#quickfit3 value-landau
#pulseShape.RunWithChosenBunchCounters=True# for one bunch
#pulseShape.ChosenBunchCounters = [1000]#  for one bunch
pulseShape.RunNumbersDelay = [ 0 ]

########### Configurables ########
pulseShape.TimeWindow= 20#Set to 50 if only one pulseshape, but for 50-ns bunch spacing set it to 20 to select only the middle pulse
pulseShape.SelectBunchCrossing = True
pulseShape.RemoveHeaders = False #to remove channels %32 which contain header info
pulseShape.SkipStepZero = True #to do the step0 data only, def = true
#pulseShape.OnlyStepZero = True #def = false
pulseShape.NEventsPerStep = -1 #to take less stats
pulseShape.RunNumbers = [87214]

#################################

def mystuff():
  GaudiSequencer( 'MoniVELOSeq' ).Members = [moni_pulseshape]
appendPostConfigAction( mystuff )

##############################################################################
#importOptions("$VETRAROOT/options/Velo/listoftimingsteps.py")
#importOptions("$VETRAROOT/options/Velo/listoftimingsteps_lowstats.py")

a = []


##############################################################################
##
##TIMINGSCAN september 2011, where the stepper didn't step..
#for i in range(1,292):
#   a.append("DATAFILE='file:///daqarea/lhcb/data/2011/RAW/FULL/LHCb/CALIBRATION11/101371/101371_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")
#   a.append("DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/data/2011/RAW/FULL/LHCb/CALIBRATION11/101371/101371_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")

##FAKEBEAMSCAN 7 march 2012:
#for i in range(1,2):
#  a.append("DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2012/RAW/FULL/VELO/FAKEBEAMSCAN/108582/108582_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'" )

## global TIMINGSCAN 27 march 2012:(500 ev per step)
#for i in range(1,8):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110296/110296_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")
#for i in range(1,8):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110309/110309_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")
#  a.append("DATAFILE='file:///castorfs/cern.ch/grid/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110309/110309_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'" )

## Test global TIMINGSCAN for TAE events March 28 2012: (100 ev per step), error banks on (only step0 appears in data)
#for i in range(1,5):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110490/110490_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")

## Test global TIMINGSCAN to test TAE with passthrough trigger March 28 2012:(100 ev per step), Error banks off (Has all steps, but no ADC values cause velo Voltage is off)
#for i in range(1,5):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110499/110499_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")
  
## Passthrough Markus, Error banks off,(has all steps, but no ADC values cause velo Voltage is off)
#for i in range(1,5):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110501/110501_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")

## TIMINGSCAN without beam, but with LV on, 2 April 2012, error banks off, 5000 ev per step - error: Data generator mode so all ADCs are zero
#for i in range(1,67):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110888/110888_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")

## TIMINGSCAN without beam, but with LV on, 2 April 2012, error banks off, 500 ev per step
#for i in range(1,9):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/110985/110985_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")
##
##############################################################################


## TIMINGSCAN with beam, but with LV on, 5April 2012, error banks off, 5000 ev per step
#for i in range(1,118):
#  a.append("DATAFILE='file:///daqarea/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/111176/111176_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")

## Comparison with 2011 run 87214, taken on 15 March 2011
#for i in range(1,278):
#  a.append("DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/data/2011/RAW/FULL/LHCb/CALIBRATION11/87214/087214_0000000"+('%03d' % i)+".raw' SVC='LHCb::MDFSelector'")

a.append("DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/data/2012/RAW/FULL/LHCb/CALIBRATION12/111176/111176_0000000100.raw' SVC='LHCb::MDFSelector'")

EventSelector().Input = a



##############################################################################
vetra.HistogramFile  = 'TimingRun111176_fitter.root'
