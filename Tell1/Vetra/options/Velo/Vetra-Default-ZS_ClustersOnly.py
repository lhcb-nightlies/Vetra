"""
###############################################################################
#                                                                             #
#  Example default file to run Vetra on ZS data                               #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-Default-ZS_Data.py                                     #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @date    12/03/2009                                                        #
#                                                                             #
###############################################################################
"""

from GaudiKernel.ProcessJobOptions import importOptions
from Gaudi.Configuration           import EventSelector

from Configurables import Vetra


Vetra().DataBanks = 'Clusters'

EventSelector().PrintFreq = 100

EventSelector().Input = [
    "DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/COLLISION09/63949/063949_0000000001.raw' SVC='LHCb::MDFSelector'"
    ]

###############################################################################
