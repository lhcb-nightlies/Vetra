"""
###############################################################################
#                                                                             #
#  This options file is used to confirm trained processing                    #
#  parameters
#                                                                             #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Tomasz Szumlak  ( t.szumlak@physics.gla.ac.uk )                   #
#  @date    24/04/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration     import EventSelector

from Configurables import Vetra, VetraTELL1ParaTrainingConf


vetra = Vetra()

vetra.DataBanks    = 'NZS'
vetra.RunEmulation = True
vetra.EmuConvLimit = 0
vetra.ParaTraining = True

VetraTELL1ParaTrainingConf().ParaMode = 'Checking'

EventSelector().PrintFreq = 100

EventSelector().Input = [
    "DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/VELOA/PHYSICSNTP/45445/045445_0000000001.raw' SVC='LHCb::MDFSelector'"
    ]

###############################################################################
