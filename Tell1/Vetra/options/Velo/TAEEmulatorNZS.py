from Configurables import VeloTELL1EmulatorInit
from Configurables import dataTranslator
from Configurables import GaudiSequencer
from GaudiKernel.ProcessJobOptions import importOptions

emulation = GaudiSequencer( 'TELL1Emulation' )
translatePrev7  = dataTranslator("translatePrev7")
translatePrev6  = dataTranslator("translatePrev6")
translatePrev5  = dataTranslator("translatePrev5") 
translatePrev4  = dataTranslator("translatePrev4") 
translatePrev3  = dataTranslator("translatePrev3") 
translatePrev2  = dataTranslator("translatePrev2") 
translatePrev1  = dataTranslator("translatePrev1") 
translateNext1  = dataTranslator("translateNext1") 
translateNext2  = dataTranslator("translateNext2") 
translateNext3  = dataTranslator("translateNext3") 
translateNext4  = dataTranslator("translateNext4") 
translateNext5  = dataTranslator("translateNext5") 
translateNext6  = dataTranslator("translateNext6") 
translateNext7  = dataTranslator("translateNext7") 
emulation.Members = [VeloTELL1EmulatorInit() 
                         ,dataTranslator() 
#                         ,translatePrev7    
#                         ,translatePrev6   
#                         ,translatePrev5   
#                         ,translatePrev4   
                         ,translatePrev3   
                         ,translatePrev2   
                         ,translatePrev1   
                         ,translateNext1   
                         ,translateNext2   
                         ,translateNext3   
#                         ,translateNext4   
#                         ,translateNext5   
#                         ,translateNext6   
#                         ,translateNext7   
                            ]
translatePrev7.InputDataLoc ="Prev1/Raw/Velo/DecodedADC" 
translatePrev6.InputDataLoc ="Prev2/Raw/Velo/DecodedADC" 
translatePrev5.InputDataLoc ="Prev3/Raw/Velo/DecodedADC" 
translatePrev4.InputDataLoc ="Prev4/Raw/Velo/DecodedADC" 
translatePrev3.InputDataLoc ="Prev5/Raw/Velo/DecodedADC" 
translatePrev2.InputDataLoc ="Prev6/Raw/Velo/DecodedADC" 
translatePrev1.InputDataLoc ="Prev7/Raw/Velo/DecodedADC" 
translateNext1.InputDataLoc ="Next1/Raw/Velo/DecodedADC" 
translateNext2.InputDataLoc ="Next2/Raw/Velo/DecodedADC" 
translateNext3.InputDataLoc ="Next3/Raw/Velo/DecodedADC" 
translateNext4.InputDataLoc ="Next4/Raw/Velo/DecodedADC" 
translateNext5.InputDataLoc ="Next5/Raw/Velo/DecodedADC" 
translateNext6.InputDataLoc ="Next6/Raw/Velo/DecodedADC" 
translateNext7.InputDataLoc ="Next7/Raw/Velo/DecodedADC" 

GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ emulation ]

