"""
##############################################################################
#                                                                            #
#  Configuration for Vetra's monitoring for time-alignment studies.          #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @date   03/02/2009                                                        #
#                                                                            #
##############################################################################
"""

from GaudiKernel.ProcessJobOptions import importOptions

from Configurables import GaudiSequencer

from Configurables import Velo__VeloClusterMonitor
from Configurables import Velo__VeloOccupancyMonitor
from Configurables import Velo__VeloSamplingMonitor


moni_TAE = GaudiSequencer( 'Moni_TAE' )

for i in range(1,8) :
   moniPrev = [ Velo__VeloClusterMonitor( 'VCMPrev%1d' % i ),
                Velo__VeloOccupancyMonitor( 'VOCCPrev%1d' % i )
                ]
   Velo__VeloClusterMonitor  ( 'VCMPrev%1d' % i  ).VeloClusterLocation = 'Raw/Velo/Prev%1d/Clusters' % i
   Velo__VeloOccupancyMonitor( 'VOCCPrev%1d' % i ).VeloClusterLocation = 'Raw/Velo/Prev%1d/Clusters' % i
   
   moni_TAE.Members += moniPrev

for i in range(1,8) :
   moniNext = [ Velo__VeloClusterMonitor( 'VCMNext%1d' % i ),
                Velo__VeloOccupancyMonitor( 'VOCCNext%1d' % i )
                ]
   Velo__VeloClusterMonitor  ( 'VCMNext%1d' % i  ).VeloClusterLocation = 'Raw/Velo/Next%1d/Clusters' % i
   Velo__VeloOccupancyMonitor( 'VOCCNext%1d' % i ).VeloClusterLocation = 'Raw/Velo/Next%1d/Clusters' % i
   
   moni_TAE.Members += moniNext


# ================
# Sampling monitor
# ================
moni_TAE.Members += [ Velo__VeloSamplingMonitor( 'VeloSamplingMonitor' ) ]


GaudiSequencer( 'MoniVELOSeq' ).Members += [ moni_TAE ]

##############################################################################
