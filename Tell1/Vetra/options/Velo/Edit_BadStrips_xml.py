#!/usr/bin/python

# Name of new bad strips file from CCEscan output
new = 'Dec2012'
# Name of old bad strips xml file from CondDB
old = 'Sep2012'

# Parse new bad strips file to create a sensor/strips dictionary
f = open('BadStrips_'+new+'.txt')
stripDict = {}
lines = []

for line in f:
   lines.append(line.split(','))

for line in lines:
  if line[0] in stripDict:
     stripDict[line[0]] += ','+line[1].strip()
  else:
     stripDict[line[0]] = line[1].strip()

# Import previous bad strips xml and compare entries to dictionary 
import xml.etree.ElementTree as ET
tree = ET.parse('BadStrips_'+old+'.xml')
root = tree.getroot()

for child in root[0][0]:
   if 'R' in child.attrib['name']:
      if child.attrib['name'].split('R')[1] in stripDict:
         child.text += stripDict[child.attrib['name'].split('R')[1]].replace(',',' ')+' '
   elif 'Phi' in child.attrib['name']:
      if str(int(child.attrib['name'].split('Phi')[1])+64) in stripDict:
         child.text += stripDict[str(int(child.attrib['name'].split('Phi')[1])+64)].replace(',',' ')+' '

# Write to new xml file
tree.write('BadStrips_'+new+'.xml')

# Prepend xml encoding
with open('BadStrips_'+new+'.xml', 'r+') as myfile:
   s = myfile.read()
   myfile.seek(0)
   myfile.write('<?xml version="1.0" encoding="ISO-8859-1"?>\n<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd">\n' + s)
