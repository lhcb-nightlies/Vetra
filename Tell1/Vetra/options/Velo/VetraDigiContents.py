"""
##############################################################################
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  Vetra file defining the contents of the output DIGI file                  #
#  @author Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @date   05/10/2008                                                        #
#                                                                            #
##############################################################################
"""

from Gaudi.Configuration import OutputStream


OutputStream( 'VetraDigiWriter' ).ItemList = [
    '/Event/DAQ/RawEvent#1',
    '/Event/Emu/RawEvent#1'
    ]

##############################################################################
