"""
##############################################################################
#                                                                            #
#  Vetra option file for the "TELL1Processing" phase of a Vetra job.         #
#  Defines the decoding of the VeloRawBuffer into VeloClusters               #
#  for time-alignment studies.                                               #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    06/10/2008                                                       #
#                                                                            #
##############################################################################
"""

from Configurables import GaudiSequencer

from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters


decoding_TAE = GaudiSequencer( 'Decoding_TAE' )

# =====
# Prev1
# =====
decodePrev1 = DefaultDecoderToVeloClusters( 'DecodePrev1' )

decodePrev1.VeloClusterLocation = 'Raw/Velo/Prev1/Clusters'
decodePrev1.RawEventLocation    = 'Prev1/DAQ/RawEvent'

decoding_TAE.Members += [ decodePrev1 ]


# =====
# Prev2
# =====
decodePrev2 = DefaultDecoderToVeloClusters( 'DecodePrev2' )

decodePrev2.VeloClusterLocation = 'Raw/Velo/Prev2/Clusters'
decodePrev2.RawEventLocation    = 'Prev2/DAQ/RawEvent'

decoding_TAE.Members += [ decodePrev2 ]


# =====
# Prev3
# =====
decodePrev3 = DefaultDecoderToVeloClusters( 'DecodePrev3' )

decodePrev3.VeloClusterLocation = 'Raw/Velo/Prev3/Clusters'
decodePrev3.RawEventLocation    = 'Prev3/DAQ/RawEvent'

decoding_TAE.Members += [ decodePrev3 ]


# =====
# Prev4
# =====
decodePrev4 = DefaultDecoderToVeloClusters( 'DecodePrev4' )

decodePrev4.VeloClusterLocation = 'Raw/Velo/Prev4/Clusters'
decodePrev4.RawEventLocation    = 'Prev4/DAQ/RawEvent'

decoding_TAE.Members += [ decodePrev4 ]


# =====
# Prev5
# =====
decodePrev5 = DefaultDecoderToVeloClusters( 'DecodePrev5' )

decodePrev5.VeloClusterLocation = 'Raw/Velo/Prev5/Clusters'
decodePrev5.RawEventLocation    = 'Prev5/DAQ/RawEvent'

decoding_TAE.Members += [ decodePrev5 ]


# =====
# Prev6
# =====
decodePrev6 = DefaultDecoderToVeloClusters( 'DecodePrev6' )

decodePrev6.VeloClusterLocation = 'Raw/Velo/Prev6/Clusters'
decodePrev6.RawEventLocation    = 'Prev6/DAQ/RawEvent'

decoding_TAE.Members += [ decodePrev6 ]


# =====
# Prev7
# =====
decodePrev7 = DefaultDecoderToVeloClusters( 'DecodePrev7' )

decodePrev7.VeloClusterLocation = 'Raw/Velo/Prev7/Clusters'
decodePrev7.RawEventLocation    = 'Prev7/DAQ/RawEvent'

decoding_TAE.Members += [ decodePrev7 ]


# =====
# Next1
# =====
decodeNext1 = DefaultDecoderToVeloClusters( 'DecodeNext1' )

decodeNext1.VeloClusterLocation = 'Raw/Velo/Next1/Clusters'
decodeNext1.RawEventLocation    = 'Next1/DAQ/RawEvent'

decoding_TAE.Members += [ decodeNext1 ]


# =====
# Next2
# =====
decodeNext2 = DefaultDecoderToVeloClusters( 'DecodeNext2' )

decodeNext2.VeloClusterLocation = 'Raw/Velo/Next2/Clusters'
decodeNext2.RawEventLocation    = 'Next2/DAQ/RawEvent'

decoding_TAE.Members += [ decodeNext2 ]


# =====
# Next3
# =====
decodeNext3 = DefaultDecoderToVeloClusters( 'DecodeNext3' )

decodeNext3.VeloClusterLocation = 'Raw/Velo/Next3/Clusters'
decodeNext3.RawEventLocation    = 'Next3/DAQ/RawEvent'

decoding_TAE.Members += [ decodeNext3 ]


# =====
# Next4
# =====
decodeNext4 = DefaultDecoderToVeloClusters( 'DecodeNext4' )

decodeNext4.VeloClusterLocation = 'Raw/Velo/Next4/Clusters'
decodeNext4.RawEventLocation    = 'Next4/DAQ/RawEvent'

decoding_TAE.Members += [ decodeNext4 ]


# =====
# Next5
# =====
decodeNext5 = DefaultDecoderToVeloClusters( 'DecodeNext5' )

decodeNext5.VeloClusterLocation = 'Raw/Velo/Next5/Clusters'
decodeNext5.RawEventLocation    = 'Next5/DAQ/RawEvent'

decoding_TAE.Members += [ decodeNext5 ]


# =====
# Next6
# =====
decodeNext6 = DefaultDecoderToVeloClusters( 'DecodeNext6' )

decodeNext6.VeloClusterLocation = 'Raw/Velo/Next6/Clusters'
decodeNext6.RawEventLocation    = 'Next6/DAQ/RawEvent'

decoding_TAE.Members += [ decodeNext6 ]


# =====
# Next7
# =====
decodeNext7 = DefaultDecoderToVeloClusters( 'DecodeNext7' )

decodeNext7.VeloClusterLocation = 'Raw/Velo/Next7/Clusters'
decodeNext7.RawEventLocation    = 'Next7/DAQ/RawEvent'

decoding_TAE.Members += [ decodeNext7 ]


GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ decoding_TAE ]

###############################################################################
