"""
###############################################################################
#                                                                             #
#  Example default file to run Vetra on NZS+ZS data with emulation            #
#                                                                             #
#  Example usage:                                                             #
#    gaudirun.py Vetra-Default-NZS+ZS_Data-Emul.py                            #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @date    11/03/2009                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration import EventSelector

from Configurables import Vetra


Vetra().RunEmulation = True

EventSelector().PrintFreq = 100

EventSelector().Input = [
    "DATAFILE='rfio:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/COLLISION09/63949/063949_0000000001.raw' SVC='LHCb::MDFSelector'"
    ]

###############################################################################
