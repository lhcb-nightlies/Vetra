"""
##############################################################################
#                                                                            #
#  Vetra option file for the "TELL1Processing" phase of a Vetra job.         #
#  Defines the decoding of the Zero-Suppressed (ZS) data,                    #
#  i.e. the decoding of the VeloRawBuffer into VeloClusters.                 #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    06/10/2008                                                       #
#                                                                            #
##############################################################################
"""

from Configurables import GaudiSequencer

from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters


decodeToVeloClusters = DefaultDecoderToVeloClusters( 'DecodeToVeloClusters' )
decodeToVeloClusters.DecodeToVeloClusters     = True
decodeToVeloClusters.DecodeToVeloLiteClusters = True

decoding_ZS = GaudiSequencer( 'Decoding_ZS' )

decoding_ZS.Members += [ decodeToVeloClusters ]

GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ decoding_ZS ]

###############################################################################
