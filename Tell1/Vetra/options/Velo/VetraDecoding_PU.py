"""
##############################################################################
#                                                                            #
#  Decoding of the digital PU banks				             #
#                                                                            #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Serena Oggero					             #
#  @date    22/10/2009                                                       #
#                                                                            #
##############################################################################
"""

from Configurables           import GaudiSequencer
from Configurables           import DecodePileUpData
from VeloDAQ.DefaultVeloRawBufferDecoders import DefaultDecoderToVeloClusters

# PileUp -main
decodePileUpData = DecodePileUpData()
#decodePileUpData.PUClusterLocation = "Raw/Velo/PUClusters"

# PileUp -prev1
decodePileUpDataPrev1 = DecodePileUpData( 'DecodePrev1' )
decodePileUpDataPrev1.RawEventLocation     = 'Prev1/DAQ/RawEvent'
decodePileUpDataPrev1.PUClusterLocation    = 'Raw/Velo/Prev1/PUClusters'
decodePileUpDataPrev1.PUClusterNZSLocation = 'Raw/Velo/Prev1/PUClustersNZS'

# PileUp -prev2
decodePileUpDataPrev2 = DecodePileUpData( 'DecodePrev2' )
decodePileUpDataPrev2.RawEventLocation     = 'Prev2/DAQ/RawEvent'
decodePileUpDataPrev2.PUClusterLocation    = 'Raw/Velo/Prev2/PUClusters'
decodePileUpDataPrev2.PUClusterNZSLocation = 'Raw/Velo/Prev2/PUClustersNZS'

# PileUp -next1
decodePileUpDataNext1 = DecodePileUpData( 'DecodeNext1' )
decodePileUpDataNext1.RawEventLocation     = 'Next1/DAQ/RawEvent'
decodePileUpDataNext1.PUClusterLocation    = 'Raw/Velo/Next1/PUClusters'
decodePileUpDataNext1.PUClusterNZSLocation = 'Raw/Velo/Next1/PUClustersNZS'

# PileUp -next2
decodePileUpDataNext2 = DecodePileUpData( 'DecodeNext2' )
decodePileUpDataNext2.RawEventLocation     = 'Next2/DAQ/RawEvent'
decodePileUpDataNext2.PUClusterLocation    = 'Raw/Velo/Next2/PUClusters'
decodePileUpDataNext2.PUClusterNZSLocation = 'Raw/Velo/Next2/PUClustersNZS'

decoding_PU = GaudiSequencer( 'Decoding_PU' )

decoding_PU.Members = [  decodePileUpData
                         #decodePileUpDataPrev1
                         #decodePileUpDataPrev2
                         #decodePileUpDataNext1
                         #decodePileUpDataNext2
                      ]

GaudiSequencer( 'TELL1ProcessingVELOSeq' ).Members += [ decoding_PU ]
