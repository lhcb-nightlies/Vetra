#Example of how to change condition database settings with python#

#!/usr/bin/env python
import GaudiPython as gaudi

appMgr = gaudi.AppMgr(outputlevel=3)

opts_files=[]
opts_files.append('$VETRAOPTS/ST/runSTEmulator.opts')

DETTYPE="IT"
opts=[]

if DETTYPE=="IT":
  TELL1=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
         32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45,
         64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77]
elif DETTYPE=="TT":
  TELL1=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
       32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
       64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76,
       96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108]

Hit_val = str()
CMS_val = str()
for k in range(3072):
  Hit_val += ' '+str(6)
  CMS_val += ' '+str(16)

for n in range(len(TELL1)):
  # put overriding conditions in 1 string per TELL1
  conditionsstring = 'UpdateManagerSvc.ConditionsOverride += {\"CondDB/TELL1Board%s :=' %TELL1[n]
  conditionsstring += ' int Pedestal_enable = 1;'
  conditionsstring += ' int_v Hit_threshold = ' + Hit_val + ';'
  conditionsstring += ' int_v CMS_threshold = ' + CMS_val + ';'
  for m in range(96):
    if m==0:
      conditionsstring += ' int_v Header_correction_analog_link_%i = -30 -30 -30 -30;' %(m)
    else:
      conditionsstring += ' int_v Header_correction_analog_link_%i = -4 -3 -2 -1;' %(m)
  conditionsstring += '\"}'

  opts.append(conditionsstring)


appMgr.config(files=opts_files,options=opts)
appMgr.run(1000) #iso appMgr.EvtMax

