"""
##############################################################################
#                                                                            #
#  Configuration for the "Output" phase of a Vetra job                       #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
#  Example usage:                                                            #
#  1) Run an extra algorithm in this "Output" processing phase               #
#     from VetraOutputConf import VetraOutputConf                            #
#     from Configurables   import GaudiSequencer                             #
#     from Configurables   import MyExtraAlgo                                #
#     VetraOutputConf().Sequence = [ 'VELO' ]                                #
#     algo = MyExtraAlgo( 'MyExtraAlgo' )                                    #
#     GaudiSequencer( 'OutputVELOSeq' ).Members = [ algo ]                   #
#                                                                            #
##############################################################################
"""

# =============================================================================
# Import necessary modules
# =============================================================================
from GaudiKernel.ProcessJobOptions import importOptions
from Gaudi.Configuration           import OutputStream
from LHCbKernel.Configuration      import LHCbConfigurableUser
from Configurables                 import ApplicationMgr, ProcessPhase

from VetraOptionsChecker     import optionsChecker


###############################################################################
class VetraOutputConf( LHCbConfigurableUser ):
###############################################################################
    """
    Vetra "Output" phase configuration class
    """
    
    # Steering options
    __slots__ = {
          'OutputType'   : 'NONE'
        , 'Sequence'     : []
        , 'Filename'     : 'Vetra'
        , 'RequireAlgs'  : []
        , 'Context'      : 'Offline'
        }
    
    # docstrings for the configurables attributes
    _propertyDocDct = {
          'OutputType'       : """set to 'DIGI' to produce an output file"""
        , 'Sequence'         : """define the Output phase sequence(s)"""
        , 'Filename'         : """base name for the output DIGI file"""
        , 'RequireAlgs'      : """list of required successful algorithms for the DIGI writer"""
        , 'Context'          : """top-level context: Offline/HLT environment"""
        }
    
# ============================================================================
    def checkOptions( self ):
# ============================================================================
        """ Check the correctness of the options """
        optionsChecker( 'OutputType', self.getProp( 'OutputType' ) )
        optionsChecker( 'Context',    self.getProp( 'Context'    ) )
    
# =============================================================================
    def defineOuputStream( self ) :
# =============================================================================
        """ Define the output stream """
        writer = OutputStream( 'VetraDigiWriter' )
        ApplicationMgr().OutStream.append( writer )
        
        outputName = self.getProp( 'Filename' ) + '.' + \
                     self.getProp( 'OutputType' ).lower()
        
        fileString = "DATAFILE='PFN:" + \
                     outputName + \
                     "' TYP='POOL_ROOTTREE' OPT='REC'"

        # set the output DIGI file name
        writer.Output = fileString

        # only write out if certain algorithms issue a "filter pass"
        writer.RequireAlgs = self.getProp( 'RequireAlgs' )
    
# =============================================================================
    def applyConf( self ):
# =============================================================================
        """ Apply the configuration """
        
        # check the options are OK
        self.checkOptions()
        
        # set up the contents of the extra user-defined sequencer
        ProcessPhase( 'Output' ).DetectorList += self.getProp( 'Sequence' )
        
        # set the context for the whole phase
        ProcessPhase( 'Output' ).Context = self.getProp( 'Context' )
        
        # define the output stream algorithm and options
        if self.getProp( 'OutputType' ) != 'NONE':
            self.defineOuputStream()
            importOptions( '$VETRAOPTS/Velo/VetraDigiContents.py' )

###############################################################################
