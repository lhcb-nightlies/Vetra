"""
##############################################################################
#                                                                            #
#  Exclusions: lines that are ignored when comparing the STDOUT of the test  #
#  to the reference file.                                                    #
#  Adapted from Brunel's BrunelExclusions.                                   #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Marius Bjoernstad ( Paul.Bjoernstad@hep.manchester.ac.uk )       #
#  @date    2010-04-26                                                       #
#                                                                            #
##############################################################################
"""

from GaudiTest import LineSkipper
from GaudiConf.QMTest.LHCbExclusions import preprocessor as LHCbPreprocessor

preprocessor = LHCbPreprocessor+\
    LineSkipper(["VetraInit.Vetra...SUCCESS Exceptions/Errors/Warnings/Infos Statistics : "])+\
    LineSkipper(["VetraInit.Vetra...WARNING MemoryTool:: Mean 'delta-memory' exceeds 3*sigma"])+\
    LineSkipper(regexps = ["^VetraInit\.Vetra\.\.\.SUCCESS  #WARNINGS   = [0-9]+[ ]+Message = 'Delta Memory for the event exceeds 3\*sigma'$"])+\
    LineSkipper(regexps = ["^VetraInit\.Vetra\.\.\.SUCCESS  #WARNINGS   = [0-9]+[ ]+Message = 'Mean '[dD]elta-[mM]emory' exceeds 3\*sigma'$"])+\
    LineSkipper(regexps = ["^VetraInit\.Vetra\.\.\.SUCCESS  #WARNINGS   = [0-9]+[ ]+Message = 'Total Memory for the event exceeds 3\*sigma'$"])
    
    
