"""
##############################################################################
#                                                                            #
#  CompareHistograms: Compares histograms in a ROOT file using PyROOT.       #
#                                                                            #
#  Use the CompareHistograms function.                                       #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Marius Bjrnstad ( Paul.Bjoernstad@hep.manchester.ac.uk )         #
#  @date    2010-08-02                                                       #
#                                                                            #
##############################################################################
"""

import ROOT



# Relative tolerance for floating point comparisons, used in EqualHistograms
# There are quite large differences between values on 32 and 64 bits, so a
# large tolerance is used. 
tol = 1e-6

# The smallest absolute difference between two numbers that we worry about. 
# This has been increased beyond what is expected from float precision, because
# tests kept failing on 32 bit -- probably because float errors are propagated
# and enhanced.
eps = 1e-5


# Extract important statistics for each histogram
def GetStats1D(path, hist):
    name = path + hist.GetName()
    info = {"meanx":hist.GetMean(), "rmsx":hist.GetRMS(),
            "#entries": hist.GetEntries(), "type":type(hist).__name__,
            'name':name}
    return name, info

def GetStats2D(path, hist):
    name = path + hist.GetName()
    info = {"meanx":hist.GetMean(1), "rmsx":hist.GetRMS(1),
            "meany":hist.GetMean(2), "rmsy":hist.GetRMS(2),
            "#entries": hist.GetEntries(), "type":type(hist).__name__,
            'name':name}
    return name, info

def GetStats3D(path, hist):
    name = path + hist.GetName()
    info = {"meanx":hist.GetMean(1), "rmsx":hist.GetRMS(1),
            "meany":hist.GetMean(2), "rmsy":hist.GetRMS(2),
            "meanz":hist.GetMean(3), "rmsz":hist.GetRMS(3),
            "#entries": hist.GetEntries(), "type":type(hist).__name__,
            'name':name}
    return name, info

# Recursively read and process all objects in a ROOT directory 
def ProcessDirectory(pathBase, rootDirectory, histCollection):
    keys = rootDirectory.GetListOfKeys()
    
    for k in keys:
        name = ""
        info = {}
        
        item = k.ReadObj()
        
        if item.IsFolder():
            path = pathBase + item.GetName() + "/"
            ProcessDirectory(path, item, histCollection)
        # Using ROOT's introspection seems to be much more stable than Python's
        elif item.InheritsFrom(ROOT.TH3.Class()):
            name, info = GetStats3D(pathBase, item)
        elif item.InheritsFrom(ROOT.TH2.Class()):
            name, info = GetStats2D(pathBase, item)
        elif item.InheritsFrom(ROOT.TH1.Class()):
            name, info = GetStats1D(pathBase, item)
        
        if name != "":
            histCollection[name] = info
            
    return histCollection
        

# Get the histogram statistics in the internal format: a map with keys for 
# the name of the histogram and another map as value, which contains 
# mean and rms.
def GetHistogramStats(rootFileName):
    f = ROOT.TFile(rootFileName)
    if f.IsOpen():
        histograms = ProcessDirectory("", f, {})
        f.Close()
        return histograms
    else:
        return None 
        

def Ignore(checkname, exclude):
    for prefix in exclude:
        if checkname.startswith(prefix):
            return True
    return False


# Save histogram information to a reference file. The format used is the same 
# as the internal format, but serialised using Python's str()
def SaveStatsFile(histogramInfo, fileName, exclude):
    f = open(fileName, "w")
    ks = list(histogramInfo.keys())
    # sort reference file, makes it easier to compare changed reference files
    ks.sort()
    for key in ks:
        info = histogramInfo[key]
        if not Ignore(info['name'], exclude):
            # this code was the much simpler:
            # f.write("%s\n" % str(info))
            # but it's useful to sort the keys to make the 
            # output more readable
            
            intinfo = dict(info)
            # First item: print without comma
            f.write("{%s: %s" % 
                ("'name'", repr(intinfo['name'])))
            del intinfo['name']
            ksinter = list(intinfo.keys())
            ksinter.sort()
            for keyinter in ksinter:
                f.write(", %s: %s" % 
                    (repr(keyinter), repr(intinfo[keyinter])))
            f.write("}\n")
    f.close()
    

# Reads a reference file. 1 line per histogram, using eval.
def ReadStatsFile(fileName):
    try:
        f = open(fileName, "r")
    except IOError:
        return {}
    
    histogramInfo = {}
    lines = f.readlines()
    f.close()
    for l in lines:
        info = eval(l)
        if not info.has_key('name'):
            return {} # Invalid format
        else:
            # OK, has name
            histogramInfo[info['name']] = info
    
    return histogramInfo


# Print the statistics in a human-readable format. histogramMap could contain
# several histograms
def FormatHistograms(histogramMap):
    keys = histogramMap.keys()
    keys.sort()
    
    out = ""
    
    for k in keys:
        info = histogramMap[k].copy()
        out += "%s:\n" % k
        out += "   Type= %8s    #ent=%8d\n" % (info['type'], info['#entries'])
        if info.has_key('meanx'):
            out += "   MeanX=%8g    RMSX=%8g\n" % (info['meanx'], info['rmsx'])
        if info.has_key('meany'):
            out += "   MeanY=%8g    RMSY=%8g\n" % (info['meany'], info['rmsy'])
        if info.has_key('meanz'):
            out += "   MeanZ=%8g    RMSZ=%8g\n" % (info['meanz'], info['rmsz'])
        out += "\n"
    return out

# Print out two histograms for comparison. Hist2 is a map, with key=the 
# name of a pair of histograms. The value is a tuple, containing each 
# of the histogram statistics objects (maps) 
def FormatTwoHistograms(hist2):
    keys = hist2.keys()
    keys.sort()
    
    out = ""
    
    for k in keys:
        refinfo = hist2[k][0].copy()
        saminfo = hist2[k][1].copy()
        del refinfo['name']
        del saminfo['name']
        out += "   *** %-41s ***\n\n" % k
        out += "              ref/  sample              ref/  sample\n"
        out += "   Type= %8s/%8s    #ent=%8d/%8d\n" % (refinfo['type'],saminfo['type'],
                                                     refinfo['#entries'],saminfo['#entries'])
        if refinfo.has_key('meanx') and saminfo.has_key('meanx'):
            out += "   MeanX=%8g/%8g    RMSX=%8g/%8g\n" % (refinfo['meanx'],saminfo['meanx'],
                                                                  refinfo['rmsx'], saminfo['rmsx'])
        if refinfo.has_key('meany') and saminfo.has_key('meany'):
            out += "   MeanY=%8g/%8g    RMSY=%8g/%8g\n" % (refinfo['meany'],saminfo['meany'],
                                                                  refinfo['rmsy'], saminfo['rmsy'])
        if refinfo.has_key('meanz') and saminfo.has_key('meanz'):
            out += "   MeanZ=%8g/%8g    RMSZ=%8g/%8g\n" % (refinfo['meanz'],saminfo['meanz'],
                                                                  refinfo['rmsz'], saminfo['rmsz'])
        out += "\n\n"
    return out


# Compare the statistics summary of two histograms for (some kind of) equality
def EqualHistograms(h1, h2):
    fields = set(h1).union(set(h2))
    for f in fields:
        if not h1.has_key(f):
            return False
        if not h2.has_key(f):
            return False
        
        value1 = h1[f]
        value2 = h2[f]
        
        if type(value1) == float and type(value2) == float:
            # Compare floats for equality, within a small relative tolerance
            # Also allow a small absolute difference (epsilon)
            # The test is asymmetric, but only relevant when value1~value2
            # mag represents the magnitude of the numbers
            mag = abs(value1)
            dif = abs(value1 - value2)
             
            if dif > tol * mag and dif > eps:
                return False
            
        elif value1 != value2:
            return False
        
    return True


# Compares two histogram "summaries" in the internal format, and reports any 
# mismatch in causes and result. exclude contains a list of prefixes to be 
# ignored, if the histogram name (including path) matches any of them
def CompareSummaries(causes, result, reference, sample, exclude):
    ref_extra = reference.copy()
    sam_extra = sample.copy()
    different = {}
    
    # Find extra histograms in either collection, and ones that are
    # different
    for saminfo in sample.values():
        name = saminfo['name']
        
        if Ignore(name, exclude):
            del sam_extra[name]
            if ref_extra.has_key(name):
                del ref_extra[name]
        
        if ref_extra.has_key(name):
            refinfo = ref_extra[name]
            
            if not EqualHistograms(saminfo, refinfo):
                different[name] = (refinfo, saminfo)
                
            del sam_extra[name]
            del ref_extra[name]
    
    
    if ref_extra == {} and sam_extra == {} and different == {}:
        return True
    
    # Format information about failure
    result['Histograms.extra_in_reference'] = result.Quote(FormatHistograms(ref_extra))
    if len(ref_extra) > 0:
        causes.append("extra histograms in reference")
        
    result['Histograms.extra_in_sample'] = result.Quote(FormatHistograms(sam_extra))
    if len(sam_extra) > 0:
        causes.append("extra histograms in sample")
        
    result['Histograms.different'] = result.Quote(FormatTwoHistograms(different))
    if len(different) > 0:
        causes.append("histogram statistics differ from reference")
    
    return False


def CompareHistograms(causes, result, rootFileName, referenceFile = None, exclude = ['VetraInit']):
    newinfo = GetHistogramStats(rootFileName)
    if newinfo == None:
        causes.append("Could not extract histograms from ROOT file " + rootFileName)
        return False
    
    if referenceFile:
        refinfo = ReadStatsFile(referenceFile)
        match = CompareSummaries(causes, result, refinfo, newinfo, exclude)
        
        if not match:
            SaveStatsFile(newinfo, referenceFile + ".new", exclude)
        
        return match
        
    else: # not referenceFile
        return False
    
    
    
    
    
