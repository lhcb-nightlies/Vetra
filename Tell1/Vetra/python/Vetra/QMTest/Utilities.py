
from Configurables import GaudiSequencer

# Filter the algorithms in the sequence seqName
def FilterAlgorithms(seqName, include=[], exclude=[]):
    seq = GaudiSequencer(seqName)
    newMem = []
    for alg in seq.Members:
        if alg.name() in include or (len(include) == 0 and alg.name() not in exclude):
            newMem += [alg]
    seq.Members = newMem
