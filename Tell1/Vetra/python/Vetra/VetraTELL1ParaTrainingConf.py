"""
##############################################################################
#                                                                            #
#  Configuration for the "TELL1Processing" phase of a Vetra job              #
#  During this special phase the TELL1 processing parameters are             #
#  being calculated and the VELOCOND data base created                       #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Tomasz Szumlak  ( t.szumlak@physics.gla.ac.uk )                  #
#  @date    22/04/2009                                                       #
#                                                                            #
##############################################################################
"""

# ============================================================================
# Import necessary modules
# ============================================================================
from GaudiKernel.ProcessJobOptions import importOptions
from LHCbKernel.Configuration      import LHCbConfigurableUser
from Configurables                 import ProcessPhase

from VetraOptionsChecker     import optionsChecker, VetraRuntimeError


##############################################################################
class VetraTELL1ParaTrainingConf( LHCbConfigurableUser ):
##############################################################################
    """
    Vetra configuration class for the "TELL1Processing" phase
    when running in the special mode of the TELL1 emulation to evaluate the
    TELL1 processing parameters
    """
    # Steering options
    __slots__ = {
          'Sequence'      : [ 'VELO' ]
        , 'DataBanks'     : 'NZS+ZS'
        , 'RunEmulation'  : False
        , 'EmuMode'       : 'Static'
        , 'ParaMode'      : 'Training'
        , 'Context'       : 'Offline'
        }
    
    # docstrings for the configurables attributes
    _propertyDocDct = {
          'Sequence'         : """define the TELL1Processing phase sequence(s) for parameters training"""
        , 'DataBanks'        : """check raw and/or full data banks?"""
        , 'RunEmulation'     : """run the TELL1 emulation"""
        , 'EmuMode'          : """emulation mode: 'Static'/'Dynamic' not to/to connect to the VELOCOND database"""
        , 'ParaMode'         : """run in training ('Training') or confirmation ('Checking') mode"""
        , 'Context'          : """top-level context: Offline/HLT environment"""
        }
    
# ============================================================================
    def checkOptions( self ):
# ============================================================================
        """ Check the correctness of the options """
        optionsChecker( 'DataBanks', self.getProp( 'DataBanks' ) )
        optionsChecker( 'EmuMode',   self.getProp( 'EmuMode'   ) )
        optionsChecker( 'ParaMode',  self.getProp( 'ParaMode'  ) )
        optionsChecker( 'Context',   self.getProp( 'Context'   ) )
    
# ============================================================================
    def applyConf( self ):
# ============================================================================
        """ Apply the configuration """
        # check the options are OK
        self.checkOptions()
        
        # set up the contents of the phase (typically a set of sequencers)
        ProcessPhase( 'TELL1Processing' ).DetectorList += self.getProp( 'Sequence' )
        
        # set the context for the whole phase
        ProcessPhase( 'TELL1Processing' ).Context = self.getProp( 'Context' )
        
        # define the sequence for the decoding of the VeloRawBuffer
        # note that if dataBanks == 'NONE', no decoding is performed
        dataBanks = self.getProp( 'DataBanks' )
        print " dataBanks: ", dataBanks
        if dataBanks != 'NZS' and dataBanks != 'NZS+ZS':
           raise VetraRuntimeError( 'ParaMode',
                                    'DataBanks',
                                    'For the training you need NZS data'
                                    )
       
        importOptions( '$VETRAOPTS/Velo/VetraDecoding_NZS.py' )

        # run the TELL1 para training
        runMode = self.getProp( 'ParaMode' )
        if self.getProp( 'RunEmulation' ):
          importOptions( '$VETRAOPTS/Velo/VetraTELL1Emulator.py' )
          importOptions( '$VETRAOPTS/Velo/EmulatorConvergenceLimit.py' )
          if runMode == 'Training':
              importOptions( '$VETRAOPTS/Velo/TELL1ParaTrainingComputers.py' )
          elif runMode == 'Checking':
              importOptions( '$VETRAOPTS/Velo/TELL1ParaChecking.py' )
                  
        # --> append also dynamic configuration 
        if self.getProp( 'EmuMode' ) == 'Dynamic':
            #importOptions( '$VETRAOPTS/Velo/VeloLocalCondDB.py' )
            importOptions( '$VETRAOPTS/Velo/VeloCondDB.py' )
        else:
            raise VetraRuntimeError( 'ParaMode',
                                     'EmuMode',
                                     'You need the VELOCOND DB to run in training mode'
                                     )

##############################################################################
