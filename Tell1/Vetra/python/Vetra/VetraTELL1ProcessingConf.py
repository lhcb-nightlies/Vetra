"""
##############################################################################
#                                                                            #
#  Configuration for the "TELL1Processing" phase of a Vetra job              #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
##############################################################################
"""

# ============================================================================
# Import necessary modules
# ============================================================================
from GaudiKernel.ProcessJobOptions import importOptions
from LHCbKernel.Configuration      import LHCbConfigurableUser
from Configurables                 import ProcessPhase

from VetraOptionsChecker     import optionsChecker


##############################################################################
class VetraTELL1ProcessingConf( LHCbConfigurableUser ):
##############################################################################
    """
    Vetra configuration class for the "TELL1processing" phase
    """
    # Steering options
    __slots__ = {
          'Sequence'        : [ 'VELO' ]
        , 'DataBanks'       : 'NZS+ZS'
        , 'CheckEBs'        : False
        , 'CheckTAEs'       : False
        , 'RunEmulation'    : False
        , 'EmuMode'         : 'Static'
        , 'BitPerfectness'  : False
        , 'PileUp'          : False
        , 'Context'         : 'Offline'
        }
    
    # docstrings for the configurables attributes
    _propertyDocDct = {
          'Sequence'         : """define the TELL1Processing phase sequence(s)"""
        , 'DataBanks'        : """check raw and/or full data banks?"""
        , 'CheckEBs'         : """check the error banks"""
        , 'CheckTAEs'        : """check time-alignment events"""
        , 'RunEmulation'     : """run the TELL1 emulation"""
        , 'EmuMode'          : """emulation mode: 'Static'/'Dynamic' not to/to connect to the VELOCOND database"""
        , 'BitPerfectness'   : """check the bit-perfectness of the TELL1 emulator"""
	, 'PileUp'           : """run the PU decoder and monitoring"""
        , 'Context'          : """top-level context: Offline/HLT environment"""
        }
    
# ============================================================================
    def checkOptions( self ):
# ============================================================================
        """ Check the correctness of the options """
        optionsChecker( 'DataBanks', self.getProp( 'DataBanks' ) )
        optionsChecker( 'EmuMode',   self.getProp( 'EmuMode'   ) )
        optionsChecker( 'Context',   self.getProp( 'Context'   ) )
    
# ============================================================================
    def applyConf( self ):
# ============================================================================
        """ Apply the configuration """
        
        # check the options are OK
        self.checkOptions()
        
        # set up the contents of the phase (typically a set of sequencers)
        ProcessPhase( 'TELL1Processing' ).DetectorList += self.getProp( 'Sequence' )
        
        # set the context for the whole phase
        ProcessPhase( 'TELL1Processing' ).Context = self.getProp( 'Context' )
        
        # define the sequence for the decoding of the VeloRawBuffer
        # note that if dataBanks == 'NONE', no decoding is performed
        dataBanks = self.getProp( 'DataBanks' )
        if   dataBanks == 'NZS' :
            importOptions( '$VETRAOPTS/Velo/VetraDecoding_NZS.py' )
        elif dataBanks == 'ZS' or dataBanks == 'Clusters':
            importOptions( '$VETRAOPTS/Velo/VetraDecoding_ZS.py' )
        elif dataBanks == 'NZS+ZS' :
            importOptions( '$VETRAOPTS/Velo/VetraDecoding_NZS.py' )
            importOptions( '$VETRAOPTS/Velo/VetraDecoding_ZS.py' )
        if 'ProcFull' in dataBanks :
            importOptions( '$VETRAOPTS/Velo/VetraDecoding_PF.py' )

        # decode the error banks, if requested
        if self.getProp( 'CheckEBs' ) :
            importOptions( '$VETRAOPTS/Velo/VetraDecoding_EB.py' )
        
        # define also the decoding for time-alignment studies, if requested
        if self.getProp( 'CheckTAEs' ) :
            importOptions( '$VETRAOPTS/Velo/VetraDecoding_TAE.py' )
        
        # run the TELL1 emulation chain, if requested
        doBPC = self.getProp( 'BitPerfectness' )
        if self.getProp( 'RunEmulation' ) :
            importOptions( '$VETRAOPTS/Velo/VetraTELL1Emulator.py' )
            importOptions( '$VETRAOPTS/Velo/EmulatorConvergenceLimit.py' )
            
        # if in dynamic mode, create appropriate svc and connect to the VELOCOND
        if self.getProp( 'EmuMode' ) == 'Dynamic' :
            importOptions( '$VETRAOPTS/Velo/VeloCondDB.py' )
        
        # check if the bit perfectness run has been chosen
        if doBPC :
            importOptions( '$VETRAOPTS/Velo/VetraDecoding_BPC.py' )

 	# run the PileUp decoding
        doPU = self.getProp( 'PileUp' )
        if doPU :
            importOptions( '$VETRAOPTS/Velo/VetraDecoding_PU.py' )

##############################################################################
