"""
##############################################################################
#                                                                            #
#  Configuration for the "Moni" phase of a Vetra job                         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
##############################################################################
"""

# ============================================================================
# Import necessary modules
# ============================================================================
from GaudiKernel.ProcessJobOptions import importOptions
from LHCbKernel.Configuration      import LHCbConfigurableUser
from Configurables                 import ProcessPhase

from VetraOptionsChecker     import optionsChecker, VetraRuntimeError


##############################################################################
class VetraMoniConf( LHCbConfigurableUser ):
##############################################################################
    """
    Vetra configuration class for the "Moni" phase
    """
    # Steering options
    __slots__ = {
          'Sequence'       : [ 'VELO' ]
        , 'DataBanks'      : 'NZS+ZS'
        , 'PrintOdin'      : True
        , 'CheckEBs'       : False
        , 'CheckTAEs'      : False
        , 'CheckDelayscan' : False
        , 'BitPerfectness' : False
        , 'PileUp'         : False
        , 'PlotRawData'    : False
        , 'Context'        : 'Offline'
        }
    
    # docstrings for the configurables attributes
    _propertyDocDct = {
          'Sequence'         : """define the Moni phase sequence(s)"""
        , 'DataBanks'        : """check raw and/or full data banks?"""
        , 'PrintOdin'        : """print odin info for first event"""
        , 'CheckEBs'         : """check the error banks"""
        , 'CheckTAEs'        : """check time-alignment events"""
        , 'CheckDelayscan'   : """check delay-scan settings"""
        , 'BitPerfectness'   : """check the bit-perfectness of the TELL1 emulator"""
	, 'PileUp'           : """run the PU decoder and monitoring"""
        , 'PlotRawData'      : """specialised monitoring for the raw NZS data"""
        , 'Context'          : """top-level context: Offline/HLT environment"""
        }
    
# ============================================================================
    def checkOptions( self ):
# ============================================================================
        """ Check the correctness of the options """
        optionsChecker( 'DataBanks', self.getProp( 'DataBanks' ) )
        optionsChecker( 'Context',   self.getProp( 'Context'   ) )
        if self.getProp( 'CheckDelayscan' ) :
            if 'NZS' not in self.getProp( 'DataBanks' ) :
                raise VetraRuntimeError( 'CheckDelayscan',
                                         'DataBanks',
                                         'Delay-scan studies require NZS banks.'
                                         )
    
# ============================================================================
    def applyConf( self ):
# ============================================================================
        """ Apply the configuration """
        
        # check the options are OK
        self.checkOptions()
        
        # set up the contents of the phase (typically a set of sequencers)
        ProcessPhase( 'Moni' ).DetectorList += self.getProp( 'Sequence' )

        # set the context for the whole phase
        ProcessPhase( 'Moni' ).Context = self.getProp( 'Context' )
        
        if 'VELO' not in self.getProp( 'Sequence' ): return

        # print odin information for first event, if requested
        if self.getProp( 'PrintOdin' ) :
            importOptions( '$VETRAOPTS/Velo/VetraMoni_Odin.py' )
        
        # data bank(s) on which to run the monitoring
        # note that if dataBanks == 'NONE', no monitoring is performed
        dataBanks = self.getProp( 'DataBanks'      )
        doBPC     = self.getProp( 'BitPerfectness' )
        doPU = self.getProp( 'PileUp' )
        if   dataBanks == 'NZS' :
            if not doBPC :
               importOptions( '$VETRAOPTS/Velo/VetraMoni_NZS.py' )
        elif dataBanks == 'ZS' :
            importOptions( '$VETRAOPTS/Velo/VetraMoni_ZS.py' )
        elif dataBanks == 'Clusters' :
            importOptions( '$VETRAOPTS/Velo/VetraMoni_Clusters.py' )
        elif dataBanks == 'NZS+ZS' :
            if not doBPC :
               importOptions( '$VETRAOPTS/Velo/VetraMoni_NZS.py' )
               importOptions( '$VETRAOPTS/Velo/VetraMoni_ZS.py' )
        
        # run the monitoring on error banks, if requested
        if self.getProp( 'CheckEBs' ) :
            importOptions( '$VETRAOPTS/Velo/VetraMoni_EB.py' )
        
        # define the monitoring for time-alignment studies, if requested
        if self.getProp( 'CheckTAEs' ):
            importOptions( '$VETRAOPTS/Velo/VetraMoni_TAE.py' )
        
        # define the monitoring for Delayscan studies, if requested
        if self.getProp( 'CheckDelayscan' ):
            importOptions( '$VETRAOPTS/Velo/VetraMoni_Delayscan.py' )
        
        # add special monitoring when running BitPerfectness checking
        if doBPC :
            importOptions( '$VETRAOPTS/Velo/VetraMoni_BPC.py' )

	# add special monitoring when running the PU decoder
        if doPU :
            importOptions( '$VETRAOPTS/Velo/VetraMoni_PU.py' )

        # Raw data monitoring
        if self.getProp( 'PlotRawData' ):
            importOptions( '$VETRAOPTS/Velo/VetraMoni_Raw.py' )
##############################################################################
