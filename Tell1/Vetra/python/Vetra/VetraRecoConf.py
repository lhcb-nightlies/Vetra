"""
##############################################################################
#                                                                            #
#  Configuration for the "Reco" phase of a Vetra job                         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    18/03/2010                                                       #
#                                                                            #
##############################################################################
"""

# ============================================================================
# Import necessary modules
# ============================================================================
from Gaudi.Configuration  import INFO
from LHCbKernel.Configuration import LHCbConfigurableUser

from Configurables import ProcessPhase
from Configurables import RecSysConf, TrackSys

from VetraOptionsChecker import optionsChecker


##############################################################################
class VetraRecoConf( LHCbConfigurableUser ):
##############################################################################
    """
    Vetra configuration class for the "Reco" phase
    """
    # Steering options
    __slots__ = {
          'DataType'               : ''
        , 'DataBanks'              : ''
        , 'OutputType'             : ''
        , 'Sequence'               : [ 'VELO', 'TrHLT1', 'Vertex', 'TrHLT2' ]
        , 'TrackPatRecAlgorithms'  : [ 'FastVelo' ]
        , 'SpecialData'            : [ ]
        , 'Context'                : 'Offline'
        , 'CCEscan'                : False
        , 'OutputLevel'            : INFO
        }
    
    # docstrings for the configurables attributes
    _propertyDocDct = {
          'DataType'              : """data type as propagated from Vetra"""
        , 'DataBanks'             : """data bank type as propagated from Vetra"""
        , 'OutputType'            : """output type as propagated from Vetra"""
        , 'Sequence'              : """define the Reco phase sequence(s)"""
        , 'TrackPatRecAlgorithms' : """list of track pattern recognition algorithms to run. See TrackSys configurable"""
        , 'SpecialData'           : """various special data processing options for RecSysConf"""
        , 'Context'               : """top-level context: Offline/HLT environment"""
        , 'CCEscan'               : """CCEscan flag as propagated from Vetra"""
        , 'OutputLevel'           : """the printout level to use (default=INFO)"""
        }
    
    # Possible used configurables
    __used_configurables__ = [ RecSysConf, TrackSys ]
    
# ============================================================================
    def checkOptions( self ):
# ============================================================================
        """ Check the correctness of the options """
        optionsChecker( 'Context',   self.getProp( 'Context' ) )
        # The "Reco" phase ONLY makes sense with ZS banks (clusters, etc.)!
        if self.getProp( 'DataBanks' ) not in [ 'ZS' , 'NZS+ZS', 'Clusters' ] :
            self.setProp( 'Sequence', [] )
        # If not using a Run 2 DataType, revert to the Run 1 reconstruction sequence
        elif (self.getProp( 'DataType' ) not in [ '2015' ] and not self.getProp( 'CCEscan' )):
            self.setProp( 'Sequence', ['VELO', 'Tr', 'Vertex'] )
# ============================================================================
    def applyConf( self ):
# ============================================================================
        """ Apply the configuration """
        
        # check the options are OK
        self.checkOptions()
        
        # set up the contents of the phase (typically a set of sequencers)
        ProcessPhase( 'Reco' ).DetectorList += self.getProp( 'Sequence' )

        # set the context for the whole phase
        ProcessPhase( 'Reco' ).Context = self.getProp( 'Context' )
        
        if 'VELO' not in self.getProp( 'Sequence' ): return

        # configure RecSysConf as appropriate
        self.setOtherProps( RecSysConf(),
                            [ 'DataType',
                              'SpecialData',
                              'Context',
                              'OutputLevel' ]
                            )
        RecSysConf().setProp( 'RecoSequence', self.getProp( 'Sequence' ) )
        
        # configure TrackSys as appropriate
        self.setOtherProps( TrackSys(), [ 'DataType', 'TrackPatRecAlgorithms'] )

##############################################################################
