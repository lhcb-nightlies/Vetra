"""
##############################################################################
#                                                                            #
#  High level configuration for Vetra                                        #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    03/10/2008                                                       #
#                                                                            #
#  Example usage:                                                            #
#  1) To configure a default Vetra job:                                      #
#     from Configurables import Vetra                                        #
#     Vetra()                                                                #
#                                                                            #
##############################################################################
"""

# ============================================================================
# Import necessary modules
# ============================================================================
from Gaudi.Configuration  import INFO
from Configurables                 import ApplicationMgr, GaudiSequencer
from Configurables                 import AuditorSvc
from GaudiKernel.ProcessJobOptions import importOptions, PrintOn
from LHCbKernel.Configuration      import LHCbConfigurableUser
from Configurables                 import LHCbApp
from Configurables                 import CondDB

from VetraTELL1ProcessingConf   import VetraTELL1ProcessingConf
from VetraTELL1ParaTrainingConf import VetraTELL1ParaTrainingConf
from VetraRecoConf              import VetraRecoConf
from VetraMoniConf              import VetraMoniConf
from VetraOutputConf            import VetraOutputConf
from VetraOptionsChecker        import optionsChecker, VetraRuntimeError
from VELOCONDConf               import VELOCONDConf

from Configurables import VetraInit
from Configurables import nzsStreamListener
from Configurables import VeloDataBankFilter
from Configurables import VeloDataBankFilterClient
from Configurables import LoKi__ODINFilter as ODINFilter


##############################################################################
class Vetra( LHCbConfigurableUser ):
##############################################################################
    """
    Vetra main configuration class
    """
    
    # Steering options
    __slots__ = {
          'DataType'         : '2015'
        , 'DataBanks'        : 'NZS+ZS'
        , 'Context'          : 'Offline'
        , 'VELOCONDtag'      : 'HEAD'
        , 'DDDBtag'          : ''
        , 'CondDBtag'        : ''
        # only use for Online running
        , 'UseDBSnapshot'    : False
        , 'PartitionName'    : 'LHCb'
        , 'DBSnapshotDirectory' : '/group/online/hlt/conditions'
        # does it make sense to have UseOracle and UseDBSnapshot = True?
        # not sure at all. Will leave it in as requested. Use it at your own risk!
        , 'UseOracle'        : False
        , 'Simulation'       : False
        , 'RunEmulation'     : False
        , 'EmuMode'          : 'Dynamic'
        , 'EmuConvLimit'     : 0
        , 'FilterBeamBeam'   : False
        , 'MoniDataFilter'   : 'NONE'
        , 'CheckEBs'         : True
        , 'CheckTAEs'        : False
        , 'MagnetOn'         : True
        , 'TEDTracks'        : False
        , 'ParaTraining'     : False
        , 'BitPerfectness'   : False
	, 'PileUp'           : False
	, 'CCEscan'          : False
        , 'OutputType'       : 'NONE'
        , 'HistogramFile'    : 'Vetra_histos.root'
	, "TupleFile"        : 'Vetra_tuple.root' 
        , 'EvtMax'           : -1
        , 'SkipEvents'       : 0
        , 'OutputLevel'      : INFO
        , 'ExpertMoni'       : False
        , 'MainSequence'     :   [ 'ProcessPhase/TELL1Processing',
                                   'ProcessPhase/Reco',
                                   'ProcessPhase/Moni',
                                   'ProcessPhase/Output',
                                   ]
        }

    # Docstrings for the configurables attributes
    _propertyDocDct = {
          'DataType'         : """specify the data type. (default='2015')"""
        , 'DataBanks'        : """check raw and/or full data banks?"""
        , 'Context'          : """top-level context: Offline/HLT environment"""
        , 'VELOCONDtag'      : """tag for the VELOCOND DB"""
        , 'DDDBtag'          : """tag for the detector description DB"""
        , 'CondDBtag'        : """tag for the conditions DB"""
        # only use for Online running
        , 'UseDBSnapshot'    : """Use a snapshot for velo position and rich pressure"""
        , 'PartitionName'    : """Name of the partition when running (needed to find DB: '', 'FEST', or 'LHCb'"""
        , 'DBSnapshotDirectory' : """Local Directory where the snapshot is"""
        , 'UseOracle'        : """use/not use Oracle conditions DB (for online, see also $VETRAOPTS/Velo/Online-Oracle.py)"""
        , 'Simulation'       : """flag whether to use SimCond conditions"""
        , 'RunEmulation'     : """run the TELL1 emulation"""
        , 'EmuMode'          : """emulation mode: 'Static'/'Dynamic' not to/to connect to the VELOCOND database"""
        , 'EmuConvLimit'     : """convergence limit for pedestal training"""
        , 'FilterBeamBeam'   : """filter beam-beam events"""
        , 'MoniDataFilter'   : """filter on presence of NZS banks for ZS monitoring ('ZS'), prescale events for NZS monitoring ('NZS'), or pass through ('NONE'=default)"""
        , 'CheckEBs'         : """check the error banks"""
        , 'CheckTAEs'        : """check time-alignment events"""
        , 'MagnetOn'         : """for tracking with Magnet On"""
        , 'TEDTracks'        : """for tracking with TED style tracks"""
        , 'ParaTraining'     : """special mode of the emulation to evaluate the TELL1 processing parameters"""
        , 'BitPerfectness'   : """check the bit-perfectness of the TELL1 emulator"""
        , 'PileUp'           : """run the PU decoder and monitoring"""
        , 'CCEscan'          : """set the sequence for CCEscan reco"""
        , 'OutputType'       : """set to 'DIGI' to produce an output file"""
        , "HistogramFile"    : """name of output histograms file"""
        , "TupleFile"          : """ Write name of output Tuple file"""
        , 'EvtMax'           : """maximum number of events to process (all by default)"""
        , 'SkipEvents'       : """will skip the first X events"""
        , 'OutputLevel'      : """the printout level to use (default=INFO)"""
        , 'ExpertMoni'       : """the special expert moni algorithms"""
        , 'MainSequence'     : """main Vetra sequence"""
        }

    # Possible used configurables
    __used_configurables__ = [ LHCbApp,
                               VetraTELL1ProcessingConf,
                               VetraRecoConf,
                               VetraMoniConf,
                               VetraOutputConf,
                               VetraTELL1ParaTrainingConf,
                               VELOCONDConf
                               ]
    
# ============================================================================
    def checkOptions( self ):
# ============================================================================
        """ Check the correctness of the options """
        optionsChecker( 'DataBanks'  , self.getProp( 'DataBanks'  ) )
        optionsChecker( 'EmuMode'    , self.getProp( 'EmuMode'    ) )
        optionsChecker( 'Context'   ,  self.getProp( 'Context'    ) )
        optionsChecker( 'OutputType',  self.getProp( 'OutputType' ) )
        if not self.getProp( 'RunEmulation' ) :
            if self.getProp( 'BitPerfectness' ) :
                raise VetraRuntimeError( 'BitPerfectness',
                                         'RunEmulation',
                                         'Bit-perfectness check require that the TELL1 emulation be run.'
                                         )
            if self.getProp( 'ParaTraining' ) :
                raise VetraRuntimeError( 'ParaTraining',
                                         'RunEmulation',
                                         'TELL1 parameters training jobs require that the emulation be run.'
                                         )
        if self.getProp( 'MoniDataFilter' ) == 'ZS':
            if not ( ( self.getProp( 'DataBanks' ) == 'NZS+ZS' ) or ( self.getProp( 'DataBanks' ) == 'ZS' ) or ( self.getProp( 'DataBanks' ) == 'Clusters' ) ):
                raise VetraRuntimeError( 'DataBanks',
                                         'MoniDataFilter',
                                         'MoniDataFilter with option ZS can only be run with ZS data processing.'
                                         )
        elif self.getProp( 'MoniDataFilter' ) == 'NZS':
            if not ( ( self.getProp( 'DataBanks' ) == 'NZS+ZS' ) or ( self.getProp( 'DataBanks' ) == 'NZS' ) ):
                raise VetraRuntimeError( 'DataBanks',
                                         'MoniDataFilter',
                                         'MoniDataFilter with option NZS can only be run with NZS data processing.'
                                         )
    
# ============================================================================
    def defineVetraSequencer( self ):
# ============================================================================
        """ Define the Vetra sequencer(s) tree structure """
        vetraSequencer = GaudiSequencer( 'VetraSequencer' )

        # set the main sequencer(s)
        vetraSequencer.Members = self.getProp( 'MainSequence' )

        # filter beam-beam events, if requested
        if self.getProp( 'FilterBeamBeam' ) :
            filter = ODINFilter( 'FilterBeamBeam' ,
                                 Code = "ODIN_BXTYP == LHCb.ODIN.BeamCrossing" )
            vetraSequencer.Members.insert( 0, filter )
        
        # prepend a "Init" sequencer for the VetraInit algorithm
        vetraSequencer.Members.insert( 0, 'GaudiSequencer/Init' )
        GaudiSequencer( 'Init' ).Members = [   VetraInit()
                                             , nzsStreamListener() ]
        
        # add filter to flag a prescaled amount of NZS events for ZS monitoring, or to prescale events for NZS monitoring
        if self.getProp( 'MoniDataFilter' ) == 'ZS':
            print 'adding MoniDataFilter to Init'
            vdbf = VeloDataBankFilter( 'VeloNZSBankFilter' )
            GaudiSequencer( 'Init' ).Members += [ vdbf ]
        elif self.getProp( 'MoniDataFilter' ) == 'NZS':
            print 'adding MoniDataFilter to Init'
            vdbf = VeloDataBankFilter( 'VeloZSPrescaleFilter' )
            vdbf.FilterNZS = False
            GaudiSequencer( 'Init' ).Members += [ vdbf ]

        ApplicationMgr().TopAlg  = [ vetraSequencer ]
        # note that the' Context' property is actually set by each
        # processing phase, to give more control, if necessary (see below)
        
        # retrieve the names of the main sequencer members
        vetraSequencerNames = [ m[m.rfind('/')+1:] if isinstance( m, str ) else m.name()[m.name().rfind('/')+1:]
                                for m in vetraSequencer.Members ]
        
        return vetraSequencerNames
    
# ============================================================================
    def defineEvents( self ):
# ============================================================================
        """ Pass on some properties to the application manager """
        # simply delegate the work to the LHCbApp configurable
        self.setOtherProps( LHCbApp(),
                            [ 'EvtMax', 'SkipEvents', 'OutputLevel' ] )
    
# ============================================================================
    def defineDBs( self ):
# ============================================================================
        """ Pass on some properties for the database configuration """
        # simply delegate the work to the LHCbApp configurable
        # for the LHCBCOND and DDDB DBs
        self.setOtherProps( LHCbApp(),
                            [ 'DataType', 'Simulation', 'DDDBtag', 'CondDBtag' ]
                            )
        # and let VELOCONDConf set the VELOCOND DB tag
        self.setOtherProp( VELOCONDConf(), 'VELOCONDtag' )
    
# ============================================================================
    def configureDBSnapshot( self ) :
# ============================================================================
        """Configure the database to use the online snapshot"""
        tag = {   'DDDB'     : self.getProp( 'DDDBtag'   )
                , 'LHCBCOND' : self.getProp( 'CondDBtag' )
                , 'SIMCOND'  : self.getProp( 'CondDBtag' )
                , 'ONLINE'   : 'fake'
                  }

        # Set the location of the Online conditions
        from Configurables import MagneticFieldSvc 
        MagneticFieldSvc().UseSetCurrent = True

        
        cdb = CondDB()
        cdb.Tags = tag
        cdb.setProp( 'IgnoreHeartBeat'       , True )
        cdb.setProp( 'EnableRunChangeHandler', True )
        self.setOtherProps( cdb, [ 'UseDBSnapshot',
                                   'DBSnapshotDirectory',
                                   'PartitionName' ]
                            )        
        import socket
        fqdn = socket.getfqdn().split('.')

        if len(fqdn) > 2  and fqdn[1] == 'lbdaq' :
          print "Running on Online network - using Online CondDB"
          cdb.Online = True

        
        
# ============================================================================
    def setOracleEnv( self ):
# ============================================================================
        """ Additional options for accessing the Oracle conditions DB """
        from os import environ
        try :
            if environ[ 'HOSTNAME' ].startswith( 'plus' ) :
                importOptions( '$VETRAOPTS/Velo/Online-Oracle.py' )
        except KeyError :
            pass
    
# ============================================================================
    def defineProperTimeDecoder( self ):
# ============================================================================
        """ Set the proper time (not fake time counter) decoder """
        from Configurables import EventClockSvc
        EventClockSvc().EventTimeDecoder = "OdinTimeDecoder"
    
# ============================================================================
    def defineIO( self ):
# ============================================================================
        """ Settings for persistency and IO """
        # persistency service for IO of MDF files
        from Configurables import EventPersistencySvc
        EventPersistencySvc().CnvServices.append( 'LHCb::RawDataCnvSvc' )
    
# ============================================================================
    def defineHistoPersistency( self ):
# ============================================================================
        """ Settings for the histogram persistency service """
        # ROOT persistency
        importOptions( '$STDOPTS/RootHist.opts' )
        from Configurables import RootHistCnv__PersSvc
        RootHistCnv__PersSvc( 'RootHistCnv' ).ForceAlphaIds = True

        # Set a (for now hard-coded) histogram file name
        if ( self.getProp("HistogramFile") != "" ):
            from Configurables import HistogramPersistencySvc
            HistogramPersistencySvc().OutputFile = self.getProp("HistogramFile")
            print "# Histos file will be ``", self.getProp("HistogramFile"), "''"
        if ( self.getProp("TupleFile") != "" ):
            from Configurables import NTupleSvc
            tupleFile = self.getProp("TupleFile")
            ApplicationMgr().ExtSvc +=  [ NTupleSvc() ]
            tuple = "FILE1 DATAFILE='"+tupleFile+"' TYP='ROOT' OPT='NEW'"
            print "# Tuple will be in ``", tupleFile, "''"
            NTupleSvc().Output = [ tuple ]
            NTupleSvc().OutputLevel = 1 
    
# ============================================================================
    def defineAuditors( self ):
# ============================================================================
        """ Define auditoring services """
        # Time profiling
        ApplicationMgr().ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
        ApplicationMgr().AuditAlgorithms = True
        AuditorSvc().Auditors += [ 'TimingAuditor/TIMER' ] 
    
# ============================================================================
    def __apply_configuration__( self ):
# ============================================================================
        """ Apply the top-level Vetra configuration """
                
        # check the options are OK
        self.checkOptions()
        
        # print the options
        PrintOn()
        
        # define the main sequencer of the Vetra job
        vetraSequencerNames = self.defineVetraSequencer()
        
        # properties for the configuration of the databases
        self.defineDBs()
        # database hacking for online
        if self.getProp( 'UseDBSnapshot' ) : self.configureDBSnapshot()
        # does it make sense to have UseOracle and UseDBSnapshot = True?
        # not sure at all. Will leave it in as requested. Use it at your own risk!
        if self.getProp( 'UseOracle' ) :  self.setOracleEnv()
        
        # define the event time decoder
        self.defineProperTimeDecoder()
        
        # settings for POOL persistency and IO
        self.defineIO()
        
        # settings for the histogram persistency service
        self.defineHistoPersistency()
        
        # pass on event-related properties to the application manager
        self.defineEvents()

        # define auditoring services
        self.defineAuditors()
        
        # configure the TELL1 processing phase
        paraTraining = self.getProp( 'ParaTraining' )
        if 'TELL1Processing' in vetraSequencerNames:
            if not paraTraining:
                self.setOtherProps( VetraTELL1ProcessingConf(),
                                    [ 'DataBanks',
                                      'CheckEBs',
                                      'CheckTAEs',
                                      'RunEmulation',
                                      'EmuMode',
                                      'BitPerfectness',
                                      'PileUp',
                                      'Context',
                                      'OutputLevel' ]
                                    )
            elif paraTraining:
                self.setOtherProps( VetraTELL1ParaTrainingConf(),
                                    [ 'DataBanks',
                                      'RunEmulation',
                                      'EmuMode',
                                      'Context',
                                      'OutputLevel' ]
                                    )
        
        # configure the reconstruction phase
        if 'Reco' in vetraSequencerNames and self.getProp('TEDTracks') == False:
            self.setOtherProps( VetraRecoConf(),
                                [ 'DataType',
                                  'DataBanks',
                                  'OutputType',
                                  'Context',
                                  'CCEscan',
                                  'OutputLevel' ]
                                )
        
        # configure the monitoring phase
        if 'Moni' in vetraSequencerNames:
            if not paraTraining:
                self.setOtherProps( VetraMoniConf(),
                                    [ 'DataBanks',
                                      'CheckEBs',
                                      'CheckTAEs',
                                      'BitPerfectness',
                                      'PileUp',
                                      'Context',
                                      'OutputLevel' ]
                                    )
        
        # configure the output stream phase
        if 'Output' in vetraSequencerNames:
            if not paraTraining:
                self.setOtherProps( VetraOutputConf(),
                                    [ 'OutputType',
                                      'Context',
                                      'OutputLevel' ]
                                    )

        # adding VeloDataBankFilterClient algorithms to apply filtering of ZS/NZS processing/reconstruction/monitoring sequences
	# special mode for VELO offline data quality monitoring
        if self.getProp( 'MoniDataFilter' ) == 'ZS':
            print 'adding clients'
            vdbfc = VeloDataBankFilterClient( 'VeloNZSBankFilterClient' )
            GaudiSequencer( 'Decoding_ZS' ).Members.insert( 0, vdbfc )
            GaudiSequencer( 'RecoVELOSeq' ).Members.insert( 0, vdbfc )
            GaudiSequencer( 'Moni_ZS' ).Members.insert( 0, vdbfc )
            GaudiSequencer( 'VetraSequencer' ).IgnoreFilterPassed = True
            GaudiSequencer( 'TELL1ProcessingVELOSeq' ).IgnoreFilterPassed = True
            GaudiSequencer( 'MoniVELOSeq' ).IgnoreFilterPassed = True
        elif self.getProp( 'MoniDataFilter' ) == 'NZS':
            print 'adding clients'
            vdbfc = VeloDataBankFilterClient( 'VeloZSPrescaleFilterClient' )
            GaudiSequencer( 'Decoding_NZS' ).Members.insert( 0, vdbfc )
            GaudiSequencer( 'TELL1Emulation' ).Members.insert( 0, vdbfc )
            GaudiSequencer( 'Moni_NZS' ).Members.insert( 0, vdbfc )
            GaudiSequencer( 'VetraSequencer' ).IgnoreFilterPassed = True
            GaudiSequencer( 'TELL1ProcessingVELOSeq' ).IgnoreFilterPassed = True
            GaudiSequencer( 'MoniVELOSeq' ).IgnoreFilterPassed = True

##############################################################################
