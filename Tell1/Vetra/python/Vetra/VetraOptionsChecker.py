"""
##############################################################################
#                                                                            #
#  Module for dealing with Vetra configuration errors                        #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    06/10/2008                                                       #
#                                                                            #
##############################################################################
"""

# ============================================================================
# Lists of allowed values for Vetra-specific job option configurations
# ============================================================================
allowedDataBanksValues  = [ 'NZS'     , 'ZS' , 'NZS+ZS', 'ProcFull', 'NONE', 'Clusters' ]
allowedContextValues    = [ 'Offline' , 'HLT'      ]
allowedOutputTypeValues = [ 'NONE'    , 'DIGI'     ]
allowedEmuMode          = [ 'Static'  , 'Dynamic'  ]
allowedParaMode         = [ 'Training', 'Checking' ]
allowedMoniFilterMode   = [ 'NZS', 'ZS', 'NONE'    ]

allowedValues = { 'DataBanks'     : allowedDataBanksValues,
                  'Context'       : allowedContextValues,
                  'OutputType'    : allowedOutputTypeValues,
                  'EmuMode'       : allowedEmuMode,
                  'ParaMode'      : allowedParaMode,
                  'MoniFilterMode': allowedMoniFilterMode
                  }

# definition of handy colours for printing
_default = '\x1b[00m'
_green   = '\x1b[01;32m'
_red     = '\x1b[01;31m'

##############################################################################
class VetraOptionError( ValueError ):
##############################################################################
    """
    Vetra exception class for invalid job option configurations
    """
    
# ============================================================================
    def __init__( self, name, value ):
# ============================================================================
        """ Constructor """
        self.message = 'invalid Vetra "%s" option value specified: %s'\
                       % ( name, value )
    
# ============================================================================
    def __str__( self ):
# ============================================================================
        """ String representation """
        return ( _red + self.message + _default )

##############################################################################
class VetraRuntimeError( RuntimeError ):
##############################################################################
    """
    Vetra exception class for inconsistent job option configurations
    """
    
# ============================================================================
    def __init__( self, optionName1, optionName2, help ):
# ============================================================================
        """ Constructor """
        self.message = 'Options "%s" and "%s" are inconsistent!\n%s'\
                       % ( optionName1, optionName2, help )
    
# ============================================================================
    def __str__( self ):
# ============================================================================
        """ String representation """
        return ( _red + self.message + _default )

##############################################################################
def optionsChecker( name, value ):
##############################################################################
    """ Check the correctness of a configurable option """
    try:
        if value not in allowedValues[ name ]:
            printAllowedJobConfigValues( name )
            print
            raise VetraOptionError( name, value )
    except KeyError:
        print _red, 'You are trying to check an unknown option!', _default
        printAllowedJobConfigValues()

##############################################################################
def optionsChecker_2( configurable, optionName ):
##############################################################################
    """ Check the correctness of a configurable option """
    try:
        value = configurable.getProp( optionName )
        if value not in allowedValues[ optionName ]:
            printAllowedJobConfigValues( optionName )
            raise VetraOptionError( optionName, value )
    except KeyError:
        print _red, 'You are trying to check an unknown option!', _default
        printAllowedJobConfigValues()

##############################################################################
def printAllowedJobConfigValues( optionName=None):
##############################################################################
    """ Print the allowed values for the Vetra-specific
    job option configurations """
    if optionName == None:
        print _green, 'Known job option configurations and allowed values:'
        for name, values in allowedValues.iteritems():
            print _green, name, '\t :', values, _default
    else:
        print _green, 'Allowed values for job option "%s" :' % optionName
        print allowedValues[ optionName ], _default

##############################################################################
