"""
##############################################################################
#                                                                            #
#  Configuration for the VELOCOND DB                                         #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )             #
#  @date    06/08/2010                                                       #
#                                                                            #
##############################################################################
"""

# ============================================================================
# Import necessary modules
# ============================================================================
from Gaudi.Configuration      import getConfigurable, log
from LHCbKernel.Configuration import LHCbConfigurableUser


##############################################################################
class VELOCONDConf( LHCbConfigurableUser ):
##############################################################################
    """
    Configurable class for the VELOCOND DB
    """
    # Steering options
    __slots__ = {
        'VELOCONDtag'      : 'HEAD'
        }

    # Docstrings for the configurables attributes
    _propertyDocDct = {
        'VELOCONDtag'      : """tag for the VELOCOND DB"""
        }

    # Default tag
    __default_tag__ = 'HEAD'
    __all_tags__ = [
        'velo-20110307'
      , 'velo-20100818'
      , 'velo-20100816'
      , 'velo-20100806'
        ]

# ============================================================================
    def __2010_conf__(self):
# ============================================================================
        """
        Default configuration for 2010 data
        """
        from DDDB.Configuration import GIT_CONDDBS
        if 'VELOCOND' in GIT_CONDDBS:
            # get Git CondDB access component
            from Configurables import GitEntityResolver, XmlParserSvc
            from Gaudi.Configuration import appendPostConfigAction
            gitvc = GitEntityResolver('GitVELOCOND',
                                      PathToRepository=GIT_CONDDBS['VELOCOND'])
            gitvc.Ignore = r'^(?!(lhcb\.xml$|VeloCondDB)).*$'
            # FIXME: this is ugly but it's unavoidable until we get
            #        https://its.cern.ch/jira/browse/LHCBPS-1738 fixed
            def inject_git_velocond():
                try:
                    xps = XmlParserSvc()
                    xps.EntityResolver.EntityResolvers.insert(0, gitvc)
                except AttributeError:
                    # ignore non git-based configuration
                    pass
            appendPostConfigAction(inject_git_velocond)
        else:
            gitvc = None
        # get COOL access component
        partition = getConfigurable( 'VELOCOND', 'CondDBAccessSvc' )
        # Set the tag
        tag = self.getProp( 'VELOCONDtag' )
        if tag == 'HEAD' :
            tag = self.__default_tag__
            partition.setProp( 'DefaultTAG', tag )
            if gitvc:
                gitvc.Commit = tag
            log.warning( 'Using default tag %s for partition %s',
                         tag, 'VELOCOND' )
        else :
            partition.setProp( 'DefaultTAG', tag )
            if gitvc:
                gitvc.Commit = tag
            log.warning( 'Using user-specified tag %s for partition %s',
                         tag, 'VELOCOND' )

# ============================================================================
    def applyConf( self ):
# ============================================================================
        """ Apply the configuration """

        # set the actual tag for the VELOCOND DB
        self.__2010_conf__()

##############################################################################
