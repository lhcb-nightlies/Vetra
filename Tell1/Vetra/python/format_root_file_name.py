#########################################################
# a small helper module to prepare input root file name
# --> T. Szumlak
# -------------------------------------------
# --> this script is a part of Vetra project
# -------------------------------------------
#########################################################

#
Success=1
Failure=0

#
def formatRootFileName(rawName):
    rootName=str()
    rootFileStatus=Success
    nameTab=rawName.split(".")
    if nameTab[0].isdigit():
      print " --> You passed a number as a file name! Correct this "
      rootFileStatus == Failure
    if rootFileStatus:
      if len(nameTab) == 1:
        rootName=nameTab[0]+'.root'
      else:
        rootName=rawName
    #
    return ( rootName )

#
# --> main sequence
if __name__ == '__main__':
    prepareRootFileName('rootName')
