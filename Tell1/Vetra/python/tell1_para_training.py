#############################################################
# main module to run training for:
# pedestals
# clustering thresholds (hit and inclusion)
# Beetle X-Talk correction parms
# Beetle X-Talk correction thresholds
# NOTE - the format of these is compatible with the new
#        restructurised VELOCOND DB
#
# T. Szumlak @ 08/04/2011
#
# --------------------------------------------
# this script is a part of the VETRA project
# --------------------------------------------
#
#############################################################

# --> system modules
import time, datetime
import os, sys

# Define exit status code
SUCCESS = 0
FAILURE = 1
HELP_STATUS = 0
allowed_db_schemes = ['serial', 'sensor']

# --> print help messages
def _help_():
    print " --> This script should be run with one argument like below: "
    print " #>pyton tell1_para_training.py -e[--events] 5000 "
    print " NOTE: "
    print "   - This argument spcecifies how many events you would like  "
    print "     to run over, remember - basic training cycle is 4048, you need "
    print "     a few hundred more to accumulate enough statistics to create "
    print "     the control histograms, it is recommended to use 5 or 6 k "
    print "    ----------------------------------------------------------------- "
    print "     NOTE! if your file contains round robin you may need to put "
    print "     much more to get effectively around 5 or 6 k NZS banks for each"
    print "     sensor "
    print "    ----------------------------------------------------------------- "
    print "   - You can print this info using -h[--help] option "
    HELP_STATUS = 1

# --> check paths and process input arguments
def _init_():
    import getopt, sys, os
    options = []
    # -> check if the Vetra env is set properly
    try:
       vetra_env = os.environ['VETRAROOT']
    except KeyError:
        print ' --> You need to set VETRA environment first! '
        sys.exit(2)

    _line_arg_ = sys.argv[1:]
    _events_ = str()
    
    try:
        opts, args = getopt.getopt(_line_arg_, 'he:')
    except getopt.GetoptError:
        print ' --> An unknown/bad option has been passed! '
        help()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            help()
        elif opt in ('-e', '--events'):
            _events_ = arg
        else:
            assert False, " --> Unknown option! "
            help()
            exit(2)
            
    _except_ = [ None, 'None', '', '0', ' ' ]
    if _events_ in _except_:
        print " --> number of events not given! "
        exit(2)
    else:
        if _events_.isdigit():
            options.append(int(_events_))
        else:
            print " --> number of events must be integer object! "
            exit(2)

    return ( options )

# --> run the processing
def main_tell1_training():
    opt_list = _init_()
    if len(opt_list):
        print ' --> Checking environment and running job... '
        # Vetra root env var
        VETRAROOT = os.environ['VETRAROOT']
        if None == VETRAROOT:
            print ' --> You need to set the VETRA env first! '
            exit(2)
        # pytnon modules location
        PYTHON_PATH = VETRAROOT+'/python'
        # add local python dir 
        sys.path.append(PYTHON_PATH)
        import processingParaTraining as ParaTrainer
        import tell1_params_processor as CondProcessor
        # time stamp - critical - should be the same for all the files
        import time_stamp as TimeStamp
        time_stamp=TimeStamp.createTimeStamp()
        print time_stamp
        # --> main training seq
        print " ---------------------------------------------------------------- "
        print " -->   Executing Training Sequence using ", opt_list[0], " events "
        print " ---------------------------------------------------------------- "
        # --> create a global time stamp for the current run
        ParaTrainer.runTraining(opt_list[0], time_stamp)
        CondProcessor.main_tell1_para_processor(time_stamp)


# ----------
# -- MAIN --
# ----------
if __name__ == '__main__':
    main_tell1_training()
