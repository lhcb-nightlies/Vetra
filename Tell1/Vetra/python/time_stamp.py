#-------------------------------------------------------
#
# simple module to create a time stamp
#
# T. Szumlak
#
#-------------------------------------------------------

# --> standard modules
import time, datetime

def createTimeStamp():
    TIME_STAMP=datetime.datetime.now().strftime("%d_%m_%Y-%H.%M.%S")
    return TIME_STAMP

if __name__ == '__main__':
    timestamp=createTimeStamp()
    print " Generated time stamp: ", timestamp
