#---------------------------------------------------------
# This script is used to write an xml file with conditions
# for IT TELL1 boards. Based on T. Szumlak's script for
# Velo conditions.
#
# Vetra/python
#---------------------------------------------------------

# --> define the Xml headers
head0='<?xml version="1.0" encoding="ISO-8859-1"?>'
head1='<!DOCTYPE DDDB SYSTEM "../DTD/structure.dtd">'

# --> DDDB tag
DDDBTag="<DDDB>"
endDDDBTag="</DDDB>"

# --> open the Xml file and write out the data
XmlFileName='../VetraCondDB/IT/CondDB/TELL1Cond.xml'
XmlFile=open(XmlFileName, 'w')

# --> condition tag
condTag='<condition classID="5" name="'
endCondTag='</condition>'

# --> condition value types
intType=' type="int">'
doubleType=' type="double">'
stringType=' type="string">'

# --> condition tags
condANumberTag=' '*2+'<param name='
condAVecTag=' '*2+'<paramVector name='
endANumberTag=' '*2+'</param>'
endAVecTag=' '*2+'</paramVector>'
# --> TAG definitions --

# --> condition names
Pedestal_enable='"Pedestal_enable"'
Update_enable='"Update_enable"'
Header_enable='"Header_enable"'
CMS_enable='"CMS_enable"'
ZS_enable='"ZS_enable"'
PP_max_clusters='"PP_max_clusters"'
Header_threshold='"Header_threshold"'
#Header_correction='"Header_correction"'
SpillOver_threshold='"SpillOver_threshold"'
Confirmation_threshold='"Confirmation_threshold"'
CMS_threshold='"CMS_threshold"'
Hit_threshold='"Hit_threshold"'
Pedestal_mask='"Pedestal_mask"'
Pedestal_value='"Pedestal_value"'
Disable_beetles_0_11='"Disable_beetles_0_11"'
Disable_beetles_12_23='"Disable_beetles_12_23"'
Disable_links_per_beetle_0_11='"Disable_links_per_beetle_0_11"'
Disable_links_per_beetle_12_23='"Disable_links_per_beetle_12_23"'

# --> list of names
condNamesANumber=[Pedestal_enable, Update_enable, Header_enable, CMS_enable,
                  ZS_enable, PP_max_clusters]
condNamesAVector=[Header_threshold, SpillOver_threshold,
                  Confirmation_threshold, CMS_threshold, Hit_threshold,
                  Pedestal_mask, Pedestal_value,
                  Disable_beetles_0_11, Disable_beetles_12_23]

for i in range(96):
    j='%02i' %i
    Header_correction = '"'+'Header_correction_analog_link_'+str(j)+'"';
    condNamesAVector.append(Header_correction);
    
condNamesAStringVector=[Disable_links_per_beetle_0_11,
                        Disable_links_per_beetle_12_23]

# --> condition values - this may be changed - development/testing
# --> pedestal enable
Pedestal_enable_val=str(1)
# --> pedestal update enable
Update_enable_val=str(1)
# --> header correction enable
Header_enable_val=str(1)
# --> lcms enable
CMS_enable_val=str(1)
# --> zs enable
ZS_enable_val=str(1)
# --> max clusters
PP_max_clusters_val=str(255)
# --> beetle header correction thresholds
Header_threshold_low=128   # low header threshold
Header_threshold_high=128  # high header threshold
Header_threshold_val = ' '+str(Header_threshold_low)+' '+str(Header_threshold_high)

# --> header correction initial value
Header_correction_lowlow=0
Header_correction_lowhigh=0
Header_correction_highlow=0
Header_correction_highhigh=0
Header_correction_val = str()

Header_correction_val += ' '+str(Header_correction_lowlow)+' '+str(Header_correction_lowhigh)+' '+str(Header_correction_highlow)+' '+str(Header_correction_highhigh)

# --> beetle disable
init=0
Disable_beetles_0_11_val=str()
Disable_beetles_12_23_val=str()
for n in range(12):
 Disable_beetles_0_11_val +=(' '+str(init))
 Disable_beetles_12_23_val +=(' '+str(init))
# --> link disable
init=str(0x0)
Disable_links_per_beetle_0_11_val=str()
Disable_links_per_beetle_12_23_val=str()
for n in range(12):
 Disable_links_per_beetle_0_11_val +=(' '+init)
 Disable_links_per_beetle_12_23_val +=(' '+init)

# --> pedestal mask
init=0
Pedestal_mask_val=str()
for n in range(3072):
 Pedestal_mask_val+=(' '+str(init))
# --> pedestal value
init=128
Pedestal_value_val=str()
for n in range(3072):
 Pedestal_value_val+=(' '+str(init))
# --> hit thresholds values
init=5
Hit_threshold_val=str()
for n in range(3072):
 Hit_threshold_val+=(' '+str(init))
# --> confirmation threshold values
init=8
Confirmation_threshold_val=str()
for n in range(48):
 Confirmation_threshold_val+=(' '+str(init))
# --> spillover threshold values
init=20
SpillOver_threshold_val=str()
for n in range(48):
 SpillOver_threshold_val+=(' '+str(init))
# --> CMS threshold values
init=8
CMS_threshold_val=str()
for n in range(3072):
 CMS_threshold_val+=(' '+str(init))

# --> list of conditions values
condValuesANumber=[Pedestal_enable_val, Update_enable_val, Header_enable_val, CMS_enable_val,
                  ZS_enable_val, PP_max_clusters_val]
condValuesAVector=[Header_threshold_val, SpillOver_threshold_val,
                   Confirmation_threshold_val, CMS_threshold_val, Hit_threshold_val,
                   Pedestal_mask_val, Pedestal_value_val,
                   Disable_beetles_0_11_val, Disable_beetles_12_23_val]

for i in range(96):
    condValuesAVector.append(Header_correction_val);

condValuesAStringVector=[Disable_links_per_beetle_0_11_val,
                         Disable_links_per_beetle_12_23_val]

# --> TELL1Boards IT
TELL1=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
       32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45,
       64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77]

# ------------------------------------
# --> creating the Xml file's content
# ------------------------------------
# --> Header of the Xml
print >> XmlFile, head0
print >> XmlFile, head1
# --> <DDDB>
print >> XmlFile, DDDBTag
# --> TELL1 conditions -- loop over TELL1s
for n in range(len(TELL1)):
  # --> Condition collection Tag
  # --> dynamic name of the condition
  # --> <condition ... >
  condTarget="TELL1Board"+str(TELL1[n])+'">'
  print >> XmlFile, ' '+condTag+condTarget
  # --> conditions - single number first -- loop 1
  for n in range(len(condNamesANumber)):
    print >> XmlFile, condANumberTag+condNamesANumber[n]+intType
    print >> XmlFile, ' '*3+condValuesANumber[n]
    print >> XmlFile, endANumberTag
  # --> conditions - vectors -- loop 2
  for n in range(len(condNamesAVector)):
    print >> XmlFile, condAVecTag+condNamesAVector[n]+intType
    print >> XmlFile, ' '*2+condValuesAVector[n]
    print >> XmlFile, endAVecTag
  # --> conditions - string vectors -- loop 3
  for n in range(len(condNamesAStringVector)):
    print >> XmlFile, condAVecTag+condNamesAStringVector[n]+stringType
    print >> XmlFile, ' '*2+condValuesAStringVector[n]
    print >> XmlFile, endAVecTag
  # --> </condition>
  print >> XmlFile, ' '+endCondTag
# --> </DDDB>
print >> XmlFile, endDDDBTag

# --> close the Xml file
XmlFile.close()

