####################################################################
#
# Small helper module - it can extract tell1/sensor number from
# the tell1 condition name
#
# --> T. Szumlak
# -------------------------------------------
#   this script is a part of Vetra project
# -------------------------------------------
#
# NOTE - there are some assumption made here - this module is
# suppose to work in concert with scripts responsible for VELOCOND
# data base building/updating, in particular we assume that the
# condition name is of form "VeloTELL1Boardxxx", where xxx is the
# number to be extracted
# 
####################################################################

# --> find the number of a current tell1 board
def getTell1Number(name, print_str = 'N'):
    # --> cond names length
    COND_1_DIG = 15
    COND_2_DIG = 16
    COND_3_DIG = 17
    # --> take care of 1, 2 and 3 digit tell1 numbers
    hist_num = str()
    if len(name) == COND_1_DIG:
        hist_num = name[-1:]
    elif len(name) == COND_2_DIG:
        hist_num = name[-2:]
    elif len(name) == COND_3_DIG:
        hist_num = name[-3:]
    else:
       if print_str == 'Y' or print_str == 'y':
         print " --> Not one of the VeloTell1Cond - ", name
       return None
    return hist_num
