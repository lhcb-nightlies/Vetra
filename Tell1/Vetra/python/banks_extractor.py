####################################################################
#
# This module consists of a number of functions that are needed
# to extract the updated parameters after the training from a root
# file
#
# --> T. Szumlak @09/04/2011
# ---------------------------------------------
#   this script is a part of the Vetra project
# ---------------------------------------------
#
# NOTE - it is assumed throughout this script that the default
# location of the root files is $VETRAROOT/job
# 
####################################################################

import commands, os
from ROOT import TFile
USER_NAME = os.environ['USER']
command = ''
# --> trick necessary for ROOT histograms
FIRST_BIN = 1

# --> create string out of a tell1 number
def convert_tell1_2_macro_string(tell1_number):
   ZERO = '0'
   # --> passed argument is an integer number, convert to string
   if tell1_number != None:
      macro_string = str(tell1_number)
      if len(macro_string) != 0:
         if len(macro_string) == 1:
            macro_string = ( 2 * ZERO ) + macro_string
         elif len(macro_string) == 2:
            macro_string = ZERO + macro_string
         elif len(macro_string) > 3:
            print ' --> Passed tell1 number out of the allowed range'
            macro_string = None
      else:
         print ' --> The tell1 number is wrong! '
         macro_string = None
   #
   return macro_string

# --> get updated pedestal banks from root file
def getPedBanks(tell1_list, time_stamp):
  print " --> Building banks with pedestals "
  PED_SUM_SHIFT = 10
  if USER_NAME != 'velo_user':
    command = 'ls -rt $VETRAROOT/job/Tell1_Params_Storage*.root | tail -1'
  else:
    path = '/calib/velo/tell1_params/TELL1_' + time_stamp
    command = 'ls -rt ' + path + '/Tell1_Params_Storage*.root | tail -1'
  ped_bank_storage = commands.getoutput(command)
  if ped_bank_storage != None:
    print " --> The newest file with pedestal banks: ", ped_bank_storage
    hist_storage = TFile(ped_bank_storage)
    ped_stash = {}
    banks = {}
    ped_dir = hist_storage.Get('Vetra/VeloPedestalSubtractorMoni')
    if ped_dir == None:
       print ' --> There is no folder with pedestal banks! '
       return None
    for tell1 in tell1_list:
      tell1_str = convert_tell1_2_macro_string(tell1)
      ped_stash[tell1] = ped_dir.Get('TELL1_' + tell1_str + '/Pedestal_Sum')
    if ped_stash != None:
      for tell1 in tell1_list:
        if ped_stash[tell1] != None:
          hist_size = ped_stash[tell1].GetNbinsX()
          sum_vec = str()
          for n in range(hist_size):
	    val = int(ped_stash[tell1].GetBinContent(n+1))
            val = val >> PED_SUM_SHIFT
            sum_vec += " " + str(val)
          banks[tell1] = sum_vec
        else:
          print " --> Failed to process pedestal condition for the tell1: ", tell1
          print " --> DB update is terminated "
          return banks
    else:
      print " --> There are no pedestal bank histos! "
  else:
    print " --> No Pedestal Bank reposytory has been found!"
  return banks

# --> get updated cluster seeding thresholds
def getCluThresholds(tell1_list, time_stamp):
  print " --> Building banks with clusterisation thresholds "
  SEED_FACTOR = 6.0
  INC_TH_FACTOR = 0.4
  MAX_TH_SEED = 127
  if USER_NAME != 'velo_user':
    command = 'ls -rt $VETRAROOT/job/Tell1_Params_Storage*.root | tail -1'
  else:
    path = '/calib/velo/tell1_params/TELL1_' + time_stamp
    command = 'ls -rt ' + path + '/Tell1_Params_Storage*.root | tail -1'
  seed_th_storage=commands.getoutput(command)
  print " --> The newest file with clustering thresholds: ", seed_th_storage
  if seed_th_storage != None:
    hist_storage = TFile(seed_th_storage)
    th_seed_stash = {}
    th_seed_banks = {}
    th_incl_banks = {}
    th_dir = hist_storage.Get('Vetra/VeloClusteringThresholdsComputer')
    if th_dir == None:
       print ' --> Problem accessing thresholds banks! '
       return None
    for tell1 in tell1_list:
      tell1_str = convert_tell1_2_macro_string(tell1)
      th_seed_stash[tell1] = th_dir.Get('TELL1_' + tell1_str + '/Noise_RMS')
    if th_seed_stash != None:
      for tell1 in tell1_list:
        if th_seed_stash[tell1] != None:
          hist_size=th_seed_stash[tell1].GetNbinsX()
          th_seed_vec=str()
          th_incl_vec=str()
          for n in range(hist_size):
            th_seed=th_seed_stash[tell1].GetBinContent(n+1)
            if th_seed <= 1.:
              th_seed=MAX_TH_SEED
              th_incl=th_seed
            else:
              th_seed*=SEED_FACTOR
              th_incl=th_seed*INC_TH_FACTOR
            th_seed_vec+=" "+str(int(round(th_seed)))
            th_incl_vec+=" "+str(int(round(th_incl)))
          th_seed_banks[tell1]=th_seed_vec
          th_incl_banks[tell1]=th_incl_vec
        else:
          print " --> Faild to process threshold condition for the tell1: ", tell1
          print " --> DB update is terminated "
          return th_seed_banks, th_incl_banks
    else:
      print " --> There are no noise histos! "
      return th_seed_banks, th_incl_banks
  else:
    print " --> No Pedestal Bank reposytory has been found!"
  return th_seed_banks, th_incl_banks

# --> get Beetle Header XTalk correction parameters
def getBHXTalkCorrections(tell1_list, time_stamp):
  print " --> Building banks with BHX-T corrections and thresholds "
  FIRST_BIN = 1
  STEP = 3
  if USER_NAME != 'velo_user':
    command = 'ls -rt $VETRAROOT/job/Tell1_Params_Storage*.root | tail -1'
  else:
    path = '/calib/velo/tell1_params/TELL1_' + time_stamp
    command = 'ls -rt ' + path + '/Tell1_Params_Storage*.root | tail -1'
  beetle_xtalk_corr_storage=commands.getoutput(command)
  print " --> The newest file with BHXT params: ", beetle_xtalk_corr_storage
  if beetle_xtalk_corr_storage != None:
    corr_storage = TFile(beetle_xtalk_corr_storage)
    h_corr_stash = {}
    l_corr_stash = {}
    corr_banks = {}
    corr_vec = str()
    header_th_stash = {}
    header_th_banks = {}
    header_th_vec = str()
    corr_dir = corr_storage.Get('Vetra/VeloBeetleHeaderXTalkComputer')
    if corr_dir == None:
       print ' --> The BHXT computer algorithm not activated '
       return None
    # --> take the trained coefficients form the input file
    for tell1 in tell1_list:
      # --> here we fetch the BHXT correction parameters
      name = '/Mean_Values_'
      tell1_str = convert_tell1_2_macro_string(tell1)
      h_corr_stash[tell1] = corr_dir.Get('TELL1_' + tell1_str + name + 'H')
      l_corr_stash[tell1] = corr_dir.Get('TELL1_' + tell1_str + name + 'L')
      # --> and here the header thresholds
      name = 'HeaderThresholds/Global_Header_Thresholds_Sens_'
      header_th_stash[tell1] = corr_dir.Get(name + str(tell1))
    # --> retrieve/process the correction parameters
    if h_corr_stash != None and l_corr_stash !=None:
      for tell1 in tell1_list:
        if h_corr_stash[tell1] != None and l_corr_stash[tell1] != None:
          hist_size_h = h_corr_stash[tell1].GetNbinsX()
          hist_size_l = l_corr_stash[tell1].GetNbinsX()
          if hist_size_h == hist_size_l:
            corr_vec = str()
            for entry in range(FIRST_BIN, hist_size_h, STEP):
              coeff_h = h_corr_stash[tell1].GetBinContent(entry)
              coeff_l = l_corr_stash[tell1].GetBinContent(entry)
	      # 1.15 is tunning that seems to give better results with current
	      # settings - x-talk values are very small ~ 1ADC count
              corr_vec += ' ' + str(int(round(1.15*coeff_l)))
              corr_vec += ' ' + str(int(round(1.15*coeff_h)))
            corr_banks[tell1] = corr_vec
          else:
            print " --> Size of the BHXT coeff vector is not the same for H and L states "
        else:
          print " --> Problem with the BHXT parameters for the Tell1: ", tell1
          return corr_banks, header_th_banks
    else:
      print " --> Problem with retrieving the processing parameters for BHXT "
      return corr_banks, header_th_banks
    # --> retrieve/process the header thresholds
    if header_th_stash != None:
      for tell1 in tell1_list:
        if header_th_stash[tell1] != None:
	  header_th_vec = str()
	  header_mean = header_th_stash[tell1].GetMean()
	  header_th_h = int(round(header_mean+6.))
	  header_th_l = int(round(header_mean-6.))
	  header_th_vec = ' ' + str(header_th_h) + ' ' + str(header_th_l)
	  header_th_banks[tell1] = header_th_vec
        else:
          print " --> Cannot find header thresholds for tell1: ", tell1
	  return corr_banks, header_th_banks
    else:
      print " --> Cannot retrieve header thresholds "
      return corr_banks, header_th_banks
  else:
    print " --> BHXT parameters repository is empty! "
    return corr_banks, header_th_banks
  return corr_banks, header_th_banks
