#############################################################
# This script is meant to generate base line XML tree with
# VELO conditions - Tell1 processing parameters at the moment
# it can be updated to contain more parameters like the
# Beetle settings, etc
# 
# --> T. Szumlak
# -------------------------------------------
# --> this script is a part of Vetra project
# -------------------------------------------
# 
# This should be essentially used with caution since can
# overwrite any existing database! Essentially  should be
# use do generate a XML tree with some default parameters
# (not very sensible sensorwise) and next updated with
# values calculated during the training cycle
#
# usage - described in help function
# when crating conditions tree for full VELO setup one needs
# to provide a time stamp (TS) and numbering scheme, the TS
# will be generated automatically so the script should be
# executed as:
# python create_base_line_bd.py 'sensor' ['serial']
# remember to set Vetra environment first
#############################################################

# get standard modules
import os, sys

Success=0
Failure=1

def help():
   print " --> You need to provide a numbering scheme for the TELL1 mapping!"
   print " Possible schemes are: "
   print "  1) 'serial' - older numbering scheme - serial number is used for TELL1 id"
   print "  2) 'sensor' - software sensor number is used for TELL1 id"
   print "     e.g writeBaseLineCondTree(timeStamp, 'serial') "
   print "  3) 'generated' - special case when generated data are used"
   print " --> If you are going to use generated data you need to provide "
   print "     the tell1 number the generated data has been taken with "
   print "     e.g. writeBaseLineCondTree(timeStamp, 'generated', 'R', 10) "
   return 

Status=Success
type=str()
sensor=str()
tell1=-999

knownNumberingSchemes=['sensor', 'serial', 'generated']
sensorTypes=['R', 'Phi']

if len(sys.argv) == 1:
   print " --> No arguments have been given! "
   help()
   Status=Failure
else:
   if len(sys.argv) == 2:
      print " --> You asked to generate base line XML conditions tree "
      scheme=sys.argv[1]
      if scheme not in knownNumberingSchemes:
         print " --> You asked for unknown numbering scheme! "
         print " --> Allowed schemes are: ", knownNumberingSchemes
         Status=Failure
      elif scheme == 'generated':
         print " --> You asked to create condition tree for generated data"
         print "     but you did not provide enough information - check help!"
         Status=Failure
      else:
         type=scheme
   if len(sys.argv) > 2:
      print " --> You asked to generate a XML conditon for generated data "
      scheme=sys.argv[1]
      if scheme != 'generated':
         print " --> You should use scheme type - generated "
         Status=Failure
      else:
         type=scheme
      sensType=sys.argv[2]
      if sensType not in sensorTypes:
         print " --> Wrong sensor type name! "
         print " --> Allowed types are: ", sensorTypes
         Status=Failure
      else:
         sensor=sensType
      if len(sys.argv) != 4:
         print " --> You need also to specify used Tell1 number! "
         Status=Failure
      else:
         tell1=sys.argv[3]

try:
   VETRAROOT=os.environ['VETRAROOT']
except:
   print " --> No $VETRAROOT env variable has been found! "
   print "     Set VETRA environment first! "
   VETRAROOT=None

if VETRAROOT == None:
   print " --> Vetra environment is not set - Aborting! "
else:
  if Status == Success:
     # define local python directory
     LOCAL_PYTHON_PATH=VETRAROOT+'/python'
     sys.path.append(LOCAL_PYTHON_PATH)
     # import necessary modules
     import write_base_line_cond_tree as CondTreeWriter
     import time_stamp as TimeStamp
     timeStamp=TimeStamp.createTimeStamp()
     if tell1 < 0 and scheme == 'generated':
        print " --> The tell1 number is not set"
        print " --> Your Input: ", timeStamp, type, sensor, tell1
     else:
        if scheme != 'generated':
           sensor = ''
           tell1 = 0
           CondTreeWriter.writeBaseLineCondTree(timeStamp, type)
        else:
           CondTreeWriter.writeBaseLineCondTree(timeStamp, type, sensor, tell1)
        createDB='source '+VETRAROOT+'/scripts/create_sqlite_file_from_xml.sh'
        print " --> Trying to create the sqlite file "
        os.system(createDB)
  elif Status == Failure:
     print " --> Could not created database due to problems with arguments! "
