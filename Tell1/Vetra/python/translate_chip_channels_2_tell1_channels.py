####################################################################
#
# This is a helper module to scramble the data chip channel ordered
# to get the tell1 channel order. This is essential for the process.
# parameters such pedestals or Beetle Header X-Talk correction
# coefficients 
#
# --> T. Szumlak
# -------------------------------------------
#   this script is a part of Vetra project
# -------------------------------------------
#
# In order to use this module one needs to prvide an array of data
# chip channel ordered and number of the processing parameters
# needed per one processing unit (PP_FPGA)
# 
#####################################################################

def scramble2Tell1Chan(data_tab, params_per_proc):
   FPGA_PROC = 4
   scrambled_data = {}
   for proc in range(FPGA_PROC):
      temp_str = str()
      for channel in range(params_per_proc*proc, params_per_proc*(proc+1)):
         temp_str += str(data_tab[channel])
         if channel < params_per_proc*(proc+1)-1:
            temp_str += ' '
      scrambled_data[proc] = temp_str
      
   scrambling_pattern = [3, 2, 1, 0]
   data = str()
   
   for sector in range(len(scrambling_pattern)):
      data += scrambled_data[(scrambling_pattern[sector])]
      if sector < len(scrambling_pattern) - 1:
        data += ' '
        
   if len(data) == 0:
     print " --> FATAL problem with data scrambling! "
     return None
   return data
