"""
#########################################################
# This script runs pedestal subtraction algorithm
# determine pedestal bank and updates VELOCOND database
# --> T. Szumlak
# -------------------------------------------
# --> this script is a part of Vetra project
# -------------------------------------------
# This script (or its updated development versions)
# is meant to be run in the pit during the data taking
#########################################################
"""

import os, sys
import commands
import GaudiPython as Gaudi
from Gaudi.Configuration import importOptions, HistogramPersistencySvc
from GaudiKernel.Configurable import appendPostConfigAction

def runTraining(evtNumber, time_stamp):
  # create a time stamp for a root file
  print " --> Creating root file with time stamp "
  FILE_NAME = 'Tell1_Params_Storage'
  ROOT_NAME = FILE_NAME + '_' + time_stamp + '.root'
  USER_NAME = os.environ['USER']
  print " --> NEW name is: ", ROOT_NAME
  
  # create and configure an instance of application manager
  importOptions('$VETRAOPTS/Velo/Vetra-ProcessingParaTraining.py')
  appMgr=Gaudi.AppMgr(outputlevel=3)

  # get basic services
  evt=appMgr.evtSvc()

  #
  appMgr.run(evtNumber)
  appMgr.exit()

  # --> redefine output accordingly
  if USER_NAME != 'velo_user':
    command = 'ls -rt $VETRAROOT/job/Tell1_Params_Storage.root | tail -1'
    file = commands.getoutput( command )
    os.system('mv ' + file + ' ' + '$VETRAROOT/job/' + ROOT_NAME)
  elif USER_NAME == 'velo_user':
    if not os.path.exists( '/calib/velo/tell1_params' ):
      print ' --> It seems that you are velo_user operator '
      print '     but the main storage at /calib/velo/tell1_params'
      print '     is missing!'
      exit(2)
    path = '/calib/velo/tell1_params/TELL1_' + time_stamp
    command = 'mkdir ' + path
    os.system( command )
    WDIR = commands.getoutput('pwd')
    command = 'ls -rt ' + WDIR + '/Tell1_Params_Storage.root | tail -1'
    file = commands.getoutput( command )
    os.system('mv ' + file + ' ' + path)
  
# at this stage we should have output root file
