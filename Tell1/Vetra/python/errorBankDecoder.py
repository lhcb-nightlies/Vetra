#########################################################
# reading raw data from mdf file - check file content
# --> T. Szumlak
# --> this script is a part of Vetra project
# the script is to demonstrate what is the structure
# of the mdf Velo file
#########################################################

import GaudiPython as Gaudi

appMgr=Gaudi.AppMgr(outputlevel=3)
appMgr.config(files=['$STDOPTS/LHCbApplication.opts',
                     '$STDOPTS/RawDataIO.opts',
                     '$STDOPTS/DigiDicts.opts',
                     '$STDOPTS/DecodeRawEvent.opts'],
              options=['EventSelector.Input += {"DATAFILE=\'rfio:/castor/cern.ch/grid/lhcb/data/2008/RAW/VELO/PHYSICSTP/28355/028355_0000064049.raw\' SVC=\'LHCb::MDFSelector\'"}']
#              options=['EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/data/physics/030933_0000077704.raw\' SVC=\'LHCb::MDFSelector\'"}']              
#              options=['EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/data/physics/030929_0000077696.raw\' SVC=\'LHCb::MDFSelector\'"}']
#              options=['EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/data/commissioning/clusterGeneration_r_080828_1450.dat\' SVC=\'LHCb::MDFSelector\'"}']
#              options=['EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/data/commissioning/031010_0000078322.raw\' SVC=\'LHCb::MDFSelector\'"}']
#              options=['EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/data/commissioning/028348_0000064042.raw\' SVC=\'LHCb::MDFSelector\'"}']
#              options=['EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/data/commissioning/025980_0000061221.raw\' SVC=\'LHCb::MDFSelector\'"}']
#              options=['EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/data/commissioning/clusterGeneration_phi_080814_1101.dat\' SVC=\'LHCb::MDFSelector\'"}']
#              options=['EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/data/commissioning/029646_0000070334.raw\' SVC=\'LHCb::MDFSelector\'"}']
              )

evt=appMgr.evtsvc()
selector=appMgr.evtSel()
selector.PrintFreq=1

# run the stuff
for i in range(10):
  appMgr.run(1)
  raw=evt['/Event/DAQ/RawEvent']
  rawPrev1=evt['/Event/Prev1']
  #help(rawPrev1)
  rawBank=Gaudi.gbl.LHCb.RawBank
  # make sure you add stl dicts
  Gaudi.loaddict('STLRflx')
  # we are interested in following banks form the raw file
  velobank=Gaudi.gbl.std.vector('LHCb::RawBank *')()
  velofullbank=Gaudi.gbl.std.vector('LHCb::RawBank *')()
  veloerrorbank=Gaudi.gbl.std.vector('LHCb::RawBank *')()
  # the banks positions
  velo=8
  velofull=18
  veloerror=29
  # get them
  # ZS bank
  velobank=raw.banks(velo)
  if velobank.size()==0:
      print " --> There is no Zero Suppressed bank in the input FILE "
  else:
      print " --> Velo RawBank vector size: ", velobank.size()
  # NZS bank
  velofullbank=raw.banks(velofull)
  if velofullbank.size()==0:
      print " --> There is no Non-Zero Suppressed bank in the input FILE "
  else:
      print " VeloFull RawBank vector size: ", velofullbank.size()
      print " Src Id: ", velofullbank[0].sourceID()
  # Error Bank
  veloerrorbank=raw.banks(veloerror)
  if veloerrorbank.size()==0:
      print " --> There is no Error bank in the input FILE "
  else:
      print " VeloError RawBank vector size: ", veloerrorbank.size()
  # look inside the bank
  for bank in range(veloerrorbank.size()):
      aBank=veloerrorbank[bank]
      print 55*"-"
      print " --> size of error bank (body): ", aBank.size(), " Bytes "
      print " --> bank source id: ", aBank.sourceID()
      print " --> bank version: ", aBank.version()
      print " --> magic pattern: %x" % aBank.magic()
      data=aBank.data()
      print " --> data body: ", data
      print 55*"-"
      w_0=(data[0] & 0xffffffff)
      w_1=(data[1] & 0xffffffff)
      w_2=(data[2] & 0xffffffff)
      w_3=(data[3] & 0xffffffff)
      w_4=(data[4] & 0xffffffff)
      w_5=(data[5] & 0xffffffff)
      w_6=(data[6] & 0xffffffff)
      w_7=(data[7] & 0xffffffff)
      w_10=(data[10] & 0xffffffff)
      w_14=(data[14] & 0xffffffff)
      w_11=(data[11] & 0xffffffff)
      w_12=(data[12] & 0xffffffff)
      w_13=(data[13] & 0xffffffff)
      w_15=(data[15] & 0xffffffff)
      w_16=(data[16] & 0xffffffff)
      w_17=(data[17] & 0xffffffff)
      w_18=(data[18] & 0xffffffff)
      w_19=(data[19] & 0xffffffff)
      w_22=(data[22] & 0xffffffff)
      w_23=(data[23] & 0xffffffff)
      w_24=(data[24] & 0xffffffff)
      w_25=(data[25] & 0xffffffff)
      w_28=(data[28] & 0xffffffff)
      w_40=(data[40] & 0xffffffff)
      # raw numbers
      print " --> word 0: %u" % w_0
      print " --> word 1: %u" % w_1
      print " --> word 2: %u" % w_2
      print " --> word 3: %u" % w_3
      print " --> word 4: %u" % w_4
      # decoded numbers
      # bcn
      bcn=(w_0 & 0x00000fff)
      print " bcn = ", bcn
      # det id
      detid=(w_0 & 0x0000f000)
      detid=detid >> 12
      print " det id: ", detid
      # mark_1
      mark_1_4=(w_4 & 0x000000ff)
      # mark_2
      mark_2_4=(w_4 & 0x0000ff00) >> 8
      print " mark_1_4: %x" % mark_1_4, " mark_2_4: %x" % mark_2_4
      #
      for n in range(4):
        shift=n*8
        bit=(w_7 >> shift) & 0x000000ff
        print " I bit %x" % bit
      for n in range(4):
        shift=n*8
        bit=(w_19 >> shift) & 0x000000ff
        print " I bit %x" % bit
      for n in range(4):
        shift=n*8
        bit=(w_28 >> shift) & 0x000000ff
        print " I bit %x" % bit
      #
# TES structure
evt.dump()
