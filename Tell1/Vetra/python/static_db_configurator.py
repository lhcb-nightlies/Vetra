#-------------------------------------------------------------
# 
# This is a helper module to facilitate fetching the static
# part of the VELOCOND content. Some of the parameters that
# are needed for the Tell1 boards configuration do need 
# frequent updating - once calculated can be used for a very
# significant period of time.
#
# 1) Beetle header pseudo thresholds
#
# T. Szumlak 18/05/2009
#
# Vetra/python
#--------------------------------------------------------------

# --> system imports
import os, sys
from xml.etree import ElementTree as et

# --> import necessary ROOT classes 
from ROOT import TFile, TH1F
import commands
import TELL1Map as tell1_map
import get_tell1_number

# --> set paths and vetraroot
STATIC_CONF_PATH = '/VetraCondDB/VeloStaticDBConfig/'
VETRAROOT = os.environ['VETRAROOT']

# --> check if we run on plus farm and configure tell1_map
HOST_NAME = os.environ['HOSTNAME']
if HOST_NAME[:4] != 'plus':
   HOST_NAME = 'Local'
   
def header_pseudo_thresholds():
  # --> file with static parameters - keep it static as well!
  PARAMS_STORAGE = 'header_pseudo_thresholds.root'
  if VETRAROOT == None:
    print " --> You need to set Vetra environment first! "
    return None
  # --> data structure to store pseudo thresholds --
  header_ps_th_bank = {}
  # ------------------------------------------------
  STORAGE = VETRAROOT + STATIC_CONF_PATH + PARAMS_STORAGE
  if os.path.exists(STORAGE):
    hist_storage = TFile(STORAGE)
    if hist_storage == None:
      print " --> Cannot connect to the file: ", STORAGE
      return None
    high_th_hist = hist_storage.Get("High")
    low_th_hist = hist_storage.Get("Low")
    if high_th_hist.GetNbinsX() != low_th_hist.GetNbinsX():
      print " --> Problem with bin numbers! "
      return None
    BINS = high_th_hist.GetNbinsX()
    # --> the histograms limits have been defined in a funny way
    # need to shift the index tell1 = bin - 2
    FIRST_VALID_BIN = 2
    for bin in range(1, BINS):
      th_h_val = high_th_hist.GetBinContent(bin)
      th_l_val = low_th_hist.GetBinContent(bin)
      if th_h_val > 0 and th_l_val > 0:
        # --> make a pair of thresholds for each sensor (tell1)
	th_pair = ' ' + str(int(th_h_val)) + ' ' + str(int(th_l_val))
	tell1 = bin - FIRST_VALID_BIN 
	header_ps_th_bank[tell1] = th_pair
    # --> there are two sensors missing (Phi), use some resonable values here
    # this is a hack - should be corrected in near future!
    dummy_pair = ' ' + str(131) + ' ' + str(121)
    header_ps_th_bank[86] = dummy_pair
    header_ps_th_bank[87] = dummy_pair
  else:
    print " --> There is a problem with file that contains header pseudo thresholds "
    return None
  # --> return pairs of thresholds for each active sensor (tell1)
  return header_ps_th_bank

def gain_settings():
  # --> get all the xml files with gain parameters
  GAIN_STORAGE = 'GainSettings'
  cond_cnt = 0
  if VETRAROOT == None:
    print " --> You need to set Vetra environment first! "
    return None
  # --> keep it as list - it will help later
  gain_settings_bank = {}
  STORAGE = VETRAROOT + STATIC_CONF_PATH + GAIN_STORAGE
  if os.path.exists(STORAGE):
    # --> untar the archive
    ARCHIVE_EXISTS = False
    archive_name = str()
    a_list = os.listdir(STORAGE)
    for entry in a_list:
      if entry.split('.')[-1] == 'tar':
        archive_name = entry
        ARCHIVE_EXISTS = True
    if not ARCHIVE_EXISTS:
      print ' --> Cannot find gain condition archive! '
      return ( None )
    command = 'tar -xvf ' + STORAGE + '/' + archive_name + ' -C ' + STORAGE
    print ' --> Unpacking gain conditions archive '
    os.system(command)
    # --> get all the xml files
    xml_cond_files = []
    for root, dirs, files in os.walk(STORAGE):
      if len(dirs) == 1:
        xml_cond_files = files
    for xml_cond in xml_cond_files:
      if xml_cond.split('.')[-1] != 'xml':
        continue
      cond_cnt += 1
      current_tree = et.parse(STORAGE + '/' + xml_cond)
      conds = current_tree.getroot().findall('condition')
      print ' --> Found: ', len(conds), ' gain conditions in file: ', xml_cond
      for cond in conds:
        cond_name = cond.get('name')
        tell1_num = get_tell1_number.getTell1Number(cond_name)
        if HOST_NAME == 'Local':
          mapper = tell1_map.TELL1Map(VETRAROOT + STATIC_CONF_PATH + 'livdb_tell1_sensor_map.txt', '')
        else:
          mapper = tell1_map.TELL1Map()
        sensor_num = mapper.getSensor(int(tell1_num))
        if sensor_num != None:
          sub_conds = cond.findall('paramVector')
          for sub_cond in sub_conds:
            gain_vec = sub_cond.text.split()
            if len(gain_vec) != 0:
              gain_settings_bank[sensor_num] = gain_vec
  else:
    print ' --> Cannot connect to the ', STORAGE, ' directory '
    return ( None )
  # --> return map with gains
  if len(gain_settings_bank) != 0:
    delete = 'rm -f ' + STORAGE + '/*xml'
    os.system(delete)
    print ' ------------------------------------------------------------- '
    print '   --> Will insert ', cond_cnt, 'conditions in the VELOCOND DB '
    print ' ------------------------------------------------------------- '    
    return ( gain_settings_bank )
  else:
    print ' --> Could not build gain map! '
    return ( None )

