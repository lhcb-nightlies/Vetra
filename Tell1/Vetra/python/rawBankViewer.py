#############################################################
# This script can be used as validation/tutorial tool
# it runs over a given mdf (raw data file) and can present
# any information related to the file structure - especially
# the bank content
# 
# --> T. Szumlak
# -------------------------------------------
# --> this script is a part of Vetra project
# -------------------------------------------
# 
# The script can be run over any data file - noise, test
# pulse, generated data - it does not assume any internal
# structure just checks it
#
# usage - no arguments needed
# python rawBankViewer.py
# remember to set Vetra environment first
#############################################################

from Gaudi.Configuration import *
import GaudiPython as Gaudi

# import all necessary option files
importOptions('$STDOPTS/LHCbApplication.opts')
importOptions('$STDOPTS/RawDataIO.opts')
importOptions('$STDOPTS/DecodeRawEvent.py')

# application
ApplicationMgr(OutputLevel=3, AppName='RawBankViewer')

# application manager
appMgr=Gaudi.AppMgr(outputlevel=3)

# input file to be checked
appMgr.config(options=[
                       'EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/ted_data/030930_0000077697.raw\' SVC=\'LHCb::MDFSelector\'"}'
#                       'EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/ted_data/030929_0000077696.raw\' SVC=\'LHCb::MDFSelector\'"}'
#                       'EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/ted_noise_data/ntp/030831_0000077272.raw\' SVC=\'LHCb::MDFSelector\'"}'
#                       'EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/ted_noise_data/ntp/030831_0000077271.raw\' SVC=\'LHCb::MDFSelector\'"}'
#                       'EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/ted_noise_data/ntp/030829_0000077268.raw\' SVC=\'LHCb::MDFSelector\'"}'
#                       'EventSelector.Input += {"DATAFILE=\'file:///rep/LHCbData/data/full/043926_0000000001.raw\' SVC=\'LHCb::MDFSelector\'"}'
                       #'EventSelector.Input += {"DATAFILE=\'rfio:/castor/cern.ch/grid/lhcb/data/2008/RAW/VELO/PHYSICSTP/28355/028355_0000064049.raw\' SVC=\'LHCb::MDFSelector\'"}'
                       #'EventSelector.Input += {"DATAFILE=\'rfio:/castor/cern.ch/grid/lhcb/data/2008/RAW/VELO/PHYSICSNTP/30173/030173_0000074444.raw\' SVC=\'LHCb::MDFSelector\'"}'
#                       'EventSelector.Input += {"DATAFILE=\'rfio:/castor/cern.ch/user/s/szumlat/clusterGeneration_phi_080828_1447_firmware_v2r5.dat\' SVC=\'LHCb::MDFSelector\'"}'
#                       'EventSelector.Input += {"DATAFILE=\'rfio:/castor/cern.ch/user/s/szumlat/errorbanktest1.raw\' SVC=\'LHCb::MDFSelector\'"}'
#                       'EventSelector.Input += {"DATAFILE=\'file:///afs/cern.ch/user/s/szumlat/scratch0/vetellc12_6stripclustest.raw\' SVC=\'LHCb::MDFSelector\'"}'
                       #'EventSelector.Input += {"DATAFILE=\'file:///afs/cern.ch/user/s/szumlat/scratch0/vetellc12_6stripclustest_20081116.raw\' SVC=\'LHCb::MDFSelector\'"}'
                       ])
appMgr.initialize()

# get event service
evt=appMgr.evtsvc()

# get event selector service
selector=appMgr.evtSel()
selector.PrintFreq=1

# run the stuff
for i in range(1):
  appMgr.run(1)
  print " --- TES structure ------ "
  evt.dump()
  print " ------------------------ "
  raw=evt['/Event/DAQ/RawEvent']
  if raw == None:
      print " --> Error during processing, check file name! "
      continue
  # Zero Suppressed data bank
  velo=8
  # Non-Zero Suppressed data bank
  velofull=18
  # error bank
  veloerror=29
  # ZS bank
  velobank=raw.banks(velo)
  if velobank.size()==0:
      print " --> There is no Zero Suppressed bank in the input FILE "
  else:
      print " --> Velo RawBank vector size (ZS): ", velobank.size()
  # NZS bank
  velofullbank=raw.banks(velofull)
  if velofullbank.size()==0:
      print " --> There is no Non-Zero Suppressed bank in the input FILE "
  else:
      print " --> VeloFull RawBank vector size: ", velofullbank.size()
      for bank in velofullbank:
         #print " Src Id (TELL1 number): ", velofullbank[0].sourceID()
         print " Src Id (TELL1 number): ", bank.sourceID()
  # Error Bank
  veloerrorbank=raw.banks(veloerror)
  if veloerrorbank.size()==0:
      print " --> There is no Error bank in the input FILE "
  else:
      print " --> VeloError RawBank vector size: ", veloerrorbank.size()


