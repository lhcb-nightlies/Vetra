###########################################################
#
# a simple module to create a list of tell1 numbers
# that created the nzs output stream, the list will
# be needed for calculation and updating of the
# tell1 processing parameters
# this module is necessary because of a lot of different
# possible data taking scenarios - especially for the
# round-robin mode
#
# T. Szumlak
# -------------------------------------------
#   this script is a part of Vetra project
# -------------------------------------------
#
############################################################

# --> system
import os

# --> import all ROOT 
from ROOT import TFile, TH1F
import commands, os
USER_NAME = os.environ['USER']

def getActiveTell1List(time_stamp):
  command = ''
  if USER_NAME != 'velo_user':
    command = 'ls -rt $VETRAROOT/job/Tell1_Params_Storage*.root | tail -1'
  else:
    path = '/calib/velo/tell1_params/TELL1_' + time_stamp
    command = 'ls -rt ' + path + '/Tell1_Params_Storage*.root | tail -1'
  tell1_list_storage = commands.getoutput(command)
  tell1_list = {}
  if tell1_list_storage != None:
    tell1_storage = TFile(tell1_list_storage)
    tell1_dir = tell1_storage.Get('Vetra/VeloPedestalSubtractorMoni')
    tell1_stash = tell1_dir.Get('Used_Sensors')
    if tell1_stash != None:
      for bin in range(1, tell1_stash.GetNbinsX()):
        if tell1_stash.GetBinContent(bin) > 0.:
          # --> store the active tell1 boards in a dictionary
          tell1_list[bin - 1] = bin - 1
    else:
      print " --> Cannot retriev the active tell1 list! "
  else:
    print " --> Cannot find/open root file! "
  # --> return the list
  return tell1_list

# --> run the script if it is used in the standalone mode
if __name__ == '__main__':
    tell1_list = getActiveTell1List()
    print " --> Number of active tell1 boards: ", len(tell1_list)
    for tell1 in tell1_list:
      print " -> ", tell1_list[tell1]
