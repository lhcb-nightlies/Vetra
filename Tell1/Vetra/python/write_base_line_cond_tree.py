#---------------------------------------------------------
# This script is used to write Xml file with conditions
# for TELL1 boards. It is as simple as possible to aid
# readability for any person who is not a python expert
# and may in future need to change a line or two
# The produced Xml file can be subsequently converted into 
# sqlite file and use as VELOCOND DB - a partition of 
# the COND DB Within the GAUDI framework this DB is meant 
# to be used by the Vetra project.
#
# Collection of conditions that are produced by this script
# constitute so called base line set of processing params.
#
# NOTE, this script should not be executed as a standalone
# condition writer, it should be imported and a proper time
# stamp should be produced, standalone mode should be only
# used as a mean of debugging the sources
#
# T. Szumlak 20/06/2008
#
# Vetra/python
#----------------------------------------------------------
#
import time
import datetime
import os

# --> local modules
import static_db_configurator

def writeBaseLineCondTree(time_stamp, type, sens='R', genTell1=0):
  #
  print "================================================"
  print " --> Creating XML file with conditions for VELO "
  print "================================================"
  # --> define the Xml headers
  head0='<?xml version="1.0" encoding="ISO-8859-1"?>'
  head1='<!DOCTYPE DDDB SYSTEM "../DTD/structure.dtd">'

  # --> DDDB tag
  DDDBTag="<DDDB>"
  endDDDBTag="</DDDB>"

  # --> open the Xml file and write out the data
  XmlFileName='VeloTELL1Cond.xml'
  XmlFile=open(XmlFileName, 'w')

  # --> condition tag
  condTag='<condition classID="5" name="'
  endCondTag='</condition>'

  # --> condition value types
  intType=' type="int">'
  doubleType=' type="double">'

  # --> condition tags
  condANumberTag=' '*2+'<param name='
  condAVecTag=' '*2+'<paramVector name='
  endANumberTag=' '*2+'</param>'
  endAVecTag=' '*2+'</paramVector>'
  # --> TAG definitions --
 
  # --> condition names
  firEnable = '"fir_enable"'
  firCoefficient = '"fir_coefficient"'
  pedEnable = '"pedestal_enable"'
  pedUpdateEnable = '"pedestal_auto_update_enable"'
  mcmsEnable = '"mcms_enable"'
  reorderMode = '"reorder_mode"'
  cmEnable = '"cm_enable"'
  zsEnable = '"zs_enable"'
  scalingMode = '"scaling_mode"'
  headerCorrEnable = '"header_correction_enable"'
  headerCorrValue = '"header_corr_value"'
  headerCorrThreshold = '"header_corr_threshold"'
  headerPseudoThreshold = '"header_pseudo_threshold"'
  gain_name = '"gain"'
  linkMask = '"link_mask"'
  pedestalSum = '"pedestal_sum"'
  hitThreshold = '"hit_threshold"'
  lowThreshold = '"low_threshold"'
  sumThreshold = '"sum_threshold"'
  maxClu = '"pp_max_clusters"'
  startStrip = '"strip_start"'

  # --> list of names
  condNamesANumber=[pedEnable, pedUpdateEnable, firEnable, mcmsEnable,
                    reorderMode, cmEnable, zsEnable, scalingMode,
                    headerCorrEnable, maxClu]
  condNamesAVector=[firCoefficient, headerCorrThreshold, headerCorrValue,
                    linkMask, pedestalSum, hitThreshold, lowThreshold,
                    sumThreshold]

  # --> condition values - this may be changed - development/testing
  # --> fir enable
  firEnableVal=str(0)

  # --> pedestal enable
  pedEnableVal=str(1)

  # --> pedestal update enable
  pedUpdateEnableVal=str(1)

  # --> mcms process enable
  mcmsEnableVal=str(1)

  # --> reorder enable
  reorderEnableVal=str(1)

  # --> lcms enable
  lcmsEnableVal=str(1)

  # --> zs enable
  zsEnableVal=str(1)

  # --> scaling mode
  scalingModeVal=str(0)

  # --> beetle header correction enable
  headerCorrEnableVal=str(0)

  # --> beetle header correction value - BHC stands for
  #     Beetle header correction
  highThBHC=530  # high header threshold
  lowThBHC=480   # low header threshold
  headerCorrThresholdVal=' '+str(highThBHC)+' '+str(lowThBHC)

  # --> correction values
  highLow=[1, -1]
  corrVector=highLow*64
  headerCorrValueVal=str()
  for n in range(len(corrVector)):
    headerCorrValueVal+=(' '+str(corrVector[n]))

  # --> FIR coefficients vector
  coeffValue=0
  firCoefficientVal=str()
  for n in range(128):
   firCoefficientVal+=(' '+str(0))

  # --> link mask - pedestal subtractor algorithm
  flag=0         # initial value of the mask
  linkMaskVal=str()
  for n in range(2048):
    linkMaskVal+=(' '+str(flag))

  # --> link pedestal - initial values of pedestal or fixed after
  #     the pedestal subtractor training has been finished
  pedSumInit=524288
  pedestalSumVal=str()
  for n in range(2048):
    pedestalSumVal+=(' '+str(pedSumInit))

  # --> hit thresholds values
  hitThValue=10
  hitThresholdVal=str()
  for n in range(2304):
    hitThresholdVal+=(' '+str(hitThValue))

  # --> low threshold values
  lowThValue=4
  lowThresholdVal=str()
  for n in range(2304):
    lowThresholdVal+=(' '+str(lowThValue))

  # --> sum threshold values
  sumThValue=20
  sumThresholdVal=str()
  for n in range(36):
    sumThresholdVal+=(' '+str(sumThValue))

  # --> max clusters
  maxCluVal=str(512)

  # --> start strips
  # R sensor
  rStrips=str()
  rStripsVal=[0, 64, 128, 192, 256, 320, 384, 448, 512, 512, 576, 640,
              704, 768, 832, 896, 960, 1024, 1024, 1088, 1152, 1216, 1280,
              1344, 1408, 1472, 1536, 1536, 1600, 1664, 1728, 1792, 1856, 1920,
              1984, 0]
  for n in range(len(rStripsVal)):
    rStrips+=(' '+str(rStripsVal[n]))

  # Phi sensor
  phiStrips=str()
  phiStripsVal=[0, 64, 128, 683, 747, 811, 875, 939, 1003, 171, 235, 299, 1024,
                1088, 1152, 1216, 1280, 1344, 342, 406, 470, 1365, 1429, 1493,
                1557, 1621, 1685, 512, 576, 640, 1707, 1771, 1835, 1899,
                1963, 2027]
  for n in range(len(phiStripsVal)):
    phiStrips+=(' '+str(phiStripsVal[n]))

  # --> list of conditions values
  condValuesANumber=[pedEnableVal, pedUpdateEnableVal,
                     firEnableVal, mcmsEnableVal,
                     reorderEnableVal, lcmsEnableVal,
                     zsEnableVal, scalingModeVal,
                     headerCorrEnableVal, maxCluVal]
  condValuesAVector=[firCoefficientVal, headerCorrThresholdVal,
                     headerCorrValueVal, linkMaskVal, pedestalSumVal,
                     hitThresholdVal, lowThresholdVal,
                     sumThresholdVal]

  # serial numbers of tell1 with R type sensors
  rSerial=[49, 51, 62, 121, 143, 145, 147, 151, 152, 161, 170, 175, 177,
           180, 188, 190, 192, 194, 195, 196, 197, 199, 200, 201, 205,
           207, 216, 217, 218, 229, 232, 236, 241, 242, 245, 247, 252,
           263, 264]

  # serial numbers of tell1 with Phi type sensors
  pSerial=[29, 30, 50, 54, 61, 118, 120, 122, 128, 131, 132, 142, 146,
           149, 158, 162, 166, 173, 174, 176, 181, 184, 186, 198, 202,
           203, 204, 214, 220, 222, 225, 228, 231, 234, 235, 240, 244, 249,
           259, 261, 400]
  
  # assign lists with sensor numbers according to data type
  # --> R sensors
  rSensors=[]
  if type == 'serial':
    rSensors=rSerial
  elif type == 'sensor':
    for sensor in range(42):
      rSensors.append(sensor)
  elif type == 'generated' and sens == 'R':
    rSensors.append(genTell1)
  
  print " --> Number of R sensors: ", len(rSensors)
  print " ------------------------ "
  if len(rSensors) != 0:
    for sens in rSensors:
        print " ->     ", sens
  else:
    print " --> No information on R sensors will be put in the COND DB"
  print " --------------------------- "

  # --> Phi sensors
  pSensors=[]
  if type == 'serial':
    pSensors=pSerial
  elif type == 'sensor':
    for sensor in range(64, 106):
      pSensors.append(sensor)
  elif type == 'generated' and sens == 'Phi':
    pSensors.append(genTell1)
  
  print " --> Number of Phi sensors: ", len(pSensors)
  print " --------------------------- "
  if len(pSensors) != 0:
    for sens in pSensors:
        print " ->     ", sens
  else:
    print " --> No information on Phi sensors will be put in the COND DB"
  print " --------------------------- "

  # --> PU sensors
  puSensors=[]
  #if type == 'sensor':
  #  for sensor in range(128, 132):
  #    puSensors.append(sensor)
  
  print " --> Number of PU sensors: ", len(puSensors)
  print " --------------------------- "
  if len(puSensors) != 0:
    for sens in puSensors:
        print " ->     ", sens
  else:
    print " --> No information on PU sensors will be put in the COND DB"
  print " ---------------------------"

  # --> create Tell1Vec
  tell1Vec=str()
  for n in range(len(rSensors)):
    tell1Vec += ( " " + str(rSensors[n]) )
  for n in range(len(pSensors)):
    tell1Vec += ( " " +str(pSensors[n]) )
#  for n in range(len(puSensors)):
#    tell1Vec+=(" "+str(puSensors[n]))
  tell1Vec += " "

  # ------------------------------------
  # --> Static configuration section
  # ------------------------------------

  # --> header pseudo thresholds a data structure with thresholds for all tell1s
  headerPseudoThresholdVal = static_db_configurator.header_pseudo_thresholds()

  # --> gain settings, if some condition are missing use default ones
  default_gain_cond_vec = str()
  for gain_entry in range(64):
    default_gain_cond_vec += ( " " + "0xFFF" )
  default_gain_cond_vec += " "

  # --> build conditions bank using tuned static data
  gain_cond_bank = {}
  gain_values = static_db_configurator.gain_settings()
  sensor_list = gain_values.keys()
  for sensor in sensor_list:
    gains = gain_values[sensor]
    gain_cond = str()
    for gain in gains:
      gain_cond += ( " " + gain )
    gain_cond += " "
    gain_cond_bank[sensor] = gain_cond
  
  # ------------------------------------
  # --> Static configuration section
  # ------------------------------------

  # ------------------------------------
  # --> creating the Xml file's content
  # ------------------------------------
  # --> Header of the Xml
  print >> XmlFile, head0
  print >> XmlFile, head1
  # --> <DDDB>
  print >> XmlFile, DDDBTag

  # --> Tell1 numbers only one such condition
  print >> XmlFile, '<condition classID="5" name="Tell1Map">'
  print >> XmlFile, '<paramVector name="tell1_map" type="int">'
  print >> XmlFile, tell1Vec
  print >> XmlFile, '</paramVector>'
  print >> XmlFile, '</condition>'

  # --> R sensors TELL1 conditions -- loop over R sensors/TELL1s
  if len(rSensors) != 0:  
    for rsens in rSensors:
      # --> Condition collection Tag
      # --> dynamic name of the condition
      # --> <condition ... >
      condTarget="VeloTELL1Board"+str(rsens)+'">'
      print >> XmlFile, ' '+condTag+condTarget
      # --> conditions - single number first -- loop 1
      for num_name in range(len(condNamesANumber)):
        print >> XmlFile, condANumberTag + condNamesANumber[num_name] + intType
        print >> XmlFile, condValuesANumber[num_name]
        print >> XmlFile, endANumberTag
      # --> conditions - vectors -- loop 2
      for vec_name in range(len(condNamesAVector)):
        print >> XmlFile, condAVecTag + condNamesAVector[vec_name] + intType
        print >> XmlFile, ' '*2 + condValuesAVector[vec_name]
        print >> XmlFile, endAVecTag
      # --> this part is sensor specific - start strips - clusterization
      print >> XmlFile, condAVecTag + startStrip + intType
      print >> XmlFile, ' '*2 + rStrips
      print >> XmlFile, endAVecTag
      # --> include header pseudo thresholds for R sensors
      print >> XmlFile, condAVecTag + headerPseudoThreshold + intType
      print >> XmlFile, headerPseudoThresholdVal[rsens]
      print >> XmlFile, endAVecTag
      # --> include gain settings for R sensors
      print >> XmlFile, condAVecTag + gain_name + intType
      if rsens in gain_cond_bank:
        print >> XmlFile, gain_cond_bank[rsens]
      else:
        print >> XmlFile, default_gain_cond_vec
      print >> XmlFile, endAVecTag
      # --> </condition>
      print >> XmlFile, ' ' + endCondTag

  # --> Phi sensors TELL1 conditions -- loop over Phi sensors/TELL1s
  if len(pSensors) != 0:
    for psens in pSensors:
      # --> Condition collection Tag
      # --> dynamic name of the condition
      # --> <condition ... >
      condTarget = "VeloTELL1Board" + str(psens) + '">'
      print >> XmlFile, ' ' + condTag + condTarget
      # --> conditions - single number first -- loop 1
      for num_name in range(len(condNamesANumber)):
        print >> XmlFile, condANumberTag + condNamesANumber[num_name] + intType
        print >> XmlFile, condValuesANumber[num_name]
        print >> XmlFile, endANumberTag
      # --> conditions - vectors -- loop 2
      for vec_name in range(len(condNamesAVector)):
        print >> XmlFile, condAVecTag + condNamesAVector[vec_name] + intType
        print >> XmlFile, ' '*2 + condValuesAVector[vec_name]
        print >> XmlFile, endAVecTag
      # --> this part is sensor specific - start strips - clusterization
      print >> XmlFile, condAVecTag + startStrip + intType
      print >> XmlFile, ' '*2 + phiStrips
      print >> XmlFile, endAVecTag
      # --> include header pseudo thresholds for Phi sensors
      print >> XmlFile, condAVecTag + headerPseudoThreshold + intType
      print >> XmlFile, headerPseudoThresholdVal[psens]
      print >> XmlFile, endAVecTag
      # --> include gain settings for Phi sensors
      print >> XmlFile, condAVecTag + gain_name + intType
      if psens in gain_cond_bank:
        print >> XmlFile, gain_cond_bank[psens]
      else:
        print >> XmlFile, default_gain_cond_vec
      print >> XmlFile, endAVecTag
      # --> </condition>
      print >> XmlFile, ' '+endCondTag

  # --> PileUp sensors TELL1 conditions -- loop over PU sensors/TELL1s
  if len(puSensors) !=0:
    for n in puSensors:
      # --> Condition collection Tag
      # --> dynamic name of the condition
      # --> <condition ... >
      condTarget="VeloTELL1Board"+str(n)+'">'
      print >> XmlFile, ' '+condTag+condTarget
      # --> conditions - single number first -- loop 1
      for n in range(len(condNamesANumber)):
        print >> XmlFile, condANumberTag+condNamesANumber[n]+intType
        print >> XmlFile, condValuesANumber[n]
        print >> XmlFile, endANumberTag
      # --> conditions - vectors -- loop 2
      for n in range(len(condNamesAVector)):
        print >> XmlFile, condAVecTag+condNamesAVector[n]+intType
        print >> XmlFile, ' '*2+condValuesAVector[n]
        print >> XmlFile, endAVecTag
      # --> this part is sensor specific - start strips - clusterization
      print >> XmlFile, condAVecTag+startStrip+intType
      print >> XmlFile, ' '*2+rStrips
      print >> XmlFile, endAVecTag
      # --> </condition>
      print >> XmlFile, ' '+endCondTag

  # --> </DDDB>
  print >> XmlFile, endDDDBTag

  # --> close the Xml file
  XmlFile.close()

  # --> file with condition names
  condFileName='velocond.xml'
  condFile=open(condFileName, 'w')
  print >> condFile, head0
  print >> condFile, head1
  print >> condFile, DDDBTag
  print >> condFile, '<catalog name="VeloCondDB">'
  print >> condFile, '<conditionref href="VeloTELL1Cond.xml#Tell1Map"/>'
  cond='<conditionref href="VeloTELL1Cond.xml#VeloTELL1Board'
  
  # tell1 for R sensors
  if len(rSensors ) != 0:
    print >> condFile, '<!-- R sensors -->'
    for tell1 in rSensors:
       print >> condFile, cond+str(tell1)+'"/>'

  # tell1 for Phi sensors
  if len(pSensors ) != 0:
    print >> condFile, '<!-- Phi sensors -->'
    for tell1 in pSensors:
       print >> condFile, cond+str(tell1)+'"/>'

  # tell1 for PileUp sensors
  if len(puSensors ) != 0:
    print >> condFile, '<!-- Pile-Up sensors -->'
    for tell1 in puSensors:
       print >> condFile, cond+str(tell1)+'"/>'

  # end tag
  print >> condFile, '</catalog>'
  print >> condFile, endDDDBTag
  condFile.close()
  
  # --> house keeping
  # --> the file has been created locally - move it to its nominal position
  # --> check if any xml file with conditions is present, rename if necessary
  VETRAROOT=os.environ['VETRAROOT']
  FILE_LOC=VETRAROOT+'/VetraCondDB/Velo/VeloCondDB/'
  FILE_NAME='VeloTELL1Cond.xml'
  DB_LOC=VETRAROOT+'/VetraCondDB/Velo/'
  DB_NAME='VELOCOND.db'
  XML_REP=VETRAROOT+'/VetraCondDB/Repository/Velo_OLD_XML/'  
  NEW_NAME=FILE_NAME+'_'+time_stamp+'_OLD'
  if os.path.exists(FILE_LOC+FILE_NAME):
    print " --> OLD XML file has been found and it will be renamed "
    print " --> OLD XML file has been moved to: ", NEW_NAME
    print " --> The old file will be moved to: ", XML_REP

  # create the XML files repositiory if not present
  if not os.path.exists(XML_REP):
    print " --> Looks like you are running this script for the first time "
    print "     Repository for older XML files will be created "
    os.system('mkdir '+VETRAROOT+'/VetraCondDB/Repository')
    os.system('mkdir '+VETRAROOT+'/VetraCondDB/Repository/Velo_OLD_XML')
    if not os.path.exists(VETRAROOT+'/VetraCondDB/Velo/VeloCondDB'):
      print " --> Will create folder for the default DB location "
      os.system('mkdir '+VETRAROOT+'/VetraCondDB/Velo/VeloCondDB')

  # cp the freshly created file to the archive
  LATEST_XML=FILE_NAME+'_'+time_stamp
  os.system('cp '+FILE_NAME+' '+LATEST_XML)
  os.system('mv '+LATEST_XML+' '+XML_REP)
  # move old XML to the repository
  if os.path.exists(FILE_LOC+FILE_NAME):
     os.system('mv '+FILE_LOC+FILE_NAME+' '+XML_REP+NEW_NAME)

  #
  os.system('mv '+FILE_NAME+' '+FILE_LOC)
  os.system('mv '+condFileName+' '+FILE_LOC)
  
  # --> remove old database SQLite file
  if os.path.exists(DB_LOC+DB_NAME):
    print " --> OLD data base SQLite file is being removed "
    os.system('rm -f '+DB_LOC+DB_NAME)

if __name__ == '__main__':
  writeBaseLineCondTree('999', 'sensor', 'R', 0)
