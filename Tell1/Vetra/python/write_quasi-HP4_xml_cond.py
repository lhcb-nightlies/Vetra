#---------------------------------------------------------
# This script will produce QUASI HP4 condition to run
# emulation with test beam data. This DB will not provide
# full description of the geometry used during ACDC3 but
# is sufficient to run TELL1 emulation for the data.
#
# T. Szumlak
#
# Vetra/python
#----------------------------------------------------------

# --> define the Xml headers
head0='<?xml version="1.0" encoding="ISO-8859-1"?>'
head1='<!DOCTYPE DDDB SYSTEM "../DTD/structure.dtd">'

# --> DDDB tag
DDDBTag="<DDDB>"
endDDDBTag="</DDDB>"

# --> open the Xml file and write out the data
XmlFileName='VeloTELL1Cond.xml'
XmlFile=open(XmlFileName, 'w')

# --> condition tag
condTag='<condition classID="5" name="'
endCondTag='</condition>'

# --> condition value types
intType=' type="int">'
doubleType=' type="double">'

# --> condition tags
condANumberTag=' '*2+'<param name='
condAVecTag=' '*2+'<paramVector name='
endANumberTag=' '*2+'</param>'
endAVecTag=' '*2+'</paramVector>'
# --> TAG definitions --

# --> condition names
firEnable='"fir_enable"'
firCoefficient='"fir_coefficient"'
pedEnable='"pedestal_enable"'
pedUpdateEnable='"pedestal_auto_update_enable"'
mcmsEnable='"mcms_enable"'
reorderMode='"reorder_mode"'
cmEnable='"cm_enable"'
zsEnable='"zs_enable"'
scalingMode='"scaling_mode"'
headerCorrEnable='"header_correction_enable"'
headerCorrValue='"header_corr_value"'
headerCorrThreshold='"header_corr_threshold"'
linkMask='"link_mask"'
pedestalSum='"pedestal_sum"'
hitThreshold='"hit_threshold"'
lowThreshold='"low_threshold"'
sumThreshold='"sum_threshold"'
maxClu='"pp_max_clusters"'
startStrip='"strip_start"'

# --> list of names
condNamesANumber=[pedEnable, pedUpdateEnable, firEnable, mcmsEnable,
                  reorderMode, cmEnable, zsEnable, scalingMode,
                  headerCorrEnable, maxClu]
condNamesAVector=[firCoefficient, headerCorrThreshold, headerCorrValue,
                  linkMask, pedestalSum, hitThreshold, lowThreshold,
                  sumThreshold]

# --> condition values - this may be changed - development/testing
# --> fir enable
firEnableVal=str(0)
# --> pedestal enable
pedEnableVal=str(1)
# --> pedestal update enable
pedUpdateEnableVal=str(1)
# --> mcms process enable
mcmsEnableVal=str(1)
# --> reorder enable
reorderEnableVal=str(1)
# --> lcms enable
lcmsEnableVal=str(1)
# --> zs enable
zsEnableVal=str(1)
# --> scaling mode
scalingModeVal=str(0)
# --> beetle header correction enable
headerCorrEnableVal=str(0)
# --> beetle header correction value - BHC stands for
#     Beetle header correction
highThBHC=510  # high header threshold
lowThBHC=490   # low header threshold
headerCorrThresholdVal=' '+str(lowThBHC)+' '+str(highThBHC)
# --> correction values
highLow=[-1, 1]
corrVector=highLow*64
headerCorrValueVal=str()
for n in range(len(corrVector)):
 headerCorrValueVal+=(' '+str(corrVector[n]))
# --> FIR coefficients vector
coeffValue=0
firCoefficientVal=str()
for n in range(128):
 firCoefficientVal+=(' '+str(0))
# --> link mask - pedestal subtractor algorithm
flag=0         # initial value of the mask
linkMaskVal=str()
for n in range(2048):
 linkMaskVal+=(' '+str(flag))
# --> link pedestal - initial values of pedestal or fixed after
#     the pedestal subtractor training has been finished
pedSumInit=524288
pedestalSumVal=str()
for n in range(2048):
 pedestalSumVal+=(' '+str(pedSumInit))
# --> hit thresholds values
hitThValue=10
hitThresholdVal=str()
for n in range(2304):
 hitThresholdVal+=(' '+str(hitThValue))
# --> low threshold values
lowThValue=4
lowThresholdVal=str()
for n in range(2304):
 lowThresholdVal+=(' '+str(lowThValue))
# --> sum threshold values
sumThValue=20
sumThresholdVal=str()
for n in range(36):
 sumThresholdVal+=(' '+str(sumThValue))
# --> max clusters
maxCluVal=str(512)
# --> start strips
# R sensor
rStrips=str()
rStripsVal=[0, 64, 128, 192, 256, 320, 384, 448, 512, 512, 576, 640,
            704, 768, 832, 896, 960, 1024, 1024, 1088, 1152, 1216, 1280,
            1344, 1408, 1472, 1536, 1536, 1664, 1728, 1792, 1856, 1920,
            1984, 0]
for n in range(len(rStripsVal)):
  rStrips+=(' '+str(rStripsVal[n]))
# Phi sensor
phiStrips=str()
phiStripsVal=[0, 64, 128, 683, 747, 811, 875, 939, 1003, 171,
                  235, 299, 1024, 1088, 1152, 1216, 1280, 1344,
                  342, 406, 470, 1365, 1429, 1493, 1557, 1621,
                  1685, 512, 576, 640, 1707, 1771, 1835, 1899,
                  1963, 2027]
for n in range(len(phiStripsVal)):
  phiStrips+=(' '+str(phiStripsVal[n]))

# --> list of conditions values
condValuesANumber=[pedEnableVal, pedUpdateEnableVal, firEnableVal, mcmsEnableVal,
                   reorderEnableVal, lcmsEnableVal, zsEnableVal, scalingModeVal,
                   headerCorrEnableVal, maxCluVal]
condValuesAVector=[firCoefficientVal, headerCorrThresholdVal, headerCorrValueVal,
                   linkMaskVal, pedestalSumVal, hitThresholdVal, lowThresholdVal,
                   sumThresholdVal]
# list of tell1s for r and phi sensors
rSensors=[21, 24, 49, 50, 61, 62]
phiSensors=[23, 25, 27, 29, 30, 51]

# Tell1 vector
tell1Vec=str()
for n in range(len(rSensors)):
    tell1Vec+=(' '+str(rSensors[n]))
for n in range(len(phiSensors)):
    tell1Vec+=(' '+str(phiSensors[n]))

#rSensors=[21, 24, 49, 50, 30, 62]
#phiSensors=[23, 25, 27, 29, 61, 51]
# ------------------------------------
# --> creating the Xml file's content
# ------------------------------------
# --> Header of the Xml
print >> XmlFile, head0
print >> XmlFile, head1
# --> <DDDB>
print >> XmlFile, DDDBTag
# --> Tell1 numbers only one such condition
print >> XmlFile, ' <condition classID="5" name="Tell1Map">'
print >> XmlFile, '  <paramVector name="tell1_map" type="int">'
print >> XmlFile, ' '*3+tell1Vec
print >> XmlFile, '  </paramVector>'
print >> XmlFile, ' </condition>'
# --> R sensors TELL1 conditions -- loop over R sensors/TELL1s
for n in range(len(rSensors)):
  # --> Condition collection Tag
  # --> dynamic name of the condition
  # --> <condition ... >
  condTarget="VeloTELL1Board"+str(rSensors[n])+'">'
  print >> XmlFile, ' '+condTag+condTarget
  # --> conditions - single number first -- loop 1
  for n in range(len(condNamesANumber)):
    print >> XmlFile, condANumberTag+condNamesANumber[n]+intType
    print >> XmlFile, ' '*3+condValuesANumber[n]
    print >> XmlFile, endANumberTag
  # --> conditions - vectors -- loop 2
  for n in range(len(condNamesAVector)):
    print >> XmlFile, condAVecTag+condNamesAVector[n]+intType
    print >> XmlFile, ' '*2+condValuesAVector[n]
    print >> XmlFile, endAVecTag
  # --> this part is sensor specific - start strips - clusterization
  print >> XmlFile, condAVecTag+startStrip+intType
  print >> XmlFile, ' '*2+rStrips
  print >> XmlFile, endAVecTag
  # --> </condition>
  print >> XmlFile, ' '+endCondTag
# --> Phi sensors TELL1 conditions -- loop over R sensors/TELL1s
for n in range(len(phiSensors)):
  # --> Condition collection Tag
  # --> dynamic name of the condition
  # --> <condition ... >
  condTarget="VeloTELL1Board"+str(phiSensors[n])+'">'
  print >> XmlFile, ' '+condTag+condTarget
  # --> conditions - single number first -- loop 1
  for n in range(len(condNamesANumber)):
    print >> XmlFile, condANumberTag+condNamesANumber[n]+intType
    print >> XmlFile, ' '*3+condValuesANumber[n]
    print >> XmlFile, endANumberTag
  # --> conditions - vectors -- loop 2
  for n in range(len(condNamesAVector)):
    print >> XmlFile, condAVecTag+condNamesAVector[n]+intType
    print >> XmlFile, ' '*2+condValuesAVector[n]
    print >> XmlFile, endAVecTag
  # --> this part is sensor specific - start strips - clusterization
  print >> XmlFile, condAVecTag+startStrip+intType
  print >> XmlFile, ' '*2+phiStrips
  print >> XmlFile, endAVecTag
  # --> </condition>
  print >> XmlFile, ' '+endCondTag
# --> </DDDB>
print >> XmlFile, endDDDBTag

# --> close the Xml file
XmlFile.close()
