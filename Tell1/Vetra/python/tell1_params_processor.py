####################################################################
#
# This script uses trained values of the processing parameters
# and uses them to update the base line (or previously determined)
# xml tree. This script should not be executed as a standalone
# piecec of software it should be always imported as a module.
# Look at mainTraining.py script for example
#
# The main function is processCondTree() it calles sequentially
# functions that extract and update the latest values of the
# processing parameters.
#
# --> T. Szumlak @09/04/2011
# ---------------------------------------------
#   this script is a part of the Vetra project
# ---------------------------------------------
#
# NOTE - it is assumed throughout this script that the default
# location of the root files is $VETRAROOT/job
# 
####################################################################

# --> xml tree processing modules - the main tool to process the xml
from xml.etree import ElementTree as et

# --> system
import os, commands

# --> local
import get_active_tell1_list as tell1ListReader
import TELL1Map as tell1_map
import translate_chip_channels_2_tell1_channels as chan_scrambler
import get_tell1_number
import banks_extractor as _ext_

# ----------------------
# -- GLOBAL variables --
# ----------------------
# --> Define exit status code
SUCCESS = 0
FAILURE = 1
# --> this is hardcoded at the moment it may be made an option
RUN_CONSECUTIVE_TRAINING = 1
FPGA_CHAN = 512
BEETLE_XTALK_PER_FPGA = 32
# --> default Cond tree name - do not change this!
DB_NAME = 'VELOCOND.db'
COND_NAME_ROOT = 'VeloTELL1Board'
VETRAROOT = os.environ['VETRAROOT']
head1 = '<?xml version="1.0" encoding="ISO-8859-1"?>'
head2 = '<!DOCTYPE DDDB SYSTEM "../../DTD/structure.dtd">'

# --> set all necessary paths to move updated and old files
def checkEnv():
   if VETRAROOT == None:
     print " --> Set VETRA environment first! "
     return FAILURE
   #
   return SUCCESS

def main_tell1_para_processor(time_stamp):
    env_status = checkEnv()
    if SUCCESS != env_status:
        print '--> Problem with the tell1 para processor initialisation! '
        exit(2)
    print " -------------------------------------------------------------------- "
    print " -->   Processing and updating the conditions! "
    print " -------------------------------------------------------------------- "
    print " ==========================="
    print " --> Retrieve the XML tree"
    print " ==========================="
    
    XML_DEST = DB_REPO = DB_LOC = STATIC_DB_PATH = str()
    USER_NAME = os.environ['USER']
    path_main = ''
    if USER_NAME != 'velo_user':
       path_repo = VETRAROOT + '/VetraCondDB/Repository/'
       if not os.path.exists( path_repo ):
          os.system( 'mkdir ' + path_repo )
       path_main = path_repo + 'TELL1_' + time_stamp
       comm = 'mkdir ' + path_main
       os.system( comm )
    else:
       path_repo = '/calib/velo/tell1_params/'
       path_main = path_repo + 'TELL1_' + time_stamp
       if not os.path.exists( path_main ):
          comm = 'mkdir ' + path_main
          os.system( comm )
       
    print ' --> New parameters will be stored at: ', path_main
    DB_REPO = path_main

    # --> folder for the updated conditions
    up_path = path_main + '/TELL1_new'
    if not os.path.exists( up_path ):
       os.system( 'mkdir ' + up_path )
    
    # --> update the latest db do not recreate it, get the HEAD tag!
    from CondDBUI.Admin import DumpToFiles
    connect_string = 'sqlite_file:$VELOSQLITEDBPATH/VELOCOND.db/VELOCOND'
    tmp_dump_loc = '/tmp/velocond_scratch'
    time_str = commands.getoutput('`date +"%s000000000"`')
    time = time_str.split()[1][:-1]
    DumpToFiles(connect_string, int(time), "HEAD", ["/"], tmp_dump_loc, '')
    cp_path = tmp_dump_loc + '/VeloCondDB/TELL1' + ' ' + path_main + '/TELL1_HEAD'
    os.system( 'cp -r ' + cp_path )
    os.system( 'rm -rf ' + tmp_dump_loc )

    # --> connect to conditions and update them
    MAP_FILE_NAME = 'Tell1Map.xml'
    MASTER_COND_NAME = 'VeloTELL1Board'
    cond_file_list = []
    try:
        map_tree = et.parse(DB_REPO + '/TELL1_HEAD/' + MAP_FILE_NAME)
    except Exception, inst:
       print " --> Problem with parsing %s: %s" % (MAP_FILE_NAME, inst)
       print " --> ABORT! "
       exit(2)
    tell1_map = map_tree.getroot().find('condition').find('paramVector').text
    # --> the list of tell1s from the VELOCOND db
    tell1_db_list = tell1_map.split()
    # --> the list of tell1s registered in the training job
    tell1_list = tell1ListReader.getActiveTell1List(time_stamp)

    print " ======================================= "
    print " --> Building banks with new parameters "
    print " ======================================= "
    # --> created banks with the updated values
    up_ped_banks = { }
    up_th_seed_banks = { }
    up_th_incl_banks = { }
    up_bhct_corr = { }
    up_bhxt_th = { }
    if len(tell1_list):
        up_ped_banks = _ext_.getPedBanks(tell1_list, time_stamp)
        if len(up_ped_banks) == 0:
           print " --> There are no pedestal banks available! "
           print " --> Aborting processing! "
           exit(2)
        up_th_seed_banks, up_th_incl_banks = _ext_.getCluThresholds(tell1_list, time_stamp)
        if len(up_th_seed_banks) == 0:
           print " --> There are no thereshold banks available! "
           print " --> Aborting processing! "
           exit(2)
        up_bhxt_corr, up_bhxt_th = _ext_.getBHXTalkCorrections(tell1_list, time_stamp)
        if len(up_bhxt_corr) == 0 or len(up_bhxt_th) == 0:
           print " --> Problem with BHXT corrections! "
           print " --> Aborting processing! "
           exit(2)
    else:
       print " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "
       print " --> Problem with Vetra job or not enough events processed! "
       print " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "
       exit(2)

    print " --> Updating XML tree "    
    for tell1 in tell1_list:
       print " --> Found active Tell1 number: ", tell1_list[tell1]
       if str(tell1) not in tell1_db_list:
           print " --> Inconsistency between VELOCOND and the data file! "
           print " --> Unknown Tell1 number: ", tell1, " found! "
           exit(2)
       
       # --> update all conditions
       cond_name = MASTER_COND_NAME + str(tell1)
       try:
          board_cond = et.parse(DB_REPO + '/TELL1_HEAD/' + cond_name + '.xml')
       except Exception, inst:
          print " --> Problem with parsing %s: %s" % (cond_name, inst)
          print " --> ABORT! "
          exit(2)
       paramvec_list = board_cond.getroot().find('condition').findall('paramVector')
       # --> write in Tell1 channel order
       # --> translate chip channel -> Tell1 channel
       for vector in paramvec_list:
          if 'header_corr_threshold' == vector.get('name'):
             vector.text = up_bhxt_th[tell1]
          if 'header_corr_value' == vector.get('name'):
             chc_vec = up_bhxt_corr[tell1]
             tch_vec = chan_scrambler.scramble2Tell1Chan(chc_vec.split(), 32)
             vector.text = tch_vec
          if 'hit_threshold' == vector.get('name'):
             vector.text = up_th_seed_banks[tell1]
          if 'low_threshold' == vector.get('name'):
             vector.text = up_th_incl_banks[tell1]
          if 'pedestal_sum' == vector.get('name'):
             chc_vec = up_ped_banks[tell1]
             tch_vec = chan_scrambler.scramble2Tell1Chan(chc_vec.split(), 512)
             vector.text = tch_vec
             
       # --> write the updated conditions
       f_name = up_path + '/' + cond_name + '.xml'
       board_cond.write(f_name)
       db_cond = open(f_name)
       db_cond.seek(0)
       db_cond_body = db_cond.read()
       os.system( 'rm -f ' + f_name )
       db_cond_f = open(f_name, 'w')
       print >> db_cond_f, head1
       print >> db_cond_f, head2
       print >> db_cond_f, db_cond_body
    os.system( 'rm -rf ' + path_main + '/TELL1_HEAD' )
    print " path: ", path_main
    print " --------------------------- "
    print " --> Processing Finished! "
    print " --------------------------- "

# ----------
# -- MAIN --
# ----------
if __name__ == '__main__':
    main_tell1_para_processor(time_stamp)
