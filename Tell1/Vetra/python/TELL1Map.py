#!/usr/bin/env python

"""Handle TELL1 to sensor number mapping.

This Exports:
  
  - TELL1Map.TELL1Map  Every time a TELL1Map object is constructed it will
    check the modification time of the local TELL1 mapping text file.  In case
    this is older than a minute it will try to fetch the latest mapping from
    the Liverpool website.  If there are network problems, a warning is issued
    and the (possibly obsolete) local copy is used to initialize the map.

  Example usage:

    import TELL1Map
    m = TELL1Map.TELL1Map()
    t1 = 183
    s1 = m.getSensor(t1)
    s2 = 65
    t2 = m.getTell1(s2)

  This example will use the default map file path on the online cluster,
  /group/velo/config/livdb_tell1_sensor_map.txt

  If you want to use a different file path, you can specify it in the
  constructor:

    import TELL1Map
    m = TELL1Map.TELL1Map("/path/to/mapfile.txt")

  By default, the constructor will use the LHCb online network http proxy.  So
  if you are on a different network you want to set your own proxy or none.
  You can do that using the second constructor argument.  Here is an example
  how to disable the proxy entirely:

    import TELL1Map
    m = TELL1Map.TELL1Map("/path/to/mapfile.txt","")

Author: Kurt Rinnert <kurt.rinnert@cern.ch>
Date:   2009-04-07
Revision: $Id: TELL1Map.py,v 1.3 2009-08-21 13:18:34 szumlat Exp $

"""
import os
import time

class TELL1Map:
  """Provide interface to TELL1 to sensor mapping."""
  
  tell1ToSensor = {}
  sensorToTell1 = {}

  def __init__(self, mapfilename="/group/velo/config/livdb_tell1_sensor_map.txt", proxy="http://netgw01:8080/"):
    """Maintain local cache file and initialize map.

    Keyword arguments:
    string -- the path to the local cache file (default: "/group/velo/config/livdb_tell1_sensor_map.txt")
    string -- the http proxy, set to "" for direct access (default: "http://netgw01:8080/")
    
    """
    # Check timestamp of local mapping file and fetch file
    # if local copy is too old.
    dt = 61
    if os.path.exists(mapfilename):
      dt = time.time() - os.path.getmtime(mapfilename)
    if dt > 60:
      os.umask(002)
      temp_file = "/tmp/%d_tell1_sensor_map.txt" % os.getpid()
      if 0 != len(proxy):
        os.putenv("http_proxy",proxy)
      cmd_status = os.system("wget -q --connect-timeout=5 --tries=1 -O %s http://hep.ph.liv.ac.uk/velodb/VELO/ExportTell1SW.txt" % temp_file)
      if 0 != cmd_status:
        print "WARNING: could not retrieve mapping from Liverpool DB, using local cache."
        os.system("rm -f %s" % temp_file)
      else:
        os.system("mv %s %s" % (temp_file, mapfilename))
    
    if not os.path.exists(mapfilename):     
      print "ERROR: no connection to Liverpool DB and no local cache available.  TELL1 Map is invalid!"
      return

    # OK, we are good to go. Build map from file.
    mf = open(mapfilename)

    # The first line contains the legend and should be ignored.
    mf.readline()
    for line in mf:
      tokens = line.split()
      tell1 = int(tokens[0][3:])
      sens  = int(tokens[1])
      self.tell1ToSensor[tell1] = sens
      self.sensorToTell1[sens]  = tell1
  
    mf.close()

    # add pile up sensors, will be in data base in the near future
    self.tell1ToSensor[21] = 128
    self.tell1ToSensor[414] = 129
    self.tell1ToSensor[22] = 130
    self.tell1ToSensor[26] = 131
    self.sensorToTell1[128] = 21
    self.sensorToTell1[129] = 414 
    self.sensorToTell1[130] = 22
    self.sensorToTell1[131] = 26

  def getSensor( self, tell1 ):
    """Return sensor number.

    Keyword arguments:
    int -- the TELL1 number

    """
    if tell1 in self.tell1ToSensor.keys():
        return ( self.tell1ToSensor[tell1] )
    else:
        print ' --> Mapping file does not contain tell1: ', tell1
        return ( None )

  def getTell1( self, sensor ):
    """Return TELL1 number.

    Keyword arguments:
    int -- the sensor number
    """
    if sensor in self.sensorToTell1.keys():
        return self.sensorToTell1[sensor]
    else:
        print ' --> Mapping file does not contain sensor: ', sensor
        return ( None )

if __name__ ==  "__main__":
  """Maintain local cache."""
  m = TELL1Map()


  
