"""
###############################################################################
#                                                                             #
#  Check that the ZS monitoring will run without processing any events        #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Marius Bjoernstad ( Paul.Bjoernstad@hep.manchester.ac.uk )        #
#  @date    2010-07-28                                                        #
#                                                                             #
###############################################################################
"""


from Configurables import Vetra

vetra = Vetra()

# Setting DataBanks to "ZS" enables by default a set of ZS monitoring algos
# It causes Vetra to load: options/Velo/VetraMoni_ZS.py 
vetra.DataBanks = 'ZS'

# Do not run error banks test, tested in separate file
vetra.CheckEBs = True

# No Data
vetra.DataType = '2010'


