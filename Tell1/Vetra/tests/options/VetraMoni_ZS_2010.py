"""
###############################################################################
#                                                                             #
#  Test for monitoring with zero-suppressed data. Includes everything but     #
#   error banks.                                                              #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Eduardo Rodrigues  ( e.rodrigues@physics.gla.ac.uk )              #
#  @author  Marius Bjoernstad ( Paul.Bjoernstad@hep.manchester.ac.uk )        #
#  @date    2010-04-26                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration           import EventSelector
from Configurables import Vetra

vetra = Vetra()

# Setting DataBanks to "ZS" enables by default a set of ZS monitoring algos
# It causes Vetra to load: options/Velo/VetraMoni_ZS.py 
vetra.DataBanks = 'ZS'

# Do not run error banks test, tested in separate file
vetra.CheckEBs = False



# First FULL stream from run #69354
vetra.DataType = '2010'

from PRConfig import TestFileDB
TestFileDB.test_file_db["2010_RAW_FULL_LHCb_COLLISION10_69354_069354_0000000003.raw"].run()

from Configurables import Vetra
Vetra().HistogramFile = "moni-zs-2010.root"


