"""
###############################################################################
#                                                                             #
#  Select input for tests of NZS stream                                       #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Marius Bjoernstad ( Marius.Bjoernstad@cern.ch )                   #
#  @date    2011-06-10                                                        #
#                                                                             #
###############################################################################
"""

# Use run #

from Gaudi.Configuration           import EventSelector
from Configurables import Vetra

Vetra().EvtMax = 100

from PRConfig import TestFileDB
TestFileDB.test_file_db["2011_RAW_CALIB_LHCb_COLLISION11_92222_092222_0000000003.raw"].run()


EventSelector().PrintFreq = 10

from Configurables import Vetra
Vetra().HistogramFile = "moni-nzs-stream-201105.root"

