"""
###############################################################################
#                                                                             #
#  Select input for tests with Round-Robin period of 1/1000                   #
#  (name is wrong, but not thought to be of major importance)                 #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Marius Bjoernstad ( Paul.Bjoernstad@hep.manchester.ac.uk )        #
#  @date    2010-06-14                                                        #
#                                                                             #
###############################################################################
"""

# Use run #75770

from Gaudi.Configuration           import EventSelector
from Configurables import Vetra

Vetra().EvtMax = 10000


from PRConfig import TestFileDB
TestFileDB.test_file_db["2010_RAW_FULL_VELO_NZS_75770_075770_0000000001.raw"].run()

EventSelector().PrintFreq = 1000

from Configurables import Vetra
Vetra().HistogramFile = "moni-nzs-round-robin-100.root"

