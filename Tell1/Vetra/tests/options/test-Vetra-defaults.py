# Default settings for all Vetra QMTests
# --------------------------------------
from Gaudi.Configuration import *
from Configurables import Vetra
vetra = Vetra()
vetra.EvtMax = 100
EventSelector().PrintFreq = 10

vetra.DDDBtag      = 'head-20110303'
vetra.CondDBtag    = 'head-20110524'
vetra.VELOCONDtag  = 'HEAD'

vetra.HistogramFile = ''
vetra.TupleFile = ''

# Reduce number of messages in STDOUT, to compare only important
# ones
MessageSvc().OutputLevel = WARNING

# This will print the ODIN bank of the first event. It
# may be useful to test PrintOdinBank
from Configurables import PrintODINBank
PrintODINBank().OutputLevel = INFO
