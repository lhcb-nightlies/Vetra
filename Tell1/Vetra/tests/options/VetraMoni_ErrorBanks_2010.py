"""
##################################################################################
#                                                                                #
# Options for testing the error banks monitoring. This will run Vetra on data    #
# that is known to have error banks. Only a minimal set of algorithms is run.    #
#                                                                                #
#  @package Tell1/Vetra                                                          #
#  @author  Marius Bjoernstad (Paul.Bjoernstad@hep.manchester.ac.uk)             #
#  @date    2010-04-22                                                           #
#                                                                                #
##################################################################################
"""
from Gaudi.Configuration import EventSelector

from Configurables import Vetra
from Configurables import GaudiSequencer
from Configurables import Velo__Monitoring__ErrorMon as ErrorMon

from GaudiKernel.Configurable import appendPostConfigAction
from Vetra.QMTest.Utilities import FilterAlgorithms

vetra = Vetra()

vetra.DataBanks    = 'ZS'
EventSelector().PrintFreq = 1000

# This leaves out reconstruction and output sequences
vetra.MainSequence = ["ProcessPhase/TELL1Processing", "ProcessPhase/Moni"]
vetra.CheckEBs = True
# Disable bb filtering; the filter would reject all EB events in test
vetra.FilterBeamBeam = False

from Gaudi.Configuration import INFO
ErrorMon("ErrorMon").OutputLevel = INFO


# Location of error banks found with trial and error 
# There are two error banks after event 20001, before event 21001.
vetra.EvtMax = 1000
EventSelector().FirstEvent = 20001

# This function will remove algorithms that are tested in other tests
def removeAlgo():
    # Find the Moni_EB sequence, and use only that algorithm.
    FilterAlgorithms('MoniVELOSeq', include = ['Moni_EB'])
    
    # Use only the decoding for EBs, not ZS or anything else
    FilterAlgorithms('TELL1ProcessingVELOSeq', include = ['Decoding_EB'])

appendPostConfigAction( removeAlgo )    


# Input data, containing error banks
vetra.DataType = '2010'

from PRConfig import TestFileDB
TestFileDB.test_file_db["2010-Raw-data-testVetraErrorHandling"].run()


vetra.HistogramFile = "moni-error-banks-2010.root"

###############################################################################

