"""
###############################################################################
#                                                                             #
#  Select input from run #45445. This is an old run, taken with the           #
#  "no testpulse" recipe.
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Marius Bjoernstad ( Paul.Bjoernstad@hep.manchester.ac.uk )        #
#  @date    2010-06-14                                                        #
#                                                                             #
###############################################################################
"""
from Gaudi.Configuration           import EventSelector

from PRConfig import TestFileDB
TestFileDB.test_file_db["2009-Raw-data-Vetra_NZS_NoTestPulse"].run()

from Configurables import Vetra
Vetra().HistogramFile = "moni-nzs-no-testpulse.root"

