"""
###############################################################################
#                                                                             #
#  Select input for tests with Round-Robin period of 1                        #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Marius Bjoernstad ( Paul.Bjoernstad@hep.manchester.ac.uk )        #
#  @date    2010-06-14                                                        #
#                                                                             #
###############################################################################
"""

#
#  This is run #71649. 
#

from Gaudi.Configuration           import EventSelector

from PRConfig import TestFileDB
TestFileDB.test_file_db["2010_RAW_FULL_VELO_TESTPULSENZS_71649_071649_0000000001.raw"].run()

from Configurables import Vetra
Vetra().HistogramFile = "moni-nzs-round-robin-1.root"

