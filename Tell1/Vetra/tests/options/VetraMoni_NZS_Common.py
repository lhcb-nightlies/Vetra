"""
###############################################################################
#                                                                             #
#  Test for monitoring with non zero-suppressed data.                         #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Marius Bjoernstad ( Paul.Bjoernstad@hep.manchester.ac.uk )        #
#  @date    2010-06-14                                                        #
#                                                                             #
###############################################################################
"""

from Configurables import Vetra
from Gaudi.Configuration           import EventSelector


vetra = Vetra()

# Vetra will automatically load: options/Velo/VetraMoni_NZS.py
vetra.DataBanks = 'NZS'

# TELL1 Emulation is needed for NZS monitoring
vetra.RunEmulation = True

# Disable Beam-Beam filter: reference NZS is taken without beam
vetra.FilterBeamBeam = False

# Do not run error banks test, tested in separate file
vetra.CheckEBs = False
vetra.DataType = '2010'

vetra.EvtMax = 90

EventSelector().PrintFreq = 10


# Noisy algorithms, don't need to compare properties in detail
from Configurables import Velo__Monitoring__NoiseMon as NoiseMon
NoiseMon().PropertiesPrint = False





