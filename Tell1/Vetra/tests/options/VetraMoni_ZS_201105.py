"""
###############################################################################
#                                                                             #
#  Test for monitoring with zero-suppressed data. Includes everything but     #
#   error banks. Data taken in May 2011.                                      #
#                                                                             #
#  @package Tell1/Vetra                                                       #
#  @author  Marius Bjoernstad ( Paul.Bjoernstad@hep.manchester.ac.uk )        #
#  @date    2010-06-10                                                        #
#                                                                             #
###############################################################################
"""

from Gaudi.Configuration           import EventSelector
from Configurables import Vetra

vetra = Vetra()

# Setting DataBanks to "ZS" enables by default a set of ZS monitoring algos
# It causes Vetra to load: options/Velo/VetraMoni_ZS.py 
vetra.DataBanks = 'ZS'

# Do not run error banks test, tested in separate file
vetra.CheckEBs = False

from PRConfig import TestFileDB
TestFileDB.test_file_db["2011_RAW_FULL_LHCb_COLLISION11_92222_092222_0000000005.raw"].run()


# FULL stream from run #92222
vetra.DataType = '2011'

from Configurables import Vetra
Vetra().HistogramFile = "moni-zs-201105.root"


