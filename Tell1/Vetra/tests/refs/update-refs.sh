#!/bin/bash

echo This will replace the existing reference files with the newly generated 
echo ones. Are you sure that the new references are correct?

echo Press Enter to continue, or Ctrl-C to cancel...
read

for f in *
do
	if [ -e $f.new ] 
	then
		echo $f
		mv $f.new $f
	fi
done

echo Done.
