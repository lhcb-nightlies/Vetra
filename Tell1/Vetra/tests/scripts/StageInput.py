# Pre-load all the input data for the tests
# rfcp will write to standard error if there is an error, then 
# the test will fail

import os, sys, re, glob

def DoFile(v):
    ii = os.system("rfcp -s 1 " + v + " ./")
    base = re.findall("[^ /]*.raw", v)
    if len(base) > 0:
      os.remove(base[0])
    sys.exit(ii)

# Ensure that CASTOR has staged the input data for the tests
def StageInput():
    # Get all occurrences of 'rfio:/castor/cern.ch/input/file' in options files
    # specifically excluding comments
    castorfiles = []
    for f in glob.glob(os.environ["VETRAROOT"] + "/tests/options/*" % os.environ):
        for l in open(f).readlines():
            line = l.strip("\n")
            castorfile, n = re.subn("^[ ]*[^# ].*'castor:(.*?)'.*$", "\\1", line)
            if n > 0:
                castorfiles.append(castorfile)
    pids = []
    pid = 0
    for fname in castorfiles:
        pid = os.fork()
        if pid == 0:
            DoFile(fname)
            break
        else:
            pids.append(pid)
    
    if pid != 0:
      for pid in pids:
          os.waitpid(pid, 0)



StageInput()
