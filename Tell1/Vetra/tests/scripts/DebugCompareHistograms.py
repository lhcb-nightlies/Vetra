import sys
from Vetra.QMTest.CompareHistograms import CompareHistograms

# Use to test CompareHistograms on any root file + reference file    

if len(sys.argv) > 3:
    print "usage: python DebugCompareHistograms rootFile referenceFile"


class res(dict):
    def Quote(self, str):
        return str
causes = []
results = res()
res = CompareHistograms(causes, results, sys.argv[1], sys.argv[2])

print "Equal: ", res
print "Results:"
for (k,v) in results.items():
    print "  ", k, " = ", v
print "Causes:"
for c in causes:
    print "  ", c
    
