#============================================================================
# Created    : 2006-05-08
# Maintainer : Tomasz Szumlak
#
# Documentation on the requirement file can be found at
# http://cern.ch/lhcb-comp/Support/html/new_structure.pdf
#============================================================================
package           Vetra
version           v16r1

#============================================================================
# This template is an example for a standard Algorithm package
#
# Structure, i.e. directories to process. 
#============================================================================
branches  cmt doc options job scripts python VetraCondDB macros 

#============================================================================
# Used packages. Specify the version, * at the end specifies 'any revision'
#    Put as many lines as needed, with all packages, without the '#'
#============================================================================

# Set the default path for Vetra options.
set VETRAOPTS  $(VETRAROOT)/options

#-- python 

use GaudiPython v*

#-- External libraries taken from LCG, version defined by GaudiEnv
use RELAX    v* LCG_Interfaces    -no_auto_imports
use HepMC    v* LCG_Interfaces    -no_auto_imports

# Packages needed to resolve external dependencies at run time
use Python   v* LCG_Interfaces -no_auto_imports

#-- packages needed to compile and link 
use GaudiKernel                v*             -no_auto_imports
use GaudiConf                  v*             -no_auto_imports

#-- from Gaudi
use Gaudi                      v*             -no_auto_imports 
use RootHistCnv                v*             -no_auto_imports

#-- Databases
use DDDB                       v*     Det     -no_auto_imports
use ParamFiles                 v8r*           -no_auto_imports
use FieldMap                   v5r*           -no_auto_imports
use AppConfig                  v3r*           -no_auto_imports 
use VeloDet                    v*     Det     -no_auto_imports
use VeloSQLDDDB                v*     Det     -no_auto_imports

#-- LHCb libs
use KernelSys                  v*    Kernel   -no_auto_imports
use DetSys                     v*    Det      -no_auto_imports
use LHCbAlgs                   v*    Kernel   -no_auto_imports
use EventSys                   v*    Event    -no_auto_imports  
use DAQSys                     v*    DAQ      -no_auto_imports
use VeloEvent                  v*    Event    -no_auto_imports

# The whole REC and Lbcom projects
use RecSys                     v*             -no_auto_imports
use LbcomSys                   v*             -no_auto_imports

# Vetra packages - put explicitely here to avoid double dependency
use TELL1Engine                v*        Tell1 #
use VeloTELL1Event             v*        Velo  #
use VeloTELL1Algorithms        v*        Velo  #
use VeloTELL1Tools             v*        Velo  #
use VetraKernel                v*        Tell1 #
use VetraAnalysis              v*        Velo  #
use VeloTELL1DevelAlgorithms   v*        Velo  #

# monitoring packages
use VeloDataMonitor            v*        Velo #
use VeloTELL1Checkers          v*        Velo #

# ST packages
use STTELL1Algorithms          v*        ST   #
use STVetraAnalysis            v*        ST   #
use STSQLDDDB                  v*     Det     -no_auto_imports #

#-- Other packages
use MDF                        v*    DAQ

apply_pattern application_path

# ================================== Install configurable =============================
apply_pattern install_python_modules

# ============ Export application name and version to gaudirun.py =====================
apply_pattern GaudiApp

#
private
use GaudiObjDesc v* -no_auto_imports
# needed for PRConfig loading of qmtest files
use PRConfig v*

# ===================================== Include QMTests ===============================
apply_pattern QMTest
path_prepend PYTHONPATH "" QMTest "${VETRAROOT}/tests/qmtest"
