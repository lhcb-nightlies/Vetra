################################################################################
# Package: Vetra
################################################################################
gaudi_subdir(Vetra v18r0)

gaudi_depends_on_subdirs(DAQ/DAQSys
                         DAQ/MDF
                         Det/DDDB
                         Det/DetSys
                         Det/VeloDet
                         Event/EventSys
                         Event/VeloEvent
                         Gaudi
                         GaudiConf
                         GaudiKernel
                         GaudiObjDesc
                         GaudiPython
                         Kernel/KernelSys
                         Kernel/LHCbAlgs
                         LbcomSys
                         RecSys
                         RootHistCnv
                         ST/STTELL1Algorithms
                         ST/STVetraAnalysis
                         Tell1/TELL1Engine
                         Tell1/VetraKernel
                         Velo/VeloDataMonitor
                         Velo/VeloTELL1Algorithms
                         Velo/VeloTELL1Checkers
                         Velo/VeloTELL1DevelAlgorithms
                         Velo/VeloTELL1Event
                         Velo/VeloTELL1Tools
                         Velo/VetraAnalysis)

find_package(HepMC)
find_package(RELAX)

gaudi_install_python_modules()

gaudi_env(SET VETRAOPTS \${VETRAROOT}/options)


gaudi_add_test(QMTest QMTEST)
