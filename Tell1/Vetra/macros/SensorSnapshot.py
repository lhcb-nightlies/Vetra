from ROOT import *


gROOT.SetStyle("Plain")

def SensorSnapshot(RootFN,Tell1No=0):


    #Check filename. Add .root if necessary:
    if RootFN[-5:] != ".root":
      RootFN = RootFN + ".root"

    # Open ROOT file

    #I removed the preset path, the user has to give the full pathname himself"
    f=TFile(RootFN)
    if f.IsOpen() == 0:
        print "Could not open " + RootFN
        return 

    #Auto detect the TELL1 Number from the directory structure:
    detected = False
    dir = f.Get("/Vetra/PedestalMon")
    if dir:
     if (dir.GetListOfKeys()).GetEntries() == 1 and Tell1No == 0:
	 name = ((dir.GetListOfKeys()).At(0)).GetName()
         if name[:4] == "TELL" :
             Tell1No = int(name[-3:])
             detected = True           
    else:
        print "Can't find proper directory structure. QUITTING. Check your file name."
        return
    

    if detected == True:
        print "TELL1 %d was found in the file."%Tell1No  


    #Here's the histogram name for the relevant pedestal plot:
    h1=f.Get("/Vetra/PedestalMon/TELL1_" + str(Tell1No).zfill(3) +"/SubtractedPedestals_vs_ChipChannel")

    #Is the histogram working?
    if not h1:
      print "Can't open histogram, QUITTING.  Are you sure that the filename is correct?"
      return
    
    h1.SetMaximum(570)
    h1.SetMinimum(470)
    #Removed an underscore from title (and for all other histograms): 
    h1.SetTitle("Pedestals Tell1 %d"%(Tell1No))
    h1.GetXaxis().SetTitle("Chip Channel")
    h1.GetYaxis().SetTitle("Pedestal")

    #Use this histogram name:
    h3=f.Get("/Vetra/Noise/SubtractedPedADCs/TELL1_" + str(Tell1No).zfill(3) +"/RMSNoise_vs_ChipChannel")
    h3.SetTitle("Noise Vs Chip Channel Tell1 %d"%(Tell1No))
    h3.GetXaxis().SetTitle("Chip Channel")
    #Changed histogram label:
    h3.GetYaxis().SetTitle("Noise [ADC counts]")

    #Changed limits:
    h3.SetMaximum(5.)
    h3.SetMinimum(-0.1)
    
    maxVal=float(h3.GetBinContent(h3.GetMaximumBin()))
    
    h2=TH1F("Noise Distribution","Noise Distribution",100,0.,maxVal)
    h2.SetTitle("Noise Distribution Tell1 %d"%(Tell1No))
    #Changed histogram axis label:
    h2.GetXaxis().SetTitle("ADC counts")
    h2.GetYaxis().SetTitle("Frequency")
    h2.GetXaxis().SetRangeUser(0,5)   

       
    nBins=h3.GetNbinsX()
    
    for BinNo in range(1,nBins+1):
        binContent = float(h3.GetBinContent(BinNo))
        if binContent:                       
            h2.Fill(binContent)
            

    #histogram name:
    h4=f.Get("/Vetra/Noise/ADCCMSuppressed/TELL1_%s/RMSNoise_vs_ChipChannel"% str(Tell1No).zfill(3))

    #histogram name:
    h5=f.Get("/Vetra/Noise/SubtractedPedADCs/TELL1_%s/RMSNoise_vs_Strip"% str(Tell1No).zfill(3))
    h5.SetTitle("Noise Vs Strip Tell1_%d"%(Tell1No))
    h5.GetXaxis().SetTitle("Strip")
    #Changed title:
    h5.GetYaxis().SetTitle("Noise [ADC counts]")
    #Changed axis limits:
    h5.SetMaximum(5.)
    h5.SetMinimum(-0.1)

    #histogram name:
    h6=f.Get("/Vetra/Noise/ADCCMSuppressed/TELL1_%s/RMSNoise_vs_Strip"% str(Tell1No).zfill(3))
    
    c1= TCanvas("c1","Ped & Noise Dist",0,0,600,900)
    
    pad11=TPad("p11","p1",0,0.97,1.0,1.0)
    pad12= TPad("p12","p2",0,0.485,1.0,0.97)
    pad13=TPad("p13","p3",0,0,1.0,0.485)
    pad11.Draw()
    pad12.Draw()
    pad13.Draw()
    
    pad11.cd()
    pt1 = TPaveText(0.2,0.2,0.9,0.9,"NDC")
    ttext = pt1.AddText(RootFN)
    pt1.SetBorderSize(0)
    pt1.SetFillColor(0)
    pt1.Draw()
    
    pad12.cd()
    h1.Draw("histo")
    pad13.cd();
    h2.SetFillColor(8)
    h2.Draw("histo")
    
    c2= TCanvas("c2","Noise",0,0,600,900)
    
    pad21=TPad("p21","p1",0,0.97,1.0,1.0)
    pad22= TPad("p22","p2",0,0.485,1.0,0.97)
    pad23=TPad("p23","p3",0,0,1.0,0.485)
    pad21.Draw()
    pad22.Draw()
    pad23.Draw()
    
    pad21.cd()
    pt2 = TPaveText(0.2,0.2,0.9,0.9,"NDC")
    ttext = pt2.AddText(RootFN)
    pt2.SetBorderSize(0)
    pt2.SetFillColor(0)
    pt2.Draw()
    
    h3.SetLineColor(4)
    h4.SetLineColor(2)
    h5.SetLineColor(4)
    h6.SetLineColor(2)
    
    pad22.cd()
    h3.Draw("histo")
    h4.Draw("hist,same")
    pad23.cd();
    h5.Draw("histo")
    h6.Draw("hist,same")
    
    
    pad13.Update()            
    pad12.Update()            
    pad23.Update()            
    pad22.Update()            
    c1.Update()            
    c2.Update()            

    #The output filename now becomes filename.ps stripped from the full path:
    #i.e. /user/myfile.root becomes myfile_TELL1_25.ps
    
    psTitle = RootFN[RootFN.rfind('/')+1:-5]+"_TELL1_%d" % Tell1No
    
    savefile = raw_input("Save histograms to file %s.ps? (y/n): "%psTitle )

       # print psTitle
    if savefile in ('y', 'Y'):  
        c1.Print("%s.ps("%(psTitle))
        c2.Print("%s.ps)"%(psTitle))        
       
    #Anything else quits:
    else:
        print 'Histograms NOT saved'
    
        
    
    
