void cableTest(Char_t inFile[], Char_t Tell1No[],TString option="") // the function and its inputs?? 
{
  
  
  // Initiate chars
  
  char CableTest_[100];
  char rootFileName[100];
  
  // Open ROOT file and access (sub)directories
  
  sprintf(rootFileName,"%s.root",inFile);
  TFile *ff=TFile::Open(rootFileName);
  if ( ff == NULL ) {
    cout << " No such file!" << endl; 
    return;
  }
  
  
  TDirectory *tdVetra = ff->Get("Vetra");
  if ( tdVetra == NULL ) {
    cout << " No Vetra directory " << endl;
    return;
  }
  
  TDirectory *tdFullDataMonitorTELL1Test = tdVetra->Get("FullDataMonitorTELL1Test");
  if ( tdFullDataMonitorTELL1Test == NULL ) {
    cout <<  " No FullDataMonitorTELL1Test directory " << endl;
    return;
  }

  // Setup TStrings for histo names
  
  TString tell1=Tell1No;
  TString h1name="LinkNumberVsBitPattern1_Tell1_";
  h1name += tell1;
  
  TString h2name="LinkNumberVsBitPattern2_Tell1_";
  h2name += tell1;
  
  TString h3name="BitPattern2VsBitPattern1_Tell1_";
  h3name += tell1;
  
  TString h4name="HybridNumber1VsLink_Tell1_";
  h4name += tell1;
  
  TString h5name="HybridNumber2VsLink_Tell1_";
  h5name += tell1;
  
  TString h6name="HybridNumber2VsHybridNumber1_Tell1_";
  h6name += tell1;
  
  // setup canvas and titles for Link Number
  
  TH2D *hCableTest1=tdFullDataMonitorTELL1Test->Get(h1name);
  TH2D *hCableTest2=tdFullDataMonitorTELL1Test->Get(h2name);
  TH2D *hCableTest3=tdFullDataMonitorTELL1Test->Get(h3name);
  
  TCanvas *c1 = new TCanvas("c1", "Link Number", 600,900);
  
  hCableTest1->GetXaxis()->SetTitle("Bit Pattern 1");
  hCableTest1->GetYaxis()->SetTitle("Link Number");
  hCableTest2->GetXaxis()->SetTitle("Bit Pattern 2");
  hCableTest2->GetYaxis()->SetTitle("Link Number");
  hCableTest3->GetXaxis()->SetTitle("Bit Pattern 1");
  hCableTest3->GetYaxis()->SetTitle("Bit Pattern 2");
  
  // divide canvas and draw histos for  Link Number
 
  c1->Divide(1,2);
  c1->cd(1)->Divide(2,1);
  
  c1->cd(1)->cd(1);
  hCableTest1->Draw(option);
  
  c1->cd(1)->cd(2);
  hCableTest2->Draw(option);
  
  c1->cd(2);
  hCableTest3->Draw(option);
  
  // setup canvas and titles for plot Hybrid Number
  
  TH2D *hCableTest4=tdFullDataMonitorTELL1Test->Get(h4name);
  TH2D *hCableTest5=tdFullDataMonitorTELL1Test->Get(h5name);
  TH2D *hCableTest6=tdFullDataMonitorTELL1Test->Get(h6name);
  
  TCanvas *c2 = new TCanvas("c2", "Hybrid Number", 600,900);
  
  hCableTest4->GetXaxis()->SetTitle("Link Number");
  hCableTest4->GetYaxis()->SetTitle("Hybrid Number 1");
  hCableTest5->GetXaxis()->SetTitle("Link Number");
  hCableTest5->GetYaxis()->SetTitle("Hybrid Number 2");
  hCableTest6->GetXaxis()->SetTitle("Hybrid Number 1");
  hCableTest6->GetYaxis()->SetTitle("Hybrid Number 2");
  
  hCableTest4->SetTitleOffset(1.3,"y"); //adjust axis title 
  hCableTest5->SetTitleOffset(1.3,"y"); //adjust axis title 

  // divide canvas and draw histos for Hybrid Number
  
  c2->Divide(1,2);
  c2->cd(1)->Divide(2,1);
  
  c2->cd(1)->cd(1);
  hCableTest4->Draw(option);
  
  c2->cd(1)->cd(2);
  hCableTest5->Draw(option);
  
  c2->cd(2);
  hCableTest6->Draw(option);
  
  // fix stats box to only show no of entries
  
  gStyle->SetOptStat(10);
  
  // print to poscript file
  
  TString psTitle="CableTest_";
  psTitle += tell1;
  
  c1->Print(Form("%s.ps(",psTitle.Data()));
  c2->Print(Form("%s.ps)",psTitle.Data()));
  
  // convert to pdf
  
  gSystem->Exec(Form("ps2pdf %s.ps",psTitle.Data()));
  
  
  
}
