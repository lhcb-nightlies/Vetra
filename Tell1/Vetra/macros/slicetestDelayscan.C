void slicetestDelayscan(Char_t inFile[], Int_t tell1Id, Bool_t print=true, Bool_t quit=false)
{
  Char_t histBaseName[] = "hSamplingPhaseScan";
  // Char_t histBaseName[] = "hSamplingPhasePedestal";
  Int_t flagProject2d = 1;
  // number of links
  const Int_t nLinks = 64;

  // number of links per plot (has to be a divisor of 64)
  const Int_t nLinksPerPlot = 16;
  const Int_t nPlots = nLinks/nLinksPerPlot;
  // definition of the color for each link
  Int_t colorTable[nLinksPerPlot] ={50,41,38,30,28,25,15,9,8,7,6,5,4,3,2,1}

  Int_t arxCard, iBeetle, iLink, pad;
  Char_t dirName[50], rootFileName[400], histName[50], plotBaseName[400], plotName[400], padName[50], legendName[50];
  TH1D *hSamplingPhaseEven, *hSamplingPhaseOdd, *hSamplingPhase;
  TH2D *h2dSamplingPhaseEven, *h2dSamplingPhaseOdd, *h2dSamplingPhase;
  
  TCanvas *tempCanvas;
  TPad *tempPad;
  TPad *tempCanvas_1, tempCanvas_2, tempCanvas_3, tempCanvas_4;
  TString canvasString;
  TLegend *legend; 
  
  style(); 

  // Open ROOT file and access (sub)directories

  sprintf(rootFileName,"%s.root",inFile);
  TFile *ff=TFile::Open(rootFileName);
  if ( ff == NULL ) {
    cout << " No such file!" << endl;
    return;
  }
 
  TDirectory *tdVetra = ff->Get("Vetra");
  if ( tdVetra == NULL ) {
    cout << " No Vetra directory " << endl;
    return;
  }
 
  sprintf(dirName, "PhaseScan");
  TDirectory *tdPhaseScan = tdVetra->Get(dirName);
  if ( tdPhaseScan == NULL ) {
    cout << " Directory " << dirName << " does not exist" << endl;
    return;
  }

  sprintf(dirName, "TELL1_%03d", tell1Id);
  TDirectory *tdTell1 = tdPhaseScan->Get(dirName);
  if ( tdTell1 == NULL ) {
    cout << " Directory " << dirName << " does not exist" << endl;
    return;
  }

  // fill difference of even and odd channels to TClonesArray

  TObjArray histArray(nLinks);

  for(Int_t i=0; i<nLinks; i++) {
    if (flagProject2d == 0) {
      sprintf(histName, "%sEven_%d", histBaseName, i);
      hSamplingPhaseEven = (TH1D*) tdTell1->Get(histName);
      sprintf(histName, "%sOdd_%d", histBaseName, i);
      hSamplingPhaseOdd = (TH1D*) tdTell1->Get(histName);
    } else {
      sprintf(histName, "%sEven_Link_%d", histBaseName, i);
      h2dSamplingPhaseEven = (TH2D*) tdTell1->Get(histName);
      sprintf(histName, "%sOdd_Link_%d", histBaseName, i);
      h2dSamplingPhaseOdd = (TH2D*) tdTell1->Get(histName);
      hSamplingPhaseEven = h2dSamplingPhaseEven->ProfileX("h2dProfxEven");
      hSamplingPhaseOdd = h2dSamplingPhaseOdd->ProfileX("h2dProfxOdd");
    }
    sprintf(histName, "%sEvenMinusOdd_%d", histBaseName, i);
    hSamplingPhase = addHistograms(histName, hSamplingPhaseEven, hSamplingPhaseOdd, 1., -1.);
    histArray.AddAt(hSamplingPhase,i);
  }

  // determine minimum and maximum of each group of links in one plot
  
  Float_t actMin, actMax;
  Float_t minPlot[nPlots], maxPlot[nPlots];

  for (Int_t i=0; i<nPlots; i++) {
    minPlot[i]=0;
    maxPlot[i]=0;
  }
  
  for (Int_t i=0; i<nPlots; i++) {
    for (Int_t j=0; j<nLinksPerPlot; j++) {
      hSamplingPhase = (TH1D*) histArray[(i*nLinksPerPlot+j)];
      actMin = hSamplingPhase->GetMinimum();
      actMax = hSamplingPhase->GetMaximum();
      if (actMin < minPlot[i]) {
	minPlot[i] = actMin;
      }
      if (actMax > maxPlot[i]) {
	maxPlot[i] = actMax;
      }
    }
  }

  // determine optimal delay

  //  const Int_t iPulsedChannel=23, iStartChannel=18, iEndChannel=28, iWindow=1, iOptimalShiftFromPlateauCenter=2;
  const Int_t iPulsedChannel=23, iStartChannel=18, iEndChannel=31, iWindow=1, iOptimalShiftFromPlateauCenter=2;
  Int_t iStartBin, iEndBin, nSamplingPerChannel, iExtremumBin, iRisingEdge, iFallingEdge, iPlateauBin, iCenterPlateauExpectedBin, iOptimalDelay;
  Float_t centerPlateauExpected=22.5;
  Float_t binValue, extremumValue, halfHeight, binValueNext;
  
  TH1F *hOptimalDelay = new TH1F("hOptimalDelay", "optimal delay per link", nLinks, -0.5, (((Float_t) nLinks)-0.5));
  
  for (Int_t i=0; i<nLinks; i++) {
    hSamplingPhase = (TH1D*) histArray[i];
    if (i == 0) {
      iStartBin = hSamplingPhase->GetXaxis()->FindBin((Float_t) iStartChannel);
      iEndBin = hSamplingPhase->GetXaxis()->FindBin((Float_t) iEndChannel);
      nSamplingPerChannel = (iEndBin - iStartBin)/(iEndChannel - iStartChannel);
      iCenterPlateauExpectedBin = hSamplingPhase->GetXaxis()->FindBin(centerPlateauExpected);
      // cout << "iStartBin: " << iStartBin << ", iEndBind: " << iEndBin << ", nSamplingPerChannel: " << nSamplingPerChannel << endl;
    }
	
    // find maximum or minimum

    extremumValue = 0;
    iExtremumBin = -1;
    for (Int_t j=iStartBin; j<=iEndBin; j++) {
      binValue = TMath::Abs(hSamplingPhase->GetBinContent(j));
      if (binValue > extremumValue) {
	extremumValue = binValue;
	iExtremumBin = j;
      }
    }
    // cout << "iExtremumBin: " << iExtremumBin << ", extremumValue: " << extremumValue << endl;

    // find rising and falling edge

    halfHeight = extremumValue/2.;
    iRisingEdge = -1;
    iFallingEdge = -1;
    for (Int_t j=(iExtremumBin-iWindow*nSamplingPerChannel); j<=(iExtremumBin+iWindow*nSamplingPerChannel); j++) {
      binValue = TMath::Abs(hSamplingPhase->GetBinContent(j));
      binValueNext = TMath::Abs(hSamplingPhase->GetBinContent(j+1));
      if ((binValue <= halfHeight) && (binValueNext >= halfHeight)) {
	iRisingEdge = j;
      }
      if ((binValue >= halfHeight) && (binValueNext <= halfHeight)) {
	iFallingEdge = j;
      }
    }
    iPlateauBin = (Int_t) round(((((Double_t) iRisingEdge) + ((Double_t) iFallingEdge))/2.),0);
    // cout << "iRisingEdge: " << iRisingEdge << ", iFallingEdge: " << iFallingEdge << ", iExtremumBin: " << iExtremumBin << ", iPlateauBin: " << iPlateauBin << endl;
    iOptimalDelay = iPlateauBin - (iCenterPlateauExpectedBin - iOptimalShiftFromPlateauCenter);
    // cout << "iOptimalDelay: " << iOptimalDelay << endl;
    hOptimalDelay->SetBinContent(i,((Float_t) iOptimalDelay));
    hOptimalDelay->SetBinError(i,0.0001);
  } 

  TCanvas *c1 = new TCanvas("c1", "Optimal Delay Per Link", 400, 300);
  sprintf(plotBaseName,"%s_optimaldelay",inFile);

  TPaveText *ptc2 = new TPaveText(0.1,0.01,0.9,0.05,"NDC");
  TText *tc2 = ptc2->AddText(plotBaseName);
  ptc2->SetBorderSize(0);
  ptc2->SetFillColor(0);

  hOptimalDelay->SetMarkerStyle(20);
  hOptimalDelay->SetMarkerSize(0.5);
  hOptimalDelay->Draw("E");
  hOptimalDelay->GetXaxis()->SetTitle("link");
  hOptimalDelay->GetYaxis()->SetTitle("optimal delay [phase delay]");
  ptc2->Draw();
  c1->Update();
	
  // average to have one delay setting per PP-FPGA

  Int_t optDelayPerPpfpga[nPlots];
  Double_t ppfpgaAverage, delayValue;
  TLine *averageLine;
  FILE *filePtr;
  sprintf(plotName,"%s.txt",plotBaseName);
  filePtr = fopen(plotName,"w");
  fprintf(filePtr, "Change digitisation delay in the following way:\n");
  fprintf(filePtr, "\n");
  for (Int_t i=0; i<nPlots; i++) {
    ppfpgaAverage = 0.;
    for (Int_t j=0; j<nLinksPerPlot; j++) {
      delayValue = hOptimalDelay->GetBinContent(i*nLinksPerPlot+j);
      //      if ((delayValue > -30.) && (delayValue < 50.)) {
      if ((delayValue > -30.) && (delayValue < 150.)) {
	ppfpgaAverage += hOptimalDelay->GetBinContent(i*nLinksPerPlot+j);
      } else {
	fprintf(filePtr, "Link %d excluded from average, delay value: %f\n", i*nLinksPerPlot+j, delayValue);
      }
    }
    ppfpgaAverage = ppfpgaAverage/((Double_t) nLinksPerPlot);
    iPpfpga = 3-i;
    optDelayPerPpfpga[iPpfpga] = (Int_t) round(ppfpgaAverage,0);
    fprintf(filePtr, "PP-FPGA %d: cycle delay: %1d, phase delay: %2d\n",iPpfpga,optDelayPerPpfpga[iPpfpga]/nSamplingPerChannel,optDelayPerPpfpga[iPpfpga]%nSamplingPerChannel);
    averageLine = new TLine(-0.5+((Double_t) i*nLinksPerPlot),((Double_t) optDelayPerPpfpga[iPpfpga]),-0.5+((Double_t)(i+1)*nLinksPerPlot),((Double_t) optDelayPerPpfpga[iPpfpga]));
    averageLine->SetLineColor(kRed);
    averageLine->Draw();
  }
  fclose(filePtr);
  gSystem->Exec(Form("cat %s",plotName));

  c1->Update();
  if(print){
    sprintf(plotName,"%s.ps",plotBaseName);
    c1->Print(plotName);
    sprintf(plotName,"%s.png",plotBaseName);
    c1->Print(plotName);
  }

  // write optimal delays to xml file

  Int_t iCableDelay = 16; // cable delay used during data taking
  Int_t optCableDelay;
  Int_t optCycleDelayPerPpfpga[nPlots];
  Int_t optPhaseDelayPerPpfpga[nPlots];

  // convert delay settings of TELL1 firmware 2.3 to 2.5

  const Int_t deltaPhaseDelay = 129; // new phase delay - old phase delay

  /*

  for (Int_t i=0; i<nPlots; i++) {
      optDelayPerPpfpga[i] += deltaPhaseDelay;
  } 

  */

  // take care of negative delays
  // (find the minimum delay and adjust for this by the cable delay if negative)
  Int_t iMinimumDelay=9999999;
  for (Int_t i=0; i<nPlots; i++) {
    if (optDelayPerPpfpga[i] < iMinimumDelay) {
      iMinimumDelay = optDelayPerPpfpga[i];
    }
  }

  if (iMinimumDelay < 0) {
    iCableDelay = iCableDelay + (iMinimumDelay/nSamplingPerChannel-1);
    for (Int_t i=0; i<nPlots; i++) {
      optDelayPerPpfpga[i] = optDelayPerPpfpga[i] - (iMinimumDelay/nSamplingPerChannel-1)*nSamplingPerChannel;    }
  }

  for (Int_t i=0; i<nPlots; i++) {
    iPpfpga = 3-i;
    optCycleDelayPerPpfpga[iPpfpga] = optDelayPerPpfpga[iPpfpga]/nSamplingPerChannel;
    optPhaseDelayPerPpfpga[iPpfpga] = optDelayPerPpfpga[iPpfpga]%nSamplingPerChannel;
  }

  // absorb cycle delays larger than desired cycle delay in cable delay

  Int_t iMaximumCycleDelay=-1;
  for (Int_t i=0; i<nPlots; i++) {
    if (optCycleDelayPerPpfpga[iPpfpga] > iMaximumCycleDelay) {
       iMaximumCycleDelay = optCycleDelayPerPpfpga[iPpfpga];
    }
  }

  if (iMaximumCycleDelay > 1) {
    iCableDelay = iCableDelay + (iMaximumCycleDelay-1);
    for (Int_t i=0; i<nPlots; i++) {
      optCycleDelayPerPpfpga[i] = optCycleDelayPerPpfpga[i] - (iMaximumCycleDelay-1);
    }
  }

  optCableDelay = iCableDelay;

  sprintf(plotName,"%s.xml",plotBaseName);
  // sprintf(plotName,"digitisationDelays_tell1_%d.xml",tell1Id);
  filePtr = fopen(plotName,"w");
  fprintf(filePtr, "<?xml version=%c1.0%c encoding=%cUTF-8%c?>\n",34,34,34,34);
  fprintf(filePtr, "<!DOCTYPE DDDB SYSTEM %cconddb:/DTD/structure.dtd%c>\n",34,34);
  fprintf(filePtr, "<DDDB>\n");
  fprintf(filePtr, "	<condition name=%cDigitisationDelays%c>\n",34,34);
  fprintf(filePtr, "		<param name=%cCableDelay%c type=%cint%c>\n",34,34,34,34);
  fprintf(filePtr, "		%d\n", optCableDelay);
  fprintf(filePtr, "		</param>\n");
  fprintf(filePtr, "		<paramVector name=%cCycleDelays%c type=%cint%c>\n",34,34,34,34);
  fprintf(filePtr, "		");
  for (Int_t i=0; i<nLinks; i++) {
    fprintf(filePtr, "%d ", optCycleDelayPerPpfpga[3-(i/nSamplingPerChannel)]);
  }
  fprintf(filePtr, "\n");
  fprintf(filePtr, "		</paramVector>\n");
  fprintf(filePtr, "		<paramVector name=%cPhaseDelays%c type=%cint%c>\n",34,34,34,34);
  fprintf(filePtr, "		");
  for (Int_t i=0; i<nLinks; i++) {
    fprintf(filePtr, "%d ", optPhaseDelayPerPpfpga[3-(i/nSamplingPerChannel)]);
  }
  fprintf(filePtr, "\n");
  fprintf(filePtr, "		</paramVector>\n");
  fprintf(filePtr, "	</condition>\n");
  fprintf(filePtr, "</DDDB>\n");
  fclose(filePtr);

  // plot number of events per delay

  /*

  TCanvas *c2 = new TCanvas("c2", "Events per Delay", 400, 300);
  sprintf(plotBaseName,"%s_eventsperdelay",inFile);
  hSamplingPhase = (TH1D*) tdTell1->Get("hSamplingPhase");
  hSamplingPhase->SetTitle("events per delay");
  hSamplingPhase->GetXaxis()->SetTitle("phase delay");
  hSamplingPhase->GetYaxis()->SetTitle("#events");
  hSamplingPhase->DrawCopy("histo");
  c1->Update();
  TLine *sepFirstDummy = new TLine(hSamplingPhase->GetXaxis()->GetXmin()+1,c1->GetUymin(),hSamplingPhase->GetXaxis()->GetXmin()+1,c1->GetUymax());
  sepFirstDummy->SetLineColor(kRed);
  sepFirstDummy->SetLineStyle(2);
  sepFirstDummy->Draw();
  TLine *sepLastDummy = new TLine(hSamplingPhase->GetXaxis()->GetXmax()-1,c1->GetUymin(),hSamplingPhase->GetXaxis()->GetXmax()-1,c1->GetUymax());
  sepLastDummy->SetLineColor(kRed);
  sepLastDummy->SetLineStyle(2);
  sepLastDummy->Draw();
  TPaveText *ptc1 = new TPaveText(0.1,0.01,0.9,0.05,"NDC");
  TText *tc1 = ptc1->AddText(plotBaseName);
  ptc1->SetBorderSize(0);
  ptc1->SetFillColor(0);
  ptc1->Draw();
  c2->Update();
  if(print){
    sprintf(plotName,"%s.ps",plotBaseName);
    c2->Print(plotName);
    sprintf(plotName,"%s.png",plotBaseName);
    c2->Print(plotName);
  }

  */

  Double_t xLine;
  TLine *optimalDelayLine;
  
  for (Int_t j=0; j<3; j++) {

    switch(j)
      {
      case 0:
	TCanvas *c3 = new TCanvas("c3", "Digitisation Delay Scan", 800, 600);
	c3->Divide(2,2);
	TPad* c3_1 = (TPad *)(c3->GetPrimitive("c3_1"));
	TPad* c3_2 = (TPad *)(c3->GetPrimitive("c3_2"));
	TPad* c3_3 = (TPad *)(c3->GetPrimitive("c3_3"));
	TPad* c3_4 = (TPad *)(c3->GetPrimitive("c3_4"));
	c3_1->SetGrid();
	c3_2->SetGrid();
	c3_3->SetGrid();
	c3_4->SetGrid();
	tempCanvas = c3;
	sprintf(plotBaseName,"%s_delay",inFile);
	break;
      case 1:
	TCanvas *c4 = new TCanvas("c4", "Digitisation Delay Scan Zoom TP4", 800, 600);
	c4->Divide(2,2);
	TPad* c4_1 = (TPad *)(c4->GetPrimitive("c4_1"));
	TPad* c4_2 = (TPad *)(c4->GetPrimitive("c4_2"));
	TPad* c4_3 = (TPad *)(c4->GetPrimitive("c4_3"));
	TPad* c4_4 = (TPad *)(c4->GetPrimitive("c4_4"));
	c4_1->SetGrid();
	c4_2->SetGrid();
	c4_3->SetGrid();
	c4_4->SetGrid();
	tempCanvas = c4;
	sprintf(plotBaseName,"%s_delay_zoomtp4",inFile);
	break;
      case 2:
	TCanvas *c5 = new TCanvas("c5", "Digitisation Delay Scan Zoom TP23", 800, 600);
	c5->Divide(2,2);
	TPad* c5_1 = (TPad *)(c5->GetPrimitive("c5_1"));
	TPad* c5_2 = (TPad *)(c5->GetPrimitive("c5_2"));
	TPad* c5_3 = (TPad *)(c5->GetPrimitive("c5_3"));
	TPad* c5_4 = (TPad *)(c5->GetPrimitive("c5_4"));
	c5_1->SetGrid();
	c5_2->SetGrid();
	c5_3->SetGrid();
	c5_4->SetGrid();
	tempCanvas = c5;
	sprintf(plotBaseName,"%s_delay_zoomtp23",inFile);
	break;
      }

    for(Int_t i=0; i<nLinks; i++){
      /* WRONG DEFINITION USED IN ASSEMBLY LAB...
	 iPpfpga = (Int_t)(i/16);
	 iBeetle = (Int_t)(i/4);
	 iLink = (Int_t)((i-iBeetle*4));
      */
      //CORRECT DEFINITION OF PPFPGA,BEETLE,LINK:
      iLink = i;
      iPpfpga = 3-i/16 ;
      iBeetle = i/4;


      pad = i/nLinksPerPlot+1;
      hSamplingPhase = (TH1D*) histArray[i];
      hSamplingPhase->SetLineColor(colorTable[i%nLinksPerPlot]);
      if ((i%nLinksPerPlot) == 0) {
        sprintf(histName, "PP-FPGA %d", iPpfpga);
        hSamplingPhase->SetTitle(histName);
        switch(j) {
        case 0:
          hSamplingPhase->SetMinimum(1.1*minPlot[(pad-1)]);
          hSamplingPhase->SetMaximum(1.1*maxPlot[(pad-1)]);
          break;
        case 1:
          hSamplingPhase->GetXaxis()->SetRangeUser(1.,6.);
          hSamplingPhase->SetMinimum(-100.); 
          hSamplingPhase->SetMaximum(200.); 
          break;
        case 2:
          hSamplingPhase->GetXaxis()->SetRangeUser(20.,25.);
          hSamplingPhase->SetMinimum(-200.); 
          hSamplingPhase->SetMaximum(100.); 
          break;
        }
        hSamplingPhase->GetXaxis()->SetTitle("channel");
        hSamplingPhase->GetYaxis()->SetTitle("ADC counts");
        tempCanvas->cd(pad);
        // cout << tempCanvas->GetName() << " " << pad << endl;
        hSamplingPhase->DrawCopy("histo");
        tempCanvas->Update();
        sprintf(padName, "c%d_%d", j+3, pad);
        tempPad = (TPad *)(tempCanvas->GetPrimitive(padName));
        switch(j) {
        case 0:
          legend = new TLegend((tempPad->GetUxmin()+0.82*(tempPad->GetUxmax()-tempPad->GetUxmin())), (tempPad->GetUymin()+0.6*(tempPad->GetUymax()-tempPad->GetUymin())), (tempPad->GetUxmin()+0.97*(tempPad->GetUxmax()-tempPad->GetUxmin())), (tempPad->GetUymin()+0.97*(tempPad->GetUymax()-tempPad->GetUymin())),"","");
          break;
        case 1:
          legend = new TLegend((tempPad->GetUxmin()+0.82*(tempPad->GetUxmax()-tempPad->GetUxmin())), (tempPad->GetUymin()+0.6*(tempPad->GetUymax()-tempPad->GetUymin())), (tempPad->GetUxmin()+0.97*(tempPad->GetUxmax()-tempPad->GetUxmin())), (tempPad->GetUymin()+0.97*(tempPad->GetUymax()-tempPad->GetUymin())),"","");
          break;
        case 2:
          legend = new TLegend((tempPad->GetUxmin()+0.82*(tempPad->GetUxmax()-tempPad->GetUxmin())), (tempPad->GetUymin()+0.06*(tempPad->GetUymax()-tempPad->GetUymin())), (tempPad->GetUxmin()+0.97*(tempPad->GetUxmax()-tempPad->GetUxmin())), (tempPad->GetUymin()+0.43*(tempPad->GetUymax()-tempPad->GetUymin())),"","");
          break;
        }
        legend->SetFillColor(0);
        legend->SetBorderSize(1);
      } else {
        hSamplingPhase->DrawCopy("histosame");
      }
      sprintf(legendName, "Beetle %d, Link %d", iBeetle, iLink);
      legend->AddEntry(hSamplingPhase,legendName,"l");
    
      if ((i%nLinksPerPlot) == (nLinksPerPlot-1)) {
	if (j == 2) {
	  for (Int_t k=-1; k<=1; k++) {
	    xLine = hSamplingPhase->GetXaxis()->GetBinCenter(iCenterPlateauExpectedBin + optDelayPerPpfpga[iPpfpga]) + ((Double_t) k);
	    optimalDelayLine = new TLine(xLine,-200.,xLine,100.);
	    if (k==0) {
	      optimalDelayLine->SetLineStyle(1);
	    } else {
	      optimalDelayLine->SetLineStyle(2);
	    }
	    optimalDelayLine->SetLineColor(kRed);
	    optimalDelayLine->Draw();
	  }
        legend->Draw();
	}
        if (iPpfpga == 3) {
          TPaveText *ptc1 = new TPaveText(0.1,0.01,0.9,0.05,"NDC");
          TText *tc1 = ptc1->AddText(plotBaseName);
          ptc1->SetBorderSize(0);
          ptc1->SetFillColor(0);
          ptc1->Draw();
        }
        tempCanvas->Update();
        // cin.get();
      }
    }
    if(print==true){
      sprintf(plotName,"%s.ps",plotBaseName);
      tempCanvas->Print(plotName);
      sprintf(plotName,"%s.png",plotBaseName);
      tempCanvas->Print(plotName);
    }
  }
  if (quit) {
    gROOT->ProcessLine(".q");
  }
}

TH1D* addHistograms(Char_t addHistName[], TH1D *h1One, TH1D *h1Two, Double_t cOne, Double_t cTwo) {

  Int_t contOne, contTwo;
  Int_t nBinsOne = h1One->GetXaxis()->GetNbins();
  Double_t lowXOne = h1One->GetXaxis()->GetXmin();
  Double_t hiXOne = h1One->GetXaxis()->GetXmax();
  // cout << "nBinsOne: " << nBinsOne << ", lowXOne: " << lowXOne << ", hiXOne: " << hiXOne << endl;
  Int_t nBinsTwo = h1Two->GetXaxis()->GetNbins();
  Double_t lowXTwo = h1Two->GetXaxis()->GetXmin();
  Double_t hiXTwo = h1Two->GetXaxis()->GetXmax();
  // cout << "nBinsTwo: " << nBinsTwo << ", lowXTwo: " << lowXTwo << ", hiXTwo: " << hiXTwo << endl;
  TH1D* add = new TH1D(addHistName,addHistName,nBinsOne,lowXOne,hiXOne);
  if ((nBinsOne == nBinsTwo) && (lowXOne == lowXTwo) && (hiXOne == hiXTwo)) {
    for (Int_t i=1; i<nBinsOne; i++) {
      contOne = h1One->GetBinContent(i);
      contTwo = h1Two->GetBinContent(i);
      add->SetBinContent(i,cOne*contOne+cTwo*contTwo);
    }
  } else {
    cout << "addHistograms: The two histgrams " << " and " << " do not have the same binning!!! Adding cannot be performed!" << endl;
  }
  
  
  return add;
}

Double_t round(Double_t number, Int_t decimalPlace) {
  /*
   * function rounds number number to decimalPlace'th decimal place
   */
  Double_t sign, floorNumber, roundedNumber;
  if (number < 0) {
    sign = -1.0;
  } else {
    sign= 1.0;
  }
  number = sign*number;
  //  cout << "number to round: " << number << endl;
  number = number * TMath::Power(10, decimalPlace);
  //  cout << "shifted number: " << number << endl;
  floorNumber = TMath::Floor(number);
  //  cout << "floor number: " << floorNumber << endl;
  if ((number - floorNumber) >= 0.5) {
    roundedNumber = floorNumber + 1;
  } else {
    roundedNumber = floorNumber;
  }
  roundedNumber = roundedNumber / TMath::Power(10, decimalPlace);
  roundedNumber = sign*roundedNumber;
  return roundedNumber;
}

void style() {

  TStyle *st1 = new TStyle("st1", "my style");

  st1->SetCanvasColor(0);
  st1->SetCanvasBorderMode(0);

  st1->SetPadColor(0);
  st1->SetPadBorderMode(0);
  st1->SetPadLeftMargin(0.1);
  st1->SetPadRightMargin(0.05);
  st1->SetPadTopMargin(0.08);
  st1->SetPadBottomMargin(0.15);

  // histogram title

  st1->SetOptTitle(1);
  st1->SetTitleFont(132,"");
  st1->SetTitleBorderSize(1);
  st1->SetTitleFillColor(0);
  st1->SetTitleX(0.1);

  // statistic box

  st1->SetStatColor(0);
  // st1->SetOptStat(111110);
  st1->SetOptStat(0);
  // st1->SetOptDate(3);
  st1->SetStatFont(132);

  // axis labels

  st1->SetLabelOffset(0.01,"x");
  st1->SetLabelOffset(0.01,"y");
  st1->SetLabelFont(132,"xyz");

  // axis title

  st1->SetTitleOffset(1.2,"x");
  st1->SetTitleOffset(1.4,"y");
  st1->SetTitleFont(22,"xyz");
  st1->SetPaperSize(19.,27.);
  st1->SetPalette(1);

  // Legend


  // apply style

  st1->cd();
  gROOT->ForceStyle();
  
}
