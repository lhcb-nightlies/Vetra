void dispGenData(char* file)
{
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(kFALSE);
  // get the files
  TFile* ff=TFile::Open(file);
  if(!ff)
    std::cout<< " --> Cannot open specified file! " << std::endl;
  // correlation plots
  TH2D* h1=ff->Get("Vetra/VeloTELL1ADCReader/177");
  if(!h1)
    std::cout<< " --> Cannot find the right histogram! " <<std::endl;
  //
  TCanvas *gen=new TCanvas("gen", "Decoded data", 10, 10, 800, 600);
  h1->GetYaxis()->SetRangeUser(500., 550.);
  h1->Draw();
}
