void dispComp(char* file)
{
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(kFALSE);
  // get the files
  TFile* ff=TFile::Open(file);
  if(!ff)
    std::cout<< " --> Cannot open one/both files! " << std::endl;
  // correlation plots
  TH2D* h1=ff->Get("Velo/VeloClusterCompare/103");
  TH1F* h3=ff->Get("Velo/VeloClusterCompare/102");
  TH1F* h4=ff->Get("Velo/VeloClusterCompare/222");
  TH1F* h5=ff->Get("Velo/VeloClusterCompare/104");
  TH1F* h6=ff->Get("Velo/VeloClusterCompare/204");
  //
  TCanvas *comp=new TCanvas("comp", "Clus comp", 10, 10, 800, 800);
  pad1=new TPad("evts","Cluster number per event",0.02,0.58,0.50,0.98,21);
  pad2=new TPad("spec","Clusters spectrum",0.51,0.58,0.98,0.98,21);
  pad3=new TPad("landau","Clusters adc",0.02,0.02,0.98,0.57,21);
  pad1->Draw();
  pad2->Draw();
  pad3->Draw();
  //
  pad1->cd();
  h1->Draw();
  //
  h3->SetMarkerStyle(21);
  h3->SetMarkerColor(1);
  h3->SetMarkerSize(.5);
  pad3->cd();
  h3->Draw();
  h4->SetMarkerStyle(25);
  h4->SetMarkerColor(5);
  h4->SetMarkerSize(.5);
  h4->Draw("SAME");
  //
  h5->SetMarkerStyle(21);
  h5->SetMarkerColor(1);
  h5->SetMarkerSize(.5);
  pad2->cd();
  h6->SetMarkerStyle(25);
  h6->SetMarkerColor(5);
  h6->SetMarkerSize(.5);
  h6->DrawNormalized();
  h5->DrawNormalized("SAME");
  

}
