int savePlot(char plotMsg[])
{
  char plotFile[50];
  char answer;

  cout << plotMsg << ", save plot (y/n/q)";

  //  answer = getchar();
  cin >> answer;

  if ( answer == 'q' ) {
    return 1;
  }

  if ( answer == 'y' ) {
    cout << "File name: ";
    cin >> plotFile;
    c1->Print(plotFile);
  }
  return 0;
}


void headerAna(char rootFile[]) {
  int tell1Id = 30;
  headerAna(rootFile, tell1Id);
}


void headerAna(char rootFile[], int tell1Id) {
  cout << "====  ROOT file:  " << rootFile << endl;
  cout << "====  TELL1 ID:   " << tell1Id << endl;
  
  // Open ROOT file and access (sub)directories
  TFile *ff=TFile::Open(rootFile);
  if ( ff == NULL ) {
    cout << " No such file!" << endl;
    return;
  }

  TDirectory *tdVetra = ff->Get("Vetra");
  if ( tdVetra == NULL ) {
    cout << " No Vetra directory " << endl;
    return;
  }

  char dirName[50];
  sprintf(dirName, "TELL1_%d_ADC", tell1Id );
  TDirectory *tdADC = tdVetra->Get(dirName);
  if ( tdADC == NULL ) {
    cout << " Directory Vetra/" << dirName << " does not exist" << endl;
    return;
  }

  sprintf(dirName, "TELL1_%d_CommonMode", tell1Id );
  TDirectory *tdCommonMode = tdVetra->Get(dirName);
  if ( tdCommonMode == NULL ) {
    cout << " Directory Vetra/" << dirName << " does not exist" << endl;
    return;
  }

  sprintf(dirName, "TELL1_%d_PedAndNoise", tell1Id );
  TDirectory *tdPedAndNoise = tdVetra->Get(dirName);
  if ( tdADC == NULL ) {
    cout << " Directory Vetra/" << dirName << " does not exist" << endl;
    return;
  }

  // Some variables
  int i, j;
  int iCard;
  int iChip;
  int iChipOld;
  int iLink;
  int iChannel;
  int link;
  int linkOld;
  int nGaussian;
  char name[50];
  char title[50];
  Double_t sumNoise;
  Double_t avgNoise;
  Double_t xMean[4];
  Double_t xSigma[4];
  Double_t allParam[12];

  // Book histograms, some are booked later
  TH1F *noiseDist[64];
  TH1F *yProjLink[64];
  TF1  *fitGauss[64];
  TH1F *yProjTemp;
  TH1F *yNoise     = new TH1F("yNoise",     "Noise vs link", 64, -0.5, 63.5);
  TH1F *yProjAll   = new TH1F("yProjAll",   "Last header bit signal", 1024, -0.5, 1023.5);
  TH1F *yLowMinus  = new TH1F("yLowMinus",  "Low minus",       64, -0.5, 63.5);
  TH1F *yLowPlus   = new TH1F("yLowPlus",   "Low plus",        64, -0.5, 63.5);
  TH1F *yHighMinus = new TH1F("yHighMinus", "High minus",      64, -0.5, 63.5);
  TH1F *yHighPlus  = new TH1F("yHighPlus",  "High Plus",       64, -0.5, 63.5);
  TH1F *ySigma     = new TH1F("ySigma",     "Largest sigma",   64, -0.5, 63.5);
  TH1F *yRatioPlusDist=new TH1F("yRatioPlusDist",  "Signal/noise distribution", 100, 10.0, 30.0);
  TH1F *nGaussianF = new TH1F("nGaussianF", "Number of peaks", 64, -0.5, 63.5);
  TF1  *fitGauss_1 = new TF1("fitGauss_1",  "gaus",            300.0, 800.0);
  TF1  *fitGauss_2 = new TF1("fitGauss_2",  "gaus(0)+gaus(3)", 300.0, 800.0);

  fitGauss_1->SetLineColor(2);
  fitGauss_2->SetLineColor(2);
  fitGauss_2->SetParNames("A1","Mean1","Sigma1","A2","Mean2","Sigma2");

  // Common mode suppressed noise vs electronic channel
  TH1F *hnoiseChannel = (TH1F *) tdPedAndNoise->Get("hRMSEvenAfterCMMap");

  for ( iCard = 0; iCard < 4; iCard++ ) {
    for ( iChipOld = 0; iChipOld < 4; iChipOld++ ) {
      for ( iLink = 0; iLink < 4; iLink++ ) {
        if ( iChipOld < 2 ) {
          iChip = iChipOld+2;
        } else {
          iChip = iChipOld-2;
        }
        linkOld = iCard*16 + iChipOld*4 + iLink;
        link = iCard*16 + iChip*4 + iLink;

        // electronic noise of all channels in the link.
        sumNoise = 0;
        for ( iChannel = 0; iChannel < 32; iChannel++ ) {
          sumNoise += hnoiseChannel->GetBinContent(iChannel+linkOld*32+1);
        }
        avgNoise = sumNoise/32;        

        sprintf(name, "noiseDist_%d", link);
        sprintf(title, "Electronic Cms noise, card %d beetle %d link %d", iCard, iChip, iLink);
        if ( avgNoise < 4.0 ) {
          noiseDist[link] = new TH1F(name, title, 60, 0.0, 6.0);
        } else {
          noiseDist[link] = new TH1F(name, title, 120, 0.0, 12.0);
        }
        
        for ( iChannel = 0; iChannel < 32; iChannel++ ) {
          noiseDist[link]->Fill(hnoiseChannel->GetBinContent(iChannel+linkOld*32+1));
        }
        noiseDist[link]->Fit("gaus", "QL");
        TF1 *fit = noiseDist[link]->GetFunction("gaus");
        yNoise->SetBinContent(link+1, fit->GetParameter(1));
        yNoise->SetBinError  (link+1, fit->GetParameter(2));
        
        // last bit of header of the link
        sprintf(name, "hADCHeaders_%d", linkOld);
        TH2F *hADCHeader = (TH2F*) tdADC->Get(name);

        sprintf(name, "yLastBit_%d", link);
        sprintf(title, "Last Bit of Header, card %d beetle %d link %d", iCard, iChip, iLink);
        yProjLink[link] = (TH1F*) hADCHeader->ProjectionY(name, 4, 4);
        yProjLink[link]->SetTitle(title);

        // Fit atmost 4 Gaussian peaks
        TH1F *yProjTemp = (TH1F*) yProjLink[link]->Clone("yProjTemp");
        Double_t yMaximum = yProjTemp->GetMaximum( 100000.0 );
        nGaussian = 0;
        for ( i = 0; i < 4; i++ ) {
          Double_t xMax = yProjTemp->GetMaximumBin()-1;
          Double_t yMax = yProjTemp->GetMaximum( 100000.0 );
          if ( yMax/yMaximum > 0.2 ) {
            fitGauss_1 = new TF1("fitGauss_1", "gaus", xMax-10.0, xMax+10.0);
            fitGauss_1->SetParameters(yMax, xMax, 0.8);
            yProjTemp->Fit("fitGauss_1","RQ");
            if ( abs(fitGauss_1->GetParameter(1)-xMax) > 1 ) {
              fitGauss_1->SetRange(xMax-2.0, xMax+2.0);
              fitGauss_1->SetParameters(yMax, xMax, 0.8);
              yProjTemp->Fit("fitGauss_1","RQ");
            }
            
            if ( abs(fitGauss_1->GetParameter(1)-xMax) > 2 ) {
              xMean[i] = xMax;
            } else {
              xMean[i] = fitGauss_1->GetParameter(1);
            }
            
            xSigma[i] = fitGauss_1->GetParameter(2);
            fitGauss_1->GetParameters(&allParam[i*3]);
            nGaussian ++;
            Double_t cut = 3*xSigma[i];
            if ( cut > 6 ) cut = 6;
            if ( cut < 2 ) cut = 2;
            
            for ( j = (int) (xMean[i]-cut); j < (xMean[i]+cut); j++ ) {
              yProjTemp->SetBinContent(j+1, 0.0);
            }
          }
        }

	// Put all gaussian together
        sprintf(name, "fitGauss_%d", link);
        if ( nGaussian == 0 ) {
          cout << " link " << link << " failed gaussian fit" << endl;
        } else {
          if ( nGaussian == 1 ) {
            fitGauss[link] = new TF1(name, "gaus", 300.0, 800.);
          } else if ( nGaussian == 2 ) {
            fitGauss[link] = new TF1(name, "gaus(0)+gaus(3)", 300.0, 800.);
          } else if ( nGaussian == 3 ) {
            fitGauss[link] = new TF1(name, "gaus(0)+gaus(3)+gaus(6)", 300.0, 800.);
          } else {
            fitGauss[link] = new TF1(name, "gaus(0)+gaus(3)+gaus(6)+gaus(9)", 300.0, 800.);
          }
          fitGauss[link]->SetLineColor(2);
          fitGauss[link]->SetParameters(&allParam[0]);
          yProjLink[link]->Fit(name, "RQ");
        }

	// Sort gaussian functions according to center value
        Double_t xtmp;
        for ( i = 0; i < nGaussian; i++ ) {
          for ( j = 0; j < i; j++ ) {
            if ( xMean[j] > xMean[i] ) {
              xtmp = xMean[j];
              xMean[j] = xMean[i];
              xMean[i] = xtmp;
              xtmp = xSigma[j];
              xSigma[j] = xSigma[i];
              xSigma[i] = xtmp;
            }
          }
        }

	// Decide the double double structure
        if ( nGaussian == 4 ) {
          yLowMinus->SetBinContent(link+1, xMean[0]);
          yLowMinus->SetBinError(link+1, xSigma[0]);
          yLowPlus->SetBinContent(link+1, xMean[1]);
          yLowPlus->SetBinError(link+1, xSigma[1]);
          yHighMinus->SetBinContent(link+1, xMean[2]);
          yHighMinus->SetBinError(link+1, xSigma[2]);
          yHighPlus->SetBinContent(link+1, xMean[3]);
          yHighPlus->SetBinError(link+1, xSigma[3]);
        }
        
        if ( nGaussian == 3 ) {
          yLowMinus->SetBinContent(link+1, xMean[0]);
          yLowMinus->SetBinError(link+1, xSigma[0]);
          yHighPlus->SetBinContent(link+1, xMean[2]);
          yHighPlus->SetBinError(link+1, xSigma[2]);
          if ( xMean[1]-xMean[0] < xMean[2]- xMean[1] ) {
            yLowPlus->SetBinContent(link+1, xMean[1]);
            yLowPlus->SetBinError(link+1, xSigma[1]);
            yHighMinus->SetBinContent(link+1, xMean[2]);
            yHighMinus->SetBinError(link+1, xSigma[2]);
          } else {
            yLowPlus->SetBinContent(link+1, xMean[0]);
            yLowPlus->SetBinError(link+1, xSigma[0]);
            yHighMinus->SetBinContent(link+1, xMean[1]);
            yHighMinus->SetBinError(link+1, xSigma[1]);
          }
        }

        if ( nGaussian == 2 ) {
          yLowMinus->SetBinContent(link+1, xMean[0]);
          yLowMinus->SetBinError(link+1, xSigma[0]);
          yLowPlus->SetBinContent(link+1, xMean[0]);
          yLowPlus->SetBinError(link+1, xSigma[0]);
          yHighMinus->SetBinContent(link+1, xMean[1]);
          yHighMinus->SetBinError(link+1, xSigma[1]);
          yHighPlus->SetBinContent(link+1, xMean[1]);
          yHighPlus->SetBinError(link+1, xSigma[1]);
        }
          
        if ( nGaussian == 1 ) {
          yLowMinus->SetBinContent(link+1, xMean[0]);
          yLowMinus->SetBinError(link+1, xSigma[0]);
          yLowPlus->SetBinContent(link+1, xMean[0]);
          yLowPlus->SetBinError(link+1, xSigma[0]);
          yHighMinus->SetBinContent(link+1, xMean[0]);
          yHighMinus->SetBinError(link+1, xSigma[0]);
          yHighPlus->SetBinContent(link+1, xMean[0]);
          yHighPlus->SetBinError(link+1, xSigma[0]);
        }

        nGaussianF->SetBinContent(link+1, nGaussian);        

	// find the largest sigma of all structed line
        Double_t sigmaMax = 0.0;
        for ( i = 0; i < nGaussian; i++ ) {
          if ( sigmaMax < xSigma[i] ) {
            sigmaMax = xSigma[i];
          }
        }
        ySigma->SetBinContent(link+1, sigmaMax);

        yProjAll->Add(yProjLink[link]);
      }
    }
  }

  // Fit double gaussian on yProjAll
  fitGauss_2->SetParameters(21300.0, 461.5, 4.73, 21500.0, 566.0, 5.48);
  TH1F *yProjTemp = (TH1F*) yProjAll->Clone("yProjTemp");
  Double_t yMaximum = yProjTemp->GetMaximum( 100000.0 );
  nGaussian = 0;
  for ( i = 0; i < 2; i++ ) {
    Double_t xMax = yProjTemp->GetMaximumBin();
    Double_t yMax = yProjTemp->GetMaximum( 100000.0 );
    if ( yMax/yMaximum > 0.2 ) {
      fitGauss_1 = new TF1("fitGauss_1", "gaus", xMax-20.0, xMax+20.0);
      fitGauss_1->SetParameters(yMax, xMax, 1.0);
      yProjTemp->Fit("fitGauss_1","RQ");
      fitGauss_1->GetParameters(&allParam[i*3]);
      nGaussian ++;
      Double_t cut = 3*allParam[i*3+2];
      if ( cut > 12 ) cut = 12;
      if ( cut < 5 ) cut = 5;
      for ( j = (int) (allParam[i*3+1]-cut); j < (allParam[i*3+1]+cut); j++ ) {
        yProjTemp->SetBinContent(j+1, 0.0);
      }
    }
  }
  if ( nGaussian != 2 ) {
    cout << " Header has no structure, check phase! " << endl;
  } else {
    fitGauss_2->SetParameters(&allParam[0]);
    yProjAll->Fit("fitGauss_2", "RQ");
  }
  cout << "Header high mean: " << fitGauss_2->GetParameter(1) << endl;
  cout << "             rms: " << fitGauss_2->GetParameter(2) << endl;
  cout << "Header low  mean: " << fitGauss_2->GetParameter(4) << endl;
  cout << "             rms: " << fitGauss_2->GetParameter(5) << endl;
  
  // Header high low difference
  TH1F *yDiffMinus = (TH1F*) yHighMinus->Clone("yDiffMinus");
  yDiffMinus->Add(yLowMinus, -1.0); 
  yDiffMinus->SetTitle("Difference minus");

  TH1F *yDiffPlus = (TH1F*) yHighPlus->Clone("yDiffPlus");
  yDiffPlus->Add(yLowPlus, -1.0);
  yDiffPlus->SetTitle("Last header bit signal difference");

  // Header difference / electronic noise to estimate S/N
  TH1F *yRatioMinus = (TH1F*) yDiffMinus->Clone("yRatioMinus");
  yRatioMinus->Divide(yDiffMinus, yNoise, 1.0, 2.38, "");
  yRatioMinus->SetTitle("Ratio minus");

  TH1F *yRatioPlus = (TH1F*) yDiffPlus->Clone("yRatioPlus");
  yRatioPlus->Divide(yDiffPlus, yNoise, 1.0, 2.38, "");
  yRatioPlus->SetTitle("Signal/noise using Last header bit");

  for ( link = 0; link < 64; link++ ){
    yRatioPlusDist->Fill(yRatioPlus->GetBinContent(link+1));
  }
  yRatioPlusDist->Fit("gaus","QL");

// Print plots
  TCanvas *c1 = new TCanvas("c1","",10,10,600,600);
  gStyle->SetOptStat("n"); // name only
  gStyle->SetOptFit(111);
  yProjAll->GetXaxis()->SetRange(301,801);
  yProjAll->SetFillColor(4);
  fitGauss_2->SetLineColor(2);
  yProjAll->Draw();
  c1->Update();
  if ( savePlot("Signal in all last header bits") ) return;

  yHighPlus->SetTitle("Last header bit high and low signal");
  yHighPlus->SetMinimum(300.0);
  yHighPlus->SetMaximum(800.0);
  yHighPlus->SetMarkerStyle(20);
  yHighPlus->SetMarkerColor(4);
  yLowPlus->SetMarkerStyle(20);
  yLowPlus->SetMarkerColor(2);
  yHighPlus->Draw();
  yLowPlus->Draw("same");
  c1->Update();
  if ( savePlot("High/Low signal in last header bits vs link") ) return;

  yDiffPlus->SetMinimum(0);
  //  yDiffPlus->SetMaximum(220.0);
  yDiffPlus->SetMarkerStyle(20);
  yDiffPlus->SetMarkerColor(4);
  yDiffPlus->Draw();
  c1->Update();
  if ( savePlot("High/Low signal difference in last header bits vs link") ) return;

  yNoise->SetMinimum(0);
  //  yDiffPlus->SetMaximum(220.0);
  yNoise->SetMarkerStyle(20);
  yNoise->SetMarkerColor(4);
  yNoise->Draw();
  c1->Update();
  if ( savePlot("Average CMS noise of all channels vs link") ) return;

  yRatioPlus->SetMinimum(0);
  //  yRatioPlus->SetMaximum(33.0);
  yRatioPlus->SetMarkerStyle(20);
  yRatioPlus->SetMarkerColor(4);
  yRatioPlus->Draw();
  c1->Update();
  if ( savePlot("Signal/noise derived from header vs link") ) return;

  yRatioPlusDist->SetFillColor(4);
  yRatioPlusDist->Draw();
  c1->Update();
  if ( savePlot("Signal/noise distribution") ) return;
  
  
}


