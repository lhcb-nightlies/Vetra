#!/bin/bash
 
if [ $# -ne 1 ]
then
    echo "Enter filename as argument".
else
    export INPUT_FILE=$1
    export ROOT_FILE=/group/velo/temp_data/`echo $INPUT_FILE | sed 's/.*\///' | cut -f 1 -d .`_arastest.root
     
    $VETRAROOT/$CMTCONFIG/Vetra.exe $VETRAROOT/options/UniformityTest.opts
 
fi

