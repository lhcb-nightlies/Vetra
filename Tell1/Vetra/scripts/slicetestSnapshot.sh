#! /bin/bash

if [ $# -ne 1 ]
    then
    echo "usage: slicetestSnapshot.sh filename"
else

    export INPUT_FILE=$1
#    echo $INPUT_FILE
    length=`expr length $INPUT_FILE`

# count the number of slashes in the filename
    
    nSlash=0
    count=0
    while [ $count -ne $length ]
      do
      count=$[$count+1] 
      if [ `expr substr $INPUT_FILE $count 1` = "/" ]
	  then
	  nSlash=$[$nSlash+1]
      fi
    done
#    echo $nSlash

    name=$(echo $INPUT_FILE | cut -d '/' -f$[$nSlash+1])
    name=$(echo $name | cut -d '.' -f1)
    
# extract the filename

    export ROOT_FILE=/group/velo/slicetest/snapshot/$name.root
    echo $ROOT_FILE
    
    $VETRAROOT/slc4_ia32_gcc34/Vetra.exe $VETRAROOT/options/slicetestSnapshot.opts
fi
      
