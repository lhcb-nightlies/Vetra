#!/bin/csh

setenv INPUT_FILE $1
setenv start $PWD

shift
 
lbcmt
setenvVetra v5r1
source $User_release_area/Vetra_v5r1/Velo/Vetra/v5r1/cmt/setup.csh

setenv ROOT_FILE /tmp/`echo $INPUT_FILE | sed 's/.*\///' | cut -f 1 -d .`.root
$VETRAROOT/$CMTCONFIG/Vetra.exe $VETRAROOT/options/UniformityTest.opts
 
cd $start
