#!/bin/bash
export UTGID

User_release_area=/group/velo/sw/online/install/current; export User_release_area
cd /group/velo/sw/online/install/currentVetra/cmt
. ./setup.vars

cd /group/velo/sw/online/install/currentVetraSys/cmt
. ./setup.vars
TASKCLASS=-tasktype=LHCb::${2}Task
TASKTYPE=${3}
DYNAMIC_OPTS=/group/online/dataflow/options
OPTIONS=${DYNAMIC_OPTS}/${PARTITION}/${1}
export PREAMBLE_OPTS=/group/online/dataflow/templates/options/Preamble.opts
export DATAINTERFACE=`python -c "import socket;print socket.gethostbyname(socket.gethostname().split('.')[0]+'-d1')"`
export TAN_PORT=YES
export TAN_NODE=${DATAINTERFACE}
export gaudi_exe="$GAUDIONLINEROOT/$CMTCONFIG/Gaudi.exe libGaudiOnline.so OnlineTask -msgsvc=LHCb::FmcMessageSvc "
export DIM_DNS_NODE=${4}
export LD_PRELOAD=/group/online/dataflow/cmtuser/Online_v4r11/InstallArea/${CMTCONFIG}/lib/libGaudiOnline.so
$gaudi_exe ${TASKCLASS} -opt=${OPTIONS} -main=/group/online/dataflow/templates/options/Main.opts
