#!/bin/bash

if [ $# -ne 2 ]
then 
    echo "Enter filename and TELL1 number as arguments".    
else
    export INPUT_FILE=$1
    export TELL1=$2
    export ROOT_FILE=/group/velo/uniformitytest/root/`echo $INPUT_FILE | sed 's/.*\///' | cut -f 1 -d .`.root
    
    $VETRAROOT/$CMTCONFIG/Vetra.exe $VETRAROOT/options/UniformityTest_oldMonitoring.opts

fi
