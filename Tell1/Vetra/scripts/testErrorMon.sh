#! /bin/bash

if [ $# -ne 1 ]
    then
    echo "usage: modulepowerupErrorMon.sh filename"
else

    export INPUT_FILE=$1
#    echo $INPUT_FILE
    length=`expr length $INPUT_FILE`

# count the number of slashes in the filename
    
    nSlash=0
    count=0
    while [ $count -ne $length ]
      do
      count=$[$count+1] 
      if [ `expr substr $INPUT_FILE $count 1` = "/" ]
	  then
	  nSlash=$[$nSlash+1]
      fi
    done
#    echo $nSlash

    name=$(echo $INPUT_FILE | cut -d '/' -f$[$nSlash+1])
    name=$(echo $name | cut -d '.' -f1)
    

    export ROOT_FILE=./$name.root
    echo $ROOT_FILE
    
    $VETRAROOT/$CMTCONFIG/Vetra.exe $VETRAROOT/options/Velo/commissioning/testErrorMon.opts
fi
      
