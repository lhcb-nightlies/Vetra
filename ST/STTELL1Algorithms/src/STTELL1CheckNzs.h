#ifndef STTELL1CHECKNZS_H
#define STTELL1CHECKNZS_H 1

//include files
#include "STTELL1Algorithm.h"
#include "Event/RawBank.h"

/** @class STTELL1CheckNzs STTELL1CheckNzs.h
 *  
 *
 *  @author Joschka Lingemann
 *  @date   2009-07-14
 */
class STTELL1CheckNzs : public STTELL1Algorithm {
 public:
  STTELL1CheckNzs(const std::string& name, ISvcLocator* pSvcLocator );
  
  virtual ~STTELL1CheckNzs();
  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

 private:
  std::string m_inputLocation;
  //std::string m_detType;

  bool   m_selectedTell1s;///< Use only selected TELL1s
  std::vector< unsigned int > m_limitToTell;///< List of TELL1s to look at
  
  unsigned int m_nNZS;
  unsigned int m_totEvts;

  bool m_found;///< True if "enough" (m_numberNZS) NZS raw banks are found
  bool m_targetNZS;///< Ask for number of events to have "enough" (m_numberNZS) NZS raw banks
  unsigned int m_numberNZS;///< Target number of NZS raw banks
};
#endif
