// $Id: STTELL1Algorithm.h,v 1.12 2010/04/01 12:05:12 mtobin Exp $
#ifndef STTELL1ALGORITHM_H 
#define STTELL1ALGORITHM_H 1

// Include files
// base class
#include "Kernel/STTupleAlgBase.h"

// from TELL1 Kernel
#include "Tell1Kernel/STTell1Core.h"

// from ST TELL1 Event
#include "Event/STTELL1Data.h"

#include "DetDesc/Condition.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "Kernel/STTell1ID.h"
#include "Kernel/ISTReadoutTool.h"

/** @class STTELL1Algorithm STTELL1Algorithm.h
 *  
 *  base class for any TELL1 algorithm
 *
 *  @author Tomasz Szumlak, Anne Keune
 *  @date   2006-12-06
 */

class ISTVetraCfg;

class STTELL1Algorithm : public ST::TupleAlgBase {
 public: 

  typedef std::vector<unsigned int> dataVec;
  typedef std::vector<unsigned int>::const_iterator cdatIt;
  typedef std::vector<unsigned int>::iterator datIt;
  typedef std::vector<signed int> sdataVec;
  typedef std::vector<signed int>::const_iterator scdatIt;
  typedef std::vector<signed int>::iterator sdatIt;

  typedef std::pair<int,sdataVec> TELL1RAMPair;
  typedef std::map<int,sdataVec> RAM;

  enum convergenceInit{
    CONV_LIMIT=2000
  };

  enum ram{
    PED_SUM_SHIFT=10,
  };

  //configuration defaults *****
  enum clusterMakerInits{
    HIT_THRESHOLD=9,
    CONFIRMATION_THRESHOLD=12,
    SPILLOVER_THRESHOLD=30,
    MAX_CLUSTERS=255
  };

  enum lcmsInits{
    CMS_THRESHOLD=12
  };

  enum pedestalSubtractorInits{
    HEADER_CORRECTION = 0,
    HEADER_THRESHOLD = 100,
    PEDESTAL = 128
  };

  enum conf{
    TOT_BEETLES = 24,
    CHANNELS_PER_BEETLE = 128,
    LINKS_PER_BEETLE = 4,
    CHANNELS_PER_LINK = 32
  }; 

  /** Standard constructor **/
  STTELL1Algorithm(const std::string& name, ISvcLocator* pSvcLocator );
  
  /** Destructor **/
  virtual ~STTELL1Algorithm();
  
  /** Algorithm initialization **/
  virtual StatusCode initialize() override; 

  /** Algorithm execution **/
  virtual StatusCode execute() override;

  /** Algorithm finalization **/
  virtual StatusCode finalize() override;

 protected:
  
  virtual StatusCode getData();
  virtual StatusCode writeData();
  virtual StatusCode cloneData();
  virtual void setInputDataLoc(const std::string inValue);
  virtual void setOutputDataLoc(const std::string inValue);
  virtual LHCb::STTELL1Datas* inputData() const;
  virtual void outputData(LHCb::STTELL1Data* inData);
  virtual LHCb::STTELL1Datas* outputData();
  //virtual StatusCode inputStream(LHCb::STTELL1Datas* inVec) const;
  virtual STTELL1Algorithm::sdataVec flattenData(LHCb::STTELL1Data::Data* inData);
  virtual LHCb::STTELL1Data::Data structureData(sdataVec& inVec);

  unsigned int m_evtNumber; 
  std::map<unsigned int, unsigned int> m_evtNumberTELL1;
  std::string m_inputDataLoc;
  std::string m_outputDataLoc;
  std::string m_subPedsLoc; /// TES location of the subtracted peds
  unsigned int m_convergenceLimit;
  std::string m_condPath; 
  unsigned int m_tell1;
  std::vector<unsigned int> m_sentPPs;
 
  template<class T, class U>
    void vecInit(T& inData, const int size, U& init) {
    for(int i=0; i<size; i++){
      inData.push_back(init);
    }
  }

  template<class T>
    void vecInit(T& inData, const int size, T& init) {
    if(size != (int)init.size()) error() << "vecInit. Supplied vector is of size: " << init.size() 
					 << " but should be of size: " << size << endmsg;
    for(int i=0; i<size; i++){
      inData.push_back(init[i]);
    }
  }

  /**
   * Vetra cond DB readout tool
   */
  ISTVetraCfg* m_stVetraCfg;
  std::string m_stVetraCfgName;
  std::string m_stVetraCfgType;

  LHCb::STTELL1Datas* m_inputData;
  LHCb::STTELL1Datas* m_outputData;

};
#endif // STTELL1ALGORITHM_H
