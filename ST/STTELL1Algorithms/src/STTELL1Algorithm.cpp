// $Id: STTELL1Algorithm.cpp,v 1.15 2010/05/05 11:53:55 jluisier Exp $
// Include files

#include "STTELL1Algorithm.h"

// Reading conditions
#include "VetraKernel/ISTVetraCfg.h"
#include "VetraKernel/STVetraCfg.h"

using namespace LHCb;
using namespace STTELL1;

//-----------------------------------------------------------------------------
// Implementation file for class : STTELL1Algorithm
//
// 2006-12-06 : Tomasz Szumlak, Anne Keune
//-----------------------------------------------------------------------------
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STTELL1Algorithm::STTELL1Algorithm( const std::string& name,
                                    ISvcLocator* pSvcLocator):
  ST::TupleAlgBase ( name, pSvcLocator ),
  m_evtNumber ( 0 ),
  m_inputDataLoc ( ),
  m_outputDataLoc ( ),
  m_inputData ( 0 ),
  m_outputData ( 0 )
{  
  declareProperty("CondPath", m_condPath="CondDB");
  declareProperty("ConvergenceLimit", m_convergenceLimit=0);

  declareProperty("VetraCfgToolType", m_stVetraCfgType="STVetraCfg");
  declareSTConfigProperty("VetraCfgToolName", m_stVetraCfgName, "ITVetraCfgTool");

  m_stVetraCfg = 0;
}
//=============================================================================
// Destructor
//=============================================================================
STTELL1Algorithm::~STTELL1Algorithm() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode STTELL1Algorithm::initialize() {
  StatusCode sc( ST::TupleAlgBase::initialize() ); // must be executed first
  if ( sc.isFailure() ) return sc;
  
  debug() << "==> initialize" << endmsg;

  m_stVetraCfg = tool<ISTVetraCfg>(m_stVetraCfgType,m_stVetraCfgName);

  sc = m_stVetraCfg -> initialize();

  return sc;
}
//=============================================================================
// Finalization
//=============================================================================
StatusCode STTELL1Algorithm::finalize() {

  debug() << "==> finalize" << endmsg;

  return ST::TupleAlgBase::finalize(); // must be executed last
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode STTELL1Algorithm::execute() {

  debug() << "==> execute" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
/**
 * Method that reads the data from TES.
 *
 * @return StatusCode \e FAILURE if data doesn't exist in TES.\n
 * \e RECOVERABLE if the data exists but is empty (typically round robin).\n
 * \e SUCCESS if data exists and contains something
 */
StatusCode STTELL1Algorithm::getData()
{
  debug()<< " ==> getData() " <<endmsg;
  //
  m_outputData = 0;

  if( !exist<STTELL1Datas>(m_inputDataLoc) )
  {
    debug()<< " ==> There is no input data: "
           << m_inputDataLoc <<endmsg;
    return Error( "No input data in " + m_inputDataLoc,
                  StatusCode( StatusCode::FAILURE ), 10 );
  }
  else
  {  
    // get data banks from default TES location
    m_inputData = get<STTELL1Datas>(m_inputDataLoc);
    debug()<< " ==> The data have been read-in from location: "
           << m_inputDataLoc
           << ", size of the data container: "
           << m_inputData->size() <<endmsg;

    if ( m_inputData -> size() == 0 )
      return Warning( "No data found, assuming we run on round robin data",
                      StatusCode( StatusCode::RECOVERABLE ), 10 );

    if( !m_outputDataLoc.empty() )
    {
      m_outputData = new LHCb::STTELL1Datas();
      m_outputData->reserve(m_inputData->size());
    }
  }

  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode STTELL1Algorithm::writeData()
{
  debug()<< " ==> writeData() " <<endmsg;
  //
  if(m_outputData!=0){
    put(m_outputData, m_outputDataLoc);

    return ( StatusCode::SUCCESS );
  }else{
    error()<< " ==> empty output container! " <<endmsg;
    return ( StatusCode::FAILURE );
  }
}
//=============================================================================
StatusCode STTELL1Algorithm::cloneData()
{
  debug()<< " ==> cloneData() " <<endmsg;

  STTELL1Data* out=new STTELL1Data(m_tell1);
  STTELL1Data::Data replica=(m_inputData->object(m_tell1))->data();
  out->setData(replica);
  outputData(out);

  return ( StatusCode::SUCCESS );
}
//=============================================================================
LHCb::STTELL1Datas* STTELL1Algorithm::inputData() const
{
  debug()<< " ==> inputData() " <<endmsg;
  //
  return ( m_inputData );
}
//=============================================================================
void STTELL1Algorithm::outputData(STTELL1Data* inData)
{
  debug()<< " ==> outputData() " <<endmsg;
  //
  m_outputData->insert(inData);
}
//=============================================================================
LHCb::STTELL1Datas* STTELL1Algorithm::outputData()
{
  return ( m_outputData );
}
//=============================================================================
void STTELL1Algorithm::setInputDataLoc(const std::string inValue)
{
  m_inputDataLoc=inValue;
}
//=============================================================================
void STTELL1Algorithm::setOutputDataLoc(const std::string inValue)
{
  m_outputDataLoc=inValue;
}
//=============================================================================
// StatusCode STTELL1Algorithm::inputStream(STTELL1Datas* inVec) const
// {
//   debug()<< " ==> inputStream() " << endmsg;
//   if( inVec->empty() )
//     return Warning("No data found, assuming we run on round robin data",
//                    StatusCode( StatusCode::RECOVERABLE ) );
//   else
//     return ( StatusCode::SUCCESS);
// }

//=============================================================================
STTELL1Algorithm::sdataVec STTELL1Algorithm::flattenData(STTELL1Data::Data* inData) 
{
  debug()<< " ==> flattenData() " <<endmsg;
  sdataVec vec; vec.reserve(STDAQ::nStripsPerBoard);
   for(int i = 0; i < TOT_BEETLES; i++) {
    for(int j = 0; j < CHANNELS_PER_BEETLE; j++) {
      vec.push_back((*inData)[i][j]);
    }  
  }
  return vec;
}
//=============================================================================
STTELL1Data::Data STTELL1Algorithm::structureData(sdataVec& inVec)
{
  debug()<< " ==> structureData() " <<endmsg;
  STTELL1Data::Data data; data.reserve(24);
  sdataVec dataVec; dataVec.reserve(128);
  for(int i = 0; i < TOT_BEETLES; i++) {
    dataVec.clear();
    for(int j = 0; j < CHANNELS_PER_BEETLE; j++) {
      dataVec.push_back(inVec[(CHANNELS_PER_BEETLE*i)+j]);
    }
    data.push_back(dataVec);
  }
  debug()<< " ==> structureData() finished" <<endmsg << std::flush;
  return data;
}

