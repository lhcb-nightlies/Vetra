// $Id: STTELL1Monitoring.cpp,v 1.16 2010/04/19 13:45:43 mtobin Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h"

// local
#include "STTELL1Monitoring.h"
#include "STDet/DeSTSector.h"

// read CondDB
#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

// boost
#include "boost/lexical_cast.hpp"
#include "Kernel/STLexicalCaster.h"
#include <bitset>

#include<map>
#include<algorithm>
//-----------------------------------------------------------------------------
// Implementation file for class : STTELL1Monitoring
//
// 2007-10-15 : Anne Keune
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STTELL1Monitoring )

using namespace Gaudi::Units;
using namespace LHCb;
using namespace STTELL1;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STTELL1Monitoring::STTELL1Monitoring( const std::string& name,
                                      ISvcLocator* pSvcLocator)
  : STTELL1Algorithm ( name , pSvcLocator )
{
  
  declareSTConfigProperty("RawDataLoc",m_rawDataLoc,STTELL1DataLocation::ITFull);
  declareSTConfigProperty("PedsubDataLoc",m_pedsubDataLoc,STTELL1DataLocation::ITPedSubADCs);
  declareSTConfigProperty("LCMSDataLoc",m_lcmsDataLoc,STTELL1DataLocation::ITLCMSADCs);
  declareSTConfigProperty("PedestalLoc",m_pedestalLoc,STTELL1DataLocation::ITSubPeds);
  declareSTConfigProperty("TELL1PedestalLoc",m_TELL1PedestalLoc,STTELL1DataLocation::ITPedestal);
  
  declareSTConfigProperty("ClusterLoc",m_clusterCont,"Raw/IT/Clusters");
  declareSTConfigProperty("EmuClusterLoc",m_emuClusterCont,"Emu/IT/Clusters");
  
  declareSTConfigProperty("ErrorLoc",m_errorLoc,STTELL1BoardErrorBankLocation::ITErrors);

  //declareProperty("ConvergenceLimit",m_convergenceLimit = 0);
  declareProperty("MCTruth",m_MCTruth = false);
  declareProperty("IncludeNZS",m_includeNZS=false);
  declareProperty("IncludeZS",m_includeZS=true);
  declareProperty("IncludeCondDBValues",m_includeCondDBValues=true);
  declareProperty("SkipIfErrorBank",m_skipIfErrorBank=true);
  declareProperty("RoundRobin",m_roundrobin=false);

}
//=============================================================================
// Destructor
//=============================================================================
STTELL1Monitoring::~STTELL1Monitoring() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode STTELL1Monitoring::initialize() {
  StatusCode sc = STTELL1Algorithm::initialize();
  if ( sc.isFailure() ) return sc; 
  
  debug() << "==> Initialize" << endmsg;

  if(m_includeCondDBValues) {
    //Update Manager
    std::vector<STTell1ID> tell1Vec = readoutTool()->boardIDs();
    for(unsigned int i = 0; i<tell1Vec.size(); i++) {
      unsigned int i_tell1 = tell1Vec[i].id();
      const std::string tell1 = boost::lexical_cast<std::string>(i_tell1);
      std::string condName = "TELL1Board" + tell1;
      std::string cond = m_condPath + "/" + condName;
      registerCondition(cond,
                        &STTELL1Monitoring::initConditions );
    }
  
    // force first updates
    sc = runUpdate();
    if (sc.isFailure()) return Error ( "Failed first UMS update", sc );
  }
  
  if(!(m_includeNZS||m_includeZS)){
    error() << "Need to include ZS or NZS data for the monitoring." << endmsg;
    return StatusCode::FAILURE;
  }
  
  for(unsigned int i=0; i<procTOT; i++){
    std::pair<bool,LHCb::STTELL1Datas*> init;
    init.first = 0;
    init.second = NULL;
    m_processVec.push_back(init);
  }

  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode STTELL1Monitoring::execute() {

  debug() << "==> Execute" << endmsg;
  //
  m_evtNumber++;

  checkAvailableData();
   
  if(m_includeZS) {
    Tuple zs_tuple = nTuple("zs");
    StatusCode ok = fillZS(zs_tuple);
    if (!ok) return ok;
  }
  if(m_includeNZS) {
    Tuple nzs_tuple = nTuple("nzs");
    StatusCode ok = fillNZS(nzs_tuple);
    if (!ok) return ok;
  }
    
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode STTELL1Monitoring::finalize() {

  debug() << "==> Finalize" << endmsg;

  return STTELL1Algorithm::finalize();
}
//=============================================================================
//  checkAvailableData
//=============================================================================
StatusCode STTELL1Monitoring::checkAvailableData() {

  debug() << "==> checkAvailableData" << endmsg;

  m_availableData.clear();

  StatusCode nzsStatus( getNZSData() );
  
  if ( nzsStatus.isRecoverable() )
    return Warning( "No data, assuming round robin",
                    StatusCode( StatusCode::SUCCESS ), 10 );
  else if( nzsStatus.isFailure() )
    return Warning( "Cannot retrieve any raw data.", nzsStatus, 10 );
  else {
    debug() << "Retrieved available raw data. Checking TELL1s and PPs" << endmsg;
    
    LHCb::STTELL1Datas::const_iterator STMainIt;
    LHCb::STTELL1Datas::const_iterator STMainItEnd;
    
    for(unsigned int i=0; i<m_processVec.size(); i++) {
      if(m_processVec[i].first){
        STMainIt = (m_processVec[i].second)->begin();
        STMainItEnd = (m_processVec[i].second)->end();
        break;
      }
    }
    for(; STMainIt != STMainItEnd ; ++STMainIt){
      m_tell1=(*STMainIt)->TELL1ID(); 
      m_sentPPs=(*STMainIt)->sentPPs();
      m_availableData.insert(std::make_pair(m_tell1,m_sentPPs));

      debug() << "Found TELL1 " << m_tell1 << " with sent PPs: ";
      for(unsigned int i(0); i<m_sentPPs.size();i++)
        debug() << m_sentPPs[i] << " ";
      debug() << endmsg;
      
      if(m_evtNumberTELL1.find(m_tell1)==m_evtNumberTELL1.end()) m_evtNumberTELL1[m_tell1]=0;        
      m_evtNumberTELL1[m_tell1]+=1;
      
    }
  }
  return StatusCode::SUCCESS;
}
//=============================================================================
StatusCode STTELL1Monitoring::getNZSData()
{
  debug()<< " ==> getNZSData() " <<endmsg;

  bool noData( true ), emptyData( true );

  /////rawADC
  if(!exist<STTELL1Datas>(m_rawDataLoc))
  {
    debug()<< " ==> There is no Full data in " << m_rawDataLoc << " in TES " <<endmsg;
    m_processVec[STFull].first = false;
    m_processVec[STFull].second = 0;
  }
  else
  {
    m_rawDatas=get<STTELL1Datas>(m_rawDataLoc);
    m_processVec[STFull].first = !(m_rawDatas->empty());
    m_processVec[STFull].second = m_rawDatas;
    noData &= false;
    emptyData &= m_rawDatas->empty();
  }
  
  /////pedsubADC
  if(!exist<STTELL1Datas>(m_pedsubDataLoc))
  {
    debug()<< " ==> There is no emulated pedestal subtracted data in TES " <<endmsg;
    m_processVec[pedSub].first  = false;
    m_processVec[pedSub].second = 0;
  }
  else
  {
    m_pedsubDatas=get<STTELL1Datas>(m_pedsubDataLoc);
    m_processVec[pedSub].first = !(m_pedsubDatas->empty());
    m_processVec[pedSub].second = m_pedsubDatas; 
    noData &= false;
    emptyData &= m_pedsubDatas->empty();
  }

  /////lcmsADC
  if(!exist<STTELL1Datas>(m_lcmsDataLoc))
  {
    debug()<< " ==> There is no emulated LCMS data in TES " <<endmsg;
    m_processVec[LCMS].first  = false;
    m_processVec[LCMS].second = 0;
  }
  else
  {
    m_lcmsDatas=get<STTELL1Datas>(m_lcmsDataLoc);
    m_processVec[LCMS].first = !(m_lcmsDatas->empty());
    m_processVec[LCMS].second = m_lcmsDatas;
    noData &= false;
    emptyData &= m_lcmsDatas->empty();
  }
  
  /////emulated peds
  if(!exist<STTELL1Datas>(m_pedestalLoc))
  {
    debug()<< " ==> There are no emulated peds in TES " <<endmsg;
    m_processVec[emuPeds].first  = false;
    m_processVec[emuPeds].second = 0;
  }
  else
  {
    m_emuPeds=get<STTELL1Datas>(m_pedestalLoc);
    m_processVec[emuPeds].first = !(m_emuPeds->empty());
    m_processVec[emuPeds].second = m_emuPeds;
    noData &= false;
    emptyData &= m_emuPeds->empty();
  }

  /////TELL1 pedestals
  if(!exist<STTELL1Datas>(m_TELL1PedestalLoc))
  {
    debug()<< " ==> There are no peds in TES " <<endmsg;
    m_processVec[peds].first  = false;
    m_processVec[peds].second = 0;
  }
  else
  {
    m_peds=get<STTELL1Datas>(m_TELL1PedestalLoc);
    m_processVec[peds].first = !(m_peds->empty());
    m_processVec[peds].second = m_peds;
    noData &= false;
    emptyData &= m_peds->empty();
  }

  if ( noData && emptyData )
  {
    // noData == true means that none of the container exists
    debug() << "no data found." << endmsg;
    return ( StatusCode::FAILURE );
  }
  else if ( emptyData )
  {
    // emptyData == true means that every bank is empty
    debug() << "Assuming round robin, data is empty" << endmsg;
    return Warning( "Empty container, assuming round robin",
                    StatusCode( StatusCode::RECOVERABLE ), 10 );
  }
  else return ( StatusCode::SUCCESS );
  
}
//=============================================================================
StatusCode STTELL1Monitoring::getClusterContainer()
{
  debug()<< " ==> getClusterContainer() " <<endmsg;

  debug() << "Retrieving " << m_clusterCont << endmsg;
  if ( !exist<LHCb::STClusters>(m_clusterCont ) )
  {
    debug() << " ----  No STClusters container retrieved --- "
            << endmsg;
    
    return (StatusCode::FAILURE);
    
  }
  else
  {
    m_clusters=get<LHCb::STClusters>(m_clusterCont );
    debug() << "---- STCluster Container retrieved from location : Default ----"
            << endmsg;
    
    debug() << "Number of clusters found in TES: "
	    << m_clusters->size() <<endmsg;
  }
  
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode STTELL1Monitoring::getEmuClusterContainer()
{
  debug()<< " ==> getEmuClusterContainer() " <<endmsg;

  
  debug() << "Retrieving " << m_emuClusterCont << endmsg;
  if ( !exist<LHCb::STClusters>(m_emuClusterCont ) )
  {
    debug() << " ----  No Emulated STClusters container retrieved --- "
            << endmsg;
    
    return (StatusCode::FAILURE);
    
  }
  else
  {
    m_emuClusters=get<LHCb::STClusters>(m_emuClusterCont );
    debug() << "---- STCluster Container retrieved from location : Emulated ----"
            << endmsg;
    
    debug() << "Number of clusters found in TES: "
            << m_emuClusters->size() <<endmsg;
  }
  
  return ( StatusCode::SUCCESS );
}
//============================================================================
StatusCode STTELL1Monitoring::fillZS(Tuple& zs_tuple)
{
  debug()<< " ==> fillZS() " <<endmsg;
  //
  
  //Save tell1s which have an error bank (just skip the whole tell1 for now)
  std::vector<unsigned int> skipTELL1s;
  if(m_skipIfErrorBank) {
    if (!exist<STTELL1BoardErrorBanks>(m_errorLoc)) 
      debug() << "No error banks found" << endmsg;
    else {
      m_errorBanks = get<STTELL1BoardErrorBanks>(m_errorLoc);
      debug() << "Error bank found for TELL1s: ";
      STTELL1BoardErrorBanks::const_iterator errorBank = m_errorBanks->begin();
      for(; errorBank!=m_errorBanks->end(); errorBank++) {
        debug() << (*errorBank)->key() << " ";
        skipTELL1s.push_back((*errorBank)->key());
      }
      debug() << endmsg;
    }
  }

  StatusCode clusterStatus=getClusterContainer();
  
  if(!clusterStatus.isSuccess())
    return Warning("Cannot retrieve cluster container.",clusterStatus,10);
  
  else {
    m_clusterIt = m_clusters->begin();
    bool findMatch = true;
    if(!getEmuClusterContainer()) findMatch = false;
 
    for(; m_clusterIt!=m_clusters->end();++m_clusterIt) {
      
      STCluster* cluster = (*m_clusterIt);
      STChannelID chan = cluster->channelID();  
      std::vector<STChannelID> chans = cluster->channels();
      unsigned int tell1 = (readoutTool()->offlineChanToDAQ(chan,0)).first.id();
      
      //check if tell1 has errorbank and if so, skip bank in this event
      if(m_skipIfErrorBank) {
        std::vector<unsigned int>::iterator findTELL1;
        findTELL1 = find(skipTELL1s.begin(),skipTELL1s.end(),tell1);
        if(findTELL1 != skipTELL1s.end()) continue;
      }

      int type(0);
      if(findMatch&&m_emuClusters->object(chan)) {
        bool foundMatch = true;
        STCluster::ADCVector ADCVec = cluster->stripValues();
        STCluster *testCluster = m_emuClusters->object(chan);
        STCluster::ADCVector testADCVec = testCluster->stripValues();
        if(ADCVec.size()!=testADCVec.size()) foundMatch = false;
        else {
          for(unsigned int i(0); i<ADCVec.size(); i++) {
            STCluster::ADCVector::iterator iter = find(testADCVec.begin(),testADCVec.end(),ADCVec[i]);
            if(iter==testADCVec.end()) foundMatch = false;
          }
        }
        if(foundMatch) {
          type = 2;
          
          //check if the spillover matches
          debug() << "spillover. ZS: " << (int)cluster->highThreshold() 
                  << ". Emulator: " << (int)testCluster->highThreshold() << endmsg;
          if((int)cluster->highThreshold()!=(int)testCluster->highThreshold())
            warning() << "spillover discrepancy in clusters. \n"
                      << "ZS: " << (int)cluster->highThreshold() << ". Emulator: " << (int)testCluster->highThreshold() << "\n"
                      << "TELL1 " << tell1 << ", strip: " << chan.strip() << endmsg;
        }
      }
      
      std::vector<int> strips,ADCs,DAQstrips;
      for(unsigned int j=0; j<chans.size(); j++) {
        strips.push_back(chans[j].strip());
        ADCs.push_back(cluster->adcValue(j));
        DAQstrips.push_back((readoutTool()->offlineChanToDAQ(chans[j],0)).second);
      } 

      unsigned int pp = DAQstrips[0]/768;

      zs_tuple->column("strip",chan.strip());
      zs_tuple->column("event",m_evtNumber);
      zs_tuple->column("tell1",tell1);
      zs_tuple->column("pp",pp);
      zs_tuple->column("totalADC",(int)cluster->totalCharge());
      zs_tuple->column("interStrip",cluster->interStripFraction());
      zs_tuple->column("station",chan.station());
      zs_tuple->column("layer",chan.layer());
      zs_tuple->column("region",chan.detRegion());
      zs_tuple->column("sector",chan.sector());
      zs_tuple->column("type",type);
      zs_tuple->column("spillover",(int)cluster->highThreshold());
      zs_tuple->farray("strips",strips,"clusterSize",4); //vector
      zs_tuple->farray("ADCs",ADCs,"clusterSize",4); //vector
      zs_tuple->farray("DAQstrips",DAQstrips,"clusterSize",4); //vector
      
      if(m_MCTruth) {
        
        int MCTrue(0),match(0),match_left(0),match_right(0);
        int totalADC_left(0), totalADC_right(0);
        double noise(0);        
        
        std::string asctLocation = m_clusterCont + "2MCHits"; 
        AsctTool associator(evtSvc(), asctLocation);
        const Table* aTable = associator.direct();
        if (aTable) { 
          
          // get MC truth for this cluster
          Range range = aTable->relations(cluster);
          if (!range.empty()) { //MCTrue
            MCTrue = 1;
            const DeSTSector* sector = findSector(chan);
            noise = sector->noise(chan);
            if(findMatch&&m_emuClusters->object(chan)) { //not checking charge
              match = 1;
            }
            if(findMatch&&m_emuClusters->object(tracker()->nextLeft(chan))) {
              match_left = 1;
              STCluster* cluster_compare = m_emuClusters->object(tracker()->nextLeft(chan));
              totalADC_left = (int)cluster_compare->totalCharge(); 
            }
            if(findMatch&&m_emuClusters->object(tracker()->nextRight(chan))) {
              match_right = 1;
              STCluster* cluster_compare = m_emuClusters->object(tracker()->nextRight(chan));
              totalADC_right = (int)cluster_compare->totalCharge(); 
            }
          }
        }
        zs_tuple->column("MCTrue",MCTrue);
        zs_tuple->column("noise",noise);
        zs_tuple->column("match",match);
        zs_tuple->column("match_left",match_left);
        zs_tuple->column("match_right",match_right);
        zs_tuple->column("totalADC_left",totalADC_left);
        zs_tuple->column("totalADC_right",totalADC_right);
      }
      bool writeOK = true;
      if(m_roundrobin) {
        std::map<unsigned int,std::vector<unsigned int> >::iterator map_it = m_availableData.find(tell1);
        if(map_it==m_availableData.end()) writeOK = false;
        else {
          debug() << "TELL1 " << tell1 << " found in available Round Robin data" << endmsg;
          std::vector<unsigned int> pps = map_it->second;
          if(find(pps.begin(),pps.end(),pp)==pps.end()) writeOK = false;
          else debug() << "Found pp " << pp << endmsg;
        }
      }
      if(writeOK) zs_tuple->write();
    }
  }
  

  clusterStatus=getEmuClusterContainer();
  if(!clusterStatus.isSuccess())
    return Warning("Cannot retrieve emulation cluster container.",clusterStatus,10);
  else {
    
    m_emuClusterIt = m_emuClusters->begin();

    for(; m_emuClusterIt!=m_emuClusters->end();++m_emuClusterIt) {
      
      STCluster* cluster = (*m_emuClusterIt);
      STChannelID chan = cluster->channelID();
      unsigned int tell1 = (readoutTool()->offlineChanToDAQ(chan,0)).first.id();
      
      if(getClusterContainer()&&m_clusters->object(chan)) {
        bool foundMatch = true;
        STCluster::ADCVector ADCVec = cluster->stripValues();
        STCluster *testCluster = m_clusters->object(chan);
        STCluster::ADCVector testADCVec = testCluster->stripValues();
        if(ADCVec.size()!=testADCVec.size()) foundMatch = false;  
        else {
          for(unsigned int i(0); i<ADCVec.size(); i++) {
            STCluster::ADCVector::iterator iter = find(testADCVec.begin(),testADCVec.end(),ADCVec[i]);
            if(iter==testADCVec.end()) foundMatch = false;
          }
        }
        if(foundMatch) continue;
      }
      
      //check if tell1 has errorbank and if so, skip bank in this event
      if(m_skipIfErrorBank) {
        std::vector<unsigned int>::iterator findTELL1;
        findTELL1 = find(skipTELL1s.begin(),skipTELL1s.end(),tell1);
        if(findTELL1 != skipTELL1s.end()) continue;
      }
      
      int type(1);
      
      std::vector<STChannelID> chans = cluster->channels();
      std::vector<int> strips,ADCs,DAQstrips;
      for(unsigned int j=0; j<chans.size(); j++) {
        strips.push_back(chans[j].strip());
        ADCs.push_back(cluster->adcValue(j));
        DAQstrips.push_back((readoutTool()->offlineChanToDAQ(chans[j],0)).second);
      } 

      unsigned int pp = DAQstrips[0]/768;

      zs_tuple->column("strip",chan.strip());
      zs_tuple->column("event",m_evtNumber);
      zs_tuple->column("tell1",tell1);
      zs_tuple->column("pp",pp);
      zs_tuple->column("totalADC",(int)cluster->totalCharge());
      zs_tuple->column("interStrip",cluster->interStripFraction());
      zs_tuple->column("station",chan.station());
      zs_tuple->column("layer",chan.layer());
      zs_tuple->column("region",chan.detRegion());
      zs_tuple->column("sector",chan.sector());
      zs_tuple->column("type",type); 
      zs_tuple->column("spillover",(int)cluster->highThreshold());
      zs_tuple->farray("strips",strips,"clusterSize",4); //vector
      zs_tuple->farray("ADCs",ADCs,"clusterSize",4); //vector
      zs_tuple->farray("DAQstrips",DAQstrips,"clusterSize",4); //vector

      bool writeOK = true;
      if(m_roundrobin) {
        std::map<unsigned int,std::vector<unsigned int> >::iterator map_it = m_availableData.find(tell1);
        if(map_it==m_availableData.end()) writeOK = false;
        else {
          debug() << "TELL1 " << tell1 << " found in available Round Robin data" << endmsg;
          std::vector<unsigned int> pps = map_it->second;
          if(find(pps.begin(),pps.end(),pp)==pps.end()) writeOK = false;
          else debug() << "Found pp " << pp << endmsg;
        }
      }
      if(writeOK) zs_tuple->write();
    }
  }
  
  return (StatusCode::SUCCESS);
}

//============================================================================
StatusCode STTELL1Monitoring::fillNZS(Tuple& nzs_tuple)
{
  debug()<< " ==> fillNZS() " <<endmsg;
  //

  if(!m_availableData.empty()) {
    
    //fill the data    
    LHCb::STTELL1Datas::const_iterator STMainItBegin;
    LHCb::STTELL1Datas::const_iterator STMainItEnd;
    
    for(unsigned int i=0; i<m_processVec.size(); i++) {
      if(m_processVec[i].first){
        STMainItBegin = (m_processVec[i].second)->begin();
        STMainItEnd = (m_processVec[i].second)->end();
        break;
      }
    }
    
    if(m_processVec[STFull].first) m_rawIt=m_rawDatas->begin();
    if(m_processVec[pedSub].first) m_pedsubIt=m_pedsubDatas->begin();
    if(m_processVec[LCMS].first) m_lcmsIt=m_lcmsDatas->begin();
    if(m_processVec[peds].first) m_pedsIt=m_peds->begin();
    if(m_processVec[emuPeds].first) m_emuPedsIt=m_emuPeds->begin();
    
    LHCb::STTELL1Data::Data stheaders;
    
    for(; STMainItBegin != STMainItEnd ; ++STMainItBegin){
      
      if(m_processVec[STFull].first) stheaders = (*m_rawIt)->header();
      if(m_processVec[STFull].first) m_STFullData = (*m_rawIt)->data();
      if(m_processVec[pedSub].first) m_pedsubData = (*m_pedsubIt)->data();
      if(m_processVec[LCMS].first) m_lcmsData = (*m_lcmsIt)->data();
      if(m_processVec[peds].first) m_ped = (*m_pedsIt)->data();
      if(m_processVec[emuPeds].first) m_emuPed = (*m_emuPedsIt)->data();
      
      int tell1=(*STMainItBegin)->key();
      
      nzs_tuple->column("event",m_evtNumber);
      nzs_tuple->column("tell1",tell1);
      
      if(m_includeCondDBValues) {
        std::vector<int> conf_threshold, disabled;
        int disabled_array[ALL_STRIPS] = {0};
        
        //Setting the values of disabled beetles 
        for(unsigned int i=0; i<TOT_BEETLES; i++){
          if(m_disableBeetles[tell1][i]==1){
            for(unsigned int channel=0; channel<CHANNELS_PER_BEETLE; channel++)
              disabled_array[i*CHANNELS_PER_BEETLE+channel] = 1;
          }
          std::bitset<4> bs((long)m_disableLinksPerBeetle[tell1][i]);
          for(unsigned int bit=0; bit<4; bit++) {
            if(bs[bit]) {
              for(unsigned int channel=0; channel<CHANNELS_PER_BEETLE; channel++)
                disabled_array[i*CHANNELS_PER_BEETLE+bit*CHANNELS_PER_LINK+channel] = 1;
            }
          }
        }
        for(unsigned int i=0; i<ALL_STRIPS; i++) {
          conf_threshold.push_back(m_confirmationThreshold[tell1][(int)i/64]);
          disabled.push_back(disabled_array[i]);
        }
        nzs_tuple->column("header_enable",m_headerCorrectionEnable[tell1]);
        nzs_tuple->array("mask",m_linkMask[tell1]);
        nzs_tuple->array("lcms_threshold",m_cmsThreshold[tell1]);
        nzs_tuple->array("hit_threshold",m_hitThreshold[tell1]);
        nzs_tuple->array("conf_threshold",conf_threshold); //vector - not variable size
        nzs_tuple->array("header_correction",m_headerCorrections[tell1]);
        nzs_tuple->array("disabled",disabled); //vector - not variable size
      }
      
      std::vector<int> rawADC,pedsubADC,lcmsADC,pedestal,TELL1Pedestal,strip;
      std::vector<int> header;

      for(int i = 0; i<PP_FPGA*BEETLES_PER_PP_FPGA; i++)
        for(int j = 0; j<ALINKS_PER_BEETLE*CHANNELS_PER_ALINK; j++) {
          if(m_processVec[STFull].first) rawADC.push_back(m_STFullData[i][j]);
          if(m_processVec[pedSub].first) pedsubADC.push_back(m_pedsubData[i][j]);
          if(m_processVec[LCMS].first) lcmsADC.push_back(m_lcmsData[i][j]);
          if(m_processVec[emuPeds].first) pedestal.push_back(m_emuPed[i][j]);
          if(m_processVec[peds].first) TELL1Pedestal.push_back(m_ped[i][j]);
          strip.push_back(i*ALINKS_PER_BEETLE*CHANNELS_PER_ALINK+j);  
        }

      if(m_processVec[STFull].first) {
        int nHeadersPerLink = 3;
        for(int i = 0; i<PP_FPGA*BEETLES_PER_PP_FPGA; i++)
          for(int j = 0; j<ALINKS_PER_BEETLE*nHeadersPerLink; j++) 
            header.push_back(stheaders[i][j]);
      }
      
      nzs_tuple->array("strip",strip);
      
      if(m_processVec[STFull].first) nzs_tuple->array("rawADC",rawADC);
      if(m_processVec[pedSub].first) nzs_tuple->array("pedsubADC",pedsubADC);
      if(m_processVec[LCMS].first) nzs_tuple->array("lcmsADC",lcmsADC);
      if(m_processVec[emuPeds].first) nzs_tuple->array("pedestal",pedestal);
      if(m_processVec[peds].first) nzs_tuple->array("TELL1Pedestal",TELL1Pedestal);
      if(m_processVec[STFull].first) nzs_tuple->array("header",header);
      
      if(m_processVec[STFull].first) ++m_rawIt;
      if(m_processVec[pedSub].first) ++m_pedsubIt;
      if(m_processVec[LCMS].first) ++m_lcmsIt;
      if(m_processVec[emuPeds].first) ++m_emuPedsIt;
      if(m_processVec[peds].first) ++m_pedsIt;  

      nzs_tuple->write();

    } 
  }
  
  return (StatusCode::SUCCESS);
}

//============================================================================
StatusCode STTELL1Monitoring::initConditions() {

  info() << "==> initConditions()" << endmsg;

  clearVectors();

  std::vector<STTell1ID> tell1Vec = readoutTool()->boardIDs();
  unsigned int i_tell1, i_link;
  const std::vector< int > * vecIntPtr;
  const std::vector< std::string > * vecStrPtr;
  STVetraCondition *condition;
  std::vector<int> linkMask;
  std::vector<int> cmsThreshold;
  std::vector<int> hitThreshold;
  std::vector<int> disableBeetles;
  std::vector<std::string> disableLinksPerBeetle;
  
  for(unsigned int i = 0; i<tell1Vec.size(); i++) { 
    i_tell1 = tell1Vec[i].id();

    condition = m_stVetraCfg -> getSTVetraCond( i_tell1 );
    
    sdataVec headerCorrections;  
    
    for(i_link = 0; i_link<ALINKS; i_link++)
    {
      vecIntPtr = condition -> headerCorrectionAnalogLink( i_link );
      m_headerCorrections[i_tell1].assign( vecIntPtr -> begin(),
                                           vecIntPtr -> end() );
    }    

    m_headerCorrectionEnable[i_tell1] = condition -> headerEnable();
    vecIntPtr = condition -> confirmationThreshold();
    m_confirmationThreshold[i_tell1].assign( vecIntPtr -> begin(),
                                             vecIntPtr -> end() );
    
    //unable to store unsigned chars/ints in xml. Therefore this conversion is needed.
    vecIntPtr = condition -> pedestalMask();
    linkMask.assign( vecIntPtr -> begin(), vecIntPtr -> end() );
    vecIntPtr = condition -> cmsThreshold();
    cmsThreshold.assign( vecIntPtr -> begin(), vecIntPtr -> end() );
    vecIntPtr = condition -> hitThreshold();
    hitThreshold.assign( vecIntPtr -> begin(), vecIntPtr -> end() );
        
    for(unsigned int k = 0; k < linkMask.size(); k++) {
      m_linkMask[i_tell1].push_back(static_cast<STTELL1::u_int8_t>(linkMask[k]));
    }   
    
    for(unsigned int k = 0; k < cmsThreshold.size(); k++) {
      m_cmsThreshold[i_tell1].push_back(static_cast<STTELL1::u_int8_t>(cmsThreshold[k]));
    }
    
    for(unsigned int k = 0; k < hitThreshold.size(); k++) {
      m_hitThreshold[i_tell1].push_back(static_cast<STTELL1::u_int32_t>(hitThreshold[k]));
    }

    //for convenience storing 2 disabled beetles in 1
    
    vecIntPtr = condition -> disableBeetles();
    disableBeetles.assign( vecIntPtr -> begin(), vecIntPtr -> end() );
        
    for(unsigned int k = 0; k < disableBeetles.size(); k++) 
      m_disableBeetles[i_tell1].push_back(static_cast<unsigned int>(disableBeetles[k]));
    
    vecStrPtr = condition -> disableLinksPerBeetle();
    disableLinksPerBeetle.assign( vecStrPtr -> begin(), vecStrPtr -> end() );
    
    for(unsigned int k = 0; k < disableLinksPerBeetle.size(); k++) 
      m_disableLinksPerBeetle[i_tell1].push_back(atoi(disableLinksPerBeetle[k].c_str()));
    
  }  
  return ( StatusCode::SUCCESS );
}

//=============================================================================

void STTELL1Monitoring::clearVectors()
{
  // We must clear all the used vectors :
  // m_linkMask
  // m_cmsThreshold
  // m_hitThreshold
  // m_disableBeetles
  // m_disableLinksPerBeetle
  
  std::vector<STTell1ID> tell1Vec(readoutTool()->boardIDs());
  unsigned int i_tell1, NumberOfTELL1s(tell1Vec.size());
  for(unsigned int i(0); i < NumberOfTELL1s; i++)
  { 
    i_tell1 = tell1Vec[i].id();
    m_linkMask[i_tell1].clear();
    m_cmsThreshold[i_tell1].clear();
    m_hitThreshold[i_tell1].clear();
    m_disableBeetles[i_tell1].clear();
    m_disableLinksPerBeetle[i_tell1].clear();
    
  }
}


//=============================================================================
