// $Id: STTELL1LCMS.h,v 1.6 2009/11/18 12:23:36 akeune Exp $
#ifndef STTELL1LCMS_H 
#define STTELL1LCMS_H 1

// Include files
#include "STTELL1Algorithm.h"

/** @class STTELL1LCMS STTELL1LCMS.h
 *  
 *
 *  @author Tomasz Szumlak, Anne Keune
 *  @date   2007-02-01
 */

class STTELL1LCMSProcessEngine;

class STTELL1LCMS: public STTELL1Algorithm {

public: 

  /** Standard constructor **/
  STTELL1LCMS( const std::string& name, ISvcLocator* pSvcLocator );

  /** Destructor **/
  virtual ~STTELL1LCMS(); 

  /** Algorithm initialization **/
  virtual StatusCode initialize() override;

  /** Algorithm execution **/  
  virtual StatusCode execute() override;

  /** Algorithm finalization **/
  virtual StatusCode finalize() override;

protected:

  void prepareEngine();
  void clearEngine();
  StatusCode runLCMSuppression();
  virtual StatusCode initConditions(); 
  void clearVectors();

private:

  STTELL1LCMSProcessEngine* m_lcmsEngine;
  STTELL1::CMSThresholdMap m_cmsThreshold;
  std::map<unsigned int,unsigned int> m_cmsEnable;


};
#endif // STTELL1LCMS_H
