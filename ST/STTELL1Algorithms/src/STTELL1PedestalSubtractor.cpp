// $Id: STTELL1PedestalSubtractor.cpp,v 1.24 2010/01/13 13:05:06 akeune Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h"

// stl
#include <vector>
#include <algorithm>

//boost
#include <boost/lexical_cast.hpp>
#include "Kernel/STLexicalCaster.h"

// read CondDB
#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

// local
#include "STTELL1PedestalSubtractor.h"

// engine classes
#include "TELL1Engine/STTELL1PedestalProcessEngine.h"
#include "TELL1Engine/STTELL1UpdateProcessEngine.h"

//-----------------------------------------------------------------------------
// Implementation file for class : STTELL1PedestalSubtractor
//
// 2007-02-08 : Tomasz Szumlak, Anne Keune
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STTELL1PedestalSubtractor )

using namespace LHCb;
using namespace STTELL1;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STTELL1PedestalSubtractor::STTELL1PedestalSubtractor(const std::string& name, 
                                                     ISvcLocator* pSvcLocator)
  : STTELL1Algorithm ( name , pSvcLocator ),
    m_TELL1Data ( ), 
    m_TELL1Headers ( ),
    m_pedestalSumMemory ( ),
    m_subPeds( 0 ),
    m_pedestalEngine ( 0 ),
    m_updateEngine ( 0 )
  
{
  declareProperty("UseTELL1PedestalBank", m_useTELL1PedestalBank=false);

  declareSTConfigProperty("InputDataLoc", m_inputDataLoc, STTELL1DataLocation::TTFull);
  declareSTConfigProperty("OutputDataLoc", m_outputDataLoc, STTELL1DataLocation::TTPedSubADCs);
  declareSTConfigProperty("SubPedsLoc",m_subPedsLoc, STTELL1DataLocation::TTSubPeds);
  declareSTConfigProperty("TELL1PedestalBankLoc", m_TELL1PedestalBankLoc, STTELL1DataLocation::TTPedestal);
  
}
//=============================================================================
// Destructor
//=============================================================================
STTELL1PedestalSubtractor::~STTELL1PedestalSubtractor() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode STTELL1PedestalSubtractor::initialize() 
{
  debug() << "==> initialize()" << endmsg;
  
  StatusCode sc = STTELL1Algorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc; 
  
  //Update Manager
  std::vector<STTell1ID> tell1Vec = readoutTool()->boardIDs();
  for(unsigned int i = 0; i<tell1Vec.size(); i++) {
    unsigned int i_tell1 = tell1Vec[i].id();
    const std::string tell1 = boost::lexical_cast<std::string>(i_tell1);
    std::string condName = "TELL1Board" + tell1;
    std::string cond = m_condPath + "/" + condName;
    registerCondition(cond,
                      &STTELL1PedestalSubtractor::initConditions );
  }
  
  // force first updates
  sc = runUpdate();
  if (sc.isFailure()) return Error ( "Failed first UMS update", sc );
  
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode STTELL1PedestalSubtractor::execute() 
{
  debug() << "==> execute()" << endmsg;
  
  m_evtNumber++;

  //get pedestal bank id required
  if(m_useTELL1PedestalBank) {
    StatusCode pedestalStatus=getPedestals();
    if( !pedestalStatus.isSuccess() )
      return Warning("Failed to get pedestal bank or it is empty. Switch option off.",pedestalStatus,10);  
  }

  StatusCode dataStatus( getData() );

  if(dataStatus.isSuccess())
  { 
    /// output - subtracted pedestals
    m_subPeds = new STTELL1Datas();  
    
    STTELL1Datas::const_iterator TELL1It = inputData()->begin();
    for( ; TELL1It!=inputData()->end(); ++TELL1It)
    {
      m_tell1=(*TELL1It)->TELL1ID(); 
      m_sentPPs=(*TELL1It)->sentPPs();

      if(m_evtNumberTELL1.find(m_tell1)==m_evtNumberTELL1.end()) m_evtNumberTELL1[m_tell1]=0;        
        m_evtNumberTELL1[m_tell1]+=1;

        debug() << "Processing TELL1 " << m_tell1 << "with sent PPs: ";
        for(unsigned int i(0); i<m_sentPPs.size();i++)
          debug() << m_sentPPs[i] << " ";
        debug() << endmsg;
        
        debug() << "This is event " << m_evtNumberTELL1[m_tell1] << " for this TELL1." << endmsg;
      
      if(m_pedestalEnable[m_tell1])
      {  
        prepareEngine();
        runPedestalSub();
        clearEngine();
      }
      else
      {
        cloneData();
      }
    }
  }
  else if ( dataStatus.isRecoverable() )
  {
    // Here we have to create an empty container for the next step
    debug() << "We assume it's round robin data" << endmsg;

    // make container of TELL1 boards
    m_outputData = new STTELL1Datas();
    m_subPeds    = new STTELL1Datas();
  }
  else
    return Warning("Failed to get data",dataStatus,10);  
    
  writeData();
  writePedestals();

  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode STTELL1PedestalSubtractor::finalize() 
{
  debug() << "==> finalize()" << endmsg;

  // must be called after all other actions
  return STTELL1Algorithm::finalize();
}
//=============================================================================
StatusCode STTELL1PedestalSubtractor::initConditions() {

  info() << "==> initConditions()" << endmsg;

  clearVectors();

  std::vector<STTell1ID> tell1Vec = readoutTool()->boardIDs();
  unsigned int i_tell1, i_link;
  STVetraCondition *condition;
  const std::vector< int > *vecIntPtr;
  sdataVec headerCorrections;
  std::vector<int> linkPedestal;
  std::vector<int> linkMask;
  int headerLowThreshold, headerHighThreshold;
  std::string pseudoHeaderLowThreshold, pseudoHeaderHighThreshold;
  
  for(unsigned int i = 0; i<tell1Vec.size(); i++)
  {
    i_tell1 = tell1Vec[i].id();

    condition = m_stVetraCfg -> getSTVetraCond( i_tell1 );
    
    m_headerCorrectionEnable[i_tell1] = condition -> headerEnable();
    m_zsEnable[i_tell1] = condition -> zsEnable();
    m_pedestalEnable[i_tell1] = condition -> pedestalEnable();
    m_updateEnable[i_tell1] = condition -> updateEnable();
    
    for(i_link = 0; i_link<ALINKS; i_link++)
    {
      vecIntPtr = condition -> headerCorrectionAnalogLink( i_link );
      m_headerCorrections[i_tell1].insert( m_headerCorrections[i_tell1].end(),vecIntPtr -> begin(),vecIntPtr -> end() );
    }   

    //unable to store unsigned chars/ints in xml. Therefore this conversion is needed.
    vecIntPtr = condition -> pedestalValue();
    linkPedestal.assign( vecIntPtr -> begin(), vecIntPtr -> end() );
    vecIntPtr = condition -> pedestalMask();
    linkMask.assign( vecIntPtr -> begin(), vecIntPtr -> end() );
    
    for(unsigned int k = 0; k < linkPedestal.size(); k++)
    {
      m_linkPedestal[i_tell1].push_back(static_cast<STTELL1::u_int8_t>(linkPedestal[k]));
      m_linkMask[i_tell1].push_back(static_cast<STTELL1::u_int8_t>(linkMask[k]));
    }
    
    headerLowThreshold = condition -> headerLowThreshold();
    headerHighThreshold = condition -> headerHighThreshold();
      
    m_headerThresholds[i_tell1].push_back(static_cast<STTELL1::u_int32_t>(headerLowThreshold));
    m_headerThresholds[i_tell1].push_back(static_cast<STTELL1::u_int32_t>(headerHighThreshold));

    pseudoHeaderLowThreshold = condition -> pseudoHeaderLowThreshold();
    pseudoHeaderHighThreshold = condition -> pseudoHeaderHighThreshold();
      
    char* end;
    m_pseudoHeaderThresholds[i_tell1].push_back(static_cast<STTELL1::u_int32_t>(strtol(pseudoHeaderLowThreshold.c_str(),&end,16)));
    m_pseudoHeaderThresholds[i_tell1].push_back(static_cast<STTELL1::u_int32_t>(strtol(pseudoHeaderHighThreshold.c_str(),&end,16)));
    
    debug() << "TELL1 " << i_tell1 << " process info: " 
            << "Pedestal subtraction = " << m_pedestalEnable[i_tell1] 
            << ", Pedestal update = "<< m_updateEnable[i_tell1]
            << ", Header Correction = "<< m_headerCorrectionEnable[i_tell1]
            << ", Zero Suppression = "<< m_zsEnable[i_tell1] << endmsg;
    
  }  
  return ( StatusCode::SUCCESS );
}

//=============================================================================

void STTELL1PedestalSubtractor::clearVectors()
{
  // Pedestal subtractor alg
  // We must clear all the used vectors :
  // m_linkPedestal
  // m_linkMask
  // m_headerThresholds
  
  std::vector<STTell1ID> tell1Vec(readoutTool()->boardIDs());
  unsigned int i_tell1, NumberOfTELL1s(tell1Vec.size());
  for(unsigned int i(0); i < NumberOfTELL1s; i++)
  { 
    i_tell1 = tell1Vec[i].id();
    m_linkPedestal[i_tell1].clear();
    m_linkMask[i_tell1].clear();
    m_headerThresholds[i_tell1].clear();
  }
}


//=============================================================================
StatusCode STTELL1PedestalSubtractor::runPedestalSub()
{
  debug()<< "==> runPedestalSub()" <<endmsg;
   
  // create output container for pedestals
  // and prepare memory for pedestal sum
  prepareMemory();
   
  m_TELL1Data.clear();
  m_TELL1Data = (inputData()->object(m_tell1))->data();
  
  m_TELL1Headers.clear();
  m_TELL1Headers = (inputData()->object(m_tell1))->header();
  if(m_TELL1Headers.size()==0) warning() << "No headers found" << endmsg;

  // Get the vector of which PPs have sent data
  std::vector<unsigned int> sentPPs = (inputData()->object(m_tell1))->sentPPs();

  STTELL1Data* subPeds =new STTELL1Data(m_tell1);
  STTELL1Data* subData =new STTELL1Data(m_tell1);
  // rawADCs    - data before ped subtraction
  // peds       - subtracted pedestals
  sdataVec rawADCs = flattenData(&m_TELL1Data);
  sdataVec peds(ALL_STRIPS, 0);

  // add 1 header per link (3->4) also done in TELL1
  sdataVec headers;
  for(int i = 0; i < TOT_BEETLES; i++) { //24    
    for(int j = 0; j < 12; j++) { //number of headers
      if((j%3)==0 ) headers.push_back(0);
      headers.push_back(m_TELL1Headers[i][j]);  
    }  
  }
  // get sum of the pedestals  
  sdataVec& pedestalSum=m_pedestalSumMemory[m_tell1];

  if(m_useTELL1PedestalBank) {
    LHCb::STTELL1Data::Data TELL1Pedestals =  (m_TELL1Pedestals->object(m_tell1))->data();
    sdataVec flatTELL1Pedestals = flattenData(&TELL1Pedestals);
    sdatIt pIT=flatTELL1Pedestals.begin();
    linkIT lIT=m_linkPedestal[m_tell1].begin();
    for( ; pIT!=flatTELL1Pedestals.end(); ++pIT, ++lIT) 
      *lIT = *pIT; 
  }
  else {
    // transform pedestal sum into pedestals
    // 10bit shift -> dividing by 1024
    sdatIt psIT=pedestalSum.begin();
    linkIT lIT=m_linkPedestal[m_tell1].begin();    
    for( ; psIT!=pedestalSum.end(); ++psIT, ++lIT)
      *lIT = (((*psIT))>>PED_SUM_SHIFT);
  }
  

  // subtract pedestals that were calculated for the previous event
  // pass input data to the engine object
  m_pedestalEngine->setInData(rawADCs);
  m_pedestalEngine->setHeaders(headers);
  m_pedestalEngine->setLinkPedestal(m_linkPedestal[m_tell1]);
  m_pedestalEngine->runSubtraction();

  subData->setData(structureData(m_pedestalEngine->outData()));
  subData->setSentPPs( sentPPs );
  outputData(subData);
 
  // translate pedestals from STTELL1::u_int8_t into signed int
  linkIT lpIT=m_linkPedestal[m_tell1].begin();
  sdatIt pedsIT=peds.begin();
  for( ; lpIT!=m_linkPedestal[m_tell1].end(); ++pedsIT, ++lpIT)
    *pedsIT=static_cast<signed int>(*lpIT);

  subPeds->setData(structureData(peds));
  subPeds->setSentPPs( sentPPs );
  m_subPeds->insert(subPeds);

  // calculate pedestals for the next event by refreshing ped sum
  m_updateEngine->setInData(rawADCs);
  m_updateEngine->setHeaders(headers);
  m_updateEngine->setPedestalSum(pedestalSum);
  m_updateEngine->runUpdate();
  //m_pedestalSumMemory[m_tell1]=m_updateEngine->updatedPedestalSum();

  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode STTELL1PedestalSubtractor::prepareMemory()
{
  debug()<< " ==> prepareMemory() " <<endmsg;
  //
  //Create the initial pedestal sum per strip
  if ( m_pedestalSumMemory.find( m_tell1 ) == m_pedestalSumMemory.end() )
  {
    debug()<< "Memory preparation for tell1 " << m_tell1 <<endmsg;
    sdataVec linkPedestalSum;
    linkIT lIT=m_linkPedestal[m_tell1].begin();
    for(;lIT!=m_linkPedestal[m_tell1].end();++lIT)
      linkPedestalSum.push_back(static_cast<signed int>(*lIT)*1024);

    m_pedestalSumMemory.insert(TELL1RAMPair(m_tell1,linkPedestalSum));
  }

  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void STTELL1PedestalSubtractor::prepareEngine()
{
  debug()<< " ==> prepareEngine() "<<endmsg;
  
  // pedestal engine
  m_pedestalEngine=new STTELL1PedestalProcessEngine();
  m_pedestalEngine->setProcessEnable(m_pedestalEnable[m_tell1]);
  m_pedestalEngine->setHeaderCorrectionFlag(m_headerCorrectionEnable[m_tell1]);
  m_pedestalEngine->setZSEnable(m_zsEnable[m_tell1]);
  m_pedestalEngine->setLinkMask(m_linkMask[m_tell1]);
  m_pedestalEngine->setHeaderCorrectionValues(m_headerCorrections[m_tell1]);
  m_pedestalEngine->setHeaderThresholdValues(m_headerThresholds[m_tell1]);
  m_pedestalEngine->setPseudoHeaderThresholdValues(m_pseudoHeaderThresholds[m_tell1]);

  // update engine - end of pedestal following cycle
  m_updateEngine=new STTELL1UpdateProcessEngine();
  m_updateEngine->setProcessEnable(m_updateEnable[m_tell1]);
  m_updateEngine->setHeaderCorrectionFlag(m_headerCorrectionEnable[m_tell1]);
  m_updateEngine->setHeaderCorrectionValues(m_headerCorrections[m_tell1]);
  m_updateEngine->setHeaderThresholdValues(m_headerThresholds[m_tell1]);
  m_updateEngine->setPseudoHeaderThresholdValues(m_pseudoHeaderThresholds[m_tell1]);
  m_updateEngine->setLinkMask(m_linkMask[m_tell1]);
}
//=============================================================================
void STTELL1PedestalSubtractor::clearEngine()
{
  debug()<< " ==> clearEngine() "<<endmsg;

  delete m_pedestalEngine;
  delete m_updateEngine;
}
//=============================================================================
StatusCode STTELL1PedestalSubtractor::writePedestals()
{
  debug()<< " ==> writePedestals() " <<endmsg;
  
  if(m_subPeds!=0){
    put(m_subPeds, m_subPedsLoc);
  }else{
    debug()<< " ==> empty pedestal container! " <<endmsg;
  }
  return ( StatusCode::SUCCESS );
}	 
//=============================================================================	
StatusCode STTELL1PedestalSubtractor::getPedestals()
{
  debug()<< " ==> getPedestals() "<<endmsg;
  
  if(!exist<STTELL1Datas>(m_TELL1PedestalBankLoc)){
    debug()<< " ==> There is no TELL1 Pedestal Bank: "
           << m_TELL1PedestalBankLoc << endmsg;
    return ( StatusCode::FAILURE );
  }else{  
    // get pedestal banks from default TES location
    m_TELL1Pedestals=get<STTELL1Datas>(m_TELL1PedestalBankLoc);
    debug()<< " ==> The pedestals have been read-in from location: "
           << m_TELL1PedestalBankLoc
           << ", size of the data container: "
           << m_TELL1Pedestals->size() <<endmsg;
    if(m_TELL1Pedestals->empty()) {
      debug() << "Pedestal bank is empty!" << endmsg;
      return ( StatusCode::FAILURE );
    }
  }  
  return ( StatusCode::SUCCESS );
}
//=============================================================================
