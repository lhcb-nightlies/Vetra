// $Id: STTELL1ClusterMaker.cpp,v 1.24 2009/11/18 12:23:36 akeune Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h"

// boost
#include <boost/assign/std/vector.hpp>
#include <boost/lexical_cast.hpp>

// read CondDB
#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

// stl
#include <vector>
#include <algorithm>
#include <bitset>

// local
#include "STTELL1ClusterMaker.h"

// engine class
#include "TELL1Engine/STTELL1ZSProcessEngine.h"

//-----------------------------------------------------------------------------
// Implementation file for class : STTELL1ClusterMaker
//
// 2006-10-02 : Tomasz Szumlak, Anne Keune
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STTELL1ClusterMaker )

using namespace boost::assign;
using namespace STTELL1;
using namespace LHCb;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STTELL1ClusterMaker::STTELL1ClusterMaker( const std::string& name,
                                          ISvcLocator* pSvcLocator)
  : STTELL1Algorithm ( name, pSvcLocator ),
    m_rawEvent ( 0 ),
    m_clusters ( ),
    m_clustersMem ( ),
    m_adcs ( ),
    m_adcsMem ( ),
    m_bankVersion ( V4 ),
    m_zsEngine ( 0 )
{
  m_boundaryStrips += 0, 64, 128, 192, 256, 320, 384, 448, 512, 576, 
    640, 704, 768, 832, 896, 960, 1024, 1088, 1152, 1216, 
    1280, 1344, 1408, 1472, 1536, 1600, 1664, 1728, 1792, 1856, 
    1920, 1984, 2048, 2112, 2176, 2240, 2304, 2368, 2432, 2496,
    2560, 2624, 2688, 2752, 2816, 2880, 2944, 3008;
  
  declareProperty("RawEventLoc", m_rawEventLoc = RawEventLocation::Emulated );
  declareProperty("UseFixedPCN", m_useFixedPCN = true );
  
  declareSTConfigProperty("originalDataLoc" , m_originalDataLoc, STTELL1DataLocation::TTFull);
  declareSTConfigProperty("InputDataLoc", m_inputDataLoc, STTELL1DataLocation::TTLCMSADCs);
}
//=============================================================================
// Destructor
//=============================================================================
STTELL1ClusterMaker::~STTELL1ClusterMaker() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode STTELL1ClusterMaker::initialize() {
  StatusCode sc = STTELL1Algorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc; 
  
  debug() << "==> initialize" << endmsg;
  
  // Update Manager
  std::vector<STTell1ID> tell1Vec = readoutTool()->boardIDs();
  for(unsigned int i = 0; i<tell1Vec.size(); i++) {
    unsigned int i_tell1 = tell1Vec[i].id();
    const std::string tell1 = boost::lexical_cast<std::string>(i_tell1);
    std::string condName = "TELL1Board" + tell1;
    std::string cond = m_condPath + "/" + condName;
    registerCondition(cond,
                      &STTELL1ClusterMaker::initConditions );
  }
    
  // force first updates
  sc = runUpdate();
  if (sc.isFailure())
    return Error ( "Failed first UMS update", sc );
  
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode STTELL1ClusterMaker::execute() {
  
  debug() << "==> execute()" << endmsg;
  
  m_evtNumber++;
  
  StatusCode dataStatus=getData();
  StatusCode rawEvtStatus=createRawEvent();
  StatusCode cluMakerStatus;
  
  if(dataStatus.isSuccess()&&rawEvtStatus.isSuccess())
  {
    STTELL1Datas::const_iterator TELL1It =inputData()->begin();
    for( ; TELL1It!=inputData()->end(); ++TELL1It){
      m_tell1=(*TELL1It)->TELL1ID(); 
      m_sentPPs=(*TELL1It)->sentPPs();
      
      if(m_evtNumberTELL1.find(m_tell1)==m_evtNumberTELL1.end()) m_evtNumberTELL1[m_tell1]=0;        
      m_evtNumberTELL1[m_tell1]+=1;
      
      debug() << "Processing TELL1 " << m_tell1 << "with sent PPs: ";
      for(unsigned int i(0); i<m_sentPPs.size();i++)
        debug() << m_sentPPs[i] << " ";
      debug() << endmsg;
      
      debug() << "This is event " << m_evtNumberTELL1[m_tell1] << " for this TELL1." << endmsg;
      
      
      if(m_zsEnable[m_tell1] && m_evtNumberTELL1[m_tell1]>m_convergenceLimit)
      { 
        prepareEngine();
        cluMakerStatus=runClusterMaker();
        clearEngine();
        
        if( !cluMakerStatus.isSuccess() )
          return Error( " The cluster maker was not successful! ", cluMakerStatus );
      }
    }
    fillAndWriteRawEvent();
    flushMemory();   
    
  }
  else if ( dataStatus.isRecoverable() )
    return Warning( "Obtained data is empty", StatusCode( StatusCode::SUCCESS ), 10 );
  else
    return Warning( "Failed to get data or event could not be created", dataStatus, 10 );
  
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode STTELL1ClusterMaker::finalize() {
  
  debug() << "==> Finalize" << endmsg;
  
  // must be called after all other actions
  return STTELL1Algorithm::finalize();
}
//=============================================================================
StatusCode STTELL1ClusterMaker::initConditions() {
  
  info() << "==> initConditions()" << endmsg;
  
  clearVectors();
  
  std::vector<STTell1ID> tell1Vec = readoutTool()->boardIDs();
  unsigned int i_tell1;

  STVetraCondition *condition;
  const std::vector< int >* vecIntPtr;
  const std::vector< std::string >* vecStrPtr;
  
  for(unsigned int i = 0; i<tell1Vec.size(); i++)
  { 
    i_tell1 = tell1Vec[i].id();
    
    condition = m_stVetraCfg -> getSTVetraCond( i_tell1 );
    
    
    m_zsEnable[i_tell1] = condition -> zsEnable();
    
    vecIntPtr = condition -> confirmationThreshold();
    m_confirmationThresholds[i_tell1].assign( vecIntPtr -> begin(),
                                              vecIntPtr -> end() );
    
    vecIntPtr = condition -> spilloverThreshold();
    m_spillOverThresholds[i_tell1].assign( vecIntPtr -> begin(),
                                           vecIntPtr -> end() );
    
    //unable to store unsigned chars/ints in xml. Therefore this conversion is needed.
    std::vector<int> hitThresholds;
    int ppMaxClusters;
    
    vecIntPtr = condition -> hitThreshold();
    hitThresholds.assign( vecIntPtr -> begin(),
                          vecIntPtr -> end() );

    ppMaxClusters = condition -> ppMaxClusters(); //unsigned int
    
    for(unsigned int k = 0; k < hitThresholds.size(); k++) 
      m_hitThresholds[i_tell1].push_back(static_cast<STTELL1::u_int32_t>(hitThresholds[k]));
    
    m_ppMaxClusters[i_tell1] = static_cast<unsigned int>(ppMaxClusters);
    
    //for convenience storing 2 disabled beetles in 1
    std::vector<int> disableBeetles_0_11;
    std::vector<int> disableBeetles_12_23;
    
    vecIntPtr = condition -> disableBeetles();
    disableBeetles_0_11.assign( vecIntPtr -> begin(), vecIntPtr -> begin() + 12 );
    
    disableBeetles_12_23.assign( vecIntPtr -> begin() + 12, vecIntPtr -> end() );
    
    for(unsigned int k = 0; k < disableBeetles_0_11.size(); k++) 
      m_disableBeetles[i_tell1].push_back(static_cast<unsigned int>(disableBeetles_0_11[k]));
    
    for(unsigned int k = 0; k < disableBeetles_12_23.size(); k++) 
      m_disableBeetles[i_tell1].push_back(static_cast<unsigned int>(disableBeetles_12_23[k]));
    
    std::vector<std::string> disableLinksPerBeetle_0_11;
    std::vector<std::string> disableLinksPerBeetle_12_23;
    
    vecStrPtr = condition -> disableLinksPerBeetle();
    disableLinksPerBeetle_0_11.assign( vecStrPtr -> begin(), vecStrPtr -> begin() + 12 );
    disableLinksPerBeetle_12_23.assign( vecStrPtr -> begin() + 12, vecStrPtr -> end() );
    
    for(unsigned int k = 0; k < disableLinksPerBeetle_0_11.size(); k++) 
      m_disableLinksPerBeetle[i_tell1].push_back(atoi(disableLinksPerBeetle_0_11[k].c_str()));
    
    for(unsigned int k = 0; k < disableLinksPerBeetle_12_23.size(); k++) 
      m_disableLinksPerBeetle[i_tell1].push_back(atoi(disableLinksPerBeetle_12_23[k].c_str()));

    debug() << "TELL1 " << i_tell1 << " process info: " 
            << "Zero Suppression = "<< m_zsEnable[i_tell1] << endmsg;
    
  }
  return (StatusCode::SUCCESS);
}

//=============================================================================
void STTELL1ClusterMaker::clearVectors()
{
  debug() << " clearVectors() " << endmsg;
  
  std::vector<STTell1ID> tell1Vec(readoutTool()->boardIDs());
  unsigned int i_tell1, NumberOfTELL1s(tell1Vec.size());
  for(unsigned int i(0); i < NumberOfTELL1s; i++)
  { 
    i_tell1 = tell1Vec[i].id();
    m_hitThresholds[i_tell1].clear();
    m_disableBeetles[i_tell1].clear();
    m_disableLinksPerBeetle[i_tell1].clear();
  }
}
//=============================================================================
StatusCode STTELL1ClusterMaker::createRawEvent()
{
  debug()<< " createRawEvent() " <<endmsg;
  m_rawEvent = getOrCreate<LHCb::RawEvent, LHCb::RawEvent>(m_rawEventLoc);
  
  return ( StatusCode::SUCCESS );
}
//=============================================================================
StatusCode STTELL1ClusterMaker::runClusterMaker()
{
  debug()<< " ==> runClusterMaker() " <<endmsg;

  //
  m_clusters.clear();
  m_adcs.clear();

  if(m_evtNumberTELL1[m_tell1]==m_convergenceLimit+1)
    m_TELL1s.push_back(m_tell1);
  
  STTELL1Data::Data data=(inputData()->object(m_tell1))->data();
  sdataVec flatData = flattenData(&data);
  
  //Setting the values of disabled beetles to 0 before processing
  for(unsigned int i=0; i<TOT_BEETLES; i++){
    if(m_disableBeetles[m_tell1][i]==1){
      for(unsigned int channel=0; channel<CHANNELS_PER_BEETLE; channel++)
        flatData[i*CHANNELS_PER_BEETLE+channel] = 0;
    }
    std::bitset<4> bs((long)m_disableLinksPerBeetle[m_tell1][i]);
    for(unsigned int bit=0; bit<4; bit++) {
      if(bs[bit]) {
        for(unsigned int channel=0; channel<CHANNELS_PER_BEETLE; channel++)
          flatData[i*CHANNELS_PER_BEETLE+bit*CHANNELS_PER_LINK+channel] = 0;
      }
    }
  }
  
  m_zsEngine->setInData(flatData);    
  m_zsEngine->setOutClusters(m_clusters);
  m_zsEngine->setOutADCS(m_adcs);
  m_zsEngine->runZeroSuppression();
  
  debug() << "Number of clusters found: " << m_clusters.size() << endmsg;
  
  m_clustersMem[m_tell1]=m_clusters;
  m_adcsMem[m_tell1]=m_adcs;
  
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void STTELL1ClusterMaker::flushMemory()
{
  debug()<< " ==> flushMemory() " <<endmsg;
  //
  m_clustersMem.clear();
  m_adcsMem.clear();
}
//=============================================================================
StatusCode STTELL1ClusterMaker::fillAndWriteRawEvent()
{
  debug()<< " ==> fillAndWriteRawEvent() " <<endmsg;

  if(!m_useFixedPCN) m_originalData = get<STTELL1Datas>(m_originalDataLoc);

  cdatIt IT=m_TELL1s.begin();
  for( ; IT!=m_TELL1s.end(); ++IT){
    // clear bank body for next bank
    m_bankBody.clear();
    TELL1ClusterVec clusters=m_clustersMem[(*IT)];
    unsigned int clusterNumber=clusters.size();
    
    // create and sort a header word
    unsigned int inSync = 0u; unsigned int pcn = 0u;

    if(!m_useFixedPCN&&m_originalData->object(*IT))      
      pcnVote(m_originalData->object(*IT), pcn, inSync);
    
    debug() << "PCN number used: " << pcn << endmsg;
    
    SiHeaderWord aHWord(clusterNumber, pcn,  inSync);
    SiDAQ::buffer_word aHeader=aHWord.value();
    m_bankBody.push_back(aHeader);
    // check if clusters were produced for current sensor
    // check if the cluster number is even
    
    unsigned bufferWords=0;
    if((clusterNumber)&&(clusterNumber%CLUSTER_PER_WORD==0)){
      
      // calculate number of buffer_words needed to code clusters
      bufferWords = (unsigned int) clusterNumber/CLUSTER_PER_WORD;
      
      for(unsigned int word=0; word<bufferWords; ++word){
        SiDAQ::buffer_word codedClusters=0x0, tempBuffWord=0x0;
        for(int cluCnt=0; cluCnt<CLUSTER_PER_WORD; ++cluCnt){
          tempBuffWord=clusters[CLUSTER_PER_WORD*word+cluCnt];
          tempBuffWord<<=(cluCnt*CLUSTER_SHIFT);
          codedClusters|=tempBuffWord;
        }
        m_bankBody.push_back(codedClusters);
      }
      
    }else if((clusterNumber)&&((clusterNumber%CLUSTER_PER_WORD!=0))){
      
      unsigned int fullWords = (unsigned int) clusterNumber/CLUSTER_PER_WORD;
      // take into account last cluster
      bufferWords=fullWords+1;
      for(unsigned int word=0; word<bufferWords; ++word){
        SiDAQ::buffer_word codedClusters=0x0, tempBuffWord=0x0;
        // write first full words (2 clusters per word)
        if(clusterNumber==1){
          codedClusters|=clusters[CLUSTER_PER_WORD*word];
          m_bankBody.push_back(codedClusters);
        }else if((clusterNumber>1)&&(word<fullWords)){
          for(int cluCnt=0; cluCnt<CLUSTER_PER_WORD; ++cluCnt){
            tempBuffWord=clusters[CLUSTER_PER_WORD*word+cluCnt];
            tempBuffWord<<=(cluCnt*CLUSTER_SHIFT);
            codedClusters|=tempBuffWord;
          }
          m_bankBody.push_back(codedClusters);
        }else if((clusterNumber>1)&&(word==fullWords)){
          codedClusters|=clusters[CLUSTER_PER_WORD*word];
          m_bankBody.push_back(codedClusters);
        }
      }
    }
    
    // store adcs in raw bank
    TELL1ADCVec adcs=m_adcsMem[(*IT)];
    unsigned int adcNumber=adcs.size();
    if(clusterNumber&&(adcNumber%ADCS_PER_WORD==0)){
      
      // calculate number of buffer_words needed to code the adcs 
      bufferWords=(unsigned int) adcNumber/ADCS_PER_WORD;
      
      for(unsigned int word=0; word<bufferWords; ++word){
        SiDAQ::buffer_word codedADCS=0x0, tempBuffWord=0x0;
        for(int adcCnt=0; adcCnt<ADCS_PER_WORD; ++adcCnt){
          tempBuffWord=adcs[ADCS_PER_WORD*word+adcCnt];
          tempBuffWord<<=(adcCnt*ADC_SHIFT);
          codedADCS|=tempBuffWord;
          
        }
        m_bankBody.push_back(codedADCS);
      } 
    }
    
    else if(clusterNumber&&(adcNumber%ADCS_PER_WORD!=0)){
      
      unsigned int fullWords=adcNumber/ADCS_PER_WORD;
      // take into account remaining adcs
      bufferWords=fullWords+1;
      for(unsigned int word=0; word<bufferWords; ++word){
        SiDAQ::buffer_word codedADCS=0x0, tempBuffWord=0x0;
        // write first full word (4 adcs per word)
        if(word<fullWords){
          for(int adcCnt=0; adcCnt<ADCS_PER_WORD; ++adcCnt){
            tempBuffWord=adcs[ADCS_PER_WORD*word+adcCnt];
            tempBuffWord<<=(adcCnt*ADC_SHIFT);
            codedADCS|=tempBuffWord;
          }
          m_bankBody.push_back(codedADCS);
        }else if(word==fullWords){
          // calculate number of adcs for the last buffer word
          unsigned int lastADCS=adcNumber%ADCS_PER_WORD;
          for(unsigned int adcCnt=0; adcCnt<lastADCS; ++adcCnt){
            tempBuffWord=adcs[ADCS_PER_WORD*word+adcCnt];
            tempBuffWord<<=(adcCnt*ADC_SHIFT);
            codedADCS|=tempBuffWord;
          }
          m_bankBody.push_back(codedADCS);
        }
      }
    }
    
    // calculate bank size in bytes:
    int paddingSpace=adcNumber%ADCS_PER_WORD;
    m_bankBodySize=sizeof(SiDAQ::buffer_word)*m_bankBody.size()-
      (paddingSpace?sizeof(SiDAQ::buffer_word)-
       paddingSpace*sizeof(STTELL1::u_int8_t):0);
    
    // store raw bank
    m_bankType = RawBank::TT;
    if(detType() == "IT") m_bankType = RawBank::IT;
    
    RawBank* aBank=
      m_rawEvent->createBank(static_cast<SiDAQ::buffer_word>(*IT),
                             m_bankType, m_bankVersion, m_bankBodySize, &(m_bankBody[0]));
        
    m_rawEvent->adoptBank(aBank,true);
  }
  
  //
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void STTELL1ClusterMaker::prepareEngine()
{
  
  debug() << "==> prepareEngine()" << endmsg;
  
  m_zsEngine=new STTELL1ZSProcessEngine();
  m_zsEngine->setProcessEnable(m_zsEnable[m_tell1]);
  m_zsEngine->setHitThresholds(m_hitThresholds[m_tell1]);
  m_zsEngine->setConfirmationThresholds(m_confirmationThresholds[m_tell1]);
  m_zsEngine->setSpillOverThresholds(m_spillOverThresholds[m_tell1]);
  m_zsEngine->setBoundaryStrips(m_boundaryStrips);  
  m_zsEngine->setPPMaxClusters(m_ppMaxClusters[m_tell1]);
}
//--
//=============================================================================
void STTELL1ClusterMaker::clearEngine()
{
  debug() << "==> clearEngine()" << endmsg;
  delete m_zsEngine;
}
//--
//=============================================================================
void STTELL1ClusterMaker::pcnVote(const LHCb::STTELL1Data* originalData,
                                  unsigned int& pcn, unsigned int& inSync) const{
  
  // get the PP info
  std::vector<const LHCb::STTELL1EventInfo*> info  = originalData->validData();
  if (info.size() == 0) return;
  
  // take first enabled pp as reference
  std::vector<const LHCb::STTELL1EventInfo*>::const_iterator iterPP = info.begin();
  while ( iterPP != info.end() && (*iterPP)->enabled() == false ){
    ++iterPP;
  }
  
  if (iterPP == info.end()) return; // no pps enabled
  
  // this is the reference pcn
  pcn = (*iterPP)->pcnVote(); 
  ++iterPP;
  
  // test if all pp are in sync
  for (; iterPP != info.end() ; ++iterPP){
    if ((*iterPP)->enabled() == true && (*iterPP)->pcnVote() != pcn ) {
      inSync = false;
      break;
    } // if
  } // loop pp
} 
