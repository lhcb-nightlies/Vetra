// $Id: STTELL1LCMS.cpp,v 1.15 2009/11/18 12:23:36 akeune Exp $
// Include files

// from Gaudi
#include "GaudiKernel/AlgFactory.h"

// stl
#include <vector>
#include <algorithm>

//boost
#include <boost/lexical_cast.hpp>

// read CondDB
#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

// local
#include "STTELL1LCMS.h"

// engine classes
#include "TELL1Engine/STTELL1LCMSProcessEngine.h"

//-----------------------------------------------------------------------------
// Implementation file for class : STTELL1LCMS
//
// 2007-02-01 : Tomasz Szumlak, Anne Keune
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STTELL1LCMS )

using namespace LHCb;
using namespace STTELL1;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STTELL1LCMS::STTELL1LCMS( const std::string& name,
                          ISvcLocator* pSvcLocator)
  : STTELL1Algorithm( name , pSvcLocator ),
    m_lcmsEngine ( 0 )
{
  declareSTConfigProperty("InputDataLoc", m_inputDataLoc, STTELL1DataLocation::TTPedSubADCs);
  declareSTConfigProperty("OutputDataLoc", m_outputDataLoc, STTELL1DataLocation::TTLCMSADCs);

}
//=============================================================================
// Destructor
//=============================================================================
STTELL1LCMS::~STTELL1LCMS() {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode STTELL1LCMS::initialize() 
{
  debug() << "==> initialize()" << endmsg;

  StatusCode sc = STTELL1Algorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;

  // Update Manager
  std::vector<STTell1ID> tell1Vec = readoutTool()->boardIDs();
  for(unsigned int i = 0; i<tell1Vec.size(); i++) {
    unsigned int i_tell1 = tell1Vec[i].id();
    const std::string tell1 = boost::lexical_cast<std::string>(i_tell1);
    std::string condName = "TELL1Board" + tell1;
    std::string cond = m_condPath + "/" + condName;
    registerCondition(cond,
                      &STTELL1LCMS::initConditions );
  }

  sc = runUpdate();  
  if (sc.isFailure()) return Error ( "Failed first UMS update", sc );
  
  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode STTELL1LCMS::execute() 
{

  debug() << "==> execute()" << endmsg;
  
  m_evtNumber++;
  StatusCode dataStatus( getData() );

  if(dataStatus.isSuccess())
  {      
    STTELL1Datas::const_iterator TELL1It =inputData()->begin();
    for( ; TELL1It!=inputData()->end(); ++TELL1It){
      m_tell1=(*TELL1It)->TELL1ID(); 
      m_sentPPs=(*TELL1It)->sentPPs();
      
      if(m_evtNumberTELL1.find(m_tell1)==m_evtNumberTELL1.end()) m_evtNumberTELL1[m_tell1]=0;        
      m_evtNumberTELL1[m_tell1]+=1;
      
      debug() << "Processing TELL1 " << m_tell1 << "with sent PPs: ";
      for(unsigned int i(0); i<m_sentPPs.size();i++)
        debug() << m_sentPPs[i] << " ";
      debug() << endmsg;
        
      debug() << "This is event " << m_evtNumberTELL1[m_tell1] << " for this TELL1." << endmsg;
	
      if(m_cmsEnable[m_tell1]&&(m_evtNumberTELL1[m_tell1]>m_convergenceLimit))
      {
        prepareEngine();
        runLCMSuppression();
        clearEngine();
      }
      else cloneData();
    }
    
    writeData();
  }
  else if ( dataStatus.isRecoverable() )
  {
    // Here we have to create an empty container for the next step
    debug() << "We assume it's round robin data" << endmsg;

    // make container of TELL1 boards
    m_outputData = new STTELL1Datas();    
    // put the object on the transient event store
    put( m_outputData, m_outputDataLoc );

    return StatusCode::SUCCESS;
  }
  else return Warning("Failed to get data",dataStatus,10);
  
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode STTELL1LCMS::finalize() 
{
  debug() << "==> finalize()" << endmsg;

  return STTELL1Algorithm::finalize();  // must be called after all other actions
}
//=============================================================================
StatusCode STTELL1LCMS::initConditions() {
  
  info() << "==> initConditions()" << endmsg;

  clearVectors();

  std::vector<STTell1ID> tell1Vec = readoutTool()->boardIDs();
  unsigned int i_tell1;
  std::vector<int> cmsThreshold;
  STVetraCondition *condition;
  const std::vector< int > *vecIntPtr;
  
  for(unsigned int i = 0; i<tell1Vec.size(); i++)
  { //deze loop wil ik eigenlijk niet
    i_tell1 = tell1Vec[i].id();

    condition = m_stVetraCfg -> getSTVetraCond( i_tell1 );
    
    m_cmsEnable[i_tell1] = condition -> cmsEnable();
    
    //unable to store unsigned chars/ints in xml. Therefore this conversion is needed.
    vecIntPtr = condition -> cmsThreshold();
    cmsThreshold.assign( vecIntPtr -> begin(), vecIntPtr -> end() );
    
    for(unsigned int k = 0; k < cmsThreshold.size(); k++) 
      m_cmsThreshold[i_tell1].push_back(static_cast<STTELL1::u_int8_t>(cmsThreshold[k]));

    debug() << "TELL1 " << i_tell1 << " process info: " 
	    << "Common Mode Suppression = "<< m_cmsEnable[i_tell1] << endmsg;

  }
  
  return (StatusCode::SUCCESS);
}
//=============================================================================
void STTELL1LCMS::clearVectors()
{
  // Common mode suppression alg
  // We must clear all the used vectors :
  // m_cmsThreshold
  
  std::vector<STTell1ID> tell1Vec(readoutTool()->boardIDs());
  unsigned int i_tell1, NumberOfTELL1s(tell1Vec.size());
  for(unsigned int i(0); i < NumberOfTELL1s; i++)
  { 
    i_tell1 = tell1Vec[i].id();
    m_cmsThreshold[i_tell1].clear();
  }
}
//=============================================================================
StatusCode STTELL1LCMS::runLCMSuppression()
{
  debug()<< "==> runLCMSuppression()" <<endmsg;

  STTELL1Data* lcmsData=new STTELL1Data(m_tell1);
  STTELL1Data::Data inData=(inputData()->object(m_tell1))->data();
  sdataVec flatData = flattenData(&inData);
  m_lcmsEngine->setInData(flatData);
  m_lcmsEngine->runLCMS();
  lcmsData->setData(structureData(m_lcmsEngine->outData()));
  lcmsData->setSentPPs( (inputData()->object(m_tell1))->sentPPs() );
  // store the corrected data
  outputData(lcmsData);
  
  return ( StatusCode::SUCCESS );
}
//=============================================================================
void STTELL1LCMS::prepareEngine()
{
  debug() << "==> prepareEngine()" << endmsg;

  m_lcmsEngine=new STTELL1LCMSProcessEngine();
  m_lcmsEngine->setProcessEnable(m_cmsEnable[m_tell1]);
  m_lcmsEngine->setCMSThreshold(m_cmsThreshold[m_tell1]);
}
//=============================================================================
void STTELL1LCMS::clearEngine()
{
  debug() << "==> clearEngine()" << endmsg;
  delete m_lcmsEngine;
}
