// $Id: STTELL1Monitoring.h,v 1.7 2010/04/19 13:51:05 mtobin Exp $
#ifndef STTELL1MONITORING_H 
#define STTELL1MONITORING_H 1

// Include files
#include "STTELL1Algorithm.h"

// from Gaudi
//#include "Kernel/STTupleAlgBase.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "Kernel/STTell1Board.h"
#include "Event/STCluster.h"
#include "STDet/DeSTDetector.h"
#include "Event/MCHit.h"
#include "Linker/LinkerTool.h"
#include "Event/STTELL1BoardErrorBank.h"

#include<map>
#include<vector>
#include<utility>

/** @class STTELL1Monitoring STTELL1Monitoring.h
 *  
 *
 *  @author Anne Keune
 *  @date   2007-10-15
 */

class DeSTDetector;

namespace LHCb{
  class MCHit;
  class STCluster;
  class MCParticle;
}

class STTELL1Monitoring : public STTELL1Algorithm {
public: 
  /// Standard constructor
  STTELL1Monitoring(const std::string& name, ISvcLocator* pSvcLocator );

  enum processData {
    STFull = 0,
    pedSub = 1,
    LCMS = 2,
    emuPeds = 3,
    peds = 4,
  };

  enum processSize {
    procTOT = 5
  };

  virtual ~STTELL1Monitoring( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getNZSData();
  StatusCode checkAvailableData();
  StatusCode getClusterContainer();
  StatusCode getEmuClusterContainer();
  StatusCode fillZS(Tuple&);
  StatusCode fillNZS(Tuple&);
  virtual StatusCode initConditions();
  void clearVectors();  

private:

  std::map<unsigned int,std::vector<unsigned int> > m_disableBeetles;
  std::map<unsigned int,std::vector<unsigned int> > m_disableLinksPerBeetle; //hex
  std::map<unsigned int,unsigned int> m_headerCorrectionEnable;
  STTELL1::HitThresholdMap m_hitThreshold;
  STTELL1::ConfirmationThresholdMap m_confirmationThreshold;
  STTELL1::CMSThresholdMap m_cmsThreshold;
  STTELL1::LinkMap m_linkMask;
  STTELL1::HeaderCorrectionMap m_headerCorrections;

  typedef LinkerTool<LHCb::STCluster, LHCb::MCHit> AsctTool;
  typedef AsctTool::DirectType Table;
  typedef Table::Range Range;

  LHCb::STTELL1Datas* m_rawDatas;
  LHCb::STTELL1Datas* m_pedsubDatas;
  LHCb::STTELL1Datas* m_lcmsDatas;

  LHCb::STTELL1Datas* m_peds;
  LHCb::STTELL1Datas* m_emuPeds;

  LHCb::STTELL1Datas::const_iterator m_rawIt;
  LHCb::STTELL1Datas::const_iterator m_pedsubIt;
  LHCb::STTELL1Datas::const_iterator m_lcmsIt;

  LHCb::STTELL1Datas::const_iterator m_emuPedsIt;
  LHCb::STTELL1Datas::const_iterator m_pedsIt;
  
  LHCb::STTELL1Data::Data m_STFullData;
  LHCb::STTELL1Data::Data m_pedsubData;
  LHCb::STTELL1Data::Data m_lcmsData;
 
  LHCb::STTELL1Data::Data m_emuPed;
  LHCb::STTELL1Data::Data m_ped;

  LHCb::STTELL1BoardErrorBanks* m_errorBanks;
  std::string m_errorLoc;

  std::string m_rawDataLoc;
  std::string m_pedsubDataLoc;
  std::string m_lcmsDataLoc;
  
  std::string m_pedestalLoc;
  std::string m_TELL1PedestalLoc;

  std::string m_emuClusterCont;
  LHCb::STClusters* m_emuClusters;
  LHCb::STClusters::const_iterator m_emuClusterIt;

  std::string m_clusterCont;
  LHCb::STClusters* m_clusters;
  LHCb::STClusters::const_iterator m_clusterIt;
  
  bool m_STFull;
  bool m_pedSub;
  bool m_LCMS;

  std::vector<std::pair<bool,LHCb::STTELL1Datas*> > m_processVec;

  bool m_MCTruth;
  bool m_includeNZS;
  bool m_includeZS;
  bool m_includeCondDBValues;
  bool m_skipIfErrorBank;
  //unsigned int m_convergenceLimit;
  bool m_roundrobin;

  std::map<unsigned int, std::vector<unsigned int> > m_availableData;
  
  
  

};
#endif // STTELL1MONITORING_H
