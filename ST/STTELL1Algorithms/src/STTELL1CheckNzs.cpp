#include "STTELL1CheckNzs.h"

// from Gaudi
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/IEventProcessor.h"

#include "Kernel/STRawBankMap.h"
#include <algorithm>

#include "Event/RawEvent.h"

#include <sstream>
#include <fstream>

//-----------------------------------------------------------------------------
// Implementation file for class : STTELL1CheckNzs
//
// 2009-07-14 : Joschka Lingemann
//-----------------------------------------------------------------------------


// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STTELL1CheckNzs )

using namespace LHCb;
using namespace STTELL1;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STTELL1CheckNzs::STTELL1CheckNzs( const std::string& name, 
				  ISvcLocator* pSvcLocator)
  : STTELL1Algorithm( name, pSvcLocator  )
{
  declareProperty("LimitToTell", m_limitToTell );

  declareProperty("TargetNZS",m_targetNZS=false);
  declareProperty("NumberNZS",m_numberNZS=-1);

  //declareSTConfigProperty("DetType",m_detType,"TT");
  declareSTConfigProperty("InputLocation",m_inputLocation,RawEventLocation::Default);
}

//=============================================================================
// Destructor
//=============================================================================
STTELL1CheckNzs::~STTELL1CheckNzs() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode STTELL1CheckNzs::initialize() {
  StatusCode sc = STTELL1Algorithm::initialize();
  
  if (sc.isFailure() ) return sc;
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> initialize" << endmsg;
  
  // Select small number of TELL1s
  m_selectedTell1s = false;
  if ( m_limitToTell.size() > 0 ) {
    m_selectedTell1s = true;
    sort(m_limitToTell.begin(), m_limitToTell.end());
  }

  // Find minimum number of events to get m_numberNZS NZS raw banks
  if( m_numberNZS > 0){
    m_targetNZS = true;
    info() << "Only looking for " << m_numberNZS << " NZS banks; skipping emulation for the other events" << endmsg;
  }
  
  info() << "Data to check taken from " << m_inputLocation << endmsg;
  info() << "Detector: " << detType() << endmsg;

  m_nNZS = 0;
  m_totEvts = 0;
  m_found = false;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode STTELL1CheckNzs::execute()
{

  // Stop running if enough NZS raw banks
  if( m_targetNZS ){
    if(m_nNZS == m_numberNZS){
      info() << "Number on NZS (" << m_nNZS << ") equal to target number (" << m_numberNZS << "). Total number of events: " << m_totEvts << endmsg;
      SmartIF< IEventProcessor> ep = (SmartIF< IEventProcessor>)serviceLocator();
      if(ep){
	ep->stopRun();
	m_found = true;
      }
      else{
        error() << "Cannot get IEventProcessor instance." << endmsg;
        return StatusCode::FAILURE;
      }
    }
  }

  if ( msgLevel(MSG::DEBUG) ) {
    debug() << "==> execute" << endmsg;
    debug() << "Checking for NZS data..." << endmsg;
  }
  counter("Number of events") += 1; 
  ++m_totEvts;
  
  if(!exist<RawEvent>(m_inputLocation)) {
    setFilterPassed(false);
    return StatusCode::SUCCESS;
  } else {
    RawEvent* raw = get<RawEvent>( m_inputLocation);
    //Get bank including nzs data
    //RawBank::BankType bankType = STRawBankMap::stringToType(m_detType + "Full");
    RawBank::BankType bankType = STRawBankMap::stringToType(detType() + "Full");
    const std::vector<RawBank*>& itf = raw->banks(RawBank::BankType(bankType));
    // check if bank size is 0
    if (itf.size() == 0){
      if ( msgLevel(MSG::DEBUG) ) debug() << "No NZS data, skipping emulation..." << endmsg;
      setFilterPassed(false);
      return StatusCode::SUCCESS;
    }
    // Only interested in selected tell1s
    if( m_selectedTell1s ) {
      bool foundTell = false;
      std::vector<RawBank*>::const_iterator itB;
      for( itB = itf.begin(); itB != itf.end(); ++itB ) {
        const RawBank* p = *itB;
        unsigned int tellID = (this->readoutTool())->SourceIDToTELLNumber(p->sourceID());
        if ( msgLevel(MSG::DEBUG) ) debug() << "Looking for " << detType() << "TELL" << tellID << endmsg;
	foundTell = binary_search(m_limitToTell.begin(), m_limitToTell.end(), tellID);
        if( foundTell ) {
          if ( msgLevel(MSG::DEBUG) ) //debug() << "Found " << detType() << "TELL" << tellID << endmsg;
          info() << "Found " << detType() << "TELL" << tellID << endmsg;
	  break;
        }
      }
      if( ! foundTell ) {
        if ( msgLevel(MSG::DEBUG) ) debug() << "No NZS data for tells " << m_limitToTell << ", skipping emulation..." << endmsg;
	setFilterPassed(false);
        return StatusCode::SUCCESS;
      }
    }
  }

  ++m_nNZS;
  counter("Number of events with NZS") += 1;
  if ( msgLevel(MSG::DEBUG) ) debug() << "NZS data found, running emulation..." << endmsg;
  setFilterPassed(true);

  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalize
//=============================================================================
StatusCode STTELL1CheckNzs::finalize() {
  debug() << "==> finalize" << endmsg; 

  if( m_targetNZS && !m_found ){
    warning() << "Found NZS (" << m_nNZS << ") are less than requested (" << m_numberNZS << ")" << endmsg;
  }

  return STTELL1Algorithm::finalize();
}
