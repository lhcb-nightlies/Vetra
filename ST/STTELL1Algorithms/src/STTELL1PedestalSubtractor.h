// $Id: STTELL1PedestalSubtractor.h,v 1.10 2010/01/13 13:05:06 akeune Exp $
#ifndef STTELL1PEDESTALSUBTRACTOR_H 
#define STTELL1PEDESTALSUBTRACTOR_H 1

// Include files
#include "STTELL1Algorithm.h"

/** @class STTELL1PedestalSubtractor STTELL1PedestalSubtractor.h
 *  
 *
 *  @author Tomasz Szumlak, Anne Keune
 *  @date   2007-02-08
 */

class STTELL1PedestalProcessEngine;
class STTELL1UpdateProcessEngine;

class STTELL1PedestalSubtractor : public STTELL1Algorithm{
public: 
  
  /** Standard constructor **/
  STTELL1PedestalSubtractor(const std::string& name, ISvcLocator* pSvcLocator);

  /** Destructor **/
  virtual ~STTELL1PedestalSubtractor(); 

  /** Algorithm initialization **/
  virtual StatusCode initialize() override;  

  /** Algorithm execution **/
  virtual StatusCode execute() override; 

  /** Algorithm finalization **/
  virtual StatusCode finalize() override;

protected:

  StatusCode runPedestalSub();
  StatusCode prepareMemory();
  StatusCode writePedestals();
  void prepareEngine();
  void clearEngine();
  virtual StatusCode initConditions();
  void clearVectors();
  virtual StatusCode getPedestals();
  
private:

  LHCb::STTELL1Data::Data m_TELL1Data;
  LHCb::STTELL1Data::Data m_TELL1Headers;

  bool m_useTELL1PedestalBank;
  std::string m_TELL1PedestalBankLoc;
  LHCb::STTELL1Datas* m_TELL1Pedestals;  
   
  RAM m_pedestalSumMemory;           /// this one stores adc sum
  LHCb::STTELL1Datas* m_subPeds;
  STTELL1PedestalProcessEngine* m_pedestalEngine;
  STTELL1UpdateProcessEngine* m_updateEngine;

  STTELL1::LinkMap m_linkPedestal;
  STTELL1::LinkMap m_linkMask;
  STTELL1::HeaderCorrectionMap m_headerCorrections;
  STTELL1::HeaderThresholdMap m_headerThresholds;
  STTELL1::HeaderThresholdMap m_pseudoHeaderThresholds;
  std::map<unsigned int,unsigned int> m_headerCorrectionEnable;
  std::map<unsigned int,unsigned int> m_zsEnable;
  std::map<unsigned int,unsigned int> m_pedestalEnable;
  std::map<unsigned int,unsigned int> m_updateEnable;
   
};
#endif // STTELL1PEDESTALSUBTRACTOR_H
