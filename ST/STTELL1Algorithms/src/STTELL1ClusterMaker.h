// $Id: STTELL1ClusterMaker.h,v 1.10 2009/11/18 12:23:36 akeune Exp $
#ifndef STTELL1CLUSTERMAKER_H 
#define STTELL1CLUSTERMAKER_H 1

// Include files
#include "STTELL1Algorithm.h"

// event
#include "Event/RawEvent.h"

// Si
#include "SiDAQ/SiHeaderWord.h"
#include "SiDAQ/SiRawBufferWord.h"

/** @class STTELL1ClusterMaker STTELL1ClusterMaker.h
 *  
 *  @author Tomasz Szumlak, Anne Keune
 *  @date   2006-10-06
 */

class STTELL1ZSProcessEngine;

class STTELL1ClusterMaker : public STTELL1Algorithm{

public:

  enum version{
    V4=3
  };

  /** Standard constructor **/
  STTELL1ClusterMaker(const std::string& name, ISvcLocator* pSvcLocator);

  /** Destructor **/
  virtual ~STTELL1ClusterMaker(); 

  /** Algorithm initialization **/
  virtual StatusCode initialize() override;  

  /** Algorithm execution **/
  virtual StatusCode execute() override; 

  /** Algorithm finalization **/
  virtual StatusCode finalize() override;

protected:

  StatusCode runClusterMaker();
  void flushMemory();
  StatusCode fillAndWriteRawEvent();
  StatusCode createRawEvent();
  void prepareEngine();
  void clearEngine();
  virtual StatusCode initConditions();
  void clearVectors();

private:

  void pcnVote(const LHCb::STTELL1Data* originalData,
	       unsigned int& pcn, unsigned int& inSync) const;

  bool m_useFixedPCN;

  LHCb::RawEvent* m_rawEvent;
  std::string m_rawEventLoc;

  //  LHCb::STTELL1EventInfos *m_Infos;
  //std::string m_EventInfoLoc;

  LHCb::RawBank::BankType m_bankType;

  std::string m_pedSubDataLoc;
  std::string m_originalDataLoc;
  LHCb::STTELL1Datas* m_originalData;


  STTELL1::HitThresholdMap m_hitThresholds;
  STTELL1::ConfirmationThresholdMap m_confirmationThresholds;
  STTELL1::SpillOverThresholdMap m_spillOverThresholds;
  std::map<unsigned int,unsigned int> m_ppMaxClusters;
  std::map<unsigned int,unsigned int> m_zsEnable;

  std::map<unsigned int,std::vector<unsigned int> > m_disableBeetles;
  std::map<unsigned int,std::vector<unsigned int> > m_disableLinksPerBeetle; //hex

  STTELL1::TELL1ClusterVec m_clusters;
  STTELL1::CLUSTER_MEMORY m_clustersMem;      /// list of created clusters
  STTELL1::TELL1ADCVec m_adcs;
  STTELL1::ADC_MEMORY m_adcsMem;              /// list of adcs contributed to the clusters
  dataVec m_TELL1s;                  /// container with source numbers
  std::vector<SiDAQ::buffer_word> m_bankBody;
  const int m_bankVersion;
  unsigned int m_bankBodySize;
  STTELL1ZSProcessEngine* m_zsEngine;

  STTELL1::BoundaryStripVec m_boundaryStrips;

};
#endif // STTELL1CLUSTERMAKER_H
