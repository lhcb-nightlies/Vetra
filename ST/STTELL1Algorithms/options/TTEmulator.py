from Gaudi.Configuration import *

from Configurables import STVetraCfg
vetraTool = STVetraCfg('TTVetraCfgTool')
vetraTool.DetType = 'TT'
vetraTool.CondPath = 'TTCondDB'

from Configurables import (  STTELL1CheckNzs, STFullDecoding, STTELL1PedestalSubtractor, STTELL1LCMS, STTELL1ClusterMaker )

ttCheckNZS = STTELL1CheckNzs( 'TTCheckNZS' )
ttCheckNZS.DetType = 'TT'
ttCheckNZS.VetraCfgToolType = vetraTool
ttCheckNZS.InputLocation =  '/Event/DAQ/RawEvent'

ttFullDecoding = STFullDecoding("TTFullDecoding")
ttFullDecoding.PrintErrorInfo   = 0;
ttFullDecoding.DetType          = "TT";
ttFullDecoding.OutputLevel      = 3;
ttFullDecoding.InputLocation    = "/Event/DAQ/RawEvent";
ttFullDecoding.OutputLocation    = "/Event/Raw/TT/Full";
ttFullDecoding.EventInfoLocation = "/Event/Raw/TT/EventInfo";

ttTELL1PedestalSubtractor = STTELL1PedestalSubtractor( 'TTTELL1PedestalSubtractor' )
ttTELL1PedestalSubtractor.CondPath = 'TTCondDB'
ttTELL1PedestalSubtractor.VetraCfgToolType = vetraTool
ttTELL1PedestalSubtractor.InputDataLoc = '/Event/Raw/TT/Full'
ttTELL1PedestalSubtractor.OutputDataLoc = '/Event/Raw/TT/PedSubADCs'
ttTELL1PedestalSubtractor.SubPedsLoc = '/Event/Raw/TT/SubPeds'
ttTELL1PedestalSubtractor.DetType = 'TT'
ttTELL1PedestalSubtractor.UseTELL1PedestalBank = False

ttTELL1LCMS = STTELL1LCMS( 'TTTELL1LCMS' )
ttTELL1LCMS.CondPath = 'TTCondDB'
ttTELL1LCMS.VetraCfgToolType = vetraTool
ttTELL1LCMS.InputDataLoc = '/Event/Raw/TT/PedSubADCs'
ttTELL1LCMS.OutputDataLoc = '/Event/Raw/TT/LCMSADCs' 
ttTELL1LCMS.DetType = 'TT' 

ttEmuSeq = GaudiSequencer( 'EmuTTSeq' )
ttEmuSeq.MeasureTime = True
#ApplicationMgr().TopAlg.append( ttEmuSeq )
ttEmuSeq.Members.append(ttCheckNZS)
ttEmuSeq.Members.append(ttFullDecoding)
ttEmuSeq.Members.append(ttTELL1PedestalSubtractor)
ttEmuSeq.Members.append(ttTELL1LCMS)

