# Emulation sequence for different spills in TAE mode.
# Each spill runs as a different sequence so that it can be aborted if there is no NZS bank in that spill.
# Then run pedestal subtraction and the LCMS subtraction

from Gaudi.Configuration import *

from Configurables import STVetraCfg
vetraTool = STVetraCfg('TTVetraCfgTool')
vetraTool.DetType = 'TT'
vetraTool.CondPath = 'TTCondDB'

from Configurables import (  STTELL1CheckNzs, STFullDecoding, STTELL1PedestalSubtractor, STTELL1LCMS, STTELL1ClusterMaker )

ttCheckNZSCentral = STTELL1CheckNzs( 'TTCheckNZSCentral' )
ttCheckNZSCentral.DetType = 'TT'
ttCheckNZSCentral.VetraCfgToolType = vetraTool
ttCheckNZSCentral.InputLocation =  '/Event/DAQ/RawEvent'

ttCheckNZSPrev2 = STTELL1CheckNzs( 'TTCheckNZSPrev2' )
ttCheckNZSPrev2.DetType = 'TT'
ttCheckNZSPrev2.VetraCfgToolType = vetraTool
ttCheckNZSPrev2.InputLocation =  '/Event/Prev2/DAQ/RawEvent'

ttCheckNZSPrev1 = STTELL1CheckNzs( 'TTCheckNZSPrev1' )
ttCheckNZSPrev1.DetType = 'TT'
ttCheckNZSPrev1.VetraCfgToolType = vetraTool
ttCheckNZSPrev1.InputLocation =  '/Event/Prev1/DAQ/RawEvent'

ttCheckNZSNext1 = STTELL1CheckNzs( 'TTCheckNZSNext1' )
ttCheckNZSNext1.DetType = 'TT'
ttCheckNZSNext1.VetraCfgToolType = vetraTool
ttCheckNZSNext1.InputLocation =  '/Event/Next1/DAQ/RawEvent'

ttCheckNZSNext2 = STTELL1CheckNzs( 'TTCheckNZSNext2' )
ttCheckNZSNext2.DetType = 'TT'
ttCheckNZSNext2.VetraCfgToolType = vetraTool
ttCheckNZSNext2.InputLocation =  '/Event/Next2/DAQ/RawEvent'

ttTELL1PedestalSubtractorPrev2 = STTELL1PedestalSubtractor( 'TTTELL1PedestalSubtractorPrev2' )
ttTELL1PedestalSubtractorPrev2.CondPath = 'TTCondDB'
ttTELL1PedestalSubtractorPrev2.VetraCfgToolType = vetraTool
ttTELL1PedestalSubtractorPrev2.InputDataLoc = '/Event/Prev2/Raw/TT/Full'
ttTELL1PedestalSubtractorPrev2.OutputDataLoc = '/Event/Prev2/Raw/TT/PedSubADCs'
ttTELL1PedestalSubtractorPrev2.SubPedsLoc = '/Event/Prev2/Raw/TT/SubPeds'
ttTELL1PedestalSubtractorPrev2.DetType = 'TT'

ttTELL1PedestalSubtractorPrev1 = STTELL1PedestalSubtractor( 'TTTELL1PedestalSubtractorPrev1' )
ttTELL1PedestalSubtractorPrev1.CondPath = 'TTCondDB'
ttTELL1PedestalSubtractorPrev1.VetraCfgToolType = vetraTool
ttTELL1PedestalSubtractorPrev1.InputDataLoc = '/Event/Prev1/Raw/TT/Full'
ttTELL1PedestalSubtractorPrev1.OutputDataLoc = '/Event/Prev1/Raw/TT/PedSubADCs'
ttTELL1PedestalSubtractorPrev1.SubPedsLoc = '/Event/Prev1/Raw/TT/SubPeds'
ttTELL1PedestalSubtractorPrev1.DetType = 'TT'

ttTELL1PedestalSubtractorCentral = STTELL1PedestalSubtractor( 'TTTELL1PedestalSubtractorCentral' )
ttTELL1PedestalSubtractorCentral.CondPath = 'TTCondDB'
ttTELL1PedestalSubtractorCentral.VetraCfgToolType = vetraTool
ttTELL1PedestalSubtractorCentral.InputDataLoc = '/Event/Raw/TT/Full'
ttTELL1PedestalSubtractorCentral.OutputDataLoc = '/Event/Raw/TT/PedSubADCs'
ttTELL1PedestalSubtractorCentral.SubPedsLoc = '/Event/Raw/TT/SubPeds'
ttTELL1PedestalSubtractorCentral.DetType = 'TT'

ttTELL1PedestalSubtractorNext1 = STTELL1PedestalSubtractor( 'TTTELL1PedestalSubtractorNext1' )
ttTELL1PedestalSubtractorNext1.CondPath = 'TTCondDB'
ttTELL1PedestalSubtractorNext1.VetraCfgToolType = vetraTool
ttTELL1PedestalSubtractorNext1.InputDataLoc = '/Event/Next1/Raw/TT/Full'
ttTELL1PedestalSubtractorNext1.OutputDataLoc = '/Event/Next1/Raw/TT/PedSubADCs'
ttTELL1PedestalSubtractorNext1.SubPedsLoc = '/Event/Next1/Raw/TT/SubPeds'
ttTELL1PedestalSubtractorNext1.DetType = 'TT'

ttTELL1PedestalSubtractorNext2 = STTELL1PedestalSubtractor( 'TTTELL1PedestalSubtractorNext2' )
ttTELL1PedestalSubtractorNext2.CondPath = 'TTCondDB'
ttTELL1PedestalSubtractorNext2.VetraCfgToolType = vetraTool
ttTELL1PedestalSubtractorNext2.InputDataLoc = '/Event/Next2/Raw/TT/Full'
ttTELL1PedestalSubtractorNext2.OutputDataLoc = '/Event/Next2/Raw/TT/PedSubADCs'
ttTELL1PedestalSubtractorNext2.SubPedsLoc = '/Event/Next2/Raw/TT/SubPeds'
ttTELL1PedestalSubtractorNext2.DetType = 'TT'

ttTELL1LCMSPrev2 = STTELL1LCMS( 'TTTELL1LCMSPrev2' )
ttTELL1LCMSPrev2.CondPath = 'TTCondDB'
ttTELL1LCMSPrev2.VetraCfgToolType = vetraTool
ttTELL1LCMSPrev2.InputDataLoc = '/Event/Prev2/Raw/TT/PedSubADCs'
ttTELL1LCMSPrev2.OutputDataLoc = '/Event/Prev2/Raw/TT/LCMSADCs' 
ttTELL1LCMSPrev2.DetType = 'TT' 

ttTELL1LCMSPrev1 = STTELL1LCMS( 'TTTELL1LCMSPrev1' )
ttTELL1LCMSPrev1.CondPath = 'TTCondDB'
ttTELL1LCMSPrev1.VetraCfgToolType = vetraTool
ttTELL1LCMSPrev1.InputDataLoc = '/Event/Prev1/Raw/TT/PedSubADCs'
ttTELL1LCMSPrev1.OutputDataLoc = '/Event/Prev1/Raw/TT/LCMSADCs' 
ttTELL1LCMSPrev1.DetType = 'TT' 

ttTELL1LCMSCentral = STTELL1LCMS( 'TTTELL1LCMSCentral' )
ttTELL1LCMSCentral.CondPath = 'TTCondDB'
ttTELL1LCMSCentral.VetraCfgToolType = vetraTool
ttTELL1LCMSCentral.InputDataLoc = '/Event/Raw/TT/PedSubADCs'
ttTELL1LCMSCentral.OutputDataLoc = '/Event/Raw/TT/LCMSADCs' 
ttTELL1LCMSCentral.DetType = 'TT' 

ttTELL1LCMSNext1 = STTELL1LCMS( 'TTTELL1LCMSNext1' )
ttTELL1LCMSNext1.CondPath = 'TTCondDB'
ttTELL1LCMSNext1.VetraCfgToolType = vetraTool
ttTELL1LCMSNext1.InputDataLoc = '/Event/Next1/Raw/TT/PedSubADCs'
ttTELL1LCMSNext1.OutputDataLoc = '/Event/Next1/Raw/TT/LCMSADCs' 
ttTELL1LCMSNext1.DetType = 'TT' 

ttTELL1LCMSNext2 = STTELL1LCMS( 'TTTELL1LCMSNext2' )
ttTELL1LCMSNext2.CondPath = 'TTCondDB'
ttTELL1LCMSNext2.VetraCfgToolType = vetraTool
ttTELL1LCMSNext2.InputDataLoc = '/Event/Next2/Raw/TT/PedSubADCs'
ttTELL1LCMSNext2.OutputDataLoc = '/Event/Next2/Raw/TT/LCMSADCs' 
ttTELL1LCMSNext2.DetType = 'TT' 

emuCentralSeq = GaudiSequencer( 'EmuTTCentralSeq' )
emuCentralSeq.MeasureTime = True
ApplicationMgr().TopAlg.append( emuCentralSeq )
emuCentralSeq.Members.append(ttCheckNZSCentral)
emuCentralSeq.Members.append(ttTELL1PedestalSubtractorCentral)
emuCentralSeq.Members.append(ttTELL1LCMSCentral)

emuPrev2Seq = GaudiSequencer( 'EmuTTPrev2Seq' )
emuPrev2Seq.MeasureTime = True
ApplicationMgr().TopAlg.append( emuPrev2Seq )
emuPrev2Seq.Members.append(ttCheckNZSPrev2)
emuPrev2Seq.Members.append(ttTELL1PedestalSubtractorPrev2)
emuPrev2Seq.Members.append(ttTELL1LCMSPrev2)

emuPrev1Seq = GaudiSequencer( 'EmuTTPrev1Seq' )
emuPrev1Seq.MeasureTime = True
ApplicationMgr().TopAlg.append( emuPrev1Seq )
emuPrev1Seq.Members.append(ttCheckNZSPrev1)
emuPrev1Seq.Members.append(ttTELL1PedestalSubtractorPrev1)
emuPrev1Seq.Members.append(ttTELL1LCMSPrev1)

emuNext1Seq = GaudiSequencer( 'EmuTTNext1Seq' )
emuNext1Seq.MeasureTime = True
ApplicationMgr().TopAlg.append( emuNext1Seq )
emuNext1Seq.Members.append(ttCheckNZSNext1)
emuNext1Seq.Members.append(ttTELL1PedestalSubtractorNext1)
emuNext1Seq.Members.append(ttTELL1LCMSNext1)

emuNext2Seq = GaudiSequencer( 'EmuTTNext2Seq' )
emuNext2Seq.MeasureTime = True
ApplicationMgr().TopAlg.append( emuNext2Seq )
emuNext2Seq.Members.append(ttCheckNZSNext2)
emuNext2Seq.Members.append(ttTELL1PedestalSubtractorNext2)
emuNext2Seq.Members.append(ttTELL1LCMSNext2)
