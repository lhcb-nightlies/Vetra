from Gaudi.Configuration import *

from Configurables import STVetraCfg
vetraTool = STVetraCfg('ITVetraCfgTool')
vetraTool.DetType = 'IT'
vetraTool.CondPath = 'ITCondDB'

from Configurables import (  STTELL1CheckNzs, STFullDecoding, STTELL1PedestalSubtractor, STTELL1LCMS, STTELL1ClusterMaker )

itCheckNZS = STTELL1CheckNzs( 'ITCheckNZS' )
itCheckNZS.DetType = 'IT'
itCheckNZS.VetraCfgToolType = vetraTool
itCheckNZS.InputLocation =  '/Event/DAQ/RawEvent'

itFullDecoding = STFullDecoding("ITFullDecoding")
itFullDecoding.PrintErrorInfo   = 0;
itFullDecoding.DetType          = "IT";
itFullDecoding.OutputLevel      = 3;
itFullDecoding.InputLocation    = "/Event/DAQ/RawEvent";
itFullDecoding.OutputLocation    = "/Event/Raw/IT/Full";
itFullDecoding.EventInfoLocation = "/Event/Raw/IT/EventInfo";

itTELL1PedestalSubtractor = STTELL1PedestalSubtractor( 'ITTELL1PedestalSubtractor' )
itTELL1PedestalSubtractor.CondPath = 'ITCondDB'
itTELL1PedestalSubtractor.VetraCfgToolType = vetraTool
itTELL1PedestalSubtractor.InputDataLoc = '/Event/Raw/IT/Full'
itTELL1PedestalSubtractor.OutputDataLoc = '/Event/Raw/IT/PedSubADCs'
itTELL1PedestalSubtractor.SubPedsLoc = '/Event/Raw/IT/SubPeds'
itTELL1PedestalSubtractor.DetType = 'IT'
itTELL1PedestalSubtractor.UseTELL1PedestalBank = False

itTELL1LCMS = STTELL1LCMS( 'ITTELL1LCMS' )
itTELL1LCMS.CondPath = 'ITCondDB'
itTELL1LCMS.VetraCfgToolType = vetraTool
itTELL1LCMS.InputDataLoc = '/Event/Raw/IT/PedSubADCs'
itTELL1LCMS.OutputDataLoc = '/Event/Raw/IT/LCMSADCs' 
itTELL1LCMS.DetType = 'IT' 

itEmuSeq = GaudiSequencer( 'EmuITSeq' )
itEmuSeq.MeasureTime = True
#ApplicationMgr().TopAlg.append( itEmuSeq )
itEmuSeq.Members.append(itCheckNZS)
itEmuSeq.Members.append(itFullDecoding)
itEmuSeq.Members.append(itTELL1PedestalSubtractor)
itEmuSeq.Members.append(itTELL1LCMS)

