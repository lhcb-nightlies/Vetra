# Emulation sequence for different spills in TAE mode.
# Each spill runs as a different sequence so that it can be aborted if there is no NZS bank in that spill.
# Then run pedestal subtraction and the LCMS subtraction

from Gaudi.Configuration import *

from Configurables import STVetraCfg
vetraTool = STVetraCfg('ITVetraCfgTool')
vetraTool.DetType = 'IT'
vetraTool.CondPath = 'ITCondDB'

from Configurables import (  STTELL1CheckNzs, STFullDecoding, STTELL1PedestalSubtractor, STTELL1LCMS, STTELL1ClusterMaker )

itCheckNZSCentral = STTELL1CheckNzs( 'ITCheckNZSCentral' )
itCheckNZSCentral.DetType = 'IT'
itCheckNZSCentral.VetraCfgToolType = vetraTool
itCheckNZSCentral.InputLocation =  '/Event/DAQ/RawEvent'

itCheckNZSPrev2 = STTELL1CheckNzs( 'ITCheckNZSPrev2' )
itCheckNZSPrev2.DetType = 'IT'
itCheckNZSPrev2.VetraCfgToolType = vetraTool
itCheckNZSPrev2.InputLocation =  '/Event/Prev2/DAQ/RawEvent'

itCheckNZSPrev1 = STTELL1CheckNzs( 'ITCheckNZSPrev1' )
itCheckNZSPrev1.DetType = 'IT'
itCheckNZSPrev1.VetraCfgToolType = vetraTool
itCheckNZSPrev1.InputLocation =  '/Event/Prev1/DAQ/RawEvent'

itCheckNZSNext1 = STTELL1CheckNzs( 'ITCheckNZSNext1' )
itCheckNZSNext1.DetType = 'IT'
itCheckNZSNext1.VetraCfgToolType = vetraTool
itCheckNZSNext1.InputLocation =  '/Event/Next1/DAQ/RawEvent'

itCheckNZSNext2 = STTELL1CheckNzs( 'ITCheckNZSNext2' )
itCheckNZSNext2.DetType = 'IT'
itCheckNZSNext2.VetraCfgToolType = vetraTool
itCheckNZSNext2.InputLocation =  '/Event/Next2/DAQ/RawEvent'

itTELL1PedestalSubtractorPrev2 = STTELL1PedestalSubtractor( 'ITTELL1PedestalSubtractorPrev2' )
itTELL1PedestalSubtractorPrev2.CondPath = 'ITCondDB'
itTELL1PedestalSubtractorPrev2.VetraCfgToolType = vetraTool
itTELL1PedestalSubtractorPrev2.InputDataLoc = '/Event/Prev2/Raw/IT/Full'
itTELL1PedestalSubtractorPrev2.OutputDataLoc = '/Event/Prev2/Raw/IT/PedSubADCs'
itTELL1PedestalSubtractorPrev2.SubPedsLoc = '/Event/Prev2/Raw/IT/SubPeds'
itTELL1PedestalSubtractorPrev2.DetType = 'IT'

itTELL1PedestalSubtractorPrev1 = STTELL1PedestalSubtractor( 'ITTELL1PedestalSubtractorPrev1' )
itTELL1PedestalSubtractorPrev1.CondPath = 'ITCondDB'
itTELL1PedestalSubtractorPrev1.VetraCfgToolType = vetraTool
itTELL1PedestalSubtractorPrev1.InputDataLoc = '/Event/Prev1/Raw/IT/Full'
itTELL1PedestalSubtractorPrev1.OutputDataLoc = '/Event/Prev1/Raw/IT/PedSubADCs'
itTELL1PedestalSubtractorPrev1.SubPedsLoc = '/Event/Prev1/Raw/IT/SubPeds'
itTELL1PedestalSubtractorPrev1.DetType = 'IT'

itTELL1PedestalSubtractorCentral = STTELL1PedestalSubtractor( 'ITTELL1PedestalSubtractorCentral' )
itTELL1PedestalSubtractorCentral.CondPath = 'ITCondDB'
itTELL1PedestalSubtractorCentral.VetraCfgToolType = vetraTool
itTELL1PedestalSubtractorCentral.InputDataLoc = '/Event/Raw/IT/Full'
itTELL1PedestalSubtractorCentral.OutputDataLoc = '/Event/Raw/IT/PedSubADCs'
itTELL1PedestalSubtractorCentral.SubPedsLoc = '/Event/Raw/IT/SubPeds'
itTELL1PedestalSubtractorCentral.DetType = 'IT'

itTELL1PedestalSubtractorNext1 = STTELL1PedestalSubtractor( 'ITTELL1PedestalSubtractorNext1' )
itTELL1PedestalSubtractorNext1.CondPath = 'ITCondDB'
itTELL1PedestalSubtractorNext1.VetraCfgToolType = vetraTool
itTELL1PedestalSubtractorNext1.InputDataLoc = '/Event/Next1/Raw/IT/Full'
itTELL1PedestalSubtractorNext1.OutputDataLoc = '/Event/Next1/Raw/IT/PedSubADCs'
itTELL1PedestalSubtractorNext1.SubPedsLoc = '/Event/Next1/Raw/IT/SubPeds'
itTELL1PedestalSubtractorNext1.DetType = 'IT'

itTELL1PedestalSubtractorNext2 = STTELL1PedestalSubtractor( 'ITTELL1PedestalSubtractorNext2' )
itTELL1PedestalSubtractorNext2.CondPath = 'ITCondDB'
itTELL1PedestalSubtractorNext2.VetraCfgToolType = vetraTool
itTELL1PedestalSubtractorNext2.InputDataLoc = '/Event/Next2/Raw/IT/Full'
itTELL1PedestalSubtractorNext2.OutputDataLoc = '/Event/Next2/Raw/IT/PedSubADCs'
itTELL1PedestalSubtractorNext2.SubPedsLoc = '/Event/Next2/Raw/IT/SubPeds'
itTELL1PedestalSubtractorNext2.DetType = 'IT'

itTELL1LCMSPrev2 = STTELL1LCMS( 'ITTELL1LCMSPrev2' )
itTELL1LCMSPrev2.CondPath = 'ITCondDB'
itTELL1LCMSPrev2.VetraCfgToolType = vetraTool
itTELL1LCMSPrev2.InputDataLoc = '/Event/Prev2/Raw/IT/PedSubADCs'
itTELL1LCMSPrev2.OutputDataLoc = '/Event/Prev2/Raw/IT/LCMSADCs' 
itTELL1LCMSPrev2.DetType = 'IT' 

itTELL1LCMSPrev1 = STTELL1LCMS( 'ITTELL1LCMSPrev1' )
itTELL1LCMSPrev1.CondPath = 'ITCondDB'
itTELL1LCMSPrev1.VetraCfgToolType = vetraTool
itTELL1LCMSPrev1.InputDataLoc = '/Event/Prev1/Raw/IT/PedSubADCs'
itTELL1LCMSPrev1.OutputDataLoc = '/Event/Prev1/Raw/IT/LCMSADCs' 
itTELL1LCMSPrev1.DetType = 'IT' 

itTELL1LCMSCentral = STTELL1LCMS( 'ITTELL1LCMSCentral' )
itTELL1LCMSCentral.CondPath = 'ITCondDB'
itTELL1LCMSCentral.VetraCfgToolType = vetraTool
itTELL1LCMSCentral.InputDataLoc = '/Event/Raw/IT/PedSubADCs'
itTELL1LCMSCentral.OutputDataLoc = '/Event/Raw/IT/LCMSADCs' 
itTELL1LCMSCentral.DetType = 'IT' 

itTELL1LCMSNext1 = STTELL1LCMS( 'ITTELL1LCMSNext1' )
itTELL1LCMSNext1.CondPath = 'ITCondDB'
itTELL1LCMSNext1.VetraCfgToolType = vetraTool
itTELL1LCMSNext1.InputDataLoc = '/Event/Next1/Raw/IT/PedSubADCs'
itTELL1LCMSNext1.OutputDataLoc = '/Event/Next1/Raw/IT/LCMSADCs' 
itTELL1LCMSNext1.DetType = 'IT' 

itTELL1LCMSNext2 = STTELL1LCMS( 'ITTELL1LCMSNext2' )
itTELL1LCMSNext2.CondPath = 'ITCondDB'
itTELL1LCMSNext2.VetraCfgToolType = vetraTool
itTELL1LCMSNext2.InputDataLoc = '/Event/Next2/Raw/IT/PedSubADCs'
itTELL1LCMSNext2.OutputDataLoc = '/Event/Next2/Raw/IT/LCMSADCs' 
itTELL1LCMSNext2.DetType = 'IT' 

emuCentralSeq = GaudiSequencer( 'EmuITCentralSeq' )
emuCentralSeq.MeasureTime = True
ApplicationMgr().TopAlg.append( emuCentralSeq )
emuCentralSeq.Members.append(itCheckNZSCentral)
emuCentralSeq.Members.append(itTELL1PedestalSubtractorCentral)
emuCentralSeq.Members.append(itTELL1LCMSCentral)

emuPrev2Seq = GaudiSequencer( 'EmuITPrev2Seq' )
emuPrev2Seq.MeasureTime = True
ApplicationMgr().TopAlg.append( emuPrev2Seq )
emuPrev2Seq.Members.append(itCheckNZSPrev2)
emuPrev2Seq.Members.append(itTELL1PedestalSubtractorPrev2)
emuPrev2Seq.Members.append(itTELL1LCMSPrev2)

emuPrev1Seq = GaudiSequencer( 'EmuITPrev1Seq' )
emuPrev1Seq.MeasureTime = True
ApplicationMgr().TopAlg.append( emuPrev1Seq )
emuPrev1Seq.Members.append(itCheckNZSPrev1)
emuPrev1Seq.Members.append(itTELL1PedestalSubtractorPrev1)
emuPrev1Seq.Members.append(itTELL1LCMSPrev1)

emuNext1Seq = GaudiSequencer( 'EmuITNext1Seq' )
emuNext1Seq.MeasureTime = True
ApplicationMgr().TopAlg.append( emuNext1Seq )
emuNext1Seq.Members.append(itCheckNZSNext1)
emuNext1Seq.Members.append(itTELL1PedestalSubtractorNext1)
emuNext1Seq.Members.append(itTELL1LCMSNext1)

emuNext2Seq = GaudiSequencer( 'EmuITNext2Seq' )
emuNext2Seq.MeasureTime = True
ApplicationMgr().TopAlg.append( emuNext2Seq )
emuNext2Seq.Members.append(itCheckNZSNext2)
emuNext2Seq.Members.append(itTELL1PedestalSubtractorNext2)
emuNext2Seq.Members.append(itTELL1LCMSNext2)
