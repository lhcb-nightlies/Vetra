import os

dettype = str(os.environ.get("MYDETTYPE"))
lhcbcondsliceloc = str(os.environ.get("MYLHCBCONDSLICELOC"))
date = str(os.environ.get("MYCALIBNAME"))
run = str(os.environ.get("MYCALIBRUNNB"))

vec = lhcbcondsliceloc.split('/')
outputfile = "/group/st/sw/Tell1Config_Calib/DB/BadStripLists/"+vec[len(vec)-1]+".txt"

import GaudiPython
from Gaudi.Configuration import *
gbl = GaudiPython.gbl

# decoding version
vers = 3
   
# set up the DDDB and include any slices you need to get the real head !!!!!
from Configurables import CondDB
from Configurables import ( CondDB, CondDBAccessSvc )
mycalib = CondDBAccessSvc('myCalib')
mycalib.ConnectionString = 'sqlite_file:'+lhcbcondsliceloc+'.db/LHCBCOND'
CondDB().addLayer( mycalib )

from Configurables import LHCbApp, WriteSTStatusConditions, WriteSTNoiseConditions
lhcbApp = LHCbApp()
lhcbApp.DDDBtag   = "Default"
lhcbApp.CondDBtag = "Default"
ApplicationMgr( OutputLevel = INFO, AppName = 'BadStripsInLHCBCOND')

# get the detector elements
appMgr = GaudiPython.AppMgr()
det    = appMgr.detSvc()
IT   = det['/dd/Structure/LHCb/AfterMagnetRegion/T/IT']
TT   = det['/dd/Structure/LHCb/BeforeMagnetRegion/TT']

# convert the online channel to the offline one
readoutTool = appMgr.toolsvc().create(dettype+'ReadoutTool', interface = 'ISTReadoutTool')

if dettype == 'TT':
    sectors = TT.sectors()
else:
    sectors = IT.sectors()

MaskFromDB = [ "OK",
               "Open",
               "Short",
               "Pinhole",
               "ReadoutProblems",
               "NotBonded",
               "LowGain",
               "Noisy",
               "-",
               "OtherFault",
               "Dead"]

#---Find sectors

print ""
print ""
print "#############################################################"
print "########## Looking for Sectors ##############################"
print "#############################################################"

badSectorList = []

for sector in sectors:
    if(sector.sectorStatus() != 0):
        badSectorList.append(sector.nickname())

print ""
print "----- List of bad sectors -----"

for i in range(0,badSectorList.__len__()):
    print badSectorList[i]

#---Find beetles

print ""
print ""
print "#############################################################"
print "########## Looking for Beetles/Links ########################"
print "#############################################################"

badBeetleList = {}
sectorsWithBadBeetlesList = {}

for sector in sectors:
    if(sector.sectorStatus() == 0):
        beetleDone = -1
        for strip in range(1,sector.nStrip()+1):
            if(sector.beetleStatus(sector.stripToChan(strip)) != 0 and sector.beetle(sector.stripToChan(strip)) != beetleDone):
                beetleDone = sector.beetle(sector.stripToChan(strip))
                badBeetleList[readoutTool.SourceIDToTELLNumber(readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).first.id())*10000
                              +readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).second] = sector.beetleStatus(sector.stripToChan(strip))
                sectorsWithBadBeetlesList[readoutTool.SourceIDToTELLNumber(readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).first.id())*10000
                                         +readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).second] = sector.nickname()

print ""
print "----- List of bad beetles (TELL1 link sector) -----"

beetleTxtFile = open(dettype+"allBadBeetles-r"+run+"-"+date+".txt",'w')

for i in sorted(badBeetleList.iterkeys()):
    print i/10000, (i%10000)/128, sectorsWithBadBeetlesList[i]
    beetleTxtFile.write( str(i/10000)+" "+str((i%10000)/128)+" "+str(badBeetleList[i])+"\n" )

beetleTxtFile.close()

#---Find ports

print ""
print ""
print "#############################################################"
print "########## Looking for Ports ################################"
print "#############################################################"

badPortList = {}
sectorsWithBadPortsList = {}

for sector in sectors:
    if(sector.sectorStatus() == 0):
        stripCounter = 0
        for strip in range(1,sector.nStrip()+1):
            if(sector.stripStatus(sector.stripToChan(strip)) == 4):
                stripCounter += 1
            else:
                stripCounter = 0
            if stripCounter == 32 and strip%32 == 0:
                stripCounter = 0
                badPortList[readoutTool.SourceIDToTELLNumber(readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).first.id())*10000
                            +readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).second] = sector.beetleStatus(sector.stripToChan(strip))
                sectorsWithBadPortsList[readoutTool.SourceIDToTELLNumber(readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).first.id())*10000
                                        +readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).second] = sector.nickname()

print ""
print "----- List of bad ports (TELL1 link port sector) -----"

portsTxtFile = open(dettype+"allBadPorts-r"+run+"-"+date+".txt",'w')

import math
for i in sorted(badPortList.iterkeys()):
    print i/10000, math.trunc((i%10000)/128), ((i%10000)/32)%4,  sectorsWithBadPortsList[i]
    portsTxtFile.write(str(i/10000)+" "+str(math.trunc((i%10000)/128))+" "+str(((i%10000)/32)%4)+" 4\n")

portsTxtFile.close()

#---Finds strips

print ""
print ""
print "#############################################################"
print "########## Looking for Strips ###############################"
print "#############################################################"

badStripList = {}
sectorsWithBadStripsList = {}
stripStatusCounter = [0,0,0,0,0,0,0,0,0,0,0]
counter = 0
totalcounter = 0

for sector in sectors:
    if(sector.sectorStatus() == 0):
        for strip in range(1,sector.nStrip()+1):
            if(sector.beetleStatus(sector.stripToChan(strip)) != 0):
                stripStatusCounter[sector.beetleStatus(sector.stripToChan(strip))] += 1
                badStripList[readoutTool.SourceIDToTELLNumber(readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).first.id())*10000
                             +readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).second] = sector.beetleStatus(sector.stripToChan(strip))
                sectorsWithBadStripsList[readoutTool.SourceIDToTELLNumber(readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).first.id())*10000
                                         +readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).second] = sector.nickname()
                counter += 1
                totalcounter += 1
            else:
                stripStatusCounter[sector.stripStatus(sector.stripToChan(strip))] += 1
                if sector.stripStatus(sector.stripToChan(strip)) != 0 and sector.stripStatus(sector.stripToChan(strip)) != 3:
                    badStripList[readoutTool.SourceIDToTELLNumber(readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).first.id())*10000
                                 +readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).second] = sector.stripStatus(sector.stripToChan(strip))
                    sectorsWithBadStripsList[readoutTool.SourceIDToTELLNumber(readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).first.id())*10000
                                             +readoutTool.offlineChanToDAQ(sector.stripToChan(strip),0).second] = sector.nickname()
                    counter+= 1
                totalcounter += 1    
    else:
        for strip in range(1,sector.nStrip()+1):
            stripStatusCounter[sector.sectorStatus()] += 1
            counter+= 1
            totalcounter += 1

print ""
print "----- List of bad strips (TELL1 strip status sector) -----"

stripsTxtFile = open(dettype+"allBadStrips-r"+run+"-"+date+".txt",'w')

for i in sorted(badStripList.iterkeys()):
    print i/10000, i%10000, badStripList[i], sectorsWithBadStripsList[i]
    stripsTxtFile.write(str(i/10000)+" "+str(i%10000)+" "+str(badStripList[i])+"\n")

stripsTxtFile.close()

print ""
print "----- Number of strips with given strip status -----"

for i in range(0,len(stripStatusCounter)):
    print stripStatusCounter[i], "(",i,",",MaskFromDB[i],")"

print ""
print "----- Number of masked strips -----"    

from decimal import *
getcontext().prec = 2
print counter, "over", totalcounter, "channels", "(", Decimal(counter)/Decimal(totalcounter)*Decimal(100), "% )"

print ""

file = open(outputfile, "w")
for i in sorted(badStripList.iterkeys()):
    file.write(str(i/10000)+" "+str(i%10000)+"\n")
file.close()    
    
appMgr.exit()
