#---------------------------------------------------------------------------------
#
#  BASH script that gives bad strips from LHCBCOND
#
#  Author : Frederic Dupertuis
#  Modified: Vincenzo Battista
#---------------------------------------------------------------------------------

# Detector to be calibrated
export MYDETTYPE="TT"

# LHCBCOND tag
export MYLHCBCONDTAG="HEAD"
# Input calibration name, run number
# > if let to "" latest applied calibration name is used
export MYCALIBNAME="MaskedLink20161127"
export MYCALIBRUNNB="186440"
#---------------------------------------------------------------------------------

export DefEnvVar="DefEnvVar.sh"
if [[ ${MYCALIBNAME} == "" ]]||[[ ${MYCALIBRUNNB} == "" ]]
then
    ( source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb` 
	echo export MYCALIBNAME=`python $MYRESULTS/STCONDDB_interface.py $MYDETTYPE calibname` > "$DefEnvVar"
	echo export MYCALIBRUNNB=`python $MYRESULTS/STCONDDB_interface.py $MYDETTYPE calibrun` >> "$DefEnvVar" )
    source $DefEnvVar
    export MYLHCBCONDSLICELOC="$MYDB/LHCBCOND-"${MYDETTYPE}"-"${MYCALIBNAME}"-r"${MYCALIBRUNNB}
else
    export MYLHCBCONDSLICELOC="$MYDB/LHCBCOND-"${MYDETTYPE}"-"${MYCALIBNAME}"-r"${MYCALIBRUNNB}
fi

echo "LHCBCOND database:"
echo "$MYLHCBCONDSLICELOC"

if [[ !(-a ${MYLHCBCONDSLICELOC}.db) ]]
then 
    ( source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`	
	copy_files_to_db.py -c sqlite_file:${MYLHCBCONDSLICELOC}.db/LHCBCOND -s ${MYLHCBCONDSLICELOC} )
fi

( source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra` 
    python stripStatus_LHCBCOND.py | tee stripStatus_LHCBCOND_${MYDETTYPE}.log )

rm -f ${MYLHCBCONDSLICELOC}.db
rm -f $DefEnvVar