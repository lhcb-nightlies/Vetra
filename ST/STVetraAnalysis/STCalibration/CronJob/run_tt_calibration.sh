#!/bin/bash --login
#################################################
#
# run_vetra_analysis.sh
#
# @author: Marco Gersabeck
# @date: 2010/08/25
#
# Bash script to analyse runs with vetraOffline
# which have note yet been analysed
#
#################################################

echo "host1" $HOSTNAME
export HOSTNAME;echo "import os;print os.environ['HOSTNAME']" | /sw/lib/lcg/external/Python/2.6.5/x86_64-slc5-gcc43-opt/bin/python
echo "host2" $HOSTNAME

# get latest run in DQS directory
latest_run=`ls -rt $HOME/Tell1Config_Calib/Calib/data/TT_analysed/ |tail --lines=1`
echo "Latest analysed run:"
echo ${latest_run}

# define run range to check
incr=1     # this should be 1
incrmore=10000 # this should be >1

# finish definition of run range to check
target_run=$(($latest_run + $incrmore))
latest_run=$(($latest_run + $incr))
echo "Range of runs to check:"
echo ${latest_run} ${target_run}

# get list of run in run range from rdbt which are in the bookkeeping
#runs=`/group/online/scripts/rdbt -p LHCb -n "$latest_run" "$target_run" | grep -B2 -A2 "IN BKK" | grep -B1 CASTOR | grep run | grep -v runType | awk '{print $2}'`
runs=`/group/online/scripts/rdbt -p LHCb -n "$latest_run" "$target_run" | grep -B1 CASTOR | grep -B1 EOF_CALIB16 | grep run | grep -v runType | awk '{print $2}'`
echo "Found runs:" $runs

# loop over list of candidate runs and analyse the first one found
ranrun=false
for run in $runs
do
  # check for a lock file and create one if there is none
  echo "trying" $run
  echo "Local switch status:" $local
  lock_file="$HOME/Tell1Config_Calib/Calib/data/TT_"$run"_NZS.lock"
  touch_file="$HOME/Tell1Config_Calib/Calib/data/TT_analysed/"$run
  dothis=true

  nbfiles=`/group/online/scripts/rdbt -p LHCb -n "$run" | grep exist | awk '{print $3}'`
  minfiles=5

  if [ "$nbfiles" -lt "$minfiles" ]; then 
    echo "Too few files in run" $run
    dothis=false 
  elif [ -f $lock_file ]; then
    echo "Found lock file for run" $run
    dothis=false
  elif [ -f $touch_file ]; then
    echo "Already a calibration for run" $run
    dothis=false
  else
    touch $lock_file 
    echo "Creating lock file for run" $run
    echo "Creating" $lock_file
  fi
  
  if [ "$dothis" = "true" ] && [ "$run" ] && [ "$run" -gt 0 ]; then
      echo "Ready to go"
  else
      echo "Not ready to go"
  fi

  useCastorData=false
  place=""
  if [ "$dothis" = "true" ] && [ "$run" ] && [ "$run" -gt 0 ]; then
      data_type=`python $MYCALIB/data/GetDataTypeFromRunNumber.py $run`
      echo "Data Type: "$data_type
      run_dir="/daqarea/lhcb/data/2016/RAW/CALIB/LHCb/${data_type}/"$run
      if [ -d $run_dir ]; then
	  echo "Found corresponding local directory; taking data from there"
	  place="CALIBLOCAL"
	  useCastorData=false
      else
	  echo "No local run directory with name" $run_dir
	  run_dir="/castorfs/cern.ch/grid/lhcb/data/2016/RAW/CALIB/LHCb/${data_type}/"$run
	  echo "Now trying with " $run_dir
	  echo "Check carefully the logfile: a crash may happen!"
	  place="CALIBCASTOR"
	  useCastorData=true
      fi
  else
      echo "Nothing to do."
      if [ -f $lock_file ]; then
	  rm $lock_file
	  echo "Deleted" $lock_file
      fi
      dothis=false
  fi
  
  # perform the analysis
  if [ "$dothis" = "true" ] && [ "$run" ] && [ "$run" -gt 0 ]; then
      echo "Use Castor Data: "$useCastorData
      echo "Running Calibration on run" $run
      cd $HOME/Tell1Config_Calib/Calib
      export time_tmp=`/group/online/scripts/rdbt -p LHCb -n $run |grep startTime|awk '{print $2}'`
      export time=`perl -e '$new=$ENV{'time_tmp'}; $new =~ s/-//g; print "$new\n";'`
      echo "Time: "$time
      if [ "$useCastorData" = "true" ] ; then
	  echo "Starting calibration (CALIBCASTOR)..."
	  source launch_calib.sh TT $time $run CALIBCASTOR >& /group/st/sw/Tell1Config_Calib/Cron/logs/TT_${time}_${run}
      elif [ "$useCastorData" = "false" ] ; then
	  echo "Starting calibration (CALIBLOCAL)..."
	  source launch_calib.sh TT $time $run CALIBLOCAL >& /group/st/sw/Tell1Config_Calib/Cron/logs/TT_${time}_${run}
      else
	  echo "No calibration launched"
      fi
    # possibly check success of analysis here
      ranrun=true
      touch $HOME/Tell1Config_Calib/Calib/data/TT_analysed/${run}
  else
      echo "nothing to do for run" $run  
  fi
  
  # delete the lock file if it was created earlier
  if [ "$dothis" = "true" ] && [ -f $lock_file ]; then
      rm $lock_file
      echo "Deleted" $lock_file
      (echo "to:vibattis@cern.ch"
	  echo "subject:New TT Calibration based on Run ${run}, Time ${time}, Data from ${place}"
	  echo "mime-version: 1.0"
	  echo "content-type: multipart/related; boundary=frontier"
	  echo
	  echo "--frontier"
	  echo "content-type: text/plain"
	  echo
	  echo "This e-mail has been sent automatically by st_user"
	  echo
	  echo "Data taken from ${run_dir}"
	  echo
	  echo "******** LowNoiseFinder Results ********"
	  echo "Tell1 Number | Channel"
	  echo
	  cat /group/st/sw/Tell1Config_Calib/DB/BadStripLists/LowNoiseFinder-${time}-r${run}-TT.txt
	  echo
	  echo "--frontier"
	  echo "content-type: application/x-tar; name=${time}-r${run}-TT.tar"
	  echo "content-transfer-encoding: base64"
	  echo
	  openssl base64 < /group/st/sw/Tell1Config_Calib/Results/Plots/${time}-r${run}-TT.tar
	  echo "--frontier"
	  echo "content-type: application/x-tar; name=LowNoiseFinder-${time}-r${run}-TT.tar"
	  echo "content-transfer-encoding: base64"
	  echo
	  openssl base64 < /group/st/sw/Tell1Config_Calib/Results/LowNoiseFinder/Plots/LowNoiseFinder-${time}-r${run}-TT.tar
	  echo "--frontier"
	  echo "content-type: text/plain; name=TT_${time}_${run}.txt"
	  echo "content-transfer-encoding: base64"
	  echo
	  openssl base64 < /group/st/sw/Tell1Config_Calib/Cron/logs/TT_${time}_${run}) | sendmail -t
      rm -f /group/st/sw/Tell1Config_Calib/Cron/logs/TT_${time}_${run}_header
  fi
  
  # only analyse one run at a time to avoid endless running (could remove this)
  if [ "$ranrun" = "true" ]; then
      break
  fi
done
