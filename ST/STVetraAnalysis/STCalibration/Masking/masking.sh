#---------------------------------------------------------------------------------
#
#  BASH script to mask Beetles, Ports and Strips
#
#  Author : Frederic Dupertuis
#  Modified : Vincenzo Battista
#
#---------------------------------------------------------------------------------

#--------------------------------------------------------------------------------- 
# Detector to be calibrated
export MYDETTYPE="TT"
#---------------------------------------------------------------------------------
# Day and Run used for the Calibration
export MYCALIBNAME="MaskedStrips20161201"
export MYCALIBRUNNB="186440"
#---------------------------------------------------------------------------------
# Mask List File Locations (Usually in $MYDB/MaskLists/)
# > Beetles:
# > Beetle file must contain a List of | Tell1 No | Link No | Status
# > Status:
# > 0 = OK (CAUTION: With status OK you need to relaunch the Calibration!)
# > 4 = ReadoutProblems
# > if let to "" no Beetle needs to be masked
#export MYMASKBEETLELISTFILE="$MYDB/MaskLists/TT-20161127-r186440.txt"
export MYMASKBEETLELISTFILE=""
# > Ports:
# > Port file must contain a List of | Tell1 No | Link No | Port No | Status
# > Status:
# > 0 = OK (CAUTION: With status OK you need to relaunch the Calibration!)
# > 4 = ReadoutProblems
# > if let to "" no Port needs to be masked
#export MYMASKPORTLISTFILE="$MYDB/MaskLists/TTallBadPorts20161114.txt"
export MYMASKPORTLISTFILE=""
# > Strips:
# > Strip file must contain a List of | Tell1 No | Strip No | Status
# > Status:
# > 0 = OK (CAUTION: With status OK you need to relaunch the Calibration!)
# > 1 = Open
# > if let to "" no Strip needs to be masked
export MYMASKSTRIPLISTFILE="$MYDB/MaskLists/TT-20161201-r186440.txt"
#export MYMASKSTRIPLISTFILE=""
#---------------------------------------------------------------------------------
# LHCBCOND tag
export MYLHCBCONDTAG="HEAD"
# Default VetraCondDB location
export MYDEFAULTCONDDBNAME="/group/st/condxml/DBASE/STCOND.db"
# Input calibration name, run number
# > if let to "" latest applied calibration name is used
export MYSTARTCALIBNAME=""
export MYSTARTCALIBRUNNB=""
# Number of NZS to run over (just needed for Vetra code compatibility)
export MYNZSEVENTS="20000"
#--------------------------------------------------------------------------------- 
# Do you want to overpast the strip status if already masked ? (1=True and 0=False)
export MYOVERPAST=1
# Do you want to use AjustXml.py ? (1=True and 0=False) 
ajust=0
# Do you want to update CCESCAN/CCESCAN_ST recipes as well?
ccescan=1
# Do you want to update the ROOT files ? (1=True and 0=False) 
update=1
# Do you want to create a debug folder? (1=True and 0=False)
debug=0
#--------------------------------------------------------------------------------- 
# Calibration Name (Ex : today date like 20110101 or 20110101-r84000 or 20110101.1) 
export MYFULLCALIBNAME="${MYCALIBNAME}-r${MYCALIBRUNNB}"
echo "Calibration name:"
echo "$MYFULLCALIBNAME"
# XML informations
export MYAUTHOR="V Battista"
# > XML Version (Ex : today date like 2011/01/01 or Data11-20110101)
export MYXMLVERSION="Data16-${MYCALIBNAME}"
# > XML Description (Ex : Based on run 84000)
export MYXMLDESCRIPTION="Masking of some links/ports/strips. Noise based on run ${MYCALIBRUNNB}. Charge calibration from 2010."
#---------------------------------------------------------------------------------

if [[ $debug == 1 ]]
then
    rm -rf Debug_${MYFULLCALIBNAME}
    mkdir Debug_${MYFULLCALIBNAME}
fi

if [[ ${MYMASKBEETLELISTFILE} == "" ]]
then
    export MYMASKBEETLELISTFILE="NONE"
fi

if [[ ${MYMASKPORTLISTFILE} == "" ]]
then
    export MYMASKPORTLISTFILE="NONE"
fi

if [[ ${MYMASKSTRIPLISTFILE} == "" ]]
then
    export MYMASKSTRIPLISTFILE="NONE"
fi

echo "Beetle file:"
echo "$MYMASKBEETLELISTFILE"
echo "Port file"
echo "$MYMASKPORTLISTFILE"
echo "Strips file"
echo "$MYMASKSTRIPLISTFILE"

# Location of starting CONDDB and LHCBCOND slices. If calib name and run are empty,
# retrieve them from "/group/st/condxml/DBASE/recipes.list" file (latest names used)
echo -e "\n\n\n\nStep 1: retrieving starting databases"
export DefEnvVar="DefEnvVar.sh"
if [[ ${MYSTARTCALIBNAME} == "" ]]||[[ ${MYSTARTCALIBRUNNB} == "" ]]
then
    (source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
	echo export MYSTARTCALIBNAME=`python $MYRESULTS/STCONDDB_interface.py $MYDETTYPE calibname` > "$DefEnvVar"
	echo export MYSTARTCALIBRUNNB=`python $MYRESULTS/STCONDDB_interface.py $MYDETTYPE calibrun` >> "$DefEnvVar")
    source $DefEnvVar
    export MYSTARTVETRACONDDBSLICELOC="$MYDB/DEFAULT_RECIPES/"${MYDETTYPE}"COND-"${MYSTARTCALIBNAME}"-r"${MYSTARTCALIBRUNNB}".xml"
    export MYSTARTLHCBCONDSLICELOC="$MYDB/LHCBCOND-"${MYDETTYPE}"-"${MYSTARTCALIBNAME}"-r"${MYSTARTCALIBRUNNB}
else
    export MYSTARTVETRACONDDBSLICELOC="$MYDB/DEFAULT_RECIPES/"${MYDETTYPE}"COND-"${MYSTARTCALIBNAME}"-r"${MYSTARTCALIBRUNNB}".xml"
    export MYSTARTLHCBCONDSLICELOC="$MYDB/LHCBCOND-"${MYDETTYPE}"-"${MYSTARTCALIBNAME}"-r"${MYSTARTCALIBRUNNB}
fi

echo "Starting CONDDB:"
echo "$MYSTARTVETRACONDDBSLICELOC"

echo "Starting LHCBCOND DB:"
echo "$MYSTARTLHCBCONDSLICELOC"

# ---Create CONDB and LHCBCOND db in a .db format (LHCb software requires them instead of .xml files)

# Create (temporary) .db database from starting CONDDB
# 1) Create the required "structure" by making the following directories:
#    /home/st_user/Tell1Config_Calib/DB/VetraCondDB/<det>COND-<date>-r<run>/CondDB/cond.xml ("current" calibration)
#    /home/st_user/Tell1Config_Calib/DB/VetraCondDB/<det>COND-<date>-r<run>/CondDB/TELL1Cond.xml ("starting" calibration)
# 2) Create the .db file:
#    /home/st_user/Tell1Config_Calib/DB/<det>COND-<date>-r<run>.db
echo -e "\n\n\n\nStep 2: putting CONDDB and LHCBCOND in a .db format"
export MYVETRACONDDBSLICELOC="$MYDB/${MYDETTYPE}COND-${MYFULLCALIBNAME}"
mkdir -p $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
tar -C $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/ -zxvf $MYDB/VetraCondDB/${MYDETTYPE}.tar.gz
cp ${MYSTARTVETRACONDDBSLICELOC} $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/CondDB/TELL1Cond.xml
rm -f ${MYVETRACONDDBSLICELOC}.db
(source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
    copy_files_to_db.py -c sqlite_file:${MYVETRACONDDBSLICELOC}.db/COND -s $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/)

# Create (temporary) database from starting LHCBCOND
# 1) Create the required "structure" by making the following directory:
#    /home/st_user/Tell1Config_Calib/DB/LHCBCOND-<det>-r<date>-r<run>/Conditions/* (all .xml files in the starting directory)
# 2) Create the .db file:
#    /home/st_user/Tell1Config_Calib/DB/LHCBCOND-<det>-<date>-r<run>.db
echo -e "\n\n\n\nStep 3: creating temporary database from LHCBCOND"
export MYLHCBCONDSLICELOC="$MYDB/LHCBCOND-${MYDETTYPE}-${MYFULLCALIBNAME}"
mkdir -p ${MYLHCBCONDSLICELOC}
cp -r ${MYSTARTLHCBCONDSLICELOC}/* ${MYLHCBCONDSLICELOC}/
rm -f ${MYLHCBCONDSLICELOC}.db
(source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
    copy_files_to_db.py -c sqlite_file:${MYLHCBCONDSLICELOC}.db/LHCBCOND -s ${MYLHCBCONDSLICELOC})

# Launch masking script
echo -e "\n\n\n\nStep 4: launching masking script"
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    python masking.py)

# Update LHCBCOND database
echo -e "\n\n\n\nStep 5: updating LHCBCOND database"
(source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
    echo 'updating LHCBCOND database', ${MYLHCBCONDSLICELOC}
    copy_files_to_db.py -c sqlite_file:${MYLHCBCONDSLICELOC}.db/LHCBCOND -s ${MYLHCBCONDSLICELOC})

# Update CONDDB database (from LHCBCOND) using the $STVETRAANALYSISROOT/python/UpdateVetraCondFromLHCBCOND.py script.
# The options are the following:
# -s: detector type (IT, TT)
# -c: LHCBCOND path (HEAD /home/st_user/Tell1Config_Calib/DB/LHCBCOND-<det>-<date>-r<run>.db)
# -v: CONDDB path (/home/st_user/Tell1Config_Calib/DB/<det>COND-<date>-r<run>.db)
# -f: output file (/home/st_user/Tell1Config_Calib/DB/DEFAULT_RECIPES/<det>COND-<date>-r<run>.xml)
echo -e "\n\n\n\nStep 6: updating CONDDB from LHCBCOND"
if [[ $debug == 1 ]]
then
    mkdir -p Debug_${MYFULLCALIBNAME}/Step6_beforeUpdating
    cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml Debug_${MYFULLCALIBNAME}/Step6_beforeUpdating
fi
echo "Options:"
echo "MYLHCBCONDTAG: ${MYLHCBCONDTAG}"
echo "MYLHCBCONDSLICELOC: ${MYLHCBCONDSLICELOC}.db"
echo "MYVETRACONDDBSLICELOC: ${MYVETRACONDDBSLICELOC}.db"
echo "Recipe: $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml)"
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    python $STVETRAANALYSISROOT/python/UpdateVetraCondFromLHCBCOND.py -s ${MYDETTYPE} -c ${MYLHCBCONDTAG} ${MYLHCBCONDSLICELOC}.db -v ${MYVETRACONDDBSLICELOC}.db -f $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml)
if [[ $debug == 1 ]]
then
    mkdir -p Debug_${MYFULLCALIBNAME}/Step6_afterUpdating
    cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml Debug_${MYFULLCALIBNAME}/Step6_afterUpdating
fi

# Ajust CONDDB xml if needed (/home/st_user/Tell1Config_Calib/DB/<det>COND-<date>-r<run>.xml)
if [[ $ajust == 1 ]]
then 
    echo -e "\n\n\n\nStep 7: adjusting CONDDB (xml file)"
    if [[ $debug == 1 ]]
    then
	mkdir -p Debug_${MYFULLCALIBNAME}/Step7_beforeAjust
	cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml Debug_${MYFULLCALIBNAME}/Step7_beforeAjust
    fi
    (source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
	python $STVETRAANALYSISROOT/python/AjustXml.py $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml)
    echo "CONDDB adjusted:"
    echo "$MYDB/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml"
    if [[ $debug == 1 ]]
    then
        mkdir -p Debug_${MYFULLCALIBNAME}/Step7_afterAjust
        cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml Debug_${MYFULLCALIBNAME}/Step7_afterAjust
    fi
else 
    echo -e "\n\n\n\nStep 7: doing some cosmetics on CONDDB (xml file)"
    if [[ $debug == 1 ]]
    then
        mkdir -p Debug_${MYFULLCALIBNAME}/Step7_beforeDBCosmetics
        cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml Debug_${MYFULLCALIBNAME}/Step7_beforeDBCosmetics
    fi
    (source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
	perl $STVETRAANALYSISROOT/perl/DBCosmetics.pl $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml)
    if [[ $debug == 1 ]]
    then
        mkdir -p Debug_${MYFULLCALIBNAME}/Step7_afterDBCosmetics
        cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml Debug_${MYFULLCALIBNAME}/Step7_afterDBCosmetics
    fi
fi

# Update CONDDB database (/home/st_user/Tell1Config_Calib/DB/<det>COND-<date>-r<run>.db) using
# the updated TELL1Cond.xml db
echo -e "\n\n\n\nStep 8: updating CONDDB"
cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/CondDB/TELL1Cond.xml
rm -f ${MYVETRACONDDBSLICELOC}.db
(source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
    copy_files_to_db.py -c sqlite_file:${MYVETRACONDDBSLICELOC}.db/COND -s $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/)

# Updating CCESCAN and CCESCAN_ST recipes starting from the DEFAULT one
if [[ $ccescan == 1 ]]
then
    echo -e "\n\n\n\nStep 9: creating CCESCAN and CCESCAN_ST recipes"
    cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml $MYDB/CCESCAN_RECIPES/
    cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml $MYDB/CCESCAN_ST_RECIPES/
    (source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
        python /home/st_user/Tell1Config_Calib/Tell1Config/AjustXML/AdjustXml-CCESCAN.py $MYDB/CCESCAN_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml
        python /home/st_user/Tell1Config_Calib/Tell1Config/AjustXML/AdjustXml-CCESCAN_ST.py $MYDB/CCESCAN_ST_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml

        rm -f $MYDB/CCESCAN_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml
        rm -f $MYDB/CCESCAN_ST_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml)
    
    if [ ! -f "$MYDB/CCESCAN_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}-CCESCAN.xml" ]; then
        echo "File $MYDB/CCESCAN_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}-CCESCAN.xml not created! Please check your code"
        cd $MYCALIB
        rm -f ${MYDATAFILELOC}
        rm -f ${MYLHCBCONDSLICELOC}.db
        rm -f ${MYVETRACONDDBSLICELOC}.*
        rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
        rm -f ${DefEnvVar}
    exit 1
    fi
    if [ ! -f "$MYDB/CCESCAN_ST_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}-CCESCAN_ST.xml" ]; then
        echo "File $MYDB/CCESCAN_ST_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}-CCESCAN_ST.xml not created! Please check your code"
        cd $MYCALIB
        rm -f ${MYDATAFILELOC}
        rm -f ${MYLHCBCONDSLICELOC}.db
        rm -f ${MYVETRACONDDBSLICELOC}.*
        rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
        rm -f ${DefEnvVar}
        exit 1
    fi
fi

# Update ROOT files (if requested)
if [[ $update == 1 ]]
then 
    
    export MYINPUTROOTFILENAME="${MYRESULTS}/rootfiles/${MYSTARTCALIBNAME}-r${MYSTARTCALIBRUNNB}-${MYDETTYPE}-Noise.root" 
    export MYOUTPUTROOTFILENAME="${MYRESULTS}/rootfiles/${MYCALIBNAME}-r${MYCALIBRUNNB}-${MYDETTYPE}-Noise.root" 

    echo -e "\n\n\n\nStep 10: updating noise ROOT file"
    
    # Update noise tuple (/home/st_user/Tell1Config_Calib/Results/rootfiles/<date>-r<run>-<det>-Noise.root)
    cd $MYRESULTS
    (source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
	python UpdateNoise.py)

    echo -e "\n\n\n\nStep 11: updating calibration ROOT file"

    # Update calibration tuple (/home/st_user/Tell1Config_Calib/Results/rootfiles/<date>-r<run>-<det>-Tuple.root)
    cd $MYCALIB
    export MYSECONDHEADERBIT="True"
    export MYNBEVENTS=-1
    export MYPROCESS="Tuple"
    (source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
	gaudirun.py calib.py)

    echo -e "\n\n\n\nStep 12: adding mean noise to calibration ROOT file"

    cd $MYRESULTS
    (source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
	./AddMeanNoiseToTuple.exe $MYDETTYPE $MYCALIBNAME $MYCALIBRUNNB
    )
    cd $MYMASKING
fi

# Some cleaning
echo -e "\n\n\n\nStep 13: cleaning up"
rm -f ${MYDATAFILELOC}
rm -f ${MYLHCBCONDSLICELOC}.*
rm -f ${MYVETRACONDDBSLICELOC}.db
rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
rm -f $DefEnvVar

echo -e "\n\n\n\n!!! THE END !!!"