import os

author = str(os.environ.get("MYAUTHOR"))
tag = str(os.environ.get("MYFULLCALIBNAME"))
description = str(os.environ.get("MYXMLDESCRIPTION"))
dettype = str(os.environ.get("MYDETTYPE"))
lhcbcondsliceloc = str(os.environ.get("MYLHCBCONDSLICELOC"))
maskbeetlelistfile = str(os.environ.get("MYMASKBEETLELISTFILE"))
maskportlistfile = str(os.environ.get("MYMASKPORTLISTFILE"))
maskstriplistfile = str(os.environ.get("MYMASKSTRIPLISTFILE"))
overpast = int(os.environ.get("MYOVERPAST"))

import GaudiPython
from Gaudi.Configuration import *
gbl = GaudiPython.gbl

beetlefile = []
portfile = []
stripfile = []

if maskbeetlelistfile != 'NONE':
    beetlefile = open(maskbeetlelistfile, 'r')
if maskportlistfile != 'NONE':    
    portfile = open(maskportlistfile, 'r')
if maskstriplistfile != 'NONE':
    stripfile = open(maskstriplistfile, 'r')

import shlex
from ctypes import *

# decoding version
vers = 3

# set up the DDDB and include any slices you need to get the real head !!!!!
from Configurables import CondDB
from Configurables import ( CondDB, CondDBAccessSvc )
mycalib = CondDBAccessSvc('myCalib')
mycalib.ConnectionString = 'sqlite_file:'+lhcbcondsliceloc+'.db/LHCBCOND'
CondDB().addLayer( mycalib )

from Configurables import LHCbApp, WriteSTStatusConditions, WriteSTNoiseConditions
lhcbApp = LHCbApp()
lhcbApp.DDDBtag   = "Default"
lhcbApp.CondDBtag = "Default"
ApplicationMgr( OutputLevel = INFO, AppName = 'Masking')

# cfg the output writer
writer = WriteSTStatusConditions('writer')
writer.DetType = dettype
writer.author = author
writer.tag = tag
writer.description = description
writer.outputFile = '$MYDB/LHCBCOND-'+dettype+'-'+tag+'/Conditions/'+dettype+'/ChannelInfo/ReadoutSectors.xml'
writer2 = WriteSTNoiseConditions('writer2')
writer2.DetType = dettype
writer2.author = author
writer2.tag = tag
writer2.description = description
writer2.outputFile = '$MYDB/LHCBCOND-'+dettype+'-'+tag+'/Conditions/'+dettype+'/ChannelInfo/NoiseValuesInfo.xml'
ApplicationMgr().TopAlg +=[ writer, writer2 ]

# get the detector elements
appMgr = GaudiPython.AppMgr()
det    = appMgr.detSvc()
IT   = det['/dd/Structure/LHCb/AfterMagnetRegion/T/IT']
TT   = det['/dd/Structure/LHCb/BeforeMagnetRegion/TT']

# convert the online channel to the offline one
readoutTool = appMgr.toolsvc().create(dettype+'ReadoutTool', interface = 'ISTReadoutTool')

print "-------------------> Beetle file (TELL1 link status):"
for line in beetlefile:
    list = shlex.split(line)
    tell1 = int(list[0])
    link = int(list[1])
    status = int(list[2])
    print "-------------------> ", tell1, link, status
    # the link you want to disable
    if( status == 0 ):
        newStatus = gbl.DeITSector().OK
    elif( status == 1 ):
        newStatus = gbl.DeITSector().Open
    elif( status == 4 ):
        newStatus = gbl.DeITSector().ReadoutProblems
    else:
        print 'Status ' + str(status) + ' does not exist or not implemented yet !'
    
    print 'New status: ', newStatus
    tell1ID =readoutTool.TELLNumberToSourceID(tell1)
    aBoard = readoutTool.findByBoardID(gbl.STTell1ID(tell1ID))
    beetleRep = gbl.STDAQ.BeetleRepresentation(link)
    channo = beetleRep.value()

    frac = 0
    offlineChan = aBoard.DAQToOffline(frac,vers, gbl.STDAQ.StripRepresentation(channo))
    stid = offlineChan.first

    # look up the sector and modify the status of the link
    if dettype == 'TT':
        sec = TT.__class__.__bases__[0].findSector(TT,stid)
        print 'Acting on ', sec.nickname() ,  ' Beetle ' , sec.beetle(stid)
        sec.setBeetleStatus(stid, newStatus)  
    else:
        sec = IT.__class__.__bases__[0].findSector(IT,stid)
        print 'Acting on ', sec.nickname() ,  ' Beetle ' , sec.beetle(stid)
        sec.setBeetleStatus(stid, newStatus)

    # modify noise (disabling: put 999.0; enabling: put 2.0)
    beetleRepLow = gbl.STDAQ.BeetleRepresentation(link).value()
    beetleRepHigh = gbl.STDAQ.BeetleRepresentation(link,0,127).value()
        
    if( beetleRepHigh < beetleRepLow ):
        beetleRepHigh_tmp = beetleRepHigh
        beetleRepHigh = beetleRepLow
        beetleRepLow = beetleRepHigh_tmp
            
    for chan in range(beetleRepLow,beetleRepHigh+1):
        frac = 0
        offlineChan = aBoard.DAQToOffline(frac,vers, gbl.STDAQ.StripRepresentation(chan))
        stid = offlineChan.first
        # look up the sector
        if dettype == 'TT':
            sec = TT.__class__.__bases__[0].findSector(TT,stid)
            if( status != 0 and status != 3 ):
                sec.setNoise(stid, 999.)
                sec.setCMNoise(stid, 999.)
            if( status == 0 ):
                sec.setNoise(stid, 2.)
                sec.setCMNoise(stid, 2.)
        else:
            sec = IT.__class__.__bases__[0].findSector(IT,stid)
            if( status != 0 and status != 3 ):
                sec.setNoise(stid, 999.)
                sec.setCMNoise(stid, 999.)
            if( status == 0 ):
                sec.setNoise(stid, 2.)
                sec.setCMNoise(stid, 2.)

print "-------------------> Port file (TELL1 link port status):"
for line in portfile:
    list = shlex.split(line)
    tell1 = int(list[0])
    link = int(list[1])
    port = int(list[2])
    status = int(list[3])
    print "-------------------> ", tell1, link, port, status
    # the link you want to disable
    if( status == 0 ):
        newStatus = gbl.DeITSector().OK
    elif( status == 1 ):
        newStatus = gbl.DeITSector().Open
    elif( status == 4 ):
        newStatus = gbl.DeITSector().ReadoutProblems
    else:
        print 'Status ' + str(status) + ' does not exist or not implemented yet !'

    print 'New status: ', newStatus    
    tell1ID =readoutTool.TELLNumberToSourceID(tell1)
    aBoard = readoutTool.findByBoardID(gbl.STTell1ID(tell1ID))

    # modify noise (disabling: put 999.0; enabling: put 2.0)
    portRepLow = gbl.STDAQ.BeetleRepresentation(link,port).value()
    portRepHigh = gbl.STDAQ.BeetleRepresentation(link,port,31).value()

    if( portRepHigh < portRepLow ):
        portRepHigh_tmp = portRepHigh
        portRepHigh = portRepLow
        portRepLow = portRepHigh_tmp
    
    for chan in range(portRepLow,portRepHigh+1):
        frac = 0
        offlineChan = aBoard.DAQToOffline(frac,vers, gbl.STDAQ.StripRepresentation(chan))
        stid = offlineChan.first
        
        # look up the sector and disable the link
        if dettype == 'TT':
            sec = TT.__class__.__bases__[0].findSector(TT,stid)
            print 'Acting on ', sec.nickname() ,  ' Strip ' , stid.strip()
            sec.setStripStatus(stid, newStatus)
            if( status != 0 and status != 3 ):
                sec.setNoise(stid, 999.)
                sec.setCMNoise(stid, 999.)
            if( status == 0 ):
                sec.setNoise(stid, 2.)
                sec.setCMNoise(stid, 2.)
        else:
            sec = IT.__class__.__bases__[0].findSector(IT,stid)
            print 'Acting on ', sec.nickname() ,  ' Strip ' , stid.strip() 
            sec.setStripStatus(stid, newStatus)
            if( status != 0 and status != 3 ):
                sec.setNoise(stid, 999.)
                sec.setCMNoise(stid, 999.)
            if( status == 0 ):
                sec.setNoise(stid, 2.)
                sec.setCMNoise(stid, 2.)

print "-------------------> Strip file (TELL1 channel status):"
for line in stripfile:
    list = shlex.split(line)
    tell1 = int(list[0])
    channo = int(list[1])
    status = int(list[2])
    print "-------------------> ", tell1, channo, status
    # the link you want to disable
    if( status == 0 ):
        newStatus = gbl.DeITSector().OK
    elif( status == 1 ):
        newStatus = gbl.DeITSector().Open
    elif( status == 4 ):
        newStatus = gbl.DeITSector().ReadoutProblems
    else:
        print 'Status ' + str(status) + ' does not exist or not implemented yet !'
        
    print 'New status: ', newStatus
    tell1ID = readoutTool.TELLNumberToSourceID(tell1)
    aBoard = readoutTool.findByBoardID(gbl.STTell1ID(tell1ID))
    
    frac = 0
    offlineChan = aBoard.DAQToOffline(frac,vers, gbl.STDAQ.StripRepresentation(channo))
    stid = offlineChan.first

    # look up the sector and modify the status of the link
    if dettype == 'TT':
        sec = TT.__class__.__bases__[0].findSector(TT,stid)
        print 'Acting on ', sec.nickname() ,  ' Strip ' , stid.strip()
        sec.setStripStatus(stid, newStatus)
        if( status != 0 and status != 3 ):
            sec.setNoise(stid, 999.)
            sec.setCMNoise(stid, 999.)
        if( status == 0 ):
            sec.setNoise(stid, 2.)
            sec.setCMNoise(stid, 2.)
    else:
        sec = IT.__class__.__bases__[0].findSector(IT,stid)
        print 'Acting on ', sec.nickname() ,  ' Strip ' , stid.strip()
        sec.setStripStatus(stid, newStatus)
        if( status != 0 and status != 3 ):
            sec.setNoise(stid, 999.)
            sec.setCMNoise(stid, 999.)
        if ( status == 0 ):
            sec.setNoise(stid, 2.)
            sec.setCMNoise(stid, 2.)
                        
appMgr.exit()

# make the output look pretty
import os
cmd = 'xsltproc -o $MYDB/LHCBCOND-'+dettype+'-'+tag+'/Conditions/'+dettype+'/ChannelInfo/ReadoutSectors.xml.tmp pretty.xsl $MYDB/LHCBCOND-'+dettype+'-'+tag+'/Conditions/'+dettype+'/ChannelInfo/ReadoutSectors.xml'
cmd2 = 'xsltproc -o $MYDB/LHCBCOND-'+dettype+'-'+tag+'/Conditions/'+dettype+'/ChannelInfo/NoiseValuesInfo.xml.tmp pretty.xsl $MYDB/LHCBCOND-'+dettype+'-'+tag+'/Conditions/'+dettype+'/ChannelInfo/NoiseValuesInfo.xml'
os.system(cmd)
os.system(cmd2)
os.system('mv $MYDB/LHCBCOND-'+dettype+'-'+tag+'/Conditions/'+dettype+'/ChannelInfo/ReadoutSectors.xml.tmp $MYDB/LHCBCOND-'+dettype+'-'+tag+'/Conditions/'+dettype+'/ChannelInfo/ReadoutSectors.xml')
os.system('mv $MYDB/LHCBCOND-'+dettype+'-'+tag+'/Conditions/'+dettype+'/ChannelInfo/NoiseValuesInfo.xml.tmp $MYDB/LHCBCOND-'+dettype+'-'+tag+'/Conditions/'+dettype+'/ChannelInfo/NoiseValuesInfo.xml')
