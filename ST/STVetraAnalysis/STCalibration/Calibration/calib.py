from Gaudi.Configuration import FileCatalog, EventSelector
from Configurables import STVetraAnalysis

import os

dettype = str(os.environ.get("MYDETTYPE"))
process = str(os.environ.get("MYPROCESS"))
calibname = str(os.environ.get("MYFULLCALIBNAME"))
nbevents = str(os.environ.get("MYNBEVENTS"))
nzsevents = str(os.environ.get("MYNZSEVENTS"))
author = str(os.environ.get("MYAUTHOR"))
xmlversion = str(os.environ.get("MYXMLVERSION"))
xmldescription = str(os.environ.get("MYXMLDESCRIPTION"))
lhcbcondtag = str(os.environ.get("MYLHCBCONDTAG"))
lhcbcondsliceloc = str(os.environ.get("MYLHCBCONDSLICELOC"))
vetracondsliceloc = str(os.environ.get("MYVETRACONDDBSLICELOC"))
secondheaderbit = str(os.environ.get("MYSECONDHEADERBIT"))

FileCatalog().Catalogs = [ "xmlcatalog_file:MyCatalog.xml" ]

STVetraAnalysis().DatasetName = "$MYRESULTS/rootfiles/"+calibname

STVetraAnalysis().VetraOutLvl = 3
STVetraAnalysis().DetType     = dettype
STVetraAnalysis().Process     = process
STVetraAnalysis().VetraCondDB = vetracondsliceloc+'.db'
STVetraAnalysis().CondDBtag   = lhcbcondtag
STVetraAnalysis().CondDB      = lhcbcondsliceloc+'.db'
STVetraAnalysis().CheckNZS    = True

STVetraAnalysis().XMLAuthor      = author
STVetraAnalysis().XMLTag         = xmlversion
STVetraAnalysis().XMLDescription = xmldescription

if(secondheaderbit == 'True'):
    STVetraAnalysis().SecondHeaderBit = True
else:
    STVetraAnalysis().SecondHeaderBit = False

if(process == "PedestalUpdate"):
    STVetraAnalysis().Convergence    = int(nbevents) - 5
else:
    STVetraAnalysis().Convergence    = 0

STVetraAnalysis().PrintFreq = 100000
STVetraAnalysis().EvtMax    = nbevents
STVetraAnalysis().NZSEvents = nzsevents
