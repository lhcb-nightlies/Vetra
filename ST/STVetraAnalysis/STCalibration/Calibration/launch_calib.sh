#---------------------------------------------------------------------------------
#
#  BASH script that performs the ST calibration
#
#  Author   : Frederic Dupertuis
#  Modified : Vincenzo Battista
#---------------------------------------------------------------------------------
# Launching example:
# ./launch_calib.sh IT 20140429 141817 EOFLOCAL

#--------------------------------------------------------------------------------- 
# Detector to be calibrated (ex IT, TT)
export MYDETTYPE=$1
#---------------------------------------------------------------------------------
# Day and Run used for the Calibration 
export MYCALIBNAME=$2
export MYCALIBRUNNB=$3
# Data location LOCAL, EOFLOCAL, CASTOR, EOFCASTOR, CALIBLOCAL or CALIBCASTOR?
export MYDATALOC=$4
# Required number of events (recommended: -1)
export MYNBEVENTS="-1" 
# Required number of raw banks to be processed for the Calibration
export MYCALIBNBEVENTS="20000"
# Required number of raw banks to be processed for the Monitoring
export MYMONITORNBEVENTS="5000"
# Required number of raw banks to be processed for the HeaderMonitoring
export MYHEADERMONITORNBEVENTS="5"
# Emulate the 2nd header bit (True or False)
export MYSECONDHEADERBIT="True"
#---------------------------------------------------------------------------------
# LHCBCOND tag ("HEAD": latest version of LHCBCOND)
export MYLHCBCONDTAG="HEAD"
# Default VetraCondDB location (all the informations on the previous calibration)
export MYDEFAULTCONDDBNAME="/group/st/condxml/DBASE/STCOND.db"
# Input calibration name, run number
# > if let to "" latest applied calibration name is used
export MYSTARTCALIBNAME=""
export MYSTARTCALIBRUNNB=""
#--------------------------------------------------------------------------------- 
# Do you want to use AjustXml.py ?
ajust=0
# Do you want to create CCESCAN/CCESCAN_ST recipes as well?
ccescan=1
# Do you want to create a debug folder? (1=True and 0=False)
debug=0
#--------------------------------------------------------------------------------- 
# Calibration Name (Ex : today date like 20110101 or 20110101-r84000 or 20110101.1) 
export MYFULLCALIBNAME="${MYCALIBNAME}-r${MYCALIBRUNNB}"
# XML informations
export MYAUTHOR="V Battista"
# > XML Version (Ex : today date like 2011/01/01 or Data11-20110101)
export MYXMLVERSION="Data16-${MYCALIBNAME}"
# > XML Description (Ex : Based on run 84000)
export MYXMLDESCRIPTION="Noise based on run ${MYCALIBRUNNB}. Charge calibration from 2010."
# NZS data informations (NZS: Non Zero Suppressed data, or Raw data).
export MYDATAFILELOC="data/${MYDETTYPE}_${MYCALIBRUNNB}_RAW.py"
#---------------------------------------------------------------------------------

if [[ $debug == 1 ]]
then
    rm -rf Debug_${MYFULLCALIBNAME}
    mkdir Debug_${MYFULLCALIBNAME}
fi

# List of NZS data location. Create the gaudi option file <det>_<run>_RAW.py
# to retrieve informations from the detector
cd data
echo -e "\n\n\n\nStep 1: creating ${MYDETTYPE}_${MYCALIBRUNNB}_RAW.py for ${MYDETTYPE} and run ${MYCALIBRUNNB} in ${MYDATALOC}"
(source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
    source ${MYDETTYPE}EventSelectorCreator${MYDATALOC}.sh ${MYCALIBRUNNB})
cd ..

# Location of starting CONDDB and LHCBCOND slices. If calib name and run are empty,
# retrieve them from "/group/st/condxml/DBASE/recipes.list" file (latest names used)
echo -e "\n\n\n\nStep 2: retrieving starting databases"
export DefEnvVar="DefEnvVar.sh"
if [[ ${MYSTARTCALIBNAME} == "" ]]||[[ ${MYSTARTCALIBRUNNB} == "" ]]
then
    (source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
	echo export MYSTARTCALIBNAME=`python $MYRESULTS/STCONDDB_interface.py $MYDETTYPE calibname` > "$DefEnvVar"
	echo export MYSTARTCALIBRUNNB=`python $MYRESULTS/STCONDDB_interface.py $MYDETTYPE calibrun` >> "$DefEnvVar")
    source $DefEnvVar
    export MYSTARTVETRACONDDBSLICELOC="$MYDB/DEFAULT_RECIPES/"${MYDETTYPE}"COND-"${MYSTARTCALIBNAME}"-r"${MYSTARTCALIBRUNNB}".xml"
    export MYSTARTLHCBCONDSLICELOC="$MYDB/LHCBCOND-"${MYDETTYPE}"-"${MYSTARTCALIBNAME}"-r"${MYSTARTCALIBRUNNB}
else
    export MYSTARTVETRACONDDBSLICELOC="$MYDB/DEFAULT_RECIPES/"${MYDETTYPE}"COND-"${MYSTARTCALIBNAME}"-r"${MYSTARTCALIBRUNNB}".xml"
    export MYSTARTLHCBCONDSLICELOC="$MYDB/LHCBCOND-"${MYDETTYPE}"-"${MYSTARTCALIBNAME}"-r"${MYSTARTCALIBRUNNB}
fi

echo "Starting CONDDB:"
echo "$MYSTARTVETRACONDDBSLICELOC"

echo "Starting LHCBCOND DB:"
echo "$MYSTARTLHCBCONDSLICELOC"

# ---Create CONDB and LHCBCOND db in a .db format (LHCb software requires them instead of .xml files) 

# Create (temporary) .db database from starting CONDDB
# 1) Create the required "structure" by making the following directories:
#    /home/st_user/Tell1Config_Calib/DB/VetraCondDB/<det>COND-<date>-r<run>/CondDB/cond.xml ("current" calibration)
#    /home/st_user/Tell1Config_Calib/DB/VetraCondDB/<det>COND-<date>-r<run>/CondDB/TELL1Cond.xml ("starting" calibration)
# 2) Create the .db file:
#    /home/st_user/Tell1Config_Calib/DB/<det>COND-<date>-r<run>.db
echo -e "\n\n\n\nStep 3: putting CONDDB and LHCBCOND in a .db format"  
export MYVETRACONDDBSLICELOC="$MYDB/${MYDETTYPE}COND-${MYFULLCALIBNAME}"
mkdir -p $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
tar -C $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/ -zxvf $MYDB/VetraCondDB/${MYDETTYPE}.tar.gz
cp ${MYSTARTVETRACONDDBSLICELOC} $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/CondDB/TELL1Cond.xml
rm -f ${MYVETRACONDDBSLICELOC}.db
(source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
    copy_files_to_db.py -c sqlite_file:${MYVETRACONDDBSLICELOC}.db/COND -s $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/)

# Create (temporary) database from starting LHCBCOND
# 1) Create the required "structure" by making the following directory:
#    /home/st_user/Tell1Config_Calib/DB/LHCBCOND-<det>-r<date>-r<run>/Conditions/* (all .xml files in the starting directory)
# 2) Create the .db file:
#    /home/st_user/Tell1Config_Calib/DB/LHCBCOND-<det>-<date>-r<run>.db
echo -e "\n\n\n\nStep 4: creating temporary database from LHCBCOND"
export MYLHCBCONDSLICELOC="$MYDB/LHCBCOND-${MYDETTYPE}-${MYFULLCALIBNAME}"
mkdir -p ${MYLHCBCONDSLICELOC}
cp -r ${MYSTARTLHCBCONDSLICELOC}/* ${MYLHCBCONDSLICELOC}/
rm -f ${MYLHCBCONDSLICELOC}.db
(source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
    copy_files_to_db.py -c sqlite_file:${MYLHCBCONDSLICELOC}.db/LHCBCOND -s ${MYLHCBCONDSLICELOC})

# ---Start Monitoring and Calibration. For each step, the following code is used:
# ---calib.py: perform the calibration step. Is launched via gaudirun.py
# ---<det>-<run>_RAW.py: option file to retrieve NZS data
# ---Configuration.py: STVetraAnalysis class definition (called in calib.py)

# Monitor new NZS data with previous calibration using STFastNoiseMonitor class
# Create /home/st_user/Tell1Config_Calib/Results/rootfiles/<date>-r<run>-<det>-Monitor-<start_date>-r<start_run>.root  
echo -e "\n\n\n\nStep 5: starting monitoring"
export MYNZSEVENTS=${MYMONITORNBEVENTS}
export MYPROCESS="Monitor"
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    gaudirun.py calib.py ${MYDATAFILELOC})
mv $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}.root $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-${MYSTARTCALIBNAME}-r${MYSTARTCALIBRUNNB}.root
if [ ! -f "$MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-${MYSTARTCALIBNAME}-r${MYSTARTCALIBRUNNB}.root" ]; then
    echo "File ${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-${MYSTARTCALIBNAME}-r${MYSTARTCALIBRUNNB}.root not created! Please check your code"
    cd $MYCALIB
    rm -f ${MYDATAFILELOC}
    rm -f ${MYLHCBCONDSLICELOC}.db
    rm -f ${MYVETRACONDDBSLICELOC}.db
    rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
    rm -f ${DefEnvVar}
    exit 1
fi

# Launch the pedestal calibration using STPedestalEstimator class
# Dump new pedestals into /home/st_user/Tell1Config_Calib/Results/<det>-r<run>-<det>-Pedestal-TELL1Cond.xml (temporary db)
# using STVetraCondDBWriter 
echo -e "\n\n\n\nStep 6: starting pedestal calibration"
export MYNZSEVENTS=${MYCALIBNBEVENTS}
export MYPROCESS="Pedestal"
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    gaudirun.py calib.py ${MYDATAFILELOC})

# Update CONDDB database (/home/st_user/Tell1Config_Calib/DB/<det>COND-<date>-r<run>.db) with new pedestals
# taken from the updated TELL1Cond.xml db
echo -e "\n\n\n\nStep 7: updating CONDDB"
if [[ $debug == 1 ]]
then
    mkdir -p Debug_${MYFULLCALIBNAME}/Step7_beforeUpdating
    cp $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-TELL1Cond.xml Debug_${MYFULLCALIBNAME}/Step7_beforeUpdating
fi
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    perl $STVETRAANALYSISROOT/perl/DBCosmetics.pl $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-TELL1Cond.xml)
if [[ $debug == 1 ]]
then
    mkdir -p Debug_${MYFULLCALIBNAME}/Step7_afterUpdating
    cp $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-TELL1Cond.xml Debug_${MYFULLCALIBNAME}/Step7_afterUpdating
fi
cp $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-TELL1Cond.xml $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/CondDB/TELL1Cond.xml
echo "CONDDB database updated with new pedestals:"
echo "$MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/CondDB/TELL1Cond.xml"
rm -f $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-TELL1Cond.xml
rm -f $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}.root
rm -f ${MYVETRACONDDBSLICELOC}.db
(source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
    copy_files_to_db.py -c sqlite_file:${MYVETRACONDDBSLICELOC}.db/COND -s $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/)

# Launch the header correction calibration using STHeaderCorrection class
# Dump new parameters into /home/st_user/Tell1Config_Calib/Results/<det>-r<run>-<date>-HeaderCorr-TELL1Cond.xml (temporary db) 
# using STVetraCondDBWriter
# Create /home/st_user/Tell1Config_Calib/Results/rootfiles/<date>-r<run>-<det>-HeaderCorr.root tuple
echo -e "\n\n\n\nStep 8: starting header correction"
export MYPROCESS="HeaderCorr"
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    gaudirun.py calib.py ${MYDATAFILELOC})
if [ ! -f "$MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-HeaderCorr.root" ]; then
    echo "File ${MYFULLCALIBNAME}-${MYDETTYPE}-HeaderCorr.root not created! Please check your code"
    cd $MYCALIB
    rm -f ${MYDATAFILELOC}
    rm -f ${MYLHCBCONDSLICELOC}.db
    rm -f ${MYVETRACONDDBSLICELOC}.*
    rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
    rm -f ${DefEnvVar}
    exit 1
fi

# Update CONDDB database (/home/st_user/Tell1Config_Calib/DB/<det>COND-<date>-r<run>.db) with new header correction
# parameters taken from the updated TELL1Cond.xml
# Create also /home/st_user/Tell1Config_Calib/DB/<det>COND-<date>-r<run>.xml
echo -e "\n\n\n\nStep 9: updating CONDDB"
if [[ $debug == 1 ]]
then
    mkdir -p Debug_${MYFULLCALIBNAME}/Step9_beforeUpdating
    cp $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-TELL1Cond.xml Debug_${MYFULLCALIBNAME}/Step9_beforeUpdating
fi
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    perl $STVETRAANALYSISROOT/perl/DBCosmetics.pl $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-TELL1Cond.xml)
if [[ $debug == 1 ]]
then
    mkdir -p Debug_${MYFULLCALIBNAME}/Step9_afterUpdating
    cp $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-TELL1Cond.xml Debug_${MYFULLCALIBNAME}/Step9_afterUpdating
fi
cp $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-TELL1Cond.xml $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/CondDB/TELL1Cond.xml
cp $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-TELL1Cond.xml $MYDB/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml
echo "CONDDB database updated with new header correction:"
echo "$MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/CondDB/TELL1Cond.xml"
echo "$MYDB/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml"
rm -f $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}-TELL1Cond.xml
#rm -f $MYRESULTS/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}.root
rm -f ${MYVETRACONDDBSLICELOC}.db
(source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
    copy_files_to_db.py -c sqlite_file:${MYVETRACONDDBSLICELOC}.db/COND -s $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/)

# Launch the noise calibration (measure RAW and LCMS, compute CM) using STFastNoiseMonitor class
# Dump noise informations into /home/st_user/Tell1Config_Calib/Results/<det>-r<date>-<run>-NoiseValuesInfo.xml
# using WriteSTNoiseConditions
# Create /home/st_user/Tell1Config_Calib/Results/rootfiles/<date>-r<run>-<det>-Noise.root tuple 
echo -e "\n\n\n\nStep 10: starting noise calibration"
export MYPROCESS="Noise"
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    gaudirun.py calib.py ${MYDATAFILELOC})
if [ ! -f "$MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-Noise.root" ]; then
    echo "File ${MYFULLCALIBNAME}-${MYDETTYPE}-Noise.root not created! Please check your code"
    cd $MYCALIB
    rm -f ${MYDATAFILELOC}
    rm -f ${MYLHCBCONDSLICELOC}.db
    rm -f ${MYVETRACONDDBSLICELOC}.*
    rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
    rm -f ${DefEnvVar}
    exit 1
fi

# Update LHCBCOND database (/home/st_user/Tell1Config_Calib/DB/LHCBCOND-<det>-<date>-r<run>.db) with new noise info
# taken from the updated NoiseValuesInfo.xml db
echo -e "\n\n\n\nStep 11: updating LHCBCOND"
if [[ $debug == 1 ]]
then
    mkdir -p Debug_${MYFULLCALIBNAME}/Step11_beforeUpdating
    cp $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}ValuesInfo.xml Debug_${MYFULLCALIBNAME}/Step11_beforeUpdating
fi
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    perl $STVETRAANALYSISROOT/perl/DBCosmetics.pl $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}ValuesInfo.xml)
if [[ $debug == 1 ]]
then
    mkdir -p Debug_${MYFULLCALIBNAME}/Step11_afterUpdating
    cp $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}ValuesInfo.xml Debug_${MYFULLCALIBNAME}/Step11_afterUpdating
fi
mv $MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-${MYPROCESS}ValuesInfo.xml $MYDB/LHCBCOND-${MYDETTYPE}-${MYFULLCALIBNAME}/Conditions/${MYDETTYPE}/ChannelInfo/NoiseValuesInfo.xml
echo "LHCBCOND database updated with new noise:"
echo "$MYDB/LHCBCOND-${MYDETTYPE}-${MYFULLCALIBNAME}/Conditions/${MYDETTYPE}/ChannelInfo/NoiseValuesInfo.xml"
rm -f ${MYLHCBCONDSLICELOC}.db
(source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
    copy_files_to_db.py -c sqlite_file:${MYLHCBCONDSLICELOC}.db/LHCBCOND -s ${MYLHCBCONDSLICELOC})

# Update clustering thresholds in CONDDB using the $STVETRAANALYSISROOT/python/UpdateVetraCondFromLHCBCOND.py script.
# The options are the following:
# -s: detector type (IT, TT)
# -c: LHCBCOND path (HEAD /home/st_user/Tell1Config_Calib/DB/LHCBCOND-<det>-<date>-r<run>.db)
# -v: CONDDB path (/home/st_user/Tell1Config_Calib/DB/<det>COND-<date>-r<run>.db)
# -f: output file (/home/st_user/Tell1Config_Calib/DB/<det>COND-<date>-r<run>.xml)
echo -e "\n\n\n\nStep 12: updating clustering thresholds in CONDDB"
if [[ $debug == 1 ]]
then
    mkdir -p Debug_${MYFULLCALIBNAME}/Step12_beforeUpdating
    cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml Debug_${MYFULLCALIBNAME}/Step12_beforeUpdating
fi
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    python $STVETRAANALYSISROOT/python/UpdateVetraCondFromLHCBCOND.py -s ${MYDETTYPE} -c ${MYLHCBCONDTAG} $MYDB/LHCBCOND-${MYDETTYPE}-${MYFULLCALIBNAME}.db -v $MYDB/${MYDETTYPE}COND-${MYFULLCALIBNAME}.db -f $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml)
if [[ $debug == 1 ]]
then
    mkdir -p Debug_${MYFULLCALIBNAME}/Step12_afterUpdating
    cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml Debug_${MYFULLCALIBNAME}/Step12_afterUpdating
fi
echo "CONDDB database updated with new clustering thresholds:"
echo "$MYDB/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml"

# Ajust CONDDB xml if needed (/home/st_user/Tell1Config_Calib/DB/<det>COND-<date>-r<run>.xml)
if [[ $ajust == 1 ]]
then 
    echo -e "\n\n\n\nStep 13: adjusting CONDDB (xml file)"
    if [[ $debug == 1 ]]
    then
	mkdir -p Debug_${MYFULLCALIBNAME}/Step13_beforeUpdating
	cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml Debug_${MYFULLCALIBNAME}/Step13_beforeUpdating
    fi
    (source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
	python $STVETRAANALYSISROOT/python/AjustXml.py $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml)
    if [[ $debug == 1 ]]
    then
        mkdir -p Debug_${MYFULLCALIBNAME}/Step13_afterUpdating
        cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml Debug_${MYFULLCALIBNAME}/Step13_afterUpdating
    fi
    echo "CONDDB adjusted:"
    echo "$MYDB/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml"
else 
    echo -e "\n\n\n\nStep 13: doing some cosmetics on CONDDB (xml file)"
    if [[ $debug == 1 ]]
    then
        mkdir -p Debug_${MYFULLCALIBNAME}/Step13_beforeUpdating
        cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml Debug_${MYFULLCALIBNAME}/Step13_beforeUpdating
    fi
    (source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra` 
	perl $STVETRAANALYSISROOT/perl/DBCosmetics.pl $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml)
    if [[ $debug == 1 ]]
    then
        mkdir -p Debug_${MYFULLCALIBNAME}/Step13_afterUpdating
	cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml Debug_${MYFULLCALIBNAME}/Step13_afterUpdating
    fi
fi

# Update CONDDB database (/home/st_user/Tell1Config_Calib/DB/<det>COND-<date>-r<run>.db) with new clustering threshold values
# taken from the updated TELL1Cond.xml db
echo -e "\n\n\n\nStep 14: updating CONDDB"
cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/CondDB/TELL1Cond.xml
rm -f ${MYVETRACONDDBSLICELOC}.db
(source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
    copy_files_to_db.py -c sqlite_file:${MYVETRACONDDBSLICELOC}.db/COND -s $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}/)

# Create CCESCAN and CCESCAN_ST recipes starting from the DEFAULT one
if [[ $ccescan == 1 ]]
then
    echo -e "\n\n\n\nStep 15: creating CCESCAN and CCESCAN_ST recipes"
    cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml $MYDB/CCESCAN_RECIPES/
    cp $MYDB/DEFAULT_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml $MYDB/CCESCAN_ST_RECIPES/
    (source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
	python /home/st_user/Tell1Config_Calib/Tell1Config/AjustXML/AdjustXml-CCESCAN.py $MYDB/CCESCAN_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml
	python /home/st_user/Tell1Config_Calib/Tell1Config/AjustXML/AdjustXml-CCESCAN_ST.py $MYDB/CCESCAN_ST_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml
	
	rm -f $MYDB/CCESCAN_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml
	rm -f $MYDB/CCESCAN_ST_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}.xml)
    
    if [ ! -f "$MYDB/CCESCAN_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}-CCESCAN.xml" ]; then
	echo "File $MYDB/CCESCAN_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}-CCESCAN.xml not created! Please check your code"
	cd $MYCALIB
	rm -f ${MYDATAFILELOC}
	rm -f ${MYLHCBCONDSLICELOC}.db
	rm -f ${MYVETRACONDDBSLICELOC}.*
	rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
	rm -f ${DefEnvVar}
    exit 1
    fi
    if [ ! -f "$MYDB/CCESCAN_ST_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}-CCESCAN_ST.xml" ]; then
	echo "File $MYDB/CCESCAN_ST_RECIPES/${MYDETTYPE}COND-${MYFULLCALIBNAME}-CCESCAN_ST.xml not created! Please check your code"
	cd $MYCALIB
	rm -f ${MYDATAFILELOC}
	rm -f ${MYLHCBCONDSLICELOC}.db
	rm -f ${MYVETRACONDDBSLICELOC}.*
	rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
	rm -f ${DefEnvVar}
	exit 1
    fi
fi

#---Create output plots and tuples

# Create temporary header tuple (/home/st_user/Tell1Config_Calib/Results/rootfiles/<date>-r<run>-<det>-HeaderTuple.root) 
# using STHeaderTuple class
echo -e "\n\n\n\nStep 16: creating header tuples"
export MYNZSEVENTS=${MYHEADERMONITORNBEVENTS}
export MYPROCESS="HeaderTuple"
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    gaudirun.py calib.py ${MYDATAFILELOC})
if [ ! -f "$MYRESULTS/rootfiles/${MYCALIBNAME}-r${MYCALIBRUNNB}-${MYDETTYPE}-HeaderTuple.root" ]; then
    echo "File ${MYCALIBNAME}-r${MYCALIBRUNNB}-${MYDETTYPE}-HeaderTuple.root not created! Please check your code"
    cd $MYCALIB
    rm -f ${MYDATAFILELOC}
    rm -f ${MYLHCBCONDSLICELOC}.db
    rm -f ${MYVETRACONDDBSLICELOC}.*
    rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
    rm -f ${DefEnvVar}
    exit 1
fi

# Create calibration tuple (/home/st_user/Tell1Config_Calib/Results/rootfiles/<date>-r<run>-<det>-Tuple.root)
# using STCalibrationTuple class
echo -e "\n\n\n\nStep 17: creating calibration tuples"
export MYPROCESS="Tuple"
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    gaudirun.py calib.py)
if [ ! -f "$MYRESULTS/rootfiles/${MYCALIBNAME}-r${MYCALIBRUNNB}-${MYDETTYPE}-Tuple.root" ]; then
    echo "File ${MYCALIBNAME}-r${MYCALIBRUNNB}-${MYDETTYPE}-Tuple.root not created! Please check your code"
    cd $MYCALIB
    rm -f ${MYDATAFILELOC}
    rm -f ${MYLHCBCONDSLICELOC}.db
    rm -f ${MYVETRACONDDBSLICELOC}.*
    rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
    rm -f ${DefEnvVar}
    exit 1
fi

# Create monitoring and summary plots (stored in /home/st_user/Tell1Config_Calib/Results/Plots/<date>-r<run>-<det>.tar)
# Create output tuples (stored in /home/st_user/Tell1Config_Calib/Results/rootfiles/*.root)
# Remove intermediate root files and plots
echo -e "\n\n\n\nStep 18: creating monitoring and summary plots"
cd $MYRESULTS
(source `which SetupProject.sh` LHCb `cat $MYCALIB/versionLHCb`
    
    echo -e "\n\n\n\nStep 18a: adding mean noise to tuple"
    ./AddMeanNoiseToTuple.exe $MYDETTYPE $MYCALIBNAME $MYCALIBRUNNB
    if [ ! -f "$MYRESULTS/rootfiles/${MYCALIBNAME}-r${MYCALIBRUNNB}-${MYDETTYPE}-Tuple.root" ]; then
	echo "File ${MYCALIBNAME}-r${MYCALIBRUNNB}-${MYDETTYPE}-Tuple.root not created! Please check your code"
	cd $MYCALIB
	rm -f ${MYDATAFILELOC}
	rm -f ${MYLHCBCONDSLICELOC}.db
	rm -f ${MYVETRACONDDBSLICELOC}.*
	rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
	rm -f ${DefEnvVar}
	exit 1
    fi

    echo -e "\n\n\n\nStep 18b: creating monitoring tuple"
    if [ ! -f "$MYRESULTS/rootfiles/${MYSTARTCALIBNAME}-r${MYSTARTCALIBRUNNB}-${MYDETTYPE}-Tuple.root" ]; then
	echo "File ${MYSTARTCALIBNAME}-r${MYSTARTCALIBRUNNB}-${MYDETTYPE}-Tuple.root doesn't exists! Please check your reference calibration"
        cd $MYCALIB
        rm -f ${MYDATAFILELOC}
        rm -f ${MYLHCBCONDSLICELOC}.db
        rm -f ${MYVETRACONDDBSLICELOC}.*
        rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
        rm -f ${DefEnvVar}
        exit 1
    fi
    ./CreateMonitorTuple.exe $MYDETTYPE $MYCALIBNAME $MYCALIBRUNNB $MYSTARTCALIBNAME $MYSTARTCALIBRUNNB
    if [ ! -f "$MYRESULTS/rootfiles/${MYCALIBNAME}-r${MYCALIBRUNNB}-${MYDETTYPE}-Monitor-${MYSTARTCALIBNAME}-r${MYSTARTCALIBRUNNB}-Tuple.root" ]; then
        echo "File $MYRESULTS/rootfiles/${MYCALIBNAME}-r${MYCALIBRUNNB}-${MYDETTYPE}-Monitor-${MYSTARTCALIBNAME}-r${MYSTARTCALIBRUNNB}-Tuple.root not created! Please check your code"
        cd $MYCALIB
        rm -f ${MYDATAFILELOC}
        rm -f ${MYLHCBCONDSLICELOC}.db
        rm -f ${MYVETRACONDDBSLICELOC}.*
        rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
        rm -f ${DefEnvVar}
        exit 1
    fi

    echo -e "\n\n\n\nStep 18c: creating calibration summary plots (1)"
    ./CalibSummaryPlots.exe $MYDETTYPE $MYCALIBNAME $MYCALIBRUNNB $MYSTARTCALIBNAME $MYSTARTCALIBRUNNB
    echo -e "\n\n\n\nStep 18d: creating calibration summary plots (2)"
    ./CalibSummaryPlots.exe $MYDETTYPE $MYCALIBNAME $MYCALIBRUNNB $MYSTARTCALIBNAME $MYSTARTCALIBRUNNB $MYCALIBNAME $MYCALIBRUNNB
    echo -e "\n\n\n\nStep 18e: creating header monitor plots"
    ./HeaderMonitor.exe $MYDETTYPE $MYCALIBNAME $MYCALIBRUNNB
)

rm -f rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-Monitor-${MYSTARTCALIBNAME}-r${MYSTARTCALIBRUNNB}.root
rm -f rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-HeaderTuple.root
rm -f rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}*TupleDiff.root
cd Plots
tar -cvf ${MYFULLCALIBNAME}-${MYDETTYPE}.tar ${MYFULLCALIBNAME}-${MYDETTYPE}*.png
rm -f ${MYFULLCALIBNAME}-${MYDETTYPE}*.{png,eps}

# Analyse the noise to search for low noise channels/broken bonds.
# All plots are stored in /group/st/sw/Tell1Config_Calib/Results/LowNoiseFinder/Plots/LowNoiseFinder-<date>-r<run>-<det>.tar
echo -e "\n\n\n\nStep 19: looking for low noise channels"
cd $MYCALIB
export MYROOTFILENAME="$MYRESULTS/rootfiles/${MYFULLCALIBNAME}-${MYDETTYPE}-Noise.root"
(source `which SetupProject.sh` Vetra `cat $MYCALIB/versionVetra`
    python /group/st/sw/Tell1Config_Calib/Tell1Config/NoiseAnalysis/LowNoiseFinder.py)
cd /group/st/sw/Tell1Config_Calib/Results/LowNoiseFinder/Plots/
tar -cvf LowNoiseFinder-${MYFULLCALIBNAME}-${MYDETTYPE}.tar LowNoiseFinder-${MYFULLCALIBNAME}-${MYDETTYPE}*.png
rm -f LowNoiseFinder-${MYFULLCALIBNAME}-${MYDETTYPE}*.{png,eps}

# Some cleaning
echo -e "\n\n\n\nStep 20: cleaning up"
cd $MYCALIB
rm -f ${MYDATAFILELOC}
rm -f ${MYLHCBCONDSLICELOC}.db
rm -f ${MYVETRACONDDBSLICELOC}.*
rm -rf $MYDB/VetraCondDB/${MYDETTYPE}-${MYFULLCALIBNAME}
rm -f ${DefEnvVar}

echo -e "\n\n\n\n!!! THE END !!!"