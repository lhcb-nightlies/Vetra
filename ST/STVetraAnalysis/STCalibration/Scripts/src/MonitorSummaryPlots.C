#include "TString.h"
#include "TCut.h"
#include "TH1I.h"
#include "TH2I.h"
#include "TProfile.h"
#include "THStack.h"
#include "TPaveStats.h"
#include "TLegend.h"
#include "TF1.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TPad.h"

#include <iostream>
#include <vector>
#include <string>

using namespace std;

void GetMean( TH1I* mean, TProfile* profile ){
  for(Int_t i(1);i<=profile->GetNbinsX();i++ ){
    if(profile->GetBinError(i)>0.)
      mean->Fill(profile->GetBinContent(i));
  }
  delete profile;
}

void GetRMS( TH1I* RMS, TProfile* profile ){
  for(Int_t i(1);i<=profile->GetNbinsX();i++ ){
    if(profile->GetBinError(i)>0.)
      RMS->Fill(profile->GetBinError(i));
  }
  delete profile;
}

void MonitorSummaryPlots( TString dettype,
			  TString date, 
			  TString run,
			  TString date_ref,
			  TString run_ref,
			  TString date2 = "", 
			  TString run2 = "",
			  TString date_ref2 = "",
			  TString run_ref2 = "" ){
  
  TString filename_Monitor("$MYRESULTS/rootfiles/"+date+"-r"+run+"-"+dettype+"-Monitor-"+date_ref+"-r"+run_ref+".root");
  TString filename_Monitor_ref("");
  if(date2 == "")
    filename_Monitor_ref = "$MYRESULTS/rootfiles/"+date_ref+"-r"+run_ref+"-"+dettype+"-Noise.root";
  else
    filename_Monitor_ref = "$MYRESULTS/rootfiles/"+date2+"-r"+run2+"-"+dettype+"-Monitor-"+date_ref2+"-r"+run_ref2+".root";
  
  Int_t nbtell1(42);

  if(filename_Monitor.Contains("TT")){
    filename_Monitor_ref.ReplaceAll("IT","TT");
    nbtell1 = 48;
  }
  
  gROOT->SetStyle("Plain");
  //gStyle->SetOptStat(0);
  gStyle->SetTitleX(0.523);
  gStyle->SetTitleAlign(23); 
  gStyle->SetTitleBorderSize(0);
  gStyle->SetPaintTextFormat("5.1f");
  //gStyle->SetStatFontSize(0.15);
  gStyle->SetStatY(0.865);
  gStyle->SetStatX(0.92);
  gStyle->SetStatH(0.15);
  gStyle->SetOptStat(111110);
  gStyle->SetTitleFontSize(0.07);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetPadRightMargin(0.05);
  gStyle->SetPadBottomMargin(0.07);
  
  TFile* file_Monitor = new TFile(filename_Monitor.Data());
  TFile* file_Monitor_ref = new TFile(filename_Monitor_ref.Data());
  
  TH1I* mean_PedSub = new TH1I("mean_PedSub","Mean of PedSubADCs",210,-10.5,10.5); 
  TH1I* mean_LCMS = new TH1I("mean_LCMS","Mean of LCMSADCs",210,-10.5,10.5); 
  TH1I* RMS_PedSub = new TH1I("RMS_PedSub","RMS of PedSubADCs",160,-0.5,15.5); 
  TH1I* RMS_LCMS = new TH1I("RMS_LCMS","RMS of LCMSADCs",160,-0.5,15.5); 
  
  TH1I* mean_PedSub_ref = new TH1I("mean_PedSub_ref","Mean of PedSubADCs",210,-10.5,10.5); 
  TH1I* mean_LCMS_ref = new TH1I("mean_LCMS_ref","Mean of LCMSADCs",210,-10.5,10.5); 
  TH1I* RMS_PedSub_ref = new TH1I("RMS_PedSub_ref","RMS of PedSubADCs",160,-0.5,15.5); 
  TH1I* RMS_LCMS_ref = new TH1I("RMS_LCMS_ref","RMS of LCMSADCs",160,-0.5,15.5); 

  THStack* mean_PedSub_stack = new THStack("mean_PedSub_stack","Mean of PedSubADCs;Mean of PedSubADCs [ADC];Channels []");
  THStack* mean_LCMS_stack = new THStack("mean_LCMS_stack","Mean of LCMSADCs;Mean of LCMSADCs [ADC];Channels []");
  THStack* RMS_PedSub_stack = new THStack("RMS_PedSub_stack","RMS of PedSubADCs;RMS of PedSubADCs [ADC];Channels []");
  THStack* RMS_LCMS_stack = new THStack("RMS_LCMS_stack","RMS of LCMSADCs;RMS of LCMSADCs [ADC];Channels []");
  
  mean_PedSub->SetLineColor(kBlue);
  mean_LCMS->SetLineColor(kBlue);
  RMS_PedSub->SetLineColor(kBlue);
  RMS_LCMS->SetLineColor(kBlue);

  mean_PedSub_ref->SetLineColor(kRed);
  mean_LCMS_ref->SetLineColor(kRed);
  RMS_PedSub_ref->SetLineColor(kRed);
  RMS_LCMS_ref->SetLineColor(kRed);
  
  TString loc_PedSub("");
  TString loc_LCMS("");
  
  for(Int_t i(0); i< 150; i++){
    loc_PedSub = "NoiseReader/PedSub";
    loc_LCMS = "NoiseReader/";
    if(i < 10){
      loc_PedSub += "00";
      loc_LCMS += "00";
    }else if(i < 100){
      loc_PedSub += "0";
      loc_LCMS += "0";
    }
    loc_PedSub += i;
    loc_LCMS += i;
    loc_PedSub += "prof";
    loc_LCMS += "prof";
    
    if(file_Monitor->Get(loc_PedSub))
      GetMean( mean_PedSub, (TProfile*)file_Monitor->Get(loc_PedSub) );
    if(file_Monitor->Get(loc_LCMS))
      GetMean( mean_LCMS, (TProfile*)file_Monitor->Get(loc_LCMS) );
    if(file_Monitor->Get(loc_PedSub))
      GetRMS( RMS_PedSub, (TProfile*)file_Monitor->Get(loc_PedSub) );
    if(file_Monitor->Get(loc_LCMS))
      GetRMS( RMS_LCMS, (TProfile*)file_Monitor->Get(loc_LCMS) );
    
    if(file_Monitor_ref->Get(loc_PedSub))
      GetMean( mean_PedSub_ref, (TProfile*)file_Monitor_ref->Get(loc_PedSub) );
    if(file_Monitor_ref->Get(loc_LCMS))
      GetMean( mean_LCMS_ref, (TProfile*)file_Monitor_ref->Get(loc_LCMS) );
    if(file_Monitor_ref->Get(loc_PedSub))
      GetRMS( RMS_PedSub_ref, (TProfile*)file_Monitor_ref->Get(loc_PedSub) );
    if(file_Monitor_ref->Get(loc_LCMS))
      GetRMS( RMS_LCMS_ref, (TProfile*)file_Monitor_ref->Get(loc_LCMS) );
  }

  mean_PedSub_stack->Add(mean_PedSub,"sames");
  mean_PedSub_stack->Add(mean_PedSub_ref,"sames");
  mean_LCMS_stack->Add(mean_LCMS,"sames");
  mean_LCMS_stack->Add(mean_LCMS_ref,"sames");
  RMS_PedSub_stack->Add(RMS_PedSub,"sames");
  RMS_PedSub_stack->Add(RMS_PedSub_ref,"sames");
  RMS_LCMS_stack->Add(RMS_LCMS,"sames");
  RMS_LCMS_stack->Add(RMS_LCMS_ref,"sames");
  
  TCanvas* canvas = new TCanvas("canvas","canvas",1200,800);
  canvas->Divide(2,2);
  
  TPaveStats *pstats;
  
  canvas->cd(1);
  mean_PedSub_stack->Draw("nostack");
  mean_PedSub->SetMinimum(0.4);
  mean_PedSub->SetLineColor(kBlue);
  mean_PedSub_ref->SetLineColor(kRed);
  canvas->Update();
  pstats = (TPaveStats*)mean_PedSub->GetFunction("stats");
  pstats->SetTextColor(kBlue);
  pstats->SetLineColor(kBlue);
  canvas->Update();
  pstats = (TPaveStats*)mean_PedSub_ref->GetFunction("stats");
  pstats->SetTextColor(kRed);
  pstats->SetLineColor(kRed);
  pstats->SetY1NDC(0.485);
  pstats->SetY2NDC(0.665);
  gPad->SetLogy();

  canvas->cd(2);
  mean_LCMS_stack->Draw("nostack");
  mean_LCMS->SetMinimum(0.4);
  mean_LCMS->SetLineColor(kBlue);
  mean_LCMS_ref->SetLineColor(kRed);
  canvas->Update();
  pstats = (TPaveStats*)mean_LCMS->GetFunction("stats");
  pstats->SetTextColor(kBlue);
  pstats->SetLineColor(kBlue);
  canvas->Update();
  pstats = (TPaveStats*)mean_LCMS_ref->GetFunction("stats");
  pstats->SetTextColor(kRed);
  pstats->SetLineColor(kRed);
  pstats->SetY1NDC(0.485);
  pstats->SetY2NDC(0.665);
  gPad->SetLogy();
  
  canvas->cd(3);
  RMS_PedSub_stack->Draw("nostack");
  RMS_PedSub->SetMinimum(0.4);
  RMS_PedSub->SetLineColor(kBlue);
  RMS_PedSub_ref->SetLineColor(kRed);
  canvas->Update();
  pstats = (TPaveStats*)RMS_PedSub->GetFunction("stats");
  pstats->SetTextColor(kBlue);
  pstats->SetLineColor(kBlue);
  canvas->Update();
  pstats = (TPaveStats*)RMS_PedSub_ref->GetFunction("stats");
  pstats->SetTextColor(kRed);
  pstats->SetLineColor(kRed);
  pstats->SetY1NDC(0.485);
  pstats->SetY2NDC(0.665);
  gPad->SetLogy();

  canvas->cd(4);
  RMS_LCMS_stack->Draw("nostack");
  RMS_LCMS->SetMinimum(0.4);
  RMS_LCMS->SetLineColor(kBlue);
  RMS_LCMS_ref->SetLineColor(kRed);
  canvas->Update();
  pstats = (TPaveStats*)RMS_LCMS->GetFunction("stats");
  pstats->SetTextColor(kBlue);
  pstats->SetLineColor(kBlue);
  canvas->Update();
  pstats = (TPaveStats*)RMS_LCMS_ref->GetFunction("stats");
  pstats->SetTextColor(kRed);
  pstats->SetLineColor(kRed);
  pstats->SetY1NDC(0.485);
  pstats->SetY2NDC(0.665);
  gPad->SetLogy();

  if(date2 == ""){
    canvas->SaveAs("Plots/"+date+"-r"+run+"-"+dettype+"-Monitor-"+date_ref+"-r"+run_ref+".png");
    canvas->SaveAs("Plots/"+date+"-r"+run+"-"+dettype+"-Monitor-"+date_ref+"-r"+run_ref+".eps");
  }else{
    canvas->SaveAs("Plots/"+date+"-r"+run+"-"+dettype+"-Monitor-"+date_ref+"-r"+run_ref+"-"+date2+"-r"+run2+"-"+date_ref2+"-r"+run_ref2+".png");
    canvas->SaveAs("Plots/"+date+"-r"+run+"-"+dettype+"-Monitor-"+date_ref+"-r"+run_ref+"-"+date2+"-r"+run2+"-"+date_ref2+"-r"+run_ref2+".eps");
  }
}
	      
int main(int argc, char *argv[])
{
  if(argc == 6)
    MonitorSummaryPlots(argv[1],argv[2],argv[3],argv[4],argv[5]);
  else if(argc == 10)
    MonitorSummaryPlots(argv[1],argv[2],argv[3],argv[4],argv[5],
			argv[6],argv[7],argv[8],argv[9]);
  
  return 1;
}
