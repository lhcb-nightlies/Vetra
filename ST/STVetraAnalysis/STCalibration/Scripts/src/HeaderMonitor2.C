#include "TString.h"
#include "TCut.h"
#include "TH1I.h"
#include "TH2I.h"
#include "TH2F.h"
#include "TProfile.h"
#include "THStack.h"
#include "TPaveStats.h"
#include "TLegend.h"
#include "TF1.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TPad.h"

#include "CalibNameHandler.C"

#include <iostream>
#include <vector>
#include <string>

using namespace std;

void HeaderMonitor2( TString fullcalibname ){
  
  TCut default_cut("");
  TString filename_Tuple("$MYRESULTS/rootfiles/"+fullcalibname+"-HeaderTuple.root");
  TString treename("HeaderTuple/ITHeader");

  Int_t nbtell1(42);
  Long_t nbchannelpersector(384);
  
  if(filename_Tuple.Contains("TT")){
    treename.ReplaceAll("IT","TT");
    nbtell1 = 48;
    nbchannelpersector = 512;
  }
  
  gROOT->SetStyle("Plain");
  //gStyle->SetOptStat(0);
  gStyle->SetTitleX(0.523);
  gStyle->SetTitleAlign(23); 
  gStyle->SetTitleBorderSize(0);
  gStyle->SetPaintTextFormat("5.1f");
  //gStyle->SetStatFontSize(0.15);
  gStyle->SetStatY(0.865);
  gStyle->SetStatX(0.92);
  gStyle->SetStatH(0.15);
  gStyle->SetOptStat(111110);
  gStyle->SetTitleFontSize(0.07);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetPadRightMargin(0.05);
  gStyle->SetPadBottomMargin(0.09);
  
  UInt_t nColors=52;
  Int_t MyPalette[52];
  Double_t s[7] = {0.00, 0.30, 0.40, 0.50, 0.60, 0.70, 1.00};
  Double_t r[7] = {0.40, 1.00, 1.00, 1.00, 1.00, 0.20, 0.00};
  Double_t g[7] = {0.00, 0.20, 1.00, 1.00, 1.00, 0.20, 0.00};
  Double_t b[7] = {0.00, 0.20, 1.00, 1.00, 1.00, 1.00, 0.40};
  Int_t FI = TColor::CreateGradientColorTable(7, s, r, g, b, nColors);
  for (unsigned int k(0); k < nColors; k++){
    MyPalette[k] = FI+k;
  }
  gStyle->SetNumberContours(nColors);
  gStyle->SetPalette(nColors, MyPalette);  
  
  TFile* file_Tuple = new TFile(filename_Tuple.Data());
  TTree* tree_Tuple = (TTree*)file_Tuple->Get(treename);
  
  TH1I* h_pedestal_000 = new TH1I("h_pedestal_000","000",60,110,170); 
  TH1I* h_pedestal_001 = new TH1I("h_pedestal_001","001",60,110,170); 
  TH1I* h_pedestal_010 = new TH1I("h_pedestal_010","010",60,110,170); 
  TH1I* h_pedestal_011 = new TH1I("h_pedestal_011","011",60,110,170); 
  TH1I* h_pedestal_100 = new TH1I("h_pedestal_100","100",60,110,170); 
  TH1I* h_pedestal_101 = new TH1I("h_pedestal_101","101",60,110,170); 
  TH1I* h_pedestal_110 = new TH1I("h_pedestal_110","110",60,110,170); 
  TH1I* h_pedestal_111 = new TH1I("h_pedestal_111","111",60,110,170); 
  
  TH1I* h_pedestal_header0_diff = new TH1I("h_pedestal_header0_diff","header_0 : 1-0",200,-100.,100.);

  TString tmp;

  vector<int> channelvec(32,0);

  int header_0,header_1,header_2;

  tree_Tuple->SetBranchAddress( "header_0" , &header_0 );
  tree_Tuple->SetBranchAddress( "header_1" , &header_1 );
  tree_Tuple->SetBranchAddress( "header_2" , &header_2 );
  
  tree_Tuple->SetBranchAddress( "channel_6" , &channelvec[6] );
  tree_Tuple->SetBranchAddress( "channel_7" , &channelvec[7] );
  tree_Tuple->SetBranchAddress( "channel_8" , &channelvec[8] );
  tree_Tuple->SetBranchAddress( "channel_9" , &channelvec[9] );
  tree_Tuple->SetBranchAddress( "channel_10" , &channelvec[10] );
  tree_Tuple->SetBranchAddress( "channel_11" , &channelvec[11] );
  tree_Tuple->SetBranchAddress( "channel_12" , &channelvec[12] );
  tree_Tuple->SetBranchAddress( "channel_13" , &channelvec[13] );
  tree_Tuple->SetBranchAddress( "channel_14" , &channelvec[14] );
  tree_Tuple->SetBranchAddress( "channel_15" , &channelvec[15] );
  tree_Tuple->SetBranchAddress( "channel_16" , &channelvec[16] );
  tree_Tuple->SetBranchAddress( "channel_17" , &channelvec[17] );
  tree_Tuple->SetBranchAddress( "channel_18" , &channelvec[18] );
  tree_Tuple->SetBranchAddress( "channel_19" , &channelvec[19] );
  tree_Tuple->SetBranchAddress( "channel_20" , &channelvec[20] );
  tree_Tuple->SetBranchAddress( "channel_21" , &channelvec[21] );
  tree_Tuple->SetBranchAddress( "channel_22" , &channelvec[22] );
  tree_Tuple->SetBranchAddress( "channel_23" , &channelvec[23] );
  tree_Tuple->SetBranchAddress( "channel_24" , &channelvec[24] );
  tree_Tuple->SetBranchAddress( "channel_25" , &channelvec[25] );
  tree_Tuple->SetBranchAddress( "channel_26" , &channelvec[26] );
  tree_Tuple->SetBranchAddress( "channel_27" , &channelvec[27] );
  tree_Tuple->SetBranchAddress( "channel_28" , &channelvec[28] );
  tree_Tuple->SetBranchAddress( "channel_29" , &channelvec[29] );
  tree_Tuple->SetBranchAddress( "channel_30" , &channelvec[30] );
  tree_Tuple->SetBranchAddress( "channel_31" , &channelvec[31] );

  Long64_t tree_nentries = tree_Tuple->GetEntriesFast();

  Double_t pedestal_header0_0, pedestal_header0_1;
  Double_t pedestal_header0_diff;
  Int_t n_header0_0,n_header0_1;
  
  for(Long64_t ientry=0; ientry<tree_nentries;ientry++) {
    tree_Tuple->GetEntry(ientry);
    if(ientry%100000==0)
      cout << ientry << endl;

    n_header0_0 = 0;
    n_header0_1 = 0;
    pedestal_header0_0 = 0.;
    pedestal_header0_1 = 0.;
    pedestal_header0_diff = 0.;

    for(UInt_t chan(6);chan<32u;chan++){
      if(header_2<128 && header_1<128 && header_0<128){
	h_pedestal_000->Fill(channelvec[chan]);
	if(channelvec[chan]>110 && channelvec[chan]<160){
	  pedestal_header0_0 += channelvec[chan];
	  n_header0_0++;
	}
      }else if(header_2<128 && header_1<128 && header_0>128){
	h_pedestal_001->Fill(channelvec[chan]);
	if(channelvec[chan]>110 && channelvec[chan]<160){
	  pedestal_header0_1 += channelvec[chan];
	  n_header0_1++;
	}
      }else if(header_2<128 && header_1>128 && header_0<128){
	h_pedestal_010->Fill(channelvec[chan]);
	if(channelvec[chan]>110 && channelvec[chan]<160){
	  pedestal_header0_0 += channelvec[chan];
	  n_header0_0++;
	}
      }else if(header_2<128 && header_1>128 && header_0>128){
	h_pedestal_011->Fill(channelvec[chan]);
	if(channelvec[chan]>110 && channelvec[chan]<160){
	  pedestal_header0_1 += channelvec[chan];
	  n_header0_1++;
	}
      }else if(header_2>128 && header_1<128 && header_0<128){
	h_pedestal_100->Fill(channelvec[chan]);
	if(channelvec[chan]>110 && channelvec[chan]<160){
	  pedestal_header0_0 += channelvec[chan];
	  n_header0_0++;
	}
      }else if(header_2>128 && header_1<128 && header_0>128){
	h_pedestal_101->Fill(channelvec[chan]);
	if(channelvec[chan]>110 && channelvec[chan]<160){
	  pedestal_header0_1 += channelvec[chan];
	  n_header0_1++;
	}
      }else if(header_2>128 && header_1>128 && header_0<128){
	h_pedestal_110->Fill(channelvec[chan]);
	if(channelvec[chan]>110 && channelvec[chan]<160){
	  pedestal_header0_0 += channelvec[chan];
	  n_header0_0++;
	}
      }else if(header_2>128 && header_1>128 && header_0>128){
	h_pedestal_111->Fill(channelvec[chan]);
	if(channelvec[chan]>110 && channelvec[chan]<160){
	  pedestal_header0_1 += channelvec[chan];
	  n_header0_1++;
	}
      }
      if(n_header0_0>0)
	pedestal_header0_0 /= n_header0_0;
      if(n_header0_1>0)
	pedestal_header0_1 /= n_header0_1;

      pedestal_header0_diff = pedestal_header0_1-pedestal_header0_0;

      h_pedestal_header0_diff->Fill(pedestal_header0_diff);
      
    }
  }

  TCanvas* canvas = new TCanvas("canvas","canvas",1200,800);
  canvas->Divide(3,3);

  canvas->cd(1);
  h_pedestal_000->Draw();
  canvas->cd(2);
  h_pedestal_001->Draw();
  canvas->cd(3);
  h_pedestal_010->Draw();
  canvas->cd(4);
  h_pedestal_011->Draw();
  canvas->cd(5);
  h_pedestal_100->Draw();
  canvas->cd(6);
  h_pedestal_101->Draw();
  canvas->cd(7);
  h_pedestal_110->Draw();
  canvas->cd(8);
  h_pedestal_111->Draw();
  canvas->cd(9);
  h_pedestal_header0_diff->Draw();
  canvas->SaveAs("test.eps");
  
}

int main(int argc, char *argv[])
{
  if(argc == 2){
    CalibNameHandler Calib(argv[1]);
    HeaderMonitor2(Calib.GetFileCalibName());
  }
  return 1;
}
