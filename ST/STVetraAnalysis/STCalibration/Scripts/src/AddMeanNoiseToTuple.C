#include "TString.h"
#include "TCut.h"
#include "TH1I.h"
#include "TH2I.h"
#include "TProfile.h"
#include "THStack.h"
#include "TPaveStats.h"
#include "TLegend.h"
#include "TF1.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLeaf.h"

#include <iostream>
#include <vector>
#include <string>

#include "CalibNameHandler.C"

using namespace std;

void AddMeanToTTree( TTree* tree, TFile* file_Noise ){

  cout << "Adding mean to TTree\n";

  Int_t pedestal, channel, tell1;
  Float_t raw_mean, pedsub_mean, cms_mean;
  Float_t pedsub_rms, cms_rms;

  TBranch *branch_raw_mean = tree->Branch("raw_mean", &raw_mean, "raw_mean/F");
  TBranch *branch_pedsub_mean = tree->Branch("pedsub_mean", &pedsub_mean, "pedsub_mean/F");
  TBranch *branch_cms_mean = tree->Branch("cms_mean", &cms_mean, "cms_mean/F");
  TBranch *branch_pedsub_rms = tree->Branch("pedsub_rms", &pedsub_rms, "pedsub_rms/F");
  TBranch *branch_cms_rms = tree->Branch("cms_rms", &cms_rms, "cms_rms/F");
  tree->SetBranchAddress("pedestal", &pedestal);
  tree->SetBranchAddress("channel", &channel);
  tree->SetBranchAddress("tell1", &tell1);

  Long64_t tree_nentries = tree->GetEntriesFast();

  TString loc_PedSub("");
  TString loc_LCMS("");

  for(Long64_t ientry=0; ientry<tree_nentries;ientry++) {
    tree->GetEntry(ientry);

    loc_PedSub = "NoiseReader/PedSub";
    loc_LCMS = "NoiseReader/";
    if(tell1 < 10){
      loc_PedSub += "00";
      loc_LCMS += "00";
    }else if(tell1 < 100){
      loc_PedSub += "0";
      loc_LCMS += "0";
    }
    loc_PedSub += tell1;
    loc_LCMS += tell1;
    loc_PedSub += "prof";
    loc_LCMS += "prof";

    pedsub_mean = ((TProfile*)file_Noise->Get(loc_PedSub))->GetBinContent(channel+1);
    cms_mean = ((TProfile*)file_Noise->Get(loc_LCMS))->GetBinContent(channel+1);
    raw_mean = pedsub_mean + pedestal;
    pedsub_rms = ((TProfile*)file_Noise->Get(loc_PedSub))->GetBinError(channel+1);
    cms_rms = ((TProfile*)file_Noise->Get(loc_LCMS))->GetBinError(channel+1);

    branch_raw_mean->Fill();
    branch_pedsub_mean->Fill();
    branch_cms_mean->Fill();
    branch_pedsub_rms->Fill();
    branch_cms_rms->Fill();
  }
  tree->Write();
}

void AddMeanNoiseToTuple( CalibNameHandler const& Calib ){
  
  TString filename_Noise("$MYRESULTS/rootfiles/"+Calib.GetFileCalibName()+"-Noise.root");
  TString filename_Tuple("$MYRESULTS/rootfiles/"+Calib.GetFileCalibName()+"-Tuple.root");
  TString treename("CalibrationTuple/ITCalibration");

  cout << "Input noise file: " << filename_Noise.Data() << "\n";
  cout << "Output tuple: " << filename_Tuple.Data() << "\n";
    
  if(Calib.GetDetType() == "TT"){
    treename.ReplaceAll("IT","TT");
  }

  TFile* file_Noise = new TFile(filename_Noise.Data());
  TFile* file_Tuple = new TFile(filename_Tuple.Data());

  if(!(TTree*)file_Tuple->Get(treename)){
    cout << "WARNING: " << treename.Data() << " doesn't exist! Trying different path...\n";
    treename.ReplaceAll("CalibrationTuple/","");
  }
  
  if(!(TTree*)file_Tuple->Get(treename)){
    cout << "ERROR: " << treename.Data() << " doesn't exist as well! Please check your code...\n";
    exit(1);
  }
  else{
    cout << "Ok, " << treename.Data() << " found\n";
  }

  TTree* tree_Tuple_ = (TTree*)file_Tuple->Get(treename);

  TFile* file = new TFile("$MYRESULTS/rootfiles/"+Calib.GetFileCalibName()+"-Tuple_.root","RECREATE");
  
  TTree* tree_Tuple = tree_Tuple_->CloneTree();
  
  AddMeanToTTree( tree_Tuple, file_Noise );
  
  file->Close();
  
  gSystem->Exec("mv $MYRESULTS/rootfiles/"+Calib.GetFileCalibName()+"-Tuple_.root "+filename_Tuple);
}

int main(int argc, char *argv[])
{
  if(argc == 2){
    CalibNameHandler Calib(argv[1]);
    AddMeanNoiseToTuple(Calib);
  }else if(argc == 4){
    CalibNameHandler Calib(argv[1],argv[2],argv[3]);
    AddMeanNoiseToTuple(Calib);
  }
  
  return 1;
}
