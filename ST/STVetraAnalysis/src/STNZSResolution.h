// $Id: STNZSResolution.h,v 1.4 2009/12/05 23:16:55 jvantilb Exp $
#ifndef STNZSResolution_H
#define STNZSResolution_H 1

// from STKernel
//#include "Kernel/STHistoAlgBase.h"
#include "Kernel/STTupleAlgBase.h"

#include "Kernel/ITrajPoca.h"


#include "GaudiKernel/IMagneticFieldSvc.h"

#include "TrackInterfaces/ITrackExtrapolator.h"

/** @class STNZSResolution STNZSResolution.h
 *
 *  Class for 
 *  
 *  A presentation on this algorithm and the results was giving on 20.10.2009 in
 *  the ST TED run analysis meeting by Helge: http://indico.cern.ch/event/71185 
 *
 *  @author J. van Tilburg
 *  @date   22/10/2009
 */

class STNZSResolution : public ST::TupleAlgBase {

public:
 
  /// Standard constructer
  STNZSResolution( const std::string& name, ISvcLocator *svcloc );

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

private:

  unsigned int nHitsInWindow(const LHCb::STClusters* clusters,
                             const LHCb::STChannelID channelID);

  unsigned int distanceToNextCluster(const LHCb::STClusters* clusters,
                                     const LHCb::STChannelID channelID);

  // internal event counter
  int                 m_evtNumber;

  ITrajPoca*    m_poca;

  double m_clusteringCut;
  
  std::string m_clusterLocation;
  
  ITrackExtrapolator* m_extrapolator;
  
  IMagneticFieldSvc* m_pIMF;

  // jobOptions:

  /// Location in the Event Data Store with the ADC values
  std::string m_dataLocation;

  /// Location in the Event Data Store with the STClusters
  std::string m_trackLocation;

  /// List to the spills to loop over. E.g. "Next1", "Central", "Prev2", etc.
  std::vector<std::string> m_spills;

  /// Cut on the bunch ID (distinguish TED from cosmics)
  std::vector<unsigned int> m_bunchID;

  bool m_useNZSdata;     ///< Flag to use either NZS or ZS data
  double m_chargeCut;    ///< Cut on the total charge of the STClusters
  bool m_skipShortThick; ///< Skip short and thick ladders (for IT only)
  /// List of directory names to subidvide the sectors in the service box.
  /// Possible names are "all", "sector", "type".
  std::vector<std::string> m_dirNames;

};

#endif // STNZSResolution_H
