// $Id: STMCThresholds.cpp,v 1.3 2010-04-20 08:34:29 mtobin Exp $

// Gaudi
#include "GaudiKernel/AlgFactory.h"

// LHCbKernel includes
#include "Kernel/ISTReadoutTool.h"
#include "Kernel/STTell1Board.h"
#include "Kernel/STDetSwitch.h"


// local
#include "STMCThresholds.h"

using namespace LHCb;

DECLARE_ALGORITHM_FACTORY( STMCThresholds )

STMCThresholds::STMCThresholds( const std::string& name,
                                    ISvcLocator* pSvcLocator):
  GaudiAlgorithm(name, pSvcLocator),
  m_readoutTool(0)

{
  declareProperty("detType", m_detType = "TT" );
}

STMCThresholds::~STMCThresholds()
{
  // STMCThresholds destructor
}

StatusCode STMCThresholds::initialize()
{
  StatusCode sc = GaudiAlgorithm::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);

  STDetSwitch::flip(m_detType,m_readoutToolName);
  
  // readout tool
  m_readoutTool = tool<ISTReadoutTool>(m_readoutToolName,m_readoutToolName);
 
  return StatusCode::SUCCESS;
}


StatusCode STMCThresholds::execute()
{
  return StatusCode::SUCCESS;
}

StatusCode STMCThresholds::finalize()
{

  // init the map
//   unsigned int nBoard = m_readoutTool->nBoard();
//   for (unsigned int iVal = 0; iVal<nBoard; ++iVal ){

//     STTell1Board* aBoard = m_readoutTool->findByOrder(iVal);
//     // get the sectors involved...
//     const std::vector<LHCb::STChannelID>& vec = aBoard->sectorIDs();
//     std::vector<LHCb::STChannelID>::const_iterator iter = vec.begin();
//     for (; iter != vec.end(); ++iter){
//       const double noiseValue = m_sigNoiseTool->noiseInADC(*iter);
//     } // iter sectors
//   } // boards
 
  return StatusCode::SUCCESS;
}


