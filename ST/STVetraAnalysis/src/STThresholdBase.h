// $Id: STThresholdBase.h,v 1.14 2010-04-06 09:07:14 jluisier Exp $
#ifndef STTHRESHOLDBASE_H 
#define STTHRESHOLDBASE_H 1

// Gaudi
#include "GaudiKernel/AlgFactory.h"

// Include files
#include "Kernel/STHistoAlgBase.h"

// Reading TELL1 data
#include "Event/STTELL1Data.h"

// Converting string <-> numbers
#include "Kernel/STLexicalCaster.h"

// Output file and cosmetics
#include <fstream>
#include <iomanip>
#include <cmath>

// Char -> bit conversion
#include <bitset>

/** @class STThresholdBase STThresholdBase.h
 *
 * Base class to study noise, header crosstalk and cluster rate in ST TELL1s
 * This class isn't intended to be instanciated
 *
 *  @author Johan Luisier
 *  @date   2008-01-10
 */

class Condition;
class ISTVetraCfg;

class STThresholdBase : public ST::HistoAlgBase
{
public: 
  /// Standard constructor
  STThresholdBase( const std::string& name, ISvcLocator* pSvcLocator );
  
  virtual ~STThresholdBase( ); ///< Destructor
  
  virtual StatusCode initialize() override;    ///< Algorithm initialization
  //virtual StatusCode execute   ();    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization
  
protected:

  /**
   * Fills the EnabledLinks and ManageTELL1
   * maps, who contain all info on which
   * TELL1 are present and which link is
   * disabled.
   * @return StatusCode
   */
  StatusCode  initHistograms(); 
  
  /**
   * Check if links are disabled.
   * @param TELL1 source ID of TELL1 board (unsigned int)
   * @param FPGA PP number from 0 to 6 (unsigned int)
   * @param Lnk status of links in a PP (unsigned char)
   * @return Status Code
   */
  StatusCode CheckChannels(unsigned int& TELL1, unsigned int& FPGA,
                           unsigned char& Lnk);
  
  /**
   * Reads the CondDB and fills the Condition container.
   * @return Status Code
   */
  StatusCode GetConditions();
  
  /**
   * Contain true if the channel is enable
   */
  std::map<unsigned int, bool> EnabledLinks; 

  /**
   * Contain true if the TELL1 is enable
   */
  std::map<unsigned int, bool> EnabledTELL1s; 

  /**
   * Map the TELL1 number and the internal ID.
   */
  std::map<unsigned int, unsigned int> ManageTELL1s; 

  /**
   * Output filename
   */
  std::string m_FileName;

  /**
   * Location of the read data
   */
  std::string m_DataLoc;

  /**
   * Location of EventInfo
   */
  std::string m_eventInfoLoc;

  /**
   * Input data (optimization)
   */
  LHCb::STTELL1Datas* m_Datas;

  /**
   * Input event info (optimization)
   */
  LHCb::STTELL1EventInfos* m_Infos;

  /**
   * Source IDs of excluded TELL1s
   */
  std::vector<unsigned int> m_UnwantedTELL1s;

  /**
   * Number of bins
   */
  int nbr_bin;

  /**
   * Upper and lower bins of histograms
   */
  double lower_bin, upper_bin;

  /**
   *  Needed for pedestal convergence, the algorithm does nothing till this
   * number of processed event is reached
   */
  unsigned int m_WaitingTill;

  /**
   * Event number counter
   */
  unsigned int counter;

  /**
   * Stores information about initialisation.
   */
  bool initDone;

  /**
   * Number of TELL1s present in the datafile and total number of enabled
   * links.
   */
  unsigned int NumberOfTELL1s, NumberOfUsedLinks;

  /**
   * Toggles On / Off file prodution
   */
  bool m_ProduceFile;

  /**
   * Strings used for optimization purpose.
   */
  std::string histoName, histoTitle;

  /**
   * Mapping between sourceIDs and TELL number
   */
  const std::map<unsigned int, unsigned int>* TELL1Mapping;
  
  /**
   * Path to the Condition.
   */
  std::string m_ConditionLocation;

  /**
   * Stores the different Conditions.
   */
  std::vector<Condition*> m_Cond;

  /**
   * Tries to get the data
   * @param m_input data container.
   * @param m_loc location of container in TES.
   * @return StatusCode \e FAILURE if location doesn't exist in TES\n
   * \e RECOVERABLE if location exists but container is empty\n
   * \e SUCCESS if location exists and container has some data.
   */
  StatusCode getDatas(LHCb::STTELL1Datas*& m_input, const std::string& m_loc);

  /**
   * Tries to get the event info.
   * @param m_input data container.
   * @param m_loc location of container in TES.
   * @return StatusCode \e FAILURE if location doesn't exist in TES\n
   * \e RECOVERABLE if location exists but container is empty\n
   * \e SUCCESS if location exists and container has some data.
   */
  StatusCode getEventInfo(LHCb::STTELL1EventInfos*& m_input, const std::string& m_loc);
  
  /**
   * Return the TELL1 number
   * @param sourceID source ID of the TELL1 board
   * @return the TELL1 number
   */
  unsigned int boardAlias(const unsigned int& sourceID) const;

  /**
   * Vetra cond DB readout tool
   */
  ISTVetraCfg* m_stVetraCfg;

  /**
   * Vetra cond DB readout tool name
   */
  std::string m_stVetraToolName;

  unsigned int m_NbrBoardsInDet;
private:
};

#endif // STTHRESHOLDBASE_H
