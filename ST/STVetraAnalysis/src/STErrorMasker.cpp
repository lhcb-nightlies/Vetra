// $Id: STErrorMasker.cpp,v 1.16 2009-11-19 18:18:00 szumlat Exp $
// Include files 

// local
#include "STErrorMasker.h"
#include "Kernel/BeetleRepresentation.h"
#include "Kernel/ISTReadoutTool.h"
#include "Kernel/STTell1Board.h"

#include <algorithm>

using namespace std;
using namespace LHCb;
using namespace ST;
using namespace AIDA;

//-----------------------------------------------------------------------------
// Implementation file for class : STErrorMasker
//
// 2008-07-03 : Johan Luisier
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STErrorMasker )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STErrorMasker::STErrorMasker( const std::string& name,
                              ISvcLocator* pSvcLocator)
  : STThresholdBase ( name , pSvcLocator ),
    errorNbr(0)
{
  declareProperty("RunOnNZS"     , m_RunOnNZS      = true);
  declareProperty("FullDetails"  , m_FullDetails   = true);
  declareProperty("BoardAlias"   , m_BoardAlias    = false);
  declareProperty("FullNames"    , m_FullNames     = false);

  // ST properties
  declareSTConfigProperty("ErrorLocation", m_ErrorLocation, "Raw/IT/ErrorTELL1");
}
//=============================================================================
// Destructor
//=============================================================================
STErrorMasker::~STErrorMasker()
{} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode STErrorMasker::initialize()
{
  StatusCode sc(STThresholdBase::initialize()); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by STThresholdBase
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  if (m_RunOnNZS)
    info() << "Errors will be written in " << m_ErrorLocation << endmsg;
  else
    info() << "Errors will be retrieved from " << m_ErrorLocation << endmsg;

  if (m_FullNames)
  {
    m_FullDetails = true;
    setForcedInit();
  }
  

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode STErrorMasker::execute()
{
  StatusCode sc(SUCCESS);

  counter++;
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  if (m_RunOnNZS)
  {
    sc = generateErrorInfo();
    if ( sc.isRecoverable() )
      return StatusCode::SUCCESS;
    else if (sc.isFailure())
      return sc;
  }

  if (!exist<STTELL1BoardErrorBanks>(m_ErrorLocation))
  {
    verbose() << "No error bank at iteration " << counter << endmsg;
    return StatusCode::SUCCESS;
  }
  else
    errorNbr++;

  m_Errors = get<STTELL1BoardErrorBanks>(m_ErrorLocation);
  
  STTELL1BoardErrorBanks::const_iterator it, Begin(m_Errors -> begin()),
    End(m_Errors -> end());

  vector<STTELL1Error*>::const_iterator errorIt, errorBegin, errorEnd;

  unsigned int port, strip, beetle, PCN, PCNV, TELL1, PP;

  STTELL1Error::FailureMode tmp;
  
  for (it = Begin; it != End; it++)
  {
    TELL1 = (*it) -> key();

    debug() << "Error with key " << TELL1 << endmsg;

    errorBegin = ((*it) -> errorInfo()).begin();
    errorEnd   = ((*it) -> errorInfo()).end();

    for (errorIt = errorBegin; errorIt != errorEnd; errorIt++)
    {
      PP = static_cast<unsigned int>(errorIt - errorBegin);
      for (beetle = 0; beetle < 6; beetle++)
      {
        PCN  = (*errorIt) -> findPCN(beetle);
        PCNV = (*errorIt) -> pcnVote();
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "PCN : " << setw(3) << PCN << ", majority voted PCN :"
                    << PCNV << endmsg;

        for (port = 0; port < 4; port++)
        {
          tmp = (*errorIt) -> linkInfo(beetle, port, PCNV);
          histoName = toString(TELL1,3);
          histoTitle = "TELL1 " + toString(TELL1);
          for (strip = 0; strip < 32; strip++)
            plot2D((beetle + 6 * PP) * 128 + strip + 32 * port,
                   static_cast<unsigned int>(tmp),
                   histoName, histoTitle,
                   -.5, 3071.5, // x bins
                   -.5, 9.5,    // y bins
                   96, 10);    // binning
        }
      }
    }    
  }

  return sc;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode STErrorMasker::finalize()
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  string ErrorNames[10] = {
    "None", "CorruptedBank", "OptLnkDisabled", "TlkLinkLoss",
    "OptLinkNoClock", "SyncRAMFull", "SyncEvtSize",
    "OptLinkNoEvent", "PseudoHeader", "WrongPCN"
  };

  const Histo2DMapID& histos(histo2DMapID());

  Histo2DMapID::const_iterator histoIt, histoBegin(histos.begin()),
    histoEnd(histos.end());

  IHistogram2D *histo;

  string litID;

  unsigned int xbin, ybin, port, link, TELL1;

  vector<unsigned int> values, temp_values;

  vector< vector<unsigned int> > LinksAndPorts;

  vector<bool> FaultyPorts;

  ofstream out(m_FileName.c_str());

  vector<unsigned int>::const_iterator vecBegin(m_UnwantedTELL1s.begin()),
    vecEnd(m_UnwantedTELL1s.end());
  
  if (out.fail())
    return Error("Cannot open " + m_FileName + " for writing");

  vector<unsigned int> PseudoMap;
  
  for (histoIt = histoBegin; histoIt != histoEnd; histoIt++)
  { 
    litID = histoIt -> first;
    
    histo =  histoIt -> second;
    
    if (histo == 0)
      continue;
    
    fromString(litID, TELL1);
    
    PseudoMap.push_back(TELL1);
  }
  
  sort(PseudoMap.begin(), PseudoMap.end());
  
  vector<unsigned int>::const_iterator mapIt, mapBegin(PseudoMap.begin()),
    mapEnd(PseudoMap.end());
  
  for (mapIt = mapBegin; mapIt != mapEnd; mapIt++)
  {
    TELL1 = *mapIt;

    if (vecEnd != vecBegin && find(vecBegin, vecEnd, TELL1) != vecEnd)
    {
      if ( msgLevel(MSG::DEBUG) )
        debug() << "Skipping TELL1 " << TELL1 << endmsg;
      continue;
    }

    litID = toString(TELL1,3);

    histo = histos[litID];

    info() << setfill('*') << setw(30) << '*' << setfill(' ') << endmsg;
     
    info() << "TELL1 with sourceID " << TELL1;
    
    if (m_BoardAlias && boardAlias(TELL1))
      info() << " (aka TELL1 " << setw(2) << boardAlias(TELL1) << ")";

    info() << endmsg;

    values.assign(10, 0);

    LinksAndPorts.assign(24, vector<unsigned int>(4));

    out << "TELL1Board" <<  TELL1 << endl << flush;
    out << "<paramVector name=\"pedestal_mask\" type=\"int\""
        << " comment=\"Masks strips to be skipped in PS process\"> " << flush;
    for (xbin = 0; xbin < 96; xbin++)
    {
      temp_values.assign(10, 0);

      link = xbin / 4;
      port = xbin % 4;
      for (ybin = 0; ybin < 10; ybin++)
      {
        values[ybin] +=
          static_cast<unsigned int>(histo -> binEntries(xbin, ybin));
        temp_values[ybin] +=
          static_cast<unsigned int>(histo -> binEntries(xbin, ybin));
      }
      
      if (temp_values[1] + temp_values[2] + temp_values[3] +
          temp_values[4] + temp_values[5] + temp_values[6] +
          temp_values[7] + temp_values[8] + temp_values[9] != 0)
      {
        // STTELL1Error
        out << "1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 " << flush;
        LinksAndPorts[link][port] += 1;
      }
      else
        out << "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 " << flush;
    }
    
    out << "</paramVector>" << endl << flush;

    if (m_RunOnNZS && values[0] == counter * 3072)
      info() << "No error found !" << endmsg;
    
    for (ybin = 1; ybin < 10; ybin++)
    {
      if (values[ybin] != 0)
        info()  << values[ybin] << " errors of type " << ErrorNames[ybin]
                << endmsg;
    }

    if (m_FullDetails)
    {
      for (link = 0; link < 24; link++)
      {
        if (LinksAndPorts[link][0] + LinksAndPorts[link][1] +
            LinksAndPorts[link][2] + LinksAndPorts[link][3] == 0)
          // This link has no error ! Great!!
          continue;
        else if (LinksAndPorts[link][0] + LinksAndPorts[link][1] +
                 LinksAndPorts[link][2] + LinksAndPorts[link][3] == 4)
          // All strips of the link have errors!
        {
          if ( msgLevel(MSG::DEBUG) )
            debug() << LinksAndPorts[link][0] << ' '
                    << LinksAndPorts[link][1] << ' '
                    << LinksAndPorts[link][2] << ' '
                    << LinksAndPorts[link][3] << endmsg;
          info() << fillStream(TELL1, link)
                 << " has errors in every port" << endmsg;
          continue;
        }

        for (port = 0; port < 4; port++)
        {
          if(LinksAndPorts[link][port] == 1)
            info() << fillStream(TELL1, link, port)
                   << " has errors" << endmsg;
        }
        //         for (port = 0; port < 4; port++)
        //         {
        //           if (FaultyPorts[0] * FaultyPorts[1] *
        //               FaultyPorts[2] * FaultyPorts[3])
        //           {
        //             if ( msgLevel(MSG::DEBUG) )
        //               debug() << LinksAndPorts[link][0] << ' '
        //                       << LinksAndPorts[link][1] << ' '
        //                       << LinksAndPorts[link][2] << ' '
        //                       << LinksAndPorts[link][3] << endmsg;
        //             info() << fillStream(TELL1, link)
        //                    << " has some " << endmsg;
        //             break;
        //           }
        //           else if (FaultyPorts[port])
        //           {
        //             if ( msgLevel(MSG::DEBUG) )
        //               debug() << LinksAndPorts[link][0] << ' '
        //                       << LinksAndPorts[link][1] << ' '
        //                       << LinksAndPorts[link][2] << ' '
        //                       << LinksAndPorts[link][3] << endmsg;
        //             info() << fillStream(TELL1, link, port)
        //                    << " has some bad strips" << endmsg;
        //           }
        //         }
      }
    }
  }

  info() << setfill('*') << setw(30) << '*' << setfill(' ') << endmsg;

  out.close();
  
  return STThresholdBase::finalize();  // must be called after all other actions
}


//=============================================================================
// Gets the Error Info from the Event Info, when running on NZS
//=============================================================================
StatusCode STErrorMasker::generateErrorInfo()
{
  StatusCode sc(SUCCESS);
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> generateErrorInfo" << endmsg;
  
  if (!initDone)
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "Finalizing the setting up of histograms" << endmsg;
    // STThresholdBase::initHistograms is called
    sc = initHistograms();
    if (sc.isFailure())
      return sc;
    else if ( sc.isRecoverable() )
    {
      if ( msgLevel(MSG::DEBUG) )
        debug() << "Got an empty container, assuming round robin data"
                << endmsg;
      return StatusCode::SUCCESS;
    }
    if ( initDone && msgLevel( MSG::DEBUG) )
      debug() << "Histograms are finaly set up!" << endmsg;
  }
  
  // First, get the eventinfo
  sc = getEventInfo( m_Infos, m_eventInfoLoc );

  if ( sc.isRecoverable() )
  {
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "No event info found, assuming round robin data" << endmsg;
    return sc;
  }
  else if ( sc.isFailure() )
    return sc;

  STTELL1EventInfos::const_iterator infoIt, infoBegin(m_Infos -> begin()),
    infoEnd(m_Infos -> end());
  
  if ( msgLevel(MSG::DEBUG) )
    debug() << "Number of corresponding event infos : "
            << m_Infos -> size() << endmsg;

  if (static_cast<unsigned int>(infoEnd - infoBegin) % 4 != 0)
    return Error("Event info doesn't have the correct size");

  unsigned int TELL1, PP, word0;
  
  STTELL1EventInfos::const_iterator InfoIt, InfoBegin(m_Infos -> begin()),
    InfoEnd(m_Infos -> end());

  // Stuff needed to write some new container on TES, which will
  // contain errors
  STTELL1BoardErrorBanks* outputErrors;
  outputErrors = new STTELL1BoardErrorBanks();
  put(outputErrors, m_ErrorLocation);
  STTELL1BoardErrorBank* myData;
  myData = 0;
  STTELL1Error* myError;

  for (InfoIt = InfoBegin; InfoIt != InfoEnd; InfoIt++)
  {
    // Check if by chance this TELL1 is unwanted
    TELL1 = (*InfoIt) -> sourceID();

    if ( EnabledTELL1s.find( TELL1 ) == EnabledTELL1s.end() )
      return Warning( "Trying to access an unexisting TELL1 board : "
                      + toString( TELL1 ), SUCCESS );

    if (!EnabledTELL1s[TELL1])
    {
      // If the TELL1 board is unwanted, it is skipped
      if ( msgLevel(MSG::DEBUG) )
        debug() << "Skipping TELL1 " << TELL1 << endmsg;
      InfoIt += 3;
      continue;
    }

    if ( msgLevel(MSG::DEBUG) ) debug() << "Source ID " << TELL1 << endmsg;
    PP = static_cast<unsigned int>(InfoIt - InfoBegin) % 4;

    if (PP == 0)
    {
      myData = new STTELL1BoardErrorBank();
      outputErrors->insert(myData, TELL1);
    }

    word0 = (*InfoIt) -> word0();

    if (word0 > 0x80000000)
      word0 -= 0x80000000;

    myError = new STTELL1Error(word0,
                               (*InfoIt) -> word1(),
                               0,
                               (*InfoIt) -> ProcessInfo() * 0x1000000 +
                               (*InfoIt) -> pcn() * 0x10000,
                               0x148E00);

    myError -> setWord10(36353);
    myError -> setWord11(0x8E02);

    if (myError -> BankList() && 0x8)
      myError -> setWord12(0x8E03);

    if (myError -> BankList() && 0x10)
      myError -> setWord13(0x8E04);
    
    myError -> setOptionalErrorWords((*InfoIt) -> word3(), (*InfoIt) -> word4(),
                                     (*InfoIt) -> word5(), (*InfoIt) -> word6(),
                                     (*InfoIt) -> word7());

    myData -> addToErrorInfo(myError);
  }

  return sc;
}

void STErrorMasker::addTELL1(const unsigned int& TELL1)
{
  map<unsigned int, unsigned int>::const_iterator mapIt,
    mapEnd(ManageTELL1s.end());
  
  mapIt = ManageTELL1s.find(TELL1);
  
  if (mapIt == mapEnd)
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "Add TELL1 " << TELL1 << " to the list" << endmsg;
    ManageTELL1s[TELL1] = NumberOfTELL1s;
    EnabledTELL1s[TELL1] = true;
    NumberOfTELL1s++;
  }
}

/**
 * Produce the output, depending on m_FullNames state. Output can be either :
 * link 0 port 1, or
 * IT1Sector1...
 *
 * @param TELL1 the TELL1 ID
 * @param link the link number (which is the beetle number as well)
 * @param port the port number of the TELL1
 *
 * @return the corresponding string
 */
string STErrorMasker::fillStream(const unsigned int& TELL1, const unsigned int& link, const unsigned int& port) const
{
  string result("");

  if (!m_FullNames)
  {
    switch(port)
    {
    case 4:
      result = "Link " + toString(link);
      break;
    default:
      result = "Port " + toString(port) + " of link " + toString(link);
    }
  }
  else
  {
    STTell1Board* board;
    STChannelID channelID;
    STDAQ::BeetleRepresentation beetle(link);
    
    board = readoutTool() -> findByBoardID(STTell1ID(TELL1));
    channelID = ( board -> DAQToOffline(0, STDAQ::v4, beetle.toStripRepresentation() ).first );

    switch(port)
    {
    case 4:
      result = uniqueBeetle(channelID);
      break;
    default:
      result = "Port " + toString(port) + " of  " + uniqueBeetle(channelID);
    }
  }
  
  return result;
}


//=============================================================================
