// $Id: STPedestalWriter.h,v 1.4 2010-04-06 09:07:14 jluisier Exp $
#ifndef STPEDESTALWRITER_H 
#define STPEDESTALWRITER_H 1

// Include files
#include "STThresholdBase.h"

//
#include "Kernel/STBoardMapping.h"

/** @class STPedestalWriter STPedestalWriter.h
 *  
 * This class gets the pedestal at the end of the run and write
 * a condDB-like text file with the values.
 *
 *  @author Johan Luisier
 *  @date   2009-01-30
 */
class STPedestalWriter : public STThresholdBase {
public: 
  /// Standard constructor
  STPedestalWriter( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~STPedestalWriter( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:
  std::set< unsigned int > m_processedBoards;
  
private:
  StatusCode writePedestals();

  bool m_HeaderCorrDisable;
    
  std::vector< unsigned int > m_initialValues;
  std::vector< unsigned int > m_initialHdrSubValues;

  STBoardMapping::Map m_detMap;
};
#endif // STPEDESTALWRITER_H
