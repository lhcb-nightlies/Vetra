// $Id: STPedestalWriter.cpp,v 1.8 2010-04-06 09:07:14 jluisier Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// local
#include "STPedestalWriter.h"

#include "VetraKernel/ISTVetraCfg.h"
#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

using namespace std;
using namespace LHCb;
using namespace ST;
using namespace STBoardMapping;

//-----------------------------------------------------------------------------
// Implementation file for class : STPedestalWriter
//
// 2009-01-30 : Johan Luisier
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STPedestalWriter )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STPedestalWriter::STPedestalWriter( const std::string& name,
                                    ISvcLocator* pSvcLocator)
  : STThresholdBase ( name , pSvcLocator )
{
  //declareSTConfigProperty("InputLocation", m_DataLoc, "Raw/IT/SubPeds");
  declareProperty("HeaderCorrDisable", m_HeaderCorrDisable = true );
}
//=============================================================================
// Destructor
//=============================================================================
STPedestalWriter::~STPedestalWriter(){} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode STPedestalWriter::initialize() 
{
  StatusCode sc( STThresholdBase::initialize() ); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by STThresholdBase

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // Get the conditions
  sc = GetConditions();

  if ( sc.isFailure() ) return sc;
  
  // Initialize the detMap and the initial value of the updateEnable flag
  if ( detType() == "IT" )
  {
    m_detMap = ITNumberToSourceIDMap();
    m_initialValues.assign( 42, 0 );
    if(m_HeaderCorrDisable)
      m_initialHdrSubValues.assign( 42, 0 );
  } else {
    m_detMap = TTNumberToSourceIDMap();
    m_initialValues.assign( 48, 0 );
    if(m_HeaderCorrDisable)
      m_initialHdrSubValues.assign( 48, 0 );
  }
  
  Map::iterator it, End;
  End = m_detMap . end();
  STVetraCondition * m_stCond;
  // Store the initial value of the updateEnable flag and set it to enable
  for (it = m_detMap . begin(); it != End; it++)
  {
    m_stCond = m_stVetraCfg -> getSTVetraCond( it -> second );
    // Get the initial value of the Pedestal following flag
    m_initialValues[ it -> first - 1 ] = 
      m_stCond -> updateEnable();
    // Enable the pedestal following in the emulator
    m_stCond -> updateEnable( 1 );

    if(m_HeaderCorrDisable){
      // Get the initial value of the header subtraction flag
      m_initialHdrSubValues[ it -> first - 1 ] = 
        m_stCond -> headerEnable();
      // Disable the header subtraction in the emulator
      m_stCond -> headerEnable( 0 );
    }
    updMgrSvc() -> invalidate( m_stCond );
    sc = updMgrSvc() -> update( m_stCond );
    
    if(sc.isFailure())
      return sc;
  }
  
  return sc;  
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode STPedestalWriter::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  StatusCode sc(StatusCode::SUCCESS);
  // This initialization is done mainly to be able to exclude Tell1s from the
  // processing (do we need this) and to look for disabled links.
  if (!initDone)
  {
    sc = initHistograms() ;
    if ( sc.isRecoverable() )
    {
      if ( msgLevel(MSG::DEBUG) )
        debug() << "Got an empty container, assuming round robin data" << endmsg;
      return StatusCode::SUCCESS;
    }
    else if (sc.isFailure())
      return sc;
    if ( initDone && msgLevel( MSG::DEBUG) )
      debug() << "Histograms are finaly set up!" << endmsg;
  }


  // If the "convergence limit" is not reached, nothing has to be done
  // Also, when the "convergence limit" is set to zero, the pedestals
  // are only updated at the end (only works in non-round-robin mode).
  counter++;
  if (counter <= m_WaitingTill)
    return StatusCode::SUCCESS;

  if (m_WaitingTill > 0 && counter >= m_WaitingTill)
  {
    // write the pedestals from the emulator to the vetra conditions
    sc = writePedestals() ;
    if ( sc.isRecoverable() )
    {
      debug() << "Will try next time" << endmsg;
      return StatusCode::SUCCESS;
    }
    if ( sc.isFailure() )  
      return sc;
  }
  
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode STPedestalWriter::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  Map::iterator it, End( m_detMap . end() );

  for (it = m_detMap . begin(); it != End; it++)
  {
    m_stVetraCfg -> getSTVetraCond( it -> second ) 
      -> updateEnable( m_initialValues[ it -> first - 1] );
    if(m_HeaderCorrDisable){
      m_stVetraCfg -> getSTVetraCond( it -> second ) 
        -> headerEnable( m_initialHdrSubValues[ it -> first - 1] );
    }
  }

  info() << "Processed " << m_processedBoards.size() 
         << " TELL1 boards." << endmsg;
  
  return STThresholdBase::finalize();  // must be called after all other actions
}

StatusCode STPedestalWriter::writePedestals()
{
  StatusCode sc(SUCCESS);
  
  if ( msgLevel(MSG::DEBUG) )
    debug() << "Retrieving pedestals from " << m_DataLoc << endmsg;

  // Get the data  
  sc = getDatas( m_Datas, m_DataLoc );
  
  if ( sc.isRecoverable() )
    return StatusCode::SUCCESS;
  else if ( sc.isFailure() )
  {
    fatal() << "Cannot get data" << endmsg;
    return sc;
  }

  STTELL1Data* it;

  map<unsigned int, unsigned int>::const_iterator mapIt,
    mapBegin(ManageTELL1s.begin()), mapEnd(ManageTELL1s.end());

  vector<signed int> m_data;
  vector<signed int>::const_iterator dataIt, dataBegin, dataEnd;

  map<unsigned int, bool>::const_iterator lnkIt,
    lnkBegin(EnabledLinks.begin()), lnkEnd(EnabledLinks.end());

  unsigned int i;

  // Make data structure for pedestals in this TELL1
  vector< int > pedestalValues;

  // Iterate over the TELL1s
  for (mapIt = mapBegin; mapIt != mapEnd; mapIt++)
  {
    if ( ManageTELL1s.find( mapIt -> first ) == ManageTELL1s.end() )
      return Warning( "Trying to access an unexisting board",
                      StatusCode( SUCCESS ) );

    //if ( m_processedBoards.find( mapIt -> first ) != m_processedBoards.end() )
    //  continue;
    
    // Get the data for this TELL1
    it = m_Datas -> object( mapIt -> first );

    // Skip the TELL1s that have no data (in case of round robin)
    if (it == 0)
    {
      if ( msgLevel(MSG::DEBUG) )
        debug() << "No data for TELL1 " << mapIt -> first << endmsg;
      continue;
    }

    // Count the number of TELL1 processed
    m_processedBoards.insert( mapIt -> first );

    // Fill default value
    pedestalValues.assign( 3072, 128 );
    
    // Iterate over the beetles (links)
    for (i = 0; i < 24; i++)
    {
      lnkIt = EnabledLinks.find(mapIt -> first * 100000 + i * 1000);
      
      // If the link is disabled, the default value 128 will be written
      if ( !lnkIt -> second || lnkIt == lnkEnd )
      {
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "Skipping link " << i << " of TELL1 " << mapIt -> first
                    << endmsg;
        continue;
      }
      else
        m_data = it -> beetleData(i);
      
      dataBegin = m_data.begin();
      dataEnd   = m_data.end();

      copy( dataBegin, dataEnd, pedestalValues.begin() + i * 128 );
    }

    // Update the pedestal values in the transient Vetra conditions
    m_stVetraCfg -> getSTVetraCond( mapIt -> first ) -> pedestalValue( pedestalValues );
  }
  
  return sc;
}


//=============================================================================
