// $Id: STFastNoiseMonitor.cpp,v 1.12 2010-03-26 09:30:49 jluisier Exp $NoisePerSector
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// local
#include "STFastNoiseMonitor.h"

//
#include "Kernel/STBoardMapping.h"
#include "Kernel/BeetleRepresentation.h"
#include "Kernel/ISTReadoutTool.h"
#include "Kernel/STTell1Board.h"
#include "STDet/DeSTDetector.h"
#include "STDet/DeSTSector.h"

// Profiles
#include "AIDA/IProfile1D.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "TProfile.h"

#include "Kernel/STXMLUtils.h"

using namespace std;
using namespace AIDA;
using namespace LHCb;
using namespace ST;

//-----------------------------------------------------------------------------
// Implementation file for class : STFastNoiseMonitor
//
// 2011-10-10 : Frederic Dupertuis
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STFastNoiseMonitor )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STFastNoiseMonitor::STFastNoiseMonitor( const std::string& name,
                                ISvcLocator* pSvcLocator)
  : STThresholdBase ( name , pSvcLocator )
{
  declareProperty("ConstantFactor"  , m_constantFactor  = 776.);
  declareProperty("SlopeFactor"     , m_slopeFactor     = 47.9);
  declareProperty("NbrDigits"       , m_digits          = 2);
  declareProperty("ElectronsPerADC" , m_ElectronsPerADC = false);
  
  setForcedInit();
}
//=============================================================================
// Destructor
//=============================================================================
STFastNoiseMonitor::~STFastNoiseMonitor() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode STFastNoiseMonitor::initialize() {
  StatusCode sc(STThresholdBase::initialize()); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by STThresholdBase

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  if ( detType() == "IT" )
  {
    m_lcmsLoc = STTELL1DataLocation::ITLCMSADCs;
    m_pedsubtractedLoc = STTELL1DataLocation::ITPedSubADCs;
  }
  else
  {
    m_lcmsLoc = STTELL1DataLocation::TTLCMSADCs;
    m_pedsubtractedLoc = STTELL1DataLocation::TTPedSubADCs;
  }

  info() << "CMS data will be retrieved from " << m_lcmsLoc << endmsg;
  info() << "Ped data will be retrieved from " << m_pedsubtractedLoc << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode STFastNoiseMonitor::execute()
{
  StatusCode sc(SUCCESS);

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  counter++;
  // If the "convergence limit" is not reached, nothing has to be done
  if (counter <= m_WaitingTill)
    return sc;
  
  if (!initDone)
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "Finalizing the setting up of histograms" << endmsg;
    // STThresholdBase::initHistograms is called
    sc = initHistograms();
    if ( sc.isRecoverable() )
    {
      if ( msgLevel(MSG::DEBUG) )
        debug() << "Got an empty container, assuming round robin data" << endmsg;
      return StatusCode::SUCCESS;
    }
    else if (sc.isFailure())
      return sc;
    if ( initDone && msgLevel( MSG::DEBUG) )
      debug() << "Histograms are finaly set up!" << endmsg;
  }

  sc = getDatas( m_Datas, m_lcmsLoc );
  
  if ( sc.isRecoverable() )
    return StatusCode::SUCCESS;
  else if ( sc.isFailure() )
  {
    fatal() << "Cannot get data" << endmsg;
    return sc;
  }

  sc = getDatas( m_pedDatas, m_pedsubtractedLoc );
  
  if ( sc.isRecoverable() )
    return StatusCode::SUCCESS;
  else if ( sc.isFailure() )
  {
    fatal() << "Cannot get data" << endmsg;
    return sc;
  }
  
  sc = updateStats( m_Datas );
  if ( sc.isFailure() )
    return sc;
  
  sc = updateStats( m_pedDatas, "PedSub" );
  if ( sc.isFailure() )
    return sc;
  
  return sc;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode STFastNoiseMonitor::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  string litID, sectorName, beetleName;
  unsigned int  tell1, chan, link, realChan;
  double capacitance;

  STTell1Board* board;
  STChannelID channelID;
  STDAQ::BeetleRepresentation beetle(0);
  DeSTSector *sector = 0;
  DeSTSector::Status StripStatus;

  map<unsigned int, unsigned int>::const_iterator mapIt,
    mapBegin(ManageTELL1s.begin()), mapEnd(ManageTELL1s.end());

  TProfile *h1, *h2;

  double pedNoise, CMSNoise;

  //const Profile1DMapID& histos(profile1DMapID());
  // iterate over the map!
  for (mapIt = mapBegin; mapIt != mapEnd; mapIt++)
  {
    tell1 = mapIt -> first;

    if ( msgLevel(MSG::DEBUG) ) debug() << "TELL1 " << tell1 << endmsg;

    board = readoutTool() -> findByBoardID(STTell1ID(tell1));

    // Must here collect the noise values...
    for (link = 0; link < 24; link++)
    {
      sectorName = "";

      beetle = STDAQ::BeetleRepresentation(link);
      
      channelID = ( board -> DAQToOffline(0, STDAQ::v4, beetle.toStripRepresentation() ).first );
      
      sector = tracker() -> findSector( channelID );
      
      if ( sector == 0 )
      {
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "Link " << link << " skipped" << endmsg;
        continue;
      }

      sectorName = sector -> conditionsPathName();

      if ( msgLevel(MSG::VERBOSE) )
        verbose() << sectorName << " is processed" << endl;

      if (NoisePerSector.find(sector) == NoisePerSector.end())
        NoisePerSector[ sector ] = vector<double>( sector -> nStrip() );

      if (CMPerSector.find(sector) == CMPerSector.end())
        CMPerSector[ sector ] = vector<double>( sector -> nStrip() );

      if (Capacitances.find( sector ) == Capacitances.end())
        Capacitances[ sector ] = sector -> capacitance();

      for (chan = 0; chan < 128; chan++)
      {
        // Check if the channel is "bad"
        channelID = (board -> DAQToOffline(0, STDAQ::v4, STDAQ::StripRepresentation(chan + 128 * link)).first);
        StripStatus = (sector -> stripStatus(channelID));
        realChan = channelID.strip() - 1;
        if ( StripStatus != DeSTSector::OK &&
             StripStatus != DeSTSector::Pinhole )
        { 
          NoisePerSector[ sector ][realChan] = 999.;
          CMPerSector[ sector ][realChan]    = 999.;
        }
        else
        {
          CMSNoise = sqrt( LCMSStatMap[tell1][chan + 128 * link].moment2 - pow(LCMSStatMap[tell1][chan + 128 * link].mean,2) );
	  pedNoise = sqrt( PedSubStatMap[tell1][chan + 128 * link].moment2 - pow(PedSubStatMap[tell1][chan + 128 * link].mean,2) );
	  profile1D(chan + 128 * link,
		    LCMSStatMap[tell1][chan + 128 * link].mean,
		    toString(tell1,3)+"prof", "TELL1 " + toString(tell1,3),
		    -.5, 3071.5, 3072, "s");
          profile1D(chan + 128 * link,
		    PedSubStatMap[tell1][chan + 128 * link].mean, 
		    (string)"PedSub" + toString(tell1,3)+"prof", (string)"PedSub" + "TELL1 " + toString(tell1,3),
		    -.5, 3071.5, 3072, "s");
	  NoisePerSector[ sector ][realChan] = CMSNoise;
          CMPerSector[ sector ][realChan] =
            sqrt( std::max( pedNoise * pedNoise - CMSNoise * CMSNoise, 0. ) );
        }
      } // Loop over channels
    } // Loop over link
    for (chan = 0; chan < 3072; chan++){
      h1 = Gaudi::Utils::Aida2ROOT::aida2root(profile1D("TELL1 " + toString(tell1,3)));
      h1->SetBinError(chan+1, sqrt( LCMSStatMap[tell1][chan].moment2 - pow(LCMSStatMap[tell1][chan].mean,2)));
      h2 = Gaudi::Utils::Aida2ROOT::aida2root(profile1D((string)"PedSub" + "TELL1 " + toString(tell1,3)));
      h2->SetBinError(chan+1, sqrt( PedSubStatMap[tell1][chan].moment2 - pow(PedSubStatMap[tell1][chan].mean,2)));
    }
  } // Loop over the map

  debug() << "All values collected" << endmsg;

  map<DeSTSector*, vector<double> >::const_iterator secIt,
    secBegin(NoisePerSector.begin()),
    secEnd(NoisePerSector.end());
  
  for (secIt = secBegin; secIt != secEnd; secIt++)
  {
    sector = secIt -> first;
    
    sector -> setNoise( NoisePerSector[ sector ] );
    
    sector -> setCMNoise( CMPerSector[ sector ] );

    if ( m_ElectronsPerADC )
    {
      capacitance = Capacitances[sector] / Gaudi::Units::picofarad;

      vector< double > tmpCap;

      for (chan = 0; chan < sector -> nStrip(); chan++)
      {
        tmpCap.push_back( m_constantFactor + m_slopeFactor * capacitance );
      }
      
      sector -> setADCConversion( tmpCap );
    }
    
    if (secIt == secEnd) break;
  }
  
  return STThresholdBase::finalize();  // must be called after all other action
}

StatusCode STFastNoiseMonitor::updateStats( STTELL1Datas* container,
					    const string& prefixName )
{
  STTELL1Datas::const_iterator Begin(container -> begin()),
    End(container -> end());
  
  map<unsigned int, bool>::const_iterator lnkIt,
    lnkBegin(EnabledLinks.begin()), lnkEnd(EnabledLinks.end());
  
  vector<signed int> m_data;
  vector<signed int>::const_iterator dataIt, dataBegin, dataEnd;
  unsigned int i, TELL1_nbr;
  
  for (STTELL1Datas::const_iterator it(Begin); it != End; it++)
  {
    // Loop on TELL1s
    
    TELL1_nbr = static_cast<unsigned int>((*it) -> key());
    
    if (!EnabledTELL1s[TELL1_nbr])
    {
      if ( msgLevel(MSG::VERBOSE) )
        verbose() << "Skipping TELL1 " << TELL1_nbr << endmsg;
      continue;
    }
    
    if ( PedSubStatMap.find(TELL1_nbr) == PedSubStatMap.end() ) {
      PedSubStatMap[TELL1_nbr].assign( 3072, EvolutiveStat() );
      LCMSStatMap[TELL1_nbr].assign( 3072, EvolutiveStat() );
    }

    for (i = 0; i < 24; i++)
    {
      // Loop on optical links
      
      lnkIt = EnabledLinks.find(TELL1_nbr * 100000 + i * 1000);
      
      if (!lnkIt -> second || lnkIt == lnkEnd)
      {
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "Skipping link " << i << " of TELL1 "
                    << TELL1_nbr << endmsg;
        continue;
      }
      
      m_data = (*it) -> beetleData(i);
      
      dataBegin = m_data.begin();
      dataEnd   = m_data.end();
      
      for (dataIt = dataBegin; dataIt != dataEnd; dataIt++)
      {
        // Loop on data          
	if(prefixName == "PedSub")
	  PedSubStatMap[TELL1_nbr][i*128+dataIt-dataBegin].Add(*dataIt);
	else
	  LCMSStatMap[TELL1_nbr][i*128+dataIt-dataBegin].Add(*dataIt);
      }
    }
  }

  return SUCCESS;
}


//=============================================================================
