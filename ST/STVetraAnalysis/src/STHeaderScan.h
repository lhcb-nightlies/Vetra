// $Id: STHeaderScan.h,v 1.1 2009-02-18 16:36:09 jvantilb Exp $
#ifndef STHeaderScan_H
#define STHeaderScan_H 1

#include "Kernel/STHistoAlgBase.h"
#include "STDet/DeSTDetector.h"
#include "Event/STTELL1Data.h"

/** @class STHeaderScan STHeaderScan.h
 *
 *  Class to monitor the shape of the header signal using the ADC scan w/o 
 *  test pulses. The output consists of a profile histogram per channel
 *  showing the ADC value as a function of the ADC sampling time. Also the
 *  header part of the signal can be monitored (referred to as the negative
 *  channels -1, -2 and -3). A separate histogram is filled for each of the
 *  8 header bit combinations (e.g. 111, 110, etc). The main job options are:
 *  - \b FirstChannel: This gives the first channel to monitor. Default is -3.
 *  - \b LastChannel: This gives the last channel to monitor. Default is 4.
 *  - \b Pedestal: Average pedestal for the whole detector. Offsets the 
 *                 histogram in along y.
 *  - \b OneThreshold: Threshold in ADC counts to determine whether the header
 *                     bit is a logical one. Default is 129.
 *  - \b ZeroThreshold: Threshold in ADC counts to determine whether the header
 *                      bit is a logical zero.Default is 129.
 *  - \b TimeRangeMin: Lower range of the profile histogram. Default is 0.0.
 *  - \b TimeRangeMax: Upper range of the profile histogram. Default is 25.0.
 *  - \b NBins: Number of bins in the profile histogram. Default is 50.
 *  - \b StepSize: Step size in time units, used in the stepping of the ADC
 *                 sampling point. Default is 0.5 (ns).
 * 
 *  @author J. van Tilburg
 *  @date   18.02.2009
 */

class STHeaderScan : public ST::HistoAlgBase {

public:
 
  /// constructer
  STHeaderScan( const std::string& name, ISvcLocator *svcloc );

  /// destructer
  virtual ~STHeaderScan();

  /// initialize
  StatusCode initialize() override;

  /// execute
  StatusCode execute() override;

private:

  void fillProfiles(const LHCb::STTELL1Datas* data, double timeStep);

  // job options
  std::string m_dataLocation;
  int m_one_threshold;
  int m_zero_threshold;  
  int m_firstChannel;
  int m_lastChannel;
  double m_pedestal;
  double m_rangeMin;
  double m_rangeMax;  
  int m_nBins;
  double m_stepSize;

};

#endif // STHeaderScan_H
