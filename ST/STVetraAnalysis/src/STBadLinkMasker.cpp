// $Id: STBadLinkMasker.cpp,v 1.22 2009-11-19 18:18:00 szumlat Exp $
// Include files

// local
#include "STBadLinkMasker.h"
#include "Kernel/STTell1Board.h"
#include "Kernel/BeetleRepresentation.h"
#include "Kernel/ISTReadoutTool.h"
#include "STDet/DeSTDetector.h"
#include "STDet/DeSTSector.h"
#include "LHCbMath/Power.h"

#include <boost/assign/list_of.hpp>

using namespace std;
using namespace LHCb;
using namespace AIDA;
using namespace ST;
using namespace boost::assign;

//-----------------------------------------------------------------------------
// Implementation file for class : STBadLinkMasker
//
// 2008-07-08 : Johan Luisier
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STBadLinkMasker )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STBadLinkMasker::STBadLinkMasker( const std::string& name,
                                  ISvcLocator* pSvcLocator)
  : STThresholdBase ( name , pSvcLocator ),
    BeetlesPerSector(3)
{
  declareProperty("LowGainThreshold"  , m_LowGain         = 110.);
  declareProperty("LowNoiseThreshold" , m_LowNoise        = 1.3);
  declareProperty("HighNoiseThreshold", m_HighNoise       = 6.);
  //declareProperty("HighSigThreshold"  , m_HighSig         = 300.);
  declareProperty("ShortThreshold"    , m_Short           = 6.);
  declareProperty("TestPulseData"     , m_TPdata          = false);
  declareProperty("BoardAlias"        , m_BoardAlias      = false);
  declareProperty("FullDetails"       , m_FullDetails     = true);
  declareProperty("KnownBadStrips"    , m_KnownBadStrips  = true);
  declareProperty("FullNames"         , m_FullNames       = false);
  std::vector< unsigned int > tmpMasked = 
    list_of( 1 )( 2 )( 4 )( 5 )( 6 )( 7 )( 9 )( 10 ); 
  declareProperty("MaskFromDB"        , m_MaskedBadValues = tmpMasked );

  // Initialises the ReadoutTool
  setForcedInit();
}
//=============================================================================
// Destructor
//=============================================================================
STBadLinkMasker::~STBadLinkMasker()
{} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode STBadLinkMasker::initialize()
{
  StatusCode sc(STThresholdBase::initialize()); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by STThresholdBase

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  if (detType() == "TT")
    BeetlesPerSector = 4;

  if (m_FullNames)
    m_FullDetails = true;

  if (m_TPdata)
  {
    info() << "Requested to run on test pulses data" << endmsg
           << " => noisy channels search will be deactivated" << endmsg
           << " => short channels search activated" << endmsg
      /*<< " => high signal channels search activated"<< endmsg*/;
    m_HighNoise = 100.;
  }
  //else
  //m_HighSig   = 300.;

  
  vector< unsigned int >::const_iterator vecBegin( m_MaskedBadValues.begin() ),
    vecEnd( m_MaskedBadValues.end() ), vecIt;

  info() << "Will mask strips with status ";

  for (vecIt = vecBegin; vecIt != vecEnd; vecIt++)
    info() << *vecIt << ' ';

  info() << endmsg;

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode STBadLinkMasker::execute()
{
  StatusCode sc(SUCCESS);

  counter++;
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  // Get the data
  sc = getDatas( m_Datas, m_DataLoc );
  
  if ( sc.isRecoverable() )
    return StatusCode::SUCCESS;
  else if ( sc.isFailure() )
  {
    fatal() << "Cannot get data" << endmsg;
    return sc;
  }
  
  if (!initDone)
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "Finalizing the setting up of histograms" << endmsg;
    // STThresholdBase::initHistograms is called
    sc = initHistograms();
    if ( sc.isRecoverable() )
    {
      if ( msgLevel(MSG::DEBUG) )
        debug() << "Got an empty container, assuming round robin data"
                << endmsg;
      return StatusCode::SUCCESS;
    }
    else if (sc.isFailure())
      return sc;
    if ( initDone && msgLevel( MSG::DEBUG) )
      debug() << "Histograms are finaly set up!" << endmsg;
  }

  STTELL1Datas::const_iterator it, Begin(m_Datas -> begin()),
    End(m_Datas -> end());

  if ( msgLevel(MSG::DEBUG) )
    debug() << "Number of TELL1s : "
            << static_cast<unsigned int>(End - Begin) << endmsg;

  vector<signed int> m_data;
  vector<signed int>::const_iterator dataIt, dataBegin, dataEnd;

  unsigned int TELL1, i;
  
  map<unsigned int, bool>::const_iterator lnkIt,
    lnkBegin(EnabledLinks.begin()), lnkEnd(EnabledLinks.end());

  for (it = Begin; it != End; it++)
  {
    // Loop on TELL1s

    TELL1 = static_cast<unsigned int>((*it) -> key());
    
    if (!EnabledTELL1s[TELL1])
    {
      verbose() << "Skipping TELL1 " << TELL1 << endmsg;
      continue;
    }

    for (i = 0; i < 24; i++)
    {
      // Loop on optical links

      lnkIt = EnabledLinks.find(TELL1 * 100000 + i * 1000);
      
      if (!lnkIt -> second || lnkIt == lnkEnd)
	    {
	      if (msgLevel(MSG::VERBOSE))
          verbose() << "Skipping link " << i << " of TELL1 " << TELL1
                    << endmsg;
	      continue;
	    }

      m_data = (*it) -> beetleData(i);
      
      dataBegin = m_data.begin();
      dataEnd   = m_data.end();
      
      histoTitle = "TELL1 ";
      histoTitle += toString(TELL1,3);
      histoName  = toString(TELL1,3);
      histoName  += "prof";
      for (dataIt = dataBegin; dataIt != dataEnd; dataIt++)
	    {
	      // Loop on data
	      profile1D(i*128 + static_cast<unsigned int>(dataIt - dataBegin),
                  *dataIt, histoName, histoTitle,
                  -.5, 3071.5, 3072, "s");
	      // x => chan, y => ADC value
	    }
    }
  }

  return sc;
}

//=============================================================================
//  Finalize
//=============================================================================
  StatusCode STBadLinkMasker::finalize()
  {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  const Profile1DMapID& profiles(profile1DMapID());
  
  // Iterate over the TELL1
  map<unsigned int, unsigned int>::const_iterator mapIt,
    mapBegin(ManageTELL1s.begin()), mapEnd(ManageTELL1s.end());

  IProfile1D *profile;

  string litID, SecStr;

  unsigned int bin, port, link, TELL1, i, BadSize, PortsDisabled;

  double mean, rms, NextRms, NNextRms, PrevRms;

  ofstream out(m_FileName.c_str());

  vector<unsigned int> values;

  vector< vector<unsigned int> > LinksAndPorts;

  vector< vector<unsigned int> > BadLinks(5);

  vector<bool> FaultyPorts;

  bool same(true);

  stringstream stripsnumber, DisLinks1, DisLinks2, DisPorts1, DisPorts2;

  string ErrorNames[7] = 
    {
      "None", "Low gain", "Low Noise", "Noisy", "Short", "Known bad",
      /*"High Signal",*/ "Already masked"
    };

  vector< unsigned int >::const_iterator vecBegin( m_MaskedBadValues.begin() ),
    vecEnd( m_MaskedBadValues.end() );
  
  if (out.fail())
    return Error("Cannot open " + m_FileName + " for writing");

  STTell1Board* board;
  STChannelID channelID;
  DeSTSector *sector;
  DeSTSector::Status StripStatus, OldSStatus;

  vector<unsigned int> chans;
  unsigned int chansSize, j, difference;

  for (mapIt = mapBegin; mapIt != mapEnd; mapIt++)
  {
    chans.clear();

    TELL1 = mapIt -> first;
    litID = toString(TELL1,3) + "prof";

    profile = profiles[litID];

    if (profile == 0)
      continue;

    if ( msgLevel(MSG::DEBUG) )
      debug() << "TELL1 " << mapIt -> first << endmsg;

    values.assign(7, 0);

    LinksAndPorts.assign(24, vector<unsigned int>(4));

    BadLinks.assign(6, vector<unsigned int>(0));
    
    out << "TELL1Board" << TELL1 << endl << flush;
    out << "<paramVector name=\"pedestal_mask\" type=\"int\""
        << " comment=\"Masks strips to be skipped in PS process\"> " << flush;

    board = readoutTool() -> findByBoardID(STTell1ID(TELL1));

    for (bin = 0; bin < 3072; bin++)
    {
      link = bin / 128;
      port = (bin / 32) % 4;

      channelID = (board -> DAQToOffline(0, STDAQ::v4, STDAQ::StripRepresentation(bin)).first);

      sector = tracker() -> findSector(channelID);
	  
      if (sector != 0)
        StripStatus = (sector -> stripStatus(channelID));
      else                       
        StripStatus = DeSTSector::OK;

      verbose() << "status                [ OK ]" << endmsg;

      if ( msgLevel(MSG::VERBOSE) )
        verbose() << bin << " : " << StripStatus << endmsg;
	  
      if (StripStatus != DeSTSector::OK)
      {
        values[5]++;                                              
        BadLinks[4].push_back(bin);
      }

      mean    = profile -> binHeight(bin);
      rms     = profile -> binRms(bin);
      if (bin == 0)
        PrevRms = 100.;
      else
        PrevRms = profile -> binRms(bin - 1);
      if (bin == 3071)                       
        NextRms = 100.;
      else
        NextRms = profile -> binRms(bin + 1);
      if (bin >= 3070)
        NNextRms = 100.;
      else
        NNextRms = profile -> binRms(bin + 2);

      if (rms < 1e-7 && mean < 1e-7)
        // RMS and mean == 0 -> no data => this thing is masked off
	    {
        values[6]++;
	      LinksAndPorts[link][port] += 1;
        BadLinks[5].push_back(bin);
	      out << "1 " << flush;
	    }
      else if (m_TPdata && rms < m_Short &&
               NextRms < m_Short && PrevRms > m_Short && NNextRms > m_Short)
        // We probably have two short strips
	    {
        values[4] += 2;
        LinksAndPorts[link][port] += 2;
        out << "1 1 " << flush;
        BadLinks[3].push_back(bin);
        BadLinks[3].push_back(bin + 1);
        bin++;
	    }
      /*
        else if (mean > m_HighSig)
        // TP data, and signal is too high (=> open?)
        {
        values[6]++;
	      LinksAndPorts[link][port] += 1;
        BadLinks[5].push_back(bin);
	      out << "1 " << flush;
        }
      */
      else if (mean < m_LowGain)
        // Looks like we have a low gain channel (signal too low)
	    {
	      values[1]++;
	      LinksAndPorts[link][port] += 1;
	      out << "1 " << flush;
        BadLinks[0].push_back(bin);
	    }
      else if (rms > .1 && rms < m_LowNoise)
        // Noise is too low
	    {
	      values[2]++;
	      LinksAndPorts[link][port] += 1;
	      out << "1 " << flush;
        BadLinks[1].push_back(bin);
	    }
      else if (bin % 32 != 0 && rms > m_HighNoise)
        // Noisy channel
	    {
	      values[3]++;
	      LinksAndPorts[link][port] += 1;
	      out << "1 " << flush;
        BadLinks[2].push_back(bin);
	    }
      else
      {
        // Check if it's really OK
        // but only if KnownBadStrips is true !

        if (m_KnownBadStrips)
        {
          if ( find(vecBegin, vecEnd, static_cast<unsigned int>(StripStatus))
               != vecEnd )
          {
            LinksAndPorts[link][port] += 1;
            out << "1 " << flush;
          }
          else
            out << "0 " << flush;
        }
        else
        {
          out << "0 " << flush;
        }
	    }
    }
    
    out << "</paramVector>" << endl << flush;
    
    if (values[0] == 3072 && values[5] == 0)
      continue;
    
    info() << setfill('*') << setw(30) << '*' << setfill(' ') <<endmsg;

    info() << "TELL1 with sourceID " << TELL1;

    if (m_BoardAlias && boardAlias(TELL1) != 0)
      info() << " (aka TELL1 " << setw(2) << boardAlias(TELL1) << ")";

    info() << endmsg;

    for (bin = 1; bin < 7; bin++)
    {
      chans.clear();

      if (values[bin] != 0)
      {
        info() << values[bin] << " " << ErrorNames[bin] << " strips in :"
               << endmsg;
        BadSize = BadLinks[bin - 1].size();
        
        if ( msgLevel(MSG::DEBUG) )
        {
          for (i = 0; i < BadSize; i++)
            debug() << BadLinks[bin - 1][i] << ' ';
          debug() << endmsg;
        }
        
        for (i = 0; i <= BadSize; i++)
        {
          // Need to check if whole 1) port 2) link (beetle) 3) module
          // has the problem
          if (chans.empty())
            chans.push_back(BadLinks[bin - 1][i]);
          else if (BadLinks[bin - 1][i] == chans.back() + 1)
            chans.push_back(BadLinks[bin - 1][i]);
          else
          {
            // Now we check the size of chans
            chansSize = chans.size();
            if (chansSize == BeetlesPerSector * 128)
            {
              if ( msgLevel(MSG::DEBUG) )
                debug() << "Probably a whole sector" << endmsg;
              // Probably a module
              channelID = board -> DAQToOffline(0, STDAQ::v4, STDAQ::StripRepresentation(chans[0])).first;
              if (chans[0] / (BeetlesPerSector * 128) ==
                  chans.back() / (BeetlesPerSector * 128))
              {
                SecStr = uniqueSector(channelID);
                if (SecStr.find("Unknown") != string::npos)
                  info() << "Unconnected sector, strips " <<  chans[0]
                         << " to "  << chans.back() << endmsg;
                else
                  info() << "sector " << channelID.sector()
                         << " detRegion " << channelID.detRegion()
                         << " layer " << channelID.layer()
                         << " station " << channelID.station()
                         << " (" << tracker() -> findSector(channelID) -> sectorStatus()
                         << ")"<< endmsg;
              }
            }
            else if (chansSize == 128)
            {
              if ( msgLevel(MSG::DEBUG) )
                debug() << "Probably a whole link" << endmsg;
              // Probably a link (beetle)
              channelID = board -> DAQToOffline(0, STDAQ::v4, STDAQ::StripRepresentation(chans[0])).first;
              if (chans[0] / 128 == chans.back() / 128)
                info() << "sector " << channelID.sector()
                       << " detRegion " << channelID.detRegion()
                       << " layer " << channelID.layer()
                       << " station " << channelID.station()
                       << " beetle " << (channelID.strip() - 1) / 128 + 1
                       << " (" << tracker() -> findSector(channelID) -> beetleStatus(channelID)
                       << ")"<< endmsg;
            }
            else if (chansSize % 128 == 0)
            {
              difference = chansSize / 128;
              if ( msgLevel(MSG::DEBUG) )
                debug() << "Probably " << difference << " whole links"
                        << endmsg;
              channelID = board -> DAQToOffline(0, STDAQ::v4, STDAQ::StripRepresentation(chans[0])).first;
              if (chans[0] / 128 + difference == chans.back() / 128)
                info() << "sector " << channelID.sector()
                       << " detRegion " << channelID.detRegion()
                       << " layer " << channelID.layer()
                       << " station " << channelID.station()
                       << " beetles " << (channelID.strip() - 1) / 128 + 1 << " and "
                       << channelID.strip() / 128 + difference + 1
                       << " (" << tracker() -> findSector(channelID) -> stripStatus(channelID)
                       << ")"<< endmsg;
            }
            if (chansSize == 32)
            {

              if ( msgLevel(MSG::DEBUG) )
                debug() << "Probably a whole port" << endmsg;
              // Probably a port
              channelID = board -> DAQToOffline(0, STDAQ::v4, STDAQ::StripRepresentation(chans[0])).first;
              if (chans[0] / 32 == chans.back() / 32)
                info() << "sector " << channelID.sector()
                       << " detRegion " << channelID.detRegion()
                       << " layer " << channelID.layer()
                       << " station " << channelID.station()
                       << " beetle " << (channelID.strip() - 1) / 128 + 1
                       << " port " << (chans[0] / 32) % 4 + 1
                       << " (" << tracker() -> findSector(channelID) -> stripStatus(channelID)
                       << ")"<< endmsg;
            }
            else if (chansSize % 32 == 0)
            {
              // Probably many ports
              difference = chansSize / 32;
              if ( msgLevel(MSG::DEBUG) )
                debug() << "Probably " << difference << " whole ports"
                        << endmsg;
              channelID = board -> DAQToOffline(0, STDAQ::v4, STDAQ::StripRepresentation(chans[0])).first;
              if (chans[0] / 32 + difference == chans.back() / 32)
                info() << "sector " << channelID.sector()
                       << " detRegion " << channelID.detRegion()
                       << " layer " << channelID.layer()
                       << " station " << channelID.station()
                       << " beetle " << (channelID.strip() - 1) / 128 + 1
                       << " ports " << (chans[0] / 32) % 4 + 1
                       << " to " << (chans[0] / 32) % 4 + difference + 1
                       << " (" << tracker() -> findSector(channelID) -> stripStatus(channelID)
                       << ")"<< endmsg;
            }
            else
            {
              if ( msgLevel(MSG::DEBUG) )
                debug() << chansSize << " random strip(s)" << endmsg;
              // "Random" strip(s) ie pair of shorted channels, others...
              for (j = 0; j < chansSize; j++)
              {
                channelID = board -> DAQToOffline(0, STDAQ::v4, STDAQ::StripRepresentation(chans[j])).first;
                StripStatus  = tracker() -> findSector(channelID) -> stripStatus(channelID);
                if (j == 0)
                  OldSStatus = StripStatus;

                same = (OldSStatus == StripStatus);

                if (j == (chansSize - 1))
                {
                  same = false;
                }
                

                if (same)
                {
                  // While the StripStatus is the same, fill the sstream
                  stripsnumber << chans[j] << ' ';
                }
                else if (chansSize == 1)
                {
                  debug() << chans[0] << endmsg; // to be erased
                  info() << "strip(s) " << chans[0] << endmsg
                         << "sector " << channelID.sector()
                         << " detRegion " << channelID.detRegion()
                         << " layer " << channelID.layer()
                         << " station " << channelID.station()
                         << " beetle " << channelID.strip() / 128 + 1
                         << " (" << StripStatus << ")"<< endmsg;
                  stripsnumber.str(""); // Reinitialise the sstream
                  OldSStatus = StripStatus;
                }
                else
                {
                  // Now, we print the stored strips and the status
                  if (OldSStatus == StripStatus && j == (chansSize - 1))
                  {
                    stripsnumber << chans[j];
                    info() << "strip(s) " << stripsnumber.str() << endmsg
                           << "sector " << channelID.sector()
                           << " detRegion " << channelID.detRegion()
                           << " layer " << channelID.layer()
                           << " station " << channelID.station()
                           << " beetle " << channelID.strip() / 128 + 1
                           << " (" << OldSStatus << ")"<< endmsg;
                    stripsnumber.str(""); // Reinitialise the sstream
                    OldSStatus = StripStatus;
                  }
                  if (j == (chansSize - 1 ) && OldSStatus != StripStatus)
                  {
                    info() << "strip(s) " << stripsnumber.str() << endmsg
                           << "sector " << channelID.sector()
                           << " detRegion " << channelID.detRegion()
                           << " layer " << channelID.layer()
                           << " station " << channelID.station()
                           << " beetle " << channelID.strip() / 128 + 1
                           << " (" << OldSStatus << ")"<< endmsg;
                    stripsnumber.str(""); // Reinitialise the sstream
                    OldSStatus = StripStatus;
                    
                    info() << "strip(s) " << chans[j] << endmsg
                           << "sector " << channelID.sector()
                           << " detRegion " << channelID.detRegion()
                           << " layer " << channelID.layer()
                           << " station " << channelID.station()
                           << " beetle " << channelID.strip() / 128 + 1
                           << " (" << StripStatus << ")"<< endmsg;
                  }
                  else if (stripsnumber.str().size() > 0)
                  {
                     info() << "strip(s) " << stripsnumber.str() << endmsg
                           << "sector " << channelID.sector()
                           << " detRegion " << channelID.detRegion()
                           << " layer " << channelID.layer()
                           << " station " << channelID.station()
                           << " beetle " << channelID.strip() / 128 + 1
                           << " (" << OldSStatus << ")"<< endmsg;
                    stripsnumber.str(""); // Reinitialise the sstream
                    OldSStatus = StripStatus;
                  }
                  
                }
              }
            }
            chans.clear();
            if (i != BadLinks[bin - 1].size())
              chans.push_back(BadLinks[bin - 1][i]);
          }
        }
      }
      debug() << "bin = " << bin << endmsg; // to be erased
    }

    DisLinks1.str("");
    DisLinks2.str("");
    DisPorts1.str("");
    DisPorts2.str("");

    // Check for links that have 128 bad strips
    for (link = 0; link < 24; link++)
    {
      if (LinksAndPorts[link][0] + LinksAndPorts[link][1] +
          LinksAndPorts[link][2] + LinksAndPorts[link][3]== 0)
        // This link has no error ! Great!!
      {
        if (link < 12)
          DisLinks1 << "0 ";
        else
          DisLinks2 << "0 ";
        //continue;
      }
      else if (LinksAndPorts[link][0] + LinksAndPorts[link][1] +
               LinksAndPorts[link][2] + LinksAndPorts[link][3] == 128)
        // All strips of the link have errors!
      {
        if (link < 12)
          DisLinks1 << "1 ";
        else
          DisLinks2 << "1 ";
        if (m_FullDetails)
        {
          if ( msgLevel(MSG::DEBUG) )
            debug() << LinksAndPorts[link][0] << ' '
                    << LinksAndPorts[link][1] << ' '
                    << LinksAndPorts[link][2] << ' '
                    << LinksAndPorts[link][3] << endmsg;
          info() << fillStream(TELL1, link)
                 << " has every strip bad" << endmsg;
        }
        //continue;
      }
      else
        if (link < 12)
          DisLinks1 << "0 ";
        else
          DisLinks2 << "0 ";
      
      FaultyPorts.assign(4, false);

      PortsDisabled = 0;

      // Check for ports that have 32 bad strips
      for (port = 0; port < 4; port++)
      {
        if (LinksAndPorts[link][port] == 0)
          // This port has no error!
          continue;
        else if (LinksAndPorts[link][port] == 32)
        {
          PortsDisabled += Gaudi::Math::pow(2, port);
          if (m_FullDetails)
            info() << fillStream(TELL1, link, port)
                   << " has every strip bad" << endmsg;
        }
        
        else
          FaultyPorts[port] = true;
      }

      if (link < 12)
        DisPorts1 << "0x" << (hex) << PortsDisabled << " ";
      else
        DisPorts2 << "0x" << (hex) << PortsDisabled << " ";
      
      for (port = 0; port < 4; port++)
      {
          if (FaultyPorts[0] * FaultyPorts[1] *
              FaultyPorts[2] * FaultyPorts[3])
          {
            if (m_FullDetails)
            {
              if ( msgLevel(MSG::DEBUG) )
                debug() << LinksAndPorts[link][0] << ' '
                        << LinksAndPorts[link][1] << ' '
                        << LinksAndPorts[link][2] << ' '
                        << LinksAndPorts[link][3] << endmsg;
              info() << fillStream(TELL1, link)
                     << " has some bad strips " << endmsg;
            }
            break;
          }
          else if (FaultyPorts[port])
          {
            if (m_FullDetails)
            {
              if ( msgLevel(MSG::DEBUG) )
                debug() << LinksAndPorts[link][0] << ' '
                        << LinksAndPorts[link][1] << ' '
                        << LinksAndPorts[link][2] << ' '
                        << LinksAndPorts[link][3] << endmsg;
              info() << fillStream(TELL1, link, port)
                     << " has some bad strips" << endmsg;
            }
          }
      }
    }
    if ( msgLevel(MSG::DEBUG) )
    {
      debug() << "Links_0_11  " << DisLinks1.str() << endmsg;
      debug() << "Links_12_23 " << DisLinks2.str() << endmsg;
      debug() << "Ports_0_11  " << DisPorts1.str() << endmsg;
      debug() << "Ports_12_23 " << DisPorts1.str() << endmsg;
    }
    out << "<paramVector name=\"arx_beetle_disable_0\""
        << " type=\"string\"> " << DisPorts1.str()
        << "</paramVector>" << endl;
    out << "<paramVector name=\"arx_beetle_disable_1\""
        << " type=\"string\"> " << DisPorts2.str()
        << "</paramVector>" << endl;
    out << "<paramVector name=\"orx_beetle_disable_0\" type=\"int\"> "
        << DisLinks1.str() << "</paramVector>" << endl;
    out << "<paramVector name=\"orx_beetle_disable_1\" type=\"int\"> "
        << DisLinks2.str() << "</paramVector>" << endl;
  }
  
  info() << setfill('*') << setw(30) << '*' << setfill(' ') << endmsg;

  out.close();

  return STThresholdBase::finalize();  // must be called after all other actions
}

/**
 * Produce the output, depending on m_FullNames state. Output can be either :
 * link 0 port 1, or
 * IT1Sector1...
 *
 * @param TELL1 the TELL1 ID
 * @param link the link number (which is the beetle number as well)
 * @param port the port number of the TELL1
 *
 * @return the corresponding string
 */
std::string STBadLinkMasker::fillStream(const unsigned int& TELL1,
                                        const unsigned int& link,
                                        const unsigned int& port) const
{
  string result("");

  if (!m_FullNames)
  {
    switch(port)
    {
    case 4:
      result = "Link " + toString(link);
      break;
    default:
      result = "Port " + toString(port) + " of link " + toString(link);
    }
  }
  else
  {
    STTell1Board* board;
    STChannelID channelID;
    STDAQ::BeetleRepresentation beetle(link);
    
    board = readoutTool() -> findByBoardID(STTell1ID(TELL1));
    channelID = ( board -> DAQToOffline(0, STDAQ::v4, beetle.toStripRepresentation() ).first );

    switch(port)
    {
    case 4:
      result = uniqueBeetle(channelID);
      break;
    default:
      result = "Port " + toString(port) + " of  " + uniqueBeetle(channelID);
    }
  }
  
  return result;
}

//=============================================================================
