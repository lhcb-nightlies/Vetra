// $Id: STCalibrationTuple.cpp,v 1.8 2010-04-06 09:07:14 jluisier Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// LHCbKernel
#include "Kernel/ISTReadoutTool.h"
#include "Kernel/STDAQDefinitions.h"
#include "Kernel/STLexicalCaster.h"
#include "Kernel/STTell1Board.h"
#include "Kernel/STTell1ID.h"
#include "Kernel/STChannelID.h"

// VetraKernel
#include "VetraKernel/ISTVetraCfg.h"
#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

#include "STDet/DeSTDetector.h"
#include "STDet/DeSTSector.h"

// local
#include "STCalibrationTuple.h"

using namespace std;
using namespace LHCb;
using namespace STDAQ;
using namespace ST;
using namespace STBoardMapping;

//-----------------------------------------------------------------------------
// Implementation file for class : STCalibrationTuple
//
// 2011-09-21 : Frederic Dupertuis
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STCalibrationTuple )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STCalibrationTuple::STCalibrationTuple( const std::string& name,
					    ISvcLocator* pSvcLocator)
  : ST::TupleAlgBase( name , pSvcLocator )
{
  declareSTConfigProperty("TupleName" , m_tupleName,
                          "ITCalibration");
  declareSTConfigProperty("VetraToolName" , m_stVetraToolName,
                          "ITVetraCfgTool",
                          "Name of the STVetraCfg tool");
}
//=============================================================================
// Destructor
//=============================================================================
STCalibrationTuple::~STCalibrationTuple(){} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode STCalibrationTuple::initialize() 
{
  StatusCode sc( ST::TupleAlgBase::initialize() ); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by STThresholdBase

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // Get the conditions
  m_stVetraCfg = tool<ISTVetraCfg>( "STVetraCfg", m_stVetraToolName );
  sc = m_stVetraCfg -> initialize();
    
  // Initialize the detMap and the initial value of the header correction flag
  if ( detType() == "IT" )
  {
    m_detMap = ITNumberToSourceIDMap();
  } else {
    m_detMap = TTNumberToSourceIDMap();
  }
  
  return sc;  
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode STCalibrationTuple::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode STCalibrationTuple::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  StatusCode sc(StatusCode::SUCCESS);

  unsigned int iLink, iStrip, chan, tell1no;
  Tuple tuple = nTuple( m_tupleName );
  STVetraCondition* cond;
  STTell1Board* tell1;
  DeSTSector* sector;
  STChannelID stid;
  STTell1Board::chanPair pair;

  Map::iterator it, End;
  End = m_detMap . end();
  for (it = m_detMap . begin(); it != End; it++)
  {
    cond = m_stVetraCfg -> getSTVetraCond( it -> second ) ;
    tell1 = readoutTool() -> findByBoardID( STTell1ID( it -> second ) );
    tell1no = readoutTool() -> SourceIDToTELLNumber( it -> second );
    
    for (iLink = 0; iLink < 24; iLink++){
      for (iStrip = 0; iStrip < 4*nstrips; iStrip++) {
	chan = iLink * 4*nstrips + iStrip;
	
	pair = tell1->DAQToOffline(0, STDAQ::v4, STDAQ::StripRepresentation(chan));
	stid = pair.first;
	sector = tracker() -> findSector(stid);
	
	if(!sector)
	  continue;

	tuple->column( "tell1", static_cast<int>(it -> second) );
	tuple->column( "tell1no", static_cast<int>(tell1no) );
	tuple->column( "channel", static_cast<int>(chan) );
	tuple->column( "port", static_cast<int>(chan/32) );
	tuple->column( "beetle", static_cast<int>(iLink) );
	tuple->column( "mask", (*(cond->pedestalMask()))[chan] );
	tuple->column( "beetle_mask", (*(cond->disableBeetles()))[chan/128] );
	tuple->column( "pedestal", (*(cond->pedestalValue()))[chan] );
	tuple->column( "hit_threshold", (*(cond->hitThreshold()))[chan] );
	tuple->column( "cms_threshold", (*(cond->cmsThreshold()))[chan] );
	tuple->column( "confirmation_threshold", (*(cond->confirmationThreshold()))[chan/64] );
	tuple->column( "spillover_threshold", (*(cond->spilloverThreshold()))[chan/64] );
	if(chan%32<6){
	  tuple->column( "headercorr_0", (*(cond->headerCorrectionAnalogLink(chan/32)))[8*(chan%32)+0] );
	  tuple->column( "headercorr_1", (*(cond->headerCorrectionAnalogLink(chan/32)))[8*(chan%32)+1] );
	  tuple->column( "headercorr_2", (*(cond->headerCorrectionAnalogLink(chan/32)))[8*(chan%32)+2] );
	  tuple->column( "headercorr_3", (*(cond->headerCorrectionAnalogLink(chan/32)))[8*(chan%32)+3] );
	  tuple->column( "headercorr_4", (*(cond->headerCorrectionAnalogLink(chan/32)))[8*(chan%32)+4] );
	  tuple->column( "headercorr_5", (*(cond->headerCorrectionAnalogLink(chan/32)))[8*(chan%32)+5] );
	  tuple->column( "headercorr_6", (*(cond->headerCorrectionAnalogLink(chan/32)))[8*(chan%32)+6] );
	  tuple->column( "headercorr_7", (*(cond->headerCorrectionAnalogLink(chan/32)))[8*(chan%32)+7] );
	}else{
	  tuple->column( "headercorr_0", 0 );
	  tuple->column( "headercorr_1", 0 );
	  tuple->column( "headercorr_2", 0 );
	  tuple->column( "headercorr_3", 0 );
	  tuple->column( "headercorr_4", 0 );
	  tuple->column( "headercorr_5", 0 );
	  tuple->column( "headercorr_6", 0 );
	  tuple->column( "headercorr_7", 0 );	
	}
	tuple->column( "raw_noise", static_cast<float>(sector->rawNoise(stid)) );
	tuple->column( "cms_noise", static_cast<float>(sector->noise(stid)) );
	tuple->column( "cm_noise", static_cast<float>(sector->cmNoise(stid)) );
	
	tuple->write();
      }
    }
  }

  sc = ST::TupleAlgBase::finalize();
  sc = m_stVetraCfg -> finalize();
  
  return sc;  // must be called after all other actions
}

//=============================================================================
