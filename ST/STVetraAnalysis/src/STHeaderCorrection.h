// $Id: STHeaderCorrection.h,v 1.2 2010-03-10 10:31:49 jluisier Exp $
#ifndef STHeaderCorrection_H
#define STHeaderCorrection_H 1

#include "STThresholdBase.h"

#include "Kernel/STBoardMapping.h"

/** @class STHeaderCorrection STHeaderCorrection.h
 *
 *  Algorithm to calculate the header corrections. The header corrections are
 *  determined based on the configuration of the last two header bits of each
 *  port and the parity (odd/even) of the PCN. This means that in total there
 *  are eight possible configurations: 000, 001, 010, etc, where the first bit
 *  (MSB) is the LSB of the PCN and the last two bit are the last two header
 *  bits of each port. The output of this algorithm is a TELL1 config (cfg)
 *  file and a corresponding xml file. The main job options are:
 *  - \b InputLocation: The location in the TES with the input data. Ideally,
 *                      pedestal subtracted data should be chosen here:
 *                      "Raw/TT/PedSubADCs".
 *  - \b DetType: String to switch between "IT" and "TT".
 *  - \b HeaderThreshold: Threshold in ADC counts to determine whether the
 *                        header bit is a logical one or zero. Default is 129.
 *  - \b SubtractCM: Flag to subtract the common-mode. This flag needs to be
 *                   true when the InputLocation is Full or PedSubADCs. When
 *                   running over LCMSADCs this flag can be set to false.
 *  - \b NumChannels: Number of channels for which to calculate the header
 *                    correction. Default value is 6.
 *  - \b ProduceHistos: Flag to produce the histograms. The default value is
 *                     \e false.
 * 
 *  @author J. van Tilburg
 *  @date   20.11.2009
 */

class STHeaderCorrection : public STThresholdBase {

public:
 
  /// constructer
  STHeaderCorrection( const std::string& name, ISvcLocator *svcloc );

  /// initialize
  StatusCode initialize() override;

  /// execute
  StatusCode execute() override;

  /// initialize
  StatusCode finalize() override;


private:

  // Local vectors for given TELL1
  typedef std::map<int, std::vector<std::vector<double> > > DataMap;  
  DataMap m_meanMap;            ///< Internal map for the pedestals

  /// Internal map of number of events per tell1 and FPGA-PP
  std::map<int, std::vector<std::vector<int> > > m_nEvents;

  // job options
  int m_hdr_threshold;       ///< Threshold to determine header bit is 0 or 1.
  unsigned int m_numChannels;///< Number of channels to apply correction.
  bool m_subtractCM;         ///< Flag for doing common-mode subtraction.
  bool m_pedestalfromconddb; ///< Flag for taking pedestal from CONDDB. 
  bool m_produceHistos;      ///< Flag for producing histograms.
  bool m_secondheaderbit;    ///< Flag for calibrating the second header correction.
  std::vector< unsigned int > m_initialHdrSubValues;
  STBoardMapping::Map m_detMap;
};

#endif // STHeaderCorrection_H
