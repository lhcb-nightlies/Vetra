// $Id: STThresholdFinder.cpp,v 1.18 2009-11-19 18:18:00 szumlat Exp $
// Include files

// local
#include "STThresholdFinder.h"
#include "DetDesc/Condition.h"
#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

using namespace std;
using namespace AIDA;
using namespace LHCb;
using namespace ST;


//-----------------------------------------------------------------------------
// Implementation file for class : STThresholdFinder
//
// 2008-02-13 : Johan Luisier
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STThresholdFinder )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STThresholdFinder::STThresholdFinder( const std::string& name,
                                      ISvcLocator* pSvcLocator)
  : STThresholdBase ( name , pSvcLocator )
{
  declareProperty("ThresholdsAndValues", m_Thresholds);
}
//=============================================================================
// Destructor
//=============================================================================
STThresholdFinder::~STThresholdFinder() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode STThresholdFinder::initialize() {
  StatusCode sc(STThresholdBase::initialize()); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  info() << "The data will be retrieved from " << m_DataLoc << endmsg;

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode STThresholdFinder::execute()
{
  StatusCode sc( SUCCESS );
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  
  if (!initDone)
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "Finalizing the setting up of histograms" << endmsg;
    // STThresholdBase::initHistograms is called
    sc = initHistograms();
    if ( sc.isRecoverable() )
    {
      if ( msgLevel(MSG::DEBUG) )
        debug() << "Got an empty container, assuming round robin data" << endmsg;
      return StatusCode::SUCCESS;
    }
    else if (sc.isFailure())
      return sc;
    if ( initDone && msgLevel( MSG::DEBUG) )
      debug() << "Histograms are finaly set up!" << endmsg;
  }

  // Get the data
  sc = getDatas( m_Datas, m_DataLoc );
  
  if ( sc.isRecoverable() )
    return StatusCode::SUCCESS;
  else if ( sc.isFailure() )
  {
    fatal() << "Cannot get data" << endmsg;
    return sc;
  }
  
  counter++;
  // If the "convergence limit" is not reached, nothing has to be done
  if (counter <= m_WaitingTill)
    return sc;

  STTELL1Datas::const_iterator Begin(m_Datas -> begin()),
    End(m_Datas -> end());

  map<unsigned int, bool>::const_iterator lnkIt,
    lnkBegin(EnabledLinks.begin()), lnkEnd(EnabledLinks.end());

  if ( msgLevel(MSG::DEBUG) )
    debug() << "There are " << static_cast<unsigned int>(End-Begin)
            << " found TELL1(s)" << endmsg;
  
  vector<signed int> m_data;
  vector<signed int>::const_iterator dataIt, dataBegin, dataEnd;
  unsigned int i, TELL1_nbr;

  for (STTELL1Datas::const_iterator it(Begin); it != End; it++)
  {
    // Loop on TELL1s

    TELL1_nbr = static_cast<unsigned int>((*it) -> key());
    
    if (!EnabledTELL1s[TELL1_nbr])
    {
      if ( msgLevel(MSG::VERBOSE) )
        verbose() << "Skipping TELL1 " << TELL1_nbr << endmsg;
      continue;
    }

    for (i = 0; i < 24; i++)
    {
      // Loop on optical links

      lnkIt = EnabledLinks.find(TELL1_nbr * 100000 + i * 1000);
      
      if (!lnkIt -> second || lnkIt == lnkEnd)
      {
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "Skipping link " << i << " of TELL1 " << TELL1_nbr << endmsg;
        continue;
      }

      m_data = (*it) -> beetleData(i);

      dataBegin = m_data.begin();
      dataEnd   = m_data.end();

      histoTitle = "TELL1 " + toString(TELL1_nbr,3);
      histoName  = toString(TELL1_nbr,3);
      profTitle  = histoTitle;
      profName   = histoName + "prof";
      for (dataIt = dataBegin; dataIt != dataEnd; dataIt++)
      {
        // Loop on data          
        profile1D(i*128 + static_cast<unsigned int>(dataIt - dataBegin),
                  *dataIt, profName, profTitle,
                  -.5, 3071.5, 3072, "s");
      }
    }
  }

  return sc;
}
  
//=============================================================================
//  Finalize
//=============================================================================
StatusCode STThresholdFinder::finalize()
{
  StatusCode sc(SUCCESS);

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  if (!m_ProduceFile)
    return STThresholdBase::finalize();
  
  string litID, name, title;
  unsigned int str_size, tell1, chan, i, j;

  vector<ofstream*> out;
  str_size = m_Thresholds.size() / 3;
  
  for (i = 0; i < str_size; i++)
  {
    name = m_FileName + m_Thresholds[3 * i].substr(0,3)
      + m_Thresholds[3 *i + 1];
    out.push_back(new ofstream(name.c_str()));
    if (out[i] -> fail())
      return Error("Cant open file " + name + "for writing");
  }

  vector< vector< double > > Noises;
  vector< double > tmp(3072, 0.);
  
  Noises.assign(NumberOfTELL1s, tmp);

  map<unsigned int, unsigned int>::const_iterator mapIt,
    mapBegin(ManageTELL1s.begin()), mapEnd(ManageTELL1s.end());

  sc = GetConditions();

  if (sc.isFailure())
    return sc;

  const vector<int> *Mask;

  /*
   * Now we're creating some "condition like" vectors for the thresholds
   */

  AIDA::IProfile1D* h;

  const Profile1DMapID& histos(profile1DMapID());
  // iterate over the map!
  for (mapIt = mapBegin; mapIt != mapEnd; mapIt++)
  {
    tell1 = mapIt -> first;

    litID = toString(tell1,3) + "prof";
    
    h = histos[litID];

    if ( msgLevel(MSG::DEBUG) ) debug() << "TELL1 " << tell1 << endmsg;

    // Must here collect the noise values...
    for (chan = 0; chan < 3072; chan++)
      Noises[mapIt -> second][chan] = h -> binRms(chan);
    
  }

  vector<string>::const_iterator vecIt, vecBegin(m_Thresholds.begin()),
    vecEnd(m_Thresholds.end());
  double sigma, value;
  unsigned int granularity;

  string CondName("Pedestal_mask");

  for (vecIt = vecBegin; vecIt != vecEnd; vecIt+=3)
  {
    j = static_cast<unsigned int>(vecIt - vecBegin) / 3;
    debug() << "File " << j << endmsg;
    // Now, writes the values in the correct order...
    for (mapIt = mapBegin; mapIt != mapEnd; mapIt++)
    {
      debug() << "TELL1 " << mapIt -> first << endmsg << flush;
      //       sc = GetCondValues(const_cast<unsigned int&>(mapIt -> first),
      //                          CondName, Mask);

      //       if (sc.isFailure())
      //         return sc;

      Mask =
        m_stVetraCfg -> getSTVetraCond( mapIt -> first ) -> pedestalMask();

      (*out[j]) << "TELL1Board" << toString(mapIt -> first) << endl;
      (*out[j]) << "<paramVector name=\"" << *vecIt
                << "\" type=\"int\" comment=\"Comment has to be added\"> ";
      fromString(*(vecIt + 1), sigma);
      fromString(*(vecIt + 2), granularity);
      if ( msgLevel(MSG::VERBOSE) )
        verbose() << *vecIt << " sigma value : " << sigma << ", granularity :"
                  << granularity << endmsg;
      value = 0.;
      for (i = 0; i < 3072; i++)
      {
        if (granularity == 3072)
        {
          if (*vecIt == "Hit_threshold" && Mask -> at(i) == 1)
            (*out[j]) << "50 " << flush;
          else
            (*out[j]) << static_cast<int>(sigma * Noises[mapIt -> second][i] + .5) << " " << flush;
        }
        
        else if ((i + 1) % (3072 / granularity) == 0)
        {
          value += Noises[mapIt -> second][i];
          value *= static_cast<double>(granularity) / 3072.;
          (*out[j]) << static_cast<int>(sigma * value + .5) << " " << flush;
          value = 0.;
        }
        else
          value += Noises[mapIt -> second][i];
      }
      
      (*out[j]) << "</paramVector>" << endl << flush;
    }
  }

  str_size = m_Thresholds.size() / 3;
  for (i = 0; i < str_size; i++)
  {
    out[i] -> close();
    delete out[i];
  }
  

  return STThresholdBase::finalize();  // must be called after all other actions
}

//=============================================================================
