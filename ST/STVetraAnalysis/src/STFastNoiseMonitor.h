// $Id: STFastNoiseMonitor.h,v 1.8 2010-03-10 10:31:49 jluisier Exp $
#ifndef STFASTNOISEMONITOR_H 
#define STFASTNOISEMONITOR_H 1

// Include files
// from Gaudi
#include "STThresholdBase.h"

#include <map>
#include <vector>

class DeSTSector;

/** @class STFastNoiseMonitor STFastNoiseMonitor.h
 *
 * This class is meant to compute the noise in every strip,
 * and then build a xml file containing the noise level for
 * each sector.
 *
 *  @author Dupertuis Frederic
 *  @date   2011-10-10
 */

struct EvolutiveStat{
  public:
  EvolutiveStat(){
    mean = 0.0;
    moment2 = 0.0; 
    count = 0;
  };
  
  void Add(double const& value){
    count++;
    mean = (mean*(count-1)+value)/count;
    moment2 = (moment2*(count-1)+value*value)/count;
  }
  
  double mean;
  double moment2;
  int count;
};

class STFastNoiseMonitor : public STThresholdBase
{
public: 
  /// Standard constructor
  STFastNoiseMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~STFastNoiseMonitor( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:
  StatusCode updateStats( LHCb::STTELL1Datas* container, const std::string& prefixName = "" );

  typedef std::map< unsigned int, std::vector< EvolutiveStat > > EvolutiveStatMap;
private:
  EvolutiveStatMap PedSubStatMap;
  EvolutiveStatMap LCMSStatMap;

  /**
   * Storage of noise values vs sectors
   */
  std::map< DeSTSector*, std::vector<double> > NoisePerSector;
  /**
   * Storage of common mode  values vs sectors
   */
  std::map< DeSTSector*, std::vector<double> > CMPerSector;
  /**
   * Storage of capacitance vs sectors
   */
  std::map< DeSTSector*, double > Capacitances;
  /**
   * Strings used for optimization purpose.
   */
  std::string profTitle, profName;

  double m_constantFactor, /**< Constant in noise = A * C + B  ie this is B */
    m_slopeFactor; /**< Slope in noise = A * C + B  ie this is A */

  /**
   * Number of decimals used when writing the noise into DDDB, default = 2
   */
  unsigned int m_digits;

  /**
   * Toggles on / off the electron per ADC production
   */
  bool m_ElectronsPerADC;

  /**
   * Location of LCMS data
   */
  std::string m_lcmsLoc;
  
  /**
   * Location of ped subtracted data
   */
  std::string m_pedsubtractedLoc;
  
  /**
   * Input data (optimization)
   */
  LHCb::STTELL1Datas* m_pedDatas;

};
#endif // STNOISEMONITOR_H
