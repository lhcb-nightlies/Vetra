// $Id: STTimeScan.cpp,v 1.9 2009-02-17 10:46:30 jvantilb Exp $

// Gaudi
#include "GaudiKernel/AlgFactory.h"

// LHCbKernel
#include "Kernel/STDetSwitch.h"
#include "Kernel/STDAQDefinitions.h"

// local
#include "STTimeScan.h"

//using namespace LHCb;

#include "Event/STTELL1EventInfo.h"
#include "Event/ODIN.h"

#include "Kernel/ISTReadoutTool.h"
#include "Kernel/STTell1Board.h"
#include "Kernel/BeetleRepresentation.h"

#include "Kernel/LHCbConstants.h"

#include "STDet/DeSTSector.h"

//standard
#include <iostream>
#include <sstream>
#include <string>

#include "boost/lexical_cast.hpp"


using namespace LHCb;

DECLARE_ALGORITHM_FACTORY( STTimeScan)

//--------------------------------------------------------------------
//
//  STPerformanceMonitor : Monitor the STPerformances
//
//--------------------------------------------------------------------

STTimeScan::STTimeScan( const std::string& name, ISvcLocator* pSvcLocator ) :
  ST::HistoAlgBase(name, pSvcLocator)
{
  m_dataLocations.push_back( STTELL1DataLocation::TTFull );

  // constructer
  declareProperty("InputData", m_dataLocations );
  // Standard constructor, initializes variables
  declareProperty("FirstPulsedChannel", m_firstPulsedChannel = 3 );
  declareProperty("ChannelPeriodicity", m_channelPeriodicity = 8 );
  declareProperty("Pedestal", m_pedestal = 133.5);
  declareProperty("TimeRangeMin", m_rangeMin = 0.0);
  declareProperty("TimeRangeMax", m_rangeMax = 150.0);
  declareProperty("NBins", m_nBins = 75);
  declareProperty("StepSize", m_stepSize = 2);
  declareProperty("SpillSize", m_spillSize = 25);
}

STTimeScan::~STTimeScan()
{
  // destructer
}

StatusCode STTimeScan::initialize()
{
  // Set the top directory to IT or TT.
  if( "" == histoTopDir() ) setHistoTopDir(detType()+"/");

  // Initialize GaudiHistoAlg
  StatusCode sc = ST::HistoAlgBase::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);
    
  std::vector<std::string>::iterator iPath = m_dataLocations.begin();
  for ( ; iPath !=  m_dataLocations.end() ; ++iPath) {
    flip(*iPath);
  }
    
  // initialize counters
  m_prevCalibrationStep = 9999;
  m_event = 0;
  
  return StatusCode::SUCCESS;
}

StatusCode STTimeScan::execute()
{
  // get the ODIN info
  const ODIN* odin = get<ODIN>(ODINLocation::Default);
  unsigned int calibrationStep = odin->calibrationStep();
  m_event = odin->eventNumber();

  // Initialization before each step
  if ( m_prevCalibrationStep != calibrationStep ) {
    // Always assume first event in step is negative (can be corrected offline)
    m_firstPositiveEvent = m_event+1;

    m_prevCalibrationStep = calibrationStep;
  }

  // Loop over the spills
  std::vector<std::string>::const_iterator iPath = m_dataLocations.begin();
  for ( ; iPath !=  m_dataLocations.end() ; ++iPath) {
    // get the clusters
    if (!exist<STTELL1Datas>(*iPath)) continue;
    const STTELL1Datas* data = get<STTELL1Datas>(*iPath);  

    double timeStep = double(calibrationStep) * double(m_stepSize) 
      + double(iPath-m_dataLocations.begin()) * m_spillSize;
    fillProfiles(data, timeStep );
  }
  
  return StatusCode::SUCCESS;
}

void STTimeScan::fillProfiles(const STTELL1Datas* data, double timeStep) 
{
  // loop over the data
  STTELL1Datas::const_iterator iterBoard = data->begin(); 
  for (; iterBoard != data->end() ; ++iterBoard){

    // get the tell board
    int sourceID = (*iterBoard)->TELL1ID();    
    const STTell1Board* board=readoutTool()->findByBoardID(STTell1ID(sourceID));
    // get the data headers 
    const STTELL1Data::Data& dataValues = (*iterBoard)->data();
    for (unsigned int iBeetle= 0; iBeetle < STDAQ::noptlinks; ++iBeetle ){

      // Get the STChannelID
      STTell1Board::chanPair cPair = 
        board->DAQToOffline(0, STDAQ::v2, STDAQ::BeetleRepresentation(iBeetle).toStripRepresentation());

      // Create a name for the service box
      std::string sBox = board->serviceBox(cPair.first);

      // Create a name for the sector type
      const DeSTSector* thisSector = tracker()->findSector(cPair.first);
      if ( !thisSector ) continue;
      std::string sectorType = thisSector->type();

      // Create a name for the readout sector (tell/module)
      std::stringstream ssmodule;
      ssmodule << "tell" << sourceID << "m" << iBeetle/(thisSector->nBeetle());
      std::string module = ssmodule.str();      

      // Loop over the strips in this link
      unsigned int iStrip = 0;
      for ( ; iStrip < LHCbConstants::nStripsInBeetle ; ++iStrip){
        const unsigned int value = dataValues[iBeetle][iStrip];

        // Skip channels that are not used
        if ( value == 0.0 ) continue;

        // Check if the channel is pulsed
        unsigned int pulsed = (iStrip - m_firstPulsedChannel) % 
          m_channelPeriodicity;

        if ( pulsed == 0 ) {
          if ( (m_event-m_firstPositiveEvent)%2 == 0 ) { // positive pulse 
            profile1D(timeStep, value-m_pedestal, sBox+"_plus",
                      m_rangeMin, m_rangeMax, m_nBins, "s");
            profile1D(timeStep, value-m_pedestal, module+"_plus", 
                      m_rangeMin, m_rangeMax, m_nBins, "s");
            profile1D(timeStep, value-m_pedestal, sectorType+"_plus", 
                      m_rangeMin, m_rangeMax, m_nBins, "s");
          } else { // negative pulse
            profile1D(timeStep, value-m_pedestal, sBox+"_minus",
                      m_rangeMin, m_rangeMax, m_nBins, "s");
            profile1D(timeStep, value-m_pedestal, module+"_minus", 
                      m_rangeMin, m_rangeMax, m_nBins, "s");
            profile1D(timeStep, value-m_pedestal, sectorType+"_minus",
                      m_rangeMin, m_rangeMax, m_nBins, "s");
          } 
        } 
      } // strip
    }  // beetle
  } // boards
  return;
}



