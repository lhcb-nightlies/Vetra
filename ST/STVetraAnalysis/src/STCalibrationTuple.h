// $Id: STCalibrationTuple.h,v 1.4 2010-04-06 09:07:14 jluisier Exp $
#ifndef STCALIBRATIONMONITOR_H 
#define STCALIBRATIONMONITOR_H 1

// Include files
#include "Kernel/STTupleAlgBase.h"
#include "GaudiAlg/GaudiTupleAlg.h"

#include<string> 

class ISTVetraCfg;

/** @class STCalibrationTuple STCalibrationTuple.h
 *  
 * This class dumps a tuple with the relevant calibration parameters
 *
 *  @author Frederic Dupertuis
 *  @date   2011-09-21
 */
class STCalibrationTuple : public ST::TupleAlgBase {

public: 
  /// Standard constructor
  STCalibrationTuple( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~STCalibrationTuple( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:
    
private:
  ISTVetraCfg* m_stVetraCfg;
  std::string m_tupleName;
  std::string m_stVetraToolName;
        
  STBoardMapping::Map m_detMap;
};
#endif // STCALIBRATIONMONITOR_H
