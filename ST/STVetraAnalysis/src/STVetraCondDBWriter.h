// $Id: STVetraCondDBWriter.h,v 1.1 2010-03-10 10:31:50 jluisier Exp $
#ifndef STVETRACONDDBWRITER_H 
#define STVETRACONDDBWRITER_H 1

// Include files
// from Gaudi
#include "STThresholdBase.h"


/** @class STVetraCondDBWriter STVetraCondDBWriter.h
 *  
 * Class that writes the ST Vetra Conditions database.
 *
 *  @author Johan Luisier
 *  @date   2010-02-15
 */
class STVetraCondDBWriter : public STThresholdBase
{
public: 
  /// Standard constructor
  STVetraCondDBWriter( const std::string& name, ISvcLocator* pSvcLocator );
  
  virtual ~STVetraCondDBWriter( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  std::string m_author, /**< Author of the file. */
    m_tag, /**< Tag of the file. */
    m_description; /**< Small description of the production conditions. */

};
#endif // STVETRACONDDBWRITER_H
