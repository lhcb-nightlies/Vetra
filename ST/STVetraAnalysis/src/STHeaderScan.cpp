// $Id: STHeaderScan.cpp,v 1.1 2009-02-18 16:36:09 jvantilb Exp $

// Gaudi
#include "GaudiKernel/AlgFactory.h"

// LHCbKernel
#include "Kernel/STDetSwitch.h"
#include "Kernel/STDAQDefinitions.h"
#include "Kernel/LHCbConstants.h"

// Event
#include "Event/ODIN.h"

// standard
#include "boost/lexical_cast.hpp"

// local
#include "STHeaderScan.h"

using namespace LHCb;

DECLARE_ALGORITHM_FACTORY( STHeaderScan)

//--------------------------------------------------------------------
//
//  STHeaderScan : Monitor the shape of the header signal using the
//                 ADC scan w/o test pulses.
//
//--------------------------------------------------------------------

STHeaderScan::STHeaderScan( const std::string& name, 
                            ISvcLocator* pSvcLocator ) :
  ST::HistoAlgBase(name, pSvcLocator)
{
  // constructer
  declareProperty("InputData", m_dataLocation = STTELL1DataLocation::TTFull );
  declareProperty("FirstChannel", m_firstChannel = -3 );
  declareProperty("LastChannel", m_lastChannel = 4 );
  declareProperty("ZeroThreshold", m_zero_threshold = 129);
  declareProperty("OneThreshold", m_one_threshold  = 129);
  declareProperty("Pedestal", m_pedestal = 130.5);
  declareProperty("TimeRangeMin", m_rangeMin = 0.0);
  declareProperty("TimeRangeMax", m_rangeMax = 25.0);
  declareProperty("NBins", m_nBins = 50);
  declareProperty("StepSize", m_stepSize = 0.5);
}

STHeaderScan::~STHeaderScan()
{
  // destructer
}

StatusCode STHeaderScan::initialize()
{
  // Set the top directory to IT or TT.
  if( "" == histoTopDir() ) setHistoTopDir(detType()+"/");

  // Initialize GaudiHistoAlg
  StatusCode sc = ST::HistoAlgBase::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);

  // flip data location
  STDetSwitch::flip(detType(), m_dataLocation);
  
  return StatusCode::SUCCESS;
}

StatusCode STHeaderScan::execute()
{
  // get the ODIN info
  ODIN* odin = get<ODIN>(ODINLocation::Default);
  unsigned int calibrationStep = odin->calibrationStep();

  // get the clusters
  if (!exist<STTELL1Datas>(m_dataLocation)) 
    return Error("No valid data at " + m_dataLocation);
  const STTELL1Datas* data = get<STTELL1Datas>(m_dataLocation);  

  double timeStep = double(calibrationStep) * double(m_stepSize);
  fillProfiles(data, timeStep );
  
  return StatusCode::SUCCESS;
}

void STHeaderScan::fillProfiles(const STTELL1Datas* data, double timeStep) 
{
  unsigned int portsPerBeetle = 4;
  int stripsPerPort = LHCbConstants::nStripsInBeetle/portsPerBeetle;

  // loop over the data
  STTELL1Datas::const_iterator iterBoard = data->begin(); 
  for (; iterBoard != data->end() ; ++iterBoard){

    // get the data headers 
    const STTELL1Data::Data& header = (*iterBoard) -> header();
    const STTELL1Data::Data& dataValues = (*iterBoard)->data();

    for (unsigned int iBeetle= 0; iBeetle < STDAQ::noptlinks; ++iBeetle ){
 
      // Loop over the strips in this link
      unsigned int iPort = 0;
      for ( ; iPort < portsPerBeetle ; ++iPort ) {

        // Skip not used ports (check only channel 0)
        if ( dataValues[iBeetle][stripsPerPort*iPort] == 0.0 ) continue;

        // Get the header bits
        std::string headStr("");
        int head[3];
        for (int iHead = 0; iHead < 3; ++iHead) {
          head[iHead] = header[iBeetle][iPort*3+iHead]; 
          if (head[iHead] >= m_one_threshold) headStr += "1";
          else if (head[iHead] < m_zero_threshold) headStr += "0";
          else  headStr += "n";
        }

        int channel = (m_firstChannel > -3) ? m_firstChannel : -3 ;
        for ( ; channel <= m_lastChannel; ++channel ) {
          if ( channel < 0 ) {
            profile1D(timeStep, head[3+channel]-m_pedestal,
                      headStr+"_Ch"+boost::lexical_cast<std::string>(channel),
                      m_rangeMin, m_rangeMax, m_nBins);
          } else if ( channel < 32 ) {
            // Get ADC value
            unsigned int iStrip = channel + stripsPerPort*iPort;          
            unsigned int value = dataValues[iBeetle][iStrip];

            profile1D(timeStep, value-m_pedestal, 
                      headStr+"_Ch"+boost::lexical_cast<std::string>(channel),
                      m_rangeMin, m_rangeMax, m_nBins);
          }
        } 
      } // port
    }  // beetle
  } // boards
  return;
}
