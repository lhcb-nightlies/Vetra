// $Id: STNZSResolution.cpp,v 1.3 2009/12/05 23:11:58 jvantilb Exp $

// Gaudi
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/SystemOfUnits.h"

// Event
#include "Event/Track.h"
#include "Event/Node.h"
#include "Event/FitNode.h"
#include "Event/STMeasurement.h"

// LHCbMath
#include "LHCbMath/LineTypes.h"
#include "LHCbMath/GeomFun.h"

// LHCbKernel
#include "Kernel/LHCbConstants.h"
#include "Kernel/ISTReadoutTool.h"

// STTELL1Event
#include "Event/STTELL1Data.h"

// DigiEvent
#include "Event/ODIN.h"
#include "Event/STCluster.h"

// STDet
#include "STDet/DeSTSector.h"

// local
#include "STNZSResolution.h"

using namespace LHCb;
using namespace LHCbConstants;
using namespace Gaudi::Units;
using namespace Gaudi;
using namespace Gaudi::Math;

DECLARE_ALGORITHM_FACTORY( STNZSResolution)

//--------------------------------------------------------------------
//
//  STNZSResolution: Extract the pulse shape from the NZS data.
//
//      More details can be found in STNZSResolution.h
//
//--------------------------------------------------------------------

STNZSResolution::STNZSResolution( const std::string& name, 
                                ISvcLocator* pSvcLocator ) :
  ST::TupleAlgBase(name, pSvcLocator)
{
  // constructer
  declareSTConfigProperty("ClusterLocation", m_clusterLocation, 
                          LHCb::STClusterLocation::TTClusters);

  declareSTConfigProperty("InputData" , m_dataLocation, "Raw/TT/LCMSADCs");
  declareProperty("TrackLocation", m_trackLocation = TrackLocation::Default );
  declareProperty("ChargeCut",     m_chargeCut      = -1.0 );// ADC counts
  declareProperty("BunchID",       m_bunchID               );// BunchID 
  declareProperty("UseNZSdata",    m_useNZSdata     = true );  
  declareProperty("ClusteringCut", m_clusteringCut  = 2.5  );
}

StatusCode STNZSResolution::initialize()
{
  // Initialize ST::HistoAlgBase
  StatusCode sc = ST::TupleAlgBase::initialize();
  if (sc.isFailure()) return sc;

  m_poca = tool<ITrajPoca>( "TrajPoca" );

  // initialize event counter
  m_evtNumber = 0;

  m_extrapolator = tool<ITrackExtrapolator>( "TrackParabolicExtrapolator" );

  m_pIMF = svc<IMagneticFieldSvc>( "MagneticFieldSvc", true );

  return StatusCode::SUCCESS;
}

StatusCode STNZSResolution::execute()
{
  m_evtNumber++;

  // Create an ntuple
  Tuple tuple = nTuple("NZSResTuple");

  // Select the correct bunch id
  const ODIN* odin = get<ODIN> ( ODINLocation::Default );
  if( !m_bunchID.empty() && 
      std::find(m_bunchID.begin(), m_bunchID.end(), 
                odin->bunchId()) == m_bunchID.end()) return StatusCode::SUCCESS;

  const unsigned int bunchId = odin->bunchId();
  // Check track location exists
  if(!exist<Tracks>(m_trackLocation)) return StatusCode::SUCCESS;
  Tracks* tracks = get< LHCb::Tracks >( m_trackLocation );

  // Get the NZS data
  STTELL1Datas* dataNZS = 0;
  if( m_useNZSdata ) {
    if( !exist<STTELL1Datas>(m_dataLocation) ) return StatusCode::SUCCESS;
    dataNZS = get<STTELL1Datas>(m_dataLocation);
    if( dataNZS->size() == 0 ) return StatusCode::SUCCESS;
  }
  
  // Get the STClusters
  STClusters* clusters = get<STClusters>( m_clusterLocation );
  const unsigned int nClusters = clusters->size();

  counter("Number of selected events") += 1;

  // Loop over the tracks
  Tracks::const_iterator iTr = tracks -> begin();
  for( ; iTr != tracks->end(); ++iTr){

    // Loop over the nodes in this track
    Track::ConstNodeRange::const_iterator iNode = ((*iTr)->nodes()).begin();
    int nSTMeas = 0;
    for ( ; iNode != ((*iTr)->nodes()).end(); ++iNode) {

      // Check if this node belongs to a TT or IT cluster
      if( !(*iNode)->hasMeasurement() ) continue;
      const Measurement& meas = (*iNode)->measurement();
      if( (detType() == "TT" && meas.type() == Measurement::TT) ||
          (detType() == "IT" && meas.type() == Measurement::IT) ) {
        ++nSTMeas;
        // Get the TELL1 source ID
        const STMeasurement& stMeas = dynamic_cast<const STMeasurement&>(meas);
        const STCluster* cluster = stMeas.cluster();
        unsigned int sourceID = cluster->sourceID();
        unsigned int tell1ID = readoutTool()->SourceIDToTELLNumber(sourceID);

        // Check if the decoded NZS bank corresponds to this cluster
        STTELL1Data* boardData = 0;
        if( m_useNZSdata ) {
          boardData = dataNZS->object(sourceID);
          if( boardData == 0 ) continue;
          debug() << "Found NZS data " << endmsg;
        }
        
        // get the unbiased state
        const FitNode& node = dynamic_cast<const FitNode&>(**iNode);        
        State ubs(node.unbiasedState());

        // Get the trajectory for the channel ID belonging to the cluster
        STChannelID chanID = cluster->channelID();
        unsigned int sectorID = chanID.uniqueSector();
        DeSTSector* sector = findSector( chanID );
        
        // Get the noise corresponding to this cluster
        double noise = sector->noise( chanID );

        // Extrapolate unbiased state to intersection plane
        double oldZ = ubs.z();
        m_extrapolator->propagate( ubs, sector->middleSensor()->plane() );

        // find the sensor and the intersection point with the plane
        const DeSTSensor* sensor = sector->findSensor( ubs.position() );
        if( sensor == 0 ) continue;
        m_extrapolator->propagate( ubs, sensor->plane(), 0.001*mm );

        double localUtrack = (sensor->toLocal( ubs.position() )).x();
        unsigned int strip = sensor->localUToStrip(localUtrack);
        double localUstrip = sensor->localU( strip, 0.0 );
        double expIsf      = (localUtrack - localUstrip)/sensor->pitch();
        // Allow only expected interstrip fractions in range [0:1]
        // (i.e. use tell1 numbering order)
        if( expIsf < 0 ) {
          expIsf = expIsf + 1.0;
          strip += 1u;
        }

        // Determine Lorentz correction sign
        Gaudi::XYZVector globalTx(1.0, 0.0, 1.0);
        Gaudi::XYZPoint p1 = (sensor->toLocal( ubs.position() ));
        Gaudi::XYZPoint p2 = (sensor->toLocal( ubs.position()+globalTx ));
        double localTx = (p2.x() - p1.x())/(p2.z() - p1.z()) ;
        int lorentzSign = (localTx < 0) ? 1 : -1 ;

        // Check if the strip number is not unphysical.
        // Note that this is not a pathological case, since the strip number
        // is defined by the track position.
        if( strip < 1u || strip > sector->nStrip() ) continue;

        // Get the expected channel (offline and online)
        STChannelID expChan = sector->stripToChan(strip);
        unsigned int iTell1Chan = 
          readoutTool()->offlineChanToDAQ(expChan, 0.0).second;

        // Check if there are further hits close
        unsigned int isoFound = nHitsInWindow(clusters, expChan);
        // Work out distance to nearest hit on sector
        unsigned int nextCluster = distanceToNextCluster(clusters, expChan);

        // Check if strips are not too close to the edge of Beetle
        // todo: check if this is needed (maybe PP edge is enough)
        if( int((iTell1Chan-5)/nStripsInBeetle) !=
            int((iTell1Chan+6)/nStripsInBeetle) ) continue;

        // Get the charges on the neighbouring strips
        double charge[6] = {0,0,0,0,0,0};
        double totCharge(0), recIsf(0);
        if( m_useNZSdata ) {
          // Get the raw NZS data
          const STTELL1Data::Data& dataValues = boardData->data();
          unsigned int iBeetle = iTell1Chan/nStripsInBeetle;
          charge[0] = dataValues[iBeetle][(iTell1Chan-2)%nStripsInBeetle];
          charge[1] = dataValues[iBeetle][(iTell1Chan-1)%nStripsInBeetle];
          charge[2] = dataValues[iBeetle][iTell1Chan%nStripsInBeetle];
          charge[3] = dataValues[iBeetle][(iTell1Chan+1)%nStripsInBeetle];
          charge[4] = dataValues[iBeetle][(iTell1Chan+2)%nStripsInBeetle];
          charge[5] = dataValues[iBeetle][(iTell1Chan+3)%nStripsInBeetle];
          totCharge = charge[1]+charge[2]+charge[3]+charge[4];
          recIsf = (totCharge == 0.0) ? 0.0 : 
            (charge[3]+2*charge[4]-charge[2])/totCharge;
        }
        // Get the STCluster
        STCluster::ADCVector adcs = cluster->stripValues();
        STCluster::ADCVector::const_iterator iADC = adcs.begin();
        double clusADCs[6]={0, 0, 0, 0, 0, 0};
        //int iiii[6] = {-2, -1, 0, 1, 2, 3};
        int clusNOut=0;
        for( ; iADC != adcs.end(); ++iADC ) {
          int offset = (*iADC).first;
          STChannelID thisChan = STChannelID(chanID + offset);
          unsigned int thisTell1Chan = 
            readoutTool()->offlineChanToDAQ(thisChan, 0.0).second;
          int i = thisTell1Chan - iTell1Chan;
          if( i > -3 && i < 4  ) {
            clusADCs[i+2] = (*iADC).second;
          } else clusNOut+=1;
        }

        // Do some simple clustering myself
        double myClusCharge = 0.0;
        if( charge[0]> int(m_clusteringCut*noise+0.5)) myClusCharge +=charge[0];
        if( charge[1]> int(m_clusteringCut*noise+0.5)) myClusCharge +=charge[1];
        if( charge[2]> int(m_clusteringCut*noise+0.5)) myClusCharge +=charge[2];
        if( charge[3]> int(m_clusteringCut*noise+0.5)) myClusCharge +=charge[3];
        if( charge[4]> int(m_clusteringCut*noise+0.5)) myClusCharge +=charge[4];
        if( charge[5]> int(m_clusteringCut*noise+0.5)) myClusCharge +=charge[5];
        
        // Determine the layer
        int layer = (cluster->isTT()) ? 
          cluster->layer()+2*(cluster->station()-1) : cluster->layer();

        // Get the magnetic field
        Gaudi::XYZVector bField;
        Gaudi::XYZPoint  aPoint( ubs.x(), ubs.y(), ubs.z() );
        m_pIMF->fieldVector( aPoint, bField );

        // save in ntuple
        tuple->column("chi2", (*iTr)->chi2PerDoF() );
        tuple->column("type", (*iTr)->type() );
        tuple->column("p"   , (*iTr)->p() );
        tuple->column("ghostP"   , (*iTr)->ghostProbability() );
        tuple->column("llh"   , (*iTr)->likelihood() );
        tuple->column("x" , ubs.x()  );
        tuple->column("y" , ubs.y()  );
        tuple->column("z" , ubs.z()  );
        tuple->column("oldZ" , oldZ  );
        tuple->column("ex" , sqrt(ubs.errX2())  );
        tuple->column("qOverP" , ubs.qOverP()  );
        tuple->column("tx"  , ubs.tx() );
        tuple->column("chi2Local"  , node.chi2() );
        tuple->column("res"  , node.residual() );
        tuple->column("ubres"  , node.unbiasedResidual() );
        tuple->column("tell1Chan", iTell1Chan );
        tuple->column("sourceID", sourceID );
        tuple->column("tell1ID", tell1ID );
        tuple->column("sectorID", sectorID );
        tuple->column("layer", layer );
        tuple->column("nSensors", (sector->sensors()).size() );
        tuple->column("thickness", sector->thickness() );
        tuple->column("isTT", cluster->isTT() );
        tuple->column("pitch", sensor->pitch() );
        tuple->column("expIsf", expIsf );
        tuple->column("strip", strip );
        tuple->column("clusSize", cluster->size() );
        tuple->column("clusCharge", cluster->totalCharge() );
        tuple->column("myClusCharge", myClusCharge );
        tuple->column("chargeP2", charge[0] );
        tuple->column("chargeP1", charge[1] );
        tuple->column("charge", charge[2]   );
        tuple->column("chargeN1", charge[3] );
        tuple->column("chargeN2", charge[4] );
        tuple->column("chargeN3", charge[5] );
        tuple->column("recIsf", recIsf );
        tuple->column("clusADCP2", clusADCs[0] );
        tuple->column("clusADCP1", clusADCs[1] );
        tuple->column("clusADC", clusADCs[2]   );
        tuple->column("clusADCN1", clusADCs[3] );
        tuple->column("clusADCN2", clusADCs[4] );
        tuple->column("clusADCN3", clusADCs[5] );
        tuple->column("clusNOut", clusNOut );// # strips outside range...
        tuple->column("isOk", sector->isOKStrip( chanID )  ); 
        tuple->column("isoFound", isoFound );
        tuple->column("distNext", nextCluster );
        tuple->column("Bx", bField.x() );
        tuple->column("By", bField.y() );
        tuple->column("Bz", bField.z() );
        tuple->column("B", bField.R() );
        tuple->column("lorentzSign", lorentzSign );
        tuple->column("nClusters", nClusters );
        tuple->column("bunchId", bunchId );
        
        tuple->write();
      }
    }// end of nodes
  } // End of track iterator

  return StatusCode::SUCCESS;
}

StatusCode STNZSResolution::finalize()
{
  return ST::TupleAlgBase::finalize();// must be called after all other actions
}

unsigned int STNZSResolution::nHitsInWindow(const STClusters* clusters,
                                            const STChannelID channelID)
{
  unsigned int isoFound = 0;
  LHCb::STClusters::const_iterator jClus = clusters -> begin();
  for ( ; jClus != clusters->end(); ++jClus ){

    // count hits which are closer than 30 strips (about 5 mm)
    if( fabs( (*jClus)->channelID() - channelID ) < 30 ) ++isoFound;
  }  

  return isoFound;
  
}

unsigned int STNZSResolution::distanceToNextCluster(const STClusters* clusters,
                                                    const STChannelID channelID)
{
  unsigned int closestFound = 0;
  LHCb::STClusters::const_iterator jClus = clusters -> begin();
  std::vector<int> distances;
  for ( ; jClus != clusters->end(); ++jClus ){
    if( (*jClus)->channelID().uniqueSector() == channelID.uniqueSector() ) {
      distances.push_back( fabs((*jClus)->channelID() - channelID) );
    }  
  }
  if(distances.size() > 1) {
    std::sort(distances.begin(), distances.end());
    closestFound = distances[1];
  }
  return closestFound;
  
}
