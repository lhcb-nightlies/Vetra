// $Id: STThresholdFinder.h,v 1.4 2008-10-21 12:43:17 jluisier Exp $
#ifndef STTHRESHOLDFINDER_H 
#define STTHRESHOLDFINDER_H 1

// Include files
#include "STThresholdBase.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"

class Condition;

/** @class STThresholdFinder STThresholdFinder.h
 * 
 * Class computing the noise in any TELL1 container you want, ie
 * Full, PedSubADCs, ...
 * It uses the noise value to set thresholds
 *
 *  @author Johan Luisier
 *  @date   2008-01-10
 */
class STThresholdFinder : public STThresholdBase {
public: 
  /// Standard constructor
  STThresholdFinder( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~STThresholdFinder( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  /**
   * Contains the name of the condition we want to set, the sigma value
   * and the number of thresholds that have to be set ie :
   * "CMS_threshold", "4", "3072"
   * "Cluster_sum_threshold", "5", "48"
   */
  std::vector<std::string> m_Thresholds;
  /**
   * Strings used for optimization purpose.
   */
  std::string profTitle, profName;
};
#endif // STTHRESHOLDFINDER_H
