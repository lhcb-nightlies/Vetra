// $Id: STHeaderCorrection.cpp,v 1.4 2010-03-10 10:31:49 jluisier Exp $

// Gaudi
#include "GaudiKernel/AlgFactory.h"

// LHCbKernel
#include "Kernel/STDAQDefinitions.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/ISTReadoutTool.h"

// VetraKernel
#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

// standard
#include "gsl/gsl_math.h"
#include <fstream>
#include <iostream>

// local
#include "STHeaderCorrection.h"

using namespace std;
using namespace LHCb;
using namespace STDAQ;
using namespace LHCbConstants;
using namespace ST;
using namespace STBoardMapping;

DECLARE_ALGORITHM_FACTORY( STHeaderCorrection)

//--------------------------------------------------------------------
//
//  STHeaderCorrection : See header file for detailed description. 
//
//--------------------------------------------------------------------

STHeaderCorrection::STHeaderCorrection( const string& name, 
                                        ISvcLocator* pSvcLocator ) :
  STThresholdBase(name, pSvcLocator)
{
  // constructer
  //declareSTConfigProperty("InputLocation", m_DataLoc, "Raw/IT/Full"      );
  declareProperty("HeaderThreshold",       m_hdr_threshold  = 129        );
  declareProperty("NumChannels",           m_numChannels    = 6          );
  declareProperty("SubtractCM",            m_subtractCM     = true       );
  declareProperty("PedestalFromCONDDB",    m_pedestalfromconddb = true   );
  declareProperty("ProduceHistos",         m_produceHistos  = false      );
  declareProperty("SecondHeaderBit",       m_secondheaderbit  = true     );
}

StatusCode STHeaderCorrection::initialize()
{

  // initialisation of base class has to be done first
  StatusCode sc( STThresholdBase::initialize() );  
  if ( sc.isFailure() )
    return sc;

  sc = GetConditions();
  
  if ( detType() == "IT" ) {
    m_detMap = ITNumberToSourceIDMap();
    m_initialHdrSubValues.assign( 42, 0 );
  } else {
    m_detMap = TTNumberToSourceIDMap();
    m_initialHdrSubValues.assign( 48, 0 );
  }
  
  Map::iterator it, End;
  End = m_detMap . end();
  STVetraCondition * m_stCond;
  // Store the initial value of the updateEnable flag and set it to enable
  for (it = m_detMap . begin(); it != End; it++) {
    m_stCond = m_stVetraCfg -> getSTVetraCond( it -> second );
    
    m_initialHdrSubValues[ it -> first - 1 ] = 
      m_stCond -> headerEnable();
    // Disable the header subtraction in the emulator
    m_stCond -> headerEnable( 0 );
    
    updMgrSvc() -> invalidate( m_stCond );
    sc = updMgrSvc() -> update( m_stCond );

    if(sc.isFailure())
      return sc;
  }

  if(!m_secondheaderbit)
    info() << "No 2nd header bit emulation has been required" << endmsg;

  return sc;
}

StatusCode STHeaderCorrection::execute()
{
  // get the data
  if (!exist<STTELL1Datas>(m_DataLoc)) 
    return Error("No valid data at " + m_DataLoc);
  const STTELL1Datas* data = get<STTELL1Datas>(m_DataLoc);
  
  // get the data with the header (the header is only stored in the raw data)
  string headerLoc = "Raw/" + detType() + "/Full";
  if (!exist<STTELL1Datas>(headerLoc)) 
    return Error("No valid data at " + headerLoc);
  const STTELL1Datas* header = get<STTELL1Datas>(headerLoc);
  
  // loop over the data
  STTELL1Datas::const_iterator iterBoard = data->begin(); 
  STTELL1Datas::const_iterator iterHead  = header->begin();

  for (; iterBoard != data->end() ; ++iterBoard, ++iterHead ) {
    // get the tell board and the data headers
    unsigned int tellID = (*iterBoard)->TELL1ID();
    if( iterHead == header->end() || tellID != (*iterHead)->TELL1ID() ) {
      return Error("Data and header have different source IDs.");
    }

    // Get the conditions for this TELL1
    STVetraCondition* condition = m_stVetraCfg -> getSTVetraCond( tellID );

    const STTELL1Data::Data& headerValues = (*iterHead) ->header();
    const STTELL1Data::Data& dataValues   = (*iterBoard)->data();

    // Reset the maps for each tell1
    if ( m_meanMap.find(tellID) == m_meanMap.end() ) {
      m_meanMap[tellID].resize(9, vector<double>(3072,0.0) );
      m_nEvents[tellID].resize(9, vector<int>(96,0));
    }

    vector<vector<double> >* meanTELL = &m_meanMap[tellID];
    vector<vector<int> >* nEvents = &m_nEvents[tellID];
    
    // Loop over the PPs that have sent data
    vector<unsigned int> sentPPs = (*iterBoard)->sentPPs();
    vector<unsigned int>::const_iterator iPP = sentPPs.begin();
    for( ; iPP != sentPPs.end(); ++iPP ) {
      unsigned int pp = *iPP;
      
      // Loop over the links (i.e. Beetles)
      unsigned int iBeetle = 0;
      for ( ; iBeetle < nBeetlesPerPPx; ++iBeetle ){
        unsigned int beetle = iBeetle + pp*nBeetlesPerPPx;
 
        // Loop over the ports in this link
        for( unsigned int iPort = 0 ; iPort < nports ; ++iPort ) {

          int alink = iPort + beetle*nports ;

          // Get the previous header corrections from the TELL1 conditions
          const std::vector< int >* oldHdrCorr = 0;
          if( condition && condition->headerEnable() ) {
            oldHdrCorr = condition -> headerCorrectionAnalogLink( alink );
          }

          // Get the header bits and build the header configuration
          unsigned int hdr = 0u;
	  if(m_secondheaderbit){
	    if( headerValues[beetle][iPort*3+0] >= m_hdr_threshold ) hdr += 4;
	  }
	  if( headerValues[beetle][iPort*3+1] >= m_hdr_threshold ) hdr += 2;
          if( headerValues[beetle][iPort*3+2] >= m_hdr_threshold ) hdr += 1;

          // Count the number of events per header config per port
          (*nEvents)[hdr][alink]++;
          int nEvt = (*nEvents)[hdr][alink];
          (*nEvents)[8][alink]++;
          int nEvtTot = (*nEvents)[8][alink];

          // Calculate the common mode of this port (simple common mode)
          double slope = 0.0;
          double ave0  = 0.0;
          if( m_subtractCM ) {
            double ave1 = 0.0;
            double ave2 = 0.0;
            for( unsigned int strip=m_numChannels; strip < nstrips ; ++strip ) {
              unsigned int chan  = strip + iPort*nstrips;
              int value = dataValues[beetle][chan];
              
	      if( strip < 16 ) ave1 = (ave1*(strip-m_numChannels) + value) / 
                                 (strip-m_numChannels+1);
              else             ave2 = (ave2*(strip-16) + value)/(strip-16+1);
	      
	    }
            slope = (ave2-ave1)/(0.5*(nstrips-m_numChannels));
            ave0 = ave1 - slope*(7.5+0.5*m_numChannels); 
	    //ave0 = (ave1 + ave2 - slope*(nstrips -1. + m_numChannels/2.))/2.; 
	  }
          
          for( unsigned int strip = 0; strip < m_numChannels ; ++strip ) {
            // Get ADC value
            unsigned int chan  = strip + iPort*nstrips;

            // Correct for the common mode
            double value = dataValues[beetle][chan] - ave0 - slope*strip;

            // Take into account the already applied header correction.
            if( oldHdrCorr ) value += (*oldHdrCorr)[hdr+8*strip];
            chan += beetle*nStripsInBeetle;
             
            // Calculate the average (pedestal) of this channel
            (*meanTELL)[hdr][chan] = ( (*meanTELL)[hdr][chan]*(nEvt-1)
                                       + value ) / nEvt;
            // The last entry (8) is the average over all header configurations
            (*meanTELL)[8][chan] = ( (*meanTELL)[8][chan]*(nEvtTot-1)
                                     + value ) / nEvtTot;

          } // channel
        } // ports
      } // Beetles
    }  // PPs
  } // boards

  return StatusCode::SUCCESS;
}

StatusCode STHeaderCorrection::finalize()
{
  Map::iterator it, End( m_detMap . end() );

  for (it = m_detMap . begin(); it != End; it++) {
    m_stVetraCfg -> getSTVetraCond( it -> second ) 
	-> headerEnable( m_initialHdrSubValues[ it -> first - 1] );
  }
  
  DataMap::const_iterator iTell = m_meanMap.begin();
  vector< int > correctionValues;
  vector< double > correctionValuesTmp;
  const vector<vector<double> >* meanTELL;
  const vector<vector<int> >* nEvents;
  const vector< int >* pedestalMask;
  vector< int > pedestalValues;
  
  // Loop over all Tell1s
  for( ; iTell != m_meanMap.end(); ++iTell ) { 

    correctionValues.assign( 8 * m_numChannels, 0 );
    correctionValuesTmp.assign( 8 * m_numChannels, 0. );
    
    // Loop over strips in tell1
    meanTELL = &((*iTell).second);
    nEvents = &m_nEvents[iTell->first];
    
    pedestalMask = m_stVetraCfg -> getSTVetraCond( iTell->first ) -> pedestalMask();

    int iHead, iHeadref, iHeader, sign_0, sign_1, sign_2, alink;
    double pedestal, pedDiff, correctionTmp;
    bool firstpass(false);
    for (unsigned int strip = 0u; strip < nStripsPerBoard; ++strip) {
      if( strip%32 >= m_numChannels ) continue;
      
      // Get the analog link
      alink = strip/32;
      
      if(m_pedestalfromconddb){
	if( m_DataLoc == "Raw/IT/Full" || m_DataLoc == "Raw/TT/Full" )
	  pedestal = (*(m_stVetraCfg -> getSTVetraCond( iTell -> first ) -> pedestalValue()))[strip];
	else
	  pedestal = 0.;
      }else{
	if((*pedestalMask)[strip] == 0)
	  pedestal = (*meanTELL)[8][strip];
	else
	  pedestal = 0;
      }
      
      if(!firstpass){
	// Loop over header configs
	for( iHeader = 0; iHeader < 4; ++iHeader ) {   
	  if((*nEvents)[iHeader+4][alink] > (*nEvents)[iHeader][alink])
	    iHead = iHeader + 4;
	  else
	    iHead = iHeader;
	  
	  // Calculate the difference with the pedestal
	  if((*pedestalMask)[strip] == 0)
	    correctionValuesTmp[ 8 * (strip % 32) + iHead ] = (*meanTELL)[iHead][strip] - pedestal;
	  else
	    correctionValuesTmp[ 8 * (strip % 32) + iHead ] = 0.;
	}
	
	if(strip%32 < m_numChannels-1){
	  continue;
	}else{
	  strip -= m_numChannels;
	  firstpass = true;
	  continue;
	}
      }
      
      // 2nd header bit emulation
      for( iHeader = 0; iHeader < 4; ++iHeader ) {   
	if((*nEvents)[iHeader+4][alink] > (*nEvents)[iHeader][alink]){
	  //0xx to emulate
	  iHead = iHeader;
	  iHeadref = iHeader + 4;
	}else{
	  //1xx to emulate
	  iHead = iHeader + 4;
	  iHeadref = iHeader;
	}
	
	sign_2 = ((iHead/4)%2==1)?1:-1;
	sign_1 = ((iHead/2)%2==1)?1:-1;
	sign_0 = ((iHead/1)%2==1)?1:-1; sign_0 *= 1;// not used but multiply by one to remove compiler warning
	//sign_2 = ((iHead & 0b100)==0b100)?1:-1;
	//sign_1 = ((iHead & 0b010)==0b010)?1:-1;
	//sign_0 = ((iHead & 0b001)==0b001)?1:-1;
	
	correctionTmp = correctionValuesTmp[ 8 * (strip % 32) + iHeadref ];
	if(m_secondheaderbit){
	  if((*pedestalMask)[strip] == 0){
	    if((alink % 4 != 3) || ((alink % 4 == 3)&&(iHead % 2 != 1))){
	      if(strip % 32 < m_numChannels-1)
		correctionTmp += ((double)(sign_1*sign_2))*(correctionValuesTmp[ 8 * ((strip+1) % 32) + iHeadref ] - correctionValuesTmp[ 8 * ((strip+1) % 32) + iHeadref - sign_1*2 ]);
	    }else{
	      if(iHead % 4 != 3)
		if(strip % 32 < m_numChannels-2)
		  correctionTmp += ((double)(sign_2))*(correctionValuesTmp[ 8 * ((strip+2) % 32) + iHeadref ] - correctionValuesTmp[ 8 * ((strip+2) % 32) + iHeadref - 1 ]);
	    }
	  }
	  correctionValuesTmp[ 8 * (strip % 32) + iHead ] = correctionTmp;
	}else{
	  correctionValuesTmp[ 8 * (strip % 32) + iHead ] = correctionTmp;
	}
      }
      // Pedestal recentering
      if(!m_pedestalfromconddb){
	pedDiff = 0.;
	if(m_secondheaderbit){
	  for( iHead = 0; iHead < 8; ++iHead ) {   
	    pedDiff += correctionValuesTmp[ 8 * (strip % 32) + iHead ];
	  }
	  pedDiff /= 8.;
	  pedestal += pedDiff;
	}
	pedestalValues = *(m_stVetraCfg -> getSTVetraCond( iTell -> first ) -> pedestalValue());
	if( m_DataLoc == "Raw/IT/Full" || m_DataLoc == "Raw/TT/Full" )
	  pedestalValues[strip] = int(floor(pedestal+0.5));
	else
	  pedestalValues[strip] += int(floor(pedestal+0.5));
	m_stVetraCfg -> getSTVetraCond( iTell -> first ) -> pedestalValue(pedestalValues);
	pedDiff += int(floor(pedestal+0.5))-pedestal;
	for( iHead = 0; iHead < 8; ++iHead ) {   
	  correctionValuesTmp[ 8 * (strip % 32) + iHead ] -= pedDiff;
	}
      }
      
      for( iHead = 0; iHead < 8; ++iHead ) {   
	correctionValues[ 8 * (strip % 32) + iHead ] = GSL_MAX( GSL_MIN( int(floor(correctionValuesTmp[ 8 * (strip % 32) + iHead ]+0.5)), 7 ), -8);
	
	if( m_produceHistos ) {
	  if((*nEvents)[iHead%4][alink] > 0 || (*nEvents)[iHead%4+4][alink] > 0){
	    plot1D( (*(m_stVetraCfg -> getSTVetraCond( iTell -> first ) -> pedestalValue()))[strip], "Pedestal", -10.5, 255.5, 266 );
	    plot1D( correctionValues[ 8 * (strip % 32) + iHead ], "Header_correction", -10.5, 10.5, 21 );
	    
	    // Pedestal difference        
	    plot1D( correctionValuesTmp[ 8 * (strip % 32) + iHead ], "Pedestal_difference_ch"+toString(strip%32)+"_h"
		    + toString(iHead), -8.05, 7.05, 151 );
	    
	    profile1D(double(strip%32), correctionValuesTmp[ 8 * (strip % 32) + iHead ], "CorrVsStrip_h" +toString(iHead),
		      -0.5, 31.5, 32 );
	  }
	}
      } 

      // Write the conditions to the transient conditions database
      m_stVetraCfg -> getSTVetraCond( (*iTell).first ) 
        -> headerCorrectionAnalogLink( alink, correctionValues );
      
      if(strip%32 == m_numChannels-1)
	firstpass = false;
    }    
  }
  
  return STThresholdBase::finalize(); // must be called after all other actions
}
