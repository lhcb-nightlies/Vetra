// $Id: STScanner.cpp,v 1.16 2010-05-02 17:21:42 mtobin Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

#include "STDet/DeSTDetector.h"

// local
#include "STScanner.h"

using namespace std;
using namespace AIDA;
using namespace LHCb;
using namespace ST;

//-----------------------------------------------------------------------------
// Implementation file for class : STScanner
//
// 2008-03-26 : Johan Luisier
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STScanner )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STScanner::STScanner( const std::string& name,
                      ISvcLocator* pSvcLocator)
  : STThresholdBase ( name , pSvcLocator ),
    ClusterNbr(0),
    Size(0),
    Nbr(0)
{
  declareSTConfigProperty("ClusterLocation",  m_ClusterLoc, "Emu/IT/Clusters");
  declareProperty("ChargeCut", m_chargeCut = 0.);

  setForcedInit();
}
//=============================================================================
// Destructor
//=============================================================================
STScanner::~STScanner()
{} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode STScanner::initialize()
{
  StatusCode sc(STThresholdBase::initialize()); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  info() << "Cluster will be retrieved from " << m_ClusterLoc << endmsg;

  lower_bin = -.5;

  if (detType() == "IT")
    upper_bin = 77.5;
  else
    upper_bin = 108.5;
  
  nbr_bin = static_cast<unsigned int>(upper_bin - lower_bin);

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode STScanner::execute()
{
  StatusCode sc(SUCCESS);
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  counter++;
  // If the "convergence limit" is not reached, nothing has to be done
  if (counter <= m_WaitingTill)
    return sc;

  if (!initDone)
  {
    if ( msgLevel(MSG::DEBUG) ) debug() << "Finalizing the setting up" << endmsg;
    sc = initHistograms();
    if ( sc.isRecoverable() )
    {
      if ( msgLevel(MSG::DEBUG) )
        debug() << "Got an empty container, assuming round robin data" << endmsg;
      return StatusCode::SUCCESS;
    }
    else if (sc.isFailure())
      return sc;
    if ( initDone && msgLevel( MSG::DEBUG) )
      debug() << "Histograms are finaly set up!" << endmsg;
  }

  if ( msgLevel(MSG::DEBUG) ) debug() << "Retrieve cluster from " << m_ClusterLoc << endmsg;
  
  if ( !exist<STClusters>(m_ClusterLoc) )
  {
    warning() << "No cluster found!" << endmsg;
    return sc;
  }
  clusterCont = get<STClusters>(m_ClusterLoc);

  Nbr = clusterCont -> size();

  STClusters::const_iterator itClust, ClustBegin(clusterCont -> begin()),
    ClustEnd(clusterCont -> end());

  unsigned int ID, chan, i;

  double Charge, noise;

  vector<unsigned int>::const_iterator vecBegin(m_UnwantedTELL1s.begin()),
    vecEnd(m_UnwantedTELL1s.end());

  for (itClust = ClustBegin; itClust != ClustEnd; itClust++)
  {
    noise = tracker() -> findSector( (*itClust) -> channelID() )
      -> noise ( (*itClust) -> channelID() );
    
    ID = (*itClust) -> sourceID();
    if (find(vecBegin, vecEnd, ID) != vecEnd)
    {
      Nbr--;
      continue;
    }
    Size = (*itClust) -> size();
    Charge = (*itClust) -> totalCharge();

    if ( Charge / noise < m_chargeCut )
    {
      Nbr--;
      continue;
    }
    

    plot(Size, "clustersize", "Size of cluster", .5, 4.5, 4);
    histoName  = "strip";
    histoTitle = "N-strip clusters";
    plot2D(ID, Size, histoName, histoTitle, lower_bin, upper_bin, .5, 4.5,
           nbr_bin, 4);
    histoName  = "Landau";
    histoTitle = "Charge per cluster";
    plot2D(ID, Charge, histoName, histoTitle, lower_bin, upper_bin, .5, 512.5,
           nbr_bin, 512);
    histoName  = "Chan" + toString(ID, 3);
    histoTitle = "Channel for TELL1 " + toString(ID);
    chan = (*itClust) -> tell1Channel();
    for (i = 0; i < Size; i++)
      plot2D(chan + i, Size,histoName, histoTitle, -.5, 3071.5, .5, 4.5,
             3072, 4);
  }


  plot(Nbr, "clusternbr", "Nbr of cluster", -.5, 14.5, 15);

  ClusterNbr += Nbr;

  if ( msgLevel(MSG::DEBUG) )
    debug() << "Number of clusters at iteration " << counter - m_WaitingTill
            << " : " << Nbr << endmsg;

  return sc;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode STScanner::finalize()
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  info() << "Number of found clusters : " << ClusterNbr << " +/- "
	 << sqrt(static_cast<double>(ClusterNbr)) << endmsg;

  unsigned int ProcessedEvent(counter - m_WaitingTill),
    Denominator(ProcessedEvent * NumberOfUsedLinks * 128);

  if ( msgLevel(MSG::DEBUG) )
  {
    debug() << "Number of events taken into account : " << ProcessedEvent << endmsg;
    debug() << "Enabled links : " << NumberOfUsedLinks << endmsg;
  }

  info() << "Rate of noise cluster : " << "[0;39;49;1m"
         << static_cast<double>(ClusterNbr) / static_cast<double>(Denominator)
         << " +/- " << sqrt(static_cast<double>(ClusterNbr)) /
    static_cast<double>(Denominator) << "[0m" << endmsg;

  if ( ClusterNbr == 0 )
    return STThresholdBase::finalize();
    

  const Histo1DMapID& histos(histo1DMapID());

  Histo1DMapID::const_iterator entry;

  entry = histos.find(HistoID("clustersize"));
  
  if (entry -> second == 0) 
    // must be called after all other actions
    return STThresholdBase::finalize();

  for (unsigned int i(0); i < 4; i++)
    info() << "Nbr of " << i + 1 << " strip clusters : "
           << (entry -> second) ->  binEntries(i) << endmsg;

 // must be called after all other actions
  return STThresholdBase::finalize();
}

//=============================================================================
