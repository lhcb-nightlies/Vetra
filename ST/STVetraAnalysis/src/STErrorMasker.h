// $Id: STErrorMasker.h,v 1.6 2009-02-09 08:56:57 jluisier Exp $
#ifndef STERRORMASKER_H 
#define STERRORMASKER_H 1

// Include files
#include "STThresholdBase.h"
#include "Event/STTELL1BoardErrorBank.h"
#include "Event/STTELL1Error.h"
#include "AIDA/IHistogram2D.h"

/** @class STErrorMasker STErrorMasker.h
 *  
 *  This class looks in the Event Info and masks the
 *  strips containing errors (disabled links, bad event
 *  size, etc), according to STTELL1Error class
 *
 *  @see LHCb::STTELL1Error
 *
 *  @author Johan Luisier
 *  @date   2008-07-03
 */

class STErrorMasker : public STThresholdBase
{
 public:
  // Standard constructor
  STErrorMasker( const std::string& name, ISvcLocator* pSvcLocator );
  
  virtual ~STErrorMasker( ); // Destructor
  
  virtual StatusCode initialize() override;    //< Algorithm initialization
  virtual StatusCode execute   () override;    //< Algorithm execution
  virtual StatusCode finalize  () override;    //< Algorithm finalization
protected:
  /**
   * Gets the error info from the Event Info (used when running on NZS data).
   * @return Status Code
   */
  StatusCode generateErrorInfo();
  
private:
  /**
   * Add the TELL1 to the list when running on ZS
   * @param TELL1 the TELL1 that needs to be added
   */
  void addTELL1(const unsigned int& TELL1);

  /**
   * Toggles On / Off building STTELL1Error from EventInfo
   *
   * Default value : true
   */
  bool m_RunOnNZS;

  /**
   * Toggles On / Off the extended summary at the end of the run
   *
   * Default value : true
   */
  bool m_FullDetails;

  /**
   * Toggles On / Off printing of TELL1 number corresponding to the sourceID.
   *
   * Default value : false
   */
  bool m_BoardAlias;

  /**
   * Toggles On / Off printing of the name of the link (IT1ASector.....)
   *
   * Default value : false
   */
  bool m_FullNames;

  /**
   * Counts the found errors
   */
  unsigned int errorNbr;

  /**
   * EventInfo container
   */
  LHCb::STTELL1EventInfos *m_Infos;

  /**
   * STTELL1Error container
   */
  LHCb::STTELL1BoardErrorBanks *m_Errors;

  /**
   * Error location in TES
   */
  std::string m_ErrorLocation;

  /**
   * Construct the detailed print out "Link x has errors" or "Port y of IT1ASector1Beetle2 has errors".
   *
   * @param
   */
  std::string fillStream(const unsigned int& TELL1, const unsigned int& link, const unsigned int& port = 4) const;
};
#endif // STERRORMASKER_H
