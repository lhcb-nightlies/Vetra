// $Id: STNoiseMonitor.h,v 1.8 2010-03-10 10:31:49 jluisier Exp $
#ifndef STNOISEMONITOR_H 
#define STNOISEMONITOR_H 1

// Include files
// from Gaudi
#include "STThresholdBase.h"

#include <map>
#include <vector>

class DeSTSector;

/** @class STNoiseMonitor STNoiseMonitor.h
 *
 * This class is meant to compute the noise in every strip,
 * and then build a xml file containing the noise level for
 * each sector.
 *
 *  @author Johan Luisier
 *  @date   2009-02-11
 */
class STNoiseMonitor : public STThresholdBase
{
public: 
  /// Standard constructor
  STNoiseMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~STNoiseMonitor( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:
  StatusCode fillProfile1D( LHCb::STTELL1Datas* container, const std::string& prefixName = "" );
private:
  /**
   * Storage of noise values vs sectors
   */
  std::map< DeSTSector*, std::vector<double> > NoisePerSector;
  /**
   * Storage of common mode  values vs sectors
   */
  std::map< DeSTSector*, std::vector<double> > CMPerSector;
  /**
   * Storage of capacitance vs sectors
   */
  std::map< DeSTSector*, double > Capacitances;
  /**
   * Strings used for optimization purpose.
   */
  std::string profTitle, profName;

  double m_constantFactor, /**< Constant in noise = A * C + B  ie this is B */
    m_slopeFactor; /**< Slope in noise = A * C + B  ie this is A */

  /**
   * Number of decimals used when writing the noise into DDDB, default = 2
   */
  unsigned int m_digits;

  /**
   * Toggles on / off the electron per ADC production
   */
  bool m_ElectronsPerADC;

  /**
   * Location of LCMS data
   */
  std::string m_lcmsLoc;
  
  /**
   * Location of ped subtracted data
   */
  std::string m_pedsubtractedLoc;
  
  /**
   * Input data (optimization)
   */
  LHCb::STTELL1Datas* m_pedDatas;

//   std::string m_author;
//   std::string m_tag;
//   std::string m_desc; 
  
  
};
#endif // STNOISEMONITOR_H
