// $Id: STPedestalEstimator.cpp,v 1.8 2010-04-06 09:07:14 jluisier Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// LHCbKernel
#include "Kernel/STDAQDefinitions.h"
#include "Kernel/ISTReadoutTool.h"

// VetraKernel
#include "VetraKernel/ISTVetraCfg.h"
#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

// local
#include "STPedestalEstimator.h"

using namespace std;
using namespace LHCb;
using namespace STDAQ;
using namespace ST;
using namespace STBoardMapping;

//-----------------------------------------------------------------------------
// Implementation file for class : STPedestalEstimator
//
// 2011-09-12 : Frederic Dupertuis
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STPedestalEstimator )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STPedestalEstimator::STPedestalEstimator( const std::string& name,
                                    ISvcLocator* pSvcLocator)
  : STThresholdBase ( name , pSvcLocator )
{
  //declareSTConfigProperty("InputLocation", m_DataLoc, "Raw/IT/Full");
  declareProperty("HeaderCorrDisable", m_HeaderCorrDisable = true );
}
//=============================================================================
// Destructor
//=============================================================================
STPedestalEstimator::~STPedestalEstimator(){} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode STPedestalEstimator::initialize() 
{
  StatusCode sc( STThresholdBase::initialize() ); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by STThresholdBase

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // Get the conditions
  sc = GetConditions();

  if ( sc.isFailure() ) 
    return sc;
  
  // Initialize the detMap and the initial value of the header correction flag
  if ( detType() == "IT" )
  {
    m_detMap = ITNumberToSourceIDMap();
    if(m_HeaderCorrDisable)
      m_initialHdrSubValues.assign( 42, 0 );
  } else {
    m_detMap = TTNumberToSourceIDMap();
    if(m_HeaderCorrDisable)
      m_initialHdrSubValues.assign( 48, 0 );
  }
  
  Map::iterator it, End;
  End = m_detMap . end();
  STVetraCondition * m_stCond;
  // Store the initial value of the updateEnable flag and set it to enable
  for (it = m_detMap . begin(); it != End; it++)
  {
    m_stCond = m_stVetraCfg -> getSTVetraCond( it -> second );
    
    if(m_HeaderCorrDisable){
      // Get the initial value of the header subtraction flag
      m_initialHdrSubValues[ it -> first - 1 ] = 
        m_stCond -> headerEnable();
      // Disable the header subtraction in the emulator
      m_stCond -> headerEnable( 0 );
    }
    
    updMgrSvc() -> invalidate( m_stCond );
    
    sc = updMgrSvc() -> update( m_stCond );
    
    if( sc.isFailure() )
      return sc;
  }
  
  return sc;  
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode STPedestalEstimator::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  
  StatusCode sc(StatusCode::SUCCESS);
  
  // get the data
  if (!exist<STTELL1Datas>(m_DataLoc)) 
    return Error("No valid data at " + m_DataLoc);
  const STTELL1Datas* data = get<STTELL1Datas>(m_DataLoc);
  
  // loop over the data
  STTELL1Datas::const_iterator iterBoard = data->begin(); 

  for (; iterBoard != data->end() ; ++iterBoard ) {
    // get the tell board and the data headers
    unsigned int tellID = (*iterBoard)->TELL1ID();

    const STTELL1Data::Data& dataValues = (*iterBoard)->data();

    // Reset the maps for each tell1
    if ( m_meanMap.find(tellID) == m_meanMap.end() ) {
      m_meanMap[tellID].resize(3072, 0.0 );
      m_nEvents[tellID].resize(3072, 0 );
    }
    
    vector< double >* meanTELL = &m_meanMap[tellID];
    vector< int >* nEvents = &m_nEvents[tellID];
    
    // Loop over the PPs that have sent data
    vector< unsigned int > sentPPs = (*iterBoard)->sentPPs();
    vector< unsigned int >::const_iterator iPP = sentPPs.begin();
    for( ; iPP != sentPPs.end(); ++iPP ) {
      unsigned int pp = *iPP;
      
      // Loop over the links (i.e. Beetles)
      unsigned int iBeetle = 0;
      for ( ; iBeetle < nBeetlesPerPPx; ++iBeetle ){
        unsigned int beetle = iBeetle + pp*nBeetlesPerPPx;
 
        // Loop over the ports in this link
        for( unsigned int iPort = 0 ; iPort < nports ; ++iPort ) {
	  
          for( unsigned int strip = 0; strip < nstrips ; ++strip ) {
            // Get ADC value
            unsigned int chan  = strip + iPort*nstrips;
	    
            double value = dataValues[beetle][chan];
	    
	    chan += beetle * 128;

	    (*nEvents)[ chan ]++;
	    int nEvt = (*nEvents)[ chan ];
	    
            // Calculate the average (pedestal) of this channel
            (*meanTELL)[ chan ] = ( (*meanTELL)[ chan ]*(nEvt-1)
				    + value ) / nEvt;
	  } // channel
        } // ports
      } // Beetles
    }  // PPs
  } // boards

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode STPedestalEstimator::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  Map::iterator it, End( m_detMap . end() );

  for (it = m_detMap . begin(); it != End; it++)
  {
    if(m_HeaderCorrDisable){
      m_stVetraCfg -> getSTVetraCond( it -> second ) 
        -> headerEnable( m_initialHdrSubValues[ it -> first - 1] );
    }
  }

  StatusCode sc(StatusCode::SUCCESS);
  
  sc = writePedestals();
  
  if(sc.isFailure())
    return sc;
  
  return STThresholdBase::finalize();  // must be called after all other actions
}

StatusCode STPedestalEstimator::writePedestals()
{
  StatusCode sc(StatusCode::SUCCESS);
  
  DataMap::const_iterator mapIt, mapBegin(m_meanMap.begin()), mapEnd(m_meanMap.end());

  unsigned int iLink, iStrip, chan;
  
  // Make data structure for pedestals in this TELL1
  vector< int > pedestalValues;
  const vector< int >* pedestalMask;
  const vector< int >* disableBeetles;
  const vector< double >* meanTELL;
    
  // Iterate over the TELL1s
  for (mapIt = mapBegin; mapIt != mapEnd; mapIt++)
  {
    meanTELL = &(mapIt -> second);
    
    // Fill default value
    pedestalValues.assign( 3072, 128 );
    
    pedestalMask = m_stVetraCfg -> getSTVetraCond( mapIt -> first ) -> pedestalMask();
    disableBeetles = m_stVetraCfg -> getSTVetraCond( mapIt -> first ) -> disableBeetles();

    // Iterate over the beetles (links)
    for (iLink = 0; iLink < 24; iLink++)
      {
      if( (*disableBeetles) [ iLink ] != 0 )
	continue;

      for (iStrip = 0; iStrip < 4*nstrips; iStrip++) {
	chan = iLink * 4*nstrips + iStrip;
	
	if( (*pedestalMask) [ chan ] == 0 )
	  pedestalValues [ chan ] = int(floor( (*meanTELL) [ chan ] + 0.5 ));
      }
    }

    // Update the pedestal values in the transient Vetra conditions
    m_stVetraCfg -> getSTVetraCond( mapIt -> first ) -> pedestalValue( pedestalValues );
  }
  
  return sc;
}


//=============================================================================
