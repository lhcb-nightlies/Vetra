// $Id: STMCThresholds.h,v 1.2 2010-04-20 08:34:29 mtobin Exp $
#ifndef STMCTHRESHOLDS_H
#define STMCTHRESHOLDS_H 1

#include "GaudiAlg/GaudiAlgorithm.h"

class ISTReadoutTool;


/** @class STClusterCreator STClusterCreator.h
 *
 *  Class for clustering in the ST tracker
 *
 *  @author M.Needham
 *  @date   07/03/2002
 */

class STMCThresholds :public GaudiAlgorithm {

public:
  
  // Constructors and destructor
  STMCThresholds( const std::string& name, ISvcLocator* pSvcLocator); 
  virtual ~STMCThresholds();  

  // IAlgorithm members
  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

private:

  std::string m_detType;  
  std::string m_readoutToolName;

  ISTReadoutTool* m_readoutTool;
 
};

#endif // STMCTHRESHOLDS_H
