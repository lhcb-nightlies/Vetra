// Gaudi
#include "GaudiKernel/AlgFactory.h"

// LHCbKernel
#include "Kernel/STDAQDefinitions.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/ISTReadoutTool.h"
#include "Kernel/STDetSwitch.h"
#include "Kernel/STLexicalCaster.h"

// VetraKernel
#include "VetraKernel/ISTVetraCfg.h"
#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

// Reading Event Info
#include "Event/STTELL1EventInfo.h"
#include "Event/STTELL1Data.h"

// standard
#include <iostream>

// local
#include "STHeaderTuple.h"

using namespace std;
using namespace LHCb;
using namespace STDAQ;
using namespace LHCbConstants;
using namespace ST;

DECLARE_ALGORITHM_FACTORY( STHeaderTuple)

//-----------------------------------------------------------------------------
// Implementation file for class : STHeaderTuple
//
// 2011-04-30 : Frederic Dupertuis
//-----------------------------------------------------------------------------

STHeaderTuple::STHeaderTuple( const string& name, 
                                        ISvcLocator* pSvcLocator ) :
  ST::TupleAlgBase( name , pSvcLocator )
{
  // constructer
  declareProperty("AllSectors" , m_allSectors = false);
  declareProperty("HistosOnly" , m_histosonly = false);
  declareSTConfigProperty("TupleName" , m_tupleName,
                          "ITHeader");
  declareSTConfigProperty("VetraToolName" , m_stVetraToolName,
                          "ITVetraCfgTool",
                          "Name of the STVetraCfg tool");
}

StatusCode STHeaderTuple::initialize()
{
  // initialisation of base class has to be done first
  StatusCode sc( ST::TupleAlgBase::initialize() );  
  if ( sc.isFailure() )
    return sc;

  // Get the conditions
  m_stVetraCfg = tool<ISTVetraCfg>( "STVetraCfg", m_stVetraToolName );
  sc = m_stVetraCfg -> initialize();

  return sc;
}

StatusCode STHeaderTuple::execute()
{
  Tuple tuple = nTuple( m_tupleName );

  // get the data with the header (the header is only stored in the raw data)
  string headerLoc = "Raw/" + detType() + "/Full";
  if (!exist<STTELL1Datas>(headerLoc)) 
    return Error("No valid data at " + headerLoc);
  const STTELL1Datas* header = get<STTELL1Datas>(headerLoc);
  
  // loop over the data
  STTELL1Datas::const_iterator iterHead  = header->begin();

  for (; iterHead != header->end() ;  ++iterHead ) {
    // get the tell board and the data headers
    unsigned int tellID = (*iterHead)->TELL1ID();
    unsigned int tellno = readoutTool()->SourceIDToTELLNumber(tellID);
    string tellnostr = toString(tellno);

    // Get the conditions for this TELL1
    //STVetraCondition* condition = m_stVetraCfg -> getSTVetraCond( tellID );

    const STTELL1Data::Data& headerValues = (*iterHead) ->header();
    const STTELL1Data::Data& dataValues = (*iterHead) ->data();
    
    // Loop over the PPs that have sent data
    vector<unsigned int> sentPPs = (*iterHead)->sentPPs();
    vector<unsigned int>::const_iterator iPP = sentPPs.begin();
    for( ; iPP != sentPPs.end(); ++iPP ) {
      unsigned int pp = *iPP;
      
      // Loop over the links (i.e. Beetles)
      unsigned int iBeetle = 0;
      for ( ; iBeetle < nBeetlesPerPPx; ++iBeetle ){
        unsigned int beetle = iBeetle + pp*nBeetlesPerPPx;
	
        // Loop over the ports in this link
        for( unsigned int iPort = 0 ; iPort < nports ; ++iPort ) {
	  int alink = iPort + beetle*nports ;
	  
	  if(!m_histosonly){
	    tuple->column( "tell1", static_cast<int>(tellID) );
	    tuple->column( "tell1no", static_cast<int>(tellno) );
	    tuple->column( "beetle", static_cast<int>(beetle) );
	    tuple->column( "port", static_cast<int>(alink) );
	    tuple->column( "PCN", static_cast<int>(((*iterHead) -> eventInfo())[0]->pcnVote()) );
	    tuple->column( "header_2", headerValues[beetle][iPort*3] );
	    tuple->column( "header_1", headerValues[beetle][iPort*3+1] );
	    tuple->column( "header_0", headerValues[beetle][iPort*3+2] );
	    tuple->column( "channel_0", dataValues[beetle][iPort*32+0] );
	    tuple->column( "channel_1", dataValues[beetle][iPort*32+1] );
	    tuple->column( "channel_2", dataValues[beetle][iPort*32+2] );
	    tuple->column( "channel_3", dataValues[beetle][iPort*32+3] );
	    tuple->column( "channel_4", dataValues[beetle][iPort*32+4] );
	    tuple->column( "channel_5", dataValues[beetle][iPort*32+5] );
	    if(m_allSectors){
	      tuple->column( "channel_6", dataValues[beetle][iPort*32+6] );
	      tuple->column( "channel_7", dataValues[beetle][iPort*32+7] );
	      tuple->column( "channel_8", dataValues[beetle][iPort*32+8] );
	      tuple->column( "channel_9", dataValues[beetle][iPort*32+9] );
	      tuple->column( "channel_10", dataValues[beetle][iPort*32+10] );
	      tuple->column( "channel_11", dataValues[beetle][iPort*32+11] );
	      tuple->column( "channel_12", dataValues[beetle][iPort*32+12] );
	      tuple->column( "channel_13", dataValues[beetle][iPort*32+13] );
	      tuple->column( "channel_14", dataValues[beetle][iPort*32+14] );
	      tuple->column( "channel_15", dataValues[beetle][iPort*32+15] );
	      tuple->column( "channel_16", dataValues[beetle][iPort*32+16] );
	      tuple->column( "channel_17", dataValues[beetle][iPort*32+17] );
	      tuple->column( "channel_18", dataValues[beetle][iPort*32+18] );
	      tuple->column( "channel_19", dataValues[beetle][iPort*32+19] );
	      tuple->column( "channel_20", dataValues[beetle][iPort*32+20] );
	      tuple->column( "channel_21", dataValues[beetle][iPort*32+21] );
	      tuple->column( "channel_22", dataValues[beetle][iPort*32+22] );
	      tuple->column( "channel_23", dataValues[beetle][iPort*32+23] );
	      tuple->column( "channel_24", dataValues[beetle][iPort*32+24] );
	      tuple->column( "channel_25", dataValues[beetle][iPort*32+25] );
	      tuple->column( "channel_26", dataValues[beetle][iPort*32+26] );
	      tuple->column( "channel_27", dataValues[beetle][iPort*32+27] );
	      tuple->column( "channel_28", dataValues[beetle][iPort*32+28] );
	      tuple->column( "channel_29", dataValues[beetle][iPort*32+29] );
	      tuple->column( "channel_30", dataValues[beetle][iPort*32+30] );
	      tuple->column( "channel_31", dataValues[beetle][iPort*32+31] );
	    }
	
	    tuple->write();
	  }
	  
	  plot1D(((*iterHead) -> eventInfo())[0]->pcnVote(), "PCN", -0.5, 255.5, 256);
	  plot2D(alink, headerValues[beetle][iPort*3], "Tell"+tellnostr,
		 "Header Spectrum of Tell"+tellnostr, -0.5, 96.5, -0.5, 255.5, 97, 256);
	  plot2D(alink, headerValues[beetle][iPort*3+1], "Tell"+tellnostr,
		 "Header Spectrum of Tell"+tellnostr, -0.5, 96.5, -0.5, 255.5, 97, 256);
	  plot2D(alink, headerValues[beetle][iPort*3+2], "Tell"+tellnostr,
		 "Header Spectrum of Tell"+tellnostr, -0.5, 96.5, -0.5, 255.5, 97, 256);
	  
	} // ports
      } // Beetles
    }  // PPs
  } // boards

  return StatusCode::SUCCESS;
}

StatusCode STHeaderTuple::finalize()
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  StatusCode sc(StatusCode::SUCCESS);
  
  sc = ST::TupleAlgBase::finalize();
  sc = m_stVetraCfg -> finalize();
  
  return sc;  // must be called after all other actions
}
