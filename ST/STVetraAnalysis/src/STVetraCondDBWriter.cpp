// $Id: STVetraCondDBWriter.cpp,v 1.1 2010-03-10 10:31:50 jluisier Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// local
#include "STVetraCondDBWriter.h"

#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

#include "Kernel/STXMLUtils.h"

#include <iostream>
#include <fstream>

using namespace std;
using namespace ST;

//-----------------------------------------------------------------------------
// Implementation file for class : STVetraCondDBWriter
//
// 2010-02-15 : Johan Luisier
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( STVetraCondDBWriter )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STVetraCondDBWriter::STVetraCondDBWriter( const std::string& name,
                                          ISvcLocator* pSvcLocator)
  : STThresholdBase ( name , pSvcLocator )
{
  declareProperty( "Author"     , m_author = "" );
  declareProperty( "Tag"        , m_tag = "" );
  declareProperty( "Description",  m_description = "" );
  
}
//=============================================================================
// Destructor
//=============================================================================
STVetraCondDBWriter::~STVetraCondDBWriter() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode STVetraCondDBWriter::initialize() {
  StatusCode sc = STThresholdBase::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by STThresholdBase

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  if ( m_author == "" || m_tag == "" || m_description == "" )
    return Error( "At least one of these fields was not filled [ Author, Tag, Description ]" ); 

  sc = GetConditions();
  
  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode STVetraCondDBWriter::execute()
{

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode STVetraCondDBWriter::finalize()
{

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  ofstream out;
  
  out.open( m_FileName.c_str() );
  
  if ( out.fail() )
    return Error( "Cannot open " + m_FileName + " for writing" );

  // Writing in bold the location where the DB was saved
  info() << "[0;39;49;1m" << "Writing ST Vetra CondDB in " << m_FileName
         << "[0m" << endmsg;

  out << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>"  << endl;
  out << "<!DOCTYPE DDDB SYSTEM \"conddb:/DTD/structure.dtd\">"  << endl;

  // Add the comment from XMLUtils
  ostringstream comment;
  XMLUtils::fullComment(comment, m_author, m_tag, m_description);
  out << endl << comment.str() << endl;

  out << "<DDDB>"  << endl;

  m_stVetraCfg -> printOut( out );
  
  out << "</DDDB>"  << endl;
  
  out.close();

  return STThresholdBase::finalize();  // must be called after all other actions
}

//=============================================================================
