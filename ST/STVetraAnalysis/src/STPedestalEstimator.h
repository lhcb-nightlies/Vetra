// $Id: STPedestalEstimator.h,v 1.4 2010-04-06 09:07:14 jluisier Exp $
#ifndef STPEDESTALESTIMATOR_H 
#define STPEDESTALESTIMATOR_H 1

// Include files
#include "STThresholdBase.h"

//
#include "Kernel/STBoardMapping.h"

/** @class STPedestalEstimator STPedestalEstimator.h
 *  
 * This class gets the pedestal from an adaptive mean of the strips ADCs and 
 * write a condDB-like text file with the estimated values.
 *
 *  @author Frederic Dupertuis
 *  @date   2011-09-12
 */
class STPedestalEstimator : public STThresholdBase {
public: 
  /// Standard constructor
  STPedestalEstimator( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~STPedestalEstimator( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:
  std::set< unsigned int > m_processedBoards;
  
private:
  StatusCode writePedestals();

  // Local vectors for given TELL1
  typedef std::map<int, std::vector<double> > DataMap;  
  
  bool m_HeaderCorrDisable;     ///< Flag for disabling the header correction
  
  DataMap m_meanMap;            ///< Internal map for the pedestals
  
  std::map<int, std::vector<int> > m_nEvents;
    
  std::vector< unsigned int > m_initialHdrSubValues;

  STBoardMapping::Map m_detMap;
};
#endif // STPEDESTALESTIMATOR_H
