// $Id: STThresholdBase.cpp,v 1.19 2010-04-06 09:07:14 jluisier Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// local
#include "STThresholdBase.h"

// Reading Event Info
#include "Event/STTELL1EventInfo.h"

// Reading conditions
#include "VetraKernel/ISTVetraCfg.h"
#include "VetraKernel/STVetraCfg.h"
#include "VetraKernel/STVetraCondition.h"

#include "Kernel/STBoardMapping.h"

using namespace std;
using namespace AIDA;
using namespace LHCb;
using namespace ST;
using namespace STBoardMapping;

//-----------------------------------------------------------------------------
// Implementation file for class : STThresholdBase
//
// 2008-01-10 : Johan Luisier
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STThresholdBase::STThresholdBase( const std::string& name,
                                    ISvcLocator* pSvcLocator)
  : HistoAlgBase ( name , pSvcLocator ),
    counter(0),
    initDone(false),
    NumberOfTELL1s(0),
    NumberOfUsedLinks(0),
    TELL1Mapping(0),
    m_NbrBoardsInDet(42)
{
  // Std properties
  declareProperty("OutputFileName"   , m_FileName = "Thresholds.txt",
                  "Name of the ouput file");
  declareProperty("WaitingTill"      , m_WaitingTill = 1000,
                  "Number of event to skip for pedestal convergence");
  declareProperty("UnwantedTELL1s"   , m_UnwantedTELL1s,
                  "List of TELL1 to exclude");
  declareProperty("ProduceFile"      , m_ProduceFile = false,
                  "Swith on / off the file production");
  declareProperty("ConditionLocation", m_ConditionLocation
                  = "CondDB/TELL1Board",
                  "Path to STVetraCondition in COND");

  // ST properties
  declareSTConfigProperty("InputLocation"    , m_DataLoc,
                          "Raw/IT/LCMSADCs",
                          "Path to data in TES");
  declareSTConfigProperty("EventInfoLocation", m_eventInfoLoc,
                          "Raw/IT/EventInfo",
                          "Path to event info in TES");
  declareSTConfigProperty("VetraToolName"    , m_stVetraToolName,
                          "ITVetraCfgTool",
                          "Name of the STVetraCfg tool");

  m_stVetraCfg = 0;
}

//=============================================================================
// Destructor
//=============================================================================
STThresholdBase::~STThresholdBase() 
{} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode STThresholdBase::initialize() {
  StatusCode sc = HistoAlgBase::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by HistoAlgBase

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  if (m_DataLoc.find("LCMSADCs") != string::npos)
  {
    lower_bin = -20.5;
    upper_bin =  40.5;
  }
  else if (m_DataLoc.find("PedSubADCs") != string::npos)
  {
    lower_bin = -50.5;
    upper_bin =  50.5;
  }
  else if (m_DataLoc.find("Full") != string::npos)
  {
    lower_bin =  95.5;
    upper_bin = 180.5;
  }
  else
  {
    lower_bin = -127.5;
    upper_bin =  127.5;
  }

  if ( detType() == "TT" )
    m_NbrBoardsInDet = 48;
  
  nbr_bin = static_cast<unsigned int>(upper_bin - lower_bin);

  info() << "Data will be retrieved from " << m_DataLoc << endmsg;

  info() << "Event info will be retrieved from " << m_eventInfoLoc << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode STThresholdBase::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return HistoAlgBase::finalize();  // must be called after all other actions
}

//=============================================================================
//  Preparing the histograms
//=============================================================================
StatusCode STThresholdBase::initHistograms()
{
  StatusCode sc(SUCCESS);
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> initHistograms" << endmsg;

  // Get the Event Info (to know which links are disabled)

  sc = getEventInfo( m_Infos, m_eventInfoLoc );

  if ( sc.isRecoverable() )
    return Warning( "Empty data found", sc, 10 );
  else if ( sc.isFailure() )
    return sc;

  if ( msgLevel(MSG::DEBUG) )
    debug() << "EventInfo read from TES, continuing" << endmsg;

  unsigned int disabled(0), FPGA, TELL1;

  unsigned char links;

  vector<unsigned int>::const_iterator vecBegin(m_UnwantedTELL1s.begin()),
    vecEnd(m_UnwantedTELL1s.end()), vecIt;

  STTELL1EventInfos::const_iterator InfoIt, InfoBegin(m_Infos -> begin()),
    InfoEnd(m_Infos -> end());

  if ( (m_Infos -> size()) % 4 != 0)
    return Error("Bad Event info size");

  vector<unsigned int>
    TELL1_nbr( (m_Infos -> size()) / 4);

  for (InfoIt = InfoBegin; InfoIt != InfoEnd; InfoIt++)
  {
    TELL1 = (*InfoIt) -> sourceID();
    verbose() << "Looking at TELL1 " << TELL1 << endmsg;
    if ( EnabledTELL1s.find( TELL1 ) != EnabledTELL1s.end() )
    {
      if ( msgLevel(MSG::DEBUG) )
        debug() << "Already processed TELL1 " << TELL1 << endmsg;
      InfoIt += 3;
      continue;
    }

    TELL1_nbr[static_cast<unsigned int>(InfoIt - InfoBegin) / 4] = TELL1;
    
    // Check if by chance this TELL1 is unwanted
    vecIt = find(vecBegin, vecEnd, TELL1);

    if (vecIt != vecEnd)
    {
      // If the TELL1 board is unwanted, it is skipped
      if ( msgLevel(MSG::DEBUG) )
        debug() << "Skipping TELL1 " << TELL1 << endmsg;
      InfoIt += 3;
      continue;
    }

    // Get all needed information about disabled links
    links = (*InfoIt) -> OptLnkDisable();

    FPGA = static_cast<unsigned int>(InfoIt - InfoBegin) % 4;

    if ( msgLevel(MSG::DEBUG) )
      debug() << "OptLnkDisable " << static_cast<unsigned int>(links)
              << endmsg;

    // Check the status of each link of the PPFPGA
    sc = CheckChannels(TELL1, FPGA, links);
    
    if (sc.isFailure())
      return sc;
  }

  vector<unsigned int>::iterator ListIt, ListBegin(TELL1_nbr.begin()),
    ListEnd(TELL1_nbr.end());

  for (ListIt = ListBegin; ListIt != ListEnd; ListIt++)
    EnabledTELL1s[*ListIt] = true;

  for (vecIt = vecBegin; vecIt != vecEnd; vecIt++)
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "TELL1 nbr "  << setw(3) << *vecIt
              << " present, but I don't care" << endmsg;
    EnabledTELL1s[*vecIt] = false;
  }

  for (ListIt = ListBegin; ListIt != ListEnd; ListIt++)
  {
    if (EnabledTELL1s[*ListIt])
    {
      ManageTELL1s[*ListIt] = NumberOfTELL1s;
      NumberOfTELL1s++;
//       if ( m_stVetraCfg -> getSTVetraCond( *ListIt ) -> isAnyDisable() )
//       {
//         fatal() << "found something" << endmsg;
//         info() << *(m_stVetraCfg -> getSTVetraCond( *ListIt )) << endmsg;
//       }
    }
  }  

  if (NumberOfTELL1s == 0)
    return Error("No observed board in datafile");

  map<unsigned int, bool>::const_iterator lnkIt,
    lnkBegin(EnabledLinks.begin()), lnkEnd(EnabledLinks.end());

  if ( msgLevel(MSG::VERBOSE) )
    verbose() << "Summary of disabled links : " << endmsg;
  
  for (lnkIt = lnkBegin; lnkIt != lnkEnd; lnkIt++)
  {
    // Counts the number of disabled links
    if (!lnkIt -> second)
    {
      disabled++;
      if ( msgLevel(MSG::VERBOSE) )
        verbose() << setw(7) << lnkIt -> first << " is disabled" << endmsg;
    }
  }

  NumberOfUsedLinks = 24 * NumberOfTELL1s - disabled;

  if ( msgLevel(MSG::VERBOSE) )
  {
    for (TELL1 = 0; TELL1 < NumberOfTELL1s; TELL1++)
    {
      verbose() << "TELL1 number "  << setw(3) << TELL1_nbr[TELL1] << " is ";
      if (EnabledTELL1s[TELL1_nbr[TELL1]])
        verbose() << "enabled" << endmsg;
      else
        verbose() << "disabled" << endmsg;
    }
  }

  if ( msgLevel(MSG::DEBUG) )
  {
    debug() << "I will look at " << NumberOfTELL1s << " TELL1s" << endmsg;
    debug() << "There are " << disabled << " disabled links" << endmsg;
    debug() << "There are " << NumberOfUsedLinks << " enabled links" << endmsg;
  }
  
  if ( EnabledTELL1s.size() == m_NbrBoardsInDet )
    initDone = true;

  return sc;
}

//=============================================================================
// Reading and interpreting the EventInfo
//=============================================================================
StatusCode STThresholdBase::CheckChannels(unsigned int& TELL1,
                                          unsigned int& FPGA,
                                          unsigned char& Lnk)
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> CheckChannels" << endmsg;
  StatusCode sc(SUCCESS);

  unsigned int i;
  
  bitset<8> bitcode(Lnk);
  
  if ( msgLevel(MSG::DEBUG) )
    debug() << "Link = " << bitcode <<  " for TELL1 " << TELL1
            << ", FPGA " << FPGA << endmsg;

  for (i = 0; i < 6; i++)
  {
    if (!bitcode[i])
    {
      // If link is NOT disabled, it is enabled ;-)
      // fill enable link
      EnabledLinks[TELL1*100000+1000*(FPGA*6 + i)] = true;
    }
    else
    {
      // fill enable link
      EnabledLinks[TELL1*100000+1000*(FPGA*6 + i)] = false;
    } 
  }
  
  map<unsigned int, bool>::const_iterator mapIt,
    mapBegin(EnabledLinks.begin()), mapEnd(EnabledLinks.end());
  
  // if all are disabled, disable the TELL1 => save memory

  unsigned int nbr_disabled(0);
  bool is_disabled(true);

  if (FPGA == 3)
  {
    nbr_disabled = 0;
    // When all 4 FPGA have been processed, summarize
    for (mapIt = mapBegin; mapIt != mapEnd; mapIt++)
    {
      if (mapIt -> first / 100000 == TELL1)
      {
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "TELL1 " << setw(2) << mapIt -> first / 100000
                    << ", link " << setw(2)
                    << (mapIt -> first - (mapIt -> first / 100000) * 100000) / 1000
                    << ", status : " << mapIt -> second << endmsg;
        
        is_disabled *= !mapIt -> second;
        if (!mapIt -> second)
          nbr_disabled++;
      }
    }
    
    if ( msgLevel(MSG::VERBOSE) ) verbose() << "Nbr disabled " << nbr_disabled << endmsg;
    
    if (is_disabled && nbr_disabled == 24)
    {
      warning() << "TELL1 " << setw(2) << TELL1
                << " will be disabled, because all links are disabled"
                << endmsg;
      m_UnwantedTELL1s.push_back(TELL1);
    }
  }

  return sc;
}

StatusCode STThresholdBase::GetConditions()
{
  StatusCode sc(SUCCESS);

  m_stVetraCfg = tool<ISTVetraCfg>( "STVetraCfg", m_stVetraToolName );
  
  sc = m_stVetraCfg -> initialize();
  
  if ( m_stVetraCfg -> getDetType() != detType() )
    return Error("STVetraCfg tool and algorithm don't have the same detType!");
  
  return sc;
}

StatusCode STThresholdBase::getDatas(STTELL1Datas*& m_input,
                                     const string& m_loc)
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> getDatas" << endmsg;

  if ( !exist<STTELL1Datas>( m_loc ) )
    return Error( "No data found in " + m_loc );
  else
  {
    m_input = get<STTELL1Datas>( m_loc );
    if ( m_input -> empty() )
    {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Data is empty, assuming round robin" << endmsg;
      return Warning( "Empty data", StatusCode( StatusCode::RECOVERABLE ), 10);
    }
  }
    
  return StatusCode::SUCCESS;
}

StatusCode STThresholdBase::getEventInfo(STTELL1EventInfos*& m_input, const string& m_loc)
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> getEventInfo" << endmsg;

  if ( !exist<STTELL1EventInfos>( m_loc ) )
    return Error( "No data found in " + m_loc );
  else
  {
    m_input = get<STTELL1EventInfos>( m_loc );
    if ( m_input -> empty() )
    {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Data is empty, assuming round robin" << endmsg;
      return Warning( "Empty data", StatusCode( StatusCode::RECOVERABLE ), 10);
    }
  }
  
  return StatusCode::SUCCESS;
}


unsigned int STThresholdBase::boardAlias(const unsigned int& sourceID) const
{
  if (detType() == "IT")
  {
    return ITSourceIDToNumberMap().find(sourceID) -> second;
  }
  else
  {
    return TTSourceIDToNumberMap().find(sourceID) -> second;
  }
}

//=============================================================================
