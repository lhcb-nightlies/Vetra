#ifndef STHeaderTuple_H
#define STHeaderTuple_H 1

// Include files
#include "Kernel/STTupleAlgBase.h"
#include "GaudiAlg/GaudiTupleAlg.h"

#include<string>

class ISTVetraCfg;

/** @class STHeaderTuple STHeaderTuple.h
 *
 * This class dumps a tuple with relevant leaves for header studies 
 *
 *  @author Frederic Dupertuis
 *  @date   2011-04-18
 */

class STHeaderTuple : public ST::TupleAlgBase {

public:
 
  /// constructer
  STHeaderTuple( const std::string& name, ISvcLocator *svcloc );

  /// initialize
  StatusCode initialize() override;

  /// execute
  StatusCode execute() override;

  /// initialize
  StatusCode finalize() override;
  
private:
  
  ISTVetraCfg* m_stVetraCfg;
  std::string m_tupleName;
  std::string m_stVetraToolName;
  bool m_allSectors;
  bool m_histosonly;

};

#endif // STHeaderTuple_H
