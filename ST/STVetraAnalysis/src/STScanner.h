// $Id: STScanner.h,v 1.5 2009-08-12 09:21:27 jluisier Exp $
#ifndef STSCANNER_H 
#define STSCANNER_H 1

// Include files
// from Gaudi
#include "STThresholdBase.h"

#include "Event/STCluster.h"

/** @class STScanner STScanner.h
 *  
 *  This class allows to study the cluster rate, in order to check
 *  the thresholds we set.
 *
 *  @author Johan Luisier
 *  @date   2008-03-26
 */
class STScanner : public STThresholdBase {
public: 
  /// Standard constructor
  STScanner( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~STScanner( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  /**
   * The cluster container.
   */
  LHCb::STClusters* clusterCont;

  /**
   * Counts the found cluster.
   */
  unsigned int ClusterNbr;

  /**
   * Optimization purpose.
   */
  unsigned int Size, Nbr;

  /**
   * Path to the cluster container in TES.
   */
  std::string m_ClusterLoc;

  /**
   * Additional cut on cluster charge
   */
  double m_chargeCut;
};
#endif // STSCANNER_H
