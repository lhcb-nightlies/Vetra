// $Id: STBadLinkMasker.h,v 1.12 2009-07-01 08:20:23 jluisier Exp $
#ifndef STBADLINKMASKER_H 
#define STBADLINKMASKER_H 1

// Include files
// from Gaudi
#include "STThresholdBase.h"
#include "AIDA/IProfile1D.h"

class Map;

/** @class STBadLinkMasker STBadLinkMasker.h
 *  
 *  This class will look at data and determine
 *  which strips are bad because of :
 *
 *  1) high noise
 *
 *  2) too low noise
 *
 *  3) sign of low gain (signal is too low)
 *
 *  4) pairs of shorted strips (if running on test pulse data)
 *
 *  5) flagged as bad in the conditions database.
 *
 *  @author Johan Luisier
 *  @date   2008-07-08
 */

class STBadLinkMasker : public STThresholdBase {
public: 
  /// Standard constructor
  STBadLinkMasker( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~STBadLinkMasker( ); ///< Destructor

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  double m_LowGain,/**< Low gain threshold */
    m_LowNoise,    /**< Low noise threshold */
    m_HighNoise,   /**< High noise threshold */
    m_Short,       /**< Short strips threshold */
    m_HighSig;     /**< Threshold for too high signal (for TP data only)*/

  bool m_TPdata,      /**< Toggles On / Off running on test pulse data, default value : false */
    m_BoardAlias,     /**< Toggles On / Off printing of TELL1 number corresponding to the sourceID, default value : false */
    m_FullDetails,    /**< Toggles On / Off the extended summary at the end of the run, default value : true */
    m_KnownBadStrips, /**< Toggles On / Off looking for know bad strips in LHCb Condition DB, default value : true */
    m_FullNames; /**< Toggles On / Off  printing of the name of the link (IT1ASector.....). default value : false */

  /**
   * List of status that will be masked in the TELL1 from the conditions
   * database. The default values mask everything but OK and Pinhole status.
   */
  std::vector< unsigned int > m_MaskedBadValues;

  /**
   * Number of beetles per sector, IT => 3, TT => 4
   */
  unsigned int BeetlesPerSector;

  std::string fillStream(const unsigned int& TELL1, const unsigned int& link, const unsigned int& port = 4) const;
};
#endif // STBADLINKMASKER_H
