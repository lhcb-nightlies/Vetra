// $Id: STTimeScan.h,v 1.6 2009-02-17 10:46:30 jvantilb Exp $
#ifndef STTimeScan_H
#define STTimeScan_H 1

#include "Kernel/STHistoAlgBase.h"
#include "STDet/DeSTDetector.h"
#include "Event/STTELL1Data.h"

#include <string>

/** @class STTimeScan STTimeScan.h
 *
 *  Class for monitoring STTimesScan
 *
 *  @author M.Needham
 *  @date   28/7/2008
 */


class STTimeScan : public ST::HistoAlgBase{

public:
 
  /// constructer
  STTimeScan( const std::string& name, 
                  ISvcLocator *svcloc );

  /// destructer
  virtual ~STTimeScan();

  /// initialize
  StatusCode initialize() override;


  /// execute
  StatusCode execute() override;

private:

  void fillProfiles(const LHCb::STTELL1Datas* data, double timeStep);

  // Event and step counters
  unsigned int m_prevCalibrationStep;
  int m_firstPositiveEvent;
  unsigned int m_event;

  // jobOptions
  std::vector<std::string> m_dataLocations; 
  int m_firstPulsedChannel;
  int m_channelPeriodicity;
  double m_pedestal;
  double m_rangeMin;
  double m_rangeMax;  
  int m_nBins;
  double m_stepSize;
  double m_spillSize;

};

#endif // STTimesScan_H
