#! /usr/bin/env python
# coding = utf-8

###############################################################################
#
# This script generates the xml files for IT / TT
#
# author : Johan Luisier <johan.luisier@epfl.ch>
#
###############################################################################

import sys, fileinput, os

DEBUG = False

if (len(sys.argv) != 2):
    print "Usage : write_st_xml_cond.py [IT/TT]"
    sys.exit(0)


# --> define the Xml headers
head0 = '<?xml version="1.0" encoding="ISO-8859-1"?>\n'
head1 = '<!DOCTYPE DDDB SYSTEM "../DTD/structure.dtd">\n'

# --> DDDB tag
DDDBTag    ="<DDDB>\n"
endDDDBTag ="</DDDB>\n"

VetraRoot = os.environ.get("VETRAROOT")

# --> open the Xml file and write out the data
XmlFileName = VetraRoot + '/VetraCondDB/' + sys.argv[1] + '/' + 'CondDB/TELL1Cond.xml'
try:
    XmlFile = open(XmlFileName, 'w')
except IOError, (errno, strerror):
    print XmlFileName
    print "I/O error(%s): %s" % (errno, strerror)
    sys.exit(0)
    

if(DEBUG): print XmlFile
# --> condition tag
condTag    = '<condition classID="9105" name="'
endCondTag = '</condition>\n'

# --> condition tags
condANumberTag  = ' '*2+'<param name=\"'
condAVecTag     = ' '*2+'<paramVector name=\"'
condACommentTag = ' '*2+"<!-- "
endANumberTag   = '</param>\n'
endAVecTag      = '</paramVector>\n'
endACommentTag  = ' -->\n'

# --> TELL1 IDs (IT != TT)
TELL1IDs = []
TELL1Nbr = []
destIPbytes = ''
srcByte  = ''
if (sys.argv[1] == "IT"):
    # 0-13, 32-45, 64-77
    TELL1Nbr = range( 1, 43)
    TELL1IDs = TELL1IDs + range(0, 14)
    TELL1IDs = TELL1IDs + range(32, 46)
    TELL1IDs = TELL1IDs + range(64, 78)
    destIPbytes = '196 131 '
    srcByte     = '13 '
elif (sys.argv[1] == "TT"):
    # 0-10, 32-42, 64-76 and 96-108.
    TELL1Nbr = TELL1Nbr + range(17, 21)
    TELL1Nbr = TELL1Nbr + range( 1,  4)
    TELL1Nbr = TELL1Nbr + range(33, 37)
    TELL1Nbr = TELL1Nbr + range(21, 25)
    TELL1Nbr = TELL1Nbr + range( 4, 7)
    TELL1Nbr = TELL1Nbr + range(37, 41)
    TELL1Nbr = TELL1Nbr + range(25, 29)
    TELL1Nbr = TELL1Nbr + range( 7, 12)
    TELL1Nbr = TELL1Nbr + range(41, 45)
    TELL1Nbr = TELL1Nbr + range(29, 33)
    TELL1Nbr = TELL1Nbr + range(12, 17)
    TELL1Nbr = TELL1Nbr + range(45, 49)
    TELL1IDs = range( 0, 11)
    TELL1IDs = TELL1IDs + range(32, 43)
    TELL1IDs = TELL1IDs + range(64, 77)
    TELL1IDs = TELL1IDs + range(96, 109)
    destIPbytes = '212 65 '
    srcByte     = '10 '

print "TELL1IDs", len(TELL1IDs), "Nbr", len(TELL1Nbr)

if(DEBUG): print TELL1IDs

# --> condition values
# misc
tellname                 = sys.argv[1].upper() + 'TELL'
tellSN                   = ''
tellUserPPVer            = ''
tellCommPPVer            = ''
tellCommSLVer            = ''
tellPPcodeGenDate        = ''
tellSLcodeGenDate        = ''
# GB IP and ethernet parameters
destinationIP            = '192 168 ' + destIPbytes + ' '
sourceIP                 = '192 169 ' + srcByte
destinationMAC           = '0x00 0x01 0xE8 0x5D 0xE7 0x20 '
sourceMAC                = '0x00 0xCC 0xBB 0x' + srcByte
ethernetType             = '0x800 '
ipVersion                = '0x4 '
headerLength             = '0x5 '
serviceType              = '0x0 '
ethernetTime             = '0xFF '
nextProtocol             = '0xF2 '
gbeCtrl                  = '0x0 '
# General parameters (TTC and others)
detectorID               = '2 '
pedBankSchedule          = '0 '
ttcInfoEnable            = '1 '
extTrigEnable            = '0 '
ttcTrigAvailable         = '1 '
ttcDestIPAvailable       = '1 '
dataGenEnable            = '0 '
sepGenEnable             = '0 '
ppDerandThreshold        = '32 '
mepPID                   = '0xEDED1D1D '
sourceID                 = '0x'
version                  = '0x3 '
if (sys.argv[1] == "IT"):
    classZsBank              = '0x0B '
    classNzsBank             = '0x14 '
    classPedBank             = '0x1B '
    classErrBank             = '0x19 '
else:
    classZsBank              = '0x0A '
    classNzsBank             = '0x13 '
    classPedBank             = '0x1C '
    classErrBank             = '0x1A '
feDataCheck              = '0x0 '
seriourErrThrottle       = '0x0 '
nzsBankPeriod            = '0x3E8 '
nzsBankOffset            = '0x0 '
triggerNumber            = '0x1000 '
consecutiveTrigger       = '1 '
waitCycles               = '3600 '
ecsTrigType              = '5 '
errBankDisable           = '0 '
infoBankDisable          = '0 '
infoBankEnable           = '0 '
nzsErrEnable             = '1 '
specHeaderEnable         = '1 '
ecsMepFactor             = '1 '
mtuSize                  = '8212 '
gbePortSelection         = '0x3 '
hltDpEnable              = '1 '
mepUseThr                = '0x70000 '
throttlePPxEnable        = '0xF '
throttleMepBufferEnable  = '1 '
triggerInfoThrottleEnable= '1 '
throttleEnable           = '1 '
expectedFw               = '0x42 '
# ST part
pseudHeaderLThresholds   = '0x71 '
pseudHeaderHThresholds   = '0x85 '
zsEnableVal              = '1 '
headerCorrEnableVal      = '1 '
headerLowThresholdVal    = '120 '
headerHighThresholdVal   = '136 '
headerCorrValuesVal      = '0 ' * 48
pedEnableVal             = '1 '
pedUpdateEnableVal       = '0 '
lcmsEnableVal            = '1 '
maxCluVal                = '255 '
DisBeetles_0_11Val       = '0 ' * 12
DisBeetles_12_23Val      = '0 ' * 12
EnableTlk_0_11Val        = '1 ' * 12
EnableTlk_12_23Val       = '1 ' * 12
EnableOrxVal             = '9 '
DisBLinks_0_11Val        = '0x0 ' * 12
DisBLinks_12_23Val       = '0x0 ' * 12
pcnSelect                = '0x0 ' * 4
pedValueVal              = '128 ' * 3072
pedMaskVal               = '0 ' * 3072
HitThresholdVal          = '6 ' * 3072
CMSThresholdVal          = '12 ' * 3072
SpillOverThresholdVal    = '20 ' * 48
ConfirmationThresholdVal = '8 ' * 48
StartStrips0_23          = '0 64 128 192 256 320 384 448 512 576 640 704 768 832 896 960 1024 1088 1152 1216 1280 1344 1408 1472 '
StartStrips24_47         = '1536 1600 1664 1728 1792 1856 1920 1984 2048 2112 2176 2240 2304 2368 2432 2496 2560 2624 2688 2752 2816 2880 2944 3008 '
GeneratorValuesLink0     = '176 176  16 176 ' + '128 ' * 32
GeneratorValuesLink1     = '16 176 176 16 ' + '128 ' * 32

# --> create the data structure...
List = [
    ("c", "Misc information"),
    ("s", "Tell_name", "string", "Name of the TELL1", tellname),
    ("s", "Tell_SN", "string", "Serial Number of TELL1", tellSN),
    ("s", "Tell_UserPP_Ver", "string", "The version of user logic for PP-FPGA", tellUserPPVer),
    ("s", "Tell_CommPP_Ver", "string", "The version of common logic for PP-FPGA", tellCommPPVer),
    ("s", "Tell_CommSL_Ver", "string", "The version of common logic for SL-FPGA", tellCommSLVer),
    ("s", "Tell_PPcode_Gen_Date", "string", "The generation date of PP FPGA code", tellPPcodeGenDate),
    ("s", "Tell_SLcode_Gen_Date", "string", "The generation date of SL FPGA code", tellSLcodeGenDate),
    ("c", "Gigabit IP and Ethernet protocol parameters"),
    ("v", "ip_dest_addr", "int", "Destination IP", destinationIP),
    ("v", "ip_source_addr", "int", "Source IP", sourceIP),
    ("v", "mac_dest_addr", "string", "Destination MAC address (hex)", destinationMAC),
    ("v", "mac_source_addr", "string", "Source MAC address last two are replaced (hex)", sourceMAC),
    ("s", "ethernet_type", "string", "Ethernet type (16b, hex)", ethernetType),
    ("s", "ip_version", "string", "IP version number (4b, hex)", ipVersion),
    ("s", "IHL", "string", "Internet header length in 32b words (4b)", headerLength),
    ("s", "service_type", "string", "Type of desired service for IP packets (8b)", serviceType),
    ("s", "TTL", "string", "Time in seconds for IP packet to stay in the Ethernet (8b)", ethernetTime),
    ("s", "next_level_protocol", "string", "Next level protocol (8b)", nextProtocol),
    ("s", "gbe_forced_stop_cycle", "string", "GBE flow control", gbeCtrl),
    ("c", "General parameters defining TTC and processing operation"),
    ("s", "DETECTOR_ID", "int", "Detector ID (1-VeLo, 2-ST, ...)", detectorID),
    ("s", "pedestal_bank_schedule", "int", "Pedestal bank schedule 1=ped bank always accompagnies NZS bank", pedBankSchedule),
    ("s", "TTC_info_enable", "int", "1=TTC trigger used, 0=triggers and all info sent by ECS", ttcInfoEnable),
    ("s", "External_trigger_input_enable", "int", "1=Ext trigger used, 0=disable Ext trigger", extTrigEnable),
    ("s", "TTC_trigger_type_available", "int", "TTC_trigger_type_available, 1=TTC, 0=ECS", ttcTrigAvailable),
    ("s", "TTC_dest_ip_available", "int", "TTC_dest_ip_available, 1=TTC, 0=ECS", ttcDestIPAvailable),
    ("s", "detector_data_generator_enabled", "int", "Detector data generator enable", dataGenEnable),
    ("s", "SEP_generator_enabled", "int", "SEP generator enable", sepGenEnable),
    ("s", "pp_derandmizer_event_threshold", "int", "PP derandomizer event usage threshold", ppDerandThreshold),
    ("s", "mep_pid", "string", "MEP PID (hex)", mepPID),
    ("s", "source_id", "string", "Source ID (16b, hex)", sourceID),
    ("s", "version", "string", "Version (8b, hex)", version),
    ("s", "zs_class", "string", "Class of ZS bank (8b, hex)", classZsBank),
    ("s", "non_zs_class", "string", "Class of NZS bank (8b, hex)", classNzsBank),
    ("s", "pedestal_class", "string", "Class of pedestal bank (8b, hex)", classPedBank),
    ("s", "error_class", "string", "Class of error bank (8b, hex)", classErrBank),
    ("s", "FE_data_check_enable", "string", "FE data check (1b, hex) 1=Check, empty event sent if no FE data", feDataCheck),
    ("s", "serious_err_throttle_enable", "string", "Enable/disable serious error throttle (1b, hex), 1=Enable", seriourErrThrottle),
    ("s", "nzs_bank_period_factor", "string", "NZS bank period (32b, hex) 0=Disable periodic NZS bank", nzsBankPeriod),
    ("s", "nzs_bank_offset", "string", "NZS bank offset (9b, hex)", nzsBankOffset),
    ("s", "trigger_n", "string", "Trigger number in each DAQ loop (24b, hex)", triggerNumber),
    ("s", "consecutive_n", "int", "Consecutive trigger length, max=16", consecutiveTrigger),
    ("s", "wait_cycle", "int", "Wait cycles min=36 (16b)", waitCycles),
    ("s", "TRIGGER_TYPE", "int", "ECS trigger type 5=NZS, other=physics", ecsTrigType),
    ("s", "error_bank_disable", "int", "Error bank disable 1=Disable error bank", errBankDisable),
    ("s", "force_info_disable", "int", "Info bank disable 1=Disable error bank at PP", infoBankDisable),
    ("s", "force_info_enable", "int", "Info bank enable 1=Force error bank at PP", infoBankEnable),
    ("s", "nzs_for_error_enable", "int", "NZS in case of error 1=enable", nzsErrEnable),
    ("s", "detector_specific_bank_header_enable", "int", "Enable specific bank header which is used for ST, VeLo, OT, MUON", specHeaderEnable),
    ("s", "MEP_FACTOR", "int", "ECS mep factor, max = 32", ecsMepFactor),
    ("s", "MTU_SIZE", "int", "MTU size for the Ethernet Default=1500, Max=8192+20", mtuSize),
    ("s", "HLT_PORT", "string", "GBE port select 4LSB for 4 ports (hex)", gbePortSelection),
    ("s", "HLT_DP_EN", "int", "HLT data flow enable", hltDpEnable),
    ("s", "MEP_USE_THR", "string", "Threshold for MEP buffer", mepUseThr),
    ("s", "Thottle_PPx_en", "string", "Threshold Enable for each PP", throttlePPxEnable),
    ("s", "Thottle_MEP_buffer_en", "int", "Enable for MEP buffer throttle source", throttleMepBufferEnable),
    ("s", "trigger_info_throttle_en", "int", "Enable for trigger info throttle source", triggerInfoThrottleEnable),
    ("s", "throttle_en", "int", "Overall throttle enable", throttleEnable),
    ("s", "Firmware", "string", "Expected firmware version x.y => XY (hex)", expectedFw),
    ("s", "TTCRx", "string", "TTCRx.Control magic value (hex)", "0xBB"),
    ("c", "ST specific part"),
    ("s", "Pseudo_header_lo_thr", "string", "Low  threshold for Pseudo header", pseudHeaderLThresholds),
    ("s", "Pseudo_header_hi_thr", "string", "High threshold for Pseudo header", pseudHeaderHThresholds),
    ("s", "zerosuppress_enable", "int", "ZS Enable: used in PS and Clustermaker process", zsEnableVal),
    ("s", "header_correction_enable", "int", "Header Enable: used in PS process", headerCorrEnableVal),
    ("s", "header_corr_threshold_lo", "int", "Lower header threshold: used in PS process", headerLowThresholdVal),
    ("s", "header_corr_threshold_hi", "int", "Upper header threshold: used in PS process", headerHighThresholdVal),
    ("v", "header_corr_value_00", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_01", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_02", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_03", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_04", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_05", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_06", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_07", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_08", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_09", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_10", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_11", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_12", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_13", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_14", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_15", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_16", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_17", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_18", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_19", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_20", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_21", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_22", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_23", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_24", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_25", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_26", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_27", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_28", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_29", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_30", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_31", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_32", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_33", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_34", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_35", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_36", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_37", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_38", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_39", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_40", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_41", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_42", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_43", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_44", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_45", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_46", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_47", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_48", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_49", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_50", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_51", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_52", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_53", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_54", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_55", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_56", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_57", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_58", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_59", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_60", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_61", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_62", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_63", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_64", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_65", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_66", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_67", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_68", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_69", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_70", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_71", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_72", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_73", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_74", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_75", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_76", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_77", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_78", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_79", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_80", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_81", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_82", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_83", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_84", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_85", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_86", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_87", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_88", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_89", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_90", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_91", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_92", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_93", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_94", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("v", "header_corr_value_95", "int", "Lower and upper correction per link: used in PS process", headerCorrValuesVal),
    ("s", "pedestal_enable", "int", "Pedestal Enable: used in PS process", pedEnableVal),
    ("s", "pedestal_auto_update_enable", "int", "Update Enable: used in PS process", pedUpdateEnableVal),
    ("s", "lcms_enable", "int", "CMS Enable: used in CMS process", lcmsEnableVal),
    ("s", "cluster_number_max", "int", "Max number of clusters per PP: used in ZS process", maxCluVal),
    ("v", "orx_beetle_disable_0", "int", "Optical link disabled (1) or enabled (0) for links 0 to 11", DisBeetles_0_11Val),
    ("v", "orx_beetle_disable_1", "int", "Optical link disabled (1) or enabled (0) for links 12 to 23", DisBeetles_12_23Val),
    ("v", "orx_lck_ref_0", "int", "All 24 optical links need this flag to be set 1 for proper operation", EnableTlk_0_11Val ),
    ("v", "orx_lck_ref_1", "int", "All 24 optical links need this flag to be set 1 for proper operation", EnableTlk_12_23Val ),
    ("s", "orx_tlk_en", "int", "All en set to 1, tlk ch 0..5 en (bit0 and bit3 of orx_ctr_reg)", EnableOrxVal),
    ("v", "arx_beetle_disable_0", "string", "Analog port disabled/enabled, 4 bits per beetle, for links 0 to 11", DisBLinks_0_11Val),
    ("v", "arx_beetle_disable_1", "string", "Analog port disabled/enabled, 4 bits per beetle, for links 12 to 23", DisBLinks_12_23Val),
    ("v", "pcn_select", "string", "Select which PCN is sent with the ZS bank 0...5 per PP (hex)", pcnSelect),
    ("v", "pedestal", "int", "Initial pedestal values per strip: used in PS process", pedValueVal),
    ("v", "pedestal_mask", "int", "Masks strips to be skipped in PS process", pedMaskVal),
    ("v", "hit_threshold", "int", "Hit thresholds per strip: used in ZS process", HitThresholdVal),
    ("v", "cms_threshold", "int", "CMS thresholds per strip: used in CMS process", CMSThresholdVal),
    ("v", "spill_over_threshold", "int", "SpillOver thresholds (48): used in ZS process", SpillOverThresholdVal),
    ("v", "confirmation_threshold", "int", "Confirmation thresholds (48): used in ZS process", ConfirmationThresholdVal),
    ("v", "start_strip_n_0", "int", "The start_strip_n channel 0-23, values per Process_channel", StartStrips0_23),
    ("v", "start_strip_n_1", "int", "The start_strip_n channel 24-47, values per Process_channel", StartStrips24_47),
    ("c", "Data RAM generator"),
    ("c", "PP 0"),
    ("v", "st_data_gen_array_0_0_0", "int", "optical data generator RAM, PP0, Beetle 0, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_0_0_1", "int", "optical data generator RAM, PP0, Beetle 0, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_0_2", "int", "optical data generator RAM, PP0, Beetle 0, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_0_3", "int", "optical data generator RAM, PP0, Beetle 0, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_1_0", "int", "optical data generator RAM, PP0, Beetle 1, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_0_1_1", "int", "optical data generator RAM, PP0, Beetle 1, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_1_2", "int", "optical data generator RAM, PP0, Beetle 1, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_1_3", "int", "optical data generator RAM, PP0, Beetle 1, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_2_0", "int", "optical data generator RAM, PP0, Beetle 2, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_0_2_1", "int", "optical data generator RAM, PP0, Beetle 2, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_2_2", "int", "optical data generator RAM, PP0, Beetle 2, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_2_3", "int", "optical data generator RAM, PP0, Beetle 2, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_3_0", "int", "optical data generator RAM, PP0, Beetle 3, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_0_3_1", "int", "optical data generator RAM, PP0, Beetle 3, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_3_2", "int", "optical data generator RAM, PP0, Beetle 3, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_3_3", "int", "optical data generator RAM, PP0, Beetle 3, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_4_0", "int", "optical data generator RAM, PP0, Beetle 4, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_0_4_1", "int", "optical data generator RAM, PP0, Beetle 4, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_4_2", "int", "optical data generator RAM, PP0, Beetle 4, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_4_3", "int", "optical data generator RAM, PP0, Beetle 4, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_5_0", "int", "optical data generator RAM, PP0, Beetle 5, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_0_5_1", "int", "optical data generator RAM, PP0, Beetle 5, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_5_2", "int", "optical data generator RAM, PP0, Beetle 5, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_0_5_3", "int", "optical data generator RAM, PP0, Beetle 5, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("c", "PP 1"),
    ("v", "st_data_gen_array_1_0_0", "int", "optical data generator RAM, PP1, Beetle 0, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_1_0_1", "int", "optical data generator RAM, PP1, Beetle 0, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_0_2", "int", "optical data generator RAM, PP1, Beetle 0, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_0_3", "int", "optical data generator RAM, PP1, Beetle 0, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_1_0", "int", "optical data generator RAM, PP1, Beetle 1, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_1_1_1", "int", "optical data generator RAM, PP1, Beetle 1, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_1_2", "int", "optical data generator RAM, PP1, Beetle 1, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_1_3", "int", "optical data generator RAM, PP1, Beetle 1, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_2_0", "int", "optical data generator RAM, PP1, Beetle 2, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_1_2_1", "int", "optical data generator RAM, PP1, Beetle 2, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_2_2", "int", "optical data generator RAM, PP1, Beetle 2, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_2_3", "int", "optical data generator RAM, PP1, Beetle 2, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_3_0", "int", "optical data generator RAM, PP1, Beetle 3, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_1_3_1", "int", "optical data generator RAM, PP1, Beetle 3, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_3_2", "int", "optical data generator RAM, PP1, Beetle 3, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_3_3", "int", "optical data generator RAM, PP1, Beetle 3, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_4_0", "int", "optical data generator RAM, PP1, Beetle 4, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_1_4_1", "int", "optical data generator RAM, PP1, Beetle 4, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_4_2", "int", "optical data generator RAM, PP1, Beetle 4, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_4_3", "int", "optical data generator RAM, PP1, Beetle 4, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_5_0", "int", "optical data generator RAM, PP1, Beetle 5, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_1_5_1", "int", "optical data generator RAM, PP1, Beetle 5, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_5_2", "int", "optical data generator RAM, PP1, Beetle 5, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_1_5_3", "int", "optical data generator RAM, PP1, Beetle 5, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("c", "PP 2"),
    ("v", "st_data_gen_array_2_0_0", "int", "optical data generator RAM, PP2, Beetle 0, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_2_0_1", "int", "optical data generator RAM, PP2, Beetle 0, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_0_2", "int", "optical data generator RAM, PP2, Beetle 0, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_0_3", "int", "optical data generator RAM, PP2, Beetle 0, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_1_0", "int", "optical data generator RAM, PP2, Beetle 1, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_2_1_1", "int", "optical data generator RAM, PP2, Beetle 1, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_1_2", "int", "optical data generator RAM, PP2, Beetle 1, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_1_3", "int", "optical data generator RAM, PP2, Beetle 1, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_2_0", "int", "optical data generator RAM, PP2, Beetle 2, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_2_2_1", "int", "optical data generator RAM, PP2, Beetle 2, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_2_2", "int", "optical data generator RAM, PP2, Beetle 2, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_2_3", "int", "optical data generator RAM, PP2, Beetle 2, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_3_0", "int", "optical data generator RAM, PP2, Beetle 3, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_2_3_1", "int", "optical data generator RAM, PP2, Beetle 3, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_3_2", "int", "optical data generator RAM, PP2, Beetle 3, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_3_3", "int", "optical data generator RAM, PP2, Beetle 3, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_4_0", "int", "optical data generator RAM, PP2, Beetle 4, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_2_4_1", "int", "optical data generator RAM, PP2, Beetle 4, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_4_2", "int", "optical data generator RAM, PP2, Beetle 4, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_4_3", "int", "optical data generator RAM, PP2, Beetle 4, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_5_0", "int", "optical data generator RAM, PP2, Beetle 5, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_2_5_1", "int", "optical data generator RAM, PP2, Beetle 5, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_5_2", "int", "optical data generator RAM, PP2, Beetle 5, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_2_5_3", "int", "optical data generator RAM, PP2, Beetle 5, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("c", "PP 3"),
    ("v", "st_data_gen_array_3_0_0", "int", "optical data generator RAM, PP3, Beetle 0, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_3_0_1", "int", "optical data generator RAM, PP3, Beetle 0, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_0_2", "int", "optical data generator RAM, PP3, Beetle 0, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_0_3", "int", "optical data generator RAM, PP3, Beetle 0, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_1_0", "int", "optical data generator RAM, PP3, Beetle 1, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_3_1_1", "int", "optical data generator RAM, PP3, Beetle 1, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_1_2", "int", "optical data generator RAM, PP3, Beetle 1, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_1_3", "int", "optical data generator RAM, PP3, Beetle 1, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_2_0", "int", "optical data generator RAM, PP3, Beetle 2, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_3_2_1", "int", "optical data generator RAM, PP3, Beetle 2, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_2_2", "int", "optical data generator RAM, PP3, Beetle 2, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_2_3", "int", "optical data generator RAM, PP3, Beetle 2, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_3_0", "int", "optical data generator RAM, PP3, Beetle 3, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_3_3_1", "int", "optical data generator RAM, PP3, Beetle 3, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_3_2", "int", "optical data generator RAM, PP3, Beetle 3, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_3_3", "int", "optical data generator RAM, PP3, Beetle 3, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_4_0", "int", "optical data generator RAM, PP3, Beetle 4, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_3_4_1", "int", "optical data generator RAM, PP3, Beetle 4, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_4_2", "int", "optical data generator RAM, PP3, Beetle 4, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_4_3", "int", "optical data generator RAM, PP3, Beetle 4, link3, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_5_0", "int", "optical data generator RAM, PP3, Beetle 5, link0, words 0-35, values per strip, header included", GeneratorValuesLink0),
    ("v", "st_data_gen_array_3_5_1", "int", "optical data generator RAM, PP3, Beetle 5, link1, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_5_2", "int", "optical data generator RAM, PP3, Beetle 5, link2, words 0-35, values per strip, header included", GeneratorValuesLink1),
    ("v", "st_data_gen_array_3_5_3", "int", "optical data generator RAM, PP3, Beetle 5, link3, words 0-35, values per strip, header included", GeneratorValuesLink1)]

# ------------------------------------
# --> creating the Xml file's content
# ------------------------------------
# --> Header of the Xml
XmlFile.write(head0)
XmlFile.write(head1)
# --> <DDDB>
XmlFile.write(DDDBTag)
for n in range(len(TELL1IDs)):
  # --> Condition collection Tag
  # --> dynamic name of the condition, src MAC and IP addresses
  # --> <condition ... >
  condTarget = "TELL1Board" + str(TELL1IDs[n]) + '">\n'
  Hex = str(hex( TELL1IDs[n] ))
  nbr = str( TELL1Nbr[n] )
  if (len(nbr) < 2):
      nbr = '0' + nbr
      Hex = Hex.replace('0x', '0x0')
  nbr += ' '
  Hex += ' 0x00 '
  
  XmlFile.write(' ' + condTag + condTarget)
  # --> conditions - single number first -- loop 1
  for i in range(len(List)):
      if (List[i][0] == "s"):
          # It's a scalar value
          XmlFile.write(condANumberTag + List[i][1] + "\" type=\"" + List[i][2] + "\" comment=\"" + List[i][3] + "\"> ")
          if (List[i][1] == 'Tell_name'):
              XmlFile.write(List[i][4] + nbr + endANumberTag)
          elif (List[i][1] == 'source_id'):
              XmlFile.write(hex( TELL1IDs[n] )+ ' ' + endANumberTag)
          elif (List[i][1] == 'nzs_bank_offset'):
              if (sys.argv[1] == "IT"):
                  XmlFile.write(hex( TELL1Nbr[n]*10 )+ ' ' + endANumberTag)
              else:
                  XmlFile.write(hex( TELL1Nbr[n]*10 + 5 )+ ' ' + endANumberTag)
          else:
              XmlFile.write(List[i][4] + endANumberTag)
      elif (List[i][0] == "v"):
          XmlFile.write(condAVecTag + List[i][1] + "\" type=\"" + List[i][2] + "\" comment=\"" + List[i][3] + "\"> ")
          if (List[i][1] == 'ip_source_addr'):
              XmlFile.write(List[i][4] + str( TELL1Nbr[n] ) + ' ' + endAVecTag)
          elif (List[i][1] == 'mac_source_addr'):
              XmlFile.write(List[i][4] + '0x' + nbr + '0x00 ' + endAVecTag)
          else:
              XmlFile.write(List[i][4] + endAVecTag)
      elif (List[i][0] == "c"):
          XmlFile.write(condACommentTag + List[i][1] + endACommentTag)
  XmlFile.write(endCondTag)
      
# --> </DDDB>
XmlFile.write(endDDDBTag)

# --> close the Xml file
XmlFile.close()
