#!/usr/bin/env python

#
# Script that reads a set of DB and fills a TTree with interesting values.
# These interetinfg values are defined in Utils.py
#
# Author : Johan Luisier <johan.luisier@epfl.ch>
#

import sys

for arg in sys.argv:
    print arg

if ( len(sys.argv) != 5):
    print "Usage DB optionFile treename detType file"
    sys.exit(1)

import GaudiPython
from Gaudi.Configuration import *
gbl = GaudiPython.gbl
std = gbl.std
STChan = GaudiPython.gbl.LHCb.STChannelID
from ROOT import TTree, AddressOf, gROOT, TFile
from Utils import Data, BranchWriteSetup, fillValues, writeValues

f = TFile(sys.argv[4],'UPDATE')

listVal = Data()

tree = TTree(sys.argv[2], "TTree for " + sys.argv[2])
BranchWriteSetup( tree, listVal )

importOptions('$STDOPTS/LHCbApplication.opts')
#importOptions('$STDOPTS/DstDicts.opts')
importOptions(sys.argv[1])
#from Configurables import CondDB
#cdb = CondDB()
#cdb.addLayer(connStr="sqlite_file:$VETRAROOT/VetraCondDB/TTCOND.db/COND")
#cdb.PartitionConnectionString["LHCBCOND"] = "sqlite_file:$HOME/cmtuser/LHCb_v26r1/myLHCBCOND.db/LHCBCOND"

from Configurables import STVetraCfg
myTool = STVetraCfg()
myTool.DetType = sys.argv[3]
myTool.OutputLevel = 3

appMgr = GaudiPython.AppMgr()
det    = appMgr.detSvc()

# IT boards
AllBoards = []
if ( sys.argv[3] == 'IT' ):
    AllBoardIDs = range(0, 14)
    AllBoardIDs . extend( range(32, 46) )
    AllBoardIDs . extend( range(64, 78) )
else:
    # TT boards
    AllBoardIDs = range(0, 11)
    AllBoardIDs . extend( range(32, 43) )
    AllBoardIDs . extend( range(64, 77) )
    AllBoardIDs . extend( range(96, 108) )

toolName = sys.argv[3] + 'ReadoutTool'

stool = appMgr.toolsvc().create( toolName, interface = 'ISTReadoutTool' )
vtool = appMgr.toolsvc().create( 'STVetraCfg', interface ='ISTVetraCfg' )

svcBox  = ''

for tell1ID in AllBoardIDs:
    print tell1ID, '->', vtool.getSTVetraCond( tell1ID ).tellName()
    
    boardID = gbl.STTell1ID(tell1ID)

    allSectors = stool.sectors(boardID)

    for i in range(allSectors.size()):
        mySector = allSectors.at(i)
        strips = mySector.nStrip()
        
        for j in range(strips):
            chanID = STChan( mySector.stripToChan( j + 1 ) )
            if ( svcBox != stool.serviceBox( chanID ) ):
                svcBox = stool.serviceBox( chanID )
            svcBoxID = 0
            if ( sys.argv[3] == 'TT' ):
                if ( svcBox[0] == 'C' ):
                    svcBoxID += 100
                if ( svcBox[0] == 'T' ):
                    svcBoxID += 10
                svcBoxID += int( svcBox[2] )
            else:
                svcBoxID += 100 * ( int( svcBox[2] ) - 1 )
                if ( svcBox[0] == 'C' ):
                    svcBoxID += 10
                svcBoxID += int( svcBox[4] )
            myPair = stool.offlineChanToDAQ( chanID, 0. )
            chan = int( myPair.second )
            values = []
            fillValues( stool, vtool, mySector, tell1ID, j, chan, svcBoxID, chanID, values )
            writeValues( listVal, values )
            tree.Fill()

f.SetCompressionLevel( 9 )

f.Write()

f.Close()
