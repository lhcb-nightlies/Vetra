#!/usr/bin/env python

#

#

#
# Author : Johan Luisier <johan.luisier@epfl.ch>
#

import sys, fileinput

args = sys.argv[1:]

nbr = len(args)

if ( nbr == 0 ):
    print "Try UpdateVetraCondFromLHCBCOND.py -h\n"
    sys.exit( 1 )

cond = []
stdb = []
file = []
type = ''

path = ""

for arg in args:
    if ( arg.find('--f') != -1 or arg =='-f' ):
        path = 'file'
    elif ( arg.find('--c') != -1 or arg == '-c' ):
        path = 'cond'
    elif ( arg.find('--v') != -1 or arg == '-v' ):
        path = 'stvetra'
    elif ( arg.find('--s') != -1 or arg == '-s' ):
        path = 'sub'
    elif ( arg.find('--h') != -1 or arg == '-h' ):
        print """Usage UpdateVetraCondFromLHCBCOND.py options
options are :
  --cond / -c    path string to LHCBCOND file
  --vetra / -v   path string to ST Vetra CondDB file
  --file / -f    name of output file
  --sub / -s     subdetector type : either IT or TT.
  --help / -h    print this help screen

The order doesn't matter!"""
        sys.exit( 0 )
    else:
        if ( path == 'cond' ):
            cond . append ( arg )
        elif ( path == 'stvetra' ):
            stdb . append ( arg )
        elif ( path == 'sub' ):
            type = arg
        elif ( path == 'file' ):
            file . append ( arg )
        else:
            print "Error, dont know what to do with argument :", arg
        

#from ConfigUpdate import MaskFromDB, ThresholdsAndValues

#if ( len( ThresholdsAndValues ) % 3 != 0 ):
#    print "Error : thresholds definition does not seem to be correct, please check ConfigUpdate.py"
#    sys.exit( 1 )

import GaudiPython
from Gaudi.Configuration import *
gbl = GaudiPython.gbl
std = gbl.std
STChan = GaudiPython.gbl.LHCb.STChannelID
vecInt = std.vector( 'int' )
vecStr = std.vector( 'string' )

from Configurables import LHCbApp
lhcbApp = LHCbApp()
lhcbApp.DDDBtag   = "Default"
lhcbApp.CondDBtag = "Default"
ApplicationMgr( OutputLevel = INFO, AppName = 'UpdateVetraCondFromLHCBCOND')

#importOptions('$STDOPTS/LHCbApplication.opts')
#importOptions('$STDOPTS/DstDicts.opts')
#importOptions(sys.argv[1])

from Configurables import CondDB
cdb = CondDB()
cdb.addLayer(connStr="sqlite_file:" + stdb[0] + "/COND")
if ( len( cond ) == 1 ):
    cdb.Tags["LHCBCOND"] = cond[0]
elif ( len( cond ) == 2 ):
    cdb.Tags["LHCBCOND"] = cond[0]
    cdb.addLayer(connStr="sqlite_file:" + cond[1] + "/LHCBCOND")
    
try:
    outfile = open( file[0], 'w' )
except IOError, (errno, strerror):
    print "I/O error(%s): %s" % (errno, strerror)
    sys.exit( 1 )

head0 = '<?xml version="1.0" encoding="ISO-8859-1"?>\n'
head1 = '<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd">\n'

outfile.write( head0 )
outfile.write( head1 )
outfile.write( "<DDDB>" )

from Configurables import STVetraCfg
myTool = STVetraCfg()
myTool.DetType = type
myTool.OutputLevel = 3

appMgr = GaudiPython.AppMgr()
det    = appMgr.detSvc()

# IT boards
AllBoardIDs = []
if ( type == 'IT' ):
    AllBoardIDs = range(0, 14)
    AllBoardIDs . extend( range(32, 46) )
    AllBoardIDs . extend( range(64, 78) )
else:
    # TT boards
    AllBoardIDs = range(0, 11)
    AllBoardIDs . extend( range(32, 43) )
    AllBoardIDs . extend( range(64, 77) )
    AllBoardIDs . extend( range(96, 109) )

toolName = type + 'ReadoutTool'

stool = appMgr.toolsvc().create( toolName, interface = 'ISTReadoutTool' )
vtool = appMgr.toolsvc().create( 'STVetraCfg', interface = 'ISTVetraCfg' )

# Prepare the containers

Condition = det['/dd/Conditions/ReadoutConf/IT/ClusteringThresholds']
if ( type == 'TT' ):
    Condition   = det['/dd/Conditions/ReadoutConf/TT/ClusteringThresholds']

MaskFromDB = [ 1, # Open
               2, # Short
               #3, # Pinhole
               4, # ReadoutProblems
               5, # NotBonded
               6, # LowGain
               7, # Noisy
               9, # OtherFault
               10, # Dead
               100 ] # UnknownStatus

ThresholdsAndValues = [ "hit_threshold"         , "HitThreshold" , 3072,
                        "cms_threshold"         , "CMSThreshold" , 3072,
                        "confirmation_threshold", "ConfirmationThreshold" , 48,
                        "spill_over_threshold"  , "SpilloverThreshold" , 48 ]

numBadChannels = { }

def readThresholdFromLHCBCOND( Condition,name ) :
    return Condition.paramAsDouble( name )

# Iterate over thresholds
for i in range( len( ThresholdsAndValues ) / 3 ):
    threshold = ThresholdsAndValues[ 3*i ]
    sigma = readThresholdFromLHCBCOND( Condition, ThresholdsAndValues[ 3*i + 1] )
    granularity = ThresholdsAndValues[ 3*i + 2 ]

    print "Updating", ThresholdsAndValues[ 3*i + 1 ]  
    
     # Iterate over the TELL1 boards
    for iter in range( len( AllBoardIDs ) ):
        tell1ID = AllBoardIDs[ iter ]
        vetraCond = vtool.getSTVetraCond( tell1ID )
        #print tell1ID, '->', vetraCond.tellName()

        allThresholds = vecInt( granularity, 99 )
        
        if ( threshold == "hit_threshold" ):
            mask = vecInt( 3072, 1 )
            
        boardID = gbl.STTell1ID( tell1ID )
        allSectors = stool.sectors( boardID )

        for sector in allSectors:
            #print sector.nickname()
            value = 0.
            n = 0
            for strip in range( sector.nStrip() ):
                chanID = sector.stripToChan( strip + 1 )
                myPair = stool.offlineChanToDAQ( chanID, 0. )
                chan = int( myPair.second )
                noiseValue = sector.noise( chanID )
                chanStatus = sector.stripStatus( chanID )

                if( granularity == 3072 ) :
                    if ( MaskFromDB.count( chanStatus ) > 0 ) : # mask everything that is not OK
                        if( threshold == "hit_threshold" ) :
                            if chanStatus not in numBadChannels : numBadChannels[ chanStatus ] = 1
                            else : numBadChannels[ chanStatus ] = numBadChannels[ chanStatus ] + 1 
                    else :
                        mask[ chan ] = 0
                        allThresholds[ chan ] = int( sigma * noiseValue + .5 )
                    continue    
                        
                if ( MaskFromDB.count( chanStatus ) == 0 ) :       
                    n += 1
                    value = ( value * float( n - 1 ) + noiseValue ) / float( n )
                    #print tell1ID, strip, chan, n, value
                    if( noiseValue > 500. ) :
                        print "Inconsitency between Mask and Noise in LHCBCOND ", tell1ID, strip, chanStatus, noiseValue
                else :
                    if( noiseValue < 500. ) :
                        print "Inconsitency between Mask and Noise in LHCBCOND ", tell1ID, strip, chanStatus, noiseValue
                        
                if ( ( strip + 1 ) % ( 3072 / granularity ) == 0 ) :
                    if( n > 0 ) :
                        allThresholds[ chan / ( 3072 / granularity ) ] = int( sigma * value + .5 )
                        #print "--------> ", value
                    
                    value = 0.
                    n = 0

        if ( threshold == "hit_threshold" ):
            vetraCond.hitThreshold( allThresholds )
            vetraCond.pedestalMask( mask )
        elif ( threshold == "cms_threshold" ):
            vetraCond.cmsThreshold( allThresholds )
        elif ( threshold == "confirmation_threshold" ):
            vetraCond.confirmationThreshold( allThresholds )
        elif ( threshold == "spill_over_threshold" ):
            vetraCond.spilloverThreshold( allThresholds )                    
        else:
            print "Unknown threshold name :", threshold

        #print "STVetraCondition updated"

completeBeetle = [ 1 ] * 128
clearBeetle    = [ 0 ] * 128
completeLink   = [ 1 ] * 32

deadBeetle = 0
deadPort   = 0

for iter in range( len( AllBoardIDs ) ):
    tell1ID = AllBoardIDs[ iter ]
    #print "TELL1Board" + str( tell1ID )
    vetraCond = vtool.getSTVetraCond( tell1ID )
    boardID = gbl.STTell1ID( tell1ID )

    stripState = [ 0 ] * 3072
    for i in range( 3072 ):
        stripState[ i ] = vetraCond.pedestalMask().at( i )
 
    #print stripState

    beetleState = vecInt( 24, 0 )
    linkState   = vecStr( 24, '0x0' )

    #allSectors = stool.sectors( boardID )

    #for sector in allSectors:
        #for strip in range( sector.nStrip() ):
                #chanID = sector.stripToChan( strip + 1 )
                #myPair = stool.offlineChanToDAQ( chanID, 0. )
                #chan = int( myPair.second )
                #chanStatus = int( sector.stripStatus( chanID ) )
                #if ( MaskFromDB.count( chanStatus ) > 0 ):
                    #stripState[ chan ] = 1

    for i in range( 24 ):
        if ( stripState[ i*128:(i+1)*128 ] == completeBeetle ):
            beetleState[i] = 1
            linkState[i]   = '0xF'
            deadBeetle += 1
        elif ( stripState[ i*128:(i+1)*128 ] == clearBeetle ):
            continue
        else:
            number = 0
            for j in range( 4 ):
                if ( stripState[ i*128 + j *32: i*128 + (j+1)*32 ] == completeLink ):
                    number += 2**j
                    deadPort += 1
                    
            linkState[ i ] = str( hex( number ) )

    tmp = ""
    for i in range( 24 ):
        tmp += str( beetleState[ i ] ) + ' '
    #print tmp
    tmp = ""
    for i in range( 24 ):
        tmp += linkState[ i ] + ' '
    #print tmp
    vetraCond.disableBeetles( beetleState )
    vetraCond.disableLinksPerBeetle( linkState )

print ""
print "Number of disabled beetles :", deadBeetle
print "Number of disabled ports   :", deadPort 
print "Number of bad channels     :", numBadChannels, "\n" 

for iter in range( len( AllBoardIDs ) ):
    tell1ID = AllBoardIDs[ iter ]
    vetraCond = vtool.getSTVetraCond( tell1ID )
    outfile.write( vetraCond.toXml( "TELL1Board" + str( tell1ID ), False ) )
    print "Condition TELL1Board" + str( tell1ID ), "written"

outfile.write( "</DDDB>" )
outfile.close()

import os

os.system( "$STVETRAANALYSISROOT/perl/DBCosmetics.pl " + file[0] )
#os.system( "mv " + file[0] + " " + file[0] )

