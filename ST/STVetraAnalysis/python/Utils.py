# Utility module
#
# Author : Johan Luisier <johan.luisier.ch>
#
# Defines a structure and the functions to read / write a tree using
# this structure.
#
# Do not edit if you're not sure what to do, use the WriteUtils scrips instead!
#

from ROOT import gROOT

# Create a struct
gROOT.ProcessLine(\
	"struct Data{\
	Int_t sector;\
	Int_t strip;\
	Int_t channel;\
	Int_t tell1;\
	Int_t ped;\
	Int_t mask;\
	Int_t masked;\
	Int_t CMS;\
	Int_t Hit;\
	Int_t Data;\
	Int_t Conf;\
	Int_t svcBox;\
	Int_t station;\
	Float_t noise;\
};")
from ROOT import Data, TTree, AddressOf

def NbrOfFields():
	return 14;

def BranchWriteSetup( myTree, structName ):
	"""Sets the branches of the given TTree myTree, using the data structure
	structName (which is of type Data)."""
	myTree.Branch("sector", AddressOf(structName, 'sector'), "sector/I")
	myTree.Branch("strip", AddressOf(structName, 'strip'), "strip/I")
	myTree.Branch("channel", AddressOf(structName, 'channel'), "channel/I")
	myTree.Branch("tell1", AddressOf(structName, 'tell1'), "tell1/I")
	myTree.Branch("ped", AddressOf(structName, 'ped'), "ped/I")
	myTree.Branch("mask", AddressOf(structName, 'mask'), "mask/I")
	myTree.Branch("masked", AddressOf(structName, 'masked'), "masked/I")
	myTree.Branch("CMS", AddressOf(structName, 'CMS'), "CMS/I")
	myTree.Branch("Hit", AddressOf(structName, 'Hit'), "Hit/I")
	myTree.Branch("Data", AddressOf(structName, 'Data'), "Data/I")
	myTree.Branch("Conf", AddressOf(structName, 'Conf'), "Conf/I")
	myTree.Branch("svcBox", AddressOf(structName, 'svcBox'), "svcBox/I")
	myTree.Branch("station", AddressOf(structName, 'station'), "station/I")
	myTree.Branch("noise", AddressOf(structName, 'noise'), "noise/F")


def BranchReadSetup( myTree, structName ):
	"""Links the branches of the given TTree myTree to the data structure
	structName (which is of type Data)."""
	myTree.SetBranchAddress("sector", AddressOf(structName, 'sector') )
	myTree.SetBranchAddress("strip", AddressOf(structName, 'strip') )
	myTree.SetBranchAddress("channel", AddressOf(structName, 'channel') )
	myTree.SetBranchAddress("tell1", AddressOf(structName, 'tell1') )
	myTree.SetBranchAddress("ped", AddressOf(structName, 'ped') )
	myTree.SetBranchAddress("mask", AddressOf(structName, 'mask') )
	myTree.SetBranchAddress("masked", AddressOf(structName, 'masked') )
	myTree.SetBranchAddress("CMS", AddressOf(structName, 'CMS') )
	myTree.SetBranchAddress("Hit", AddressOf(structName, 'Hit') )
	myTree.SetBranchAddress("Data", AddressOf(structName, 'Data') )
	myTree.SetBranchAddress("Conf", AddressOf(structName, 'Conf') )
	myTree.SetBranchAddress("svcBox", AddressOf(structName, 'svcBox') )
	myTree.SetBranchAddress("station", AddressOf(structName, 'station') )
	myTree.SetBranchAddress("noise", AddressOf(structName, 'noise') )


def DoComparison( structName1, structName2, result ):
	"""Compares the fields of structName1 and structName2 and dumps the
	result into result"""
	result.sector = structName1.sector
	result.strip = structName1.strip
	result.channel = structName1.channel
	result.tell1 = structName1.tell1
	result.ped = structName1.ped - structName2.ped
	result.mask = structName1.mask - structName2.mask
	result.masked = structName1.masked
	result.CMS = structName1.CMS - structName2.CMS
	result.Hit = structName1.Hit - structName2.Hit
	result.Data = structName1.Data
	result.Conf = structName1.Conf - structName2.Conf
	result.svcBox = structName1.svcBox
	result.station = structName1.station
	result.noise = structName1.noise

	return result

def fillValues( stTool, vetraTool, mSector, tell1ID, strip, channel, svcBox, stChanID, vals ):
	"""Reads the values from the DB (the Vetra one and/or LHCb one), using
	the ST sector given in argument, the TELL1 source ID, the strip and
	channel. The values are put in the array vals"""
	vCond = vetraTool.getSTVetraCond( tell1ID )
	vals.append( mSector.prodID() )
	vals.append( strip )
	vals.append( channel )
	vals.append( tell1ID )
	vals.append( vCond.pedestalValue().at( channel ) )
	vals.append( vCond.pedestalMask().at( channel ) )
	vals.append( vCond.pedestalMask().at( channel ) )
	vals.append( vCond.cmsThreshold().at( channel ) )
	vals.append( vCond.hitThreshold().at( channel ) )
	vals.append( vCond.pedestalValue().at( channel ) )
	vals.append( vCond.confirmationThreshold().at( channel / 64 ) )
	vals.append( svcBox )
	vals.append( stChanID.station() )
	vals.append( mSector.noise( stChanID ) )


def writeValues( structName, listOfValues ):
	"""Reads the wanted values from the array listOfValues and writes them
	into structName (which is of type Data)."""
	if ( len( listOfValues ) != NbrOfFields() ): return
	structName.sector = listOfValues[0] # mSector.prodID()
	structName.strip = listOfValues[1] # strip
	structName.channel = listOfValues[2] # channel
	structName.tell1 = listOfValues[3] # tell1ID
	structName.ped = listOfValues[4] # vCond.pedestalValue().at( channel )
	structName.mask = listOfValues[5] # vCond.pedestalMask().at( channel )
	structName.masked = listOfValues[6] # vCond.pedestalMask().at( channel )
	structName.CMS = listOfValues[7] # vCond.cmsThreshold().at( channel )
	structName.Hit = listOfValues[8] # vCond.hitThreshold().at( channel )
	structName.Data = listOfValues[9] # vCond.pedestalValue().at( channel )
	structName.Conf = listOfValues[10] # vCond.confirmationThreshold().at( channel / 64 )
	structName.svcBox = listOfValues[11] # svcBox
	structName.station = listOfValues[12] # stChanID.station()
	structName.noise = listOfValues[13] # mSector.noise( stChanID )

