#!/usr/bin/env python
###############################################################################
#                                                                             #
# This scripts makes a few modifications to the xml conditions file.          #
#                                                                             #
# author : Jeroen van Tilburg <jeroen.van.tilburg@cern.ch>                    #
#                                                                             #
###############################################################################

# Get the arguments
import sys, string, os
if len(sys.argv) != 2 and len(sys.argv) != 4 : 
    print '''\
Usage:
   adjustXml <file.xml> [<tag> <string>] \n

Script for adjusting a TELL1 xml file. The output xml file is called
file.xml.new. This script will modify the default xml file from the
write_st_xml_cond script such that the output xml-file contains the
correct settings for:
- the number of GBE links
- the pseudo header thresholds
- the unused digitizer boards in the TT
One can modify this scripts according to your needs (set the
\"correctLines\" parameter.

In case a <tag> and <string> are given on the command line this script
will only change the given string for the given tags for all TELL1s in
the input file. For example:
   adjustXml file.xml pedestal_auto_update_enable 1

'''
    sys.exit()
xmlfile = sys.argv[1] # Name of the xml file to process

###############################################################################
#                                                                             #
# All the modifications are defined here.                                     #
#                                                                             #
###############################################################################

# Correct the number of enabled GBE links for the IT
correctLines =  [( "ITTELL01", "HLT_PORT", "0xF" ),
                 ( "ITTELL02", "HLT_PORT", "0xF" ),
                 ( "ITTELL08", "HLT_PORT", "0xF" ),
                 ( "ITTELL09", "HLT_PORT", "0xF" ),
                 ( "ITTELL15", "HLT_PORT", "0xF" ),
                 ( "ITTELL16", "HLT_PORT", "0xF" ),
                 ( "ITTELL22", "HLT_PORT", "0xF" ),
                 ( "ITTELL23", "HLT_PORT", "0xF" ),
                 ( "ITTELL29", "HLT_PORT", "0xF" ),
                 ( "ITTELL30", "HLT_PORT", "0xF" ),
                 ( "ITTELL36", "HLT_PORT", "0xF" ),
                 ( "ITTELL37", "HLT_PORT", "0xF" )]

# Correct the number of enabled GBE links for the TT
correctLines +=  [( "TTTELL01", "HLT_PORT", "0xF" ),
                  ( "TTTELL02", "HLT_PORT", "0xF" ),
                  ( "TTTELL03", "HLT_PORT", "0xF" ),
                  ( "TTTELL04", "HLT_PORT", "0xF" ),
                  ( "TTTELL05", "HLT_PORT", "0xF" ),
                  ( "TTTELL06", "HLT_PORT", "0xF" ),
                  ( "TTTELL08", "HLT_PORT", "0xF" ),
                  ( "TTTELL09", "HLT_PORT", "0xF" ),
                  ( "TTTELL10", "HLT_PORT", "0xF" ),
                  ( "TTTELL13", "HLT_PORT", "0xF" ),
                  ( "TTTELL14", "HLT_PORT", "0xF" ),
                  ( "TTTELL15", "HLT_PORT", "0xF" )]

# Disable the unused digitizer boards in TT
correctLines += [( "TTTELL07", "orx_beetle_disable_0", "0 "*8 + "1 " *4 ),
                 ( "TTTELL07", "orx_beetle_disable_1", "0 "*8 + "1 " *4 ),
                 ( "TTTELL07", "arx_beetle_disable_0", "0x0 "*8 + "0xF " *4 ),
                 ( "TTTELL07", "arx_beetle_disable_1", "0x0 "*8 + "0xF " *4 ),
                 ( "TTTELL11", "orx_beetle_disable_0", "0 "*8 + "1 " *4 ),
                 ( "TTTELL11", "orx_beetle_disable_1", "0 "*8 + "1 " *4 ),
                 ( "TTTELL11", "arx_beetle_disable_0", "0x0 "*8 + "0xF " *4 ),
                 ( "TTTELL11", "arx_beetle_disable_1", "0x0 "*8 + "0xF " *4 ),
                 ( "TTTELL12", "orx_beetle_disable_0", "0 "*8 + "1 " *4 ),
                 ( "TTTELL12", "orx_beetle_disable_1", "0 "*8 + "1 " *4 ),
                 ( "TTTELL12", "arx_beetle_disable_0", "0x0 "*8 + "0xF " *4 ),
                 ( "TTTELL12", "arx_beetle_disable_1", "0x0 "*8 + "0xF " *4 ),
                 ( "TTTELL16", "orx_beetle_disable_0", "0 "*8 + "1 " *4 ),
                 ( "TTTELL16", "orx_beetle_disable_1", "0 "*8 + "1 " *4 ),
                 ( "TTTELL16", "arx_beetle_disable_0", "0x0 "*8 + "0xF " *4 ),
                 ( "TTTELL16", "arx_beetle_disable_1", "0x0 "*8 + "0xF " *4 )]

# Disable the NZS submission once an error bank is emitted
correctLines +=  [( "*", "nzs_for_error_enable", "0" )]

# Disable the NZS round robin
correctLines += [( "*", "nzs_bank_period_factor", "0x0" )]

# example: to turn on the pedestal following
#correctLines =  [( "*", "pedestal_auto_update_enable", "1" )]

###############################################################################
###############################################################################

if len(sys.argv) == 4 :
    correctLines =  [( "*", sys.argv[2], sys.argv[3])]

from xml.dom import minidom, Node    

print "\n Parsing the xml file.\n"
# Parse the input xml file
doc = minidom.parse(xmlfile)
dddbNode = doc.documentElement
if dddbNode.nodeName != "DDDB" :
    print "Wrong format: DDDB tag missing at the start of the file.\n"
    sys.exit()
    
# loop over the tell1s in the xml file
for tellNode in dddbNode.childNodes :
    if tellNode.nodeName != "condition" : continue

    # Get the tell1 name
    for node in tellNode.childNodes :
        if node.nodeName != "paramVector" and node.nodeName != "param" : continue
        attrs = node.attributes
        for attrName in attrs.keys() :
            if attrName == "name" and attrs.get(attrName).nodeValue == "Tell_name" :
                Tell_name = node.firstChild.nodeValue.strip() # get the text
    #print "Processing " + Tell_name

    for line in correctLines :
        if line[0] == Tell_name or line[0].strip() == "*" :
            #print "  Found ", Tell_name
            for node in tellNode.childNodes :
                if node.nodeName != "paramVector" and node.nodeName != "param" : continue
                attrs = node.attributes
                for attrName in attrs.keys() :
                    if attrName == "name" and attrs.get(attrName).nodeValue == line[1] :
                        node.firstChild.nodeValue = line[2]
                        #print node.toxml()
                        print "Modify " + line[1] + " in " + Tell_name

f = open(xmlfile + ".tmp","w")
f.write(doc.toxml())
f.write('\n')
f.close()

os.system( "mv " + xmlfile + ".tmp " + xmlfile ) 
os.system( "$STVETRAANALYSISROOT/perl/DBCosmetics.pl " + xmlfile )
