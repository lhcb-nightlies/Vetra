#!/usr/bin/env python

#
# Script that make the comparison between any number of databases (2 at
# least)
#
# Author : Johan Luisier <johan.luisier@epfl.ch>
#

import sys, os, subprocess

# First : process the numerous arguments

args = sys.argv[1:]

nbr = len(args)

if ( nbr == 0 ):
    print "Try CompareDBs -h\n"
    sys.exit(1)

dummy = ""

dddb = []
cond = []
stdb = []
tags = []
file = []
type = ''

path = ""

for arg in args:
    if ( arg.find('--d') != -1 or arg == '-d' ):
        path = 'dddb'
    elif ( arg.find('--c') != -1 or arg == '-c' ):
        path = 'cond'
    elif ( arg.find('--v') != -1 or arg == '-v' ):
        path = 'stvetra'
    elif ( arg.find('--t') != -1 or arg == '-t' ):
        path = 'tags'
    elif ( arg.find('--f') != -1 or arg == '-f' ):
        path = 'file'
    elif ( arg.find('--s') != -1 or arg == '-s' ):
        path = 'sub'
    elif ( arg.find('--h') != -1 or arg == '-h' ):
        print """ Usage CompareDBs options files tags detector
        options are :
        --dddb / -d    connection strings to DDDB files
        --cond / -c    connection strings to LHCBCOND files
        --vetra / -v   connection strings to ST Vetra CondDB files
        --tag / -t     tag name
        --files / -f   filenames, the first one will contain the trees read from the DBs,
                       the second one will contain the comparison(s).
        --sub / -s     subdetector type : either IT or TT.

        The order is not important!

        For instance, if you want to compare two versions of ST Vetra CondDB, tagged respectively
        HEAD and OLD, use
        DBComparison -s $DDDBPATH/COND1.db/CondDB $DDDBPATH/COND2.db/CondDB --tag OLD HEAD
        If you want to compare two version of the LHCb database, tagged HEAD and CUSTOM, use
        DBComparison -d $DDDBPATH/DDDB.db/DDDB $HOME/myDDDB.db/DDDB -c $DDDBPATH/LHCBCOND.db/LHCBCOND $HOME/myLHCBCOND.db/LHCBCOND -t HEAD CUSTOM
        """
        sys.exit(0)
    else:
        if ( path == 'dddb' ):
            dddb . append ( arg )
        elif ( path == 'cond' ):
            cond . append ( arg )
        elif ( path == 'stvetra' ):
            stdb . append ( arg )
        elif ( path == 'tags' ):
            tags . append ( arg )
        elif ( path == 'file' ):
            file . append ( arg )
        elif ( path == 'sub' ):
            type = arg
        else:
            print "Error, dont know what to do with argument", arg


# The most important number is the number of tags, it gives the number of trees.
nbr = len( tags )

dddbPresent = True
condPresent = True
stdbPresent = True

if ( len( dddb ) == 0 ):
    # If no DDDB is given, fine, we keep the default one
    dddbPresent = False
elif ( len( dddb ) == 1 ):
    # If only one is given, we copy the entry as many times
    # as necessary to have len( tags ) == len( dddb )
    for i in range( nbr - 1 ):
        dddb . append( dddb[0] )
elif ( len( dddb ) == nbr ):
    # Already have the same number, fine
    pass
else:
    # Problem here
    print "Error, lhcb databases number don't match the number of tags\n"
    sys.exit(1)

if ( len( cond ) == 0 ):
    # If no LHCBCOND is given, fine, we keep the default one
    condPresent = False
elif ( len( cond ) == 1 ):
    # If only one is given, we copy the entry as many times
    # as necessary to have len( tags ) == len( cond )
    for i in range( nbr - 1 ):
        cond . append( cond[0] )
elif ( len( cond ) == nbr ):
    # Already have the same number, fine
    pass
else:
    # Problem here
    print "Error, lhcb cond databases number don't match the number of tags\n"
    sys.exit(1)

if ( len( stdb ) == 0 ):
    # If no COND is given, fine, we keep the default one
    stdbPresent = False
elif ( len( stdb ) == 1 ):
    # If only one is given, we copy the entry as many times
    # as necessary to have len( tags ) == len( stdb )
    for i in range( nbr - 1 ):
        stdb . append( stdb[0] )
elif ( len( stdb ) == nbr ):
    # Already have the same number, fine
    pass
else:
    # Problem here
    print "Error, st vetra cond databases number don't match the number of tags\n"
    sys.exit(1)


# Arguments processed, print a small summary

print "DDDB:", dddb
print "LHCBCOND", cond
print "VetraCondDB", stdb
print "TAGS", tags
print "FILES", file
print "TYPE", type


if ( len(file) != 2):
    file = ['myTest.root', 'savedComparison.root']

PATH = os.environ.get("STVETRAANALYSISROOT")

for i in range( nbr ):
    # For now I'll copy from one working opts file to a temporary one
    optsSrcFile = open(PATH + '/options/DefaultDB.opts', 'r')
    optsTrgFile = open(PATH + '/python/tmp.opts', 'w')

    # Here the connection strings are replaced according to the arguments
    # that are given to the script
    for line in optsSrcFile:

        if ( line.find('DDDB.ConnectionString') != -1 ):
            if ( dddbPresent ):
                line = "DDDB.ConnectionString     = \"sqlite_file:" + dddb[i] + "\";"
                print line
                line += '\n'
                optsTrgFile.write(line)
            else:
                optsTrgFile.write(line)
        elif ( line.find('LHCBCOND.ConnectionString') != -1 ):
            if ( condPresent ):
                line = "LHCBCOND.ConnectionString = \"sqlite_file:" + cond[i] + "\";"
                print line
                line += '\n'
                optsTrgFile.write(line)
            else:
                optsTrgFile.write(line)
            
        elif ( line.find('COND.ConnectionString') != -1):
            if ( stdbPresent ):
                line =  "COND.ConnectionString     = \"sqlite_file:" + stdb[i] + "\";"
                print line
                line += '\n'
                optsTrgFile.write(line)
            else:
                # Little add-on, be sure we flipped the detType in the
                # COND connection string
                line = line.replace('/IT', '/' + type)
                line = line.replace('/TT', '/' + type)
                optsTrgFile.write(line)
        else:
            optsTrgFile.write(line)

    optsSrcFile.close()
    optsTrgFile.close()

    command = "python $STVETRAANALYSISROOT/python/ReadDB.py $STVETRAANALYSISROOT/python/tmp.opts " + tags[i] + " " + type + " " + file[0]

    print "--> executing :", command
    
    # Create a sub process that will create the tree for the given DB
    # Continue unless an error occurs
    process = subprocess.Popen(command, shell=True)#, stdout=subprocess.PIPE)
    process.wait()
    if ( process.returncode != 0 ): sys.exit(1)
    command = "rm -f $STVETRAANALYSISROOT/python/tmp.opts"
    os.system(command)

# Now all the DB were processed, we'll do the comparison
from ROOT import TH1D, TCanvas, TH3D, TTree, AddressOf, gROOT, TFile

read = TFile(file[0], 'READ')

from Utils import Data, BranchReadSetup, BranchWriteSetup, DoComparison

treeList = []
valsList = []
entrList = []

for i in range( nbr ):
    # Getting the Trees from the file
    treeList.append( read.Get( tags[i] ) )
    valsList.append( Data() )
    BranchReadSetup( treeList[i], valsList[i] )
    entrList.append( treeList[i].GetEntries() )
    print treeList[i].Print()

for i in range( len( entrList ) - 1 ):
    # Rapid check
    if ( entrList[i] != entrList[i + 1] ):
        print "Entries don't match\n"
        sys.exit(1)

saved = TFile(file[1], 'RECREATE')

histosDiff = []
fillValues = []
canvas     = []

counter = 0

matrix = []
tmp    = []

for i in range( nbr ):
    tmp.append( -1 )

for i in range( nbr ):
    matrix .extend( tmp )

print matrix

for first in range( nbr - 1 ):
    # Create one comparison Tree per pair of DB
    print "first", first
    for second in range( first + 1, nbr):
        print "second", second
        fillValues.append( Data() )
        histosDiff.append( TTree(tags[first] + 'Vs' + tags[second], "Differences between " + tags[first] + " and "+ tags[second]) )
        BranchWriteSetup( histosDiff[counter], fillValues[counter] )
        matrix[ nbr * first + second ] = counter
        print matrix
        counter += 1
        print "will compare", tags[first], "and", tags[second], first, second, '=', matrix[ nbr * first + second ]

print entrList[0], "entries"

for entry in range( entrList[0] ):
    for i in range( nbr ):
        treeList[i].GetEntry( entry )
    for first in range( nbr - 1 ):
        for second in range( first + 1, nbr):
            index = matrix[ nbr * first + second ]
            # DoComparison will put the differences into fillValues[]
            DoComparison(valsList[first], valsList[second], fillValues[index])
            # Comparison Tree is filled
            histosDiff[index].Fill()

saved.SetCompressionLevel( 9 )

saved.Write()
saved.Close()
