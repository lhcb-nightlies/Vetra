#! /usr/bin/env python

MaskFromDB = [ 4, 10 ]

ThresholdsAndValues = [ "cms_threshold"         ,  4., 3072,
                        "hit_threshold"         ,  3., 3072,
                        "confirmation_threshold",  5., 48,
                        "spill_over_threshold"  , 10., 48 ]
