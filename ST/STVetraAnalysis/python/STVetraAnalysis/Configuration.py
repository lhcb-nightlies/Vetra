# @package STVetraAnalysis
# High level configuration tools for STVetraAnalysis
# @author Johan Luisier
# @date 08/03/2010

from Gaudi.Configuration  import *
import GaudiKernel.ProcessJobOptions

from Configurables import ( # tool to read DBs
                            STVetraCfg,
                            # decode full data
                            STFullDecoding,
                            # emulator algorithms
                            STTELL1PedestalSubtractor, STTELL1LCMS,
                            STTELL1ClusterMaker,
                            STTELL1CheckNzs,
                            # ST Vetra analysis algorithms
                            STHeaderCorrection,
                            STNoiseMonitor, STFastNoiseMonitor,
                            STPedestalWriter,
                            STPedestalEstimator,
                            STCalibrationTuple,
                            STHeaderTuple,
                            # Vetra cond DB writer
                            STVetraCondDBWriter,
                            # writes noise condition
                            WriteSTNoiseConditions,
                            # Misc
                            LHCbApp, CondDB, LHCbConfigurableUser,
                            EventSelector
                            )

## class STVetraAnalysis
# Allows to configure easily the useful tasks. Depending on the value of
# the Process option, the sequence can :\n
# 1) compute new pedestal values and create a new TELL1Cond.xml file.\n
# 2) compute new header correction values and create a new TELL1Cond.xml
# file.\n
# 3) compute new noise values and create a new NoiseValuesInfo.xml file.\n
#
# @author Johan Luisier <johan.luisier@epfl.ch>
# @date 08/03/2010
class STVetraAnalysis( LHCbConfigurableUser ):
    
    ## Possible used Configurables
    __used_configurables__ = [ STVetraCfg, STFullDecoding,
                               STTELL1PedestalSubtractor, STTELL1LCMS,
                               STTELL1ClusterMaker, STTELL1CheckNzs, STHeaderCorrection,
                               STNoiseMonitor, STFastNoiseMonitor,
                               STPedestalEstimator, STPedestalWriter,
                               STCalibrationTuple,
                               STHeaderTuple, STVetraCondDBWriter,
                               WriteSTNoiseConditions, LHCbApp, CondDB,
                               LHCbConfigurableUser, EventSelector ]

    # Steering options
    __slots__ = {
        "EvtMax"	   : -1
	,"SkipEvents"	   : 0
        ,"NZSEvents"       : -1
	,"PrintFreq"	   : 1
        ,"OutputLevel"     : INFO
        ,"EmulatorOutLvl"  : INFO
        ,"VetraOutLvl"     : INFO
        ,"WriterOutLvl"    : INFO
	,"PrintErrors"	   : False 
	,"DatasetName"     : "STVetra"
	,"DDDBtag"	   : ""
	,"CondDBtag"	   : ""
        ,"CondDB"          : ""
        ,"VetraCondDB"     : ""
        ,"VetraCondDBPath" : "CondDB"
        ,"DetType"         : "IT"
        ,"Process"         : "Noise"
        ,"Convergence"     : 10000
        ,"SecondHeaderBit" : False
	,"MainSequence"    : None
        ,"CheckNZS"        : False
        ,"XMLAuthor"       : ""
        ,"XMLTag"          : ""
        ,"XMLDescription"  : ""
	#,"Monitors"	   : []
        }
    
    ## Help message for the options
    _propertyDocDct = {
        "EvtMax"	   : """ Maximum number of events to process (default -1, all events on input files) """
	,"SkipEvents"	   : """ Number of events to skip (default 0) """
        ,"NZSEvents"       : """ Required number of events with NZS banks"""
	,"PrintFreq"	   : """ The frequency at which to print event numbers (default 1, prints every event) """
        ,"OutputLevel"     : """ The printout level to use (default INFO) """
        ,"EmulatorOutLvl"  : """ The printout level to use for the emulator algorithms (default INFO) """
        ,"VetraOutLvl"     : """ The printout level to use for ST Vetra processing algorithms (default INFO) """
        ,"WriterOutLvl"    : """ The printout level to use for writing algorithms (default INFO) """
	,"PrintErrors"	   : """ Don't print the errors from STFullDecoding """
	,"DatasetName"     : """ String used to build output file names """
	,"DDDBtag"	   : """ Tag for DDDB """
	,"CondDBtag"	   : """ Tag for CondDB """
        ,"CondDB"	   : """ Path to LHCBCOND DB. """
        ,"VetraCondDB"     : """ Path to ST Vetra Cond DB. """
        ,"VetraCondDBPath" : """ Path name in ST Vetra Cond DB structure. (default 'CondDB') """
        ,"DetType"         : """ Detector type, can be one of [ 'IT', 'TT' ] (default 'IT') """
        ,"Process"         : """ Choosed action, can be one of self.KnownProcesses (default 'Noise') """
        ,"Convergence"     : """ Convergence limit, used to get pedestal convergence (default 10000) """
        ,"SecondHeaderBit" : """ Flag to enable the second header bit emulation (default 'False') """
	,"MainSequence"    : """ List of main sequences, default self.DefaultSequence """
        ,"CheckNZS"        : """ Check for events without NZS banks, and skip them """
        ,"XMLAuthor"       : """ Author of the xml file (to be filled!) """
        ,"XMLTag"          : """ 'Tag' of the xml file, which is not supposed to be a real tag (to be filled!) """
        ,"XMLDescription"  : """ Please put here something that helps to know what are the input 'conditions', like run nbr, ... (to be filled!) """
	#,"Monitors"	   : """ List of monitors to execute, see self.KnownMonitors. Default none """
        }

    ## List of known detector types
    KnowDetTypes   = [ "IT", "TT" ]
    ## List of known processes
    KnownProcesses = [ "Pedestal", "PedestalUpdate", "HeaderCorr", "Noise", "Monitor", "Tuple", "HeaderTuple" ]

    ## Sets the correct databases
    # Uses the LHCbApp configurable to set the LHCBCOND and DDDB tags, and adds
    # a layer for COND
    def defineDatabases(self):
        # Delegate handling to LHCbApp configurable
        self.setOtherProps(LHCbApp(),["CondDBtag","DDDBtag"])
        path = self.getProp( "CondDB" )
        pathVetra = self.getProp( "VetraCondDB" )

        conddb = CondDB()
        if(path != ""):
            conddb.addLayer( connStr = "sqlite_file:" + path + "/LHCBCOND" )
        conddb.addLayer( connStr = "sqlite_file:" + pathVetra + "/COND" )

    ## Sets the event related options
    def defineEvents(self):
        # Delegate handling to LHCbApp configurable
        self.setOtherProps(LHCbApp(),["EvtMax","SkipEvents"])
        EventSelector().PrintFreq = self.getProp("PrintFreq")

    ## Checks that the detector type and process are OK
    # Can raise errors if the Process or DetType are not known.
    def defineOptions(self):
        detType = self.getProp( "DetType" )
        if detType not in self.KnowDetTypes:
            raise TypeError( "Invalid DetType '%s'"%detType )

        process = self.getProp( "Process" )
        if process not in self.KnownProcesses:
            raise TypeError( "Invalid Process '%s'"%process )

    ## Builds the GaudiSequence, according to the process any detector type.
    def defineCommon(self):
        #importOptions( '$STDOPTS/LHCbApplication.opts' )
        importOptions( '$STDOPTS/RawDataIO.opts' )

        # Shortcut
        type = self.getProp( "DetType" )
        
        ApplicationMgr().HistogramPersistency = 'ROOT'

        # Name of the output root file
        tmp = self.getProp( "DatasetName" )
        tmp += '-' + type + '-' + self.getProp( "Process" )
        HistogramPersistencySvc().OutputFile = tmp + ".root"

        mainSeq = GaudiSequencer( 'MainSeq' )
        mainSeq.MeasureTime = True
        ApplicationMgr().TopAlg.append( mainSeq )

        vetraconddbpath = self.getProp( "VetraCondDBPath" )

        myTool = STVetraCfg( "ToolSvc." + type + "VetraCfgTool" )
        myTool.DetType = type
        myTool.CondPath = vetraconddbpath

        # Check NZS banks (skip events with no nzs banks)
        if ( self.getProp( "CheckNZS" ) ):
            nzsCheck = STTELL1CheckNzs( "STTELL1CheckNzs",
                                        DetType = type,
                                        OutputLevel = self.getProp( "VetraOutLvl" ),
                                        NumberNZS = self.getProp( "NZSEvents" ) )
        mainSeq.Members += [ nzsCheck ]

        # Decoding
        decoding = STFullDecoding( "STFullDecoding" )
        decoding.DetType        = type
        decoding.PrintErrorInfo = self.getProp( "PrintErrors" )
        
        mainSeq.Members += [ decoding ]

        # Emulator
        emulator = GaudiSequencer( "EmulatorSTSeq" )
        
        pedSubtractor = STTELL1PedestalSubtractor( "STTELL1PedestalSubtractor",
                                                   DetType = type,
                                                   CondPath = vetraconddbpath,
                                                   OutputLevel = self.getProp( "EmulatorOutLvl" ) )

        cmSubtractor = STTELL1LCMS( "STTELL1LCMS",
                                    DetType = type,
                                    CondPath = vetraconddbpath,
                                    ConvergenceLimit = self.getProp( "Convergence" ),
                                    OutputLevel = self.getProp( "EmulatorOutLvl" ) )

        if ( self.getProp( "Process" ) == "HeaderCorr" ):
            emulator.Members = [ pedSubtractor ]

        if ( self.getProp( "Process" ) == "Noise" or self.getProp( "Process" ) == "Monitor" ):
            emulator.Members = [ pedSubtractor, cmSubtractor ]

        mainSeq.Members += [ emulator ]

        if ( self.getProp( "Process" ) == "Pedestal" or
             self.getProp( "Process" ) == "PedestalUpdate" ):
            if( self.getProp( "Process" ) == "Pedestal" ):
                pedLeecher = STPedestalEstimator( "PedestalEstimator",
                                                  DetType = type,
                                                  InputLocation = "Raw/" + type + "/Full",
                                                  OutputLevel = self.getProp( "VetraOutLvl" ) )
                mainSeq.Members += [ pedLeecher ]
            else:
                pedLeecher = STPedestalWriter( "PedestalReader",
                                               DetType = type,
                                               InputLocation = "Raw/" + type + "/SubPeds",
                                               WaitingTill = self.getProp( "Convergence" ),
                                               OutputLevel = self.getProp( "VetraOutLvl" ) )
                mainSeq.Members += [ pedLeecher ]
                
            tmp =  self.getProp( "DatasetName" )
            tmp += "-" + type + "-Pedestal-TELL1Cond.xml"

            pedWriter = STVetraCondDBWriter( name = "PedestalWriter",
                                             DetType = type,
                                             OutputFileName = tmp,
                                             Author = self.getProp( "XMLAuthor" ),
                                             Tag = self.getProp( "XMLTag" ),
                                             Description = self.getProp( "XMLDescription" ) )
            
            mainSeq.Members += [ pedWriter ]

        elif ( self.getProp( "Process" ) == "HeaderCorr" ):
            header = STHeaderCorrection( name = "HeaderCorrection",
                                         InputLocation = "Raw/" + type + "/PedSubADCs",
                                         DetType = type,
                                         PedestalFromCONDDB = False,
                                         ProduceHistos = True,
                                         SecondHeaderBit = self.getProp( "SecondHeaderBit" ),
                                         OutputLevel = self.getProp( "VetraOutLvl" ) )

            tmp =  self.getProp( "DatasetName" )
            tmp += "-" + type + "-HeaderCorr-TELL1Cond.xml"

            writer = STVetraCondDBWriter( name = "DBWriter",
                                          DetType = type,
                                          OutputFileName = tmp,
                                          OutputLevel = self.getProp( "WriterOutLvl" ),
                                          Author = self.getProp( "XMLAuthor" ),
                                          Tag = self.getProp( "XMLTag" ),
                                          Description = self.getProp( "XMLDescription" ) )

            mainSeq.Members += [ header, writer ]
        elif ( self.getProp( "Process" ) == "Noise" or self.getProp( "Process" ) == "Monitor" ):
            noiseLeecher = STFastNoiseMonitor( name = "NoiseReader",
                                               DetType = type,
                                               WaitingTill = 0,
                                               ElectronsPerADC = False,
                                               OutputLevel = self.getProp( "VetraOutLvl" ) )
            mainSeq.Members += [ noiseLeecher ]

            if( self.getProp( "Process" ) == "Noise" ):
                tmp =  self.getProp( "DatasetName" )
                tmp += '-' + type + "-NoiseValuesInfo.xml"
                
                noiseWriter = WriteSTNoiseConditions( name = "NoiseWriter",
                                                      DetType = type,
                                                      outputFile = tmp,
                                                      author = self.getProp( "XMLAuthor" ),
                                                      tag =  self.getProp( "XMLTag" ),
                                                      description =  self.getProp( "XMLDescription" ),
                                                      OutputLevel = self.getProp( "WriterOutLvl" ) )

                mainSeq.Members += [ noiseWriter ]

        elif ( self.getProp( "Process" ) == "Tuple" ):
            tmp = self.getProp( "DatasetName" )
            tmp += '-' + type + '-' + self.getProp( "Process" )
            tmp += '.root'
            NTupleSvc().Output=["FILE1 DATAFILE='"+tmp+"' TYP='ROOT' OPT='NEW'"]

            monitor = STCalibrationTuple( name = "CalibrationTuple",
                                          DetType = type,
                                          OutputLevel = self.getProp( "WriterOutLvl" ) )

            mainSeq.Members = [ monitor ]

        elif ( self.getProp( "Process" ) == "HeaderTuple" ):
            tmp = self.getProp( "DatasetName" )
            tmp += '-' + type + '-' + self.getProp( "Process" )
            tmp += '.root'
            NTupleSvc().Output=["FILE1 DATAFILE='"+tmp+"' TYP='ROOT' OPT='NEW'"]

            headermonitor = STHeaderTuple( name = "HeaderTuple",
                                           DetType = type,
                                           OutputLevel = self.getProp( "WriterOutLvl" ) )

            mainSeq.Members += [ headermonitor ]     

    ## Sets the monitoring tasks
    def defineMonitors(self):
        from Configurables import (ApplicationMgr,AuditorSvc,SequencerTimerTool)
        ApplicationMgr().ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
        ApplicationMgr().AuditAlgorithms = True
        AuditorSvc().Auditors += [ 'TimingAuditor' ]
        SequencerTimerTool().OutputLevel = 4

    ## Apply the configuration
    def __apply_configuration__(self):
        
        GaudiKernel.ProcessJobOptions.PrintOff()

        self.defineDatabases()
        self.defineEvents()
        self.defineOptions()
        self.defineCommon()
        self.defineMonitors()
        GaudiKernel.ProcessJobOptions.PrintOn()

        log.info( self )
        GaudiKernel.ProcessJobOptions.PrintOff()
