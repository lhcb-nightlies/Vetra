/******************************************************************************
 *
 * This set of functions is meant to be used on trees that come out of
 * CompareDBs.oy script.
 *
 *
 *****************************************************************************/

#include <iostream>
#include <sstream>

using namespace std;

bool treeLoaded = false;

TTree *tree = 0;
TFile *file = 0;

TCanvas *canvas = new TCanvas("canvas", "Nice plot", 600, 400);

/**
 * This function loads the file and the TTree you want. If something is already
 * loaded, then the file is first closed. The treeLoaded is modified by the
 * function, allowing to know whether any TTree is loaded or not.
 *
 * @param filename name of the file you want to read.
 * @param treename name of the TTree you want to look at.
 */
void LoadTree(const string& filename, const string& treename)
{
  if ( file != 0 )
  {
    cout << "Closing previous file" << endl;
    tree = 0;
    file -> Close();
    file = 0;
  }
  

  file = TFile::Open( filename.c_str() );

  if ( file == 0 )
  {
    treeLoaded = false;
    return;
  }
  
  tree = static_cast<TTree>(file -> Get ( treename.c_str() ));

  if ( tree == 0 )
    treeLoaded = false;
  else
    treeLoaded = true;
}

/**
 * Allows you to plot any leaf or leaves combination you want. Here you have to
 * specify what to plot, for instance Ped, or Noise:channel, ...
 *
 * You can also give some cuts, for instance tell1 == 34 && Ped > 20.
 *
 * If no cut is wanted, the usual Draw function is called on the TTree,
 * otherwise a cut is constructed from the string you entered.
 *
 * If you didn't load any TTree yet, you'll be asked to enter the file and
 * TTree names.
 */
int Analyse()
{
  if ( ! treeLoaded )
  {
    string fName, tName;
    cout << "File name : ";
    cin >> fName;
    cout << "Name of the tree : ";
    cin >> tName;

    LoadTree( fName, tName );
    
    if ( treeLoaded )
    {
      cout << "Tree is loaded" << endl;
    }
    else
    {
      cerr << "Cannot load the TTree, abort" << endl;
      return -1;
    }
  }

  string strWhat, strCut, waste;

  cout << "What do you want to plot ? ";
  cin >> strWhat;

  getline( cin , waste );
  
  cout << "What cuts do you want to apply ? ";
  getline( cin ,strCut );

  cout << strWhat << ' ' << strCut << endl;

  Long64_t n(0);

  if ( strCut != "" )
    n = tree -> Draw( strWhat.c_str(), strCut.c_str() );
  else
    n = tree -> Draw( strWhat.c_str() );

  if ( n > 0 )
    return 0;
  else
    return 1;
}

/**
 * Allows you to count (and see) what are the elements that you want to plot. A
 * useful usage of this function is counting the number of channels in a tell1
 * that have a noise larger than X ADC counts. This can be done easily by
 * plotting "channel", with cut "tell1 == 38 && Noise > 2". The total number of
 * channels will be printed on screen as well as the channel numbers.
 *
 * Apart from that the function works the same as Analyse().
 */
int Count()
{
    if ( ! treeLoaded )
  {
    string fName, tName;
    cout << "File name : ";
    cin >> fName;
    cout << "Name of the tree : ";
    cin >> tName;

    LoadTree( fName, tName );
    
    if ( treeLoaded )
    {
      cout << "Tree is loaded" << endl;
    }
    else
    {
      cerr << "Cannot load the TTree, abort" << endl;
      return -1;
    }
  }

  string strWhat, strCut, waste;

  cout << "What do you want to plot ? ";
  cin >> strWhat;

  TH1D histo("histo", "histo", 3072, -.5, 3071.5);

  strWhat += ">>histo";

  getline( cin , waste );
  
  cout << "What cuts do you want to apply ? ";
  getline( cin ,strCut );

  Long64_t n(0);

  if ( strCut != "" )
    n = tree -> Draw( strWhat.c_str(), strCut.c_str() );
  else
    n = tree -> Draw( strWhat.c_str() );

  Int_t min( 0. ), max( histo . GetNbinsX() );

  cout << "Min : " << min << ", max : " << max << ", entries : "
       << histo.GetEntries() << endl;

  Double_t current, last;

  stringstream ss;

  int counter(0);

  for (Int_t i(min); i < max; i++)
  {
    
    if ( histo.GetBinContent( i ) > 0. )
      current = histo.GetBinCenter( i );
    else
      continue;

    counter++;

    if ( i == min )
      ss << current << ' ';
    else
      {
        if ( current == last + 1. )
          ss << current << ' ';
        else
        {
          cout << ss.str() << endl;
          ss.str("");
          ss << current << ' ';
        }
      }

    last = current;
  }

  cout << ss.str() << endl;

  cout << "Nbr : " << counter << endl;

  if ( n > 0 )
    return 0;
  else
    return 1;
}
