#include "scanFun.C"

void plotScan_perSB(TString histofile = "scan_vfs400.root"){

  gStyle->SetStatW(0.45);
  gStyle->SetStatH(0.25);
  gStyle->SetOptTitle(1);
  gStyle->SetOptStat(0);
  gStyle->SetTitleSize(0.05,"xyz");
  gStyle->SetLabelSize(0.05,"xyz");
  gStyle->SetTitleOffset(1.0,"y");
  gStyle->SetTitleW(0.3);
  gStyle->SetTitleH(0.1);

  const int n = 6;

  char buffer[100];
  TFile* f = TFile::Open(histofile);
  if (!f){
    printf(" could not open input file %s\n",buffer);
    continue;
  }

  std::string quadrant[4] = { "AB", "AT", "CB", "CT" };

  TH1D* plus[24];
  TH1D* minus[24];

  int i = 0;
  for (int iQuadrant = 0; iQuadrant < 4; ++iQuadrant) {    
    
    sprintf(buffer,"Pulse shape scan %s", (quadrant[iQuadrant]).c_str() );

    TCanvas *c = new TCanvas(buffer,buffer,1200,750);
    c->Divide(3,2);
    TLegend *leg = new TLegend(0.7,0.8,0.99,0.99);

    for (int sb = 1; sb <= n; ++sb) {
      c->cd(sb);

      sprintf(buffer,"TT/STTimeScan/%s%d_plus", 
              (quadrant[iQuadrant]).c_str(), sb );
      TProfile* plusProf = (TProfile*)(f->Get(buffer));
      plus[i] = plusProf->ProjectionX();
      sprintf(buffer,"%s%d", (quadrant[iQuadrant]).c_str(), sb );
      plus[i]->SetTitle(buffer);

      sprintf(buffer,"TT/STTimeScan/%s%d_minus", 
              (quadrant[iQuadrant]).c_str(), sb );  
      TProfile* minusProf = (TProfile*)(f->Get(buffer));
      minus[i] = minusProf->ProjectionX();

      // Average the positive and negative pulse (removes pedestal) and smooth
      plus[i]->Add(minus[i], -1.0);
      plus[i]->Scale(0.5);
      minus[i] = (TH1D*) plus[i]->Clone(buffer);
      minus[i]->Scale(-1.0);
      smoothHistoTwoSides(plus[i], minus[i] );
      reverseXaxis(plus[i]);
      reverseXaxis(minus[i]);

      plus[i]->SetLineColor(4);
      plus[i]->SetMarkerColor(4);
      plus[i]->SetMaximum(35.);
      plus[i]->SetMinimum(-35.);
      plus[i]->GetXaxis()->SetTitle("step");
      plus[i]->GetYaxis()->SetTitle("ADC counts");
      plus[i]->Draw("hist");
      if ( sb == 1 ) {
        leg->AddEntry(plus[i],"Positive pulse");
        leg->Draw();
      }

      minus[i]->SetLineColor(2);
      minus[i]->SetMarkerColor(2);
      minus[i]->Draw("same hist");
      if ( sb == 1 ) {
        leg->AddEntry(minus[i],"Negative pulse");
        leg->Draw();
      }

      ++i;
    }
  }
}
