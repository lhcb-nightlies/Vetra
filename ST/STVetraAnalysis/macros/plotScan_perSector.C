#include "scanFun.C"

void plotScan_perSector(TString histofile = "scan_vfs400.root"){

  gStyle->SetStatW(0.45);
  gStyle->SetStatH(0.25);
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  gStyle->SetTitleSize(0.05,"xyz");
  gStyle->SetLabelSize(0.05,"xyz");
  gStyle->SetTitleOffset(1.0,"y");
  gStyle->SetTitleW(0.3);
  gStyle->SetTitleH(0.1);

  const int n = 6;

  char buffer[100];
  TFile* f = TFile::Open(histofile);
  if (!f){
    printf(" could not open input file %s\n",buffer);
    continue;
  }

  int sourceID[49]={ 999, 4, 5, 6, 36, 37, 38, 68, 69, 70, 71, 72, 100, 101, 
                     102, 103, 104, 0, 1, 2, 3, 32, 33, 34, 35, 64, 65, 66, 67,
                     96, 97, 98, 99, 7, 8, 9, 10, 39, 40, 41, 42, 73, 74, 75, 
                     76, 105, 106, 107, 108 };

  int i = 0;
  TH1D* plus[300];
  TH1D* minus[300];
  TCanvas *c[50];
  for (int iTell = 1; iTell < 49; ++iTell) {    

    sprintf(buffer,"Pulse shape scan Tell %d", iTell );

    c[iTell] = new TCanvas(buffer,buffer,1000,650);
    TLegend *leg = new TLegend(0.75, 0.6, 0.92, 0.92);

    for (int mod = 0; mod < n; ++mod) {
      sprintf(buffer,"TT/STTimeScan/tell%dm%d_plus", sourceID[iTell], mod );
      TProfile* plusProf = (TProfile*)(f->Get(buffer));
      if ( !plusProf ) continue;
      plus[i] = plusProf->ProjectionX();
      sprintf(buffer,"t%dm%d", iTell, mod );
      plus[i]->SetTitle(buffer);

      sprintf(buffer,"TT/STTimeScan/tell%dm%d_minus", sourceID[iTell], mod );
      TProfile* minusProf = (TProfile*)(f->Get(buffer));
      minus[i] = minusProf->ProjectionX();

      // Smooth and average the positive and negative pulse (removes pedestal)
      plus[i]->Add(minus[i], -1.0);
      plus[i]->Scale(0.5);
      minus[i] = (TH1D*) plus[i]->Clone(buffer);
      minus[i]->Scale(-1.0);
      smoothHistoTwoSides(plus[i], minus[i]);
      reverseXaxis(plus[i]);
      reverseXaxis(minus[i]);

      // Plot the test pulse
      plus[i]->SetMaximum(35.);
      plus[i]->SetMinimum(-5.);
      plus[i]->GetXaxis()->SetTitle("time (ns)");
      plus[i]->GetYaxis()->SetTitle("ADC counts");
      plus[i]->SetLineColor(mod+1);
      plus[i]->SetMarkerColor(mod+1);
      sprintf(buffer,"sector %d", mod ); 
      leg->AddEntry(plus[i],buffer);
      leg->Draw();
      if ( mod == 0 ) {
        plus[i]->DrawCopy("hist");
      } else {
        plus[i]->DrawCopy("hist same");
      }

      std::cout << "Tell " << iTell << " " << buffer << ": ";
      fitHisto(plus[i], mod+1 );    

      ++i;
    }
    std::cout << std::endl;
  }
}
