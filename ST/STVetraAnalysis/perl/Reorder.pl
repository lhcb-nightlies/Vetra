#!/usr/bin/env perl

###############################################################################
#
# This script is meant to transform the Condition LHCb messy output (the order
# of param and paramVector is alphabetical) into the "usual" STVetra xml.
#
# Of course it would be much nicer that the LHCb::Condition::toXml function
# would be able to write the file properly.
#
# @author Johan Luisier <johan.luisier@epfl.ch>
# @data 2011-02-14
#
###############################################################################



###############################################################################
#
# One argument is mandatory : the name of the input (and messy) file. If not
# given, the script will exit.
#
###############################################################################
if ( $#ARGV == -1 )
{
    print "Usage : Reorder.pl MessyLHCbFile.xml\n";
    exit;
}

###############################################################################
#
# Input and output files are opened, if they cannot, the script fails and
# quits.
#
###############################################################################
open( INPUT, "$ARGV[0]" ) or die "Cannot open the messy xml file named '$ARGV[0].'";
open( OUTPUT, ">$ARGV[0].new" ) or die "Cannot open '$ARGV[0].new' for writing.";

###############################################################################
#
# The whole idea of the script is to use a hash, that links the param(Vector)
# name to a line number. In order to allow easier maintaining of the code,
# the aforementioned hash is not hardcoded, but is built from a hardcoded list.
#
# Once the has is built, the input (and messy) file is read line by line, and
# for each condition an array is filled. This array has one entry per condition
# param(Vector), its order is given by the hash. This mechanism allows to put
# any line in its usual position.
#
# The two following list MUST then be kept up-to-date, in order to match the
# tag names in the COND database.
#
###############################################################################

##
# This first list contains the comment lines, the comments
# are not read from the input file.
#
my @commentlist = ( "<!-- Misc information -->",
		    "<!-- Gigabit IP and Ethernet protocol parameters -->",
		    "<!-- General parameters defining TTC and processing operation -->",
		    "<!-- ST specific part -->",
		    "<!-- Data RAM generator -->", "<!-- PP 0 -->",
		    "<!-- PP 1 -->", "<!-- PP 2 -->", "<!-- PP 3 -->" );

##
# This list contains every child node of a condition node. It is the reference,
# from which the hash will be built.
#
# WARNING : it is crucial that all the param(Vector) names appear in this list,
# and that their order is correct!!
#
my @taglist = ( "<!-- Misc information -->",
		"Tell_name", "Tell_SN", "Tell_UserPP_Ver", "Tell_CommPP_Ver",
		"Tell_CommSL_Ver", "Tell_PPcode_Gen_Date",
		"Tell_SLcode_Gen_Date", 
		"<!-- Gigabit IP and Ethernet protocol parameters -->",
		"ip_dest_addr", "ip_source_addr", "mac_dest_addr",
		"mac_source_addr", "ethernet_type", "ip_version", "IHL",
		"service_type", "TTL", "next_level_protocol",
		"gbe_forced_stop_cycle",
		"<!-- General parameters defining TTC and processing operation -->",
		"DETECTOR_ID", "pedestal_bank_schedule", "TTC_info_enable",
		"External_trigger_input_enable", "TTC_trigger_type_available",
		"TTC_dest_ip_available", "detector_data_generator_enabled",
		"SEP_generator_enabled", "pp_derandmizer_event_threshold",
		"mep_pid", "source_id", "version", "zs_class", "non_zs_class",
		"pedestal_class", "error_class", "FE_data_check_enable",
		"serious_err_throttle_enable", "nzs_bank_period_factor",
		"nzs_bank_offset", "trigger_n", "consecutive_n", "wait_cycle",
		"TRIGGER_TYPE", "error_bank_disable", "force_info_disable",
		"force_info_enable", "nzs_for_error_enable",
		"detector_specific_bank_header_enable", "MEP_FACTOR",
		"MTU_SIZE", "HLT_PORT", "HLT_DP_EN", "MEP_USE_THR",
		"Thottle_PPx_en", "Thottle_MEP_buffer_en",
		"trigger_info_throttle_en", "throttle_en", "Firmware", "TTCRx",
		"<!-- ST specific part -->",
		"Pseudo_header_lo_thr", "Pseudo_header_hi_thr",
		"zerosuppress_enable", "header_correction_enable",
		"header_corr_threshold_lo", "header_corr_threshold_hi",
		"header_corr_value_00", "header_corr_value_01",
		"header_corr_value_02", "header_corr_value_03",
		"header_corr_value_04", "header_corr_value_05",
		"header_corr_value_06", "header_corr_value_07",
		"header_corr_value_08", "header_corr_value_09",
		"header_corr_value_10", "header_corr_value_11",
		"header_corr_value_12", "header_corr_value_13",
		"header_corr_value_14", "header_corr_value_15",
		"header_corr_value_16", "header_corr_value_17",
		"header_corr_value_18", "header_corr_value_19",
		"header_corr_value_20", "header_corr_value_21",
		"header_corr_value_22", "header_corr_value_23",
		"header_corr_value_24", "header_corr_value_25",
		"header_corr_value_26", "header_corr_value_27",
		"header_corr_value_28", "header_corr_value_29",
		"header_corr_value_30", "header_corr_value_31",
		"header_corr_value_32", "header_corr_value_33",
		"header_corr_value_34", "header_corr_value_35",
		"header_corr_value_36", "header_corr_value_37",
		"header_corr_value_38", "header_corr_value_39",
		"header_corr_value_40", "header_corr_value_41",
		"header_corr_value_42", "header_corr_value_43",
		"header_corr_value_44", "header_corr_value_45",
		"header_corr_value_46", "header_corr_value_47",
		"header_corr_value_48", "header_corr_value_49",
		"header_corr_value_50", "header_corr_value_51",
		"header_corr_value_52", "header_corr_value_53",
		"header_corr_value_54", "header_corr_value_55",
		"header_corr_value_56", "header_corr_value_57",
		"header_corr_value_58", "header_corr_value_59",
		"header_corr_value_60", "header_corr_value_61",
		"header_corr_value_62", "header_corr_value_63",
		"header_corr_value_64", "header_corr_value_65",
		"header_corr_value_66", "header_corr_value_67",
		"header_corr_value_68", "header_corr_value_69",
		"header_corr_value_70", "header_corr_value_71",
		"header_corr_value_72", "header_corr_value_73",
		"header_corr_value_74", "header_corr_value_75",
		"header_corr_value_76", "header_corr_value_77",
		"header_corr_value_78", "header_corr_value_79",
		"header_corr_value_80", "header_corr_value_81",
		"header_corr_value_82", "header_corr_value_83",
		"header_corr_value_84", "header_corr_value_85",
		"header_corr_value_86", "header_corr_value_87",
		"header_corr_value_88", "header_corr_value_89",
		"header_corr_value_90", "header_corr_value_91",
		"header_corr_value_92", "header_corr_value_93",
		"header_corr_value_94", "header_corr_value_95",
		"pedestal_enable", "pedestal_auto_update_enable",
		"lcms_enable", "cluster_number_max",
		"orx_beetle_disable_0", "orx_beetle_disable_1",
		"orx_lck_ref_0", "orx_lck_ref_1", "orx_tlk_en",
		"arx_beetle_disable_0", "arx_beetle_disable_1",
		"pcn_select", "pedestal", "pedestal_mask", "hit_threshold",
		"cms_threshold", "spill_over_threshold",
		"confirmation_threshold", "start_strip_n_0", "start_strip_n_1",
		"<!-- Data RAM generator -->", "<!-- PP 0 -->",
		"st_data_gen_array_0_0_0", "st_data_gen_array_0_0_1",
		"st_data_gen_array_0_0_2", "st_data_gen_array_0_0_3",
		"st_data_gen_array_0_1_0", "st_data_gen_array_0_1_1",
		"st_data_gen_array_0_1_2", "st_data_gen_array_0_1_3",
		"st_data_gen_array_0_2_0", "st_data_gen_array_0_2_1",
		"st_data_gen_array_0_2_2", "st_data_gen_array_0_2_3",
		"st_data_gen_array_0_3_0", "st_data_gen_array_0_3_1",
		"st_data_gen_array_0_3_2", "st_data_gen_array_0_3_3",
		"st_data_gen_array_0_4_0", "st_data_gen_array_0_4_1",
		"st_data_gen_array_0_4_2", "st_data_gen_array_0_4_3",
		"st_data_gen_array_0_5_0", "st_data_gen_array_0_5_1",
		"st_data_gen_array_0_5_2", "st_data_gen_array_0_5_3",
		"<!-- PP 1 -->",
		"st_data_gen_array_1_0_0", "st_data_gen_array_1_0_1",
		"st_data_gen_array_1_0_2", "st_data_gen_array_1_0_3",
		"st_data_gen_array_1_1_0", "st_data_gen_array_1_1_1",
		"st_data_gen_array_1_1_2", "st_data_gen_array_1_1_3",
		"st_data_gen_array_1_2_0", "st_data_gen_array_1_2_1",
		"st_data_gen_array_1_2_2", "st_data_gen_array_1_2_3",
		"st_data_gen_array_1_3_0", "st_data_gen_array_1_3_1",
		"st_data_gen_array_1_3_2", "st_data_gen_array_1_3_3",
		"st_data_gen_array_1_4_0", "st_data_gen_array_1_4_1",
		"st_data_gen_array_1_4_2", "st_data_gen_array_1_4_3",
		"st_data_gen_array_1_5_0", "st_data_gen_array_1_5_1",
		"st_data_gen_array_1_5_2", "st_data_gen_array_1_5_3",
		"<!-- PP 2 -->",
		"st_data_gen_array_2_0_0", "st_data_gen_array_2_0_1",
		"st_data_gen_array_2_0_2", "st_data_gen_array_2_0_3",
		"st_data_gen_array_2_1_0", "st_data_gen_array_2_1_1",
		"st_data_gen_array_2_1_2", "st_data_gen_array_2_1_3",
		"st_data_gen_array_2_2_0", "st_data_gen_array_2_2_1",
		"st_data_gen_array_2_2_2", "st_data_gen_array_2_2_3",
		"st_data_gen_array_2_3_0", "st_data_gen_array_2_3_1",
		"st_data_gen_array_2_3_2", "st_data_gen_array_2_3_3",
		"st_data_gen_array_2_4_0", "st_data_gen_array_2_4_1",
		"st_data_gen_array_2_4_2", "st_data_gen_array_2_4_3",
		"st_data_gen_array_2_5_0", "st_data_gen_array_2_5_1",
		"st_data_gen_array_2_5_2", "st_data_gen_array_2_5_3",
		"<!-- PP 3 -->",
		"st_data_gen_array_3_0_0", "st_data_gen_array_3_0_1",
		"st_data_gen_array_3_0_2", "st_data_gen_array_3_0_3",
		"st_data_gen_array_3_1_0", "st_data_gen_array_3_1_1",
		"st_data_gen_array_3_1_2", "st_data_gen_array_3_1_3",
		"st_data_gen_array_3_2_0", "st_data_gen_array_3_2_1",
		"st_data_gen_array_3_2_2", "st_data_gen_array_3_2_3",
		"st_data_gen_array_3_3_0", "st_data_gen_array_3_3_1",
		"st_data_gen_array_3_3_2", "st_data_gen_array_3_3_3",
		"st_data_gen_array_3_4_0", "st_data_gen_array_3_4_1",
		"st_data_gen_array_3_4_2", "st_data_gen_array_3_4_3",
		"st_data_gen_array_3_5_0", "st_data_gen_array_3_5_1",
		"st_data_gen_array_3_5_2", "st_data_gen_array_3_5_3" );

my $size = $#taglist;
my $i;

##
# The tagpos hash stores the position of each param(Vector) name and each
# comment (<!-- -->).
#
my %tagpos = ();

###############################################################################
#
# As mentioned before, the tagpos hash is built from the taglist array. The
# index of each param(Vector) name is associated to the name.
#
###############################################################################
foreach $i ( 0 ... $size )
{
    $tagpos{ $taglist[ $i ] } = $i;
}

print "Hash of position has been created\n";

###############################################################################
#
# The condition array is declared here, and its first initialisation is
# performed. The correct number of fields is created, and the comments are
# written.
#
###############################################################################
my @condition = ( 0 ... $size );
my $name;

foreach $name ( @commentlist )
{
    $condition[ $tagpos{ $name } ] = "  " . $name . "\n";
}

##
# Variable that will be 1 if we are inside a condition.
#
my $pseudoBoolean = 0;

###############################################################################
#
# Let's loop over the input file.
#
###############################################################################
while ( <INPUT> )
{
    if ( $_ =~ /<\?/ )
	#######################################################################
	#
	# XML header, is written out.
        #
	#######################################################################
    {
	print OUTPUT $_;
    }
    elsif ( $_ =~ /DOCTYPE/ )
	#######################################################################
	#
	# XML header, is written out.
	#
	#######################################################################
    {
	print OUTPUT $_;
    }
    elsif ( $_ =~ /DDDB/ )
	#######################################################################
	#
	# <DDDB> and </DDDB> tags, copied to outputfile.
	#
	#######################################################################
    {
	print OUTPUT $_;
    }
    elsif ( $_ =~ /<condition/ )
	#######################################################################
	#
	# If an opening <condition> tag is found, it is copied, and
	# pseudoBoolean is set to 1, meaning that the following lines will be
	# considered as param(Vector) or comments.
        #
	#######################################################################
    {
	print OUTPUT $_;
	$pseudoBoolean = 1;
	$_ =~ /name="(.+?)"/;
	print "Reading $1.\n";
    }
    elsif ( $_ =~ /<\/condition/ )
	#######################################################################
	#
	# If a closing </condition> tag is found, the condition array is
	# written to the outputfile, then the closing tag is copied and
	# pseudoBoolean is set to 0. The condition array is re-initialised.
	#
	#######################################################################
    {
	# print every param(Vector) in output file.
	foreach $name ( @condition )
	{
	    print OUTPUT "$name";
	}
	print OUTPUT $_;
	$pseudoBoolean = 0;

	print "Condition written.\n";
	
	# Reinitialise the condition array.
	@condition = ( 0 ... $size );
		
	foreach $name ( @commentlist )
	{
	    $condition[ $tagpos{ $name } ] = "  " . $name . "\n";
	}
    }
    elsif ( $pseudoBoolean == 1 )
	#######################################################################
	#
	# If pseudoBolean is 1, it means that we're reading
	# the various children of a condition. They can be
	# - param
	# - paramVector
	# - comment that indicates the structure of the file.
	# For each of these possibilities, the corresponding field of the
	# condition array is overwritten.
	#
	# An error message is printed if the line doesn't contain any
	# param(Vector) or comment.
	#
	#######################################################################
    {
	if ( $_ =~ /<param name="(.+?)"/ )
	{
	    $name = $1;
	}
	elsif ( $_ =~ /<paramVector name="(.+?)"/ )
	{
	    $name = $1;
	}
	elsif ( $_ =~ /<!/ )
	{
	    next;
	}
	else
	{
	    print "!!! ERROR !!! a param was expected in line $_\n";
	    next;
	}
	$condition[ $tagpos{ $name } ] = $_;
    }
    else
	#######################################################################
	#
	# If none of the above statement is true, which should be unlikely,
	# the input line is just copied. This can only happen for a file
	# written by the TELL1 itself, since some time comments are also
	# written at the beginning of each condition.
	#
	#######################################################################
    {
	print OUTPUT $_;
    }
}

close( INPUT );

close( OUTPUT );

print "File $ARGV[0] modified\n";

exec("mv $ARGV[0].new $ARGV[0]")
