#! /usr/bin/env perl

my $STVETRAANALYSISROOT = $ENV{'STVETRAANALYSISROOT'};

my $config = "$STVETRAANALYSISROOT/options/Config.xml";

open( XMLFILE, $config ) or die "Cannot open $config\n";

my $utils = "$STVETRAANALYSISROOT/python/Utils.py";

open ( UTILS, ">$utils" ) or die "Cannot open $utils for writing\n";

$magicpattern = qr/name="(?:.+?)"|type="(?:.+?)"|comparison="(?:.+?)"/;

my @Liste = ();

while ( <XMLFILE> )
{
    my @matches = $_ =~ m/($magicpattern).+?($magicpattern).+?($magicpattern).?>(.+?)</;
    if ($#matches == 3)
    {
	print "@matches\n";
	my (@tmp) = (0, 0, 0, 0);
	foreach $match (@matches)
	{
	    #print "$match\n";
	    reset;
	    if ($match =~ m/name="(.+?)"/)
	    {
		#print "  -> $1\n";
		$tmp[0] = $1;
	    }
	    elsif ($match =~ m/type="(.+?)"/)
	    {
		#print "  -> $1\n";
		$tmp[1] = $1;
	    }
	    elsif ($match =~ m/comparison="(.+?)"/)
	    {
		#print "  -> $1\n";
		$tmp[2] = $1;
	    }
	    else
	    {
		my $string = $match;
		$string =~ s/^\s+//;
		$string =~ s/\s+$//;
		$tmp[3] = $string;
	    }
	}
	#print "@tmp\n";
	push @Liste,  [@tmp];
    }
}


close( XMLFILE );

print UTILS "# Utility module\n#\n# Author : Johan Luisier <johan.luisier@epfl.ch>\n#\n";
print UTILS "# Defines a structure and the functions to read / write a tree using\n# this structure.\n#\n";
print UTILS "# Do not edit if you're not sure what to do, use the WriteUtils scrips instead!\n#\n\n";
print UTILS "from ROOT import gROOT\n\n# Create a struct\ngROOT.ProcessLine(\\\n" . "\t" ."\"struct Data{\\\n";
for (my $x = 0; $x <= $#Liste; $x++)
{
    $type = $Liste[$x][1];
    $type = ucfirst $type;
    print UTILS "\t" . $type . "_t " . $Liste[$x][0] . ";\\\n";
}
$dimension = $#Liste + 1;
print UTILS "};\")\nfrom ROOT import Data, TTree, AddressOf\n\ndef NbrOfFields():\n" . "\t" ."return $dimension;\n\n";
print UTILS "def BranchWriteSetup( myTree, structName ):\n" . "\t" ."\"\"\"Sets the branches of the given TTree myTree,";
print UTILS " using the data structure\n" . "\t" ."structName (which is of type Data).\"\"\"\n";

for (my $x = 0; $x <= $#Liste; $x++)
{
    reset;
    $name = $Liste[$x][0];
    $type = $Liste[$x][1];
    $type = ucfirst $type;
    $type = substr( $type, 0, 1 );
    print UTILS "\t" ."myTree.Branch(\"$name\", AddressOf(structName, \'$name\'), \"$name/$type\")\n"
}

print UTILS "\n\n";

print UTILS "def BranchReadSetup( myTree, structName ):\n" . "\t" ."\"\"\"Links the branches of the given TTree myTree";
print UTILS" to the data structure\n" . "\t" ."structName (which is of type Data).\"\"\"\n";

for (my $x = 0; $x <= $#Liste; $x++)
{
    reset;
    $name = $Liste[$x][0];
    $type = $Liste[$x][1];
    $type = ucfirst $type;
    $type = substr( $type, 0, 1 );
    print UTILS "\t" ."myTree.SetBranchAddress(\"$name\", AddressOf(structName, \'$name\') )\n";
}

print UTILS "\n\n";

print UTILS "def DoComparison( structName1, structName2, result ):\n" . "\t" ."\"\"\"Compares the fields of";
print UTILS " structName1 and structName2 and dumps the\n" . "\t" ."result into result\"\"\"\n";


for (my $x = 0; $x <= $#Liste; $x++)
{
    reset;
    $name = $Liste[$x][0];
    $comp = $Liste[$x][2];
    if ( $comp =~ m/no/i )
    {
	print UTILS "\t" ."result.$name = structName1.$name\n";
    }
    elsif (  $comp =~ m/yes/i )
    {
	print UTILS "\t" ."result.$name = structName1.$name - structName2.$name\n";
    }
}

print UTILS "\n" . "\t" ."return result\n\n";

print UTILS "def fillValues( stTool, vetraTool, mSector, tell1ID, strip, channel, svcBox, stChanID, vals ):\n" . "\t" ."\"\"\"";
print UTILS "Reads the values from the DB (the Vetra one and/or LHCb one), using\n" . "\t" ."the ST sector given";
print UTILS " in argument, the TELL1 source ID, the strip and\n" . "\t" ."channel. The values are put in the array";
print UTILS " vals\"\"\"\n" . "\t" ."vCond = vetraTool.getSTVetraCond( tell1ID )\n";

for (my $x = 0; $x <= $#Liste; $x++)
{
    reset;
    print UTILS "\t" ."vals.append( " . $Liste[$x][3]  ." )\n";
}

print UTILS "\n\n";

print UTILS "def writeValues( structName, listOfValues ):\n" . "\t" ."\"\"\"Reads the wanted values from the";
print UTILS " array listOfValues and writes them\n" . "\t" ."into structName (which is of type Data).\"\"\"\n";
print UTILS "\t" ."if ( len( listOfValues ) != NbrOfFields() ): return\n";

for (my $x = 0; $x <= $#Liste; $x++)
{
    reset;
    print UTILS "\t" ."structName.$Liste[$x][0] = listOfValues[$x] # $Liste[$x][3]\n";
}

print UTILS "\n";
