#! /usr/bin/perl

###############################################################################
#                                                                             #
# This script allows to compare two version of condDB file. The log file      #
# contains the strips that have changed.                                      #
#                                                                             #
# Run GetDBChanges.pl -h to see how the script works.                         #
#                                                                             #
# author : Johan Luisier <johan.luisier@epfl.ch>                              #
#                                                                             #
###############################################################################


# put all the arguments into one string, in order to catch '-h' in any case
$AllArgs = join(' ', @ARGV);

@Tags = ("Pedestal_mask", "Pedestal_value", "Hit_threshold", "CMS_threshold",
	 "Conf_threshold", "Header_correction_analog_link_00");

if ($AllArgs =~ m/-h/)
{
    print "Usage : GetDBChanges.pl oldfile newfile tagname [epsilon] [logfile]\n\n";
    print "oldfile is the old database,\nnewfile is the new one,\n";
    print "tagname is the name of the tag that you want to compare\n";
    print "\tthe result will be saved in logfile (default value = log).\n";
    print "epsilon is the precision of the comparison, for exact (or integer)\n";
    print "\tcomparison, put 0. Any other (positive) value will allow to\n";
    print "\tcompare floats : abs( x - y ) < epsilon => x == y.\n\n";
    print "In the logfile, there are two categories of strips : added and\n";
    print "removed. The Added ones have a greater value in the new file, this";
    print "\nmeans that :\n\t1) the mask has been set to \'1\',\n\t";
    print "2) the new threshold / pedestal value is higher than before.\n";
    print "Removed ones are strips that :\n\t1) were tagged \'bad\', but not ";
    print "any more,\n\t2) have a lower threshold / pedestal value than ";
    print " before.\n\n";
    print "Typical tagnames are :\n";
    foreach $tag (@Tags)
    {
	print "\t$tag\n";
    }
    print "\n";
    exit(0);
}
elsif (($#ARGV != 2) && ($#ARGV != 3) && ($#ARGV != 4))
{
    print "Usage :  GetDBChanges.pl oldfile newfile tagname comparison [logfile]\n";
    print "\t GetDBChanges.pl -h to get some help\n\n";
    exit(0);
}

# Opening the files
$oldfilename = $ARGV[0];
$newfilename = $ARGV[1];
$logfilename = "log";

if ($#ARGV == 4)
{
    $logfilename = $ARGV[4];
    $comp = $ARGV[3];
}
elsif ($#ARGV == 3)
{
    $comp = $ARGV[3];
}
else
{
    $comp = 0.;
}

open(OLDFILE, $oldfilename) or die "Cannot open $oldfilename";
open(NEWFILE, $newfilename) or die "Cannot open $newfilename";
open(LOGFILE, ">$logfilename") or die "Cannot open $logfilename";

@OLDLINES = <OLDFILE>;
@NEWLINES = <NEWFILE>;


# Radip check that files match (avoiding to compare TT with IT)
if ($#OLDLINES != $#NEWLINES)
{
    print "Files don't have the same number of lines\n";
    exit(1);
}

$TELL1 = "";
$WantedTag = $ARGV[2];

for ($i = 0; $i < $#OLDLINES; $i++)
{
    # Get the TELL1 source ID 'TELL1Boardx'
    if ($OLDLINES[$i] =~ m/<condition classID="5" name="(.+?)">/)
    {
	$TELL1 = $1;
	if ($NEWLINES[$i] =~ m/$TELL1/)
	{}
	else
	{
	    print "Line $i doesn't match\n";
	    exit(1);
	}
    }
    else
    {
	# We are processing a given TELL1, let's see if the wanted tag is read
	if ($OLDLINES[$i] =~ m/.+?name="$WantedTag".+?>(.+?)</)
	    # We found the wanted tag, let's check if the other file has the
            # same
	{
	    $Old = $1;
	    if ($NEWLINES[$i] =~ m/.+?name="$WantedTag".+?>(.+?)</)
	    {
		# Both tag are the same, the line is splitted into pieces
		# in order to allow easy comparison
		$New = $1;
		@OldTab = split(/ /, $Old);
		@NewTab = split(/ /, $New);
		@Result = ();

		@Added   = ();
		@Removed = ();
		@Equal   = ();

		foreach $j (0 ... $#OldTab)
		{
		    $nbr1 = $OldTab[$j] * 1.;
		    $nbr2 = $NewTab[$j] * 1.;
		    if ($nbr1 - $nbr2 > $comp)
		    {
			push(@Removed, $j);
		    }
		    elsif ($nbr1 - $nbr2 < -$comp)
		    {
			push(@Added, $j);
		    }
		    else
		    {
			push(@Equal, $j);
		    }
		}
		# Information are logged only if a difference is found
		if (($#Added != -1) || ($#Removed != -1))
		{
		    print LOGFILE "$TELL1\n";
		}
		if ($#Added != -1)
		{
		    print LOGFILE "Added : @Added\n";
		}
		if ($#Removed != -1)
		{
		    print LOGFILE "Removed : @Removed\n";
		}
	    }
	    else
	    {
		print "Line $i doesn't match\n";

		exit(1);
	    }
	}
    }
}


close(OLDFILE);
close(NEWFILE);
close(LOGFILE);
