#! /usr/bin/perl

###############################################################################
#                                                                             #
# This script reads the noise values in the output of STNoiseMonitor and      #
# dumps them into the LHCb conditions database.                               #
#                                                                             #
# author : Johan Luisier <johan.luisier@epfl.ch>                              #
#                                                                             #
###############################################################################

if ($#ARGV != 1)
{
    print "Usage DumpNoiseIntoConditions input output\n";
    exit(1);
}

open(DATA, "$ARGV[0]") or die "Cannot open $ARGV[1]";
open(COND, "$ARGV[1]") or die "Cannot open $ARGV[2]";

open(OUT, ">tmp.xml") or die "Cannot open tmp.xml for writing";

# Let's first build the hash
my %noiseToBeModified = ();
my %elecToBeModified = ();

my $boolean = 0;
my $key = "";
my $value = "";

my $modifyElec = 1;

while (<DATA>)
{
    if ($boolean == 1)
    {
	$boolean = 2;
	if ($_ =~ m/<paramVector .+?> (.+?)<\/paramVector>/)
	{
	    $value = $1;
	    $noiseToBeModified{ $key } = $value;
	}
	else
	{
	    print "We have a problem\n";
	}
    }
    elsif ($boolean == 2)
    {
	$boolean = 0;
	if ($_ =~ m/<paramVector .+?> (.+?)<\/paramVector>/)
	{
	    $value = $1;
	    $elecToBeModified{ $key } = $value;
	}
	else
	{
	    $modifyElec = 0;
	}
    }
    elsif ($_ =~ m/<condition classID=\"5\" name=\"(.+?)\">/)
    {
	$boolean = 1;
	$key     = $1;
    }
}

close(DATA);

# Then let's read the condition file COND and replace the values with the
# new ones

$boolean = 0;

while(<COND>)
{
    reset;
    if ($boolean == 2)
    {
	# write the value
	$_ =~ s/> .+?<\/pa/> $noiseToBeModified{$key}<\/pa/;
	$count = ($noiseToBeModified{$key} =~ tr/ //);
	print OUT $_;
	$boolean = 1;
    }
    elsif ($boolean == 1)
    {
	if ( $modifyElec == 1 )
	{
	    # write the value
	    $_ =~ s/> .+?<\/pa/> $elecToBeModified{$key}<\/pa/;
	    $count = ($elecToBeModified{$key} =~ tr/ //);
	    print OUT $_;
	}
	else
	{
	    print OUT $_;
	}
	$boolean = 0;
    }
    elsif ($_ =~ m/<condition classID=\"5\" name=\"(.+?)\">/)
    {
	$key = $1;
	$boolean = 2 if $noiseToBeModified{ $key } ;
	print OUT $_;
    }
    else
    {
	print OUT $_;
    }
}

close(COND);
close(OUT);

$command = "mv tmp.xml $ARGV[1]";

#system $command;
