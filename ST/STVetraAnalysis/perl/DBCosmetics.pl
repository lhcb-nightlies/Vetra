#! /usr/bin/env perl

open( FILE, $ARGV[0] ) or die "Cannot open $ARGV[0]\n";
open( NEW ,">$ARGV[0].tmp" ) or die "Cannot open $ARGV[0].tmp\n";

while( <FILE> )
{
    $line = $_;
    $line =~ s/><([^\/])/>\n<$1/g;
    $line =~ s/<(.+?)><\/condition>/<$1>\n<\/condition>/g;
    $line =~ s/<(.+?)><\/DDDB>/<$1>\n<\/DDDB>/g;
    $line =~ s/&gt;/>/g;
    $line =~ s/(comment=".+?") (name=".+?") (type=".+?")/$2 $3 $1/g;
    $line =~ s/"other"/"string"/g;
    $line =~ s/"std::string"/"string"/g;
    print NEW $line;
}

close( FILE );
close( NEW );

exec("mv $ARGV[0].tmp $ARGV[0]")
