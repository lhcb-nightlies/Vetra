"""
##############################################################################
#                                                                            #
#  Configuration for Vetra monitoring using full tracking system             #
#                                                                            #
#  @package Tell1/Vetra                                                      #
#  @author Mark Tobin                                                        #
#  @date   09/01/2012                                                        #
#                                                                            #
##############################################################################
"""

from GaudiKernel.ProcessJobOptions import importOptions

from Configurables import GaudiSequencer
from TrackSys.Configuration import *
from Configurables import ( TrackSys, RecSysConf )
from Configurables import TrackContainerCopy
from Configurables import Velo__VeloTrackMonitor
from Configurables import EventClockSvc
from Configurables import Vetra
from Configurables import VetraRecoConf

Vetra().MainSequence = [ 'ProcessPhase/Reco', GaudiSequencer('MoniTTSeq'), GaudiSequencer('MoniITSeq'), GaudiSequencer('MoniSTSeq') ]
#GaudiSequencer('VetraSequencer').IgnoreFilterPassed = True

TrackSys().TrackPatRecAlgorithms = TrackSys().DefaultPatRecAlgorithms
VetraRecoConf().Sequence = RecSysConf().DefaultTrackingSubdets # = ["Decoding", "VELO","TT","IT","OT","Tr","Vertex"]

from Configurables import CondDB, CondDBAccessSvc, CondDBTimeSwitchSvc
connection="sqlite_file:$STSQLDDDBROOT/db/STCOND.db/COND"
CondDB().addLayer( CondDBAccessSvc("COND", ConnectionString = connection) )

importOptions('$STTELL1ALGORITHMSROOT/options/TTEmulator.py')
importOptions('$STTELL1ALGORITHMSROOT/options/ITEmulator.py')

# Run resolution algorithm
from Configurables import STNZSResolution
ttRes = STNZSResolution("TTResolution")
ttRes.OutputLevel = 3
ttRes.DetType = "TT"
ttRes.UseNZSdata = True
ttRes.InputData =  "/Event/Raw/TT/LCMSADCs";
itRes = STNZSResolution("ITResolution")
itRes.DetType = "IT"
itRes.OutputLevel = 3
itRes.UseNZSdata = True
itRes.InputData =  "/Event/Raw/IT/LCMSADCs";

from Configurables import (ST__STNoiseMonitor, ST__STNoiseCalculation, ST__STOnlineNoiseCalculationTool)

tell1s = []
# Raw noise calculation...
MoniTTToolOnline = GaudiSequencer( 'MoniTTToolOnline' )
MoniTTToolOnline.MeasureTime = True

ttNoiseCalc = ST__STNoiseCalculation("TTNoiseCalculation")
ttNoiseCalc.NoiseToolType = "ST::STOnlineNoiseCalculationTool"
ttNoiseCalc.NoiseToolName = "TTNoiseTool"
ttNoiseCalc.OutputLevel = 3;
MoniTTToolOnline.Members.append(ttNoiseCalc)

tttool = ST__STOnlineNoiseCalculationTool("TTNoiseTool")
tttool.DetType = 'TT'
tttool.CountRoundRobin = True
tttool.OutputLevel = 3
tttool.LimitToTell = tell1s
tttool.CondPath = 'TTCondDB'
tttool.RemoveOutliers = True
tttool.FollowPeriod = 2000
tttool.MaskBadChannels = True

ttNoiseMonitor = ST__STNoiseMonitor("TTNoiseMonitor")
ttNoiseMonitor.NoiseToolType = "ST::STOnlineNoiseCalculationTool"
ttNoiseMonitor.NoiseToolName = "TTNoiseTool"
ttNoiseMonitor.UpdateRate = -1
ttNoiseMonitor.OutputLevel = 3
ttNoiseMonitor.DetType = "TT"
MoniTTToolOnline.Members.append(ttNoiseMonitor)

MoniTTToolOnline = GaudiSequencer( 'MoniTTToolOnline' )
MoniTTToolOnline.MeasureTime = True

# Raw noise calculation...
MoniITToolOnline = GaudiSequencer( 'MoniITToolOnline' )
MoniITToolOnline.MeasureTime = True

itNoiseCalc = ST__STNoiseCalculation("ITNoiseCalculation")
itNoiseCalc.NoiseToolType = "ST::STOnlineNoiseCalculationTool"
itNoiseCalc.NoiseToolName = "ITNoiseTool"
itNoiseCalc.OutputLevel = 3;
MoniITToolOnline.Members.append(itNoiseCalc)

ittool = ST__STOnlineNoiseCalculationTool("ITNoiseTool")
ittool.DetType = 'IT'
ittool.CountRoundRobin = True
ittool.OutputLevel = 3
ittool.LimitToTell = tell1s
ittool.CondPath = 'ITCondDB'
ittool.RemoveOutliers = True
ittool.FollowPeriod = 2000
ittool.MaskBadChannels = True

itNoiseMonitor = ST__STNoiseMonitor("ITNoiseMonitor")
itNoiseMonitor.NoiseToolType = "ST::STOnlineNoiseCalculationTool"
itNoiseMonitor.NoiseToolName = "ITNoiseTool"
itNoiseMonitor.UpdateRate = -1
itNoiseMonitor.OutputLevel = 3
itNoiseMonitor.DetType = "IT"
MoniITToolOnline.Members.append(itNoiseMonitor)

GaudiSequencer('MoniTTSeq').Members = [ GaudiSequencer('EmuTTSeq'), ttRes, MoniTTToolOnline]
GaudiSequencer('MoniTTSeq').IgnoreFilterPassed = True#ReturnOK = True
#GaudiSequencer('EmuTTSeq').ReturnOK = True
GaudiSequencer('MoniITSeq').Members = [ GaudiSequencer('EmuITSeq'), itRes, MoniITToolOnline]
GaudiSequencer('EmuITSeq').IgnoreFilterPassed = True#.ReturnOK = True
#GaudiSequencer('MoniITSeq').ReturnOK = True

from Configurables import TrackMonitor, ST__STClusterMonitor

trackMon = TrackMonitor()
ttClusMon = ST__STClusterMonitor("TTClusterMonitor")
ttClusMon.DetType = "TT"
itClusMon = ST__STClusterMonitor("ITClusterMonitor")
itClusMon.DetType = "IT"

GaudiSequencer('MoniSTSeq').Members = [ttClusMon, itClusMon, trackMon]

Vetra().EvtMax = 100
Vetra().OutputLevel = INFO
vetra = Vetra()

def doMyChanges():
    from Configurables import VetraInit, FilterByBankType
    bankFilter = FilterByBankType()
    bankFilter.BankNames = [ "TTFull", "ITFull" ]
    from Configurables import LoKi__ODINFilter
    odinFilter = LoKi__ODINFilter('BBODINFilter', Code = " LHCb.ODIN.BeamCrossing == ODIN_BXTYP ")
    GaudiSequencer( 'Init' ).Members = [   VetraInit(), odinFilter, bankFilter   ]
appendPostConfigAction(doMyChanges)

