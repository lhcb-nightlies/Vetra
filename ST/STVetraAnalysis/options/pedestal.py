from Gaudi.Configuration import FileCatalog, EventSelector
from Configurables import STVetraAnalysis
FileCatalog().Catalogs = [ "xmlcatalog_file:MyCatalog.xml" ]

STVetraAnalysis().DatasetName = "$MYRESULTS/Pedestal"

#STVetraAnalysis().VetraOutLvl = 3
STVetraAnalysis().DetType     = "IT"
STVetraAnalysis().Process     = "Pedestal"
STVetraAnalysis().VetraCondDB = "$VETRAROOT/VetraCondDB/ITCOND.db"
STVetraAnalysis().CondDBtag   = "head-20101106"

STVetraAnalysis().XMLAuthor      = "F Dupertuis"
STVetraAnalysis().XMLTag         = "2011/01/31"
STVetraAnalysis().XMLDescription = "Based on run 75755"

STVetraAnalysis().Convergence    = 9995

STVetraAnalysis().PrintFreq = 1000
STVetraAnalysis().EvtMax    = 10

from Configurables import CondDB
#conddb = CondDB()
#conddb.ConnectionString = 'sqlite_file:/LHCBCOND'
