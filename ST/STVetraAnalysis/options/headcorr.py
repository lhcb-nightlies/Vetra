from Gaudi.Configuration import FileCatalog, EventSelector
from Configurables import STVetraAnalysis

FileCatalog().Catalogs = [ "xmlcatalog_file:MyCatalog.xml" ]

STVetraAnalysis().DatasetName = "$MYRESULTS/HeaderCorr"

#STVetraAnalysis().VetraOutLvl = 3
STVetraAnalysis().DetType     = "IT"
STVetraAnalysis().Process     = "HeaderCorr"
STVetraAnalysis().VetraCondDB = "$MYDB/ITCOND-20110124_Pedestal.db"
STVetraAnalysis().CondDBtag   = "head-20101106"

STVetraAnalysis().XMLAuthor      = "F Dupertuis"
STVetraAnalysis().XMLTag         = "2011/01/24"
STVetraAnalysis().XMLDescription = "Based on run 82894"

STVetraAnalysis().Convergence    = 7000

STVetraAnalysis().PrintFreq = 1000
#STVetraAnalysis().EvtMax    = 10000

from Configurables import CondDB
#conddb = CondDB()
#conddb.ConnectionString = 'sqlite_file:/LHCBCOND'
