from Gaudi.Configuration import FileCatalog, EventSelector
from Configurables import STVetraAnalysis

FileCatalog().Catalogs = [ "xmlcatalog_file:MyCatalog.xml" ]

STVetraAnalysis().DatasetName = "$MYRESULTS/Noise"

#STVetraAnalysis().VetraOutLvl = 3
STVetraAnalysis().DetType     = "IT"
STVetraAnalysis().Process     = "Noise"
STVetraAnalysis().VetraCondDB = "$MYDB/ITCOND.db"
#STVetraAnalysis().DDDBtag   = "head-20100624"
#STVetraAnalysis().CondDBtag = "head-20100624"

STVetraAnalysis().XMLAuthor      = "F Dupertuis"
STVetraAnalysis().XMLTag         = "2010/07/20"
STVetraAnalysis().XMLDescription = "Based on run 75755"

STVetraAnalysis().Convergence    = 8000

STVetraAnalysis().PrintFreq = 1000
#STVetraAnalysis().EvtMax    = 42

from Configurables import CondDB
conddb = CondDB()
        
#conddb.addLayer( connStr = "sqlite_file:$MYDB/CorrectReadout.db/LHCBCOND" )
