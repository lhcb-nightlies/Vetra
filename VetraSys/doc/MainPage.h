/** \mainpage notitle
 *  \anchor vetradoxygenmain
 *
 * This reference manual documents all %LHCb and %Gaudi classes accessible from
 * the Vetra the Velo offline analysis environment. Web page will be available soon.
 *
 * <hr>
 * \htmlinclude new_release.notes

 * <b>Top level Python option files</b>
 * \li Vetra-Default-NZS_Data-Emul.py: default file to run an NZS data analysis job including the emulation
 * \li Vetra-Default-ZS_Data.py: default file to run a ZS data analysis job
 * \li VetraTELL1Emulator.py: emulation sequence
 
 */
