#ifndef VETRASYS_DETDICT_H 
#define VETRASYS_DETDICT_H 1
// ============================================================================
#include <GaudiKernel/KeyedContainer.h>

#include "VeloEvent/VeloFullBank.h"
#include "VeloEvent/VeloErrorBank.h"
#include "VeloEvent/EvtInfo.h"
#include "VeloEvent/VeloProcessInfo.h"
#include "Event/VeloODINBank.h"

ObjectVector<EvtInfo> m_0;
ObjectVector<VeloErrorBank> m_2;
ObjectVector<VeloFullBank> m_3;
ObjectVector<VeloProcessInfo> m_4;
ObjectVector<VeloODINBank> m_5;

KeyedContainer<EvtInfo, Containers::HashMap> k_11; 
KeyedContainer<VeloErrorBank, Containers::HashMap> k_12; 
KeyedContainer<VeloFullBank, Containers::HashMap> k_13; 

// ============================================================================
#endif // VETRASYS_DETDICT_H
// ============================================================================

